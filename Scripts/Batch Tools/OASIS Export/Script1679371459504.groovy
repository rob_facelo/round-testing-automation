import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')
//WebUI.setText(findTestObject('Object Repository/OASIS Export/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
//'bekahhcavtest')
//WebUI.setText(findTestObject('Object Repository/OASIS Export/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
//'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/OASIS Export/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
//'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('OASIS Export/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
//WebUI.click(findTestObject('OASIS Export/Page_Home Health Centre/input_Restrictions_btnAgree'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Oasis/OasisExport/BatchExport.aspx')

WebUI.delay(3)

WebUI.click(findTestObject('OASIS Export/Oasis EXport 2023/OASIS Export calendar icon'))

WebUI.click(findTestObject('OASIS Export/Oasis EXport 2023/span_March 2022'))

WebUI.click(findTestObject('OASIS Export/Oasis EXport 2023/td_2023'))

WebUI.click(findTestObject('OASIS Export/Oasis EXport 2023/td_Jan'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/td_OK'))

WebUI.click(findTestObject('OASIS Export/Oasis EXport 2023/td_1'))

WebUI.click(findTestObject('OASIS Export/export type step 1/export type radio button/export type'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Search'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Status_ContentPlaceHolder_BatchExportC_d757e4'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Batch'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Status_ContentPlaceHolder_BatchExportC_d757e4'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Batch'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Status_ContentPlaceHolder_BatchExportC_d757e4'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Batch'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Step 2 Download  Mark as Submitted'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Date Created_ContentPlaceHolder_Submit_012990'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_dslfacelo_ContentPlaceHolder_SubmitOas_87bcce'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_dslfacelo_ContentPlaceHolder_SubmitOas_e69169'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_Submit'))

WebUI.click(findTestObject('OASIS Export/Submit OASIS/submit oasis/div_Submit'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/span_All Submitted Oasis'))

WebUI.click(findTestObject('Object Repository/OASIS Export/Page_Oasis Export  HHC/img_Submitted Date_dxGridView_gvDetailColla_f95322'))

