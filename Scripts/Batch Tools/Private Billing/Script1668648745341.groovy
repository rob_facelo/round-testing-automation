import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')
//WebUI.setText(findTestObject('Object Repository/Private Billing/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
// 'gerashhcatest')
//WebUI.setText(findTestObject('Object Repository/Private Billing/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
// 'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/Private Billing/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
// 'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/Private Billing/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
//WebUI.click(findTestObject('Object Repository/Private Billing/Page_Home Health Centre/input_Restrictions_btnAgree'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Billing/Private/DraftClaims.aspx')

WebUI.delay(3)

//WebUI.click(findTestObject('Object Repository/Private Billing/Page_Server Maintenance/input_Thursday November 17, 2022 at (0100 A_dc8553'))
//WebUI.click(findTestObject('Object Repository/Private Billing/Page_My Dashboard  HHC/span_'))
//WebUI.click(findTestObject('Object Repository/Private Billing/Page_My Dashboard  HHC/button_'))
//WebUI.click(findTestObject('Object Repository/Private Billing/Page_My Dashboard  HHC/span_Batch Tools'))
//WebUI.click(findTestObject('Object Repository/Private Billing/Page_My Dashboard  HHC/span_Private Billing'))
WebUI.setText(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/input_Patient Last Name_ctl00ContentPlaceHo_4c017a'), 
    'white')

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/div_From                                   _ec5a75'))

WebUI.setText(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/input_Loading_ctl00ContentPlaceHolderDraftC_b4ae12'), 
    '1/1/2023')

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Cancel_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Validation_ContentPlaceHolder_DraftCla_515440'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Search_glyphicon glyphicon-send'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/img_Step 1 Draft Claims_applicationLeftNav__b3f364'))

WebUI.setText(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/input_Patient Last Name_ctl00ContentPlaceHo_5a0225'), 
    'white')

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/div_Patient Last Name                      _80ccaf'))

WebUI.setText(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/input_Loading_ctl00ContentPlaceHolderBatchC_182819'), 
    '1/1/2023')

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Cancel_glyphicon glyphicon-search_1'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_History_ContentPlaceHolder_BatchClaims_cfe75a'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Search_glyphicon glyphicon-send_1'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/td_Production_dxichCellSys'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/input_Use DSLC Validator_ctl00ContentPlaceH_f67a66'))

WebUI.setText(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/input_Enter Batch Name_ctl00ContentPlaceHol_581e24'), 
    'white private')

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Batch'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Step 3 Export Claims'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Unbatch_ContentPlaceHolder_ExportClaim_70ea21'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Move to Step 4'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_Step 4 Submitted Claims'))

WebUI.click(findTestObject('Object Repository/Private Billing/Page_Private Billing  HHC/span_32275  styles private'))

