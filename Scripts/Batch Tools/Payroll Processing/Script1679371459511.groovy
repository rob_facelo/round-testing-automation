import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

//WebUI.setText(findTestObject('Object Repository/Payroll Processing/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
   // 'bolahhacatest')

//WebUI.setText(findTestObject('Object Repository/Payroll Processing/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
   // 'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Payroll Processing/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
   // 'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.delay(3)

WebUI.navigateToUrl('https://www.ultrahhc.com/Payroll/PayrollProcessing.aspx')

WebUI.delay(3)

WebUI.setText(findTestObject('Payroll Processing/Payroll Period date/Payroll Period date/input_Payroll Period date'), '03/15/2023')

WebUI.setText(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/input_Loading_ctl00ContentPlaceHolderpgPayr_b6a3b2'), 
    '01/01/2023')

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/input_Carestaff_ctl00ContentPlaceHolderpgPa_75ab8b'))

WebUI.click(findTestObject('Payroll Processing/Payroll new Carestaff/td_walker, sarah SN'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Search'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Note Status_ContentPlaceHolder_pgPayro_4c85c3'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_150.00_ContentPlaceHolder_pgPayrollPro_c40c47'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Mark Selected as Paid'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Step 2 Generated Payroll'))

WebUI.delay(3)

WebUI.click(findTestObject('Payroll Processing/Payroll 2023/Step 2 generate payrolls/Calendar Icon'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Payroll Processing/Payroll 2023/Step 2 generate payrolls/td_15'))

WebUI.delay(3)

WebUI.click(findTestObject('Payroll Processing/Carestaff/Carestaff/input_carestaff'))

WebUI.click(findTestObject('Payroll Processing/Step 2 carestaff new/td_walker, sarah SN'))

WebUI.click(findTestObject('Payroll Processing/Step 2 search button/Payroll  process/span_search'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Post Check'))

WebUI.click(findTestObject('Payroll Processing/step 2 check date/check date/click date icon'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Payroll Processing/Payroll 2023/Post Check/td_15'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/table_Check Number_ContentPlaceHolder_Gener_372619'))

WebUI.setText(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/input_Check Number_ctl00ContentPlaceHolderG_46948e'), 
    '568978')

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Print Batch Payroll'))

WebUI.click(findTestObject('Payroll Processing/Print Batch Payroll search button/Print Batch Payroll search/span_Print Option_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Notes_ContentPlaceHolder_PrintBatchPay_052cb6'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Print To PDF'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/span_Look up By Check'))

WebUI.delay(2)

WebUI.click(findTestObject('Payroll Processing/Payroll 2023/Look Up By Check/Look up Calendar icon'))

WebUI.click(findTestObject('Payroll Processing/Payroll 2023/Look Up By Check/td_15'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/input_Loading_ctl00ContentPlaceHolderPayrol_f0aac3'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/td_Carestaff'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/input_Loading_ctl00ContentPlaceHolderPayrol_3af681'))

WebUI.click(findTestObject('Object Repository/Payroll Processing/Page_Payroll  HHC/td_Acosta, Casey SN_1_2'))

WebUI.click(findTestObject('Payroll Processing/Look up By Check/Look up by Check search/Look up by Check search'))

WebUI.delay(3)

