import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

//WebUI.setText(findTestObject('Object Repository/Send and Received/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    //'friendlynursestest')

//WebUI.setText(findTestObject('Object Repository/Send and Received/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
   // 'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Send and Received/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    //'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Send and Received/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.click(findTestObject('Object Repository/Send and Received/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Server Maintenance/maintenance'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'whi')

WebUI.click(findTestObject('Object Repository/Send and Received/Page_My Dashboard  HHC/p_6 - STYLES, FURIOUS'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/a_Recertification Assessment'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Sent Date_ptoTrackingSentDate'), 
    '02/28/2023')

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Received Date_ptoTrackingReceivedDate'), 
    '03/01/2023')

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/a_Discharge Death at Home'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Sent Date_ptoTrackingSentDate'), 
    '03/02/2023')

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Received Date_ptoTrackingReceivedDate'), 
    '03/03/2023')

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/select_2 of 2  08302022 - 102820221 of 2  0_6b2289'), 
    '1 of 2 : 01/01/2023 - 03/01/2023', true)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/a_Referred for Admission'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Sent Date_ptoTrackingSentDate'), 
    '01/02/2023')

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Received Date_ptoTrackingReceivedDate'), 
    '01/03/2023')

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/a_Transfer OASIS NOT Discharge'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Sent Date_ptoTrackingSentDate'), 
    '01/13/2023')

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Received Date_ptoTrackingReceivedDate'), 
    '01/14/2023')

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/a_Resumption of Care'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Sent Date_ptoTrackingSentDate'), 
    '01/20/2023')

WebUI.setText(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/input_Received Date_ptoTrackingReceivedDate'), 
    '01/21/2023')

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Send and Received/Page_STYLES, FURIOUS , 6  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

