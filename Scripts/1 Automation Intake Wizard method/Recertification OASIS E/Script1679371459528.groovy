import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')

//WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
   // 'bolahhacatest')

//WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
  //  'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Recertification OASIS E/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
   // 'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.click(findTestObject('Recertification OASIS E/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Server Maintenance/maintenance'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_My Dashboard  HHC/button_'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'whi')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_My Dashboard  HHC/p_450 - HEN, NESY'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_Staff(s) for this order_form-control _c820f4'), 
    'walk')

WebUI.delay(3)

WebUI.click(findTestObject('Recertification OASIS E/PTO new carestaff/div_walker, sarah SN -'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_Order Date_OrderDateNoValidation'), 
    '02/27/2023')

WebUI.selectOptionByLabel(findTestObject('Recertification OASIS E/select type of order/select_-- Select an option'), 'Recertification Assessment', 
    true)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/button_OK'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_Order_prefillprevoasis'))

WebUI.scrollToPosition(0, 5)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm0150')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_Private insurance_M0150_CPAY_1'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/select_1                                   _5d470c'), 
    '01', true)

WebUI.setText(findTestObject('Recertification OASIS E/M0080 Carestaff Name/Page_Input Carestaff Name/input_Care Staff Name'), 
    'walk')

WebUI.delay(3)

WebUI.click(findTestObject('Recertification OASIS E/Recert OASIS E new carestaff/a_walker, sarah'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('M0set lookup/input_M0Set Lookup'), 'm0110')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/select_1                                   _e3007c'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/div_M0Set Lookup                           _81d67f'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1800')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/select_0                                   _157001'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1810')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/M1810 to M1830/M1810'), '00', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/M1810 to M1830/M1820'), '00', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/M1810 to M1830/M1830'), '00', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/M1840 To M1850/M1840 to M1860/M1840'), '00', true)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1850')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/M1840 To M1850/M1840 to M1860/M1850'), '00', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/M1840 To M1850/M1840 to M1860/M1860'), '00', true)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0130')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0130 Self Care/GG0130A'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0130 Self Care/GG0130B'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0130 Self Care/GG0130C'), '88', true)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Recertification OASIS E/Page_HEN, NESY , 450  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0170')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170A'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170B'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170C'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170D'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170E'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170F'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170I'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170M'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification OASIS E/GG0170 Mobility/GG0170Q'), '0', true)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Recertification OASIS E/Page_HEN, NESY , 450  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1033')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1306')

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/select_0                                    1'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save  Next_glyphicon glyphicon-circle-_2ca2ea'))

//WebUI.setText(findTestObject('Recertification OASIS E/M1021 Primary and Other Diagnoses/M1021 Primary and Other Diagnoses/input_Primary Diagnoses'), 
//'a01')
WebUI.delay(3)

//WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/p_A01.00'))
//WebUI.setText(findTestObject('Recertification OASIS E/M1023 and M1021 Date/input_M1021 Date'), '07/27/2023')
//WebUI.click(findTestObject('Recertification OASIS E/M1021 and M1023 checkbox/M1021 checkbox 1'))
//WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Save_glyphicon glyphicon-floppy-disk'))
WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Validate Oasis_glyphicon glyphicon-ok'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('Object Repository/Recertification OASIS E/Page_HEN, NESY , 450  HHC/input_Lock Date_form-control input-sm ng-pr_f6c990'), 
    '02/27/2023')

WebUI.click(findTestObject('Recertification OASIS E/Validate and Lock OASIS/h4_Lock Oasis'))

WebUI.delay(3)

WebUI.click(findTestObject('Recertification OASIS E/Validate and Lock OASIS/Page_HEN, NESY , 450  HHC/button_Lock OASIS'))

