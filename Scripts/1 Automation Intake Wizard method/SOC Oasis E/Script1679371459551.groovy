import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2fpatient%2fpatientvisitcalendarv2.aspx%3fpatientId%3d2%26episodeId%3d7707%26patintakeId%3d55846&patientId=2&episodeId=7707&patintakeId=55846')

//WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    //'goldenstatehhacatest')

//WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
   // 'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/SOC OASIS E/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
   // 'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Server Maintenance/maintenance'))

//WebUI.delay(5)
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_My Dashboard  HHC/button_'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 'whi')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_My Dashboard  HHC/p_422 - MOOG, MARTY'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/a_01012023 - 03012023'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'a1005')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_A_A1005A'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_A_A1010A'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_concat(Workers, ,  compensation)_M015_d39120'))

WebUI.click(findTestObject('SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'a1110')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/button_Go'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _bf0a03'), 
    '0', true)

WebUI.setText(findTestObject('SOC OASIS E/A1110 Input language/A1110 Input preferred language/input_What is your preferred language'), 
    'english')

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _5d470c'), 
    '01', true)

WebUI.setText(findTestObject('SOC OASIS E/M0080 input carestaff name/M0080 input carestaff/input_Care Staff Name'), 'walk')

WebUI.click(findTestObject('SOC OASIS E/SOC OASIS E carestaff/a_walker, sarah'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm0102')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_NA  No specific SOC date ordered by p_71e9a1'))

WebUI.setText(findTestObject('SOC OASIS E/M0104 input SOC date/M0104 date of referral/input SOC date ordered by physician'), 
    '01/01/2023')

WebUI.click(findTestObject('SOC OASIS E/M0104 input SOC date/M0104 date of referral/click page for selected date/div_NA  No specific SOC date ordered by physician_panel-body panelContent'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _e3007c'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'), 'A1250')

WebUI.click(findTestObject('SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_C_A1250C'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1000')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('SOC OASIS E/M1000 checkbox/input_NA_ng-untouched ng-valid ng-dirty ng-valid-parse'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'b0200')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _50f0d6'), 
    '0', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _b29df1'), 
    '0', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _881b5d'), 
    '0', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(5)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'c0100')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _53a063'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _1b3dba'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _bf3486'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _8aadac'), 
    '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _e4edbb'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(5)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'c0400')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/C0400 ReCall/C0400_A'), '2', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/C0400 ReCall/C0400_B'), '2', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/C0400 ReCall/C0400_C'), '2', true)

WebUI.setText(findTestObject('SOC OASIS E/BIMS score C0500/input_Enter Score_'), '15')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'c1310')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _5524e8'), 
    '0', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _b53a97'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/C1310 Signs and Symptomsof Delirium/C1310_C'), '2', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/C1310 Signs and Symptomsof Delirium/C1310_D'), '0', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('SOC OASIS E/M1700 Cognitive Functioning/M1700 (1)/input_M0Set Lookup_M1700'), 'M1700')

WebUI.click(findTestObject('SOC OASIS E/M0Set search/M0Set search/M0Set search search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1700 Cognitive Functioning/Select M1700/Select M1700'), '00', true)

WebUI.click(findTestObject('SOC OASIS E/M1700 Cognitive Functioning/save M1700/span_Save_M1700'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1710')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _31b5da'), 
    '00', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _e338a2'), 
    '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'd0150')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_A1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_A2'), '2', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_B1'), '0', true)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_B2'), '0', true)
WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_C1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_C2'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_D1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_D2'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_E1'), '0', true)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_E2'), '0', true)
WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_F1'), '0', true)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_F2'), '0', true)
WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_G1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_G2'), '1', false)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_H1'), '0', false)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_H2'), '0', false)
WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_I1'), '0', false)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_I2'), '0', false)
WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'd0160')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('SOC OASIS E/D0160 Total Severity Score/input_Enter Score_'), '5')

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0700 Social Isolation/D0700'), '0', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1740')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('SOC OASIS E/M1740/input_M1740'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _6bfb23'), 
    '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1100')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('SOC OASIS E/M1100 checkbox/input_Patient lives with other person checkbox'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2102')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M2102_types and source/M2102'), '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1800')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _157001'), 
    '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1810')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1800/M1810_ability to dress upper'), '00', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1800/M1820_ability to dress lower'), '00', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _a7f4fd'), 
    '00', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _c1430d'), 
    '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1845')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1845 to M1860/M1845'), '00', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1845 to M1860/M1850'), '00', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1845 to M1860/M1860'), '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0100')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _d69a28'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100B'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100C'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100D'), 
    '8', true)

WebUI.click(findTestObject('SOC OASIS E/GG0110 checkbox/input_Z_ng-pristine ng-untouched ng-valid'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0130')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130A1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130A2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130B1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130B2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130C1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130C2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130E1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130E2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130F1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130F2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130G1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130G2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130H1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130H2'), '88', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0170')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/GG0170A1/GG0170A1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170A2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170B1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170B2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170C_MOBILITY_SOCROC_PERF'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170C_MOBILITY_DSCHG_GOAL'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170D1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170D2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170E1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170E2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170F1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170F2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170G1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170G2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170I1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170I2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170J2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170K2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170L2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170M1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170M2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170N2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170O2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170P1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170P2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _aea398'), 
    '0', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1600')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _6fb160'), 
    '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1610')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _b530f2'), 
    '00', true)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1620')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _a7f4fd_1'), 
    '04', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1630 Ostomy for Bowel Elimination/Select M1630/M1630'), '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1021')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('SOC OASIS E/M1021 Primary and Other Diagnoses/M1021 Primary and Other Diagnoses/input_Primary Diagnoses'), 
    'A01')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/p_A01.01'))

WebUI.setText(findTestObject('SOC OASIS E/M1023 and M1021 Date/input_M1021 Date'), '01/01/2023')

WebUI.click(findTestObject('SOC OASIS E/M1021 and M1023 checkbox/M1021 checkbox 1'))

WebUI.setText(findTestObject('SOC OASIS E/M1021 Primary and Other Diagnoses/M1021 Primary and Other Diagnoses/input_other Diagnoses'), 
    'a37')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/p_A37.01'))

WebUI.setText(findTestObject('SOC OASIS E/M1023 and M1021 Date/input_M1023 Date'), '01/01/2023')

WebUI.click(findTestObject('SOC OASIS E/M1021 and M1023 checkbox/M1023 Checkbox 1'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1028')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('SOC OASIS E/M1028 Active Diagnoses/Page_M1028 checkbox/M1028 checkbox'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1033')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('SOC OASIS E/M1033 Risk for Hospitalization/M1033 checkbox'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/J0510 to J0530/J0510'), '1', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'j0520')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/J0510 to J0530/J0520'), '0', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/J0510 to J0530/J0530'), '1', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1400')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1400 When is the patient dyspneic/Select M1400/M1400'), '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1060')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('SOC OASIS E/Page_M1060 Height and Weight/input_Height'), '62')

WebUI.setText(findTestObject('SOC OASIS E/Page_M1060 Height and Weight/input_Weight'), '125')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'k0520')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Mechanically altered diet_K0520C1'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Therapeutic diet_K0520D1'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1870 Feeding or Eating/M1870'), '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1306')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                    1'), 
    '1', true)

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 A1 and B1/input_Number A1'), 
    '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 A1 and B1/input_Number B1'), 
    '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 C1/input_Number C1'), '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 D1/input_Number D1'), '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 E1/input_Number E1'), '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 F1/input_Number F1'), '1')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1322')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1324 TO M1342/m1322'), '00', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1324 TO M1342/M1324'), 'NA', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1324 TO M1342/M1330'), '01', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1324 TO M1342/M1332'), '01', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1334')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1324 TO M1342/M1334'), '01', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1324 TO M1342/M1340'), '01', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M1324 TO M1342/M1342'), '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'n0415')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Antibiotic_N0415F1'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Antibiotic_N0415F2'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M2001, M2003, M2020/M2001'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M2001, M2003, M2020/M2003'), '0', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2010')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M2010 TO M2030/M2010'), '00', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M2010 TO M2030/M2020'), '00', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/M2010 TO M2030/M2030'), '00', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'o0110')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('SOC OASIS E/scroll for O0110/strong_A2. IV'), 0)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_H1. IV Medications_O0110H1A'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_H3. Antibiotics_O0110H3A'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_O1. IV Access_O0110O1A'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_O2. Peripheral_O0110O2A'))

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('SOC OASIS E/M0set lookup/input_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2200')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('SOC OASIS E/M2200 Therapy Need/input_M2200'), '8')

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/button_Validate Oasis'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('SOC OASIS E/Validate and Lock OASIS/Ock date oasis/input_Lock Date_OASIS'), '01/01/2023')

WebUI.click(findTestObject('SOC OASIS E/Validate and Lock OASIS/h4_Lock Oasis'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/button_Lock OASIS'))

