import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Add Carestaff'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Add Physician'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Admin Settings'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Insurance Setting'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Visit type Setting'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Add User'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Intake Wizard'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Secondary payer with TAC'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Plotting and Posting visits'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Private insurance posting visit by Batch posting'), [:], 
FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/SOC Oasis E'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/SOC Plan of Care'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Referred for Admission'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Transfer OASIS E Not DC'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Resumption of Care OASIS E'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Recertification OASIS E'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/Recert Plan of Care'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('1 Automation Intake Wizard method/PTO Dischsrge Death OASIS E'), [:], FailureHandling.STOP_ON_FAILURE)

//WebUI.delay(5)

//WebUI.callTestCase(findTestCase('1 Automation Add Patient method/All Test Case for Add Patient'), [:], FailureHandling.STOP_ON_FAILURE)

//WebUI.delay(5)

//WebUI.callTestCase(findTestCase('Batch Tools/all'), [:], FailureHandling.STOP_ON_FAILURE)

