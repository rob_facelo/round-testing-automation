import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

//WebUI.setText(findTestObject('Object Repository/Add User/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 'whiterosehhcatest')

//WebUI.setText(findTestObject('Object Repository/Add User/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Add User/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
   // 'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Add User/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Admin/User/User.aspx')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Add User/Page_Admin  HHC/span_Add New User_glyphicon glyphicon-user'))

WebUI.click(findTestObject('Add User/radio button carestaff/carestaff radio button/input_CareStaff_15'))

WebUI.selectOptionByLabel(findTestObject('Add User/Select carestaff from list/select_Select from CareStaff List.Acosta, Casey   SN walker, sarah   SN'), 
    'acosta, casey SN', true)

WebUI.setEncryptedText(findTestObject('Object Repository/Add User/Page_Admin  HHC/input_Password_form-control input-sm font14_5affd6'), 
    'aeHFOx8jV/A=')

WebUI.setEncryptedText(findTestObject('Add User/Add user confirm password/Add user confirm password/confrim password'), 'aeHFOx8jV/A=')

WebUI.selectOptionByValue(findTestObject('Object Repository/Add User/Page_Admin  HHC/select_System AdministratorField NurseSurve_6083f3'), 
    'number:29', true)

WebUI.click(findTestObject('Object Repository/Add User/Page_Admin  HHC/input_Enable E-signature_ng-pristine ng-unt_f3eb43'))

WebUI.click(findTestObject('Add User/User is allowed to access on every/Mon checkbox'))

WebUI.click(findTestObject('Add User/User is allowed to access on every/Tue checkbox'))

WebUI.click(findTestObject('Add User/User is allowed to access on every/Wed checkbox'))

WebUI.click(findTestObject('Add User/User is allowed to access on every/Thu checkbox'))

WebUI.click(findTestObject('Add User/User is allowed to access on every/Fri checkbox'))

WebUI.click(findTestObject('Add User/User is allowed to access on every/Sat checkbox'))

WebUI.click(findTestObject('Add User/User is allowed to access on every/Sun checkbox'))

WebUI.click(findTestObject('Object Repository/Add User/Page_Admin  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

