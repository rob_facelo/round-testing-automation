import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')
//WebUI.setText(findTestObject('Object Repository/Referred for Admission/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
//'bekahhcavtest')
//WebUI.setText(findTestObject('Object Repository/Referred for Admission/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
// 'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/Referred for Admission/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
// 'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/Referred for Admission/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
//WebUI.click(findTestObject('Object Repository/Referred for Admission/Page_Home Health Centre/input_Restrictions_btnAgree'))
//WebUI.delay(3)
//WebUI.click(findTestObject('Object Repository/Referred for Admission/Page_Server Maintenance/input_Thursday October 06, 2022 at (0100 AM_ee3f71'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Referred for Admission/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.setText(findTestObject('Object Repository/Referred for Admission/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'whi')

WebUI.click(findTestObject('Object Repository/Referred for Admission/Page_My Dashboard  HHC/p_12 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Referred for Admission/Page_STYLES, FURIOUS , 12  HHC/span_Orders'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Referred for Admission/Page_STYLES, FURIOUS , 12  HHC/a_Referred for Admission'))

WebUI.setText(findTestObject('Object Repository/Referred for Admission/Page_STYLES, FURIOUS , 12  HHC/input_Staff(s) for this order_form-control _c820f4'), 
    'walk')

WebUI.click(findTestObject('Referred for Admission/Referred PTO carestaff/div_walker, sarah SN -'))

WebUI.click(findTestObject('Object Repository/Referred for Admission/Page_STYLES, FURIOUS , 12  HHC/button_Save'))

