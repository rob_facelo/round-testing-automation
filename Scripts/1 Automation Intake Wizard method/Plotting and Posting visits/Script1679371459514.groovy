import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

//WebUI.setText(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    //'stagingtest')

//WebUI.setText(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    //'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
   // 'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Server Maintenance/maintenance'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.setText(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'whi')

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_My Dashboard  HHC/p_3 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_STYLES, FURIOUS , 3  HHC/a_View Visit Calendar'))

//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/create other note/Jul 1'))
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/create other note/Face to Face Encounter'))
//WebUI.delay(3)
//WebUI.setText(findTestObject('Plotting of Visit and Posting using visit detail/create other note/input_CareStaff'), 'Aco')
//WebUI.delay(3)
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/create other note/select SN and Physician/div_Casey Acosta SN -'))
//WebUI.setText(findTestObject('Plotting of Visit and Posting using visit detail/create other note/input_Physician'), 'Str')
//WebUI.delay(3)
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/create other note/select SN and Physician/div_Stephen  Strange General Practice'))
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/create other note/save FTF/span_Save'))
//WebUI.delay(3)
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/create other note/close FTF/close button'))
WebUI.delay(3)

//Add med supply
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/div_Jul 1_cellContent cellContentPosted'))
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/a_Med Supply'))
//WebUI.setText(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/input_Employee'), 
//'aco')
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/div_SN  - Acosta, Casey'))
//WebUI.setText(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/input_Time In'), 
//'14')
//WebUI.setText(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/input_Time Out'), 
//'15')
//WebUI.setText(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/input_supply'), 
//'be')
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/div_hydrogelskintegrity 4 oz tube'))
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/span_Add'))
//WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Med Supplies/Med Supplies/span_Save'))
//WebUI.delay(3)
WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/span_Add More Visits_glyphicon glyphicon-plus'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/div_Jan 1_cursorPointer'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/div_Jan 30_cursorPointer'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/div_Jan 31_cursorPointer'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/div_Feb 1_cursorPointer'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/div_Feb 28_cursorPointer'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/div_Mar 1_cursorPointer'))

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Plot'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/select_--Select Discipline--               _075cf9'), 
    'SN', true)

WebUI.selectOptionByLabel(findTestObject('Plotting of Visit and Posting using visit detail/Page_select carestaff/select_--(TBD)To be determined--acosta, casey'), 
    'walker, sarah', true, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Plotting of Visit and Posting using visit detail/Time in and Time out/Page_/input_Time In'), 
    '10')

WebUI.setText(findTestObject('Plotting of Visit and Posting using visit detail/Time in and Time out/Page_/input_Time Out'), 
    '11')

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Plot Frequency'))

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Done'))

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Generate Planned Visits'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Post Visit 2023/SN Jan 01'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/Visit details/a_View Details jan 01'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/select_---                                (_ce7194'), 
    'Q5002', true)

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Post'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Plotting of Visit and Posting using visit detail/scroll calendar 01 2023/span_9'), 
    0)

WebUI.delay(3)

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Post Visit 2023/SN Jan 30'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/View Visit Detail/a_View Details Jsn 30'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/select_---                                (_ce7194'), 
    'Q5001', true)

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Post'))

WebUI.delay(3)

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Post Visit 2023/SN Jan 31'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/View Visit Detail/a_View Details Jan 31'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/select_---                                (_ce7194'), 
    'Q5001', true)

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Post'))

WebUI.delay(3)

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Post Visit 2023/SN Feb 1'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/View Visit Detail/a_View Details Feb 01'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/select_---                                (_ce7194'), 
    'Q5002', true)

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Post'))

WebUI.delay(3)

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Post Visit 2023/SN Feb 28'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/View Visit Detail/a_View Details Feb 28'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/select_---                                (_ce7194'), 
    'Q5001', true)

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Post'))

WebUI.delay(3)

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Post Visit 2023/SN Mar 1'))

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Jan 2023 plot visits/View Visit Detail/a_View Details Mar 01'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/select_---                                (_ce7194'), 
    'Q5001', true)

WebUI.click(findTestObject('Object Repository/Plotting of Visit and Posting using visit detail/Page_/button_Post'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 6)

WebUI.delay(3)

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Create Visit Note/Page_/click SN'))

WebUI.delay(3)

WebUI.clickImage(findTestObject('Plotting of Visit and Posting using visit detail/Create Visit Note/Page_/img'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Plotting of Visit and Posting using visit detail/Create Visit Note/save visit note/span_Save visit note'))

WebUI.delay(3)

