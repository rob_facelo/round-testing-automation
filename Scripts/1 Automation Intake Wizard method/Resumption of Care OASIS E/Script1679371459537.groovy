import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')

//WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    //'goldenstatehhacatest')

//WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    //'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    //'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Server Maintenance/maintenance'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_My Dashboard  HHC/button_'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'whi')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_My Dashboard  HHC/p_422 - MOOG, MARTY'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_Staff(s) for this order_form-control _c820f4'), 
    'walk')

WebUI.click(findTestObject('Resumption of Care OASIS E/PTO new carestaff/div_walker, sarah SN -'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_Order Date_OrderDateNoValidation'), 
    '01/20/2023')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/td_20'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Scroll to element/PTO scroll for type order/Transfer Not DC scroll/div_Staff Information'), 
    0)

WebUI.delay(2)

WebUI.selectOptionByLabel(findTestObject('Resumption of Care OASIS E/select type of order/select_-- Select an option'), 
    'Resumption of Care', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/button_Cancel'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_Inpatient Discharge Date_InpatientDis_f61833'), 
    '01/13/2023')

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_ROC Date_ROCDate'), 
    '01/20/2023')

//WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_Prefill Oasis from SOC_prefillroc'))
WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_-- Select an option--Community61 Acu_f1af28'), 
    'number:61', true)

WebUI.delay(3)

WebUI.scrollToPosition(0, 6)

WebUI.delay(3)

WebUI.click(findTestObject('Resumption of Care OASIS E/Save PTO/button_Save'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm0150')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_concat(Workers, ,  compensation)_M015_d39120'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _5d470c'), 
    '01', true)

WebUI.setText(findTestObject('Resumption of Care OASIS E/M0080 Carestaff Name/Page_Input Carestaff Name/input_Care Staff Name'), 
    'walk')

WebUI.click(findTestObject('Resumption of Care OASIS E/Resump OASIS E new carestaff/a_walker, sarah'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm0102')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/M0102/input_M0102'), '01/20/2023')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/td_20'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _e3007c'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1000')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_A_A1250A'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_Long-term nursing facility (NF)_ng-pr_971f70'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M1005. Inpatient Discharge Date (most r_632126'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/M1005/input_M1005'), '01/13/2023')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'b1300')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _881b5d'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'c0100')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _53a063'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _1b3dba'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _bf3486'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _8aadac'), 
    '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _e4edbb'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'c0400')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/C0400 ReCall/C0400_A'), '2', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/C0400 ReCall/C0400_B'), '2', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/C0400 ReCall/C0400_C'), '2', true)

WebUI.setText(findTestObject('Resumption of Care OASIS E/BIMS score C0500/input_Enter Score_'), '15')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'c1310')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _5524e8'), 
    '0', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _b53a97'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/C1310 Signs and Symptomsof Delirium/C1310_C'), '2', 
    true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/C1310 Signs and Symptomsof Delirium/C1310_D'), '0', 
    true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1700')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _c1430d'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _31b5da'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _e338a2'), 
    '02', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'd0150')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_A1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_A2'), '2', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_B1'), '0', true)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_B2'), '0', true)
WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_C1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_C2'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_D1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_D2'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_E1'), '0', true)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_E2'), '0', true)
WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_F1'), '0', true)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_F2'), '0', true)
WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_G1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_G2'), '1', false)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_H1'), '0', false)

//WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_H2'), '0', false)
WebUI.selectOptionByValue(findTestObject('SOC OASIS E/D0150_Patient Mood/D0150_I1'), '0', false)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'd0160')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/D0160 Total Severity Score/input_Enter Score_'), '5')

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/D0700 Social Isolation/D0700'), '1', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1740')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Resumption of Care OASIS E/M1740/input_M1740'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _6bfb23'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1100')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Resumption of Care OASIS E/M1100 checkbox/input_Patient lives with other person checkbox'))

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2102')

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M2102/M2102/supervision safety'), '00', true)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1810')

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1810 to M1830/M1810'), '00', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1810 to M1830/M1820'), '00', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1810 to M1830/M1830'), '00', true)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1800')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _157001'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1840')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1840 To M1850/M1840 to M1850/M1840'), '01', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1840 To M1850/M1840 to M1850/M1845'), '01', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1840 To M1850/M1840 to M1850/M1850'), '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _a7f4fd'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0100')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100A'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100B'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100C'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100D'), 
    '1', true)

WebUI.click(findTestObject('Resumption of Care OASIS E/GG0110 checkbox/input_Z_ng-pristine ng-untouched ng-valid'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0130')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130A1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130A2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130B1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130B2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130C1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130C2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130E1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/GG0130. Self-Care/GG0130E2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130F1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130F2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130G1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130G2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130H1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0130. Self-Care/GG0130. Self-Care/GG0130H2'), '88', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0170')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/GG0170A1/GG0170A1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170A2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170B1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170B2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170C_MOBILITY_SOCROC_PERF'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170C_MOBILITY_DSCHG_GOAL'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170D1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170D2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170E1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170E2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170F1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170F2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170G1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170G2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170I1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170I2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170J2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170K2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170L2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170M1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170M2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170N2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170O2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170P1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS E/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170P2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _aea398'), 
    '0', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1600')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _6fb160'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1610')

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1610/M1610'), '00', true)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1620')

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1620 to M1630/M1620'), '00', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1620 to M1630/M1630'), '00', true)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1021')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/M1021 Primary and Other Diagnoses/input_Primary Diagnoses'), 'a0')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/p_A01.00'))

WebUI.click(findTestObject('Resumption of Care OASIS E/M1021 and M1023 checkbox/M1021 checkbox 1'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/M1023 and M1021 Date/input_M1021 Date'), '01/20/2023')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/td_20'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/M1021 Primary and Other Diagnoses/input_other Diagnoses'), 'a1')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/p_A15.0'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/M1023 and M1021 Date/input_M1023 Date'), '01/20/2023')

WebUI.click(findTestObject('Resumption of Care OASIS E/M1021 and M1023 checkbox/M1023 Checkbox 1'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1028')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Resumption of Care OASIS E/M1028 checkbox/M1028 checkbox'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1033')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Resumption of Care OASIS E/M1033 Risk for Hospitalization/M1033 checkbox'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'j0510')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/J0510 to J0530/J0510'), '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/J0510 to J0530/J0520'), '0', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/J0510 to J0530/J0530'), '1', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1400')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1400/M1400'), '00', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1060')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/M1060 Height and Weigth/Page_M1060 Height and Weight/input_Height'), 
    '62')

WebUI.setText(findTestObject('Resumption of Care OASIS E/M1060 Height and Weigth/Page_M1060 Height and Weight/input_Weight'), 
    '125')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_Feeding tube_K0520B1'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1870 Feeding or Eating/M1870'), '01', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1306')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                    1'), 
    '1', true)

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 A1 and B1/input_Number A1'), 
    '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 A1 and B1/input_Number B1'), 
    '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 C1/input_Number C1'), '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 D1/input_Number D1'), '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 E1/input_Number E1'), '1')

WebUI.setText(findTestObject('SOC OASIS E/M1311 Current No. of Unhealed Pressure Ulcer/M1311 F1/input_Number F1'), '1')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1322')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _c1430d_1'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _313b6d'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _157001_1'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _1ab15f'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1334')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _d396b2'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _b530f2'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M1324 TO M1342/M1342'), '01', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'n0415')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Resumption of Care OASIS E/N0415/N0415/input_Antibiotic_N0415F1'))

WebUI.click(findTestObject('Resumption of Care OASIS E/N0415/N0415/input_Antibiotic_N0415F2'))

WebUI.click(findTestObject('Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'o0110')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('SOC OASIS E/scroll for O0110/strong_A2. IV'), 0)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_H1. IV Medications_O0110H1A'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_H3. Antibiotics_O0110H3A'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_H4. Anticoagulation_O0110H4A'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_J1. Dialysis_O0110J1A'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_J2. Hemodialysis_O0110J2A'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2200')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Resumption of Care OASIS E/M2200 Therapy Need/input_M2200'), '8')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2001')

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M2001, M2003, M2020/M2001'), '0', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M2010 TO M2030/M2010'), '01', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care OASIS E/M2010 TO M2030/M2030'), '00', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resumption of Care OASIS E/Page_MOOG, MARTY , 422  HHC/span_Validate Oasis_glyphicon glyphicon-ok'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('SOC OASIS E/Validate and Lock OASIS/Ock date oasis/input_Lock Date_OASIS'), '01/20/2023')

WebUI.click(findTestObject('SOC OASIS E/Validate and Lock OASIS/h4_Lock Oasis'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS E/Page_MOOG, MARTY , 422  HHC/button_Lock OASIS'))

