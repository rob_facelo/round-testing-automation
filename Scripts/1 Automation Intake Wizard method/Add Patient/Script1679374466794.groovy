import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'seniorhealthacatest')

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Add Patient/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Add Patient/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Add Patient/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.click(findTestObject('Object Repository/Add Patient/Page_Server Maintenance/input_Tuesday March 21, 2023 at (0100 AM PS_0aee8e'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Add Patient/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('Object Repository/Add Patient/Page_My Dashboard  HHC/button_'))

WebUI.delay(3)

WebUI.navigateToUrl('https://www.ultrahhc.com/patient/BasicInfo.aspx?patientId=-1')

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/input__fname'), 'Hanzo')

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/input__lname'), 'Hattori')

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/input__bdate'), '11/14/1957')

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/input__zip'), '90221')

WebUI.click(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/td_COMPTON'))

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/input__address'), 'East Compton')

WebUI.click(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/i__glyphicon glyphicon-circle-arrow-right'))

WebUI.scrollToElement(findTestObject('Scroll to element/Add Patient scroll/div_ADDRESS'), 0)

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/input_HICMBI Number_medicare'), '1EG4TE5MK73')

WebUI.click(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/input_MBI_ng-pristine ng-untouched ng-valid'))

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/input_Social Security Number_basicInfoSSN'), 
    '137989564')

WebUI.click(findTestObject('Add Patient/RACE-ETHNICITY/input_check RACE-ETHNICITY'))

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Add Patient/Page_Add a Patient  HHC/i_No record found_fa fa-save'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/input_Physician for this Order_form-control_5fedb1'), 
    'bart')

WebUI.click(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/div_bartowski, ellie ,  -'))

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/input_Staff(s) for this order_form-control _c820f4'), 
    'walk')

WebUI.click(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/div_walker, sarah SN -'))

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/input_Order Date_OrderDateNoValidation'), 
    '01/01/2023')

WebUI.selectOptionByLabel(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/select_-- Select an option--Referred for Ad_c8a85e'), 
    'Referred for Admission', true)

WebUI.click(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/input_Admit_14'))

WebUI.setText(findTestObject('Add Patient/Admission page Physician/input_Primary_form-control input-sm autoCompleteInput ng-pristine ng-untouched ng-valid ng-valid-maxlength'), 
    'bart')

WebUI.click(findTestObject('Add Patient/Admission page select physician/div_bartowski, ellie ,  -'))

WebUI.scrollToElement(findTestObject('Scroll to element/Adission Page Scroll to element/span_Emergency Preparedness Plan'), 
    0)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/select_-- Select Caregiver--Admitting Care _132ebe'), 
    'Skilled Nursing', true)

WebUI.setText(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/input_Remarks_form-control input-sm autoCom_a0c535'), 
    'walk')

WebUI.click(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/div_walker, sarah - RN'))

WebUI.click(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/i_Remarks_fa fa-plus'))

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/i_Save_fa fa-floppy-o'))

WebUI.click(findTestObject('Object Repository/Add Patient/Page_HATTORI, HANZO , 4  HHC/span_Summary'))

