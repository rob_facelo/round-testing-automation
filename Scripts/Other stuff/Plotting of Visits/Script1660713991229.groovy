import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

//WebUI.setText(findTestObject('Object Repository/Plotting of Visits/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    //'worldhhcatest')

//WebUI.setText(findTestObject('Object Repository/Plotting of Visits/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    //'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Plotting of Visits/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    //'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Plotting of Visits/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.click(findTestObject('Close button/span_close'))

WebUI.setText(findTestObject('Plotting of Visits/Search Patient/input_search Patient'), 'Styles')

WebUI.click(findTestObject('Plotting of Visits/Page_Search patient/p_5 - STYLES, FURIOUS'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Plotting of Visits/Page_STYLES, FURIOUS , 21  HHC/a_View Visit Calendar'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Plotting of Visits/Page_/span_Add More Visits_glyphicon glyphicon-plus'))

WebUI.click(findTestObject('Plotting of Visits/Plot visit date/Plot visit for july/div_Jul 1_cursorPointer bgPlot'))

WebUI.click(findTestObject('Plotting of Visits/Plot visit date/Plot visit for july/div_Jul 30_cursorPointer bgPlot'))

WebUI.click(findTestObject('Plotting of Visits/Plot visit date/Plot visit for july/div_Jul 31_cursorPointer bgPlot'))

WebUI.click(findTestObject('Plotting of Visits/Plot visit date/Plot visit for july/div_Aug 1_cursorPointer'))

WebUI.click(findTestObject('Plotting of Visits/Plot visit date/Plot visit for july/div_Aug 29_cursorPointer bgPlot'))

WebUI.click(findTestObject('Object Repository/Plotting of Visits/Page_/button_Plot'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting of Visits/Page_/select_--Select Discipline--               _075cf9'), 
    'SN', true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Plotting of Visits/Page_/select_--(TBD)To be determined--Acosta, Cas_ea85cb'), 
    'Acosta, Casey', true)

WebUI.setText(findTestObject('Object Repository/Plotting of Visits/Page_/input_Time In_form-control fontsize-13-weig_ecf399'), 
    '14')

WebUI.setText(findTestObject('Plotting of Visits/time out/Page_time out/input_Time Out'), '15')

WebUI.click(findTestObject('Object Repository/Plotting of Visits/Page_/button_Plot Frequency'))

WebUI.click(findTestObject('Object Repository/Plotting of Visits/Page_/button_Done'))

WebUI.click(findTestObject('Object Repository/Plotting of Visits/Page_/button_Generate Planned Visits'))

