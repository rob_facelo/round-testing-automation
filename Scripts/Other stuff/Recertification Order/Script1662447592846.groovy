import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Recertification Order/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'greenmeadowshhcatest')

WebUI.setText(findTestObject('Object Repository/Recertification Order/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Recertification Order/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.setText(findTestObject('Object Repository/Recertification Order/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_My Dashboard  HHC/p_3 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.setText(findTestObject('PTO input Carestaff/PTO input carestaff/input_Staff(s) for this order'), 'aco')

WebUI.delay(2)

WebUI.click(findTestObject('PTO input Carestaff/PTO input carestaff/div_Acosta, Casey SN -'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/input_Order Date_OrderDateNoValidation'), 
    '08/26/2022')

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/td_26'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/select_-- Select an option--Referred for Ad_c8a85e'), 
    'Recertification Assessment', true)

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/button_OK'))

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/input_Order_prefillprevoasis'))

WebUI.scrollToPosition(0, 4)

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0170')

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0170/GG0170 items/select_GG0170B'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0170/GG0170 items/select_GG0170C'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0170/GG0170 items/select_GG0170D'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0170/GG0170 items/select_GG0170E'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0170/GG0170 items/select_GG0170F'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0170/GG0170 items/select_GG0170I'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0170/GG0170 items/select_GG0170M'), '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _64e98d'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0170/GG0170 items/select_GG0170R'), '88', true)

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0130')

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0130/GG0130 items/select_GG0130A'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0130/GG0130 items/select_GG0130B'), '88', true)

WebUI.selectOptionByValue(findTestObject('Recertification Order/GG0130/GG0130 items/select_GG0130C'), '88', true)

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/span_Validate_glyphicon glyphicon-ok'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 6)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/input_Lock Date_form-control input-sm ng-pr_f6c990'), 
    '08/26/2022')

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/td_26'))

WebUI.click(findTestObject('Object Repository/Recertification Order/Page_STYLES, FURIOUS , 3  HHC/button_Lock OASIS'))

WebUI.delay(3)

