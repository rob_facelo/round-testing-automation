import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'lifelinetest')

WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'ca.acosta')

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign 2/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'aeHFOx8jV/A=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.delay(5)

//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_/input_No_pt_pcg_report_symptoms'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_/input_COVID-19 Employee Monitoring Form_sub_7095ac'))
WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/span_'))

WebUI.delay(3)

//WebUI.click(findTestObject('Carestaff E-sign 2/Face to Face/Face to Face/i_Face to Face Encounter_fa fa-file-text'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/img'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/img_1'))
//WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_PIN_form-control input-sm ng-pristine_44578d'), 
// '4nvbrPglk7k=')
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/button_Sign Note'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/button_'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/i_RN Direct Care_fa fa-file-text'))
//WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_Temp_vs_temp'), '36')
//WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_BP_vs_bp_sys'), '100')
//WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_BP_vs_bp_dia'), '90')
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_TympTemporal_dsl_vs_temp_type'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_R_dsl_vs_bp_type'))
//WebUI.selectOptionByValue(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/select_Sitting                             _b4617f'), 
// 'Sitting', true)
//WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_PR_dsl_vs_rate'), '85')
//WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_HR_dsl_vs_rate'), '85')
//WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_RR_vs_resp_rate'), '85')
//WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_WT_vs_weight'), '125')
//WebUI.delay(3)
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/img_1_2'))
//WebUI.delay(3)
//WebUI.click(findTestObject('Carestaff E-sign 2/Visit Note/Review Button/span_Review'))
//WebUI.delay(3)
//WebUI.click(findTestObject('Carestaff E-sign 2/Visit Note/E-sign Button/span_Electronically Sign'))
//WebUI.setEncryptedText(findTestObject('Carestaff E-sign 2/Visit Note/PIN/Visit note E-sign pin/input_PIN'), '4nvbrPglk7k=')
//WebUI.click(findTestObject('Carestaff E-sign 2/Visit Note/E-sign Button/Visit note E-sign button/Sign Visit Note'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/button_Yes'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/button_'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/i_Electronically Sign_fa fa-pencil-square-o'))
//WebUI.delay(5)
//WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
// '4nvbrPglk7k=')
WebUI.setText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_To electronically sign, please enter _517c9f'), 
 '07/01/2022')
WebUI.delay(5)
WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil'))
WebUI.delay(5)
WebUI.scrollToElement(findTestObject('Scroll to element/carestaff esign scroll/carestaff esign scroll'), 0)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/i_Jul  1 2022 1113AM_fa fa-pencil-square-o'))

WebUI.delay(5)

WebUI.setEncryptedText(findTestObject('Carestaff E-sign 2/PTO/Referred for Admission/Referred for admission TWF/please enter your PIN esigpin'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Carestaff E-sign 2/PTO/Referred for Admission/E-sign button/button_Sign'))

WebUI.delay(5)

WebUI.click(findTestObject('Carestaff E-sign 2/PTO/Jul 22 2022 Transfer not DC/Span 07-22/span_Jul 22 2022 1125AM'))

WebUI.delay(5)

WebUI.setEncryptedText(findTestObject('Carestaff E-sign 2/PTO/Jul 22 2022 Transfer not DC/Tranfer not DC/input_To electronically sign'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Carestaff E-sign 2/PTO/Resumption of Care/Jul 29 2022 Resumption of Care'))

WebUI.delay(5)

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Carestaff E-sign 2/PTO/Recert Assessment/Aug 26 2022 Recert Assessment'))

WebUI.delay(5)

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Carestaff E-sign 2/PTO/DC Death/Sep 18 2022 DC Death'))

WebUI.delay(5)

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_My Dashboard  HHC/a_Start of care - further visits planned'))

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_STYLES, FURIOUS , 6  HHC/span_Electronically Sign_glyphicon glyphicon-check'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign 2/Page_STYLES, FURIOUS , 6  HHC/input_PIN_form-control input-sm ng-pristine_44578d'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign 2/Page_STYLES, FURIOUS , 6  HHC/button_Sign OASIS'))

