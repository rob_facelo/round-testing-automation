import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'carefirsthhcatest')

WebUI.setText(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.navigateToUrl('https://www.ultrahhc.com/patient/patientvisitcalendarv2.aspx?patientId=3&episodeId=7707&patintakeId=55847')
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(3)

WebUI.click(findTestObject('Close button/span_close'))

WebUI.setText(findTestObject('Plotting of Visits/Page_Search patient/Search patient'), 'Styles')

WebUI.click(findTestObject('Plotting of Visits/Page_Search patient/p_5 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_STYLES, FURIOUS , 23  HHC/a_View Visit Calendar'))

WebUI.click(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/button_Add More Visits'))

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Plot Visit/Plot visit jul 10/div_Jul 10_cursorPointer'))

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Plot Visit/div_Jul 15_cursorPointer bgPlot'))

WebUI.click(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/button_Plot'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/select_--Select Discipline--               _075cf9'), 
    'SN', true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/select_--(TBD)To be determined--Acosta, Cas_ea85cb'), 
    'Acosta, Casey', true)

WebUI.setText(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/input_Time In_form-control fontsize-13-weig_ecf399'), 
    '14')

WebUI.setText(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Time out/Page_time out/input_Time Out'), 
    '15')

WebUI.click(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/button_Plot Frequency'))

WebUI.click(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/button_Done'))

WebUI.click(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/button_Generate Planned Visits'))

WebUI.delay(3)

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Post Visit/Qiuck post july 10/Page_/click SN'))

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Post Visit/Qiuck post july 10/Page_/Quick Post jul 10'))

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Post Visit/Qiuck post july 10/Page_/button_Yes'))

WebUI.delay(3)

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Post Visit/Page_quick post jul 15/Page_jul 15 visit/a_SNvisit jul15'))

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Post Visit/Page_quick post jul 15/a_Quick Post jul 15'))

WebUI.click(findTestObject('Object Repository/Plotting and Quick Post of visit wiht visit note/Page_/button_Yes'))

WebUI.delay(3)

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Visit Note/jul 10 visit note/Page_/click SN jul 10'))

WebUI.delay(2)

WebUI.clickImage(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Visit Note/Page_/img'))

WebUI.delay(3)

WebUI.click(findTestObject('Plotting and Quick Post of visit wiht visit note/Plot Visit/Visit Note/Page_save visit note/span_Save visit note'))

WebUI.delay(10)

