import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'greenmeadowshhcatest')

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Resumption of Care/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.delay(3)
//WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_My Dashboard  HHC/p_3 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.setText(findTestObject('PTO input Carestaff/PTO input carestaff/input_Staff(s) for this order'), 'aco')

WebUI.delay(2)

WebUI.click(findTestObject('PTO input Carestaff/PTO input carestaff/div_Acosta, Casey SN -'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/input_Order Date_OrderDateNoValidation'), 
    '07/29/2022')

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Scroll to element/Resumption of care scroll/Type of order scroll'), 0)

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/select_-- Select an option--Referred for Ad_c8a85e'), 
    'Resumption of Care', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/button_OK'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/input_Inpatient Discharge Date_InpatientDis_f61833'), 
    '07/22/2022')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/td_22'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/input_ROC Date_ROCDate'), 
    '07/29/2022')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/input_Prefill Oasis from SOC_prefillroc'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/select_-- Select an option--Community61 Acu_f1af28'), 
    'number:1', true)

WebUI.scrollToPosition(0, 5)

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1000')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Resumption of Care/M1000 and M1005/check 2 - skilled nursing facility'))

WebUI.click(findTestObject('Resumption of Care/M1000 and M1005/check and uncheck UK'))

WebUI.click(findTestObject('Resumption of Care/M1000 and M1005/check and uncheck UK'))

WebUI.setText(findTestObject('Resumption of Care/M1000 and M1005/input_(M1005) date'), '07/22/2022')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/td_22'))

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0170')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170A2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170B1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170B2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170C1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170C2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170D1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170D2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170E1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170E2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170F1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170F2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170G1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170G2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170I1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170I2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170J2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170K2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170L2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170M1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170M2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170N2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170O2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170P1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170P2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170Q'), '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170R1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170R2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170RR1'), '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170S1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170S2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0170/Page_STYLES, FURIOUS , 3  HHC/GG0170SS1'), '1', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0100')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _650e73'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0100/GG0100/GG0100B'), '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0100/GG0100/GG0100C'), '1', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0100/GG0100/GG0100D'), '1', true)

WebUI.click(findTestObject('Resumption of Care/GG0110/GG0110 checkbox/GG0110 checkbox A'))

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0130')

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130A1/GG0130A1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130A2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130B1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130B2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130C1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130C2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130E1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130E2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130F1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130F2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130G1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130G2'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130H1'), '88', true)

WebUI.selectOptionByValue(findTestObject('Resumption of Care/GG0130/GG0130/GG0130H2'), '88', true)

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resumption of Care/Page_STYLES, FURIOUS , 3  HHC/span_Validate_glyphicon glyphicon-ok'))

WebUI.scrollToPosition(0, 6)

WebUI.delay(2)

WebUI.click(findTestObject('Resumption of Care/Lock Oasis/Lock Oasis'))

WebUI.delay(2)

WebUI.setText(findTestObject('Resumption of Care/Lock Oasis/lock date/input_Lock Date'), '07/29/2022')

WebUI.click(findTestObject('Resumption of Care/Lock Oasis/Lock Resump OASIS/h4_Lock Oasis'))

WebUI.delay(3)

WebUI.click(findTestObject('Resumption of Care/Lock Oasis/click lock/button_Lock OASIS'))

WebUI.delay(3)

