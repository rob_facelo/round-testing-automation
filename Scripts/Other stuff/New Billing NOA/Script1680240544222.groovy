import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://dslhhclegacy.com/logon.aspx?ReturnUrl=%2fpatient%2flogon.aspx')

WebUI.setText(findTestObject('Object Repository/New Billing NOA/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'stagingtest')

WebUI.setText(findTestObject('Object Repository/New Billing NOA/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_/div_Agency ID                              _bec41d'))

WebUI.setEncryptedText(findTestObject('Object Repository/New Billing NOA/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Server Maintenance/input_Friday March 31, 2023 at (0100 AM PST_215612'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_My Dashboard  HHC/button_'))

WebUI.delay(5)

WebUI.navigateToUrl('https://dslhhclegacy.com/Billing/PDGM/DraftClaims.aspx?type=noa_step01')

WebUI.delay(5)

//WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_My Dashboard  HHC/span_Batch Tools'))
//WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_My Dashboard  HHC/span_Batch Billing (PDGM)'))
WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/a_Navigate to New layout'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/input_Patient Last Name_ctl00ContentPlaceHo_4c017a'), 
    'taylor')

WebUI.setText(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/input_Loading_ctl00ContentPlaceHolderDraftC_b4ae12'), 
    '11/1/2022')

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_Cancel_glyphicon glyphicon-search'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_View Claim History_ContentPlaceHolder__cbe78a'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_History_ContentPlaceHolder_DraftClaims_25b98f'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_Search_glyphicon glyphicon-send'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/a_Step 2 Batch Claim'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/input_Patient Last Name_ctl00ContentPlaceHo_5a0225'), 
    'taylor')

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_Cancel_glyphicon glyphicon-search_1'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_History_ContentPlaceHolder_BatchClaims_374f06'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_View Claim History_ContentPlaceHolder__72dbcf'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_Batch Claims'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_Production_ContentPlaceHolder_BatchCla_f5121b'))

WebUI.setText(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/input_Enter Batch Name_ctl00ContentPlaceHol_581e24'), 
    'taylor noa')

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_Batch'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/a_Step 3 Submit Claim'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_Batch Status_ContentPlaceHolder_Submit_809c44'))

WebUI.click(findTestObject('Object Repository/New Billing NOA/Page_Billing  HHC/span_Manually Submitted'))

