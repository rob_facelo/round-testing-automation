import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')
//WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
//'prodtest')
//WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
//'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/Batch Tools Billing/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
//'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Billing/PDGM/DraftClaims.aspx')

WebUI.delay(5)

//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Home Health Centre/input_Restrictions_btnAgree'))
//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_My Dashboard  HHC/span_'))
//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_My Dashboard  HHC/span_Batch Tools'))
//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_My Dashboard  HHC/span_Batch Billing (PDGM)'))
WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/div_From                                   _899193'))

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/input_Loading_ctl00ContentPlaceHolderDraftC_b4ae12'), 
    '7/1/2022')

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/input_Patient Last Name_ctl00ContentPlaceHo_4c017a'), 
    'styles')

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_View PPS Billing_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_History_ContentPlaceHolder_DraftClaims_25b98f'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_View Claim History_ContentPlaceHolder__906cf1'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Search_glyphicon glyphicon-send'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Step 2 Batch Claims'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/div_From                                   _37eaf1'))

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/input_Loading_ctl00ContentPlaceHolderBatchC_182819'), 
    '7/1/2022')

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/input_Patient Last Name_ctl00ContentPlaceHo_5a0225'), 
    'styles')

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Cancel_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_History_ContentPlaceHolder_BatchClaims_374f06'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_View Claim History_ContentPlaceHolder__72dbcf'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Batch Claims'))

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/input_Enter Batch Name_ctl00ContentPlaceHol_581e24'), 
    'styles noa')

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Production_ContentPlaceHolder_BatchCla_f5121b'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Batch'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/img_Step 2 Batch Claims_applicationLeftNav__276257'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Unbatch_ContentPlaceHolder_SubmitClaim_b3342c'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Move to Step 4'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_Step 4 Submitted Claims'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Page_Billing  HHC/span_32266  styles noa'))

WebUI.closeBrowser()

