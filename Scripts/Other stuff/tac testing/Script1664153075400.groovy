import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/tac testing/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    '1staidehhacatest')

WebUI.setText(findTestObject('Object Repository/tac testing/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/tac testing/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/tac testing/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/tac testing/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.click(findTestObject('Object Repository/tac testing/Page_My Dashboard  HHC/span_'))

WebUI.setText(findTestObject('Object Repository/tac testing/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 'sty')

WebUI.click(findTestObject('Object Repository/tac testing/Page_My Dashboard  HHC/p_3 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/tac testing/Page_STYLES, FURIOUS , 3  HHC/a_View Visit Calendar'))

WebUI.click(findTestObject('Object Repository/tac testing/Page_/a_SN'))

WebUI.click(findTestObject('Object Repository/tac testing/Page_/a_View Details'))

WebUI.selectOptionByValue(findTestObject('Object Repository/tac testing/Page_/select_Select TAC test ( 07012022 - 07012023 )'), 
    'number:15264', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/tac testing/Page_/select_Select TAC Item Skilled Nursing - ALL'), 
    'number:19834', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/tac testing/Page_/select_---                                (_ce7194'), 
    'Q5001', true)

WebUI.setText(findTestObject('Object Repository/tac testing/Page_/input_Time In_txtdetSHR'), '14')

WebUI.setText(findTestObject('Object Repository/tac testing/Page_/input_Time Out_txtdetEHR'), '15')

WebUI.click(findTestObject('Object Repository/tac testing/Page_/button_Post'))

WebUI.closeBrowser()

