import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/PTO Discharge Death/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'commlarecuptvecatest')

WebUI.setText(findTestObject('Object Repository/PTO Discharge Death/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/PTO Discharge Death/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.setText(findTestObject('Object Repository/PTO Discharge Death/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_My Dashboard  HHC/p_12 - STYLES, FURIOUS'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.delay(5)

WebUI.setText(findTestObject('PTO input Carestaff/PTO input carestaff/input_Staff(s) for this order'), 'aco')

WebUI.click(findTestObject('PTO input Carestaff/PTO input carestaff/div_Acosta, Casey SN -'))

WebUI.setText(findTestObject('PTO Discharge Death/PTO date/PTO date/input_Order Date_OrderDateNoValidation'), '09/18/2022')

WebUI.click(findTestObject('PTO Discharge Death/PTO date/select date/td_18'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/select_-- Select an option--Referred for Ad_c8a85e'), 
    'Discharge Death at Home', true)

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/input_Order_prefilloasis'))

WebUI.scrollToPosition(0, 5)

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0150')

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/input_(Mark all that apply.)_M0150_CPAY_1'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0080')

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('PTO Dischsrge Death OASIS E/M0080 selec code/select_CODE'), '1', true)

WebUI.setText(findTestObject('PTO Discharge Death/OASIS M0080 carestaff name/M0080 carestaff name/input_Care Staff Name'), 
    'ACOS')

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/a_Acosta, Casey'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2005')

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/select_0                                   _9381f2'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'J1800')

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/select_0                                   _885218'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/select_0                                   _879a22'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('PTO Discharge Death/J1900/J1900 input B'), '1', true)

WebUI.selectOptionByValue(findTestObject('PTO Discharge Death/J1900/J1900 input C'), '1', true)

WebUI.scrollToPosition(0, 5)

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Validate_glyphicon glyphicon-ok'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('PTO Discharge Death/OASIS lock date/OASIS lock date/input_Lock Date'), '09/18/2022')

WebUI.click(findTestObject('PTO Discharge Death/OASIS lock date/OASIS lock date/td_18'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/div_Lock OASIS                Cancel'))

WebUI.click(findTestObject('Object Repository/PTO Discharge Death/Page_STYLES, FURIOUS , 12  HHC/button_Lock OASIS'))

WebUI.delay(3)

