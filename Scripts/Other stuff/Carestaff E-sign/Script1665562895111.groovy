import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Carestaff E-sign/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'lifelinetest')

WebUI.setText(findTestObject('Object Repository/Carestaff E-sign/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'ca.acosta')

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'aeHFOx8jV/A=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

//WebUI.click(findTestObject('Carestaff E-sign/COVID Employee Monitoring Form/COVID monitoring form/input_checkbox'))
//WebUI.click(findTestObject('Carestaff E-sign/COVID Employee Monitoring Form/COVID monitoring form/input_COVID-19 Employee Monitoring Form_submitButton'))
//WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_Server Maintenance/input_Thursday October 13, 2022 at (0100 AM_9e08b9'))
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/span_'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/i_Face to Face Encounter_fa fa-file-text'))
WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/img'))
WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/img_1'))
WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/input_PIN_form-control input-sm ng-pristine_44578d'), 
'4nvbrPglk7k=')
WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/button_Sign Note'))
WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/button_'))
WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/i_Electronically Sign_fa fa-pencil-square-o'))
WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
'4nvbrPglk7k=')
WebUI.setText(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/input_To electronically sign, please enter _517c9f'), 
'07/01/2022')
WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil'))
WebUI.scrollToElement(findTestObject('Carestaff E-sign (1)/carestaff esign scroll/carestaff esign scroll'), 0)

WebUI.click(findTestObject('Carestaff E-sign/Referred for Admission PTO/Referred for Admission E-sign/referred Jul  1 2022'))

WebUI.setEncryptedText(findTestObject('Carestaff E-sign/Referred for Admission PTO/Referred for Admission E-sign/input_PIN_esigpin'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Carestaff E-sign/Referred for Admission PTO/Referred for Admission E-sign/Sign-pencil-button'))

WebUI.delay(5)

WebUI.click(findTestObject('Carestaff E-sign/PTO order date/PTO 07-22/span_Jul 22 2022 1125AM'))

WebUI.delay(5)

WebUI.setEncryptedText(findTestObject('Carestaff E-sign/e-sign Pin text box/Transfer Not DC PIN textbox 2/input_To electronically sign'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Carestaff E-sign/PTO sign button/PTO Sign button/Sign button'))

WebUI.delay(5)

WebUI.click(findTestObject('Carestaff E-sign/PTO order date/Resumption of Care/div_Jul 29 2022 1128AM Electronically Sign'))

WebUI.setEncryptedText(findTestObject('Carestaff E-sign/e-sign Pin text box/Resumption PIN text box/please enter your PIN_esigpin'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Carestaff E-sign/PTO order date/Page_My Dashboard  HHC/i_Aug 26 2022 1131AM_fa fa-pencil-square-o'))

WebUI.setEncryptedText(findTestObject('Carestaff E-sign/e-sign Pin text box/E-sign pin/Page_My Dashboard  HHC (1)/input_PIN_008-26'), '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Carestaff E-sign/PTO order date/Page_My Dashboard  HHC/i_Sep 18 2022 1133AM_fa fa-pencil-square-o'))

WebUI.setEncryptedText(findTestObject('Carestaff E-sign/e-sign Pin text box/E-sign pin/Page_My Dashboard  HHC (1)/input_PIN_09-18'), '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_My Dashboard  HHC/a_Transferred to an inpatient facilitypatie_79f445'))

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_STYLES, FURIOUS , 6  HHC/span_Electronically Sign_glyphicon glyphicon-check'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff E-sign/Page_STYLES, FURIOUS , 6  HHC/input_PIN_form-control input-sm ng-pristine_44578d'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff E-sign/Page_STYLES, FURIOUS , 6  HHC/button_Sign OASIS'))

