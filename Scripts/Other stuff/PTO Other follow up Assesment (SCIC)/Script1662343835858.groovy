import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'provisiontest')

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_My Dashboard  HHC/span_'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_My Dashboard  HHC/p_3 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_Order Date_OrderDateNoValidation'), 
    '07/17/2022')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/td_17'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Other Follow-up Assesment (SCIC)/scroll/scroll to element saving PTO/button_palm drive  BEVERLY HILLS CA 90210_btn btn-default'), 
    0)

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Other Follow-up Assesment (SCIC)/Other follow up assesment PTO (SCIC)/select_Other follow up assesment PTO'), 
    'Other follow-up Assessment (e.g. SCIC)', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0150')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_(Mark all that apply.)_M0150_CPAY_1'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0080')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _488590'), 
    '01', true)

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'ACOS')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/a_Acosta, Casey'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    '07/17/2022')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/td_17'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _f79806'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup                           _81d67f'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0110')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1021')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'N19')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/p_N19'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_a_ng-pristine ng-untouched ng-valid'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    '07/17/2022')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/td_17'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'G6')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/p_G60.0'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_a_ng-pristine ng-untouched ng-valid'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    '07/17/2022')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/td_17'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup                           _81d67f_1'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1030')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_a_ng-pristine ng-untouched ng-valid'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_a_ng-pristine ng-untouched ng-valid'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1200')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _50f612'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup                           _81d67f'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1242')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _eba846'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1306')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _3c170d'), 
    '1', true)

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    '1')

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _eba846'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1324')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _695630'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _230053'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _488590_1'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _f64394'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _50f612'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _230053'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1400')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _eba846'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1610')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _50f612'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1620')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _c0b4ec'), 
    '00', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1630')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _50f612'), 
    '00', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup                           _81d67f_1'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1800')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _230053_1'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1810')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _230053_1'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _230053_1'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _c0b4ec_1'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _eba846_1'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _c0b4ec_1_2'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup                           _81d67f_1'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0170')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _64e98d'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _c0b4ec_1'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0130')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup_1'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2030')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _dd7b3a'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2200')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    '8')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/button_Validate'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e_1'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1021')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'A00     ')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/p_A00.0'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/button_Validate'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/input_Lock Date_form-control input-sm ng-pr_f6c990'), 
    '07/17/2022')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/td_17'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assesment (SCIC)/Page_STYLES, FURIOUS , 3  HHC/button_Lock OASIS'))

WebUI.closeBrowser()

