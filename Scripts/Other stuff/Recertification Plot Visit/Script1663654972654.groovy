import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')
//WebUI.setText(findTestObject('Object Repository/Recert Plot Visit/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
//'prodtest')
//WebUI.setText(findTestObject('Object Repository/Recert Plot Visit/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
// 'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/Recert Plot Visit/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
// 'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.setText(findTestObject('Object Repository/Recert Plot Visit/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_My Dashboard  HHC/p_12 - STYLES, FURIOUS'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_STYLES, FURIOUS , 12  HHC/a_View Visit Calendar'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/span_Add More Visits_glyphicon glyphicon-plus'))

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/div_Aug 30_cursorPointer'))

WebUI.click(findTestObject('Recert Plot Visit/Plot Visit/Plot Visits/div_Aug 31_cursorPointer'))

WebUI.click(findTestObject('Recert Plot Visit/Plot Visit/Plot Visits/div_Sept 1_cursorPointer'))

WebUI.click(findTestObject('Recert Plot Visit/Plot Visit/Plot Visits/div_Sept 30_cursorPointer'))

WebUI.click(findTestObject('Recert Plot Visit/Plot Visit/Plot Visits/div_Oct 1_cursorPointer'))

WebUI.click(findTestObject('Recert Plot Visit/Plot Visit/Plot Visits/div_Oct 28_cursorPointer'))

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/button_Plot'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recert Plot Visit/Page_/select_--Select Discipline--               _075cf9'), 
    'SN', true)

WebUI.selectOptionByLabel(findTestObject('Recert Plot Visit/select carestaff/Page_/select_--(TBD)To be determined--Acosta, Casey'), 
    'Acosta, Casey', true)

WebUI.setText(findTestObject('Object Repository/Recert Plot Visit/Page_/input_Time In_form-control fontsize-13-weig_ecf399'), 
    '14')

WebUI.setText(findTestObject('Recert Plot Visit/Visit time in and out/visit time out/input_Time Out_'), '15')

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/button_Plot Frequency'))

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/button_Done'))

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/button_Generate Planned Visits'))

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/a_SN'))

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/a_View Details'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recert Plot Visit/Page_/select_---                                (_ce7194'), 
    'Q5001', true)

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/button_Post'))

WebUI.click(findTestObject('Recert Plot Visit/Post visit using Details/SN visit aug 31/SN visit 08-31'))

WebUI.click(findTestObject('Recert Plot Visit/Post visit using Details/visit detail aug 31/View Details 08-31'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recert Plot Visit/Page_/select_---                                (_ce7194'), 
    'Q5002', true)

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/button_Post'))

WebUI.click(findTestObject('Recert Plot Visit/Post visit using Details/SN visit sep 01/SN visit 09-01'))

WebUI.click(findTestObject('Recert Plot Visit/Post visit using Details/visit detail sep 01/View Details 09-01'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Recert Plot Visit/Page_/select_---                                (_ce7194'), 
    'Q5002', true)

WebUI.click(findTestObject('Object Repository/Recert Plot Visit/Page_/button_Post'))

WebUI.delay(3)

