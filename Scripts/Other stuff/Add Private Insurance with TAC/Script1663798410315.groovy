import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'goldendayhhcatest')

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Add Private Insurance/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.delay(3)

//WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
//WebUI.delay(10)
WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_My Dashboard  HHC/span_'))

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_My Dashboard  HHC/p_12 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/span_Payer_glyphicon glyphicon-check'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/span_Add_glyphicon glyphicon-plus'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/img_Loading_ContentPlaceHolder_PatientPayee_f1a2b0'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/td_Secondary'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/img_Loading_ContentPlaceHolder_PatientPayee_405bf6'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/td_Medicaid'))

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/input_Patient Insurance ID_ctl00ContentPlac_fef986'), 
    '12568978')

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/input_Loading_ctl00ContentPlaceHolderPatien_3d995d'), 
    '7/1/2022')

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/span_Save'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/img_Visits ()_dxGridView_gvDetailCollapsedB_183c9d'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/span_Add_glyphicon glyphicon-plus_1'))

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/input_TAC_ctl00ContentPlaceHolderPatientPay_f4fab9'), 
    'test')

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/input_Loading_ctl00ContentPlaceHolderPatien_6e9352'), 
    '7/1/2022')

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/input_Loading_ctl00ContentPlaceHolderPatien_d894e4'), 
    '7/1/2023')

WebUI.click(findTestObject('Add Private Insurance/TAC code information save button/TAC code info save button/save TAC code info'))

WebUI.click(findTestObject('Add Private Insurance/TAC details collaps button/TAC Detail Collapsed Button/TAC Detail Collapsed Button'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/span_Add_glyphicon glyphicon-plus_1_2'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/img_Loading_ContentPlaceHolder_PatientPayee_3bd735'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/td_SN'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/img_Loading_ContentPlaceHolder_PatientPayee_ce829f'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Add Private Insurance/TAC details collaps button/TAC details Visit Type/Visit Type/td_All'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/img_Unit_ContentPlaceHolder_PatientPayee_pn_97352b'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/td_Visits'))

WebUI.click(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/div_Limit                                  _dd8cba'))

WebUI.setText(findTestObject('Object Repository/Add Private Insurance/Page_STYLES, FURIOUS , 12  HHC/input_Limit_ctl00ContentPlaceHolderPatientP_57e7cd'), 
    '20')

WebUI.click(findTestObject('Add Private Insurance/TAC detail save button/TAC detail save button/save TAC detail'))

WebUI.delay(3)

