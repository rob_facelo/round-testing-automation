import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/POC send and receive/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'automationrt')

WebUI.setText(findTestObject('Object Repository/POC send and receive/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/POC send and receive/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.setText(findTestObject('Object Repository/POC send and receive/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_My Dashboard  HHC/p_400 - STYLES, FURIOUS'))

WebUI.scrollToElement(findTestObject('Scroll to element/POC send and receive scroll to element/POC send and receive scroll to element/Notice of Admission Status'), 
    0)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_STYLES, FURIOUS , 400  HHC/a_Plan Of Care  485'))

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_STYLES, FURIOUS,  HHC/span_Send POC_glyphicon glyphicon-send'))

WebUI.setText(findTestObject('Object Repository/POC send and receive/Page_STYLES, FURIOUS,  HHC/input_Please Enter Date_inputDateModalDate'), 
    '07/02/2022')

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_STYLES, FURIOUS,  HHC/button_OK'))

WebUI.delay(3)

WebUI.click(findTestObject('POC send and receive/Page_STYLES, FURIOUS,  HHC/span_Electronically Sign  Receive POC_glyph_8fc90b'))

WebUI.setText(findTestObject('Object Repository/POC send and receive/Page_STYLES, FURIOUS,  HHC/input_Receive Date_form-control input-sm ng_00ca56'), 
    '07/03/2022')

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_STYLES, FURIOUS,  HHC/div_Receive Date'))

WebUI.delay(3)

WebUI.setEncryptedText(findTestObject('Object Repository/POC send and receive/Page_STYLES, FURIOUS,  HHC/input_PIN_form-control input-sm ng-pristine_44578d'), 
    '4nvbrPglk7k=')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/POC send and receive/Page_STYLES, FURIOUS,  HHC/button_Receive Only'))

