import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'prodtest')

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_My Dashboard  HHC/button_'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'moo')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_My Dashboard  HHC/p_422 - MOOG, MARTY'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Staff(s) for this order_form-control _c820f4'), 
    'aco')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/div_Acosta, Casey SN -'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Order Date_OrderDateNoValidation'), 
    '01/06/2023')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/td_6'))

WebUI.scrollToElement(findTestObject('Other Follow-up Assesment (SCIC)/scroll/PTO scroll to element/div_Tracking'), 0)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_-- Select an option--Referred for Ad_c8a85e'), 
    'object:3924', true)

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Other Follow-up Assesment (SCIC)/scroll/scroll to element saving PTO/button_palm drive  BEVERLY HILLS CA 90210_btn btn-default'), 
    0)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm0150')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Private insurance_M0150_CPAY_1'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _5d470c'), 
    '01', true)

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'aco')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/a_Acosta, Casey'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    '01/06/2023')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/td_6'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm0110')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_1                                   _e3007c'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1800')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _157001'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1840')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _c1430d'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _6bfb23'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _a7f4fd'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1810')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _157001'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _157001'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _a7f4fd'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0130')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'gg0170')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _aea398'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_01                                  _43c065'), 
    '88', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1033')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Other risk(s) not listed in 1  8_ng-p_3d781d'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                   _c1430d'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1306')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/select_0                                    1'), 
    '0', true)

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/b_ACTIVE DIAGNOSES'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'a01')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/p_A01.00'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Other risk(s) not listed in 1  8_ng-p_3d781d'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    '01/06/2023')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/td_6'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Validate Oasis_glyphicon glyphicon-ok'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/input_Lock Date_form-control input-sm ng-pr_f6c990'), 
    '01/06/2023')

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/td_6'))

WebUI.click(findTestObject('Object Repository/Other Follow-up Assessment SCIC OASIS E/Page_MOOG, MARTY , 422  HHC/button_Lock OASIS'))

WebUI.delay(5)

