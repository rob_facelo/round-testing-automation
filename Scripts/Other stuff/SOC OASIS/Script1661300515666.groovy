import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://dslhhclegacy.com/Home.aspx')
//WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 'greenmeadowshhcatest')
//WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/SOC OASIS/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
//'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
//WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_Home Health Centre/input_Restrictions_btnAgree'))
//WebUI.navigateToUrl('https://dslhhclegacy.com/patient/patient.aspx?patientId=20362')
//WebUI.delay(5)
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 'car')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_My Dashboard  HHC/p_3 - STYLES, FURIOUS'))

WebUI.scrollToElement(findTestObject('Scroll to element/Scroll patient summary SOC OASIS/SOC OASIS scroll/span_Click here to view patient task'), 
    0)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/a_07012022 - 08292022'))

WebUI.delay(3)

'M0150'
WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0150')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_(Mark all that apply.)_M0150_CPAY_1'))

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

'M0080'
WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0080')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _488590'), 
    '01', true)

WebUI.setText(findTestObject('SOC OASIS/M0080 Carestaff Name/Page_Input Carestaff Name/input_Care Staff Name'), 'ACO')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/a_Acosta, Casey'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_NA  No specific SOC date ordered by p_71e9a1'))

'M0104'
WebUI.setText(findTestObject('SOC OASIS/M0104 Date/Page_Input M0104 Date/input_M0104 Date'), '07/01/2022')

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _f79806'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0102')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1028')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1028'
WebUI.click(findTestObject('SOC OASIS/M1028 Active Diagnoses/Page_M1028 checkbox/M1028 checkbox'))

WebUI.click(findTestObject('SOC OASIS/Save button/Save Button M1028/span_Save_M1028'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1030')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1030'
WebUI.click(findTestObject('SOC OASIS/M1030 Therapies and Prognosis/M1030 checkbox and Prognisis checkbox/M1030 checkbox 1'))

WebUI.click(findTestObject('SOC OASIS/M1030 Therapies and Prognosis/M1030 checkbox and Prognisis checkbox/PROGNOSIS Checkbox/PROGNOSIS Checkbox'))

'M1033'
WebUI.click(findTestObject('SOC OASIS/M1033 Risk for Hospitalization/M1033 Risk for Hospitalization/M1033 checkbox'))

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS/Moset Lookup/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

'M1060 Height and Weight'
WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1060')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('SOC OASIS/M1060 Height and Weigth/Page_M1060 Height and Weight/input_Height'), '62')

WebUI.setText(findTestObject('SOC OASIS/M1060 Height and Weigth/Page_M1060 Height and Weight/input_Weight'), '125')

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1100')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('SOC OASIS/M1100 Patient Living Situation/M1100 Patient Living Situation checkbox/M1100 Patient Living Situation'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1200')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _50f612'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1242')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _eba846'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1306')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _3c170d'), 
    '1', true)

'M1311'
WebUI.setText(findTestObject('SOC OASIS/M1311 Current No. of Unhealed Pressure Ulcer/M1311 A1 and B1/input_Number A1'), 
    '1')

WebUI.setText(findTestObject('SOC OASIS/M1311 Current No. of Unhealed Pressure Ulcer/M1311 A1 and B1/input_Number B1'), 
    '1')

WebUI.setText(findTestObject('SOC OASIS/M1311 Current No. of Unhealed Pressure Ulcer/M1311 C1/input_Number C1'), '1')

WebUI.setText(findTestObject('SOC OASIS/M1311 Current No. of Unhealed Pressure Ulcer/M1311 D1/input_Number D1'), '1')

WebUI.setText(findTestObject('SOC OASIS/M1311 Current No. of Unhealed Pressure Ulcer/M1311 E1/input_Number E1'), '1')

WebUI.setText(findTestObject('SOC OASIS/M1311 Current No. of Unhealed Pressure Ulcer/M1311 F1/input_Number F1'), '1')

'M1322'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1322/Select M1322/M1322'), '01', true)

WebUI.delay(3)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('SOC OASIS/Moset Lookup/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1324')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1324'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _695630'), 
    '01', true)

'M1330'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _230053'), 
    '01', true)

'M1332'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _488590_1'), 
    '01', true)

'M1334'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _f64394'), 
    '01', true)

'M1340'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1340 Enter Code (1)/M1340 select (1)/select_M1340'), '01', true)

'M1342'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1342 Surgical Wound that is Observerable/M1342 select (1)/select_M1342'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1400')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1400'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1400 When is the patient dyspneic/Select M1400/M1400'), '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1600')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _ddc7f2'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup_1'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1610')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1610'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1610 Urinary Incontinence or Urinary Catheter Presence/Select M1610/M1610'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1620')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1620'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _c0b4ec'), 
    '00', true)

'M1630'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1630 Ostomy for Bowel Elimination/Select M1630/M1630'), '00', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1700')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1700'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _eba846_1'), 
    '01', true)

'M1710'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _c0b4ec_1'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1720')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _dd7b3a'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _230053_1'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/M1730 Depression Screening/M1730 Depression Screening checkbox/M1730 Depression Screening (a)'))

WebUI.click(findTestObject('SOC OASIS/M1730 Depression Screening/M1730 Depression Screening checkbox/M1730 Depression Screening (b)'))

WebUI.click(findTestObject('SOC OASIS/M1740 Cognitive/M1740 Cognitive checkbox/M1740 Cognitive'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _c0b4ec_1_2'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1800')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1800'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1800 Grooming/Select M1800/M1800'), '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1810')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1810'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1810 Current Ability to Dress Upper Body safely/M1810 Current Ability to Dress Upper Body safely/M1810'), 
    '01', true)

'M1820'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1820 Current Ability to Dress Lower Body safely/M1820 Current Ability to Dress Lower Body safely/M1820'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _c0b4ec_1_2_3'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('SOC OASIS/Moset Lookup/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1840')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1840 To M1850/M1840 to M1850/M1840'), '01', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1840 To M1850/M1840 to M1850/M1845'), '01', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1840 To M1850/M1840 to M1850/M1850'), '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('SOC OASIS/Moset Lookup/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0170')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_01                                  _31b6cf'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170A2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170B1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170B2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170C_MOBILITY_DSCHG_GOAL'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170C_MOBILITY_SOCROC_PERF'), 
    '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170D1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170D2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170E1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170E2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170F1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170F2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170G1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170G2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170I1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170I2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170J2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170K2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170L2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170M1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170M2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170N2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170O2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170P1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170P2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170Q1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170R1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170R2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170RR1'), '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170S1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170S2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0170 Mobility/Page_STYLES, FURIOUS , 3  HHC/GG0170SS1'), '1', true)

'M1860'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1860 Ambulation or Locomotion/M1860 Ambulation or Locomotion/M1860'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1870')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1870'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M1870 Feeding or Eating/M1870 Feeding or Eating/M1870'), '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0100')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100A'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100B'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100C'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0100. Prior Functioning A to D/GG0100. Prior Functioning A to D/GG0100D'), 
    '1', true)

WebUI.click(findTestObject('SOC OASIS/GG0110 Prior Device Use checkbox/GG0110 Prior Device Use checkbox/GG0110 Prior Device Use'))

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('SOC OASIS/Moset Lookup/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'GG0130')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130A1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130A2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130B1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130B2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130C1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130C2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130E1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130E2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130F1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130F2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130G1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130G2'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130H1'), '88', true)

WebUI.selectOptionByValue(findTestObject('SOC OASIS/GG0130. Self-Care/GG0130. Self-Care/GG0130H2'), '88', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('SOC OASIS/Moset Lookup/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1910')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1910'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _50f612_1'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('SOC OASIS/Moset Lookup/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2001')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M2001'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M2001, M2003, M2020/M2001, M2003, M2020/M2001'), '1', true)

'M2003'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M2001, M2003, M2020/M2001, M2003, M2020/M2003'), '1', true)

'M2010'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _693d9a'), 
    '01', true)

'M2020'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M2001, M2003, M2020/M2001, M2003, M2020/M2020'), '01', true)

'M2030'
WebUI.selectOptionByValue(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _dd7b3a_1'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2102')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M2102'
WebUI.selectOptionByValue(findTestObject('SOC OASIS/M2102 Types and Sources of Assistance/M2102 Types and Sources of Assistance/M2102'), 
    '01', true)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2200')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('SOC OASIS/M2200 Therapy Need/M2200 Therapy Need/input_M2200'), '8')

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1000')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

'M1000'
WebUI.click(findTestObject('SOC OASIS/M1000 From which of the following Inpatient Facilities/M1000 Input Checkbox NA/input_NA - M1000'))

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e_1'))

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1021')

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('SOC OASIS/M1021 and M1023 Diagnoses/M1021 Dignoses Pneumonia/Dignoses Pneumonia'), 'PNEUMONIA')

WebUI.click(findTestObject('SOC OASIS/M1021 and M1023 Diagnoses/M1021 Dignoses Pneumonia/p_ASCARIASIS PNEUMONIA'))

WebUI.setText(findTestObject('SOC OASIS/M1021 and M1023 Diagnoses/M1021 Date/input_M1021 Date'), '07/01/2022')

WebUI.click(findTestObject('SOC OASIS/Diagnoses M1021 and M1023 checkbox/Page_select chackbox/input_select on chackbox'))

WebUI.setText(findTestObject('SOC OASIS/M1021 and M1023 Diagnoses/M1023 Other Diagnoses/input_M1023 Other Diagnoses code'), 
    'J6')

WebUI.click(findTestObject('SOC OASIS/M1021 and M1023 Diagnoses/M1023 Other Diagnoses/M1023 Diagnoses other/p_COALWORKERS PNEUMOCONIOSIS'))

WebUI.click(findTestObject('SOC OASIS/Diagnoses M1021 and M1023 checkbox/Page_other Diagnoses checkbox/input_other Diagnoses checkbox'))

WebUI.setText(findTestObject('SOC OASIS/M1021 and M1023 Diagnoses/M1023 Date/input_M1023 Date'), '07/01/2022')

WebUI.delay(2)

WebUI.scrollToPosition(0, 5)

WebUI.delay(2)

WebUI.click(findTestObject('SOC OASIS/Save button/save button M0150/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/span_Validate_glyphicon glyphicon-ok'))

WebUI.delay(5)

WebUI.click(findTestObject('SOC OASIS/Validate and Lock OASIS/button_Lock Oasis'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SOC OASIS/Page_STYLES, FURIOUS , 3  HHC/input_Lock Date_form-control input-sm ng-pr_f6c990'), 
    '07/01/2022')

WebUI.click(findTestObject('SOC OASIS/Validate and Lock OASIS/h4_Lock Oasis'))

WebUI.delay(2)

WebUI.click(findTestObject('SOC OASIS/Validate and Lock OASIS/Lock OASIS'))

WebUI.delay(3)

