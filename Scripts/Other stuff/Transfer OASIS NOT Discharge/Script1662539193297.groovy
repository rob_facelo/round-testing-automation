import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'greenmeadowshhcatest')

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.delay(5)

//WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_My Dashboard  HHC/p_3 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Orders_glyphicon glyphicon-paperclip'))

//WebUI.click(findTestObject('Transfer OASIS NOT Discharge/carestaff for referred PTO/input carestaff/a_Referred for Admission'))
//WebUI.setText(findTestObject('PTO input Carestaff/PTO input carestaff/input_Staff(s) for this order'), 'aco')
//WebUI.click(findTestObject('PTO input Carestaff/PTO input carestaff/div_Acosta, Casey SN -'))
//WebUI.click(findTestObject('Transfer OASIS NOT Discharge/carestaff for referred PTO/save referred for admission/span_Save referred for admission'))
WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.setText(findTestObject('PTO input Carestaff/PTO input carestaff/input_Staff(s) for this order'), 'aco')

WebUI.delay(2)

WebUI.click(findTestObject('PTO input Carestaff/PTO input carestaff/div_Acosta, Casey SN -'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_Order Date_OrderDateNoValidation'), 
    '07/22/2022')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/td_22'))

WebUI.scrollToElement(findTestObject('Scroll to element/PTO scroll for type order/Transfer Not DC scroll/div_Staff Information'), 
    0)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_-- Select an option--Referred for Ad_c8a85e'), 
    'Transfer OASIS NOT Discharge', true)

WebUI.scrollToPosition(0, 5)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/button_Confirm'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0150')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_(Mark all that apply.)_M0150_CPAY_1'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M0080')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Transfer OASIS NOT Discharge/M0080 Carestaff name/M0080 Carestaff Name/input_Care Staff Name'), 
    'ACO')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/a_Acosta, Casey'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup                           _81d67f'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M1041')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _3c170d'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _3dfe8f'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _3c170d_1'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/div_Save                                   _1c2c7e'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2005')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _9381f2'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _693d9a'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2301')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _bdb422'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_M2401_ng-pristine ng-untouched ng-valid'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup                           _81d67f'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'M2401')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Transfer OASIS NOT Discharge/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input A'))

WebUI.click(findTestObject('Transfer OASIS NOT Discharge/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_B'))

WebUI.click(findTestObject('Transfer OASIS NOT Discharge/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_C'))

WebUI.click(findTestObject('Transfer OASIS NOT Discharge/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_D'))

WebUI.click(findTestObject('Transfer OASIS NOT Discharge/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_E'))

WebUI.click(findTestObject('Transfer OASIS NOT Discharge/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_F'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_0                                   _885218'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'J1900')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Transfer OASIS NOT Discharge/J1900/J1900/select_J1900 A'), '1', true)

WebUI.selectOptionByValue(findTestObject('Transfer OASIS NOT Discharge/J1900/J1900/select_J1900 B'), '1', true)

WebUI.selectOptionByValue(findTestObject('Transfer OASIS NOT Discharge/J1900/J1900/select_J1900 C'), '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/select_1                                   _488590'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Validate_glyphicon glyphicon-ok'))

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/input_Lock Date_form-control input-sm ng-pr_f6c990'), 
    '07/22/2022')

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/td_22'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS NOT Discharge/Page_STYLES, FURIOUS , 3  HHC/button_Lock OASIS'))

WebUI.delay(3)

