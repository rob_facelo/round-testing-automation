import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')
//WebUI.setText(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
//'provisiontest')
//WebUI.setText(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
//'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
//'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.click(findTestObject('Close button/span_close'))

WebUI.setText(findTestObject('Plotting of Visits/Search Patient/input_search Patient'), 'Styles')

WebUI.click(findTestObject('Plotting of Visits/Page_Search patient/p_5 - STYLES, FURIOUS'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_STYLES, FURIOUS , 23  HHC/a_View Visit Calendar'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/button_Batch PostingUpdate'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/input_Service Location_ctl00ContentPlaceHol_4945bc'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/td_(Q5001) Patients homeresidence'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/span_Post'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/input_Service Location_ctl00ContentPlaceHol_75e4ac'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/td_(Q5002) Assisted living facility'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/span_Post'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/input_Service Location_ctl00ContentPlaceHol_6dc10a'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/td_(Q5009) Place not otherwise specified (NOS)'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/span_Post'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/input_Service Location_ctl00ContentPlaceHol_d0fcb4'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/td_(Q5001) Patients homeresidence_1'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/span_Post'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_Visit Posting  HHC/span_Home'))

WebUI.click(findTestObject('Posting of Visits and Other Notes/close window/Page_home/span_close'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_My Dashboard  HHC/i_Next_fa fa-refresh'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_My Dashboard  HHC/span_STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_STYLES, FURIOUS , 23  HHC/a_View Visit Calendar'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/div_SN'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/span_Face to Face Encounter'))

WebUI.setText(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/input_CareStaff_form-control input-sm autoC_125ce7'), 
    'aco')

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/div_Casey Acosta SN -'))

WebUI.setText(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/input_Physician_form-control input-sm autoC_c39ce5'), 
    'stra')

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/div_Stephen  Strange General Practice -'))

WebUI.click(findTestObject('Object Repository/Posting of Visits and Other Notes/Page_/span_Save'))

WebUI.delay(5)

