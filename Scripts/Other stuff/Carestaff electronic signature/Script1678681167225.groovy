import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')

WebUI.setText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'eltahhcarecatest')

WebUI.setText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'ca.acosta')

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'aeHFOx8jV/A=')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Server Maintenance/maintenance'))
WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/button_'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Scroll to element/carestaff esign scroll/carestaff esign scroll'), 0)

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Jan  1 2023 1153AM_fa fa-pencil-square-o'))

WebUI.delay(3)

WebUI.setEncryptedText(findTestObject('Carestaff Electronic Signature/Pin text box/input_PTO PIN jan 1'), '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Electronically Sign_fa fa-pencil-square-o'))

WebUI.delay(3)

WebUI.setEncryptedText(findTestObject('Carestaff Electronic Signature/Recert POC Esign text box/input_POC PIN'), '4nvbrPglk7k=')

WebUI.clickImage(findTestObject('Carestaff Electronic Signature/POC e-sign date/click esign date/click e-sign date'))

WebUI.setText(findTestObject('Carestaff Electronic Signature/POC e-sign date/i_To electronically sign, please enter your PIN_fa fa-calendar'), 
    '01/01/2023')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Carestaff Electronic Signature/PTO for E-sign/span_Jan 13 2023 1205PM'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/div_Electronically Sign'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
    '4nvbrPglk7k=')

WebUI.setText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/input_To electronically sign, please enter _517c9f'), 
    '03/02/2023')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Carestaff Electronic Signature/PTO for E-sign/span_Jan 20 2023 1207PM'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Face to Face Encounter_fa fa-file-text'))

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/img'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/input_PIN_form-control input-sm ng-pristine_44578d'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/button_Sign Note'))

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/button_'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Scroll to element/carestaff esign scroll/carestaff esign scroll'), 0)

WebUI.click(findTestObject('Carestaff Electronic Signature/PTO for E-sign/span_Feb 27 2023 1214PM'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_RN Direct Care_fa fa-file-text'))

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/img'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/input_PIN_form-control input-sm ng-pristine_44578d'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/button_Sign Note'))

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/button_'))

WebUI.scrollToElement(findTestObject('Scroll to element/carestaff esign scroll/carestaff esign scroll'), 0)

WebUI.click(findTestObject('Carestaff Electronic Signature/PTO for E-sign/span_Mar  2 2023 1216PM'))

WebUI.setEncryptedText(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/input_To electronically sign, please enter _3d891a'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Carestaff Electronic Signature/Page_My Dashboard  HHC/i_Sign_glyphicon glyphicon-pencil_1'))

