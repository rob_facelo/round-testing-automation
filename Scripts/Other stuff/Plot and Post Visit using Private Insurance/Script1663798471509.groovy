import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'primrosetest')

WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')
WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_My Dashboard  HHC/span_'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'sty')

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_My Dashboard  HHC/p_12 - STYLES, FURIOUS'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_STYLES, FURIOUS , 12  HHC/span_View Visit Calendar_glyphicon glyphico_abd850'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/span_Add More Visits_glyphicon glyphicon-plus'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/div_Jul 3_cursorPointer'))

WebUI.click(findTestObject('Plot and Post Visit using Private insurance/Plot visit dates/Plot visit date/div_Jul 5_cursorPointer'))

WebUI.click(findTestObject('Plot and Post Visit using Private insurance/Plot visit dates/Plot visit date/div_Jul 7_cursorPointer'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/button_Plot'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_MedicareMedicaid'), 
    'Medicaid', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_--Select Discipline--               _075cf9'), 
    'SN', true)

WebUI.selectOptionByLabel(findTestObject('Plot and Post Visit using Private insurance/select carestaff/select carestaff2/select_--(TBD)To be determined--Acosta, Casey  walker, sarah'), 
    'Acosta, Casey', true)

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/button_Plot Frequency'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/button_Done'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/button_Generate Planned Visits'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/a_SN'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/a_View Details'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_---                                (_ce7194'), 
    'Q5002', true)

//WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/div_Time In'))
WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Time In_txtdetSHR'), 
    '14')

//WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/div_Time Out'))
WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Time Out_txtdetEHR'), 
    '15')

WebUI.selectOptionByValue(findTestObject('Plot and Post Visit using Private insurance/Visit details/Visit detail/TAC/TAC/select_Select TAC test ( 07012022 - 07012023 )'), 
    'number:15265', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_Select TAC Item Skilled Nursing - ALL'), 
    'number:19835', true)

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/button_Post'))

WebUI.click(findTestObject('Plot and Post Visit using Private insurance/Click SN to Post/Click SN/SN Jul 5'))

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/a_View Details'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_Select TAC test ( 07012022 - 07012023 )'), 
    'number:15264', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_Select TAC Item Skilled Nursing - ALL'), 
    'number:19834', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_---                                (_ce7194'), 
    'Q5001', true)

WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Time In_txtdetSHR'), 
    '14')

WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Time Out_txtdetEHR'), 
    '15')

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/button_Post'))

WebUI.click(findTestObject('Plot and Post Visit using Private insurance/Click SN to Post/Click SN/SN Jul 7'))

WebUI.click(findTestObject('Plot and Post Visit using Private insurance/Visit details/Visit detail/View Details jul 7'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_Select TAC test ( 07012022 - 07012023 )'), 
    'number:15264', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_Select TAC Item Skilled Nursing - ALL'), 
    'number:19834', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/select_---                                (_ce7194'), 
    'Q5002', true)

WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Time In_txtdetSHR'), 
    '14')

WebUI.setText(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/input_Time Out_txtdetEHR'), 
    '15')

WebUI.click(findTestObject('Object Repository/Plot and Post Visit using Private insurance/Page_/button_Post'))

WebUI.delay(3)

