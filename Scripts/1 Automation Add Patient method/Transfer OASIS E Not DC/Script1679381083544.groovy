import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')
//WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
//  'goldenstatehhacatest')
//WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
//  'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
//'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
//WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_Home Health Centre/input_Restrictions_btnAgree'))
//WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_Server Maintenance/input_Thursday January 19, 2023 at (1200 AM_daf82c'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_My Dashboard  HHC/button_'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'hat')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_My Dashboard  HHC/p_422 - MOOG, MARTY'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Orders_glyphicon glyphicon-paperclip'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/i_New Physician Telephone Order_fa fa-file'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_Staff(s) for this order_form-control _c820f4'), 
    'walk')

WebUI.click(findTestObject('Referred for Admission/Referred PTO carestaff/div_walker, sarah SN -'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_Order Date_OrderDateNoValidation'), 
    '01/13/2023')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/td_13'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Scroll to element/PTO scroll for type order/Transfer Not DC scroll/div_Staff Information'), 
    0)

WebUI.selectOptionByLabel(findTestObject('Transfer OASIS E Not DC/select type of order/select_-- Select an option'), 'Transfer OASIS NOT Discharge', 
    true)

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon  glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/button_Confirm'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm0150')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_Private insurance_M0150_CPAY_1'))

WebUI.setText(findTestObject('Transfer OASIS E Not DC/M0080 Carestaff Name/input_Care Staff Name'), 'walk')

WebUI.click(findTestObject('Transfer OASIS E Not DC/Transfer OASIS E not DC new carestaff/a_walker, sarah'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm0100')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2301')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/select_0                                   _bcf291'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_Check all that apply_ng-pristine ng-u_8032a4'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/select_1                                   _5d470c'), 
    '01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/select_0                                   _180d1e'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'a2122')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_Electronic Health Record_A2122A'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/div_M0Set Lookup'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'j1800')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/select_0                                   _53a063'), 
    '0', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2005')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/select_0                                   _476170'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm1041')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/select_0                                    1'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/select_1                                   _7fb073'), 
    '01', true)

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_M0Set Lookup_form-control input-sm ng_a573e6'), 
    'm2401')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Go_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Transfer OASIS E Not DC/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_B'))

WebUI.click(findTestObject('Transfer OASIS E Not DC/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_C'))

WebUI.click(findTestObject('Transfer OASIS E Not DC/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_D'))

WebUI.click(findTestObject('Transfer OASIS E Not DC/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_E'))

WebUI.click(findTestObject('Transfer OASIS E Not DC/M2401 Intervention Synopsis/Page_STYLES, FURIOUS , 9  HHC/input_F'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 5)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Validate Oasis_glyphicon glyphicon-ok'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/span_Lock Oasis_glyphicon glyphicon-lock'))

WebUI.setText(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/input_Lock Date_form-control input-sm ng-pr_f6c990'), 
    '01/13/2023')

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/td_13'))

WebUI.click(findTestObject('Object Repository/Transfer OASIS E Not DC/Page_MOOG, MARTY , 422  HHC/button_Lock OASIS'))

WebUI.delay(5)

