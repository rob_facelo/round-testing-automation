import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

//WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
   // 'eltahhcarecatest')

//WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    //'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
   // 'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Home Health Centre/input_Restrictions_btnAgree'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Billing/PDGM/DraftClaims.aspx')

WebUI.delay(5)

//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_My Dashboard  HHC/span_'))
//WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_My Dashboard  HHC/span_Batch Billing (PDGM)'))
WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/img_Loading_ContentPlaceHolder_DraftClaimsC_ea7242'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/td_All Medicare Final Claims'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/div_From                                   _899193'))

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/input_Loading_ctl00ContentPlaceHolderDraftC_b4ae12'), 
    '01/01/2023')

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/input_Patient Last Name_ctl00ContentPlaceHo_4c017a'), 
    'jim')

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_View PPS Billing_glyphicon glyphicon-search'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_History_ContentPlaceHolder_DraftClaims_25b98f'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_View Claim History_ContentPlaceHolder__906cf1'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_View Claim History_ContentPlaceHolder__cbe78a'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Generate'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/img_Step 1 Draft Claims_applicationLeftNav__b3f364'))

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/input_Patient Last Name_ctl00ContentPlaceHo_5a0225'), 
    'jim')

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Cancel_glyphicon glyphicon-search'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/img_Loading_ContentPlaceHolder_BatchClaimsC_8c9416'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/td_All Medicare Final Claims_1'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Search'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_History_ContentPlaceHolder_BatchClaims_374f06'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/td_View Claim History_dxgvCommandColumn_Mod_b6f946'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_View Claim History_ContentPlaceHolder__c76500'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Batch Claims'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/input_Enter Batch Name_ctl00ContentPlaceHol_581e24'), 
    'jim final')

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Production_ContentPlaceHolder_BatchCla_f5121b'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Batch'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/img_Step 2 Batch Claims_applicationLeftNav__276257'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Unbatch_ContentPlaceHolder_SubmitClaim_b3342c'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Move to Step 4'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_Step 4 Submitted Claims'))

WebUI.click(findTestObject('Object Repository/Batch Tools Billing/Billing final claim/Page_Billing  HHC/span_32270  styles final'))

