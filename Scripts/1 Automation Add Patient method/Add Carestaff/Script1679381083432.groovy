import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Add New Carestaff/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
    'seniorhealthacatest')

WebUI.setText(findTestObject('Object Repository/Add New Carestaff/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
    'dslfacelo')

WebUI.setEncryptedText(findTestObject('Object Repository/Add New Carestaff/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    'e7Y4nXzna+m9eOqJpNWzdA==')

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Admin/CareStaff/CareStaff.aspx')

//WebUI.delay(3)

//WebUI.setText(findTestObject('Add New Carestaff/Enter PIN/Enter PIN/input_New PIN_newpin'), '1234')

//WebUI.setText(findTestObject('Add New Carestaff/Enter PIN/Enter PIN/input_Confirm New PIN_confirmPin'), '1234')

//WebUI.click(findTestObject('Add New Carestaff/Enter PIN/Enter PIN/button_Set PIN'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/span_Add New CareStaff'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/select_Case ManagerHome Health AideMedical _d83ea5'), 
    'number:551', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/select_LPNLVNRN'), 'string:RN', 
    true)

WebUI.setText(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/input_SSN_form-control input-sm font14 ng-p_3fbc6b'), 
    '258547693')

WebUI.setText(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/input_Hire Date_hdate'), '01/01/2021')

WebUI.setText(findTestObject('Add New Carestaff/Input carestaff name/Page_input carestaff 1st name/input__first name'), 
    'sarah')

WebUI.setText(findTestObject('Add New Carestaff/Input carestaff name/Page_Carestaff last name/input__last name'), 'walker')

WebUI.setText(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/input__zip'), '90210')

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/td_BEVERLY HILLS'))

WebUI.setText(findTestObject('Add New Carestaff/Carestaff Address/Page_input Carstaff address/input__carestaff address'), 
    'Rodeo Drive')

WebUI.setText(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/input_Date of Birth_bdate'), '04/14/1980')

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/button_Save New CareStaff'))

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/div_HR RequirementsGeoPayrates'))

WebUI.scrollToElement(findTestObject('Scroll to element/Page_scroll to add visit type/div_scroll to add visit type'), 0)

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/span_New'))

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/img_Cancel_ContentPlaceHolder_pgConCarestaf_9716fc'))

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/td_RN Direct Care'))

WebUI.setText(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/input_RN Resumption of Care_ctl00ContentPla_0127a2'), 
    '150')

WebUI.setText(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/input_Loading_ctl00ContentPlaceHolderpgConC_7626f0'), 
    '1/1/2020')

WebUI.click(findTestObject('Object Repository/Add New Carestaff/Page_Admin  HHC/span_Save'))

WebUI.delay(5)

