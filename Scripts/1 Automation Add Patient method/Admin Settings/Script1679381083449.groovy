import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')

//WebUI.setText(findTestObject('Object Repository/Admin Settings/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
   // 'whiterosehhcatest')

//WebUI.setText(findTestObject('Object Repository/Admin Settings/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
   // 'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Admin Settings/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
   // 'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Admin Settings/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Admin/Agency/Agency.aspx')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/button_Edit'))

WebUI.click(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/input__zip'))

WebUI.click(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/div_ZIP Code                               _4b864e'))

WebUI.setText(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/input__zip'), '90210')

WebUI.click(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/td_BEVERLY HILLS'))

WebUI.click(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/div_( Go to Map )'))

WebUI.setText(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/input__ancyaddress'), 'rodeo drive')

WebUI.click(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/div_Phone and Extension'))

WebUI.setText(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/input__form-control input-sm font14 ng-pris_1f2552'), 
    '(654) 654-6465')

WebUI.click(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/button_Copy Agency Address'))

WebUI.delay(3)

WebUI.setText(findTestObject('Admin Settings/Agency Settings/input_State Agency ID'), '23214556987')

WebUI.setText(findTestObject('Admin Settings/Agency Settings/input_Medicare Provider Number'), '875689')

WebUI.setText(findTestObject('Admin Settings/Agency Settings/input_TIN'), '234565987')

WebUI.setText(findTestObject('Admin Settings/Agency Settings/input__National Provider ID'), '5678985645')

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Scroll to element/admin setting scroll/span_HHCAHPS'), 0)

WebUI.delay(3)

//WebUI.click(findTestObject('Admin Settings/Agency admission settings/Admission settings/Require market source'))
WebUI.click(findTestObject('Admin Settings/Agency admission settings/Admission settings/Require Referral Source'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/select_NoneCOVID-19 Employee Monitoring For_c18ed7'), 
    'Covid19Rapid', true)

WebUI.click(findTestObject('Admin Settings/Visit Calendar display time/Display time/input_Display time inout'))

WebUI.scrollToElement(findTestObject('Scroll to element/Scroll for Miles Rate/label_Mileage'), 0)

WebUI.setText(findTestObject('Admin Settings/Fix Mileage rate/input_Mileage_ng-pristine ng-untouched ng-valid ng-valid-min'), 
    '50')

WebUI.scrollToPosition(0, 6)

WebUI.click(findTestObject('Object Repository/Admin Settings/Page_Admin  HHC/button_Save'))

