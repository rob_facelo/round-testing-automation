import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')
//WebUI.setText(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
//'gerashhcatest')
//WebUI.setText(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
// 'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
//'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
//WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_Home Health Centre/input_Restrictions_btnAgree'))
//WebUI.click(findTestObject('Server Maintenance/maintenance'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'hat')

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_My Dashboard  HHC/p_24 - STYLES, FURIOUS'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_STYLES, FURIOUS , 24  HHC/a_View Visit Calendar'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/button_Add More Visits'))

WebUI.click(findTestObject('Private insurance posting visit by Batch posting/2023 Visit/div_Jan 15_cursorPointer bgPlot'))

WebUI.click(findTestObject('Private insurance posting visit by Batch posting/2023 Visit/div_Jan 17_cursorPointer bgPlot'))

WebUI.click(findTestObject('Private insurance posting visit by Batch posting/2023 Visit/div_Jan 19_cursorPointer bgPlot'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/button_Plot'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/select_MedicareMedicaid'), 
    'Medicaid', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/select_--Select Discipline--               _075cf9'), 
    'SN', true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/select_--(TBD)To be determined--Acosta, Cas_ea85cb'), 
    'walker, sarah', true, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/input_Time In_form-control fontsize-13-weig_ecf399'), 
    '18')

WebUI.setText(findTestObject('Plotting of Visits/time out/Page_time out/input_Time Out'), '19')

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/button_Plot Frequency'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/button_Done'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/button_Generate Planned Visits'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_/button_Batch PostingUpdate'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_Visit Posting  HHC/img_Service Location_ContentPlaceHolder_Bat_f2067f'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_Visit Posting  HHC/td_(Q5001) Patients homeresidence'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_Visit Posting  HHC/span_Post'))

WebUI.click(findTestObject('Private insurance posting visit by Batch posting/2023 Visit/post/button_Post Visit'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_Visit Posting  HHC/img_Service Location_ContentPlaceHolder_Bat_cc6374'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_Visit Posting  HHC/td_(Q5002) Assisted living facility'))

WebUI.click(findTestObject('Private insurance posting visit by Batch posting/batch post visit/Visit Posting/span_Post 07-05'))

WebUI.delay(3)

WebUI.click(findTestObject('Private insurance posting visit by Batch posting/2023 Visit/post/button_Post Visit'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_Visit Posting  HHC/img_Service Location_ContentPlaceHolder_Bat_a2ef70'))

WebUI.click(findTestObject('Object Repository/Private insurance posting visit by Batch posting/Page_Visit Posting  HHC/td_(Q5001) Patients homeresidence_1'))

WebUI.click(findTestObject('Private insurance posting visit by Batch posting/batch post visit/Visit Posting/div_Post 07-07'))

WebUI.delay(3)

WebUI.click(findTestObject('Private insurance posting visit by Batch posting/2023 Visit/post/button_Post Visit'))

