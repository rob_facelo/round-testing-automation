import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl('https://www.ultrahhc.com/Admin/Physician/Physician.aspx')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/span_Add Physician_glyphicon glyphicon-user'))

WebUI.setText(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/input__nationalproviderid'), '4556899882')

WebUI.setText(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/input__firstname'), 'ellie')

WebUI.setText(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/input__lastname'), 'bartowski')

WebUI.selectOptionByValue(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/select_--Please Select Specialty--General P_be2604'), 
    'string:01', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/select_--Please select taxonomy code--208D00000X'), 
    'string:208D00000X', true)

WebUI.click(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/span_New'))

WebUI.click(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/span_Primary_ContentPlaceHolder_PhysicianCo_2e466b'))

WebUI.setText(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/input_Loading_ctl00ContentPlaceHolderPhysic_308245'), 
    '90210')

WebUI.click(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/em_90210'))

WebUI.setText(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/input__ctl00ContentPlaceHolderPhysicianCont_70cba4'), 
    'palm drive')

WebUI.setText(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/input_Address Type_ctl00ContentPlaceHolderP_b0a8be'), 
    'clinic')

WebUI.click(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/input_Phone 1_ctl00ContentPlaceHolderPhysic_caf986'))

WebUI.click(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/span_Save'))

WebUI.click(findTestObject('Object Repository/Add New Physician/Page_Admin  HHC/span_Physician Information_fa fa-floppy-o savebtn'))

WebUI.delay(5)

