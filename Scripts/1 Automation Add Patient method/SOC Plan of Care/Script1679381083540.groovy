import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.maximizeWindow()
//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?edis=true')
//WebUI.setText(findTestObject('Object Repository/SOC Plan of Care/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
// 'eltahhcarecatest')
//WebUI.setText(findTestObject('Object Repository/SOC Plan of Care/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
// 'dslfacelo')
//WebUI.setEncryptedText(findTestObject('Object Repository/SOC Plan of Care/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
// 'e7Y4nXzna+m9eOqJpNWzdA==')
//WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))
//WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_Home Health Centre/input_Restrictions_btnAgree'))
//WebUI.click(findTestObject('Server Maintenance/maintenance'))
//WebUI.delay(3)
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SOC Plan of Care/Page_My Dashboard  HHC/input_Chat Support_searchKey'), 
    'hat')

WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_My Dashboard  HHC/p_3 - STYLES, FURIOUS'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Scroll to element/scroll for SOC POC/scroll for POC/span_Admission Source'), 0)

WebUI.click(findTestObject('SOC Plan of Care/SOC POC/SOC POC'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SOC Plan of Care/Page_STYLES, FURIOUS,  HHC/input_Strange, Stephen_form-control input-s_473b40'), 
    'walk')

WebUI.click(findTestObject('SOC Plan of Care/SOC POC new carestff/div_walker, sarah SN -'))

WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_STYLES, FURIOUS,  HHC/span_Goals'))

WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_STYLES, FURIOUS,  HHC/span_Goals_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_STYLES, FURIOUS,  HHC/td_Independent in HEP'))

WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_STYLES, FURIOUS,  HHC/span_Save_glyphicon glyphicon-floppy-disk'))

WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_STYLES, FURIOUS,  HHC/span_For Review_glyphicon glyphicon-pencil'))

WebUI.click(findTestObject('Object Repository/SOC Plan of Care/Page_STYLES, FURIOUS,  HHC/button_Yes'))

WebUI.delay(3)

