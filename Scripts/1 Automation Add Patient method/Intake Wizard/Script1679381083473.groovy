import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2fpto%2flogon.aspx')

//WebUI.maximizeWindow()

//WebUI.setText(findTestObject('Page_Server Maintenance/login for debuging/Input Agency ID/input_Agency ID'), 'seniorhealthacatest')

//WebUI.setText(findTestObject('Page_Server Maintenance/login for debuging/Input User ID/input_User ID'), 'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Page_Server Maintenance/login for debuging/Input User Password/input_Password'), 
    //'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Page_Server Maintenance/login for debuging/login click/click login'))

//WebUI.click(findTestObject('Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Server Maintenance/maintenance'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Home.aspx')

WebUI.delay(5)

WebUI.click(findTestObject('Page_My Dashboard  HHC/span_'))

WebUI.click(findTestObject('whats new close button/whats new/button_'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/span_Patient'))

'Step 1'
WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/span_Intake Wizard'))

WebUI.setText(findTestObject('Object Repository/Page_My Dashboard  HHC/input__fname'), 'walter')

WebUI.setText(findTestObject('Object Repository/Page_My Dashboard  HHC/input__lname'), 'white')

WebUI.setText(findTestObject('Object Repository/Page_My Dashboard  HHC/input__bdate'), '01/01/1956')

WebUI.setText(findTestObject('Object Repository/Page_My Dashboard  HHC/input__zip'), '90210')

WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/td_BEVERLY HILLS'))

WebUI.setText(findTestObject('Object Repository/Page_My Dashboard  HHC/input__address'), 'rodeo drive')

WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/i__glyphicon glyphicon-circle-arrow-right'))

WebUI.setText(findTestObject('Object Repository/Page_My Dashboard  HHC/input_HICMBI Number_medicare'), '1EG4TE5MK73')

WebUI.click(findTestObject('Patient intake step 1/Page_MBI Checkbox/input_MBI_checkbox'))

WebUI.setText(findTestObject('Object Repository/Page_My Dashboard  HHC/input_Social Security Number_SSN'), '234556988')

WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/label_3 - Black or African-American'))

WebUI.scrollToElement(findTestObject('Page_My Dashboard  HHC/button_Save'), 0)

WebUI.click(findTestObject('Patient intake step 1/Page_patient intake save button/Save_button'))

WebUI.delay(5)

'Step 2'
WebUI.setText(findTestObject('Object Repository/Page_My Dashboard  HHC/input__form-control input-sm autoCompleteIn_6600f6'), 
    'bart')

WebUI.delay(3)

WebUI.click(findTestObject('intake wizard/New Physician/div_bartowski, ellie , MD -'))

WebUI.setText(findTestObject('Patient Intake step 2/Page_step 2 input order date/input__order date'), '01/01/2023')

WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/i__fa fa-binoculars'))

WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/p_________________ Home Health Care Service_526994'))

WebUI.click(findTestObject('Patient Intake step 2/Page_Intake wizard step 2/i_Save_save button step 2'))

WebUI.delay(5)

'Step 3'
WebUI.click(findTestObject('Page_My Dashboard  HHC/Page_intake wizard admit button2/label_Admit'))

WebUI.delay(3)

WebUI.setText(findTestObject('Patient Intake step 3/Page_intake wizard step 3/Page_Intake step 3 primary physician/input_Primary physician'), 
    'bart')

WebUI.delay(3)

WebUI.click(findTestObject('intake wizard/New Physician/PHysician 2/div_echo park  BURBANK CA 91501'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_My Dashboard  HHC/select_-- Select Caregiver--Admitting Care _132ebe'), 
    'string:551', true)

WebUI.setText(findTestObject('Patient Intake step 3/Page_intake step 3 select care staff/input_select carestaff'), 'walk')

WebUI.delay(3)

WebUI.click(findTestObject('intake wizard/New Carestaff/div_walker, sarah - RN'))

WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/i_Remarks_fa fa-plus'))

WebUI.scrollToPosition(0, 5)

WebUI.click(findTestObject('Patient Intake step 3/Page_Intake Step 3 save button/Save_button step 3'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_My Dashboard  HHC/button_Go to Patient Summary Page'))

WebUI.delay(10)

