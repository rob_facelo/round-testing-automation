import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')

//WebUI.setText(findTestObject('Object Repository/Visit Type Settings/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
   // 'whiterosehhcatest')

//WebUI.setText(findTestObject('Object Repository/Visit Type Settings/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
  //  'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Visit Type Settings/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
  //  'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_Server Maintenance/input_Thursday March 02, 2023 at (1200 AM P_b5f3bf'))

//WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_My Dashboard  HHC/span_'))

//WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_My Dashboard  HHC/button_'))

WebUI.navigateToUrl('https://www.ultrahhc.com/Admin/VisitType/VisitType.aspx')

WebUI.delay(3)

//WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_My Dashboard  HHC/span_Admin'))
//WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_My Dashboard  HHC/span_Visit Types'))
WebUI.setText(findTestObject('Object Repository/Visit Type Settings/Page_Admin  HHC/input_Visit Description_ctl00ContentPlaceHo_5f3294'), 
    'rn direct care')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_Admin  HHC/span_Search_glyphicon glyphicon-search'))

WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_Admin  HHC/a_RN Direct Care'))

WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_/img_Assign Patient to Users_ContentPlaceHol_bc43c8'))

WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_/span_New'))

WebUI.setText(findTestObject('Object Repository/Visit Type Settings/Page_/input_Loading_ctl00ContentPlaceHolderVisitT_978a80'), 
    'Medicaid')

WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_/span_PRIVATE_ContentPlaceHolder_VisitTypeDe_5323cf'))

WebUI.click(findTestObject('Visit Type Settings/New Insuracen specific/New Insuracen specific'))

WebUI.setText(findTestObject('Object Repository/Visit Type Settings/Page_/input_HCPC_ctl00ContentPlaceHolderVisitType_c4db92'), 
    'G0299')

WebUI.setText(findTestObject('Object Repository/Visit Type Settings/Page_/input_Amount_ctl00ContentPlaceHolderVisitTy_77b235'), 
    '140')

WebUI.setText(findTestObject('Object Repository/Visit Type Settings/Page_/input_Loading_ctl00ContentPlaceHolderVisitT_2ce992'), 
    '1/1/2023')

WebUI.setText(findTestObject('Object Repository/Visit Type Settings/Page_/input_Loading_ctl00ContentPlaceHolderVisitT_9b07f8'), 
    '1/1/2029')

WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_/span_Save HCPC'))

WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_/span_Save'))

//WebUI.click(findTestObject('Object Repository/Visit Type Settings/Page_/span_Save'))

