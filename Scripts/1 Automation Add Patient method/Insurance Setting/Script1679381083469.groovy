import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')

//WebUI.maximizeWindow()

//WebUI.navigateToUrl('https://www.ultrahhc.com/logon.aspx?ReturnUrl=%2f')

//WebUI.setText(findTestObject('Object Repository/Insurance Setting/Page_/input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538'), 
   // 'bekahhcavtest')

//WebUI.setText(findTestObject('Object Repository/Insurance Setting/Page_/input_User ID_ctl00ContentPlaceHolderLogonP_952ea2'), 
   // 'dslfacelo')

//WebUI.setEncryptedText(findTestObject('Object Repository/Insurance Setting/Page_/input_Password_ctl00ContentPlaceHolderLogon_5de2a7'), 
    //'e7Y4nXzna+m9eOqJpNWzdA==')

//WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_/input_Password_ctl00ContentPlaceHolderLogon_023c19'))

//WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_Home Health Centre/input_Restrictions_btnAgree'))

//WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_Server Maintenance/input_Thursday March 02, 2023 at (1200 AM P_b5f3bf'))
//WebUI.delay(3)
//WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_My Dashboard  HHC/span_'))
WebUI.navigateToUrl('https://www.ultrahhc.com/Admin/Insurance/Insurance.aspx')

//WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_My Dashboard  HHC/span_Admin'))
//WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_My Dashboard  HHC/span_Insurance'))
WebUI.setText(findTestObject('Object Repository/Insurance Setting/Page_Admin  Insurance/input_Insurance Name_ctl00ContentPlaceHolde_853373'), 
    'medicaid')

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_Admin  Insurance/span_Search'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_Admin  Insurance/a_Medicaid'))

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_/span_Edit_glyphicon glyphicon-edit'))

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_/div_Zip                                    _d1db20'))

WebUI.setText(findTestObject('Object Repository/Insurance Setting/Page_/input__Zip'), '90210')

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_/td_BEVERLY HILLS'))

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_/div__col-xs-12 col-sm-12 col-md-6 col-lg-6'))

WebUI.setText(findTestObject('Object Repository/Insurance Setting/Page_/input__form-control input-sm font14 ng-pris_03a462'), 
    'rodeo drive')

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_/div__col-xs-12 col-sm-12 col-md-6 col-lg-6'))

WebUI.setText(findTestObject('Object Repository/Insurance Setting/Page_/input__form-control input-sm font14 ng-pris_03a462'), 
    '123456789')

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_/input_Check TAC Details Before Plotting Vis_757f1f'))

WebUI.click(findTestObject('Object Repository/Insurance Setting/Page_/span_Save_glyphicon glyphicon-floppy-disk'))

