<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Sign_glyphicon glyphicon-pencil</name>
   <tag></tag>
   <elementGuidId>522ca129-f3d7-4053-b875-32cc072726f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='esigntemplatemodal']/div/div/div[3]/div/div/div[2]/button/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-default.pull-left.esignbtn-holder > i.glyphicon.glyphicon-pencil</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>483c94f0-3783-478f-9dc0-77c4c1b85f70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-pencil</value>
      <webElementGuid>81c20ef4-f111-4b81-90b3-8012f8280e59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;esigntemplatemodal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer esigfooter&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-2 col-lg-2&quot;]/button[@class=&quot;btn btn-default pull-left esignbtn-holder&quot;]/i[@class=&quot;glyphicon glyphicon-pencil&quot;]</value>
      <webElementGuid>e56f721e-2c72-42cd-bf9f-16d0ebc55ab6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='esigntemplatemodal']/div/div/div[3]/div/div/div[2]/button/i</value>
      <webElementGuid>52ca319c-669f-401d-b70e-63a71fbc6618</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/i</value>
      <webElementGuid>d449956a-aced-4f16-8511-1db4409b3837</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
