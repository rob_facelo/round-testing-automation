<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_RN Direct Care_fa fa-file-text</name>
   <tag></tag>
   <elementGuidId>f493218d-bb1e-4d06-9058-772da344d2db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesForETbl_DXDataRow1']/td[7]/span/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesForETbl_DXDataRow1 > td.dxgv > span.showpanelclassholder > i.fa.fa-file-text</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>6661b50f-0d32-479e-9fb9-67393e960dd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-file-text</value>
      <webElementGuid>dddfd866-a77d-4a7c-8d5d-a8371cec9ac0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesForETbl_DXDataRow1&quot;)/td[@class=&quot;dxgv&quot;]/span[@class=&quot;showpanelclassholder&quot;]/i[@class=&quot;fa fa-file-text&quot;]</value>
      <webElementGuid>01f50785-486b-43b6-87d0-70f46729ae0f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesForETbl_DXDataRow1']/td[7]/span/i</value>
      <webElementGuid>6072a14d-04b9-4d38-9a48-cc2cb0eddc88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[7]/span/i</value>
      <webElementGuid>13dcd5a0-3893-4ea4-ac15-f115498524d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
