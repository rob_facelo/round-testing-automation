<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jan 20 2023 1207PM</name>
   <tag></tag>
   <elementGuidId>0e3af774-4bac-4aff-8dfb-8c3f5513dfe7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2 > td.dxgv > div.esigpopupcolumnholder > span.showpanelclassholder</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2']/td[7]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>48fd62b7-81bb-41fb-a2af-22943b64a7bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>showpanelclassholder</value>
      <webElementGuid>2f8234a0-d692-423f-ae1c-b17147e07c00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jan 20 2023 12:07PM</value>
      <webElementGuid>3eacf256-9e55-42d8-a3b4-674f40909c9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/span[@class=&quot;showpanelclassholder&quot;]</value>
      <webElementGuid>4b035786-9533-46bf-a8b3-21ebddc47bbe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2']/td[7]/div/span</value>
      <webElementGuid>b3e305ce-2cc4-4f76-ad61-a4204b39c12d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Strange, Stephen'])[5]/following::span[1]</value>
      <webElementGuid>1b301d67-7c9f-4d9f-a1e4-29b5ee7e5bdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Resumption of Care'])[1]/preceding::span[1]</value>
      <webElementGuid>0bb17461-bc7d-45df-a357-4cb05958b0e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unsent'])[3]/preceding::span[1]</value>
      <webElementGuid>752b6b99-93eb-4433-9930-ee3ed43e4a86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jan 20 2023 12:07PM']/parent::*</value>
      <webElementGuid>16522bfe-392c-456e-844f-0fe055438678</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[7]/div/span</value>
      <webElementGuid>2cf2638a-e9d2-4905-b5b1-32c95fb96b1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Jan 20 2023 12:07PM' or . = 'Jan 20 2023 12:07PM')]</value>
      <webElementGuid>6c522ddf-da33-4331-af15-e4f42025083c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
