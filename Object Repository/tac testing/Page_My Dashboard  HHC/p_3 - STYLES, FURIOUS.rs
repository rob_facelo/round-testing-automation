<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_3 - STYLES, FURIOUS</name>
   <tag></tag>
   <elementGuidId>82a0a912-3fdd-46dc-bbab-797128faae38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.text-primary.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchresultslist']/ul/li/a/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>dd30e576-58be-47be-a869-0e28c8eaeed4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-primary ng-binding</value>
      <webElementGuid>fb861bdb-7290-403d-895c-e3774425424a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>3 - STYLES, FURIOUS</value>
      <webElementGuid>a92736f8-b081-4339-9f0c-96a0848c6d49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchresultslist&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-scope&quot;]/p[@class=&quot;text-primary ng-binding&quot;]</value>
      <webElementGuid>bcc675f7-a7df-4e4d-be5c-2d0614d51699</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/ul/li/a/p</value>
      <webElementGuid>f3b5cde6-d5c5-4b5a-a064-5d4268defbf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chat Support'])[1]/following::p[1]</value>
      <webElementGuid>8b287b06-af80-48e8-a6cf-be0871572517</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::p[4]</value>
      <webElementGuid>17d7e250-7983-4ba9-a94f-073bed13fbac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Admin'])[1]/preceding::p[4]</value>
      <webElementGuid>3ef293e1-44a7-47f8-a8ba-72229bc29771</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='3 - STYLES, FURIOUS']/parent::*</value>
      <webElementGuid>1e01a95b-562e-4e96-ad9a-cb38f84f5f92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/p</value>
      <webElementGuid>9255382a-7d11-4a09-ba9b-8f51cfc70265</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '3 - STYLES, FURIOUS' or . = '3 - STYLES, FURIOUS')]</value>
      <webElementGuid>db04ad78-6f8b-485f-93b2-38e1a10a1c51</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
