<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_---                                (_ce7194</name>
   <tag></tag>
   <elementGuidId>566f053f-f963-41ca-a09d-3edc526ca8ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ddlChangeLocation</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlChangeLocation']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>26a881db-836d-46c1-bdb9-d556e95600a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>7</value>
      <webElementGuid>538d59ac-c4e4-4c44-8bab-cdd4a8c950fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>3dac2748-5532-4a1e-90d5-1cb47ff2d6de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlChangeLocation</value>
      <webElementGuid>f2711071-b4d8-40a1-89f7-0adc792b4785</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>ServiceLocation</value>
      <webElementGuid>61fdaa67-7731-47d4-a5b8-f589e2b99197</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>isDisabled || Pagefields.CalendarChangeCareStaff.AccessType == 1</value>
      <webElementGuid>0b4dc1cf-1ca5-423e-8efd-04be265be08a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            </value>
      <webElementGuid>834cde31-b688-4b9a-968c-0c05da14fbf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlChangeLocation&quot;)</value>
      <webElementGuid>1ffd36ed-0a83-445c-83b8-8e878b40db37</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlChangeLocation']</value>
      <webElementGuid>2ee589f6-0a86-490c-91c7-d430f035ebeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Location'])[3]/following::select[1]</value>
      <webElementGuid>acde63f4-f83d-4ddb-ae32-f2f345242107</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Module:'])[1]/following::select[1]</value>
      <webElementGuid>900b7103-e31b-4082-af57-e1cff03ba9ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Military time format)'])[1]/preceding::select[1]</value>
      <webElementGuid>c09e8fcb-efbe-442e-b9a1-3cb07f68943d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div[2]/select</value>
      <webElementGuid>0574b7f8-afb6-4c21-91fa-87b0316d9eba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlChangeLocation' and (text() = '
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            ' or . = '
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            ')]</value>
      <webElementGuid>159f9c98-8e0c-44a3-807c-872fc2d822a0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
