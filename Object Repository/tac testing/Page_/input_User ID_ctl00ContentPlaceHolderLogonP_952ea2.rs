<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_User ID_ctl00ContentPlaceHolderLogonP_952ea2</name>
   <tag></tag>
   <elementGuidId>bcbd14e5-baed-4268-9fc3-e974dc67ad85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolderLogonPage_LoginControl_txtUserId</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtUserId']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f08b3e01-5449-4505-92c3-d847ad6fbf0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolderLogonPage$LoginControl$txtUserId</value>
      <webElementGuid>3c2ceca1-ce1e-4699-9cd2-c95229e71467</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>7d45010c-5bdf-4b51-942c-93be5bb4e6eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolderLogonPage_LoginControl_txtUserId</value>
      <webElementGuid>07c56d9b-6796-4f1a-83dc-91d568c3fcd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>beaf24c4-d190-4b6c-a02a-d10124c32658</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolderLogonPage_LoginControl_txtUserId&quot;)</value>
      <webElementGuid>ec52cfd4-ae6b-4a99-b029-bc59a23fb599</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtUserId']</value>
      <webElementGuid>87d73cf0-d1f0-4c95-98d0-cc884d4acc6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]/fieldset[2]/input</value>
      <webElementGuid>3a77cef7-eb6d-40a3-bd4a-d5f113f44452</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]/input</value>
      <webElementGuid>216104dd-9e36-463d-acdc-43fc222633a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$ContentPlaceHolderLogonPage$LoginControl$txtUserId' and @type = 'text' and @id = 'ContentPlaceHolderLogonPage_LoginControl_txtUserId']</value>
      <webElementGuid>43971212-8085-4e94-b173-4ce2f48d70fe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
