<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Select TAC Item Skilled Nursing - ALL</name>
   <tag></tag>
   <elementGuidId>d3d643de-4fe4-4878-87d2-d135719b3821</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ddlTar</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlTar']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>53525cc5-e1ac-4251-96c5-559f4e792afb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>4b6ca27d-a84d-4740-9231-cee7ab334e75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlTar</value>
      <webElementGuid>d5590f7b-b9b6-408e-b4ef-619c6f52c8af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>tar_item_id</value>
      <webElementGuid>2373d35b-53c9-444b-b260-9d9b7aa1b26d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-options</name>
      <type>Main</type>
      <value>tar.tar_item_id as (tar.caregivertype_code + &quot; - &quot; + tar.visit_desc) for tar in TarList</value>
      <webElementGuid>5789d2f7-6a26-4bdb-894f-212fcc81f9b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>ddlTarItemOnChange()</value>
      <webElementGuid>a013c2b7-72ba-41e1-9fe9-6857d663ab2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>isDisabled || !RequireTar</value>
      <webElementGuid>cfdb7411-3887-4a80-82d4-c608fa5f0a4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select TAC Item Skilled Nursing - ALL</value>
      <webElementGuid>b2f52ee0-71c2-4858-9ba9-85138758ec48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlTar&quot;)</value>
      <webElementGuid>ed3ef718-5ec4-4b4c-93eb-3979a443b93f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlTar']</value>
      <webElementGuid>e913ca4a-c0f5-44db-aae4-f71d17426332</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TAC Items'])[1]/following::select[1]</value>
      <webElementGuid>9c117bae-02f8-49be-a470-173d40f0bb74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TAC'])[1]/following::select[2]</value>
      <webElementGuid>bf5debc5-ac5a-4003-8e7d-901b6bf82151</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TAC Details'])[1]/preceding::select[1]</value>
      <webElementGuid>f09d52bb-e5a5-4d43-805e-76242e562fb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unit: -'])[1]/preceding::select[1]</value>
      <webElementGuid>36fe5d61-d6d1-49ae-8ebc-3b2e4b43558b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/span/select</value>
      <webElementGuid>713f6605-a625-44f5-846a-53c5a4d1a745</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlTar' and (text() = 'Select TAC Item Skilled Nursing - ALL' or . = 'Select TAC Item Skilled Nursing - ALL')]</value>
      <webElementGuid>4140d909-7516-487f-b448-f505efd6c210</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
