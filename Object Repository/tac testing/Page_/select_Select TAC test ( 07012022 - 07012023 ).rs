<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Select TAC test ( 07012022 - 07012023 )</name>
   <tag></tag>
   <elementGuidId>38a4e7a7-87c9-490c-bd8c-acf7cd091b07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ddlTac</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlTac']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>3493ff4f-8600-40b1-bf73-c471e7db539f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>11d7ad49-158d-4b5f-9e35-4c286bfd57c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlTac</value>
      <webElementGuid>7d6f8c34-5538-44c4-a11b-470bcd801912</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>tar_id</value>
      <webElementGuid>6f24b668-e7cf-43ea-b791-f304127f114e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-options</name>
      <type>Main</type>
      <value>tac.tar_id as (tac.tar + &quot; ( &quot; + tac.dateavailability + &quot; )&quot;) for tac in TacList</value>
      <webElementGuid>64336190-c595-4545-a230-77c60cca40f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>ddlTacOnChange()</value>
      <webElementGuid>92ed32e1-e47e-4828-8705-9b6080198f4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>isDisabled || !RequireTar</value>
      <webElementGuid>2c1e817f-53b2-45d4-bcb6-6144ecc1c3b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Select TAC test ( 07/01/2022 - 07/01/2023 )</value>
      <webElementGuid>2557acac-a160-4e91-bc7f-ee24483b3f99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlTac&quot;)</value>
      <webElementGuid>f37a75ff-e29d-495a-acaf-432e896faa00</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlTac']</value>
      <webElementGuid>a6cd31f0-d715-407c-b7c1-2fcd5579c1b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TAC'])[1]/following::select[1]</value>
      <webElementGuid>5198c45e-007c-4552-9a23-1c3d37f845da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Required'])[1]/following::select[1]</value>
      <webElementGuid>0ad0781e-9bcb-4c9b-ac9c-be5844039812</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TAC Items'])[1]/preceding::select[1]</value>
      <webElementGuid>df69fdb6-37c9-41c7-b3b8-6663f0f589d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TAC Details'])[1]/preceding::select[2]</value>
      <webElementGuid>15768475-bd01-4163-b431-9bde9ae3b915</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/select</value>
      <webElementGuid>de0caeb3-704e-439b-8b78-23a9ab427f24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlTac' and (text() = ' Select TAC test ( 07/01/2022 - 07/01/2023 )' or . = ' Select TAC test ( 07/01/2022 - 07/01/2023 )')]</value>
      <webElementGuid>b3df86c5-0846-40ae-a02a-72c680590330</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
