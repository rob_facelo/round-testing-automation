<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Agency ID                              _bec41d</name>
   <tag></tag>
   <elementGuidId>61caada4-6e2d-4262-ac7a-5d4c6bfcc995</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.login-form-container</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='frmLogin']/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e8ef7527-4439-48ac-acdb-aead3f9ec839</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-form-container</value>
      <webElementGuid>6e0f6a7b-0aec-4608-bc95-518d12b7971a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            Agency ID
            
        
        
            User ID
            
        
        
            Password
            
        
        
        
        
            
            Forgot Password?
        

        
        
            
                
                    
                
            
            
        
   </value>
      <webElementGuid>6235e2fc-6a28-469d-96f6-9a86921d920b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;frmLogin&quot;)/div[@class=&quot;login-form-container&quot;]</value>
      <webElementGuid>095fb7fa-7d6c-469c-8605-598e558f1869</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]</value>
      <webElementGuid>a4e4420e-98e8-4426-bbc4-6273d272af16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HEALTH SESSION CENTRE'])[1]/following::div[4]</value>
      <webElementGuid>7dd4a731-46f5-4b87-8b68-05855f22a478</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login to'])[1]/following::div[4]</value>
      <webElementGuid>13cea6eb-e991-4371-a06d-74e00eacb2cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[3]</value>
      <webElementGuid>b8763b74-14ba-47c8-b90f-4218c8f166cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
            Agency ID
            
        
        
            User ID
            
        
        
            Password
            
        
        
        
        
            
            Forgot Password?
        

        
        
            
                
                    
                
            
            
        
   ' or . = '
        
            Agency ID
            
        
        
            User ID
            
        
        
            Password
            
        
        
        
        
            
            Forgot Password?
        

        
        
            
                
                    
                
            
            
        
   ')]</value>
      <webElementGuid>667d97f8-9666-4057-bf64-b5eeb72ff6c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
