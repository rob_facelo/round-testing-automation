<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Batch</name>
   <tag></tag>
   <elementGuidId>8adcdba9-69c8-4c9e-98c7-bb6d20bd182b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchClaimsControl_btnBatch_CD > span.dx-vam</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_BatchClaimsControl_btnBatch_CD']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1659c718-beb2-4c85-aeda-2949462b9b83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam</value>
      <webElementGuid>120a5a9c-55c9-4efa-a101-f253e9964367</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Batch</value>
      <webElementGuid>ddb67846-633f-42ff-82e0-7bd16d47859f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchClaimsControl_btnBatch_CD&quot;)/span[@class=&quot;dx-vam&quot;]</value>
      <webElementGuid>849b9562-5f86-4de6-8105-54e022793b4f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_BatchClaimsControl_btnBatch_CD']/span</value>
      <webElementGuid>bf34d196-64f8-4e12-a930-b8f2d996b370</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save time now! Use the EDI Validator and detect early errors in the Claim to avoid rejections.'])[1]/following::span[1]</value>
      <webElementGuid>a7bc8f36-3bbf-4b42-802b-7332e651ce26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Use DSLC Validator:'])[1]/following::span[2]</value>
      <webElementGuid>0cc46947-c304-4f90-9687-b5e53efb4195</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[3]/preceding::span[2]</value>
      <webElementGuid>70b09380-e814-49a6-bace-38f2a23977d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ignore and proceed'])[1]/preceding::span[3]</value>
      <webElementGuid>4a6ac033-0f22-41aa-808c-669ebe187efc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Batch']/parent::*</value>
      <webElementGuid>239ab816-ea90-40e8-868d-6a946b973433</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[3]/div/div/span</value>
      <webElementGuid>79488e7d-eaa6-48e3-97a5-6159bbded4e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Batch' or . = 'Batch')]</value>
      <webElementGuid>ec4e764f-42fb-49d6-bb00-2daac312846e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
