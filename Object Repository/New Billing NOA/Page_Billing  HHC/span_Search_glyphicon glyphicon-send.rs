<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Search_glyphicon glyphicon-send</name>
   <tag></tag>
   <elementGuidId>ae82d1ab-0028-4336-a533-7e4afa6cad9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-send</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_DraftClaimsControl_btnGenerate_CD']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>28127018-3925-4164-b699-309d1eace7c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-send</value>
      <webElementGuid>dbbf6e57-6782-4a2d-9617-815548810c46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_DraftClaimsControl_btnGenerate_CD&quot;)/span[@class=&quot;glyphicon glyphicon-send&quot;]</value>
      <webElementGuid>13e93ba8-9540-417d-9308-23d0a4fb7b38</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_DraftClaimsControl_btnGenerate_CD']/span</value>
      <webElementGuid>e508ea7e-8d6a-470c-8ce8-894b4ac0e501</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/span</value>
      <webElementGuid>3e144599-ee4e-49ae-944f-59504ed232df</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
