<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Loading_ctl00ContentPlaceHolderDraftC_b4ae12</name>
   <tag></tag>
   <elementGuidId>0b05f409-f3a6-41c2-9641-25ff619313f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_DraftClaimsControl_dteFromDate_I</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder_DraftClaimsControl_dteFromDate_I']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9d62b4b4-583a-477d-b225-3eda4a514e0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeEditArea dxeEditAreaSys dxh0</value>
      <webElementGuid>3449cffd-bde1-4b53-a0ee-c05e806574a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_DraftClaimsControl_dteFromDate_I</value>
      <webElementGuid>40259e26-33b1-4e00-8b0f-4f2a4c1303ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate</value>
      <webElementGuid>a831094e-f1db-455f-91c9-b556fcebef13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onfocus</name>
      <type>Main</type>
      <value>ASPx.EGotFocus('ContentPlaceHolder_DraftClaimsControl_dteFromDate')</value>
      <webElementGuid>02d6ab4a-86fe-4b4c-a878-66459b44d20b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>ASPx.ELostFocus('ContentPlaceHolder_DraftClaimsControl_dteFromDate')</value>
      <webElementGuid>db8377ab-79b5-4476-8fef-4dcf5fd5a15c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ASPx.ETextChanged('ContentPlaceHolder_DraftClaimsControl_dteFromDate')</value>
      <webElementGuid>ba1a0547-89f7-4d3e-9577-28776d567159</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>12/30/2022</value>
      <webElementGuid>24d732f8-8d24-4a3e-8efe-27fd692e9d8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>99ead1c6-9e0d-4a4d-9eb1-4f1ddeb68ef9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>ac09b21f-ab79-4d9c-a765-26cb1731cc70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_I&quot;)</value>
      <webElementGuid>e3226372-0bb8-4f00-9e72-529189d1737a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder_DraftClaimsControl_dteFromDate_I']</value>
      <webElementGuid>fc5fbedf-6b59-4037-a1f3-24cf0dbcce91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_DraftClaimsControl_dteFromDate']/tbody/tr/td/input</value>
      <webElementGuid>dd7e0653-7f79-4f52-8aaf-465b3f3269ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/table[2]/tbody/tr/td/input</value>
      <webElementGuid>a5a12678-1c10-4043-8891-ae27c9f254eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder_DraftClaimsControl_dteFromDate_I' and @name = 'ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate' and @type = 'text']</value>
      <webElementGuid>a8f309b3-bdd3-48ba-a4fa-28683ab7ee8a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
