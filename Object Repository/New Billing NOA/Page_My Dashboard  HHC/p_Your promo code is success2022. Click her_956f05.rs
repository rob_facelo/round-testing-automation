<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Your promo code is success2022. Click her_956f05</name>
   <tag></tag>
   <elementGuidId>815629b2-a1d9-4f16-a8e4-eb5d2f9fbfcc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.item.homecontentitemholder.active > div.carousel-item-body > div > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content-carousel-popup']/div/div[6]/div/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>30f70f4f-2378-4b25-b641-09221146aa28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						Your promo code is: success2022. Click here to get more info and register for the NGS Update for 2022: Home Health and Hospice Seminar. Remember to apply the promo code when you're registering so you receive $50 off each ticket you purchase. We thank you for allowing us to be your software solution provider.
					</value>
      <webElementGuid>b3e79c89-ab31-4bce-b428-f4c24bce0967</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content-carousel-popup&quot;)/div[@class=&quot;carousel-inner&quot;]/div[@class=&quot;item homecontentitemholder active&quot;]/div[@class=&quot;carousel-item-body&quot;]/div[1]/p[1]</value>
      <webElementGuid>5b30d64c-13de-42e7-a485-ee5b15615a06</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content-carousel-popup']/div/div[6]/div/div/p</value>
      <webElementGuid>faa40202-c66b-4b28-9b90-47704da459e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$1000'])[2]/following::p[2]</value>
      <webElementGuid>f38baa00-f161-4c9e-87b3-7831afbf696e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$1000'])[1]/following::p[2]</value>
      <webElementGuid>a72b45cc-2945-46c0-8df2-fc573b6dca47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Referral Program'])[1]/preceding::p[1]</value>
      <webElementGuid>ab29516a-9507-49a9-909b-da79f7434c95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Don', &quot;'&quot;, 't show this content again.')])[1]/preceding::p[1]</value>
      <webElementGuid>dfec6778-b747-4acf-8ce4-87fd0fd5b5bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your promo code is: success2022.']/parent::*</value>
      <webElementGuid>cf7a3c84-a919-48a7-97db-a2e9c00748db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/p</value>
      <webElementGuid>809a29d0-880f-4040-b68a-7320d0a2c57b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = concat(&quot;
						Your promo code is: success2022. Click here to get more info and register for the NGS Update for 2022: Home Health and Hospice Seminar. Remember to apply the promo code when you&quot; , &quot;'&quot; , &quot;re registering so you receive $50 off each ticket you purchase. We thank you for allowing us to be your software solution provider.
					&quot;) or . = concat(&quot;
						Your promo code is: success2022. Click here to get more info and register for the NGS Update for 2022: Home Health and Hospice Seminar. Remember to apply the promo code when you&quot; , &quot;'&quot; , &quot;re registering so you receive $50 off each ticket you purchase. We thank you for allowing us to be your software solution provider.
					&quot;))]</value>
      <webElementGuid>fce9a4eb-b00b-48d6-9829-9ab798ee8dcb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
