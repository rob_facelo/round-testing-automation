<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Please Enter Date_inputDateModalDate</name>
   <tag></tag>
   <elementGuidId>2aeedd14-7219-4b62-b963-8652e6a57a82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='inputDateModalDate']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#inputDateModalDate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a0de08ac-96fb-4dc7-ab47-34b297445c39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>inputDateModalDate</value>
      <webElementGuid>f5241992-0033-4379-a062-36f4fde7b92f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>5ad9691b-6655-43f3-85d2-a6b3931a988c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>010892ab-a989-4542-a241-fb684b689f61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>date</value>
      <webElementGuid>7ad423c3-7ec8-4628-8a21-6b3d1def8b7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,false,'1')</value>
      <webElementGuid>d3cab1a4-7cdf-4ea5-a9e0-fb916c0dc59f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>a9824aab-fca8-4a58-a437-48e6f8fd7d20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inputDateModalDate&quot;)</value>
      <webElementGuid>9338ae38-90b6-4eeb-bb23-7274e5449125</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='inputDateModalDate']</value>
      <webElementGuid>8dbc8a39-b13b-454e-bbaa-b6474635b0ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/input</value>
      <webElementGuid>10005676-6ba5-467d-bcbe-2e7afd280d0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'inputDateModalDate' and @type = 'text']</value>
      <webElementGuid>2bb2817a-3796-4d16-a04b-7cb802b24eca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
