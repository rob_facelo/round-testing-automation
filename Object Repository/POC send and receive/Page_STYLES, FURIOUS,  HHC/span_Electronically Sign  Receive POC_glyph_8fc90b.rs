<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Electronically Sign  Receive POC_glyph_8fc90b</name>
   <tag></tag>
   <elementGuidId>51eebabc-77d0-491a-8884-8fa9ca6e747a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='eSignRecPOC']/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;eSignRecPOC&quot;)/span[@class=&quot;glyphicon glyphicon-pencil&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#eSignRecPOC > span.glyphicon.glyphicon-pencil</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8905b3e1-df7a-493f-9bee-ca3cb80f4296</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-pencil</value>
      <webElementGuid>7bcd8a29-9f8a-4998-9a06-3a70ecf1b78c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;eSignRecPOC&quot;)/span[@class=&quot;glyphicon glyphicon-pencil&quot;]</value>
      <webElementGuid>308ad3f1-31a7-4395-b1a5-129ab62cfe55</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='eSignRecPOC']/span</value>
      <webElementGuid>1f4158e6-d369-42de-8b92-235e3f366265</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[8]/span</value>
      <webElementGuid>c7b879f0-1a6b-42f9-ba72-e18729e32cc3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
