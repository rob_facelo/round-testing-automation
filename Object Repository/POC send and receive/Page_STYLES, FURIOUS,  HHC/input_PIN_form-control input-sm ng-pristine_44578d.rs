<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_PIN_form-control input-sm ng-pristine_44578d</name>
   <tag></tag>
   <elementGuidId>2236b094-b752-43aa-ba27-f25d5622f436</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='password'])[10]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.form-control.input-sm.ng-pristine.ng-untouched.ng-valid.ng-animate.ng-dirty-add.ng-valid-parse-add.ng-pristine-remove</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f8dce167-68af-4262-9035-9e29d9d3d197</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>9cc0bbbc-7e80-4a4a-85c8-f92a512ce9ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid ng-animate ng-dirty-add ng-valid-parse-add ng-pristine-remove</value>
      <webElementGuid>4026ce83-1cee-4e4d-b80f-650219a32975</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>pin</value>
      <webElementGuid>d1f0eb2c-dc13-458e-a62f-3728bea47380</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>isForConfirmation</value>
      <webElementGuid>6f8f915f-546d-4f8c-8ce3-9ce433730506</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ng-animate</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>ec655d0b-749c-42a0-b781-b6a8ad1953d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-106 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-md&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid ng-animate ng-dirty-add ng-valid-parse-add ng-pristine-remove&quot;]</value>
      <webElementGuid>a775d954-83cb-44a3-a5ca-2fd441c015e2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='password'])[10]</value>
      <webElementGuid>c0b64f6e-2f31-47fd-b9f5-5b2f9658ad19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/input</value>
      <webElementGuid>538843d4-0322-4004-a3d8-0823d0d4ca3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password']</value>
      <webElementGuid>ca4c871b-0ae1-4306-9a35-231b6365dc5b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
