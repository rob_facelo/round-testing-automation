<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Electronically Sign  Receive POC</name>
   <tag></tag>
   <elementGuidId>ba0d3259-b1f8-47d3-b64a-1631ebe0ca58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='eSignRecPOC']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'pocmaincontrol.ESignOrReviewPOC()' and @id = 'eSignRecPOC']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#eSignRecPOC</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8ce13e6c-f591-437e-ae34-8bee1236fa08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9ad6b822-4900-4352-98c6-86472d781534</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>pocmaincontrol.ESignOrReviewPOC()</value>
      <webElementGuid>978758ca-3805-45ab-b6f6-794b21de0077</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>eSignRecPOC</value>
      <webElementGuid>8652473a-2c28-4330-a815-6a3a8ddb73b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default toolbar-btn-default</value>
      <webElementGuid>891165eb-3404-48f4-982e-e0c4124443e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                Electronically Sign / Receive POC
                            </value>
      <webElementGuid>daab5c01-c607-4c13-9624-8938003c25cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;eSignRecPOC&quot;)</value>
      <webElementGuid>f7cdcc16-f02e-4152-ac38-9fec46bfcad4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='eSignRecPOC']</value>
      <webElementGuid>a9a78066-b1c0-4d1b-a76d-e1440831cb88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//fieldset[@id='pocButtons']/div/button[8]</value>
      <webElementGuid>0e50094f-b7ed-4df1-ba29-62df959168fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Electronically Sign / Receive POC']/parent::*</value>
      <webElementGuid>242d40d8-01d4-4041-85a6-2f5788c952dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[8]</value>
      <webElementGuid>4cd27cbb-437e-41b5-8979-01cb5dffaa4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'eSignRecPOC' and (text() = '
                                
                                Electronically Sign / Receive POC
                            ' or . = '
                                
                                Electronically Sign / Receive POC
                            ')]</value>
      <webElementGuid>71001eac-719a-4c09-9039-278a2746232f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
