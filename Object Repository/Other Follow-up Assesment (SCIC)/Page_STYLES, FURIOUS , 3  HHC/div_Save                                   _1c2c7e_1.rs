<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Save                                   _1c2c7e_1</name>
   <tag></tag>
   <elementGuidId>56a68ae1-36a5-42f3-983a-77c6a4b18a0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > div.row.ng-scope</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>63a84d4f-2aaf-4384-bd5b-0ce16469f127</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row ng-scope</value>
      <webElementGuid>b96efb2c-1611-46d9-b37e-4ee799742b7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-controller</name>
      <type>Main</type>
      <value>OasisController as oasis</value>
      <webElementGuid>68dd3445-8a7a-4de7-bb06-c52517867208</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
                PPS Calculator
            
            
                PDGM Calculator
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
            
                
                FRA
            
            
                
                HRA
            
            
                
                Upload File
            
            
            
            
                
                Fax Oasis
            
        
    

    
        Other follow-up assessment (07/01/2022 - 08/29/2022) (Oasis D1)
    
    
        3 - STYLES, FURIOUS 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            
                
                    
                    
                        
                            PATIENT TRACKING SHEET
                        
                            CLINICAL RECORD ITEMS
                        
                            PATIENT HISTORY AND DIAGNOSES
                        
                            SENSORY STATUS
                        
                            INTEGUMENTARY STATUS
                        
                            RESPIRATORY STATUS
                        
                            ELIMINATION STATUS
                        
                            ADL / IADLs
                        
                            MEDICATIONS
                        
                            THERAPY NEED AND PLAN OF CARE
                        
                    
                
            
            
                
                    

                        
                        
                            
    
        
            PATIENT TRACKING SHEET
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

    
        
            
            
                
                
                    LEGAL REPRESENTATIVE
                    
                        
                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Contact No.:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                            
                            
                                
                                     DPOA           
                                     Conservator
                                
                            
                        

                    
                
            

            
                
                
                    PATIENT CONTACT PERSON
                    

                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Home Phone:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Mobile Phone:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        

                    
                
            

        
    

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        
            
                
                    (M0140) RACE/ETHNICITY (Mark all that apply.)
                
                
                    
                        
                            
                                
                                1 - American Indian or Alaska Native
                            
                        
                        
                            
                                
                                3 - Black or African-American
                            
                        
                        
                            
                                
                                5 - Native Hawaiian or Pacific Islander
                            
                        
                        
                    
                    
                        
                            
                                
                                2 - Asian
                            
                        
                        
                            
                                
                                4 - Hispanic or Latino
                            
                        
                        
                            
                                
                                6 - White
                            
                        
                    
                
            
        

        
        
            If an assessment was erroneously submitted in a masked format, that is, it was later discovered that the patient was a Medicare or Medicaid patient
            but was not originally indicated as such at M0150, then an inactivation must be submitted. Normally, the HHA will also submit a new, corrected assessment
            in this situation. For example, if the value at M0150 for a submitted and accepted assessment is not equal to 1, 2, 3, or 4, and it should have been, then
            an inactivation request should be submitted.
            
                
                    (M0150)Current Payment Sources for Home Care (Mark all that apply.)
                
                
                    
                        
                            
                                
                                0 - None; no charge for current services
                            
                        
                        
                            
                                
                                4 - Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8 - Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1 - Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5 - Workers' compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2 - Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6 - Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10 - Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7 - Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11 - Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK - Unknown
                            
                        
                    
                
            
        

    


    .panelTitle {
        text-align: center;
        font-weight: bold;
        font-size: 15px;
        padding: 10px;
    }

    div.ui-datepicker {
        font-size: 12px;
    }




    
        
            CLINICAL RECORD ITEMS
        
    
    

    
        
            
                
                


                    
                    
                        COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

                        
                        
                            
                                
                                    
                                        (M0080) Discipline of Person Completing Assessment
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                RN
                                            
                                            
                                                2
                                                PT
                                            
                                            
                                                3
                                                SLP/ST
                                            
                                            
                                                4
                                                OT
                                            
                                        
                                    
                                
                                
                                    
                                        Care Staff Name:
                                        
                                            
                                            
                                            
                                            
                                                
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    (M0090) Date Assessment Completed:
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                        EMERGENCY PREPAREDNESSPriority Code:   Patient has an Advance Directives Order:   Yes No Time In:    Time Out:     Emergency Contact Information:  Name:   Relationship:    Phone:   Email:   Address:   
                    

                    
                    
                        
                        

                        
                        
                            
                                
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                
                                                4
                                                5
                                                
                                                
                                                
                                                
                                            
                                        
                                    
                                    
                                    
                                        
                                            Follow-Up
                                        
                                        
                                            
                                                
                                                    4
                                                    Recertification (follow-up) reassessment [ Go to M0110 ]
                                                
                                                
                                                    5
                                                    Other follow-up [ Go to M0110 ]
                                                
                                            
                                        
                                    
                                    
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                        (M0110) Episode Timing: Is the Medicare home health payment episode for which this assessment will define a case mix group
                                        an “early” episode or a “later” episode in the patient’s current sequence of adjacent Medicare home health payment episodes?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                UK
                                                NA
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Early
                                            
                                            
                                                2
                                                Later
                                            
                                            
                                                UK
                                                Unknown
                                            
                                            
                                                NA
                                                Not Applicable: No Medicare case mix group to be defined by this assessment.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                
            
        
    









    
        
            PATIENT HISTORY AND DIAGNOSES
        
    
    

    
        

            
                
                


                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    (M1021/1023) Diagnoses and Symptom Control: List each diagnosis for which the patient is receiving home care in Column 1, and enter its
                                    ICD-10-CM code at the level of highest specificity in Column 2 (diagnosis codes only -no surgical or procedure codes allowed).
                                    Diagnoses are listed in the order that best reflects the seriousness of each condition and supports the disciplines and services provided.
                                    Rate the degree of symptom control for each condition in Column 2. ICD-10-CM sequencing requirements must be followed if multiple coding is indicated for any diagnoses.
                                    
                                    

                                    Code each row according to the following directions for each column.
                                
                            
                        
                        
                            
                                
                                    Column 1:
                                
                                
                                    Enter the description of the diagnosis. Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.
                                
                            
                            
                                
                                    Column 2:
                                
                                

                                    Enter the ICD-10-CM code for the condition described in Column 1 -no surgical or procedure codes allowed. Codes must be entered at the level of highest specificity and ICD-10-CM coding rules
                                    and sequencing requirements must be followed. Note that external cause codes (ICD-10-CM codes beginning with V, W, X, or Y) may not be reported in M1021 (Primary Diagnosis) but may be reported
                                    in M1023 (Secondary Diagnoses). Also note that when a Z-code is reported in Column 2, the code for the underlying condition can often be entered in Column 2, as long as it is an active on-going
                                    condition impacting home health care.

                                    
                                        Rate the degree of symptom control for the condition listed in Column 1. Do not assign a symptom control rating if the diagnosis code is a V, W, X, Y or Z-code.
                                        Choose one value that represents the degree of symptom control appropriate for each diagnosis using the following scale:
                                    
                                    
                                        0 - Asymptomatic, no treatment needed at this time
                                    
                                    
                                        1 - Symptoms well controlled with current therapy
                                    
                                    
                                        2 - Symptoms controlled with difficulty, affecting daily functioning; patient needs ongoing monitoring
                                    
                                    
                                        3 - Symptoms poorly controlled; patient needs frequent adjustment in treatment and dose monitoring
                                    
                                    
                                        4 - Symptoms poorly controlled; history of re-hospitalizations
                                    
                                    
                                        Note that the rating for symptom control in Column 2 should not be used to determine the sequencing of the diagnoses listed in Column 1. These are separate items and sequencing may not coincide.
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    (M1021) Primary Diagnosis &amp; (M1023) Other Diagnoses
                                
                                
                                    Column 1
                                    Column 2
                                
                                
                                    
                                        Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.)
                                    
                                    
                                        ICD-10-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.
                                    
                                
                                
                                    Description
                                    ICD-10-C M / Symptom Control Rating
                                
                                
                                    
                                        (M1021) Primary Diagnosis
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        V, W, X, Y codes NOT allowed
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
                
                    
                        N19       
                        UNSPECIFIED KIDNEY FAILURE
                    
                
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        (M1023) Other Diagnoses
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        

                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        All ICD-10–CM codes allowed
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
                
                    
                        G60.0     
                        HEREDITARY MOTOR AND SENSORY NEUROPATHY
                    
                
            
                
                    
                        G60.1     
                        REFSUM'S DISEASE
                    
                
            
                
                    
                        G60.2     
                        NEUROPATHY IN ASSOCIATION WITH HEREDITARY ATAXIA
                    
                
            
                
                    
                        G60.3     
                        IDIOPATHIC PROGRESSIVE NEUROPATHY
                    
                
            
                
                    
                        G60.8     
                        OTHER HEREDITARY AND IDIOPATHIC NEUROPATHIES
                    
                
            
                
                    
                        G60.9     
                        HEREDITARY AND IDIOPATHIC NEUROPATHY, UNSPECIFIED
                    
                
            
                
                    
                        G61.0     
                        GUILLAIN-BARRE SYNDROME
                    
                
            
                
                    
                        G61.1     
                        SERUM NEUROPATHY
                    
                
            
                
                    
                        G61.81    
                        CHRONIC INFLAMMATORY DEMYELINATING POLYNEURITIS
                    
                
            
                
                    
                        G61.82    
                        MULTIFOCAL MOTOR NEUROPATHY
                    
                
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                
                            
                        

                        
                        
                            

                                

                                
                                
                                    
                                        
                                            
                                                
                                                    g. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    h. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    i. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    j. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    k. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    l. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    m. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    n. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    o. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    p. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    q. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    r. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    s. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    t. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    u. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    v. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    w. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    x. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    y. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                

                            

                            
                        
                        
                        
                        
                            
                                
                                    
                                        Surgical Procedure
                                    
                                    
                                        ICD-10-CM
                                    
                                    
                                        Date
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                a.
                                            
                                            
    
        
            
        
    
        
            
            
                
            
         

    

                                        
                                    
                                    
                                        
    
        
            
        
    
        
            
            
                
            
         

    

                                    
                                    
                                        
                                            
                                            
                                        
                                    
                                    
                                         Onset
                                    
                                

                                
                                    
                                        
                                            
                                                b.
                                            
                                            
    
        
            
        
    
        
            
            
                
            
         

    

                                        
                                    
                                    
                                        
    
        
            
        
    
        
            
            
                
            
         

    

                                    
                                    
                                        
                                            
                                            
                                        
                                    
                                    
                                         Onset
                                    
                                
                            
                        
                        

                        
                    

                    
                    
                        
                            
                                
                                    
                                        Prior history of falls within 3 months
                                     
                                    Fall Definition, “An unintentional change in position resulting in coming to rest on the ground or at a lower level.”
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Environmental Hazards
                                     
                                    May include poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits.
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Poly Pharmacy (4 or more prescriptions)
                                     
                                    Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Fall Risk
                                    
                                
                                
                                    
                                        
                                            
                                            
                                                LOW
                                                HIGH
                                            
                                        
                                    
                                
                            
                        
                    

                    
                        NEURO/EMOTIONAL/BEHAVIOR STATUS No Problem Neurological Disorder(s) type:    textAreaAdjust(false,9094); History of traumatic brain injury: (date):  History of headaches:  (Date of last headache):  Type:    Aphasia:  Receptive Expressive Tremors:  At Rest With Voluntary movement Continuous Spasms Location:     History of Seizures (date)   Type:   How does patient's condition affect their functional ability and safety?    

                        
                            
                                
                                    
                                        Cognitive Functioning: Patient's current (day of assessment) level of alertness, orientation, comprehension,
                                        concentration, and immediate memory for simple commands.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.
                                            
                                            
                                                1
                                                Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.
                                            
                                            
                                                2
                                                
                                                    Requires assistance and some direction in specific situations (for example, on all tasks involving shifting
                                                    of attention) or consistently requires low stimulus environment due to distractibility.
                                                
                                            
                                            
                                                3
                                                
                                                    Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift
                                                    attention and recall directions more than half the time.
                                                
                                            
                                            
                                                4
                                                Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.
                                            
                                        
                                    
                                
                            
                        
                        
                            
                                Cognitive Functioning Narrative
                            
                            
                        
                        
                    

                    
                    

                    
                    
                        LIVING ARRANGEMENTS/SUPPORTIVE ASSISTANCEPrimary Caregiver  Patient Family Caregiver    Phone Number (if different from patient)      Relationship   List name/relationship of other caregiver(s) (other than home health staff) and the specific assistance they give with  medical cares, ADLs, and or IADLs:    textAreaAdjust(false,6544);Able to safely care for patient  	 Yes  No Comments:    textAreaAdjust(false,6548);Other agencies/co-ordination of care:    textAreaAdjust(false,6550);Patient Received Influenza vaccine (specifically this years flu season):   Yes  No date:  Patient received any vaccines (except influenza) since they were admitted for home care:   Yes  No Type/Date:     

                        
                            
                                
                                    
                                        (M1030) Therapies the patient receives at home: (Mark all that apply.)
                                    
                                    
                                
                                
                                    
                                         1 - Intravenous or infusion therapy (excludes TPN)
                                    
                                    
                                         2 - Parenteral nutrition (TPN or lipids)
                                    
                                    
                                         3 - Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)
                                    
                                    
                                         4 - None of the above
                                    
                                    
                                         = - Ignored
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1033) Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization? (Mark all that apply.)
                                    
                                    
                                
                                
                                    
                                         1 - History of falls (2 or more falls - or any fall with an injury - in the past 12 months)
                                    
                                    
                                         2 - Unintentional weight loss of a total of 10 pounds or more in the past 12 months
                                    
                                    
                                         3 - Multiple hospitalizations (2 or more) in the past 6 months
                                    
                                    
                                         4 - Multiple emergency department visits (2 or more) in the past 6 months
                                    
                                    
                                         5 - Decline in mental, emotional, or behavioral status in the past 3 months
                                    
                                    
                                         6 - Reported or observed history of difficulty complying with any medical instructions
                                        (for example, medications, diet, exercise) in the past 3 months
                                    
                                    
                                         7 - Currently taking 5 or more medications
                                    
                                    
                                         8 - Currently reports exhaustion
                                    
                                    
                                         9 - Other risk(s) not listed in 1 – 8
                                    
                                    
                                         10 - None of the above
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        
                            
                                ADVANCE DIRECTIVES
                            
                        
                        
                            
                                 Living will
                            
                            
                                 Do not resuscitate
                            
                            
                                 Organ Donor
                            
                            
                                 Education needed
                            
                            
                                 Copies on File
                            
                            
                                 Funeral arrangements made
                            
                            
                                 Others: 
                                
                                
                            
                            
                                 Power of Attorney
                            
                        
                    

                
            

        

        
            
                
                    
                        ICD-9:
                        ICD-10:
                    
                
                
                    
                        Code
                        Description
                        Code
                        Description
                    
                    
                
            
        

    


    
        
            SENSORY STATUS
        
    
    

    
        

            
                
                


                    
                    
                         

                        
                            
                                
                                    
                                        (M1200) Vision (with corrective lenses if the patient usually wears them):
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Normal vision: sees adequately in most situations; can see medication labels, newsprint.
                                            
                                            
                                                1
                                                Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm's length.
                                            
                                            
                                                2
                                                Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.
                                            
                                        
                                    
                                
                            
                        

                        EYES No Problem PERRLA Cataracts:  Right  Left Glasses Pupils Unequal Glaucoma:  Right  Left Contacts: Right  Left Blurred Vision: Right  Left Prosthesis: Right  Left Scleral Icterus / yellowing Blind: Right  Left Ptosis: Right  Left Other (specify)   textAreaAdjust(false,6695); Infections    Cataract surgery:   Right date   Left date How does the impaired vision interfere/ impact their function/ safety? (explain)    NOSE No Problem Epistaxis Congestion Loss of smell  Sinus problem Other (specify)   MOUTH No Problem Dentures:  Upper  Lower Partial Masses Tumors Toothache  Ulcerations Gingivitis Lesions Other (specify)  THROAT No Problem Dysphagia Hoarseness Lesions Sore throat Other (specify)   
                    

                    
                    
                        PAINWhich pain assessment was used?    Wong-Baker  Pain Assessment in Advanced Dementia Scale (PAINAD) Wong-Baker FACES Pain Rating Scale  From Wong D.L., Hockenberry-Eaton M., Wilson D., Wilkenstein M.L., Schwartz P.:  Wong's Essentials of Pediatric Nursing,  ed. 6, St. Louis, 2001, p. 1301. Copyrighted by Mosby, Inc.  Reprinted by permission.   Collected using:    FACES Scale   0-10 Scale (subjective reporting) No ProblemPain Intensity:   Is patient experiencing pain?   Yes No  Unable to communicate Non-verbals demonstrated:  Diaphoresis             Grimacing Moaning/Crying Guarding   Irritability Anger Tense Restlessness  Changes in vital signs Other:     Self Assessment  Result        Implications:    How does the pain interfere/impact their functional/activity level? (explain)  Not Applicable   PAIN ASSESSMENTPain Assessment Site 1 Site 2 Site 3 Location       Onset       Present level (0-10)       Worst pain gets (0-10)       Best pain gets (0-10)       Pain description (aching, radiating, throbbing, etc.)   textAreaAdjust(false,6771);  textAreaAdjust(false,6772);  PAIN FREQUENCYWhat makes pain worse?  Movement Ambulation Immobility Other:   Is there a pattern to the pain? (explain)   textAreaAdjust(false,7275);What makes pain better?  Heat/Ice Massage Repositioning Rest/Relaxation Medication Diversion Other   How often is breakthrough medication needed?  Never Less than daily 2-3 times/day More than 3 times/dayDoes the pain radiate?  Occasionally Continuously IntermittentCurrent pain control medications adequate:   Yes NoComments:     

                        
                            
                                
                                    
                                        (M1242) Frequency of Pain Interfering with patient's activity or movement:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient has no pain
                                            
                                            
                                                1
                                                Patient has pain that does not interfere with activity or movement
                                            
                                            
                                                2
                                                Less often than daily
                                            
                                            
                                                3
                                                Daily, but not constantly
                                            
                                            
                                                4
                                                All of the time
                                            
                                        
                                    
                                
                            
                        

                         
                    

                
            

        
    




    
        
            INTEGUMENTARY STATUS
        
    
    

    
        

            
                
                

                    
                    
                         Definitions:  Unhealed:  The absence of the skin's original integrity.  Non-epithelialized:  The absence of regenerated epidermis across a wound surface.  Pressure Ulcer:  A   pressure ulcer   is localized injury to the skin and/or underlying tissue, usually over a bony prominence, as a result of pressure or pressure in combination with shear and/or friction.  A number of contributing or confounding factors are also associated with pressure ulcers; the significance of these factors is yet to be elucidated.  Newly epithelialized wound bed completely covered with new epithelium no exudate no avascular tissue (eschar and/or slough) no signs or symptoms of infection Fully granulating wound bed filled with granulation tissue to the level of the surrounding skin no dead space no avascular tissue (eschar and/or slough) wound edges open Early/ partial granulation >=25% of wound bed is covered with granulation tissue &lt;25% of the wound bed is covered with avascular tissue (eschar and/or slough) no signs or symptoms of infection wound edges open 3 - Not healing wound with >=25% avascular tissue (eschar and/or slough) OR  signs/symptoms of infection OR  clean but non-granulating wound bed OR  closed/ hyperkeratotic wound edged OR  Persistent failure to improve despite appropriate comprehensive wound management This guidance applies to surgical wounds closed by either primary intention (i.e. approximated incisions) or secondary intention (i.e. open surgical wounds). PRESSURE ULCER STAGES (NATIONAL PRESSURE ULCER ADVISORY PANEL TOOL): Stage I.  A Stage I pressure ulcer presents as intact skin with non-blanchable redness of a localized area, usually over a bony prominence. Darkly pigmented skin may not have visible blanching. Its color may differ from the surrounding area.  Further description. This area may be painful, firm, soft, and warmer or cooler as compared to adjacent tissue. Stage I ulcers may be difficult to detect in individuals with dark skin tones and may indicate &quot;at risk&quot; persons (a heralding sign of risk). Stage II.  A Stage II pressure ulcer is characterized by partial-thickness loss of dermis presenting as a shallow open ulcer with a red-pink wound bed without slough. It may also present as an intact/ ruptured serum-filled blister. Further description. A Stage II ulcer also may present as a shiny or dry shallow ulcer without slough or bruising.* This stage should not be used to describe skin tears, tape burns, perineal dermatitis, maceration, or excoriation. *Bruising indicates suspected deep tissue injury. Stage III.  A Stage III pressure ulcer is characterized by full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon or muscle are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.  Further description: The depth of a stage III pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and stage III ulcers can be shallow. In contrast, areas of significant adiposity can develop extremely deep stage III pressure ulcers. Bone/tendon is not visible or directly palpable. Stage IV.  A Stage IV pressure ulcer is characterized by Full thickness tissue loss with exposed bone, tendon or muscle. Slough or eschar may be present on some parts of the wound bed. Often include undermining and tunneling. Further description:The depth of a stage IV pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and these ulcers can be shallow. Stage IV ulcers can extend into muscle and/or supporting structures (e.g., fascia, tendon or joint capsule) making osteomyelitis possible. Exposed bone/tendon is visible or directly palpable. Unstageable.  Full-thickness tissue loss in which the base of the ulcer is covered by slough (yellow, tan, gray, green or brown) and/or eschar (tan, brown or black) in the wound bed may render a wound unstageable. Further description. Until enough slough and/or eschar is removed to expose the base of the wound, the true depth (and therefore, the stage) cannot be determined. Stable (dry, adherent, intact without erythema or fluctuance) eschar on the heels serves as &quot;the body's natural (biological) cover&quot; and should not be removed. Suspected Deep Tissue Injury:  Deep tissue injury may be characterized by a purple or maroon localized area of discolored intact skin or a blood-filled  blister due to damage of underlying soft tissue from pressure and/or shear. Presentation may be preceded by tissue that is painful, firm, mushy, boggy, and warmer or cooler as compared to adjacent tissue. Further description. Deep tissue injury may be difficult to detect in individuals with dark skin tones. Evolution may include a thin blister over a dark wound bed. The wound may further evolve and become covered by thin eschar. Evolution may be rapid, exposing additional layers of tissue even with optimal treatment.  

                        
                            
                                
                                    
                                        (M1306) Does this patient have at least one Unhealed Pressure Ulcer/Injury at Stage 2 or Higher or designated as Unstageable? 
                                        (Excludes Stage 1 pressure injuries and all healed Stage 2 pressure ulcers/injuries)
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                
                                                    No
                                                    
                                                        [ Go to M1322 ]
                                                    
                                                    
                                                
                                            
                                            
                                                1
                                                Yes
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1311) Current Number of Unhealed Pressure Ulcers/Injuries at Each Stage
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Stage
                                            
                                            
                                                Enter Number
                                            
                                        
                                        
                                            
                                                A1. Stage 2:
                                                Partial thickness loss of dermis presenting as a shallow open ulcer with a red or pink wound bed, without slough. 
                                                May also present as an intact or open/ruptured blister.
                                                Number of Stage 2 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                B1. Stage 3:
                                                Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscle is not exposed. 
                                                Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.
                                                Number of Stage 3 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                C1. Stage 4:
                                                Full thickness tissue loss with exposed bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.
                                                Number of Stage 4 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                D1. Unstageable: Non-removable dressing/device:
                                                Known but not stageable due to non-removable dressing/device
                                                Number of unstageable pressure ulcers/injuries due to non-removable dressing/device
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                E1. Unstageable: Slough and/or eschar:
                                                Known but not stageable due to coverage of wound bed by slough and/or eschar
                                                Number of unstageable pressure ulcers/injuries due to coverage of wound bed by sloughand/or eschar
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                F1. Unstageable: Deep tissue injury:
                                                
                                                Number of unstageable pressure injuries presenting as deep tissue injury
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1322) Current Number of Stage 1 Pressure Injuries: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. 
                                        The area may be painful, firm, soft, warmer, or cooler as compared to adjacent tissue. 
                                        Darkly pigmented skin may not have a visible blanching; in dark skin tones only it may appear with persistent blue or purple hues.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                            
                                            
                                                1
                                            
                                            
                                                2
                                            
                                            
                                                3
                                            
                                            
                                                4 or more
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1324) Stage of Most Problematic Unhealed Pressure Ulcer/Injury that is Stageable: 
                                        (Excludes pressure ulcer/injury that cannot be staged due to a non-removable dressing/device, coverage of wound bed by slough and/or eschar, or deep tissue injury.)
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                                NA
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Stage 1
                                            
                                            
                                                2
                                                Stage 2
                                            
                                            
                                                3
                                                Stage 3
                                            
                                            
                                                4
                                                Stage 4
                                            
                                            
                                                NA
                                                Patient has no pressure ulcers/injuries or no stageable pressure ulcers/injuries
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1330) Does this patient have a Stasis Ulcer?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No [ Go to M1340 ]
                                            
                                            
                                                1
                                                Yes, patient has BOTH observable and unobservable stasis ulcers
                                            
                                            
                                                2
                                                Yes, patient has observable stasis ulcers ONLY
                                            
                                            
                                                3
                                                Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing/device)
                                                [ Go to M1340 ]
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1332) Current Number of Stasis Ulcer(s) that are Observable:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                One
                                            
                                            
                                                2
                                                Two
                                            
                                            
                                                3
                                                Three
                                            
                                            
                                                4
                                                Four or more
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1334) Status of Most Problematic Stasis Ulcer that is Observable:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Fully granulating
                                            
                                            
                                                2
                                                Early/partial granulation
                                            
                                            
                                                3
                                                Not healing
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1340) Does this patient have a Surgical Wound?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No [Go to M1400]
                                            
                                            
                                                1
                                                Yes, patient has at least one observable surgical wound
                                            
                                            
                                                2
                                                
                                                    Surgical wound known but not observable due to non-removable dressing/device
                                                    [Go to M1400 ]
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1342) Status of Most Problematic Surgical Wound that is Observable
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Newly epithelialized
                                            
                                            
                                                1
                                                Fully granulating
                                            
                                            
                                                2
                                                Early/partial granulation
                                            
                                            
                                                3
                                                Not healing
                                            
                                        
                                    
                                
                            
                        

                        WOUND CARE (Check all that apply) Wound care done during this visit:   Yes  No Location(s) wound site:    textAreaAdjust(false,15016); Soiled dressing removed by:   Patient  Caregiver (name)     RN/PT  Other:   Technique:   Sterile  Clean  Wound cleaned with (specify):     textAreaAdjust(false,15018); Wound irrigated with (specify):     textAreaAdjust(false,15020); Wound packed with (specify):     textAreaAdjust(false,15022); Wound dressing applied (specify):      textAreaAdjust(false,15024);Patient tolerated procedure well:   Yes  NoComments:    DIABETIC FOOT EXAMFrequency of diabetic foot exam    Done by:  Patient RN/PT Caregiver (name)    Other:   Exam by clinician this visit:  Yes NoIntegument findings:    textAreaAdjust(false,7821);Pedal pulses:  Present  Right LeftAbsent  Right LeftComment:    textAreaAdjust(false,7830);Loss of sense of: Warm  Right LeftCold  Right LeftComment:    textAreaAdjust(false,7865);Neuropathy:  Right LeftTingling:  Right Left   Burning:  Right LeftLeg hair: Present   Right  Left Absent   Right Left Administered by:   WOUND / LESION#1 #2 #3 #4 #5 Date Reported:            Location           Type:  (e.g. Pressure ulcer,  Venous stasis ulcer,   Malignancy,  Malignancy, Diabetic foot ulcer)   textAreaAdjust(false,13421);  textAreaAdjust(false,13422);  textAreaAdjust(false,13423);  textAreaAdjust(false,13424);  textAreaAdjust(false,13425);Size (cm) (LxWxD)           Tunneling           Undermining (cm)           Stage (pressure ulcers only)           Odor           Surrounding Skin           Edema           Stoma           Appearance of the Wound Bed           Drainage / Amount  None None None None None Small Small Small Small Small Moderate Moderate Moderate Moderate Moderate Large Large Large Large LargeColor  Clear Clear Clear Clear Clear Tan Tan Tan Tan Tan Serosanguineous Serosanguineous Serosanguineous Serosanguineous Serosanguineous Other Other Other Other Other          Consistency  Thin Thin Thin Thin Thin Thick Thick Thick Thick Thick
                    

                
            

        
    




    
        
            RESPIRATORY STATUS
        
    
    

    
        

            
                
                


                    
                    
                        SYSTEMS REVIEWHeight:    reported actualWeight:    reported actualReported Weight Changes:  Gain Loss     lb.  X     wk.  mo.  yr. VITAL SIGNSBlood Pressure: Left Right Lying Sitting Standing At rest           With Activity           Post Activity           Temperature:    Oral Axillary Rectal TympanicPulse:  Regular Irregular Apical    Brachial    Rest Activity Radial    Carotid   Respirations:     Regular Irregular Cheynes Stokes Death rattle Rest  Activity Accessory muscles used Apnea periods    sec.  ( observed reported) Smoker Non-smokerLast smoked:    CARDIOPULMONARY No ProblemDisorders of heart / respiratory system:   textAreaAdjust(false,12175);Breath Sounds: (Clear, crankles/rales, wheezes/rhonchi, diminished, absent Aterior: Right     Left:   Posterior: Right Upper     Left Upper   Right Lower     Left Lower   O2 @      LPM via    cannula   mask  trach O2 Saturation     % Trach type/size:   Who manages?  Self Caregiver/family RN Other:   Intermittent treatments (C&amp;DB, medicated inhalation treatments, etc.)  Yes NoExplain:   textAreaAdjust(false,8096); Cough: No Yes: Productive Non-productiveDescribe:   textAreaAdjust(false,8151); Dyspnea: Rest During ADL'sComments:   textAreaAdjust(false,8102);Positioning necessary for improved breathing:  No Yes, describe   textAreaAdjust(false,8106);Heart Sounds:  Regular Irregular Murmur Pacemaker:Date   Type   Date last checked  Chest pain:  Dull Ache Sharp Vise-like Radiating Anginal Postural Localized SubsternalAssociated with:  Shortness of breath Activity SweatsFrequency/duration:   How relieved:   textAreaAdjust(false,8130); Palpitations Fatigue Edema: Pedal Right Pedal Left Sacral Dependent:    Pitting +1 +2 +3 +4 Non-pittingSite:    Cramps Claudication Capillary refill less than 3 sec Capillary refill greater than 3 sec Disease Management Problems (explain)  

                        
                            
                                
                                    
                                        (M1400) When is the patient dyspneic or noticeably Short of Breath?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient is not short of breath
                                            
                                            
                                                1
                                                When walking more than 20 feet, climbing stairs
                                            
                                            
                                                2
                                                With moderate exertion (for example, while dressing, using commode or bedpan, walking distances less than 20 feet)
                                            
                                            
                                                3
                                                With minimal exertion (for example, while eating, talking, or performing other ADLs) or with agitation
                                            
                                            
                                                4
                                                At rest (during day or night)
                                            
                                        
                                    
                                
                            
                        

                         (For M1400):   Assessed ReportedENDOCRINE STATUS A1c    %   Today's visit  Patient reported   Lab slipDate: BS    mg/dL Date/Time:     FBS  Before Meal  Postprandial  Random HS   Blood sugar ranges       Patient/Family/Caregiver Report Monitored by:   Self Caregiver Family  Nurse Other   Frequency of monitoring   Competency with use of Glucometer    Disease Management Problems (explain)  
                    

                
            

        
    



    
        
            ELIMINATION STATUS
        
    
    

    
        

            
                
                


                    
                    

                    
                    
                        ELIMINATION STATUS No ProblemDisorders of Urinary system:    textAreaAdjust(false,13519);(Check all applicable items)  Urgency  Frequency  Burning  Pain  Hesitancy  Nocturia  Retention Hematuria Oliguria  Anuria  Incontinence (details if applicable)   textAreaAdjust(false,13532);  Diapers  Other    Color:   Yellow  Straw  Amber  Brown  Gray  Blood-tinged  Other:   Clarity:   Clear  Cloudy  Sediment  Mucous Odor:   Yes No  Reported ObservedUrinary Catheter:   Type    Date last changed  Foley inserted  (date)  with    French  Inflated balloon with    mL   without difficulty  Suprapubic Irrigation solution:   Type (specify):   Amount    mL  Frequency   Returns   Patient tolerated procedure well   Yes No Urostomy (describe skin around stoma):   textAreaAdjust(false,13577);Ostomy care managed by:  Self  Caregiver Family Disease Management Problems   BOWEL ELIMINATION No ProblemDisorders of GI system:    textAreaAdjust(false,12505); Flatulence Constipation Diarrhea Rectal bleeding Hemorrhoids Fecal Impaction Last BM  Bowel Sounds: active x    quadrants absent x    quadrants hypoactive x    quadrants hyperactive x    quadrants  Frequency of stools   Bowel regime/program:    textAreaAdjust(false,12497); Laxative/Enema use: Daily Weekly Monthly PRN Other:    Incontinence (details if possible)    Diapers  Other:     Ileostomy/colostomy site (describe skin around stoma):  textAreaAdjust(false,8457);Ostomy care managed by:   Self Caregiver Family Other  

                        
                            
                                
                                    
                                        (M1610) Urinary Incontinence or Urinary Catheter Presence:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No incontinence or catheter (includes anuria or ostomy for urinary drainage)
                                            
                                            
                                                1
                                                Patient is incontinent
                                            
                                            
                                                2
                                                Patient requires a urinary catheter (specifically: external, indwelling, intermittent,
                                                    suprapubic)
                                                
                                            
                                        
                                    
                                
                            
                        

                        NUTRITIONAL STATUS No Problem NAS NPO Controlled Carbohydrate  Other    Increase fluids    amt.    Restrict fluids    amt.  Appetite:  Good Fair Poor Anorexic Nausea/Vomiting:Frequency   Amount    Heartburn (food intolerance) Other    Directions: Check each box with &quot;yes&quot; to assessment, then total score to determine additional risk. YES Has an illness or condition that changed the kind and/or amount of food eaten.  2Eats fewer than 2 meals per day.  3Eats few fruits, vegetables or milk products.  2Has 3 or more drinks of beer, liquor or wine almost every day.   2Has tooth or mouth problems that make it hard to eat.  2Does not always have enough money to buy the food needed.  4Eats alone most of the time.  1Takes 3 or more different prescribed or over-the-counter drugs a day.  1Without wanting to, has lost or gained 10 pounds in the last 6 months.  2Not always physically able to shop, cook and/or feed self.  2Total:    Reprinted with permission by the Nutrition Screening Initiative, a project of the American Academy of   Family Physician, the American Dietetic Association and the National Council on the Aging Inc. and funded in part by grant from Ross Products Division, Abbott Laboratories Inc.    INTERPRETATION 0-2 Good.   As appropriate reassess and/or provide information based on situation.         3-5 Moderate risk.   Educate, refer, monitor and reevaluate based on patient situation and organization policy. 6 or more  High risk.   Coordinate with physician, dietitian, social service professional or nurse about how to  improve nutritional health. Reassess nutritional status and educate based on plan of care.    Describe at risk intervention and plan:   
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1620) Bowel Incontinence Frequency:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                NA
                                                
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Very rarely or never has bowel incontinence
                                            
                                            
                                                1
                                                Less than once weekly
                                            
                                            
                                                2
                                                One to three times weekly
                                            
                                            
                                                3
                                                Four to six times weekly
                                            
                                            
                                                4
                                                On a daily basis
                                            
                                            
                                                5
                                                More often than once daily
                                            
                                            
                                                NA
                                                Patient has ostomy for bowel elimination
                                            
                                            
                                        
                                    
                                
                            
                        

                        PSYCHOSOCIALPrimary language    Language barrier Needs interpreter    Learning barrier: Mental  Psychosocial  Physical  Functional  Unable to read/write Educational level     Spiritual/Cultural implications that impact care.Explain    textAreaAdjust(false,8891);Spiritual resource    textAreaAdjust(false,8893);Phone No.    Sleep/Rest: Adequate InadequateExplain    textAreaAdjust(false,13589); Inappropriate responses to caregivers/clinician Inappropriate follow-through in past Angry Flat affect Discouraged Withdrawn Difficulty coping Disorganized Depressed:  Recent  Long term Treatment:    textAreaAdjust(false,8906); Inability to cope with altered health status as evidenced by:  Lack of motivation  Unrealistic expectations  Inability to recognize problems Denial of problems Evidence of abuse/neglect/exploitation: Potential Actual  Verbal/Emotional Physical Financial InterventionDescribe:    textAreaAdjust(false,8923); Other (specify)   
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that
                                        (within the last 14 days): a) was related to an inpatient facility stay; or b) necessitated a change in medical or
                                        treatment regimen?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient does not have an ostomy for bowel elimination.
                                            
                                            
                                                1
                                                Patient's ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.
                                                
                                            
                                            
                                                2
                                                The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.
                                            
                                        
                                    
                                
                            
                        

                        ABDOMEN No Problem  Tenderness Distention Soft Pain Hard Ascites Abdominal girth     cm   Other:   
                    

                
            

        
    




    
        
            ADL/IADLs
        
    
    

    
        

            
                
                


                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1800) Grooming: Current ability to tend safely to personal hygiene needs (specifically: washing face and hands, hair care, shaving or make up,
                                        teeth or denture care, fingernail care).
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to groom self unaided, with or without the use of assistive devices or adapted methods.
                                            
                                            
                                                1
                                                Grooming utensils must be placed within reach before able to complete grooming activities.
                                            
                                            
                                                2
                                                Someone must assist the patient to groom self.
                                            
                                            
                                                3
                                                Patient depends entirely upon someone else for grooming needs.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        MAHC 10 - Fall Risk Assessment ToolRequired Core ElementsAssess one point for each core element &quot;yes&quot;.Information may be gathered from medical record, assessment and if applicable, the patient/caregiver. Beyond protocols listed below, scoring should be based on your clinical judgment.  Points Age 65+   Diagnosis (3 or more co-existing)   Includes only documented medical diagnosis Prior history of falls within 3 months   An unintentional change in position resulting in coming to rest on the ground or at a lower level Incontinence   Inability to make it to the bathroom or commode in timely manner Includes frequency, urgency, and/or nocturia. Visual impairment   Includes but not limited to, macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription. Impaired functional mobility   May include patients who need help with IADLS or ADLS or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices. Environmental hazards   May include but not limited to, poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits. Poly Pharmacy (4 or more prescriptions – any type)   All PRESCRIPTIONS including prescriptions for OTC meds. Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs. Pain affecting level of function   Pain often affects an individual’s desire or ability to move or pain can be a factor in depression or ompliance with safety recommendations. Cognitive impairment   Could include patients with dementia, Alzheimer’s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patients ability to adhere to the plan of care. A score of 4 or more is considered at risk for falling Total   Missouri Alliance for HOME CARE2420 Hyde Park, Suite A, Jefferson City, MO 65109-4731  /  (573) 634-7772  /  (573) 634-4374 Fax MUSCULOSKELETAL No ProblemDisorders of musculoskeletal system:   textAreaAdjust(false,12747); Fracture (location)    Swollen, painful joints (specify)    Contractures: Joint    Location   Hand Grips:   Equal Unequal   Strong Weak  specify   Dominant side:   R L Motor Changes:  Fine Gross  specify    Weakness:  UE LE  details    Atrophy        Paresthesia      Shuffling Wild-based gait Amputation: BK  R  L  AK  R  L  UE R L Specify:    textAreaAdjust(false,9292); Other (specify)  textAreaAdjust(false,9309);How does the patient's condition affect their function ability and safety?    MENTAL STATUSDescribe the mental status of the patient:  Include the patient's general appearance, behaviors, emotional responses, mental functioning and their overall social interaction. Include both the clinical objective observations and subjective descriptions during this visit.   textAreaAdjust(false,15639);Was there a sudden/acute change in the patient's mental status?  Yes  NoIf yes, did the change coincide with something else?  No Yes, explain:  textAreaAdjust(false,15647);Mental status changes reported by:   Patient  Representative Caregiver Other:     

                        
                            
                                
                                    
                                        (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts
                                        and blouses, managing zippers, buttons, and snaps
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.
                                            
                                            
                                                1
                                                Able to dress upper body without assistance if clothing is laid out or handed to the patient.
                                            
                                            
                                                2
                                                Someone must help the patient put on upper body clothing.
                                            
                                            
                                                3
                                                Patient depends entirely upon another person to dress the upper body.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to obtain, put on, and remove clothing and shoes without assistance.
                                            
                                            
                                                1
                                                Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.
                                            
                                            
                                                2
                                                Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.
                                            
                                            
                                                3
                                                Patient depends entirely upon another person to dress lower body.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to bathe self in shower or tub independently, including getting in and out of tub/shower.
                                            
                                            
                                                1
                                                With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.
                                            
                                            
                                                2
                                                
                                                    Able to bathe in shower or tub with the intermittent assistance of another person:
                                                    
                                                        (a) for intermittent supervision or encouragement or reminders, OR
                                                    
                                                    
                                                        (b) to get in and out of the shower or tub, OR
                                                    
                                                    
                                                        (c) for washing difficult to reach areas.
                                                    
                                                
                                            
                                            
                                                3
                                                Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.
                                            
                                            
                                                4
                                                Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.
                                            
                                            
                                                5
                                                
                                                    Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the
                                                    assistance or supervision of another person.
                                                
                                            
                                            
                                                6
                                                Unable to participate effectively in bathing and is bathed totally by another person.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to get to and from the toilet and transfer independently with or without a device.
                                            
                                            
                                                1
                                                When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.
                                            
                                            
                                                2
                                                Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).
                                            
                                            
                                                3
                                                Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.
                                            
                                            
                                                4
                                                Is totally dependent in toileting.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1850) Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently transfer.
                                            
                                            
                                                1
                                                Able to transfer with minimal human assistance or with use of an assistive device.
                                            
                                            
                                                2
                                                Able to bear weight and pivot during the transfer process but unable to transfer self.
                                            
                                            
                                                3
                                                Unable to transfer self and is unable to bear weight or pivot when transferred by another person.
                                            
                                            
                                                4
                                                Bedfast, unable to transfer but is able to turn and position self in bed.
                                            
                                            
                                                5
                                                Bedfast, unable to transfer and is unable to turn and position self.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                    
                                        GG0170. Mobility: Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up code the reason.
                                    
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Coding:
                                            
                                            
                                                Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                                score according to amount of assistance provided.
                                            
                                            
                                                Activities may be completed with or without assistive devices.
                                            
                                            
                                                
                                                    06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                                
                                            
                                            
                                                
                                                    05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                                
                                            
                                            
                                                
                                                    04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as
                                                    patient completes activity. Assistance may be provided throughout the activity or intermittently.
                                                
                                            
                                            
                                                
                                                    03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides
                                                    less than half the effort.
                                                
                                            
                                            
                                                
                                                    02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than
                                                    half the effort.
                                                
                                            
                                            
                                                
                                                    01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more
                                                    helpers is required for the patient to complete the activity.
                                                
                                            
                                            
                                                If activity was not attempted, code reason:
                                            
                                            
                                                
                                                    07.    Patient refused
                                                
                                            
                                            
                                                
                                                    09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                                
                                            
                                            
                                                
                                                    10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                                
                                            
                                            
                                                
                                                    88.    Not attempted due to medical conditions or safety concerns
                                                
                                            
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                4. Follow-up Performance
                                                            
                                                        
                                                        
                                                            
                                                                Enter Codes in Boxes
                                                            
                                                        
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        A. Roll left and right: The ability to roll from lying on back to left and right side, and return to lying on back on the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        B. Sit to lying: The ability to move from sitting on side of bed to lying flat on the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        C. Lying to sitting on side of bed: The ability to move from lying on the back to sitting on the side of the bed with feet flat on the floor, and with no back support
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        D. Sit to stand: The ability to come to a standing position from sitting in a chair, wheelchair, or on the side of the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        E. Chair/bed-to-chair transfer: The ability to transfer to and from a bed to a chair (or wheelchair).
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        F. Toilet transfer: The ability to get on and off a toilet or commode.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        I. Walk 10 feet: Once standing, the ability to walk at least 10 feet in a room, corridor, or similar space.
                                                        If Follow-up performance is coded 07, 09, 10 or 88 → skip to GG0170M, 1 step (curb)
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        J. Walk 50 feet with two turns: Once standing, the ability to walk 50 feet and make two turns.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        L. Walking 10 feet on uneven surfaces: The ability to walk 10 feet on uneven or sloping surfaces (indoor or outdoor), such as turf or gravel.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        M. 1 step (curb): The ability to go up and down a curb and/or up and down one step.
                                                        If Follow-up performance is coded 07, 09, 10 or 88, skip to GG0170Q, Does patient use wheelchair and/or scooter?
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        N. 4 steps: The ability to go up and down four steps with or without a rail.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    
                                                                
                                                            
                                                            
                                                                
                                                                    Q. Does patient use wheelchair/scooter?
                                                                
                                                                
                                                                    
                                                                                  0. No → Skip GG0170R
                                                                    
                                                                    
                                                                                  1. Yes → Continue to GG0170R, Wheel 50 feet with two turns.
                                                                    
                                                                
                                                            
                                                        

                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        R. Wheel 50 feet with two turns: Once seated in wheelchair/scooter, the ability to wheel at least 50 feet and make two turns.
                                                    
                                                
                                            
                                        
                                        
                                    
                                

                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (specifically: needs no human assistance or assistive device).
                                            
                                            
                                                1
                                                With the use of a one-handed device (for example, cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.
                                            
                                            
                                                2
                                                Requires use of a two-handed device (for example, walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.
                                            
                                            
                                                3
                                                Able to walk only with the supervision or assistance of another person at all times.
                                            
                                            
                                                4
                                                Chairfast, unable to ambulate but is able to wheel self independently.
                                            
                                            
                                                5
                                                Chairfast, unable to ambulate and is unable to wheel self.
                                            
                                            
                                                6
                                                Bedfast, unable to ambulate or be up in a chair.
                                            
                                        
                                    
                                
                            
                        

                         Indications for Home Health Aides:   Yes  No  RefusedReferred to HHA:   Yes No Reason for need:     
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                    
                                        GG0130. Self-Care: Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up, code the reason.
                                    
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Coding:
                                            
                                            
                                                Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                                score according to amount of assistance provided.
                                            
                                            
                                                Activities may be completed with or without assistive devices.
                                            
                                            
                                                
                                                    06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                                
                                            
                                            
                                                
                                                    05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                                
                                            
                                            
                                                
                                                    04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as patient completes activity.
                                                    Assistance may be provided throughout the activity or intermittently.
                                                
                                            
                                            
                                                
                                                    03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides less than half the effort.
                                                
                                            
                                            
                                                
                                                    02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than half the effort.
                                                
                                            
                                            
                                                
                                                    01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more helpers is required for the patient to complete the activity.
                                                
                                            
                                            
                                                If activity was not attempted, code reason:
                                            
                                            
                                                
                                                    07.    Patient refused
                                                
                                            
                                            
                                                
                                                    09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                                
                                            
                                            
                                                
                                                    10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                                
                                            
                                            
                                                
                                                    88.    Not attempted due to medical conditions or safety concerns
                                                
                                            
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                4. Follow-up Performance
                                                            
                                                        
                                                        
                                                            
                                                                
                                                                    Enter Codes in Boxes
                                                                
                                                            
                                                        
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        A. Eating: The ability to use suitable utensils to bring food and/or liquid to the mouth and swallow food and/or liquid once the meal is placed before the patient.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        B. Oral Hygiene: 
                                                        The ability to use suitable items to clean teeth. Dentures (if applicable): The ability to insert and remove dentures into and from the mouth, and manage denture soaking and rinsing with use of equipment.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        C. Toileting Hygiene: The ability to maintain perineal hygiene, adjust clothes before and after voiding or having a bowel movement. If managing an ostomy, include wiping the opening but not managing equipment.
                                                    
                                                
                                            
                                        
                                        
                                    
                                

                            
                        

                        
                    


                    
                    

                    
                    

                
            

        
    



    
        
            MEDICATIONS
        
    
    

    
        

            
                
                

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M2030) Management of Injectable Medications: Patient's current ability to prepare and take all prescribed injectable medications reliably and safely,
                                        including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                NA
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently take the correct medication(s) and proper dosage(s) at the correct times.
                                            
                                            
                                                1
                                                
                                                    Able to take injectable medication(s) at the correct times if:
                                                    
                                                        (a) individual syringes are prepared in advance by another person; OR
                                                    
                                                    
                                                        (b) another person develops a drug diary or chart.
                                                    
                                                
                                            
                                            
                                                2
                                                Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection
                                            
                                            
                                                3
                                                Unable to take injectable medication unless administered by another person.
                                            
                                            
                                                NA
                                                No injectable medications prescribed.
                                            
                                        
                                    
                                
                            
                        

                        INFUSION N/A Central line  Peripheral line  Midline catheter Type/brand   Size/gauge/length    Groshong  Non-groshong  Tunneled  Non-tunneledInsertion site    Insertion date Lumens:   Single  Double  Triple Flush solution/frequency   Patient:  Yes  No Injection cap change frequency   Dressing change frequency    Sterile  CleanPerformed by:  Self Family  Caregiver  RN  Other    Site/skin condition   External catheter length   Other:   textAreaAdjust(false,10021);PICC Specific: Circumference of arm   X-ray verification   Yes  No IVAD Port Specific: Reservoir:   Single	 DoubleHuber gauge/length   Accessed:   No  Yes, date   Epidural/Inrathecal Access: Site/skin condition:   Infusion solution (type/volume/rate)    Pump: (type, specify)    Purpose of Intravenous:    textAreaAdjust(false,10040); Medication(s) administered: (name of drug)   Dose    Route   Frequency    Duration of therapy    Medication(s) administered: (name of drug)   Dose    Route   Frequency    Duration of therapy   Administered by:   Self Family  Caregiver  Other   Purpose of Intravenous Access:  Antibiotic therapy  Pain control   Lab draws Chemotherapy		 Main venous access  Hydration		 Parental nutrition Other    Infusion care provided during visit Yes  No Interventions/Instructions/Comments    Psychotropic drug use:   No  Yes Ability to pay for medications:   Yes  No If No, was MSW referral made?   Yes  No Comment   
                    

                
            

        
    





    
        
            THERAPY NEED AND PLAN OF CARE
        
    
    

    
        

            
                
                
                   
                    
                    
                        STRENGTHS/LIMITATIONSBased upon the patient's Physical, Psychosocial, Cognitive and Mental Status: List the patient's strengths that contribute to them meeting their goal(s), both personal and the HHA measurable goals.   textAreaAdjust(false,15786);List the patient's limitations that might challenge progress toward their goal(s), both personal and the HHA measurable goal.    textAreaAdjust(false,15789);List any diagnosed permanent disability/impairment of the patient:   textAreaAdjust(false,15791);How might the patient's limitation(s) affect their safety and/or progress?    textAreaAdjust(false,15793);What is the HHA doing/implementing to mitigate the patient's limitations?    textAreaAdjust(false,15795);Has anything significant changed since the last visit?   No Yes, explain:   REFUSED CARES Patient  Representative Other:    refuse   Cares  Services since the last assessment?  No  Yes, explain  textAreaAdjust(false,15880);Are the  cares  service(s) they refused a significant part of the recommended plan of care? No Yes, explain how  RISK FACTORS/HOSPITAL ADMISSION/EMERGENCY ROOM Not ApplicableRisk factors identified and followed up on by:   Training Discussion EducationLiterature given to:   Patient  Representative Caregiver Family Other:    List identified risk factors the patient has related to an unplanned hospital admission or an emergency department visit    

                        
                            
                                
                                    
                                        (M2200) Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, 
                                        what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)?
                                        (Enter zero [“000”] if no therapy visits indicated.)
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                
                                            
                                        
                                        
                                            Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined).
                                        
                                    
                                
                                
                                    
                                         NA - Not Applicable: No case mix group defined by this assessment.
                                    
                                
                            
                        

                        PATIENT/CAREGIVER EDUCATIONPatient/caregiver/family knowledgeable and able to    verbalize and/or demonstrate independence with:  Wound care:   Yes  No  N/ACatheter care:   Yes  No  N/ADiabetic foot exam/care:   Yes  No  N/ANutritional management:   Yes  No  N/AInsulin administration:   Yes  No  N/APain management:  Yes  No  N/AGlucometer use:   Yes  No  N/AOxygen use:  Yes  No  N/AOral medication(s) administration:	  Yes  No  N/AUse of medical devices:  Yes  No  N/AInjected medication(s) administration:  Yes  No  N/ATrach care:   Yes  No  N/AInfused medication(s) administration:  Yes  No  N/AOstomy care:   Yes  No  N/AInhaled medication(s) administration:  Yes  No  N/AOther care(s):      Yes  No  N/APatient/caregiver/family need further education with:    textAreaAdjust(false,13006);Caregiver/family present at time of visit:   Yes  No Patient/caregiver/family educated this visit for:    textAreaAdjust(false,10789);Patient/caregiver appears to understand all information given:  Yes  No Does the patient/caregive/family have an action plan?   Yes  No Comments:    SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - NURSING INTERVENTIONS/INSTRUCTIONS Skilled observation &amp; assessment    Teach   Admin.   IVs  Clysis   Diabetic observation   Post-partum assessment  Prenatal Assessment  Teach   infant  child care  Foley care  Teach   ostomy  ileo. conduit care  Teach diabetic care   Wound   care  dressing  Teach   Admin. tube feedings  Observe   Teach medication  Decubitis care   Teach   Admin. care of trach  N   or  C  Venipuncture   Teach care - terminally ill   effects  side effects  Change   NG  G tube  IM injection   SQ injection  Physiology    Disease process teaching  Admin. of vitamin B2   Psych. intervention   Diet teaching   Prep.   Admin. insulin  Observe S/S infection   Safety factors   Fall Safety Teaching   Pain Management   Other:   SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - PHYSICAL THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation   Balance training/activities    Teach hip precautions   Establish upgrade home exercise program   Pulmonary Physical Training  Teach safe/effective use of adaptive/assist device (specify)   Copy given to patient  Ultrasound     Copy attached to chart  Electrotherapy   Teach safe stair climbing skills  Patient/Family education   Prosthetic training   Teach fall safety   Therapeutic exercise   TENS  Other:   Transfer training   Functional mobility training    textAreaAdjust(false,10565); Gait training   Teach bed mobility skills  SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - OCCUPATIONAL THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation    Neuro-developmental training    Therapeutic exercise to right/left hand   Establish home exercise program   Sensory treatment  to increase strength, coordination,   Copy given to patient Orthotics  Splinting sensation and proprioception   Copy attached to chart  Adaptive equipment (fabrication   Fine motor coordination   Patient/Family education  and training)   Perceptual motor training   Independent living/ADL training    Teach alternative bathing skills   Teach fall safety   Muscle re-education  (unable to use tub/shower safely)  Retraining of cognitive,  Other:   feeding and perpetual skills    SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - SPEECH THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation   Dysphagia treatments    Safe swallowing evaluation   Establish home maintenance program   Language disorders   Speech dysphagia instruction program   Copy given to patient  Aural rehabilitation   Teach/Develop communication system   Copy attached to chart  Non-oral communication  Other:   Patient/Family education   Alaryngeal speech skill    textAreaAdjust(false,10651); Voice disorders   Language processing   Speech articulation disorders   Food texture recommendations  SKILLED CARE PROVIDED THIS VISIT  SUMMARY CHECKLIST CARE PLAN:  Reviewed with patient/caregiver/family involvementMEDICATION STATUS:  Medication Regimen completed/reviewed  No change Order obtainedCheck if any of the following were identified:    Potential adverse effects/drug reactions Ineffective drug therapy Significant side effects  Significant drug interactions Duplicate drug therapy Non-compliance with drug therapyCARE COORDINATION:  Physician  SN  PT  OT  ST  MSW  Aide  Other (specify)   Was a referral made to MSW for assistance with community resources/assistance with a living will/counseling needs (depression/suicidal ideation) and/or unsafe environment?  Date   Yes  No  Refused  N/A   Comment    Verbal Order obtained:  No  Yes, specify date   AMPLIFICATION OF CARE PROVIDED / ANALYSIS OF FINDINGS  textAreaAdjust(false,10671);Patient/Caregiver Response   HOMEBOUND REASON Needs assistance for all activities  Confusion, unable to go out of home alone Residual weakness Unable to safely leave home unassisted Requires assistance to ambulate Severe SOB, SOB upon exertion Dependent upon adaptive device(s) Medical restrictions Other (specify)   SUPERVISORY VISIT:   Yes  No  Scheduled  UnscheduledSTAFF:   Present  Not Present NEXT SCHEDULED SUPERVISORY VISIT CARE PLAN UPDATED?   Yes  NoCARE PLAN FOLLOWED ?  Yes  No (explain)   textAreaAdjust(false,10698);OBSERVATION OF    textAreaAdjust(false,10700);TEACHING/TRAINING OF    textAreaAdjust(false,10702);IS PATIENT/FAMILY SATISFIED WITH CARE?   Yes  No (explain)   CARE SUMMARY Dear Doctor   This Care Summary page is your   Recertification (follow-up) Other Follow-upThank you for allowing us to care for your patient. Disciplines Involved Comments  SN  textAreaAdjust(false,14055); PT  textAreaAdjust(false,14056); OT  textAreaAdjust(false,14057); ST  textAreaAdjust(false,14058); MSW  textAreaAdjust(false,14059); Aide      textAreaAdjust(false,14060);Date of last home visit Physician notified:  Yes No POT485 attached for signature. Please sign and return. Copy of Care Summary  sent faxed Date: SUMMARY Complete this Section for Recertification (Unless Summary is written elsewhere) REASON FOR ADMISSION (describe condition)   textAreaAdjust(false,14073);SUMMARY OF CARE (Including progress toward goals to date)   textAreaAdjust(false,14074);DISCHARGE PLANNING (specify future follow-up, referrals, etc.)   DME/MEDICAL SUPPLIES DME Company:    Phone:   Oxygen Company:    Phone:   Community org./services:    Contact:   Phone:   Comments:    ASSESSMENT SUMMARY Reason for Admission:    Homebound Reason:      Any changes since last assessment:   
                    

                    
                    

                
            

        
    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                

                
                    
                        Hide Validation Results
                    
                
            
        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    
        
            
                
                    M-ITEM
                    QUESTION
                    ERROR MESSAGE
                    SEVERITY
                
            
            
                
                    
                        
                    
                    
                         () Unable to Genarate HIPPS CODE:
                    
                    
                        
                    
                    
                        
                            ERROR
                        
                        
                    
                
                    
                        M1021
                    
                    
                        a. Primary Diagnosis 
                    
                    
                        The Primary Diagnosis N19 is not billable under PDGM.
                    
                    
                        
                            ERROR
                        
                        
                    
                
            
        
    

    

    
        Oasis Audits
        
            
                
                    MOSET
                    PREVIOUS
                    CURRENT
                    STATUS
                
            
            
                
                    
                        M1020_PRIMARY_DIAG_ICD
                    
                    
                        B77.81  
                    
                    
                        N19     
                    
                    
                        
                            Changed
                        
                    
                
                    
                        M1020_PRIMARY_DIAG_SEVERITY
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1024_OTH_DIAG1_ICD
                    
                    
                        J60     
                    
                    
                        G60.0   
                    
                    
                        
                            Changed
                        
                    
                
                    
                        M1024_OTH_DIAG1_SEVERITY
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1024_OTH_DIAG2_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG2_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG3_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG3_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG4_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG4_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG5_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG5_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_A3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_C3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_D3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_E3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_F3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_A4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_C4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_D4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_E4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_F4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1030_THH
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1200_VISION
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1242_PAIN_FREQ_ACTVTY_MVMT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1306_UNHLD_STG2_PRSR_ULCR
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG2
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG2_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG3
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG3_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG4
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG4_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_DRSG
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DRSG_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_CVRG
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_CVRG_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1322_NBR_PRSULC_STG1
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1324_STG_PRBLM_ULCER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1330_STAS_ULCR_PRSNT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1332_NUM_STAS_ULCR
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1334_STUS_PRBLM_STAS_ULCR
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1340_SRGCL_WND_PRSNT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1342_STUS_PRBLM_SRGCL_WND
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1350_LESION_OPEN_WND
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1400_WHEN_DYSPNEIC
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1610_UR_INCONT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1620_BWL_INCONT
                    
                    
                        00
                    
                    
                        00
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1630_OSTOMY
                    
                    
                        00
                    
                    
                        00
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1810_CUR_DRESS_UPPER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1820_CUR_DRESS_LOWER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1830_CRNT_BATHG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1840_CUR_TOILTG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1850_CUR_TRNSFRNG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1860_CRNT_AMBLTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1860_CRNT_AMBLTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2030_CRNT_MGMT_INJCTN_MDCTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2200_THER_NEED_NUM
                    
                    
                        8
                    
                    
                        8
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2200_THER_NEED_NA
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
            
        
        
    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

</value>
      <webElementGuid>d72c2d37-9e8d-476a-869a-426458e1b53a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]</value>
      <webElementGuid>e05ab81b-6e02-4294-804c-15b7acf50ed9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]</value>
      <webElementGuid>548b7c74-8e37-4350-aa04-a1d8f174bb90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Products/Agencies'])[1]/following::div[9]</value>
      <webElementGuid>85272b32-def3-4fb5-8210-e5066c277ba5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DDE / CWF Connectivity'])[1]/following::div[15]</value>
      <webElementGuid>d4982ea7-d2c3-4f95-8204-4769d533b01a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div/div[2]</value>
      <webElementGuid>2f38a2f1-e71d-47bc-a388-84f263268ff3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
                PPS Calculator
            
            
                PDGM Calculator
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
            
                
                FRA
            
            
                
                HRA
            
            
                
                Upload File
            
            
            
            
                
                Fax Oasis
            
        
    

    
        Other follow-up assessment (07/01/2022 - 08/29/2022) (Oasis D1)
    
    
        3 - STYLES, FURIOUS 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            
                
                    
                    
                        
                            PATIENT TRACKING SHEET
                        
                            CLINICAL RECORD ITEMS
                        
                            PATIENT HISTORY AND DIAGNOSES
                        
                            SENSORY STATUS
                        
                            INTEGUMENTARY STATUS
                        
                            RESPIRATORY STATUS
                        
                            ELIMINATION STATUS
                        
                            ADL / IADLs
                        
                            MEDICATIONS
                        
                            THERAPY NEED AND PLAN OF CARE
                        
                    
                
            
            
                
                    

                        
                        
                            
    
        
            PATIENT TRACKING SHEET
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

    
        
            
            
                
                
                    LEGAL REPRESENTATIVE
                    
                        
                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Contact No.:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                            
                            
                                
                                     DPOA           
                                     Conservator
                                
                            
                        

                    
                
            

            
                
                
                    PATIENT CONTACT PERSON
                    

                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Home Phone:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Mobile Phone:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        

                    
                
            

        
    

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        
            
                
                    (M0140) RACE/ETHNICITY (Mark all that apply.)
                
                
                    
                        
                            
                                
                                1 - American Indian or Alaska Native
                            
                        
                        
                            
                                
                                3 - Black or African-American
                            
                        
                        
                            
                                
                                5 - Native Hawaiian or Pacific Islander
                            
                        
                        
                    
                    
                        
                            
                                
                                2 - Asian
                            
                        
                        
                            
                                
                                4 - Hispanic or Latino
                            
                        
                        
                            
                                
                                6 - White
                            
                        
                    
                
            
        

        
        
            If an assessment was erroneously submitted in a masked format, that is, it was later discovered that the patient was a Medicare or Medicaid patient
            but was not originally indicated as such at M0150, then an inactivation must be submitted. Normally, the HHA will also submit a new, corrected assessment
            in this situation. For example, if the value at M0150 for a submitted and accepted assessment is not equal to 1, 2, 3, or 4, and it should have been, then
            an inactivation request should be submitted.
            
                
                    (M0150)Current Payment Sources for Home Care (Mark all that apply.)
                
                
                    
                        
                            
                                
                                0 - None; no charge for current services
                            
                        
                        
                            
                                
                                4 - Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8 - Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1 - Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5 - Workers&quot; , &quot;'&quot; , &quot; compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2 - Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6 - Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10 - Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7 - Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11 - Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK - Unknown
                            
                        
                    
                
            
        

    


    .panelTitle {
        text-align: center;
        font-weight: bold;
        font-size: 15px;
        padding: 10px;
    }

    div.ui-datepicker {
        font-size: 12px;
    }




    
        
            CLINICAL RECORD ITEMS
        
    
    

    
        
            
                
                


                    
                    
                        COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

                        
                        
                            
                                
                                    
                                        (M0080) Discipline of Person Completing Assessment
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                RN
                                            
                                            
                                                2
                                                PT
                                            
                                            
                                                3
                                                SLP/ST
                                            
                                            
                                                4
                                                OT
                                            
                                        
                                    
                                
                                
                                    
                                        Care Staff Name:
                                        
                                            
                                            
                                            
                                            
                                                
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    (M0090) Date Assessment Completed:
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                        EMERGENCY PREPAREDNESSPriority Code:   Patient has an Advance Directives Order:   Yes No Time In:    Time Out:     Emergency Contact Information:  Name:   Relationship:    Phone:   Email:   Address:   
                    

                    
                    
                        
                        

                        
                        
                            
                                
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                
                                                4
                                                5
                                                
                                                
                                                
                                                
                                            
                                        
                                    
                                    
                                    
                                        
                                            Follow-Up
                                        
                                        
                                            
                                                
                                                    4
                                                    Recertification (follow-up) reassessment [ Go to M0110 ]
                                                
                                                
                                                    5
                                                    Other follow-up [ Go to M0110 ]
                                                
                                            
                                        
                                    
                                    
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                        (M0110) Episode Timing: Is the Medicare home health payment episode for which this assessment will define a case mix group
                                        an “early” episode or a “later” episode in the patient’s current sequence of adjacent Medicare home health payment episodes?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                UK
                                                NA
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Early
                                            
                                            
                                                2
                                                Later
                                            
                                            
                                                UK
                                                Unknown
                                            
                                            
                                                NA
                                                Not Applicable: No Medicare case mix group to be defined by this assessment.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                
            
        
    









    
        
            PATIENT HISTORY AND DIAGNOSES
        
    
    

    
        

            
                
                


                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    (M1021/1023) Diagnoses and Symptom Control: List each diagnosis for which the patient is receiving home care in Column 1, and enter its
                                    ICD-10-CM code at the level of highest specificity in Column 2 (diagnosis codes only -no surgical or procedure codes allowed).
                                    Diagnoses are listed in the order that best reflects the seriousness of each condition and supports the disciplines and services provided.
                                    Rate the degree of symptom control for each condition in Column 2. ICD-10-CM sequencing requirements must be followed if multiple coding is indicated for any diagnoses.
                                    
                                    

                                    Code each row according to the following directions for each column.
                                
                            
                        
                        
                            
                                
                                    Column 1:
                                
                                
                                    Enter the description of the diagnosis. Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.
                                
                            
                            
                                
                                    Column 2:
                                
                                

                                    Enter the ICD-10-CM code for the condition described in Column 1 -no surgical or procedure codes allowed. Codes must be entered at the level of highest specificity and ICD-10-CM coding rules
                                    and sequencing requirements must be followed. Note that external cause codes (ICD-10-CM codes beginning with V, W, X, or Y) may not be reported in M1021 (Primary Diagnosis) but may be reported
                                    in M1023 (Secondary Diagnoses). Also note that when a Z-code is reported in Column 2, the code for the underlying condition can often be entered in Column 2, as long as it is an active on-going
                                    condition impacting home health care.

                                    
                                        Rate the degree of symptom control for the condition listed in Column 1. Do not assign a symptom control rating if the diagnosis code is a V, W, X, Y or Z-code.
                                        Choose one value that represents the degree of symptom control appropriate for each diagnosis using the following scale:
                                    
                                    
                                        0 - Asymptomatic, no treatment needed at this time
                                    
                                    
                                        1 - Symptoms well controlled with current therapy
                                    
                                    
                                        2 - Symptoms controlled with difficulty, affecting daily functioning; patient needs ongoing monitoring
                                    
                                    
                                        3 - Symptoms poorly controlled; patient needs frequent adjustment in treatment and dose monitoring
                                    
                                    
                                        4 - Symptoms poorly controlled; history of re-hospitalizations
                                    
                                    
                                        Note that the rating for symptom control in Column 2 should not be used to determine the sequencing of the diagnoses listed in Column 1. These are separate items and sequencing may not coincide.
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    (M1021) Primary Diagnosis &amp; (M1023) Other Diagnoses
                                
                                
                                    Column 1
                                    Column 2
                                
                                
                                    
                                        Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.)
                                    
                                    
                                        ICD-10-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.
                                    
                                
                                
                                    Description
                                    ICD-10-C M / Symptom Control Rating
                                
                                
                                    
                                        (M1021) Primary Diagnosis
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        V, W, X, Y codes NOT allowed
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
                
                    
                        N19       
                        UNSPECIFIED KIDNEY FAILURE
                    
                
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        (M1023) Other Diagnoses
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        

                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        All ICD-10–CM codes allowed
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
                
                    
                        G60.0     
                        HEREDITARY MOTOR AND SENSORY NEUROPATHY
                    
                
            
                
                    
                        G60.1     
                        REFSUM&quot; , &quot;'&quot; , &quot;S DISEASE
                    
                
            
                
                    
                        G60.2     
                        NEUROPATHY IN ASSOCIATION WITH HEREDITARY ATAXIA
                    
                
            
                
                    
                        G60.3     
                        IDIOPATHIC PROGRESSIVE NEUROPATHY
                    
                
            
                
                    
                        G60.8     
                        OTHER HEREDITARY AND IDIOPATHIC NEUROPATHIES
                    
                
            
                
                    
                        G60.9     
                        HEREDITARY AND IDIOPATHIC NEUROPATHY, UNSPECIFIED
                    
                
            
                
                    
                        G61.0     
                        GUILLAIN-BARRE SYNDROME
                    
                
            
                
                    
                        G61.1     
                        SERUM NEUROPATHY
                    
                
            
                
                    
                        G61.81    
                        CHRONIC INFLAMMATORY DEMYELINATING POLYNEURITIS
                    
                
            
                
                    
                        G61.82    
                        MULTIFOCAL MOTOR NEUROPATHY
                    
                
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                
                            
                        

                        
                        
                            

                                

                                
                                
                                    
                                        
                                            
                                                
                                                    g. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    h. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    i. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    j. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    k. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    l. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    m. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    n. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    o. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    p. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    q. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    r. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    s. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    t. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    u. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    v. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    w. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    x. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    y. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                

                            

                            
                        
                        
                        
                        
                            
                                
                                    
                                        Surgical Procedure
                                    
                                    
                                        ICD-10-CM
                                    
                                    
                                        Date
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                a.
                                            
                                            
    
        
            
        
    
        
            
            
                
            
         

    

                                        
                                    
                                    
                                        
    
        
            
        
    
        
            
            
                
            
         

    

                                    
                                    
                                        
                                            
                                            
                                        
                                    
                                    
                                         Onset
                                    
                                

                                
                                    
                                        
                                            
                                                b.
                                            
                                            
    
        
            
        
    
        
            
            
                
            
         

    

                                        
                                    
                                    
                                        
    
        
            
        
    
        
            
            
                
            
         

    

                                    
                                    
                                        
                                            
                                            
                                        
                                    
                                    
                                         Onset
                                    
                                
                            
                        
                        

                        
                    

                    
                    
                        
                            
                                
                                    
                                        Prior history of falls within 3 months
                                     
                                    Fall Definition, “An unintentional change in position resulting in coming to rest on the ground or at a lower level.”
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Environmental Hazards
                                     
                                    May include poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits.
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Poly Pharmacy (4 or more prescriptions)
                                     
                                    Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Fall Risk
                                    
                                
                                
                                    
                                        
                                            
                                            
                                                LOW
                                                HIGH
                                            
                                        
                                    
                                
                            
                        
                    

                    
                        NEURO/EMOTIONAL/BEHAVIOR STATUS No Problem Neurological Disorder(s) type:    textAreaAdjust(false,9094); History of traumatic brain injury: (date):  History of headaches:  (Date of last headache):  Type:    Aphasia:  Receptive Expressive Tremors:  At Rest With Voluntary movement Continuous Spasms Location:     History of Seizures (date)   Type:   How does patient&quot; , &quot;'&quot; , &quot;s condition affect their functional ability and safety?    

                        
                            
                                
                                    
                                        Cognitive Functioning: Patient&quot; , &quot;'&quot; , &quot;s current (day of assessment) level of alertness, orientation, comprehension,
                                        concentration, and immediate memory for simple commands.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.
                                            
                                            
                                                1
                                                Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.
                                            
                                            
                                                2
                                                
                                                    Requires assistance and some direction in specific situations (for example, on all tasks involving shifting
                                                    of attention) or consistently requires low stimulus environment due to distractibility.
                                                
                                            
                                            
                                                3
                                                
                                                    Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift
                                                    attention and recall directions more than half the time.
                                                
                                            
                                            
                                                4
                                                Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.
                                            
                                        
                                    
                                
                            
                        
                        
                            
                                Cognitive Functioning Narrative
                            
                            
                        
                        
                    

                    
                    

                    
                    
                        LIVING ARRANGEMENTS/SUPPORTIVE ASSISTANCEPrimary Caregiver  Patient Family Caregiver    Phone Number (if different from patient)      Relationship   List name/relationship of other caregiver(s) (other than home health staff) and the specific assistance they give with  medical cares, ADLs, and or IADLs:    textAreaAdjust(false,6544);Able to safely care for patient  	 Yes  No Comments:    textAreaAdjust(false,6548);Other agencies/co-ordination of care:    textAreaAdjust(false,6550);Patient Received Influenza vaccine (specifically this years flu season):   Yes  No date:  Patient received any vaccines (except influenza) since they were admitted for home care:   Yes  No Type/Date:     

                        
                            
                                
                                    
                                        (M1030) Therapies the patient receives at home: (Mark all that apply.)
                                    
                                    
                                
                                
                                    
                                         1 - Intravenous or infusion therapy (excludes TPN)
                                    
                                    
                                         2 - Parenteral nutrition (TPN or lipids)
                                    
                                    
                                         3 - Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)
                                    
                                    
                                         4 - None of the above
                                    
                                    
                                         = - Ignored
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1033) Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization? (Mark all that apply.)
                                    
                                    
                                
                                
                                    
                                         1 - History of falls (2 or more falls - or any fall with an injury - in the past 12 months)
                                    
                                    
                                         2 - Unintentional weight loss of a total of 10 pounds or more in the past 12 months
                                    
                                    
                                         3 - Multiple hospitalizations (2 or more) in the past 6 months
                                    
                                    
                                         4 - Multiple emergency department visits (2 or more) in the past 6 months
                                    
                                    
                                         5 - Decline in mental, emotional, or behavioral status in the past 3 months
                                    
                                    
                                         6 - Reported or observed history of difficulty complying with any medical instructions
                                        (for example, medications, diet, exercise) in the past 3 months
                                    
                                    
                                         7 - Currently taking 5 or more medications
                                    
                                    
                                         8 - Currently reports exhaustion
                                    
                                    
                                         9 - Other risk(s) not listed in 1 – 8
                                    
                                    
                                         10 - None of the above
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        
                            
                                ADVANCE DIRECTIVES
                            
                        
                        
                            
                                 Living will
                            
                            
                                 Do not resuscitate
                            
                            
                                 Organ Donor
                            
                            
                                 Education needed
                            
                            
                                 Copies on File
                            
                            
                                 Funeral arrangements made
                            
                            
                                 Others: 
                                
                                
                            
                            
                                 Power of Attorney
                            
                        
                    

                
            

        

        
            
                
                    
                        ICD-9:
                        ICD-10:
                    
                
                
                    
                        Code
                        Description
                        Code
                        Description
                    
                    
                
            
        

    


    
        
            SENSORY STATUS
        
    
    

    
        

            
                
                


                    
                    
                         

                        
                            
                                
                                    
                                        (M1200) Vision (with corrective lenses if the patient usually wears them):
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Normal vision: sees adequately in most situations; can see medication labels, newsprint.
                                            
                                            
                                                1
                                                Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&quot; , &quot;'&quot; , &quot;s length.
                                            
                                            
                                                2
                                                Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.
                                            
                                        
                                    
                                
                            
                        

                        EYES No Problem PERRLA Cataracts:  Right  Left Glasses Pupils Unequal Glaucoma:  Right  Left Contacts: Right  Left Blurred Vision: Right  Left Prosthesis: Right  Left Scleral Icterus / yellowing Blind: Right  Left Ptosis: Right  Left Other (specify)   textAreaAdjust(false,6695); Infections    Cataract surgery:   Right date   Left date How does the impaired vision interfere/ impact their function/ safety? (explain)    NOSE No Problem Epistaxis Congestion Loss of smell  Sinus problem Other (specify)   MOUTH No Problem Dentures:  Upper  Lower Partial Masses Tumors Toothache  Ulcerations Gingivitis Lesions Other (specify)  THROAT No Problem Dysphagia Hoarseness Lesions Sore throat Other (specify)   
                    

                    
                    
                        PAINWhich pain assessment was used?    Wong-Baker  Pain Assessment in Advanced Dementia Scale (PAINAD) Wong-Baker FACES Pain Rating Scale  From Wong D.L., Hockenberry-Eaton M., Wilson D., Wilkenstein M.L., Schwartz P.:  Wong&quot; , &quot;'&quot; , &quot;s Essentials of Pediatric Nursing,  ed. 6, St. Louis, 2001, p. 1301. Copyrighted by Mosby, Inc.  Reprinted by permission.   Collected using:    FACES Scale   0-10 Scale (subjective reporting) No ProblemPain Intensity:   Is patient experiencing pain?   Yes No  Unable to communicate Non-verbals demonstrated:  Diaphoresis             Grimacing Moaning/Crying Guarding   Irritability Anger Tense Restlessness  Changes in vital signs Other:     Self Assessment  Result        Implications:    How does the pain interfere/impact their functional/activity level? (explain)  Not Applicable   PAIN ASSESSMENTPain Assessment Site 1 Site 2 Site 3 Location       Onset       Present level (0-10)       Worst pain gets (0-10)       Best pain gets (0-10)       Pain description (aching, radiating, throbbing, etc.)   textAreaAdjust(false,6771);  textAreaAdjust(false,6772);  PAIN FREQUENCYWhat makes pain worse?  Movement Ambulation Immobility Other:   Is there a pattern to the pain? (explain)   textAreaAdjust(false,7275);What makes pain better?  Heat/Ice Massage Repositioning Rest/Relaxation Medication Diversion Other   How often is breakthrough medication needed?  Never Less than daily 2-3 times/day More than 3 times/dayDoes the pain radiate?  Occasionally Continuously IntermittentCurrent pain control medications adequate:   Yes NoComments:     

                        
                            
                                
                                    
                                        (M1242) Frequency of Pain Interfering with patient&quot; , &quot;'&quot; , &quot;s activity or movement:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient has no pain
                                            
                                            
                                                1
                                                Patient has pain that does not interfere with activity or movement
                                            
                                            
                                                2
                                                Less often than daily
                                            
                                            
                                                3
                                                Daily, but not constantly
                                            
                                            
                                                4
                                                All of the time
                                            
                                        
                                    
                                
                            
                        

                         
                    

                
            

        
    




    
        
            INTEGUMENTARY STATUS
        
    
    

    
        

            
                
                

                    
                    
                         Definitions:  Unhealed:  The absence of the skin&quot; , &quot;'&quot; , &quot;s original integrity.  Non-epithelialized:  The absence of regenerated epidermis across a wound surface.  Pressure Ulcer:  A   pressure ulcer   is localized injury to the skin and/or underlying tissue, usually over a bony prominence, as a result of pressure or pressure in combination with shear and/or friction.  A number of contributing or confounding factors are also associated with pressure ulcers; the significance of these factors is yet to be elucidated.  Newly epithelialized wound bed completely covered with new epithelium no exudate no avascular tissue (eschar and/or slough) no signs or symptoms of infection Fully granulating wound bed filled with granulation tissue to the level of the surrounding skin no dead space no avascular tissue (eschar and/or slough) wound edges open Early/ partial granulation >=25% of wound bed is covered with granulation tissue &lt;25% of the wound bed is covered with avascular tissue (eschar and/or slough) no signs or symptoms of infection wound edges open 3 - Not healing wound with >=25% avascular tissue (eschar and/or slough) OR  signs/symptoms of infection OR  clean but non-granulating wound bed OR  closed/ hyperkeratotic wound edged OR  Persistent failure to improve despite appropriate comprehensive wound management This guidance applies to surgical wounds closed by either primary intention (i.e. approximated incisions) or secondary intention (i.e. open surgical wounds). PRESSURE ULCER STAGES (NATIONAL PRESSURE ULCER ADVISORY PANEL TOOL): Stage I.  A Stage I pressure ulcer presents as intact skin with non-blanchable redness of a localized area, usually over a bony prominence. Darkly pigmented skin may not have visible blanching. Its color may differ from the surrounding area.  Further description. This area may be painful, firm, soft, and warmer or cooler as compared to adjacent tissue. Stage I ulcers may be difficult to detect in individuals with dark skin tones and may indicate &quot;at risk&quot; persons (a heralding sign of risk). Stage II.  A Stage II pressure ulcer is characterized by partial-thickness loss of dermis presenting as a shallow open ulcer with a red-pink wound bed without slough. It may also present as an intact/ ruptured serum-filled blister. Further description. A Stage II ulcer also may present as a shiny or dry shallow ulcer without slough or bruising.* This stage should not be used to describe skin tears, tape burns, perineal dermatitis, maceration, or excoriation. *Bruising indicates suspected deep tissue injury. Stage III.  A Stage III pressure ulcer is characterized by full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon or muscle are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.  Further description: The depth of a stage III pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and stage III ulcers can be shallow. In contrast, areas of significant adiposity can develop extremely deep stage III pressure ulcers. Bone/tendon is not visible or directly palpable. Stage IV.  A Stage IV pressure ulcer is characterized by Full thickness tissue loss with exposed bone, tendon or muscle. Slough or eschar may be present on some parts of the wound bed. Often include undermining and tunneling. Further description:The depth of a stage IV pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and these ulcers can be shallow. Stage IV ulcers can extend into muscle and/or supporting structures (e.g., fascia, tendon or joint capsule) making osteomyelitis possible. Exposed bone/tendon is visible or directly palpable. Unstageable.  Full-thickness tissue loss in which the base of the ulcer is covered by slough (yellow, tan, gray, green or brown) and/or eschar (tan, brown or black) in the wound bed may render a wound unstageable. Further description. Until enough slough and/or eschar is removed to expose the base of the wound, the true depth (and therefore, the stage) cannot be determined. Stable (dry, adherent, intact without erythema or fluctuance) eschar on the heels serves as &quot;the body&quot; , &quot;'&quot; , &quot;s natural (biological) cover&quot; and should not be removed. Suspected Deep Tissue Injury:  Deep tissue injury may be characterized by a purple or maroon localized area of discolored intact skin or a blood-filled  blister due to damage of underlying soft tissue from pressure and/or shear. Presentation may be preceded by tissue that is painful, firm, mushy, boggy, and warmer or cooler as compared to adjacent tissue. Further description. Deep tissue injury may be difficult to detect in individuals with dark skin tones. Evolution may include a thin blister over a dark wound bed. The wound may further evolve and become covered by thin eschar. Evolution may be rapid, exposing additional layers of tissue even with optimal treatment.  

                        
                            
                                
                                    
                                        (M1306) Does this patient have at least one Unhealed Pressure Ulcer/Injury at Stage 2 or Higher or designated as Unstageable? 
                                        (Excludes Stage 1 pressure injuries and all healed Stage 2 pressure ulcers/injuries)
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                
                                                    No
                                                    
                                                        [ Go to M1322 ]
                                                    
                                                    
                                                
                                            
                                            
                                                1
                                                Yes
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1311) Current Number of Unhealed Pressure Ulcers/Injuries at Each Stage
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Stage
                                            
                                            
                                                Enter Number
                                            
                                        
                                        
                                            
                                                A1. Stage 2:
                                                Partial thickness loss of dermis presenting as a shallow open ulcer with a red or pink wound bed, without slough. 
                                                May also present as an intact or open/ruptured blister.
                                                Number of Stage 2 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                B1. Stage 3:
                                                Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscle is not exposed. 
                                                Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.
                                                Number of Stage 3 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                C1. Stage 4:
                                                Full thickness tissue loss with exposed bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.
                                                Number of Stage 4 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                D1. Unstageable: Non-removable dressing/device:
                                                Known but not stageable due to non-removable dressing/device
                                                Number of unstageable pressure ulcers/injuries due to non-removable dressing/device
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                E1. Unstageable: Slough and/or eschar:
                                                Known but not stageable due to coverage of wound bed by slough and/or eschar
                                                Number of unstageable pressure ulcers/injuries due to coverage of wound bed by sloughand/or eschar
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                F1. Unstageable: Deep tissue injury:
                                                
                                                Number of unstageable pressure injuries presenting as deep tissue injury
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1322) Current Number of Stage 1 Pressure Injuries: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. 
                                        The area may be painful, firm, soft, warmer, or cooler as compared to adjacent tissue. 
                                        Darkly pigmented skin may not have a visible blanching; in dark skin tones only it may appear with persistent blue or purple hues.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                            
                                            
                                                1
                                            
                                            
                                                2
                                            
                                            
                                                3
                                            
                                            
                                                4 or more
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1324) Stage of Most Problematic Unhealed Pressure Ulcer/Injury that is Stageable: 
                                        (Excludes pressure ulcer/injury that cannot be staged due to a non-removable dressing/device, coverage of wound bed by slough and/or eschar, or deep tissue injury.)
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                                NA
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Stage 1
                                            
                                            
                                                2
                                                Stage 2
                                            
                                            
                                                3
                                                Stage 3
                                            
                                            
                                                4
                                                Stage 4
                                            
                                            
                                                NA
                                                Patient has no pressure ulcers/injuries or no stageable pressure ulcers/injuries
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1330) Does this patient have a Stasis Ulcer?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No [ Go to M1340 ]
                                            
                                            
                                                1
                                                Yes, patient has BOTH observable and unobservable stasis ulcers
                                            
                                            
                                                2
                                                Yes, patient has observable stasis ulcers ONLY
                                            
                                            
                                                3
                                                Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing/device)
                                                [ Go to M1340 ]
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1332) Current Number of Stasis Ulcer(s) that are Observable:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                One
                                            
                                            
                                                2
                                                Two
                                            
                                            
                                                3
                                                Three
                                            
                                            
                                                4
                                                Four or more
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1334) Status of Most Problematic Stasis Ulcer that is Observable:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Fully granulating
                                            
                                            
                                                2
                                                Early/partial granulation
                                            
                                            
                                                3
                                                Not healing
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1340) Does this patient have a Surgical Wound?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No [Go to M1400]
                                            
                                            
                                                1
                                                Yes, patient has at least one observable surgical wound
                                            
                                            
                                                2
                                                
                                                    Surgical wound known but not observable due to non-removable dressing/device
                                                    [Go to M1400 ]
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1342) Status of Most Problematic Surgical Wound that is Observable
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Newly epithelialized
                                            
                                            
                                                1
                                                Fully granulating
                                            
                                            
                                                2
                                                Early/partial granulation
                                            
                                            
                                                3
                                                Not healing
                                            
                                        
                                    
                                
                            
                        

                        WOUND CARE (Check all that apply) Wound care done during this visit:   Yes  No Location(s) wound site:    textAreaAdjust(false,15016); Soiled dressing removed by:   Patient  Caregiver (name)     RN/PT  Other:   Technique:   Sterile  Clean  Wound cleaned with (specify):     textAreaAdjust(false,15018); Wound irrigated with (specify):     textAreaAdjust(false,15020); Wound packed with (specify):     textAreaAdjust(false,15022); Wound dressing applied (specify):      textAreaAdjust(false,15024);Patient tolerated procedure well:   Yes  NoComments:    DIABETIC FOOT EXAMFrequency of diabetic foot exam    Done by:  Patient RN/PT Caregiver (name)    Other:   Exam by clinician this visit:  Yes NoIntegument findings:    textAreaAdjust(false,7821);Pedal pulses:  Present  Right LeftAbsent  Right LeftComment:    textAreaAdjust(false,7830);Loss of sense of: Warm  Right LeftCold  Right LeftComment:    textAreaAdjust(false,7865);Neuropathy:  Right LeftTingling:  Right Left   Burning:  Right LeftLeg hair: Present   Right  Left Absent   Right Left Administered by:   WOUND / LESION#1 #2 #3 #4 #5 Date Reported:            Location           Type:  (e.g. Pressure ulcer,  Venous stasis ulcer,   Malignancy,  Malignancy, Diabetic foot ulcer)   textAreaAdjust(false,13421);  textAreaAdjust(false,13422);  textAreaAdjust(false,13423);  textAreaAdjust(false,13424);  textAreaAdjust(false,13425);Size (cm) (LxWxD)           Tunneling           Undermining (cm)           Stage (pressure ulcers only)           Odor           Surrounding Skin           Edema           Stoma           Appearance of the Wound Bed           Drainage / Amount  None None None None None Small Small Small Small Small Moderate Moderate Moderate Moderate Moderate Large Large Large Large LargeColor  Clear Clear Clear Clear Clear Tan Tan Tan Tan Tan Serosanguineous Serosanguineous Serosanguineous Serosanguineous Serosanguineous Other Other Other Other Other          Consistency  Thin Thin Thin Thin Thin Thick Thick Thick Thick Thick
                    

                
            

        
    




    
        
            RESPIRATORY STATUS
        
    
    

    
        

            
                
                


                    
                    
                        SYSTEMS REVIEWHeight:    reported actualWeight:    reported actualReported Weight Changes:  Gain Loss     lb.  X     wk.  mo.  yr. VITAL SIGNSBlood Pressure: Left Right Lying Sitting Standing At rest           With Activity           Post Activity           Temperature:    Oral Axillary Rectal TympanicPulse:  Regular Irregular Apical    Brachial    Rest Activity Radial    Carotid   Respirations:     Regular Irregular Cheynes Stokes Death rattle Rest  Activity Accessory muscles used Apnea periods    sec.  ( observed reported) Smoker Non-smokerLast smoked:    CARDIOPULMONARY No ProblemDisorders of heart / respiratory system:   textAreaAdjust(false,12175);Breath Sounds: (Clear, crankles/rales, wheezes/rhonchi, diminished, absent Aterior: Right     Left:   Posterior: Right Upper     Left Upper   Right Lower     Left Lower   O2 @      LPM via    cannula   mask  trach O2 Saturation     % Trach type/size:   Who manages?  Self Caregiver/family RN Other:   Intermittent treatments (C&amp;DB, medicated inhalation treatments, etc.)  Yes NoExplain:   textAreaAdjust(false,8096); Cough: No Yes: Productive Non-productiveDescribe:   textAreaAdjust(false,8151); Dyspnea: Rest During ADL&quot; , &quot;'&quot; , &quot;sComments:   textAreaAdjust(false,8102);Positioning necessary for improved breathing:  No Yes, describe   textAreaAdjust(false,8106);Heart Sounds:  Regular Irregular Murmur Pacemaker:Date   Type   Date last checked  Chest pain:  Dull Ache Sharp Vise-like Radiating Anginal Postural Localized SubsternalAssociated with:  Shortness of breath Activity SweatsFrequency/duration:   How relieved:   textAreaAdjust(false,8130); Palpitations Fatigue Edema: Pedal Right Pedal Left Sacral Dependent:    Pitting +1 +2 +3 +4 Non-pittingSite:    Cramps Claudication Capillary refill less than 3 sec Capillary refill greater than 3 sec Disease Management Problems (explain)  

                        
                            
                                
                                    
                                        (M1400) When is the patient dyspneic or noticeably Short of Breath?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient is not short of breath
                                            
                                            
                                                1
                                                When walking more than 20 feet, climbing stairs
                                            
                                            
                                                2
                                                With moderate exertion (for example, while dressing, using commode or bedpan, walking distances less than 20 feet)
                                            
                                            
                                                3
                                                With minimal exertion (for example, while eating, talking, or performing other ADLs) or with agitation
                                            
                                            
                                                4
                                                At rest (during day or night)
                                            
                                        
                                    
                                
                            
                        

                         (For M1400):   Assessed ReportedENDOCRINE STATUS A1c    %   Today&quot; , &quot;'&quot; , &quot;s visit  Patient reported   Lab slipDate: BS    mg/dL Date/Time:     FBS  Before Meal  Postprandial  Random HS   Blood sugar ranges       Patient/Family/Caregiver Report Monitored by:   Self Caregiver Family  Nurse Other   Frequency of monitoring   Competency with use of Glucometer    Disease Management Problems (explain)  
                    

                
            

        
    



    
        
            ELIMINATION STATUS
        
    
    

    
        

            
                
                


                    
                    

                    
                    
                        ELIMINATION STATUS No ProblemDisorders of Urinary system:    textAreaAdjust(false,13519);(Check all applicable items)  Urgency  Frequency  Burning  Pain  Hesitancy  Nocturia  Retention Hematuria Oliguria  Anuria  Incontinence (details if applicable)   textAreaAdjust(false,13532);  Diapers  Other    Color:   Yellow  Straw  Amber  Brown  Gray  Blood-tinged  Other:   Clarity:   Clear  Cloudy  Sediment  Mucous Odor:   Yes No  Reported ObservedUrinary Catheter:   Type    Date last changed  Foley inserted  (date)  with    French  Inflated balloon with    mL   without difficulty  Suprapubic Irrigation solution:   Type (specify):   Amount    mL  Frequency   Returns   Patient tolerated procedure well   Yes No Urostomy (describe skin around stoma):   textAreaAdjust(false,13577);Ostomy care managed by:  Self  Caregiver Family Disease Management Problems   BOWEL ELIMINATION No ProblemDisorders of GI system:    textAreaAdjust(false,12505); Flatulence Constipation Diarrhea Rectal bleeding Hemorrhoids Fecal Impaction Last BM  Bowel Sounds: active x    quadrants absent x    quadrants hypoactive x    quadrants hyperactive x    quadrants  Frequency of stools   Bowel regime/program:    textAreaAdjust(false,12497); Laxative/Enema use: Daily Weekly Monthly PRN Other:    Incontinence (details if possible)    Diapers  Other:     Ileostomy/colostomy site (describe skin around stoma):  textAreaAdjust(false,8457);Ostomy care managed by:   Self Caregiver Family Other  

                        
                            
                                
                                    
                                        (M1610) Urinary Incontinence or Urinary Catheter Presence:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No incontinence or catheter (includes anuria or ostomy for urinary drainage)
                                            
                                            
                                                1
                                                Patient is incontinent
                                            
                                            
                                                2
                                                Patient requires a urinary catheter (specifically: external, indwelling, intermittent,
                                                    suprapubic)
                                                
                                            
                                        
                                    
                                
                            
                        

                        NUTRITIONAL STATUS No Problem NAS NPO Controlled Carbohydrate  Other    Increase fluids    amt.    Restrict fluids    amt.  Appetite:  Good Fair Poor Anorexic Nausea/Vomiting:Frequency   Amount    Heartburn (food intolerance) Other    Directions: Check each box with &quot;yes&quot; to assessment, then total score to determine additional risk. YES Has an illness or condition that changed the kind and/or amount of food eaten.  2Eats fewer than 2 meals per day.  3Eats few fruits, vegetables or milk products.  2Has 3 or more drinks of beer, liquor or wine almost every day.   2Has tooth or mouth problems that make it hard to eat.  2Does not always have enough money to buy the food needed.  4Eats alone most of the time.  1Takes 3 or more different prescribed or over-the-counter drugs a day.  1Without wanting to, has lost or gained 10 pounds in the last 6 months.  2Not always physically able to shop, cook and/or feed self.  2Total:    Reprinted with permission by the Nutrition Screening Initiative, a project of the American Academy of   Family Physician, the American Dietetic Association and the National Council on the Aging Inc. and funded in part by grant from Ross Products Division, Abbott Laboratories Inc.    INTERPRETATION 0-2 Good.   As appropriate reassess and/or provide information based on situation.         3-5 Moderate risk.   Educate, refer, monitor and reevaluate based on patient situation and organization policy. 6 or more  High risk.   Coordinate with physician, dietitian, social service professional or nurse about how to  improve nutritional health. Reassess nutritional status and educate based on plan of care.    Describe at risk intervention and plan:   
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1620) Bowel Incontinence Frequency:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                NA
                                                
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Very rarely or never has bowel incontinence
                                            
                                            
                                                1
                                                Less than once weekly
                                            
                                            
                                                2
                                                One to three times weekly
                                            
                                            
                                                3
                                                Four to six times weekly
                                            
                                            
                                                4
                                                On a daily basis
                                            
                                            
                                                5
                                                More often than once daily
                                            
                                            
                                                NA
                                                Patient has ostomy for bowel elimination
                                            
                                            
                                        
                                    
                                
                            
                        

                        PSYCHOSOCIALPrimary language    Language barrier Needs interpreter    Learning barrier: Mental  Psychosocial  Physical  Functional  Unable to read/write Educational level     Spiritual/Cultural implications that impact care.Explain    textAreaAdjust(false,8891);Spiritual resource    textAreaAdjust(false,8893);Phone No.    Sleep/Rest: Adequate InadequateExplain    textAreaAdjust(false,13589); Inappropriate responses to caregivers/clinician Inappropriate follow-through in past Angry Flat affect Discouraged Withdrawn Difficulty coping Disorganized Depressed:  Recent  Long term Treatment:    textAreaAdjust(false,8906); Inability to cope with altered health status as evidenced by:  Lack of motivation  Unrealistic expectations  Inability to recognize problems Denial of problems Evidence of abuse/neglect/exploitation: Potential Actual  Verbal/Emotional Physical Financial InterventionDescribe:    textAreaAdjust(false,8923); Other (specify)   
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that
                                        (within the last 14 days): a) was related to an inpatient facility stay; or b) necessitated a change in medical or
                                        treatment regimen?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient does not have an ostomy for bowel elimination.
                                            
                                            
                                                1
                                                Patient&quot; , &quot;'&quot; , &quot;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.
                                                
                                            
                                            
                                                2
                                                The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.
                                            
                                        
                                    
                                
                            
                        

                        ABDOMEN No Problem  Tenderness Distention Soft Pain Hard Ascites Abdominal girth     cm   Other:   
                    

                
            

        
    




    
        
            ADL/IADLs
        
    
    

    
        

            
                
                


                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1800) Grooming: Current ability to tend safely to personal hygiene needs (specifically: washing face and hands, hair care, shaving or make up,
                                        teeth or denture care, fingernail care).
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to groom self unaided, with or without the use of assistive devices or adapted methods.
                                            
                                            
                                                1
                                                Grooming utensils must be placed within reach before able to complete grooming activities.
                                            
                                            
                                                2
                                                Someone must assist the patient to groom self.
                                            
                                            
                                                3
                                                Patient depends entirely upon someone else for grooming needs.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        MAHC 10 - Fall Risk Assessment ToolRequired Core ElementsAssess one point for each core element &quot;yes&quot;.Information may be gathered from medical record, assessment and if applicable, the patient/caregiver. Beyond protocols listed below, scoring should be based on your clinical judgment.  Points Age 65+   Diagnosis (3 or more co-existing)   Includes only documented medical diagnosis Prior history of falls within 3 months   An unintentional change in position resulting in coming to rest on the ground or at a lower level Incontinence   Inability to make it to the bathroom or commode in timely manner Includes frequency, urgency, and/or nocturia. Visual impairment   Includes but not limited to, macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription. Impaired functional mobility   May include patients who need help with IADLS or ADLS or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices. Environmental hazards   May include but not limited to, poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits. Poly Pharmacy (4 or more prescriptions – any type)   All PRESCRIPTIONS including prescriptions for OTC meds. Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs. Pain affecting level of function   Pain often affects an individual’s desire or ability to move or pain can be a factor in depression or ompliance with safety recommendations. Cognitive impairment   Could include patients with dementia, Alzheimer’s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patients ability to adhere to the plan of care. A score of 4 or more is considered at risk for falling Total   Missouri Alliance for HOME CARE2420 Hyde Park, Suite A, Jefferson City, MO 65109-4731  /  (573) 634-7772  /  (573) 634-4374 Fax MUSCULOSKELETAL No ProblemDisorders of musculoskeletal system:   textAreaAdjust(false,12747); Fracture (location)    Swollen, painful joints (specify)    Contractures: Joint    Location   Hand Grips:   Equal Unequal   Strong Weak  specify   Dominant side:   R L Motor Changes:  Fine Gross  specify    Weakness:  UE LE  details    Atrophy        Paresthesia      Shuffling Wild-based gait Amputation: BK  R  L  AK  R  L  UE R L Specify:    textAreaAdjust(false,9292); Other (specify)  textAreaAdjust(false,9309);How does the patient&quot; , &quot;'&quot; , &quot;s condition affect their function ability and safety?    MENTAL STATUSDescribe the mental status of the patient:  Include the patient&quot; , &quot;'&quot; , &quot;s general appearance, behaviors, emotional responses, mental functioning and their overall social interaction. Include both the clinical objective observations and subjective descriptions during this visit.   textAreaAdjust(false,15639);Was there a sudden/acute change in the patient&quot; , &quot;'&quot; , &quot;s mental status?  Yes  NoIf yes, did the change coincide with something else?  No Yes, explain:  textAreaAdjust(false,15647);Mental status changes reported by:   Patient  Representative Caregiver Other:     

                        
                            
                                
                                    
                                        (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts
                                        and blouses, managing zippers, buttons, and snaps
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.
                                            
                                            
                                                1
                                                Able to dress upper body without assistance if clothing is laid out or handed to the patient.
                                            
                                            
                                                2
                                                Someone must help the patient put on upper body clothing.
                                            
                                            
                                                3
                                                Patient depends entirely upon another person to dress the upper body.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to obtain, put on, and remove clothing and shoes without assistance.
                                            
                                            
                                                1
                                                Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.
                                            
                                            
                                                2
                                                Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.
                                            
                                            
                                                3
                                                Patient depends entirely upon another person to dress lower body.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to bathe self in shower or tub independently, including getting in and out of tub/shower.
                                            
                                            
                                                1
                                                With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.
                                            
                                            
                                                2
                                                
                                                    Able to bathe in shower or tub with the intermittent assistance of another person:
                                                    
                                                        (a) for intermittent supervision or encouragement or reminders, OR
                                                    
                                                    
                                                        (b) to get in and out of the shower or tub, OR
                                                    
                                                    
                                                        (c) for washing difficult to reach areas.
                                                    
                                                
                                            
                                            
                                                3
                                                Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.
                                            
                                            
                                                4
                                                Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.
                                            
                                            
                                                5
                                                
                                                    Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the
                                                    assistance or supervision of another person.
                                                
                                            
                                            
                                                6
                                                Unable to participate effectively in bathing and is bathed totally by another person.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to get to and from the toilet and transfer independently with or without a device.
                                            
                                            
                                                1
                                                When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.
                                            
                                            
                                                2
                                                Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).
                                            
                                            
                                                3
                                                Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.
                                            
                                            
                                                4
                                                Is totally dependent in toileting.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1850) Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently transfer.
                                            
                                            
                                                1
                                                Able to transfer with minimal human assistance or with use of an assistive device.
                                            
                                            
                                                2
                                                Able to bear weight and pivot during the transfer process but unable to transfer self.
                                            
                                            
                                                3
                                                Unable to transfer self and is unable to bear weight or pivot when transferred by another person.
                                            
                                            
                                                4
                                                Bedfast, unable to transfer but is able to turn and position self in bed.
                                            
                                            
                                                5
                                                Bedfast, unable to transfer and is unable to turn and position self.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                    
                                        GG0170. Mobility: Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up code the reason.
                                    
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Coding:
                                            
                                            
                                                Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                                score according to amount of assistance provided.
                                            
                                            
                                                Activities may be completed with or without assistive devices.
                                            
                                            
                                                
                                                    06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                                
                                            
                                            
                                                
                                                    05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                                
                                            
                                            
                                                
                                                    04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as
                                                    patient completes activity. Assistance may be provided throughout the activity or intermittently.
                                                
                                            
                                            
                                                
                                                    03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides
                                                    less than half the effort.
                                                
                                            
                                            
                                                
                                                    02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than
                                                    half the effort.
                                                
                                            
                                            
                                                
                                                    01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more
                                                    helpers is required for the patient to complete the activity.
                                                
                                            
                                            
                                                If activity was not attempted, code reason:
                                            
                                            
                                                
                                                    07.    Patient refused
                                                
                                            
                                            
                                                
                                                    09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                                
                                            
                                            
                                                
                                                    10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                                
                                            
                                            
                                                
                                                    88.    Not attempted due to medical conditions or safety concerns
                                                
                                            
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                4. Follow-up Performance
                                                            
                                                        
                                                        
                                                            
                                                                Enter Codes in Boxes
                                                            
                                                        
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        A. Roll left and right: The ability to roll from lying on back to left and right side, and return to lying on back on the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        B. Sit to lying: The ability to move from sitting on side of bed to lying flat on the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        C. Lying to sitting on side of bed: The ability to move from lying on the back to sitting on the side of the bed with feet flat on the floor, and with no back support
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        D. Sit to stand: The ability to come to a standing position from sitting in a chair, wheelchair, or on the side of the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        E. Chair/bed-to-chair transfer: The ability to transfer to and from a bed to a chair (or wheelchair).
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        F. Toilet transfer: The ability to get on and off a toilet or commode.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        I. Walk 10 feet: Once standing, the ability to walk at least 10 feet in a room, corridor, or similar space.
                                                        If Follow-up performance is coded 07, 09, 10 or 88 → skip to GG0170M, 1 step (curb)
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        J. Walk 50 feet with two turns: Once standing, the ability to walk 50 feet and make two turns.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        L. Walking 10 feet on uneven surfaces: The ability to walk 10 feet on uneven or sloping surfaces (indoor or outdoor), such as turf or gravel.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        M. 1 step (curb): The ability to go up and down a curb and/or up and down one step.
                                                        If Follow-up performance is coded 07, 09, 10 or 88, skip to GG0170Q, Does patient use wheelchair and/or scooter?
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        N. 4 steps: The ability to go up and down four steps with or without a rail.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    
                                                                
                                                            
                                                            
                                                                
                                                                    Q. Does patient use wheelchair/scooter?
                                                                
                                                                
                                                                    
                                                                                  0. No → Skip GG0170R
                                                                    
                                                                    
                                                                                  1. Yes → Continue to GG0170R, Wheel 50 feet with two turns.
                                                                    
                                                                
                                                            
                                                        

                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        R. Wheel 50 feet with two turns: Once seated in wheelchair/scooter, the ability to wheel at least 50 feet and make two turns.
                                                    
                                                
                                            
                                        
                                        
                                    
                                

                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (specifically: needs no human assistance or assistive device).
                                            
                                            
                                                1
                                                With the use of a one-handed device (for example, cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.
                                            
                                            
                                                2
                                                Requires use of a two-handed device (for example, walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.
                                            
                                            
                                                3
                                                Able to walk only with the supervision or assistance of another person at all times.
                                            
                                            
                                                4
                                                Chairfast, unable to ambulate but is able to wheel self independently.
                                            
                                            
                                                5
                                                Chairfast, unable to ambulate and is unable to wheel self.
                                            
                                            
                                                6
                                                Bedfast, unable to ambulate or be up in a chair.
                                            
                                        
                                    
                                
                            
                        

                         Indications for Home Health Aides:   Yes  No  RefusedReferred to HHA:   Yes No Reason for need:     
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                    
                                        GG0130. Self-Care: Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up, code the reason.
                                    
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Coding:
                                            
                                            
                                                Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                                score according to amount of assistance provided.
                                            
                                            
                                                Activities may be completed with or without assistive devices.
                                            
                                            
                                                
                                                    06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                                
                                            
                                            
                                                
                                                    05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                                
                                            
                                            
                                                
                                                    04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as patient completes activity.
                                                    Assistance may be provided throughout the activity or intermittently.
                                                
                                            
                                            
                                                
                                                    03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides less than half the effort.
                                                
                                            
                                            
                                                
                                                    02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than half the effort.
                                                
                                            
                                            
                                                
                                                    01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more helpers is required for the patient to complete the activity.
                                                
                                            
                                            
                                                If activity was not attempted, code reason:
                                            
                                            
                                                
                                                    07.    Patient refused
                                                
                                            
                                            
                                                
                                                    09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                                
                                            
                                            
                                                
                                                    10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                                
                                            
                                            
                                                
                                                    88.    Not attempted due to medical conditions or safety concerns
                                                
                                            
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                4. Follow-up Performance
                                                            
                                                        
                                                        
                                                            
                                                                
                                                                    Enter Codes in Boxes
                                                                
                                                            
                                                        
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        A. Eating: The ability to use suitable utensils to bring food and/or liquid to the mouth and swallow food and/or liquid once the meal is placed before the patient.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        B. Oral Hygiene: 
                                                        The ability to use suitable items to clean teeth. Dentures (if applicable): The ability to insert and remove dentures into and from the mouth, and manage denture soaking and rinsing with use of equipment.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        C. Toileting Hygiene: The ability to maintain perineal hygiene, adjust clothes before and after voiding or having a bowel movement. If managing an ostomy, include wiping the opening but not managing equipment.
                                                    
                                                
                                            
                                        
                                        
                                    
                                

                            
                        

                        
                    


                    
                    

                    
                    

                
            

        
    



    
        
            MEDICATIONS
        
    
    

    
        

            
                
                

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M2030) Management of Injectable Medications: Patient&quot; , &quot;'&quot; , &quot;s current ability to prepare and take all prescribed injectable medications reliably and safely,
                                        including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                NA
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently take the correct medication(s) and proper dosage(s) at the correct times.
                                            
                                            
                                                1
                                                
                                                    Able to take injectable medication(s) at the correct times if:
                                                    
                                                        (a) individual syringes are prepared in advance by another person; OR
                                                    
                                                    
                                                        (b) another person develops a drug diary or chart.
                                                    
                                                
                                            
                                            
                                                2
                                                Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection
                                            
                                            
                                                3
                                                Unable to take injectable medication unless administered by another person.
                                            
                                            
                                                NA
                                                No injectable medications prescribed.
                                            
                                        
                                    
                                
                            
                        

                        INFUSION N/A Central line  Peripheral line  Midline catheter Type/brand   Size/gauge/length    Groshong  Non-groshong  Tunneled  Non-tunneledInsertion site    Insertion date Lumens:   Single  Double  Triple Flush solution/frequency   Patient:  Yes  No Injection cap change frequency   Dressing change frequency    Sterile  CleanPerformed by:  Self Family  Caregiver  RN  Other    Site/skin condition   External catheter length   Other:   textAreaAdjust(false,10021);PICC Specific: Circumference of arm   X-ray verification   Yes  No IVAD Port Specific: Reservoir:   Single	 DoubleHuber gauge/length   Accessed:   No  Yes, date   Epidural/Inrathecal Access: Site/skin condition:   Infusion solution (type/volume/rate)    Pump: (type, specify)    Purpose of Intravenous:    textAreaAdjust(false,10040); Medication(s) administered: (name of drug)   Dose    Route   Frequency    Duration of therapy    Medication(s) administered: (name of drug)   Dose    Route   Frequency    Duration of therapy   Administered by:   Self Family  Caregiver  Other   Purpose of Intravenous Access:  Antibiotic therapy  Pain control   Lab draws Chemotherapy		 Main venous access  Hydration		 Parental nutrition Other    Infusion care provided during visit Yes  No Interventions/Instructions/Comments    Psychotropic drug use:   No  Yes Ability to pay for medications:   Yes  No If No, was MSW referral made?   Yes  No Comment   
                    

                
            

        
    





    
        
            THERAPY NEED AND PLAN OF CARE
        
    
    

    
        

            
                
                
                   
                    
                    
                        STRENGTHS/LIMITATIONSBased upon the patient&quot; , &quot;'&quot; , &quot;s Physical, Psychosocial, Cognitive and Mental Status: List the patient&quot; , &quot;'&quot; , &quot;s strengths that contribute to them meeting their goal(s), both personal and the HHA measurable goals.   textAreaAdjust(false,15786);List the patient&quot; , &quot;'&quot; , &quot;s limitations that might challenge progress toward their goal(s), both personal and the HHA measurable goal.    textAreaAdjust(false,15789);List any diagnosed permanent disability/impairment of the patient:   textAreaAdjust(false,15791);How might the patient&quot; , &quot;'&quot; , &quot;s limitation(s) affect their safety and/or progress?    textAreaAdjust(false,15793);What is the HHA doing/implementing to mitigate the patient&quot; , &quot;'&quot; , &quot;s limitations?    textAreaAdjust(false,15795);Has anything significant changed since the last visit?   No Yes, explain:   REFUSED CARES Patient  Representative Other:    refuse   Cares  Services since the last assessment?  No  Yes, explain  textAreaAdjust(false,15880);Are the  cares  service(s) they refused a significant part of the recommended plan of care? No Yes, explain how  RISK FACTORS/HOSPITAL ADMISSION/EMERGENCY ROOM Not ApplicableRisk factors identified and followed up on by:   Training Discussion EducationLiterature given to:   Patient  Representative Caregiver Family Other:    List identified risk factors the patient has related to an unplanned hospital admission or an emergency department visit    

                        
                            
                                
                                    
                                        (M2200) Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, 
                                        what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)?
                                        (Enter zero [“000”] if no therapy visits indicated.)
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                
                                            
                                        
                                        
                                            Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined).
                                        
                                    
                                
                                
                                    
                                         NA - Not Applicable: No case mix group defined by this assessment.
                                    
                                
                            
                        

                        PATIENT/CAREGIVER EDUCATIONPatient/caregiver/family knowledgeable and able to    verbalize and/or demonstrate independence with:  Wound care:   Yes  No  N/ACatheter care:   Yes  No  N/ADiabetic foot exam/care:   Yes  No  N/ANutritional management:   Yes  No  N/AInsulin administration:   Yes  No  N/APain management:  Yes  No  N/AGlucometer use:   Yes  No  N/AOxygen use:  Yes  No  N/AOral medication(s) administration:	  Yes  No  N/AUse of medical devices:  Yes  No  N/AInjected medication(s) administration:  Yes  No  N/ATrach care:   Yes  No  N/AInfused medication(s) administration:  Yes  No  N/AOstomy care:   Yes  No  N/AInhaled medication(s) administration:  Yes  No  N/AOther care(s):      Yes  No  N/APatient/caregiver/family need further education with:    textAreaAdjust(false,13006);Caregiver/family present at time of visit:   Yes  No Patient/caregiver/family educated this visit for:    textAreaAdjust(false,10789);Patient/caregiver appears to understand all information given:  Yes  No Does the patient/caregive/family have an action plan?   Yes  No Comments:    SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - NURSING INTERVENTIONS/INSTRUCTIONS Skilled observation &amp; assessment    Teach   Admin.   IVs  Clysis   Diabetic observation   Post-partum assessment  Prenatal Assessment  Teach   infant  child care  Foley care  Teach   ostomy  ileo. conduit care  Teach diabetic care   Wound   care  dressing  Teach   Admin. tube feedings  Observe   Teach medication  Decubitis care   Teach   Admin. care of trach  N   or  C  Venipuncture   Teach care - terminally ill   effects  side effects  Change   NG  G tube  IM injection   SQ injection  Physiology    Disease process teaching  Admin. of vitamin B2   Psych. intervention   Diet teaching   Prep.   Admin. insulin  Observe S/S infection   Safety factors   Fall Safety Teaching   Pain Management   Other:   SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - PHYSICAL THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation   Balance training/activities    Teach hip precautions   Establish upgrade home exercise program   Pulmonary Physical Training  Teach safe/effective use of adaptive/assist device (specify)   Copy given to patient  Ultrasound     Copy attached to chart  Electrotherapy   Teach safe stair climbing skills  Patient/Family education   Prosthetic training   Teach fall safety   Therapeutic exercise   TENS  Other:   Transfer training   Functional mobility training    textAreaAdjust(false,10565); Gait training   Teach bed mobility skills  SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - OCCUPATIONAL THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation    Neuro-developmental training    Therapeutic exercise to right/left hand   Establish home exercise program   Sensory treatment  to increase strength, coordination,   Copy given to patient Orthotics  Splinting sensation and proprioception   Copy attached to chart  Adaptive equipment (fabrication   Fine motor coordination   Patient/Family education  and training)   Perceptual motor training   Independent living/ADL training    Teach alternative bathing skills   Teach fall safety   Muscle re-education  (unable to use tub/shower safely)  Retraining of cognitive,  Other:   feeding and perpetual skills    SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - SPEECH THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation   Dysphagia treatments    Safe swallowing evaluation   Establish home maintenance program   Language disorders   Speech dysphagia instruction program   Copy given to patient  Aural rehabilitation   Teach/Develop communication system   Copy attached to chart  Non-oral communication  Other:   Patient/Family education   Alaryngeal speech skill    textAreaAdjust(false,10651); Voice disorders   Language processing   Speech articulation disorders   Food texture recommendations  SKILLED CARE PROVIDED THIS VISIT  SUMMARY CHECKLIST CARE PLAN:  Reviewed with patient/caregiver/family involvementMEDICATION STATUS:  Medication Regimen completed/reviewed  No change Order obtainedCheck if any of the following were identified:    Potential adverse effects/drug reactions Ineffective drug therapy Significant side effects  Significant drug interactions Duplicate drug therapy Non-compliance with drug therapyCARE COORDINATION:  Physician  SN  PT  OT  ST  MSW  Aide  Other (specify)   Was a referral made to MSW for assistance with community resources/assistance with a living will/counseling needs (depression/suicidal ideation) and/or unsafe environment?  Date   Yes  No  Refused  N/A   Comment    Verbal Order obtained:  No  Yes, specify date   AMPLIFICATION OF CARE PROVIDED / ANALYSIS OF FINDINGS  textAreaAdjust(false,10671);Patient/Caregiver Response   HOMEBOUND REASON Needs assistance for all activities  Confusion, unable to go out of home alone Residual weakness Unable to safely leave home unassisted Requires assistance to ambulate Severe SOB, SOB upon exertion Dependent upon adaptive device(s) Medical restrictions Other (specify)   SUPERVISORY VISIT:   Yes  No  Scheduled  UnscheduledSTAFF:   Present  Not Present NEXT SCHEDULED SUPERVISORY VISIT CARE PLAN UPDATED?   Yes  NoCARE PLAN FOLLOWED ?  Yes  No (explain)   textAreaAdjust(false,10698);OBSERVATION OF    textAreaAdjust(false,10700);TEACHING/TRAINING OF    textAreaAdjust(false,10702);IS PATIENT/FAMILY SATISFIED WITH CARE?   Yes  No (explain)   CARE SUMMARY Dear Doctor   This Care Summary page is your   Recertification (follow-up) Other Follow-upThank you for allowing us to care for your patient. Disciplines Involved Comments  SN  textAreaAdjust(false,14055); PT  textAreaAdjust(false,14056); OT  textAreaAdjust(false,14057); ST  textAreaAdjust(false,14058); MSW  textAreaAdjust(false,14059); Aide      textAreaAdjust(false,14060);Date of last home visit Physician notified:  Yes No POT485 attached for signature. Please sign and return. Copy of Care Summary  sent faxed Date: SUMMARY Complete this Section for Recertification (Unless Summary is written elsewhere) REASON FOR ADMISSION (describe condition)   textAreaAdjust(false,14073);SUMMARY OF CARE (Including progress toward goals to date)   textAreaAdjust(false,14074);DISCHARGE PLANNING (specify future follow-up, referrals, etc.)   DME/MEDICAL SUPPLIES DME Company:    Phone:   Oxygen Company:    Phone:   Community org./services:    Contact:   Phone:   Comments:    ASSESSMENT SUMMARY Reason for Admission:    Homebound Reason:      Any changes since last assessment:   
                    

                    
                    

                
            

        
    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                

                
                    
                        Hide Validation Results
                    
                
            
        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    
        
            
                
                    M-ITEM
                    QUESTION
                    ERROR MESSAGE
                    SEVERITY
                
            
            
                
                    
                        
                    
                    
                         () Unable to Genarate HIPPS CODE:
                    
                    
                        
                    
                    
                        
                            ERROR
                        
                        
                    
                
                    
                        M1021
                    
                    
                        a. Primary Diagnosis 
                    
                    
                        The Primary Diagnosis N19 is not billable under PDGM.
                    
                    
                        
                            ERROR
                        
                        
                    
                
            
        
    

    

    
        Oasis Audits
        
            
                
                    MOSET
                    PREVIOUS
                    CURRENT
                    STATUS
                
            
            
                
                    
                        M1020_PRIMARY_DIAG_ICD
                    
                    
                        B77.81  
                    
                    
                        N19     
                    
                    
                        
                            Changed
                        
                    
                
                    
                        M1020_PRIMARY_DIAG_SEVERITY
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1024_OTH_DIAG1_ICD
                    
                    
                        J60     
                    
                    
                        G60.0   
                    
                    
                        
                            Changed
                        
                    
                
                    
                        M1024_OTH_DIAG1_SEVERITY
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1024_OTH_DIAG2_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG2_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG3_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG3_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG4_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG4_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG5_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG5_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_A3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_C3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_D3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_E3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_F3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_A4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_C4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_D4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_E4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_F4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1030_THH
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1200_VISION
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1242_PAIN_FREQ_ACTVTY_MVMT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1306_UNHLD_STG2_PRSR_ULCR
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG2
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG2_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG3
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG3_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG4
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG4_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_DRSG
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DRSG_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_CVRG
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_CVRG_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1322_NBR_PRSULC_STG1
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1324_STG_PRBLM_ULCER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1330_STAS_ULCR_PRSNT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1332_NUM_STAS_ULCR
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1334_STUS_PRBLM_STAS_ULCR
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1340_SRGCL_WND_PRSNT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1342_STUS_PRBLM_SRGCL_WND
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1350_LESION_OPEN_WND
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1400_WHEN_DYSPNEIC
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1610_UR_INCONT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1620_BWL_INCONT
                    
                    
                        00
                    
                    
                        00
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1630_OSTOMY
                    
                    
                        00
                    
                    
                        00
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1810_CUR_DRESS_UPPER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1820_CUR_DRESS_LOWER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1830_CRNT_BATHG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1840_CUR_TOILTG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1850_CUR_TRNSFRNG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1860_CRNT_AMBLTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1860_CRNT_AMBLTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2030_CRNT_MGMT_INJCTN_MDCTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2200_THER_NEED_NUM
                    
                    
                        8
                    
                    
                        8
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2200_THER_NEED_NA
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
            
        
        
    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

&quot;) or . = concat(&quot;
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
                PPS Calculator
            
            
                PDGM Calculator
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
            
                
                FRA
            
            
                
                HRA
            
            
                
                Upload File
            
            
            
            
                
                Fax Oasis
            
        
    

    
        Other follow-up assessment (07/01/2022 - 08/29/2022) (Oasis D1)
    
    
        3 - STYLES, FURIOUS 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            
                
                    
                    
                        
                            PATIENT TRACKING SHEET
                        
                            CLINICAL RECORD ITEMS
                        
                            PATIENT HISTORY AND DIAGNOSES
                        
                            SENSORY STATUS
                        
                            INTEGUMENTARY STATUS
                        
                            RESPIRATORY STATUS
                        
                            ELIMINATION STATUS
                        
                            ADL / IADLs
                        
                            MEDICATIONS
                        
                            THERAPY NEED AND PLAN OF CARE
                        
                    
                
            
            
                
                    

                        
                        
                            
    
        
            PATIENT TRACKING SHEET
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

    
        
            
            
                
                
                    LEGAL REPRESENTATIVE
                    
                        
                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Contact No.:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                            
                            
                                
                                     DPOA           
                                     Conservator
                                
                            
                        

                    
                
            

            
                
                
                    PATIENT CONTACT PERSON
                    

                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Home Phone:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Mobile Phone:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        

                    
                
            

        
    

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        
            
                
                    (M0140) RACE/ETHNICITY (Mark all that apply.)
                
                
                    
                        
                            
                                
                                1 - American Indian or Alaska Native
                            
                        
                        
                            
                                
                                3 - Black or African-American
                            
                        
                        
                            
                                
                                5 - Native Hawaiian or Pacific Islander
                            
                        
                        
                    
                    
                        
                            
                                
                                2 - Asian
                            
                        
                        
                            
                                
                                4 - Hispanic or Latino
                            
                        
                        
                            
                                
                                6 - White
                            
                        
                    
                
            
        

        
        
            If an assessment was erroneously submitted in a masked format, that is, it was later discovered that the patient was a Medicare or Medicaid patient
            but was not originally indicated as such at M0150, then an inactivation must be submitted. Normally, the HHA will also submit a new, corrected assessment
            in this situation. For example, if the value at M0150 for a submitted and accepted assessment is not equal to 1, 2, 3, or 4, and it should have been, then
            an inactivation request should be submitted.
            
                
                    (M0150)Current Payment Sources for Home Care (Mark all that apply.)
                
                
                    
                        
                            
                                
                                0 - None; no charge for current services
                            
                        
                        
                            
                                
                                4 - Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8 - Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1 - Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5 - Workers&quot; , &quot;'&quot; , &quot; compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2 - Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6 - Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10 - Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7 - Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11 - Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK - Unknown
                            
                        
                    
                
            
        

    


    .panelTitle {
        text-align: center;
        font-weight: bold;
        font-size: 15px;
        padding: 10px;
    }

    div.ui-datepicker {
        font-size: 12px;
    }




    
        
            CLINICAL RECORD ITEMS
        
    
    

    
        
            
                
                


                    
                    
                        COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

                        
                        
                            
                                
                                    
                                        (M0080) Discipline of Person Completing Assessment
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                RN
                                            
                                            
                                                2
                                                PT
                                            
                                            
                                                3
                                                SLP/ST
                                            
                                            
                                                4
                                                OT
                                            
                                        
                                    
                                
                                
                                    
                                        Care Staff Name:
                                        
                                            
                                            
                                            
                                            
                                                
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    (M0090) Date Assessment Completed:
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                        EMERGENCY PREPAREDNESSPriority Code:   Patient has an Advance Directives Order:   Yes No Time In:    Time Out:     Emergency Contact Information:  Name:   Relationship:    Phone:   Email:   Address:   
                    

                    
                    
                        
                        

                        
                        
                            
                                
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                
                                                4
                                                5
                                                
                                                
                                                
                                                
                                            
                                        
                                    
                                    
                                    
                                        
                                            Follow-Up
                                        
                                        
                                            
                                                
                                                    4
                                                    Recertification (follow-up) reassessment [ Go to M0110 ]
                                                
                                                
                                                    5
                                                    Other follow-up [ Go to M0110 ]
                                                
                                            
                                        
                                    
                                    
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                        (M0110) Episode Timing: Is the Medicare home health payment episode for which this assessment will define a case mix group
                                        an “early” episode or a “later” episode in the patient’s current sequence of adjacent Medicare home health payment episodes?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                UK
                                                NA
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Early
                                            
                                            
                                                2
                                                Later
                                            
                                            
                                                UK
                                                Unknown
                                            
                                            
                                                NA
                                                Not Applicable: No Medicare case mix group to be defined by this assessment.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                
            
        
    









    
        
            PATIENT HISTORY AND DIAGNOSES
        
    
    

    
        

            
                
                


                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    (M1021/1023) Diagnoses and Symptom Control: List each diagnosis for which the patient is receiving home care in Column 1, and enter its
                                    ICD-10-CM code at the level of highest specificity in Column 2 (diagnosis codes only -no surgical or procedure codes allowed).
                                    Diagnoses are listed in the order that best reflects the seriousness of each condition and supports the disciplines and services provided.
                                    Rate the degree of symptom control for each condition in Column 2. ICD-10-CM sequencing requirements must be followed if multiple coding is indicated for any diagnoses.
                                    
                                    

                                    Code each row according to the following directions for each column.
                                
                            
                        
                        
                            
                                
                                    Column 1:
                                
                                
                                    Enter the description of the diagnosis. Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.
                                
                            
                            
                                
                                    Column 2:
                                
                                

                                    Enter the ICD-10-CM code for the condition described in Column 1 -no surgical or procedure codes allowed. Codes must be entered at the level of highest specificity and ICD-10-CM coding rules
                                    and sequencing requirements must be followed. Note that external cause codes (ICD-10-CM codes beginning with V, W, X, or Y) may not be reported in M1021 (Primary Diagnosis) but may be reported
                                    in M1023 (Secondary Diagnoses). Also note that when a Z-code is reported in Column 2, the code for the underlying condition can often be entered in Column 2, as long as it is an active on-going
                                    condition impacting home health care.

                                    
                                        Rate the degree of symptom control for the condition listed in Column 1. Do not assign a symptom control rating if the diagnosis code is a V, W, X, Y or Z-code.
                                        Choose one value that represents the degree of symptom control appropriate for each diagnosis using the following scale:
                                    
                                    
                                        0 - Asymptomatic, no treatment needed at this time
                                    
                                    
                                        1 - Symptoms well controlled with current therapy
                                    
                                    
                                        2 - Symptoms controlled with difficulty, affecting daily functioning; patient needs ongoing monitoring
                                    
                                    
                                        3 - Symptoms poorly controlled; patient needs frequent adjustment in treatment and dose monitoring
                                    
                                    
                                        4 - Symptoms poorly controlled; history of re-hospitalizations
                                    
                                    
                                        Note that the rating for symptom control in Column 2 should not be used to determine the sequencing of the diagnoses listed in Column 1. These are separate items and sequencing may not coincide.
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    (M1021) Primary Diagnosis &amp; (M1023) Other Diagnoses
                                
                                
                                    Column 1
                                    Column 2
                                
                                
                                    
                                        Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.)
                                    
                                    
                                        ICD-10-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.
                                    
                                
                                
                                    Description
                                    ICD-10-C M / Symptom Control Rating
                                
                                
                                    
                                        (M1021) Primary Diagnosis
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        V, W, X, Y codes NOT allowed
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
                
                    
                        N19       
                        UNSPECIFIED KIDNEY FAILURE
                    
                
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        (M1023) Other Diagnoses
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        

                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        All ICD-10–CM codes allowed
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
                
                    
                        G60.0     
                        HEREDITARY MOTOR AND SENSORY NEUROPATHY
                    
                
            
                
                    
                        G60.1     
                        REFSUM&quot; , &quot;'&quot; , &quot;S DISEASE
                    
                
            
                
                    
                        G60.2     
                        NEUROPATHY IN ASSOCIATION WITH HEREDITARY ATAXIA
                    
                
            
                
                    
                        G60.3     
                        IDIOPATHIC PROGRESSIVE NEUROPATHY
                    
                
            
                
                    
                        G60.8     
                        OTHER HEREDITARY AND IDIOPATHIC NEUROPATHIES
                    
                
            
                
                    
                        G60.9     
                        HEREDITARY AND IDIOPATHIC NEUROPATHY, UNSPECIFIED
                    
                
            
                
                    
                        G61.0     
                        GUILLAIN-BARRE SYNDROME
                    
                
            
                
                    
                        G61.1     
                        SERUM NEUROPATHY
                    
                
            
                
                    
                        G61.81    
                        CHRONIC INFLAMMATORY DEMYELINATING POLYNEURITIS
                    
                
            
                
                    
                        G61.82    
                        MULTIFOCAL MOTOR NEUROPATHY
                    
                
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                
                            
                        

                        
                        
                            

                                

                                
                                
                                    
                                        
                                            
                                                
                                                    g. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    h. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    i. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    j. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    k. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    l. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    m. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    n. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    o. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    p. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    q. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    r. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    s. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    t. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    u. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    v. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    w. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    x. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    y. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                                                                        
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                

                            

                            
                        
                        
                        
                        
                            
                                
                                    
                                        Surgical Procedure
                                    
                                    
                                        ICD-10-CM
                                    
                                    
                                        Date
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                a.
                                            
                                            
    
        
            
        
    
        
            
            
                
            
         

    

                                        
                                    
                                    
                                        
    
        
            
        
    
        
            
            
                
            
         

    

                                    
                                    
                                        
                                            
                                            
                                        
                                    
                                    
                                         Onset
                                    
                                

                                
                                    
                                        
                                            
                                                b.
                                            
                                            
    
        
            
        
    
        
            
            
                
            
         

    

                                        
                                    
                                    
                                        
    
        
            
        
    
        
            
            
                
            
         

    

                                    
                                    
                                        
                                            
                                            
                                        
                                    
                                    
                                         Onset
                                    
                                
                            
                        
                        

                        
                    

                    
                    
                        
                            
                                
                                    
                                        Prior history of falls within 3 months
                                     
                                    Fall Definition, “An unintentional change in position resulting in coming to rest on the ground or at a lower level.”
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Environmental Hazards
                                     
                                    May include poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits.
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Poly Pharmacy (4 or more prescriptions)
                                     
                                    Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.
                                
                                
                                    
                                        
                                            
                                            
                                                0
                                                1
                                            
                                        
                                    
                                
                            
                        
                    

                    
                    
                        
                            
                                
                                    
                                        Fall Risk
                                    
                                
                                
                                    
                                        
                                            
                                            
                                                LOW
                                                HIGH
                                            
                                        
                                    
                                
                            
                        
                    

                    
                        NEURO/EMOTIONAL/BEHAVIOR STATUS No Problem Neurological Disorder(s) type:    textAreaAdjust(false,9094); History of traumatic brain injury: (date):  History of headaches:  (Date of last headache):  Type:    Aphasia:  Receptive Expressive Tremors:  At Rest With Voluntary movement Continuous Spasms Location:     History of Seizures (date)   Type:   How does patient&quot; , &quot;'&quot; , &quot;s condition affect their functional ability and safety?    

                        
                            
                                
                                    
                                        Cognitive Functioning: Patient&quot; , &quot;'&quot; , &quot;s current (day of assessment) level of alertness, orientation, comprehension,
                                        concentration, and immediate memory for simple commands.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.
                                            
                                            
                                                1
                                                Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.
                                            
                                            
                                                2
                                                
                                                    Requires assistance and some direction in specific situations (for example, on all tasks involving shifting
                                                    of attention) or consistently requires low stimulus environment due to distractibility.
                                                
                                            
                                            
                                                3
                                                
                                                    Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift
                                                    attention and recall directions more than half the time.
                                                
                                            
                                            
                                                4
                                                Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.
                                            
                                        
                                    
                                
                            
                        
                        
                            
                                Cognitive Functioning Narrative
                            
                            
                        
                        
                    

                    
                    

                    
                    
                        LIVING ARRANGEMENTS/SUPPORTIVE ASSISTANCEPrimary Caregiver  Patient Family Caregiver    Phone Number (if different from patient)      Relationship   List name/relationship of other caregiver(s) (other than home health staff) and the specific assistance they give with  medical cares, ADLs, and or IADLs:    textAreaAdjust(false,6544);Able to safely care for patient  	 Yes  No Comments:    textAreaAdjust(false,6548);Other agencies/co-ordination of care:    textAreaAdjust(false,6550);Patient Received Influenza vaccine (specifically this years flu season):   Yes  No date:  Patient received any vaccines (except influenza) since they were admitted for home care:   Yes  No Type/Date:     

                        
                            
                                
                                    
                                        (M1030) Therapies the patient receives at home: (Mark all that apply.)
                                    
                                    
                                
                                
                                    
                                         1 - Intravenous or infusion therapy (excludes TPN)
                                    
                                    
                                         2 - Parenteral nutrition (TPN or lipids)
                                    
                                    
                                         3 - Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)
                                    
                                    
                                         4 - None of the above
                                    
                                    
                                         = - Ignored
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1033) Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization? (Mark all that apply.)
                                    
                                    
                                
                                
                                    
                                         1 - History of falls (2 or more falls - or any fall with an injury - in the past 12 months)
                                    
                                    
                                         2 - Unintentional weight loss of a total of 10 pounds or more in the past 12 months
                                    
                                    
                                         3 - Multiple hospitalizations (2 or more) in the past 6 months
                                    
                                    
                                         4 - Multiple emergency department visits (2 or more) in the past 6 months
                                    
                                    
                                         5 - Decline in mental, emotional, or behavioral status in the past 3 months
                                    
                                    
                                         6 - Reported or observed history of difficulty complying with any medical instructions
                                        (for example, medications, diet, exercise) in the past 3 months
                                    
                                    
                                         7 - Currently taking 5 or more medications
                                    
                                    
                                         8 - Currently reports exhaustion
                                    
                                    
                                         9 - Other risk(s) not listed in 1 – 8
                                    
                                    
                                         10 - None of the above
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        
                            
                                ADVANCE DIRECTIVES
                            
                        
                        
                            
                                 Living will
                            
                            
                                 Do not resuscitate
                            
                            
                                 Organ Donor
                            
                            
                                 Education needed
                            
                            
                                 Copies on File
                            
                            
                                 Funeral arrangements made
                            
                            
                                 Others: 
                                
                                
                            
                            
                                 Power of Attorney
                            
                        
                    

                
            

        

        
            
                
                    
                        ICD-9:
                        ICD-10:
                    
                
                
                    
                        Code
                        Description
                        Code
                        Description
                    
                    
                
            
        

    


    
        
            SENSORY STATUS
        
    
    

    
        

            
                
                


                    
                    
                         

                        
                            
                                
                                    
                                        (M1200) Vision (with corrective lenses if the patient usually wears them):
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Normal vision: sees adequately in most situations; can see medication labels, newsprint.
                                            
                                            
                                                1
                                                Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&quot; , &quot;'&quot; , &quot;s length.
                                            
                                            
                                                2
                                                Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.
                                            
                                        
                                    
                                
                            
                        

                        EYES No Problem PERRLA Cataracts:  Right  Left Glasses Pupils Unequal Glaucoma:  Right  Left Contacts: Right  Left Blurred Vision: Right  Left Prosthesis: Right  Left Scleral Icterus / yellowing Blind: Right  Left Ptosis: Right  Left Other (specify)   textAreaAdjust(false,6695); Infections    Cataract surgery:   Right date   Left date How does the impaired vision interfere/ impact their function/ safety? (explain)    NOSE No Problem Epistaxis Congestion Loss of smell  Sinus problem Other (specify)   MOUTH No Problem Dentures:  Upper  Lower Partial Masses Tumors Toothache  Ulcerations Gingivitis Lesions Other (specify)  THROAT No Problem Dysphagia Hoarseness Lesions Sore throat Other (specify)   
                    

                    
                    
                        PAINWhich pain assessment was used?    Wong-Baker  Pain Assessment in Advanced Dementia Scale (PAINAD) Wong-Baker FACES Pain Rating Scale  From Wong D.L., Hockenberry-Eaton M., Wilson D., Wilkenstein M.L., Schwartz P.:  Wong&quot; , &quot;'&quot; , &quot;s Essentials of Pediatric Nursing,  ed. 6, St. Louis, 2001, p. 1301. Copyrighted by Mosby, Inc.  Reprinted by permission.   Collected using:    FACES Scale   0-10 Scale (subjective reporting) No ProblemPain Intensity:   Is patient experiencing pain?   Yes No  Unable to communicate Non-verbals demonstrated:  Diaphoresis             Grimacing Moaning/Crying Guarding   Irritability Anger Tense Restlessness  Changes in vital signs Other:     Self Assessment  Result        Implications:    How does the pain interfere/impact their functional/activity level? (explain)  Not Applicable   PAIN ASSESSMENTPain Assessment Site 1 Site 2 Site 3 Location       Onset       Present level (0-10)       Worst pain gets (0-10)       Best pain gets (0-10)       Pain description (aching, radiating, throbbing, etc.)   textAreaAdjust(false,6771);  textAreaAdjust(false,6772);  PAIN FREQUENCYWhat makes pain worse?  Movement Ambulation Immobility Other:   Is there a pattern to the pain? (explain)   textAreaAdjust(false,7275);What makes pain better?  Heat/Ice Massage Repositioning Rest/Relaxation Medication Diversion Other   How often is breakthrough medication needed?  Never Less than daily 2-3 times/day More than 3 times/dayDoes the pain radiate?  Occasionally Continuously IntermittentCurrent pain control medications adequate:   Yes NoComments:     

                        
                            
                                
                                    
                                        (M1242) Frequency of Pain Interfering with patient&quot; , &quot;'&quot; , &quot;s activity or movement:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient has no pain
                                            
                                            
                                                1
                                                Patient has pain that does not interfere with activity or movement
                                            
                                            
                                                2
                                                Less often than daily
                                            
                                            
                                                3
                                                Daily, but not constantly
                                            
                                            
                                                4
                                                All of the time
                                            
                                        
                                    
                                
                            
                        

                         
                    

                
            

        
    




    
        
            INTEGUMENTARY STATUS
        
    
    

    
        

            
                
                

                    
                    
                         Definitions:  Unhealed:  The absence of the skin&quot; , &quot;'&quot; , &quot;s original integrity.  Non-epithelialized:  The absence of regenerated epidermis across a wound surface.  Pressure Ulcer:  A   pressure ulcer   is localized injury to the skin and/or underlying tissue, usually over a bony prominence, as a result of pressure or pressure in combination with shear and/or friction.  A number of contributing or confounding factors are also associated with pressure ulcers; the significance of these factors is yet to be elucidated.  Newly epithelialized wound bed completely covered with new epithelium no exudate no avascular tissue (eschar and/or slough) no signs or symptoms of infection Fully granulating wound bed filled with granulation tissue to the level of the surrounding skin no dead space no avascular tissue (eschar and/or slough) wound edges open Early/ partial granulation >=25% of wound bed is covered with granulation tissue &lt;25% of the wound bed is covered with avascular tissue (eschar and/or slough) no signs or symptoms of infection wound edges open 3 - Not healing wound with >=25% avascular tissue (eschar and/or slough) OR  signs/symptoms of infection OR  clean but non-granulating wound bed OR  closed/ hyperkeratotic wound edged OR  Persistent failure to improve despite appropriate comprehensive wound management This guidance applies to surgical wounds closed by either primary intention (i.e. approximated incisions) or secondary intention (i.e. open surgical wounds). PRESSURE ULCER STAGES (NATIONAL PRESSURE ULCER ADVISORY PANEL TOOL): Stage I.  A Stage I pressure ulcer presents as intact skin with non-blanchable redness of a localized area, usually over a bony prominence. Darkly pigmented skin may not have visible blanching. Its color may differ from the surrounding area.  Further description. This area may be painful, firm, soft, and warmer or cooler as compared to adjacent tissue. Stage I ulcers may be difficult to detect in individuals with dark skin tones and may indicate &quot;at risk&quot; persons (a heralding sign of risk). Stage II.  A Stage II pressure ulcer is characterized by partial-thickness loss of dermis presenting as a shallow open ulcer with a red-pink wound bed without slough. It may also present as an intact/ ruptured serum-filled blister. Further description. A Stage II ulcer also may present as a shiny or dry shallow ulcer without slough or bruising.* This stage should not be used to describe skin tears, tape burns, perineal dermatitis, maceration, or excoriation. *Bruising indicates suspected deep tissue injury. Stage III.  A Stage III pressure ulcer is characterized by full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon or muscle are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.  Further description: The depth of a stage III pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and stage III ulcers can be shallow. In contrast, areas of significant adiposity can develop extremely deep stage III pressure ulcers. Bone/tendon is not visible or directly palpable. Stage IV.  A Stage IV pressure ulcer is characterized by Full thickness tissue loss with exposed bone, tendon or muscle. Slough or eschar may be present on some parts of the wound bed. Often include undermining and tunneling. Further description:The depth of a stage IV pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and these ulcers can be shallow. Stage IV ulcers can extend into muscle and/or supporting structures (e.g., fascia, tendon or joint capsule) making osteomyelitis possible. Exposed bone/tendon is visible or directly palpable. Unstageable.  Full-thickness tissue loss in which the base of the ulcer is covered by slough (yellow, tan, gray, green or brown) and/or eschar (tan, brown or black) in the wound bed may render a wound unstageable. Further description. Until enough slough and/or eschar is removed to expose the base of the wound, the true depth (and therefore, the stage) cannot be determined. Stable (dry, adherent, intact without erythema or fluctuance) eschar on the heels serves as &quot;the body&quot; , &quot;'&quot; , &quot;s natural (biological) cover&quot; and should not be removed. Suspected Deep Tissue Injury:  Deep tissue injury may be characterized by a purple or maroon localized area of discolored intact skin or a blood-filled  blister due to damage of underlying soft tissue from pressure and/or shear. Presentation may be preceded by tissue that is painful, firm, mushy, boggy, and warmer or cooler as compared to adjacent tissue. Further description. Deep tissue injury may be difficult to detect in individuals with dark skin tones. Evolution may include a thin blister over a dark wound bed. The wound may further evolve and become covered by thin eschar. Evolution may be rapid, exposing additional layers of tissue even with optimal treatment.  

                        
                            
                                
                                    
                                        (M1306) Does this patient have at least one Unhealed Pressure Ulcer/Injury at Stage 2 or Higher or designated as Unstageable? 
                                        (Excludes Stage 1 pressure injuries and all healed Stage 2 pressure ulcers/injuries)
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                
                                                    No
                                                    
                                                        [ Go to M1322 ]
                                                    
                                                    
                                                
                                            
                                            
                                                1
                                                Yes
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1311) Current Number of Unhealed Pressure Ulcers/Injuries at Each Stage
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Stage
                                            
                                            
                                                Enter Number
                                            
                                        
                                        
                                            
                                                A1. Stage 2:
                                                Partial thickness loss of dermis presenting as a shallow open ulcer with a red or pink wound bed, without slough. 
                                                May also present as an intact or open/ruptured blister.
                                                Number of Stage 2 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                B1. Stage 3:
                                                Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscle is not exposed. 
                                                Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.
                                                Number of Stage 3 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                C1. Stage 4:
                                                Full thickness tissue loss with exposed bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.
                                                Number of Stage 4 pressure ulcers
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                D1. Unstageable: Non-removable dressing/device:
                                                Known but not stageable due to non-removable dressing/device
                                                Number of unstageable pressure ulcers/injuries due to non-removable dressing/device
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                E1. Unstageable: Slough and/or eschar:
                                                Known but not stageable due to coverage of wound bed by slough and/or eschar
                                                Number of unstageable pressure ulcers/injuries due to coverage of wound bed by sloughand/or eschar
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                        
                                            
                                                F1. Unstageable: Deep tissue injury:
                                                
                                                Number of unstageable pressure injuries presenting as deep tissue injury
                                                
                                                
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1322) Current Number of Stage 1 Pressure Injuries: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. 
                                        The area may be painful, firm, soft, warmer, or cooler as compared to adjacent tissue. 
                                        Darkly pigmented skin may not have a visible blanching; in dark skin tones only it may appear with persistent blue or purple hues.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                            
                                            
                                                1
                                            
                                            
                                                2
                                            
                                            
                                                3
                                            
                                            
                                                4 or more
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1324) Stage of Most Problematic Unhealed Pressure Ulcer/Injury that is Stageable: 
                                        (Excludes pressure ulcer/injury that cannot be staged due to a non-removable dressing/device, coverage of wound bed by slough and/or eschar, or deep tissue injury.)
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                                NA
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Stage 1
                                            
                                            
                                                2
                                                Stage 2
                                            
                                            
                                                3
                                                Stage 3
                                            
                                            
                                                4
                                                Stage 4
                                            
                                            
                                                NA
                                                Patient has no pressure ulcers/injuries or no stageable pressure ulcers/injuries
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1330) Does this patient have a Stasis Ulcer?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No [ Go to M1340 ]
                                            
                                            
                                                1
                                                Yes, patient has BOTH observable and unobservable stasis ulcers
                                            
                                            
                                                2
                                                Yes, patient has observable stasis ulcers ONLY
                                            
                                            
                                                3
                                                Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing/device)
                                                [ Go to M1340 ]
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1332) Current Number of Stasis Ulcer(s) that are Observable:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                One
                                            
                                            
                                                2
                                                Two
                                            
                                            
                                                3
                                                Three
                                            
                                            
                                                4
                                                Four or more
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1334) Status of Most Problematic Stasis Ulcer that is Observable:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                Fully granulating
                                            
                                            
                                                2
                                                Early/partial granulation
                                            
                                            
                                                3
                                                Not healing
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1340) Does this patient have a Surgical Wound?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No [Go to M1400]
                                            
                                            
                                                1
                                                Yes, patient has at least one observable surgical wound
                                            
                                            
                                                2
                                                
                                                    Surgical wound known but not observable due to non-removable dressing/device
                                                    [Go to M1400 ]
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1342) Status of Most Problematic Surgical Wound that is Observable
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Newly epithelialized
                                            
                                            
                                                1
                                                Fully granulating
                                            
                                            
                                                2
                                                Early/partial granulation
                                            
                                            
                                                3
                                                Not healing
                                            
                                        
                                    
                                
                            
                        

                        WOUND CARE (Check all that apply) Wound care done during this visit:   Yes  No Location(s) wound site:    textAreaAdjust(false,15016); Soiled dressing removed by:   Patient  Caregiver (name)     RN/PT  Other:   Technique:   Sterile  Clean  Wound cleaned with (specify):     textAreaAdjust(false,15018); Wound irrigated with (specify):     textAreaAdjust(false,15020); Wound packed with (specify):     textAreaAdjust(false,15022); Wound dressing applied (specify):      textAreaAdjust(false,15024);Patient tolerated procedure well:   Yes  NoComments:    DIABETIC FOOT EXAMFrequency of diabetic foot exam    Done by:  Patient RN/PT Caregiver (name)    Other:   Exam by clinician this visit:  Yes NoIntegument findings:    textAreaAdjust(false,7821);Pedal pulses:  Present  Right LeftAbsent  Right LeftComment:    textAreaAdjust(false,7830);Loss of sense of: Warm  Right LeftCold  Right LeftComment:    textAreaAdjust(false,7865);Neuropathy:  Right LeftTingling:  Right Left   Burning:  Right LeftLeg hair: Present   Right  Left Absent   Right Left Administered by:   WOUND / LESION#1 #2 #3 #4 #5 Date Reported:            Location           Type:  (e.g. Pressure ulcer,  Venous stasis ulcer,   Malignancy,  Malignancy, Diabetic foot ulcer)   textAreaAdjust(false,13421);  textAreaAdjust(false,13422);  textAreaAdjust(false,13423);  textAreaAdjust(false,13424);  textAreaAdjust(false,13425);Size (cm) (LxWxD)           Tunneling           Undermining (cm)           Stage (pressure ulcers only)           Odor           Surrounding Skin           Edema           Stoma           Appearance of the Wound Bed           Drainage / Amount  None None None None None Small Small Small Small Small Moderate Moderate Moderate Moderate Moderate Large Large Large Large LargeColor  Clear Clear Clear Clear Clear Tan Tan Tan Tan Tan Serosanguineous Serosanguineous Serosanguineous Serosanguineous Serosanguineous Other Other Other Other Other          Consistency  Thin Thin Thin Thin Thin Thick Thick Thick Thick Thick
                    

                
            

        
    




    
        
            RESPIRATORY STATUS
        
    
    

    
        

            
                
                


                    
                    
                        SYSTEMS REVIEWHeight:    reported actualWeight:    reported actualReported Weight Changes:  Gain Loss     lb.  X     wk.  mo.  yr. VITAL SIGNSBlood Pressure: Left Right Lying Sitting Standing At rest           With Activity           Post Activity           Temperature:    Oral Axillary Rectal TympanicPulse:  Regular Irregular Apical    Brachial    Rest Activity Radial    Carotid   Respirations:     Regular Irregular Cheynes Stokes Death rattle Rest  Activity Accessory muscles used Apnea periods    sec.  ( observed reported) Smoker Non-smokerLast smoked:    CARDIOPULMONARY No ProblemDisorders of heart / respiratory system:   textAreaAdjust(false,12175);Breath Sounds: (Clear, crankles/rales, wheezes/rhonchi, diminished, absent Aterior: Right     Left:   Posterior: Right Upper     Left Upper   Right Lower     Left Lower   O2 @      LPM via    cannula   mask  trach O2 Saturation     % Trach type/size:   Who manages?  Self Caregiver/family RN Other:   Intermittent treatments (C&amp;DB, medicated inhalation treatments, etc.)  Yes NoExplain:   textAreaAdjust(false,8096); Cough: No Yes: Productive Non-productiveDescribe:   textAreaAdjust(false,8151); Dyspnea: Rest During ADL&quot; , &quot;'&quot; , &quot;sComments:   textAreaAdjust(false,8102);Positioning necessary for improved breathing:  No Yes, describe   textAreaAdjust(false,8106);Heart Sounds:  Regular Irregular Murmur Pacemaker:Date   Type   Date last checked  Chest pain:  Dull Ache Sharp Vise-like Radiating Anginal Postural Localized SubsternalAssociated with:  Shortness of breath Activity SweatsFrequency/duration:   How relieved:   textAreaAdjust(false,8130); Palpitations Fatigue Edema: Pedal Right Pedal Left Sacral Dependent:    Pitting +1 +2 +3 +4 Non-pittingSite:    Cramps Claudication Capillary refill less than 3 sec Capillary refill greater than 3 sec Disease Management Problems (explain)  

                        
                            
                                
                                    
                                        (M1400) When is the patient dyspneic or noticeably Short of Breath?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient is not short of breath
                                            
                                            
                                                1
                                                When walking more than 20 feet, climbing stairs
                                            
                                            
                                                2
                                                With moderate exertion (for example, while dressing, using commode or bedpan, walking distances less than 20 feet)
                                            
                                            
                                                3
                                                With minimal exertion (for example, while eating, talking, or performing other ADLs) or with agitation
                                            
                                            
                                                4
                                                At rest (during day or night)
                                            
                                        
                                    
                                
                            
                        

                         (For M1400):   Assessed ReportedENDOCRINE STATUS A1c    %   Today&quot; , &quot;'&quot; , &quot;s visit  Patient reported   Lab slipDate: BS    mg/dL Date/Time:     FBS  Before Meal  Postprandial  Random HS   Blood sugar ranges       Patient/Family/Caregiver Report Monitored by:   Self Caregiver Family  Nurse Other   Frequency of monitoring   Competency with use of Glucometer    Disease Management Problems (explain)  
                    

                
            

        
    



    
        
            ELIMINATION STATUS
        
    
    

    
        

            
                
                


                    
                    

                    
                    
                        ELIMINATION STATUS No ProblemDisorders of Urinary system:    textAreaAdjust(false,13519);(Check all applicable items)  Urgency  Frequency  Burning  Pain  Hesitancy  Nocturia  Retention Hematuria Oliguria  Anuria  Incontinence (details if applicable)   textAreaAdjust(false,13532);  Diapers  Other    Color:   Yellow  Straw  Amber  Brown  Gray  Blood-tinged  Other:   Clarity:   Clear  Cloudy  Sediment  Mucous Odor:   Yes No  Reported ObservedUrinary Catheter:   Type    Date last changed  Foley inserted  (date)  with    French  Inflated balloon with    mL   without difficulty  Suprapubic Irrigation solution:   Type (specify):   Amount    mL  Frequency   Returns   Patient tolerated procedure well   Yes No Urostomy (describe skin around stoma):   textAreaAdjust(false,13577);Ostomy care managed by:  Self  Caregiver Family Disease Management Problems   BOWEL ELIMINATION No ProblemDisorders of GI system:    textAreaAdjust(false,12505); Flatulence Constipation Diarrhea Rectal bleeding Hemorrhoids Fecal Impaction Last BM  Bowel Sounds: active x    quadrants absent x    quadrants hypoactive x    quadrants hyperactive x    quadrants  Frequency of stools   Bowel regime/program:    textAreaAdjust(false,12497); Laxative/Enema use: Daily Weekly Monthly PRN Other:    Incontinence (details if possible)    Diapers  Other:     Ileostomy/colostomy site (describe skin around stoma):  textAreaAdjust(false,8457);Ostomy care managed by:   Self Caregiver Family Other  

                        
                            
                                
                                    
                                        (M1610) Urinary Incontinence or Urinary Catheter Presence:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No incontinence or catheter (includes anuria or ostomy for urinary drainage)
                                            
                                            
                                                1
                                                Patient is incontinent
                                            
                                            
                                                2
                                                Patient requires a urinary catheter (specifically: external, indwelling, intermittent,
                                                    suprapubic)
                                                
                                            
                                        
                                    
                                
                            
                        

                        NUTRITIONAL STATUS No Problem NAS NPO Controlled Carbohydrate  Other    Increase fluids    amt.    Restrict fluids    amt.  Appetite:  Good Fair Poor Anorexic Nausea/Vomiting:Frequency   Amount    Heartburn (food intolerance) Other    Directions: Check each box with &quot;yes&quot; to assessment, then total score to determine additional risk. YES Has an illness or condition that changed the kind and/or amount of food eaten.  2Eats fewer than 2 meals per day.  3Eats few fruits, vegetables or milk products.  2Has 3 or more drinks of beer, liquor or wine almost every day.   2Has tooth or mouth problems that make it hard to eat.  2Does not always have enough money to buy the food needed.  4Eats alone most of the time.  1Takes 3 or more different prescribed or over-the-counter drugs a day.  1Without wanting to, has lost or gained 10 pounds in the last 6 months.  2Not always physically able to shop, cook and/or feed self.  2Total:    Reprinted with permission by the Nutrition Screening Initiative, a project of the American Academy of   Family Physician, the American Dietetic Association and the National Council on the Aging Inc. and funded in part by grant from Ross Products Division, Abbott Laboratories Inc.    INTERPRETATION 0-2 Good.   As appropriate reassess and/or provide information based on situation.         3-5 Moderate risk.   Educate, refer, monitor and reevaluate based on patient situation and organization policy. 6 or more  High risk.   Coordinate with physician, dietitian, social service professional or nurse about how to  improve nutritional health. Reassess nutritional status and educate based on plan of care.    Describe at risk intervention and plan:   
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1620) Bowel Incontinence Frequency:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                NA
                                                
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Very rarely or never has bowel incontinence
                                            
                                            
                                                1
                                                Less than once weekly
                                            
                                            
                                                2
                                                One to three times weekly
                                            
                                            
                                                3
                                                Four to six times weekly
                                            
                                            
                                                4
                                                On a daily basis
                                            
                                            
                                                5
                                                More often than once daily
                                            
                                            
                                                NA
                                                Patient has ostomy for bowel elimination
                                            
                                            
                                        
                                    
                                
                            
                        

                        PSYCHOSOCIALPrimary language    Language barrier Needs interpreter    Learning barrier: Mental  Psychosocial  Physical  Functional  Unable to read/write Educational level     Spiritual/Cultural implications that impact care.Explain    textAreaAdjust(false,8891);Spiritual resource    textAreaAdjust(false,8893);Phone No.    Sleep/Rest: Adequate InadequateExplain    textAreaAdjust(false,13589); Inappropriate responses to caregivers/clinician Inappropriate follow-through in past Angry Flat affect Discouraged Withdrawn Difficulty coping Disorganized Depressed:  Recent  Long term Treatment:    textAreaAdjust(false,8906); Inability to cope with altered health status as evidenced by:  Lack of motivation  Unrealistic expectations  Inability to recognize problems Denial of problems Evidence of abuse/neglect/exploitation: Potential Actual  Verbal/Emotional Physical Financial InterventionDescribe:    textAreaAdjust(false,8923); Other (specify)   
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that
                                        (within the last 14 days): a) was related to an inpatient facility stay; or b) necessitated a change in medical or
                                        treatment regimen?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Patient does not have an ostomy for bowel elimination.
                                            
                                            
                                                1
                                                Patient&quot; , &quot;'&quot; , &quot;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.
                                                
                                            
                                            
                                                2
                                                The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.
                                            
                                        
                                    
                                
                            
                        

                        ABDOMEN No Problem  Tenderness Distention Soft Pain Hard Ascites Abdominal girth     cm   Other:   
                    

                
            

        
    




    
        
            ADL/IADLs
        
    
    

    
        

            
                
                


                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1800) Grooming: Current ability to tend safely to personal hygiene needs (specifically: washing face and hands, hair care, shaving or make up,
                                        teeth or denture care, fingernail care).
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to groom self unaided, with or without the use of assistive devices or adapted methods.
                                            
                                            
                                                1
                                                Grooming utensils must be placed within reach before able to complete grooming activities.
                                            
                                            
                                                2
                                                Someone must assist the patient to groom self.
                                            
                                            
                                                3
                                                Patient depends entirely upon someone else for grooming needs.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        MAHC 10 - Fall Risk Assessment ToolRequired Core ElementsAssess one point for each core element &quot;yes&quot;.Information may be gathered from medical record, assessment and if applicable, the patient/caregiver. Beyond protocols listed below, scoring should be based on your clinical judgment.  Points Age 65+   Diagnosis (3 or more co-existing)   Includes only documented medical diagnosis Prior history of falls within 3 months   An unintentional change in position resulting in coming to rest on the ground or at a lower level Incontinence   Inability to make it to the bathroom or commode in timely manner Includes frequency, urgency, and/or nocturia. Visual impairment   Includes but not limited to, macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription. Impaired functional mobility   May include patients who need help with IADLS or ADLS or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices. Environmental hazards   May include but not limited to, poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits. Poly Pharmacy (4 or more prescriptions – any type)   All PRESCRIPTIONS including prescriptions for OTC meds. Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs. Pain affecting level of function   Pain often affects an individual’s desire or ability to move or pain can be a factor in depression or ompliance with safety recommendations. Cognitive impairment   Could include patients with dementia, Alzheimer’s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patients ability to adhere to the plan of care. A score of 4 or more is considered at risk for falling Total   Missouri Alliance for HOME CARE2420 Hyde Park, Suite A, Jefferson City, MO 65109-4731  /  (573) 634-7772  /  (573) 634-4374 Fax MUSCULOSKELETAL No ProblemDisorders of musculoskeletal system:   textAreaAdjust(false,12747); Fracture (location)    Swollen, painful joints (specify)    Contractures: Joint    Location   Hand Grips:   Equal Unequal   Strong Weak  specify   Dominant side:   R L Motor Changes:  Fine Gross  specify    Weakness:  UE LE  details    Atrophy        Paresthesia      Shuffling Wild-based gait Amputation: BK  R  L  AK  R  L  UE R L Specify:    textAreaAdjust(false,9292); Other (specify)  textAreaAdjust(false,9309);How does the patient&quot; , &quot;'&quot; , &quot;s condition affect their function ability and safety?    MENTAL STATUSDescribe the mental status of the patient:  Include the patient&quot; , &quot;'&quot; , &quot;s general appearance, behaviors, emotional responses, mental functioning and their overall social interaction. Include both the clinical objective observations and subjective descriptions during this visit.   textAreaAdjust(false,15639);Was there a sudden/acute change in the patient&quot; , &quot;'&quot; , &quot;s mental status?  Yes  NoIf yes, did the change coincide with something else?  No Yes, explain:  textAreaAdjust(false,15647);Mental status changes reported by:   Patient  Representative Caregiver Other:     

                        
                            
                                
                                    
                                        (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts
                                        and blouses, managing zippers, buttons, and snaps
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.
                                            
                                            
                                                1
                                                Able to dress upper body without assistance if clothing is laid out or handed to the patient.
                                            
                                            
                                                2
                                                Someone must help the patient put on upper body clothing.
                                            
                                            
                                                3
                                                Patient depends entirely upon another person to dress the upper body.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to obtain, put on, and remove clothing and shoes without assistance.
                                            
                                            
                                                1
                                                Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.
                                            
                                            
                                                2
                                                Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.
                                            
                                            
                                                3
                                                Patient depends entirely upon another person to dress lower body.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to bathe self in shower or tub independently, including getting in and out of tub/shower.
                                            
                                            
                                                1
                                                With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.
                                            
                                            
                                                2
                                                
                                                    Able to bathe in shower or tub with the intermittent assistance of another person:
                                                    
                                                        (a) for intermittent supervision or encouragement or reminders, OR
                                                    
                                                    
                                                        (b) to get in and out of the shower or tub, OR
                                                    
                                                    
                                                        (c) for washing difficult to reach areas.
                                                    
                                                
                                            
                                            
                                                3
                                                Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.
                                            
                                            
                                                4
                                                Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.
                                            
                                            
                                                5
                                                
                                                    Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the
                                                    assistance or supervision of another person.
                                                
                                            
                                            
                                                6
                                                Unable to participate effectively in bathing and is bathed totally by another person.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to get to and from the toilet and transfer independently with or without a device.
                                            
                                            
                                                1
                                                When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.
                                            
                                            
                                                2
                                                Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).
                                            
                                            
                                                3
                                                Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.
                                            
                                            
                                                4
                                                Is totally dependent in toileting.
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    
                                        (M1850) Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently transfer.
                                            
                                            
                                                1
                                                Able to transfer with minimal human assistance or with use of an assistive device.
                                            
                                            
                                                2
                                                Able to bear weight and pivot during the transfer process but unable to transfer self.
                                            
                                            
                                                3
                                                Unable to transfer self and is unable to bear weight or pivot when transferred by another person.
                                            
                                            
                                                4
                                                Bedfast, unable to transfer but is able to turn and position self in bed.
                                            
                                            
                                                5
                                                Bedfast, unable to transfer and is unable to turn and position self.
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                    
                                        GG0170. Mobility: Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up code the reason.
                                    
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Coding:
                                            
                                            
                                                Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                                score according to amount of assistance provided.
                                            
                                            
                                                Activities may be completed with or without assistive devices.
                                            
                                            
                                                
                                                    06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                                
                                            
                                            
                                                
                                                    05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                                
                                            
                                            
                                                
                                                    04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as
                                                    patient completes activity. Assistance may be provided throughout the activity or intermittently.
                                                
                                            
                                            
                                                
                                                    03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides
                                                    less than half the effort.
                                                
                                            
                                            
                                                
                                                    02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than
                                                    half the effort.
                                                
                                            
                                            
                                                
                                                    01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more
                                                    helpers is required for the patient to complete the activity.
                                                
                                            
                                            
                                                If activity was not attempted, code reason:
                                            
                                            
                                                
                                                    07.    Patient refused
                                                
                                            
                                            
                                                
                                                    09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                                
                                            
                                            
                                                
                                                    10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                                
                                            
                                            
                                                
                                                    88.    Not attempted due to medical conditions or safety concerns
                                                
                                            
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                4. Follow-up Performance
                                                            
                                                        
                                                        
                                                            
                                                                Enter Codes in Boxes
                                                            
                                                        
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        A. Roll left and right: The ability to roll from lying on back to left and right side, and return to lying on back on the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        B. Sit to lying: The ability to move from sitting on side of bed to lying flat on the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        C. Lying to sitting on side of bed: The ability to move from lying on the back to sitting on the side of the bed with feet flat on the floor, and with no back support
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        D. Sit to stand: The ability to come to a standing position from sitting in a chair, wheelchair, or on the side of the bed.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        E. Chair/bed-to-chair transfer: The ability to transfer to and from a bed to a chair (or wheelchair).
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        F. Toilet transfer: The ability to get on and off a toilet or commode.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        I. Walk 10 feet: Once standing, the ability to walk at least 10 feet in a room, corridor, or similar space.
                                                        If Follow-up performance is coded 07, 09, 10 or 88 → skip to GG0170M, 1 step (curb)
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        J. Walk 50 feet with two turns: Once standing, the ability to walk 50 feet and make two turns.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        L. Walking 10 feet on uneven surfaces: The ability to walk 10 feet on uneven or sloping surfaces (indoor or outdoor), such as turf or gravel.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        M. 1 step (curb): The ability to go up and down a curb and/or up and down one step.
                                                        If Follow-up performance is coded 07, 09, 10 or 88, skip to GG0170Q, Does patient use wheelchair and/or scooter?
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        N. 4 steps: The ability to go up and down four steps with or without a rail.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    
                                                                
                                                            
                                                            
                                                                
                                                                    Q. Does patient use wheelchair/scooter?
                                                                
                                                                
                                                                    
                                                                                  0. No → Skip GG0170R
                                                                    
                                                                    
                                                                                  1. Yes → Continue to GG0170R, Wheel 50 feet with two turns.
                                                                    
                                                                
                                                            
                                                        

                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        R. Wheel 50 feet with two turns: Once seated in wheelchair/scooter, the ability to wheel at least 50 feet and make two turns.
                                                    
                                                
                                            
                                        
                                        
                                    
                                

                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (specifically: needs no human assistance or assistive device).
                                            
                                            
                                                1
                                                With the use of a one-handed device (for example, cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.
                                            
                                            
                                                2
                                                Requires use of a two-handed device (for example, walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.
                                            
                                            
                                                3
                                                Able to walk only with the supervision or assistance of another person at all times.
                                            
                                            
                                                4
                                                Chairfast, unable to ambulate but is able to wheel self independently.
                                            
                                            
                                                5
                                                Chairfast, unable to ambulate and is unable to wheel self.
                                            
                                            
                                                6
                                                Bedfast, unable to ambulate or be up in a chair.
                                            
                                        
                                    
                                
                            
                        

                         Indications for Home Health Aides:   Yes  No  RefusedReferred to HHA:   Yes No Reason for need:     
                    

                    
                    
                        

                        
                        
                            
                                
                                    
                                    
                                        GG0130. Self-Care: Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up, code the reason.
                                    
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                Coding:
                                            
                                            
                                                Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                                score according to amount of assistance provided.
                                            
                                            
                                                Activities may be completed with or without assistive devices.
                                            
                                            
                                                
                                                    06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                                
                                            
                                            
                                                
                                                    05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                                
                                            
                                            
                                                
                                                    04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as patient completes activity.
                                                    Assistance may be provided throughout the activity or intermittently.
                                                
                                            
                                            
                                                
                                                    03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides less than half the effort.
                                                
                                            
                                            
                                                
                                                    02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than half the effort.
                                                
                                            
                                            
                                                
                                                    01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more helpers is required for the patient to complete the activity.
                                                
                                            
                                            
                                                If activity was not attempted, code reason:
                                            
                                            
                                                
                                                    07.    Patient refused
                                                
                                            
                                            
                                                
                                                    09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                                
                                            
                                            
                                                
                                                    10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                                
                                            
                                            
                                                
                                                    88.    Not attempted due to medical conditions or safety concerns
                                                
                                            
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                4. Follow-up Performance
                                                            
                                                        
                                                        
                                                            
                                                                
                                                                    Enter Codes in Boxes
                                                                
                                                            
                                                        
                                                    
                                                    
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        A. Eating: The ability to use suitable utensils to bring food and/or liquid to the mouth and swallow food and/or liquid once the meal is placed before the patient.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        B. Oral Hygiene: 
                                                        The ability to use suitable items to clean teeth. Dentures (if applicable): The ability to insert and remove dentures into and from the mouth, and manage denture soaking and rinsing with use of equipment.
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        C. Toileting Hygiene: The ability to maintain perineal hygiene, adjust clothes before and after voiding or having a bowel movement. If managing an ostomy, include wiping the opening but not managing equipment.
                                                    
                                                
                                            
                                        
                                        
                                    
                                

                            
                        

                        
                    


                    
                    

                    
                    

                
            

        
    



    
        
            MEDICATIONS
        
    
    

    
        

            
                
                

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M2030) Management of Injectable Medications: Patient&quot; , &quot;'&quot; , &quot;s current ability to prepare and take all prescribed injectable medications reliably and safely,
                                        including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                2
                                                3
                                                NA
                                                =
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                Able to independently take the correct medication(s) and proper dosage(s) at the correct times.
                                            
                                            
                                                1
                                                
                                                    Able to take injectable medication(s) at the correct times if:
                                                    
                                                        (a) individual syringes are prepared in advance by another person; OR
                                                    
                                                    
                                                        (b) another person develops a drug diary or chart.
                                                    
                                                
                                            
                                            
                                                2
                                                Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection
                                            
                                            
                                                3
                                                Unable to take injectable medication unless administered by another person.
                                            
                                            
                                                NA
                                                No injectable medications prescribed.
                                            
                                        
                                    
                                
                            
                        

                        INFUSION N/A Central line  Peripheral line  Midline catheter Type/brand   Size/gauge/length    Groshong  Non-groshong  Tunneled  Non-tunneledInsertion site    Insertion date Lumens:   Single  Double  Triple Flush solution/frequency   Patient:  Yes  No Injection cap change frequency   Dressing change frequency    Sterile  CleanPerformed by:  Self Family  Caregiver  RN  Other    Site/skin condition   External catheter length   Other:   textAreaAdjust(false,10021);PICC Specific: Circumference of arm   X-ray verification   Yes  No IVAD Port Specific: Reservoir:   Single	 DoubleHuber gauge/length   Accessed:   No  Yes, date   Epidural/Inrathecal Access: Site/skin condition:   Infusion solution (type/volume/rate)    Pump: (type, specify)    Purpose of Intravenous:    textAreaAdjust(false,10040); Medication(s) administered: (name of drug)   Dose    Route   Frequency    Duration of therapy    Medication(s) administered: (name of drug)   Dose    Route   Frequency    Duration of therapy   Administered by:   Self Family  Caregiver  Other   Purpose of Intravenous Access:  Antibiotic therapy  Pain control   Lab draws Chemotherapy		 Main venous access  Hydration		 Parental nutrition Other    Infusion care provided during visit Yes  No Interventions/Instructions/Comments    Psychotropic drug use:   No  Yes Ability to pay for medications:   Yes  No If No, was MSW referral made?   Yes  No Comment   
                    

                
            

        
    





    
        
            THERAPY NEED AND PLAN OF CARE
        
    
    

    
        

            
                
                
                   
                    
                    
                        STRENGTHS/LIMITATIONSBased upon the patient&quot; , &quot;'&quot; , &quot;s Physical, Psychosocial, Cognitive and Mental Status: List the patient&quot; , &quot;'&quot; , &quot;s strengths that contribute to them meeting their goal(s), both personal and the HHA measurable goals.   textAreaAdjust(false,15786);List the patient&quot; , &quot;'&quot; , &quot;s limitations that might challenge progress toward their goal(s), both personal and the HHA measurable goal.    textAreaAdjust(false,15789);List any diagnosed permanent disability/impairment of the patient:   textAreaAdjust(false,15791);How might the patient&quot; , &quot;'&quot; , &quot;s limitation(s) affect their safety and/or progress?    textAreaAdjust(false,15793);What is the HHA doing/implementing to mitigate the patient&quot; , &quot;'&quot; , &quot;s limitations?    textAreaAdjust(false,15795);Has anything significant changed since the last visit?   No Yes, explain:   REFUSED CARES Patient  Representative Other:    refuse   Cares  Services since the last assessment?  No  Yes, explain  textAreaAdjust(false,15880);Are the  cares  service(s) they refused a significant part of the recommended plan of care? No Yes, explain how  RISK FACTORS/HOSPITAL ADMISSION/EMERGENCY ROOM Not ApplicableRisk factors identified and followed up on by:   Training Discussion EducationLiterature given to:   Patient  Representative Caregiver Family Other:    List identified risk factors the patient has related to an unplanned hospital admission or an emergency department visit    

                        
                            
                                
                                    
                                        (M2200) Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, 
                                        what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)?
                                        (Enter zero [“000”] if no therapy visits indicated.)
                                    
                                    
                                
                                
                                    
                                        
                                            
                                                
                                            
                                        
                                        
                                            Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined).
                                        
                                    
                                
                                
                                    
                                         NA - Not Applicable: No case mix group defined by this assessment.
                                    
                                
                            
                        

                        PATIENT/CAREGIVER EDUCATIONPatient/caregiver/family knowledgeable and able to    verbalize and/or demonstrate independence with:  Wound care:   Yes  No  N/ACatheter care:   Yes  No  N/ADiabetic foot exam/care:   Yes  No  N/ANutritional management:   Yes  No  N/AInsulin administration:   Yes  No  N/APain management:  Yes  No  N/AGlucometer use:   Yes  No  N/AOxygen use:  Yes  No  N/AOral medication(s) administration:	  Yes  No  N/AUse of medical devices:  Yes  No  N/AInjected medication(s) administration:  Yes  No  N/ATrach care:   Yes  No  N/AInfused medication(s) administration:  Yes  No  N/AOstomy care:   Yes  No  N/AInhaled medication(s) administration:  Yes  No  N/AOther care(s):      Yes  No  N/APatient/caregiver/family need further education with:    textAreaAdjust(false,13006);Caregiver/family present at time of visit:   Yes  No Patient/caregiver/family educated this visit for:    textAreaAdjust(false,10789);Patient/caregiver appears to understand all information given:  Yes  No Does the patient/caregive/family have an action plan?   Yes  No Comments:    SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - NURSING INTERVENTIONS/INSTRUCTIONS Skilled observation &amp; assessment    Teach   Admin.   IVs  Clysis   Diabetic observation   Post-partum assessment  Prenatal Assessment  Teach   infant  child care  Foley care  Teach   ostomy  ileo. conduit care  Teach diabetic care   Wound   care  dressing  Teach   Admin. tube feedings  Observe   Teach medication  Decubitis care   Teach   Admin. care of trach  N   or  C  Venipuncture   Teach care - terminally ill   effects  side effects  Change   NG  G tube  IM injection   SQ injection  Physiology    Disease process teaching  Admin. of vitamin B2   Psych. intervention   Diet teaching   Prep.   Admin. insulin  Observe S/S infection   Safety factors   Fall Safety Teaching   Pain Management   Other:   SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - PHYSICAL THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation   Balance training/activities    Teach hip precautions   Establish upgrade home exercise program   Pulmonary Physical Training  Teach safe/effective use of adaptive/assist device (specify)   Copy given to patient  Ultrasound     Copy attached to chart  Electrotherapy   Teach safe stair climbing skills  Patient/Family education   Prosthetic training   Teach fall safety   Therapeutic exercise   TENS  Other:   Transfer training   Functional mobility training    textAreaAdjust(false,10565); Gait training   Teach bed mobility skills  SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - OCCUPATIONAL THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation    Neuro-developmental training    Therapeutic exercise to right/left hand   Establish home exercise program   Sensory treatment  to increase strength, coordination,   Copy given to patient Orthotics  Splinting sensation and proprioception   Copy attached to chart  Adaptive equipment (fabrication   Fine motor coordination   Patient/Family education  and training)   Perceptual motor training   Independent living/ADL training    Teach alternative bathing skills   Teach fall safety   Muscle re-education  (unable to use tub/shower safely)  Retraining of cognitive,  Other:   feeding and perpetual skills    SKILLED INTERVENTIONS/INSTRUCTIONS DONE THIS VISIT - SPEECH THERAPY INTERVENTIONS/INSTRUCTIONS Evaluation   Dysphagia treatments    Safe swallowing evaluation   Establish home maintenance program   Language disorders   Speech dysphagia instruction program   Copy given to patient  Aural rehabilitation   Teach/Develop communication system   Copy attached to chart  Non-oral communication  Other:   Patient/Family education   Alaryngeal speech skill    textAreaAdjust(false,10651); Voice disorders   Language processing   Speech articulation disorders   Food texture recommendations  SKILLED CARE PROVIDED THIS VISIT  SUMMARY CHECKLIST CARE PLAN:  Reviewed with patient/caregiver/family involvementMEDICATION STATUS:  Medication Regimen completed/reviewed  No change Order obtainedCheck if any of the following were identified:    Potential adverse effects/drug reactions Ineffective drug therapy Significant side effects  Significant drug interactions Duplicate drug therapy Non-compliance with drug therapyCARE COORDINATION:  Physician  SN  PT  OT  ST  MSW  Aide  Other (specify)   Was a referral made to MSW for assistance with community resources/assistance with a living will/counseling needs (depression/suicidal ideation) and/or unsafe environment?  Date   Yes  No  Refused  N/A   Comment    Verbal Order obtained:  No  Yes, specify date   AMPLIFICATION OF CARE PROVIDED / ANALYSIS OF FINDINGS  textAreaAdjust(false,10671);Patient/Caregiver Response   HOMEBOUND REASON Needs assistance for all activities  Confusion, unable to go out of home alone Residual weakness Unable to safely leave home unassisted Requires assistance to ambulate Severe SOB, SOB upon exertion Dependent upon adaptive device(s) Medical restrictions Other (specify)   SUPERVISORY VISIT:   Yes  No  Scheduled  UnscheduledSTAFF:   Present  Not Present NEXT SCHEDULED SUPERVISORY VISIT CARE PLAN UPDATED?   Yes  NoCARE PLAN FOLLOWED ?  Yes  No (explain)   textAreaAdjust(false,10698);OBSERVATION OF    textAreaAdjust(false,10700);TEACHING/TRAINING OF    textAreaAdjust(false,10702);IS PATIENT/FAMILY SATISFIED WITH CARE?   Yes  No (explain)   CARE SUMMARY Dear Doctor   This Care Summary page is your   Recertification (follow-up) Other Follow-upThank you for allowing us to care for your patient. Disciplines Involved Comments  SN  textAreaAdjust(false,14055); PT  textAreaAdjust(false,14056); OT  textAreaAdjust(false,14057); ST  textAreaAdjust(false,14058); MSW  textAreaAdjust(false,14059); Aide      textAreaAdjust(false,14060);Date of last home visit Physician notified:  Yes No POT485 attached for signature. Please sign and return. Copy of Care Summary  sent faxed Date: SUMMARY Complete this Section for Recertification (Unless Summary is written elsewhere) REASON FOR ADMISSION (describe condition)   textAreaAdjust(false,14073);SUMMARY OF CARE (Including progress toward goals to date)   textAreaAdjust(false,14074);DISCHARGE PLANNING (specify future follow-up, referrals, etc.)   DME/MEDICAL SUPPLIES DME Company:    Phone:   Oxygen Company:    Phone:   Community org./services:    Contact:   Phone:   Comments:    ASSESSMENT SUMMARY Reason for Admission:    Homebound Reason:      Any changes since last assessment:   
                    

                    
                    

                
            

        
    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                

                
                    
                        Hide Validation Results
                    
                
            
        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    
        
            
                
                    M-ITEM
                    QUESTION
                    ERROR MESSAGE
                    SEVERITY
                
            
            
                
                    
                        
                    
                    
                         () Unable to Genarate HIPPS CODE:
                    
                    
                        
                    
                    
                        
                            ERROR
                        
                        
                    
                
                    
                        M1021
                    
                    
                        a. Primary Diagnosis 
                    
                    
                        The Primary Diagnosis N19 is not billable under PDGM.
                    
                    
                        
                            ERROR
                        
                        
                    
                
            
        
    

    

    
        Oasis Audits
        
            
                
                    MOSET
                    PREVIOUS
                    CURRENT
                    STATUS
                
            
            
                
                    
                        M1020_PRIMARY_DIAG_ICD
                    
                    
                        B77.81  
                    
                    
                        N19     
                    
                    
                        
                            Changed
                        
                    
                
                    
                        M1020_PRIMARY_DIAG_SEVERITY
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1024_OTH_DIAG1_ICD
                    
                    
                        J60     
                    
                    
                        G60.0   
                    
                    
                        
                            Changed
                        
                    
                
                    
                        M1024_OTH_DIAG1_SEVERITY
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1024_OTH_DIAG2_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG2_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG3_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG3_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG4_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG4_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG5_ICD
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_OTH_DIAG5_SEVERITY
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_A3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_C3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_D3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_E3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_F3
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_A4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_B4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_C4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_D4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_E4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1024_PMT_DIAG_ICD_F4
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1030_THH
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1200_VISION
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1242_PAIN_FREQ_ACTVTY_MVMT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1306_UNHLD_STG2_PRSR_ULCR
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG2
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG2_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG3
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG3_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NBR_PRSULC_STG4
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NBR_STG4_AT_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_DRSG
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DRSG_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_CVRG
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_CVRG_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE
                    
                    
                        1
                    
                    
                        1
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1311_NSTG_DEEP_TISUE_SOC_ROC
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1322_NBR_PRSULC_STG1
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1324_STG_PRBLM_ULCER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1330_STAS_ULCR_PRSNT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1332_NUM_STAS_ULCR
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1334_STUS_PRBLM_STAS_ULCR
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1340_SRGCL_WND_PRSNT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1342_STUS_PRBLM_SRGCL_WND
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1350_LESION_OPEN_WND
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
                    
                        M1400_WHEN_DYSPNEIC
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1610_UR_INCONT
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1620_BWL_INCONT
                    
                    
                        00
                    
                    
                        00
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1630_OSTOMY
                    
                    
                        00
                    
                    
                        00
                    
                    
                        
                            Unchanged
                        
                    
                
                    
                        M1810_CUR_DRESS_UPPER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1820_CUR_DRESS_LOWER
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1830_CRNT_BATHG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1840_CUR_TOILTG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1850_CUR_TRNSFRNG
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1860_CRNT_AMBLTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M1860_CRNT_AMBLTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2030_CRNT_MGMT_INJCTN_MDCTN
                    
                    
                        01
                    
                    
                        01
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2200_THER_NEED_NUM
                    
                    
                        8
                    
                    
                        8
                    
                    
                        
                            Stabilized
                        
                    
                
                    
                        M2200_THER_NEED_NA
                    
                    
                        
                    
                    
                        
                    
                    
                        
                            N/A
                        
                    
                
            
        
        
    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

&quot;))]</value>
      <webElementGuid>57a19533-7572-4a71-b76c-91dad193ee90</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
