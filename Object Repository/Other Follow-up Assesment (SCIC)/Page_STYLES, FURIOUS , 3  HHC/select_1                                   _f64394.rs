<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _f64394</name>
   <tag></tag>
   <elementGuidId>d329e9a3-5d37-4d0a-92d7-a7398d299b7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1334 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1334']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>98f1a8a5-616a-4755-a93d-6d8e498a45ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>00cf3adb-efa3-4af9-b83a-b6b7af771994</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1334_STUS_PRBLM_STAS_ULCR</value>
      <webElementGuid>233580f1-a02b-460b-a07e-3a4ad0bb7ccf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1334_STUS_PRBLM_STAS_ULCR</value>
      <webElementGuid>11a72091-669d-4e47-b111-6d020315bd29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1334_STUS_PRBLM_STAS_ULCR', oasis.oasisDetails.M1334_STUS_PRBLM_STAS_ULCR)</value>
      <webElementGuid>a785383f-32dc-4bfe-81c2-71ef68d26a84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1330_STAS_ULCR_PRSNT == '00' || oasis.oasisDetails.M1330_STAS_ULCR_PRSNT == '03' || oasis.isLocked</value>
      <webElementGuid>3d2a293e-cadd-424e-ac07-d304740a7607</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                1
                                                2
                                                3
                                                =
                                            </value>
      <webElementGuid>64fb51a2-7614-4e48-b134-7b3e8c38f344</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1334&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>95f5116b-a1d3-4e0e-b9d7-2f8af9605d48</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1334']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>ebbb7205-b15a-411a-984f-6b3189e0c95f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[12]/following::select[1]</value>
      <webElementGuid>b7685b88-6700-45cf-8e2e-fbc230048983</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M1334) Status of Most Problematic Stasis Ulcer that is Observable:'])[1]/following::select[1]</value>
      <webElementGuid>f76c54e2-7d03-4bc5-a7e6-945ccc897dc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fully granulating'])[2]/preceding::select[1]</value>
      <webElementGuid>40b6927d-31c1-4e10-8129-b450ac9d331d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Early/partial granulation'])[1]/preceding::select[1]</value>
      <webElementGuid>f65be3bc-5014-4692-9484-6c6f1cafd5c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[5]/div/div[2]/div/div/div[2]/div[7]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>2c911e4d-7972-43f8-b64a-d418bceafcaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                1
                                                2
                                                3
                                                =
                                            ' or . = '
                                                
                                                1
                                                2
                                                3
                                                =
                                            ')]</value>
      <webElementGuid>1d0824ba-1335-4243-8e09-b06298a4a851</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
