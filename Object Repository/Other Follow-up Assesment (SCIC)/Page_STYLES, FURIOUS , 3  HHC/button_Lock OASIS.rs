<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Lock OASIS</name>
   <tag></tag>
   <elementGuidId>36f2b007-b42d-49b2-a658-2af37bba74aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-dialog.modal-sm > div.modal-content > div.modal-footer > button.btn.btn-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[168]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>860dd6b6-7d3a-4aa1-b9ac-f707e61d30b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a8f38a0e-3487-402a-8fe8-6ec1071a4f33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>lockOasis(lockDate)</value>
      <webElementGuid>c579dc78-0b50-4099-a3bb-9a49d3ba78be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-success</value>
      <webElementGuid>48032a1e-0c16-4dbd-810b-199821422b69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lock OASIS</value>
      <webElementGuid>09f0bd47-4470-4e69-a6c7-36a8ca6c939b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer&quot;]/button[@class=&quot;btn btn-success&quot;]</value>
      <webElementGuid>fa313998-3235-4c1a-a936-29ef29e534e0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[168]</value>
      <webElementGuid>af6300a6-4da3-400e-b869-c8d506b25942</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[1]/following::button[1]</value>
      <webElementGuid>09656942-8a56-4c93-bfb6-f2728585e5be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Oasis'])[1]/following::button[1]</value>
      <webElementGuid>7069fbbd-1cd7-40c7-8147-fd5c23815870</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/preceding::button[1]</value>
      <webElementGuid>ef20552f-af61-43d8-a6df-c9d524ee7c12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lock OASIS']/parent::*</value>
      <webElementGuid>650aeafb-8e79-410f-b65f-f53720bda95a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div[4]/button</value>
      <webElementGuid>1f4eaa44-7503-40d5-8406-ed3b9d149ddb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Lock OASIS' or . = 'Lock OASIS')]</value>
      <webElementGuid>15f17418-ebca-4693-9f5a-25424f6ec264</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
