<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_---                                (_ce7194</name>
   <tag></tag>
   <elementGuidId>77b44c27-734a-499d-9b55-000e44373fa8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlChangeLocation']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ddlChangeLocation</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>136bf548-9746-4bd2-8794-d4145c92539d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>7</value>
      <webElementGuid>77aaf4a0-896a-4aae-b338-c80575756679</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>163cc480-dd4e-4aba-afaa-03fd95a8f4ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlChangeLocation</value>
      <webElementGuid>c3119352-8dee-43cf-b0a2-2ec35a6b57fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>ServiceLocation</value>
      <webElementGuid>daa9ffca-76b1-45e2-aca8-0280e03715dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>isDisabled || Pagefields.CalendarChangeCareStaff.AccessType == 1</value>
      <webElementGuid>142ec92a-72b8-4a7f-a26d-2b40b2ded80c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            </value>
      <webElementGuid>3b192b37-95e5-474a-a50e-001ab1be5c1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlChangeLocation&quot;)</value>
      <webElementGuid>7e6c88de-205f-4e19-b950-400a7d1c80bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlChangeLocation']</value>
      <webElementGuid>1a9122ab-a330-4c73-92e4-400d1fd5f23f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Location'])[3]/following::select[1]</value>
      <webElementGuid>ba76431a-78ae-4e01-91e4-3e67a43a97f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Module:'])[1]/following::select[1]</value>
      <webElementGuid>80d812cc-3adb-4dea-bb4b-ee0c55bdf283</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Military time format)'])[2]/preceding::select[1]</value>
      <webElementGuid>8190fb17-4d22-4844-9044-5f3a4900958f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div[2]/select</value>
      <webElementGuid>e29238fa-dd7a-4d84-bcf8-066fa9a84314</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlChangeLocation' and (text() = '
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            ' or . = '
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            ')]</value>
      <webElementGuid>d2f0be08-64ae-4860-a089-96cc79ea8784</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
