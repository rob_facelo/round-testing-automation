<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Select Discipline--               _075cf9</name>
   <tag></tag>
   <elementGuidId>761cb9df-4501-4d9e-a6da-14815468e7ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlCaregiverType']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ddlCaregiverType</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>42730382-893f-4f73-872a-5ef5e661f85e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm fontsize-13-weight-600 ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>401bd786-cd9d-45ee-9d66-f23e16c4d867</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlCaregiverType</value>
      <webElementGuid>11dba7e3-d93b-488d-8a85-f9968e3973b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>plotVisit.OnCgTypeChange()</value>
      <webElementGuid>ef10cbdb-89cf-402f-95a9-bafdc85e72b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>plotVisit.SelectedCgType</value>
      <webElementGuid>0e6da94d-4d93-4c73-bd79-111e374c8fef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                </value>
      <webElementGuid>e536d17d-818b-4df3-a5b0-676da7f2a591</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlCaregiverType&quot;)</value>
      <webElementGuid>4c5ecf6b-c655-41e3-83c5-b94da0217951</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlCaregiverType']</value>
      <webElementGuid>cb31a06d-c8d6-44af-b71f-9cac54a58adb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>2b1960e3-f547-4e77-8404-20ad6de0d0f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff Type'])[1]/following::select[1]</value>
      <webElementGuid>84247350-af5d-4b38-886c-9950bb0df003</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payer'])[1]/following::select[2]</value>
      <webElementGuid>5d5191d1-3dd3-434f-bcc1-7e4b13e34ee8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff'])[2]/preceding::select[1]</value>
      <webElementGuid>e4c8ef92-fe4c-4ca3-a0dd-08319d4b224a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Type'])[1]/preceding::select[2]</value>
      <webElementGuid>464a8143-7974-4b1d-94f5-4ce90e242980</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>14205496-2234-41b9-b4ac-97fc2c9c513d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlCaregiverType' and (text() = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ' or . = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ')]</value>
      <webElementGuid>f723bf74-23ca-4812-a655-13006141c0c3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
