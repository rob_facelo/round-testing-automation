<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_View Visit Calendar</name>
   <tag></tag>
   <elementGuidId>1f774c3f-3b23-4942-8b3e-a9371d380133</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div/div[2]/div[3]/div[2]/div/div/div/div/span/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-default.ng-scope</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1b17a1ea-35d6-4343-a000-2177994b314c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>isParentSummary &amp;&amp; !cl.isPtoPage &amp;&amp; !cl.isChartView &amp;&amp; cl.episode_id</value>
      <webElementGuid>16309b93-2298-4630-b5b7-a44d456e39be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/patient/patientvisitcalendarv2.aspx?patientId=12&amp;episodeId=7716&amp;patintakeId=55855</value>
      <webElementGuid>ba0eee6f-4438-465b-bece-ebc71ccfb3d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default ng-scope</value>
      <webElementGuid>cbc2136e-813c-4865-b494-ba9f558911ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>  View Visit Calendar</value>
      <webElementGuid>154dda03-1241-4ad1-86c0-2295d69525f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;margin-bottom-10px&quot;]/div[@class=&quot;MainPanel displayNone&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[@class=&quot;row margin-top-sm ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-3 col-sm-3 col-md-3 col-lg-3&quot;]/span[@class=&quot;pull-left&quot;]/div[@class=&quot;btn-group&quot;]/a[@class=&quot;btn btn-default ng-scope&quot;]</value>
      <webElementGuid>3b2e279c-d7a6-4f2f-8584-228de7388945</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div/div[2]/div[3]/div[2]/div/div/div/div/span/div/a</value>
      <webElementGuid>6bdaf67d-2f5e-4a55-8965-9a1d2ef7ced8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'View Visit Calendar')]</value>
      <webElementGuid>f08f99f9-57df-4d36-9579-ef0ef7438b8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[3]/following::a[1]</value>
      <webElementGuid>ba6ddbfc-9863-4417-9ca3-2c7d1ab61ed5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::a[1]</value>
      <webElementGuid>fc260731-73c3-4131-b392-d94fc1fe4bbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show Legend'])[1]/preceding::a[1]</value>
      <webElementGuid>8de50aef-8fff-43d0-8618-e60ed6ed5934</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='WEEK'])[1]/preceding::a[1]</value>
      <webElementGuid>22539ee5-4084-4289-8265-ba9d3d08e1b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/patient/patientvisitcalendarv2.aspx?patientId=12&amp;episodeId=7716&amp;patintakeId=55855')]</value>
      <webElementGuid>36a1d35b-e8ef-4598-9d7e-9def9a3f0884</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div/a</value>
      <webElementGuid>986adcab-7bbf-4361-8f46-3858bd7ef07b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/patient/patientvisitcalendarv2.aspx?patientId=12&amp;episodeId=7716&amp;patintakeId=55855' and (text() = '  View Visit Calendar' or . = '  View Visit Calendar')]</value>
      <webElementGuid>6fc946a4-ffd9-4d22-bae4-66e102de3fcc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
