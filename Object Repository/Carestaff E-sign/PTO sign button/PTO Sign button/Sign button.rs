<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Sign button</name>
   <tag></tag>
   <elementGuidId>20f35d5a-118e-4fd9-82e1-296aa3b62369</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='esigntemplatemodal']/div/div/div[3]/div/div/div[2]/button/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;esigntemplatemodal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer esigfooter&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-2 col-lg-2&quot;]/button[@class=&quot;btn btn-default pull-left esignbtn-holder&quot;]/i[@class=&quot;glyphicon glyphicon-pencil&quot;][count(. | //*[@class = 'glyphicon glyphicon-pencil']) = count(//*[@class = 'glyphicon glyphicon-pencil'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-default.pull-left.esignbtn-holder > i.glyphicon.glyphicon-pencil</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>6e510779-b383-49ab-8454-90985b0e4055</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-pencil</value>
      <webElementGuid>e20c5d7c-0b42-4f78-bcdf-440f99b678ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;esigntemplatemodal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer esigfooter&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-2 col-lg-2&quot;]/button[@class=&quot;btn btn-default pull-left esignbtn-holder&quot;]/i[@class=&quot;glyphicon glyphicon-pencil&quot;]</value>
      <webElementGuid>b7ff87c5-7b85-40c5-96ea-39663b514266</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='esigntemplatemodal']/div/div/div[3]/div/div/div[2]/button/i</value>
      <webElementGuid>53449593-7f62-42dc-9e59-edd0d61c5a2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/i</value>
      <webElementGuid>a5bff94e-f18b-467c-8f93-429cf13c48ba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
