<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Jul 29 2022 1128AM Electronically Sign</name>
   <tag></tag>
   <elementGuidId>e00ef611-a7fc-4205-9abc-0dc95e276a55</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@onclick='electronicallySignModal(&quot;Printing/EsignPreview/PatientPTOPreview.aspx?ptoId=76600&amp;patient_id=6&amp;intakeid=55849&amp;orderdate=07/29/2022&amp;orderdatetime=07/29/2022 11:28:00&amp;isphysician=false&quot;,&quot;PTO&quot;)']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2 > td.dxgv > div.esigpopupcolumnholder</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0dacafcb-170e-4a01-b183-4142ff603199</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>esigpopupcolumnholder</value>
      <webElementGuid>0f3da6cd-4c01-4d9c-8526-a19b82b8788b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>electronicallySignModal(&quot;Printing/EsignPreview/PatientPTOPreview.aspx?ptoId=76600&amp;patient_id=6&amp;intakeid=55849&amp;orderdate=07/29/2022&amp;orderdatetime=07/29/2022 11:28:00&amp;isphysician=false&quot;,&quot;PTO&quot;)</value>
      <webElementGuid>9f0636ce-8073-4109-a9f1-81f24df44f37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jul 29 2022 11:28AM Electronically Sign</value>
      <webElementGuid>65e182b6-ddba-4689-8be0-e199027ef532</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]</value>
      <webElementGuid>9882b587-bda3-4168-9810-cc2edcac7ae5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@onclick='electronicallySignModal(&quot;Printing/EsignPreview/PatientPTOPreview.aspx?ptoId=76600&amp;patient_id=6&amp;intakeid=55849&amp;orderdate=07/29/2022&amp;orderdatetime=07/29/2022 11:28:00&amp;isphysician=false&quot;,&quot;PTO&quot;)']</value>
      <webElementGuid>18548b36-1288-4a31-93ac-b683e8cdaffd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2']/td[7]/div</value>
      <webElementGuid>a250d784-d343-493e-a2f3-aefd779672aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Strange, Stephen'])[3]/following::div[1]</value>
      <webElementGuid>ce530fbf-d655-475d-9099-6b4e25e4e365</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='STYLES, FURIOUS'])[13]/following::div[1]</value>
      <webElementGuid>0cad886b-8123-4b41-8532-c3d2d00ef767</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Resumption of Care'])[1]/preceding::div[1]</value>
      <webElementGuid>21849177-b203-46c6-bed6-dec78a9e367f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[7]/div</value>
      <webElementGuid>551b641e-e056-4311-8b9c-39e44e72597f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Jul 29 2022 11:28AM Electronically Sign' or . = 'Jul 29 2022 11:28AM Electronically Sign')]</value>
      <webElementGuid>f640a5f8-9113-4b98-b363-d516dd034165</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
