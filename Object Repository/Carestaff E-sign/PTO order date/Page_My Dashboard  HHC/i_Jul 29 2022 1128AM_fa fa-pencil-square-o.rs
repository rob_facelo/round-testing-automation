<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Jul 29 2022 1128AM_fa fa-pencil-square-o</name>
   <tag></tag>
   <elementGuidId>318d193b-213c-4e0b-81df-4a6c2df6455b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2']/td[7]/div/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;][count(. | //*[@class = 'fa fa-pencil-square-o']) = count(//*[@class = 'fa fa-pencil-square-o'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2 > td.dxgv > div.esigpopupcolumnholder > i.fa.fa-pencil-square-o</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>6aabbb49-1196-4a51-ab2d-3cc8f6955412</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-pencil-square-o</value>
      <webElementGuid>e81e9699-7769-4797-ba05-19b4b249a1cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;]</value>
      <webElementGuid>a0ac4fd8-8fec-4431-8219-54e5dfa862e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow2']/td[7]/div/i</value>
      <webElementGuid>9319d55d-48fe-40e1-97fb-f05d0acce951</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[7]/div/i</value>
      <webElementGuid>c1f1f137-238a-4dc4-a633-9e69d37e786d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
