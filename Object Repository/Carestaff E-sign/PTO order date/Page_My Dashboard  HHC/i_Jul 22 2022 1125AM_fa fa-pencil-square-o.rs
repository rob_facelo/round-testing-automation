<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Jul 22 2022 1125AM_fa fa-pencil-square-o</name>
   <tag></tag>
   <elementGuidId>8f1205ad-0237-4fbb-adac-82a33de950be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1']/td[7]/div/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;][count(. | //*[@class = 'fa fa-pencil-square-o']) = count(//*[@class = 'fa fa-pencil-square-o'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1 > td.dxgv > div.esigpopupcolumnholder > i.fa.fa-pencil-square-o</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>ac5279d3-684f-4376-b2aa-19bab2c74d40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-pencil-square-o</value>
      <webElementGuid>7f169611-71b5-4943-966d-36ea3f6a109c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;]</value>
      <webElementGuid>452fdb9e-adc3-4e81-b7af-796715dde49f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1']/td[7]/div/i</value>
      <webElementGuid>a1e6cccb-65d7-4ce4-8413-4ee23e4eaaf4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[7]/div/i</value>
      <webElementGuid>7eaf4632-977f-4ae3-aaf3-b06041944d6a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
