<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Jul  1 2022 1113AM_fa fa-pencil-square-o</name>
   <tag></tag>
   <elementGuidId>4806d31a-5b09-4b59-910e-84d3e519703d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow0']/td[7]/div/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-pencil-square-o</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>7818bd61-1352-4a24-a008-1efe3ed4c089</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-pencil-square-o</value>
      <webElementGuid>b038cc41-fc5c-4f94-84f8-7516bfa057fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow0&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;]</value>
      <webElementGuid>ebab0bbd-7f4c-4991-90bb-3f5be5ad2948</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow0']/td[7]/div/i</value>
      <webElementGuid>1ac44f02-45bf-49ce-ab8b-0c9214faccc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[7]/div/i</value>
      <webElementGuid>42efb0ad-475e-42fb-93e2-a01b4584c223</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
