<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Chat Support_searchKey</name>
   <tag></tag>
   <elementGuidId>a63ef357-2c2b-4b1d-993f-a9e937e9d4cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='searchKey']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#searchKey</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9ace4d70-0b8b-4fa8-b390-bcd5d4e7bbcf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>026f6e96-ccfb-48ac-8e5f-ff7e82aa0af1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>37749af6-bbd3-430a-bde2-3cf921243b9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control search-field-holder searchbox ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>5aa23a6f-2741-43dc-833d-e80089caf8b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search for patient...</value>
      <webElementGuid>bf7e90ad-eb15-462c-9958-60dbe4e788b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>ec8fdc7b-eab3-4158-81be-285fb8206264</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>search.startSearch()</value>
      <webElementGuid>33b4be1a-1fe2-4bf9-a078-098199ebbb67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>search.searchvalue</value>
      <webElementGuid>c36b8d9b-f835-444a-94af-09d9627e480b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-init</name>
      <type>Main</type>
      <value>search.searchvalue=''</value>
      <webElementGuid>038f0310-10be-4b32-9a51-01303983526e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 750}</value>
      <webElementGuid>019a1541-3a45-4bfe-8d0a-3e7e7a37ad57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>3a4d197e-a48b-4d5a-b65c-6ab03e9d425a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>50</value>
      <webElementGuid>c9b061b9-c798-4b97-ac39-2d0dcd00a4ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchKey&quot;)</value>
      <webElementGuid>e4518fbf-0024-45d0-a4cc-55b7fb46fdb3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='searchKey']</value>
      <webElementGuid>51965404-adae-4f94-ae84-9e458a303408</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/input[3]</value>
      <webElementGuid>9585f1cf-72d6-4c35-b7cd-acde13e2fa76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input[3]</value>
      <webElementGuid>01a3ee58-1fa8-4633-9d82-419a8bfeecd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'searchKey' and @id = 'searchKey' and @placeholder = 'Search for patient...' and @type = 'text']</value>
      <webElementGuid>1a5821cc-fa9b-4281-b8d2-91b26d1bfb2b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
