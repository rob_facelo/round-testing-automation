<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Select Discipline--               _075cf9</name>
   <tag></tag>
   <elementGuidId>4f276c93-e999-4e6f-86af-ee4bd3ae420d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlCaregiverType']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ddlCaregiverType</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>fed42884-ae9d-4031-b869-dd5c5a6b1b01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm fontsize-13-weight-600 ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>607ecb2e-3e57-472e-9606-6e08e8a1a456</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlCaregiverType</value>
      <webElementGuid>5b4f0f5c-ca33-4556-8a21-c4eea306de93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>plotVisit.OnCgTypeChange()</value>
      <webElementGuid>32d6f23b-5397-4c9c-be33-7468382161a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>plotVisit.SelectedCgType</value>
      <webElementGuid>b843f47a-eb92-4cc4-9fb7-65a0ca7d5744</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                </value>
      <webElementGuid>60e8428c-9c26-4a0a-8f70-16a5a3795f38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlCaregiverType&quot;)</value>
      <webElementGuid>e9b4580d-e649-4c49-bde6-b4fc2bf0dcb4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlCaregiverType']</value>
      <webElementGuid>15a02d4b-dee4-4656-b1f0-55d1d32d7674</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>25b35836-b5a1-4af5-a465-5212109023b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff Type'])[1]/following::select[1]</value>
      <webElementGuid>be08748d-a502-4cc9-aed8-0fe1c14f5bff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payer'])[1]/following::select[2]</value>
      <webElementGuid>26ac378d-71c6-4458-abd2-a537d7aa30aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff'])[2]/preceding::select[1]</value>
      <webElementGuid>5ecc5c5a-fa36-41b8-b7dc-b44c0cff0f88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Type'])[1]/preceding::select[2]</value>
      <webElementGuid>13755152-578b-4a9a-950a-48896513c6e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>daf2da25-036f-428a-a693-95078eaf563c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlCaregiverType' and (text() = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ' or . = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ')]</value>
      <webElementGuid>ca628e27-026c-460d-9ad4-c12678e2df12</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
