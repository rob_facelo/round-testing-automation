<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_SN</name>
   <tag></tag>
   <elementGuidId>fcbc181f-7136-4ef5-93e5-c66ba4f473cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='planPanel_8']/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.planned</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e55043e2-c2ab-4378-bf2d-43404de2e003</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>planned</value>
      <webElementGuid>18a039c6-ebd0-4869-bad1-57b22b0e4577</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>!vPlan.hastelehealth || vPlan.visitStatus != 'planned'</value>
      <webElementGuid>a1e8463a-cdb6-4201-bdc3-f2b01894a604</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>cl.showPopOver($event, dayRow, 'visitPlan', vPlan, weekRow.weekNumber)</value>
      <webElementGuid>9a9d60be-d066-487e-ab13-48a4fd05189f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-drag</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>199e0de4-4780-4b25-9877-c962184aba71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-jqyoui-options</name>
      <type>Main</type>
      <value>{revert: 'invalid'}</value>
      <webElementGuid>c0717bd3-aa47-4454-8602-7fa637006ab6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jqyoui-draggable</name>
      <type>Main</type>
      <value>{animate: true, onStart: 'onStart(weekRow.weekNumber, dayRow.ItemDate, 1, vPlan)', onStop: 'onStop(weekRow.weekNumber, dayRow.ItemDate)'}</value>
      <webElementGuid>ae69c6e6-2a6d-4e68-9b05-2b4ac40b891e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>SN</value>
      <webElementGuid>845cc460-0889-4182-99dc-52895b478841</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;planPanel_8&quot;)/div[@class=&quot;ng-scope&quot;]/a[@class=&quot;planned&quot;]</value>
      <webElementGuid>219da611-6427-4e67-82c3-c476fe1bd521</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='planPanel_8']/div/a</value>
      <webElementGuid>591dbd44-f556-45b1-aad5-0a365ea4a0cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'SN')])[3]</value>
      <webElementGuid>fdd86e5d-c716-4510-91fb-a3d68602d941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jul 3'])[1]/following::a[1]</value>
      <webElementGuid>c0d16577-9790-475d-a6f6-10da8f4bff31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actual'])[2]/following::a[1]</value>
      <webElementGuid>8acb7bf5-416f-4928-9be7-beacbe9e42b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jul 4'])[1]/preceding::a[1]</value>
      <webElementGuid>bc236b28-9da9-47ef-bae6-4a1d793edfdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jul 5'])[1]/preceding::a[1]</value>
      <webElementGuid>59024344-108c-4949-a1c0-344d3d79f2ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/div[2]/div/a</value>
      <webElementGuid>10be787c-4c6b-4032-9b29-ef292acdb0d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'SN' or . = 'SN')]</value>
      <webElementGuid>44c10aa8-ebc0-4d17-85e5-910c90aac3a9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
