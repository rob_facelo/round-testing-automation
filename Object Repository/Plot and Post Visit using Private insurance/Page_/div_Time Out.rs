<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Time Out</name>
   <tag></tag>
   <elementGuidId>bb1d2f48-5808-44e4-97e3-3f20040d740f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@id='TimeOut'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > #TimeOut</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b758d505-a21f-4a12-b1d1-6e173f12f89b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-group input-group-sm</value>
      <webElementGuid>fe41b502-e188-4f20-b11f-284999024b7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>TimeOut</value>
      <webElementGuid>84a6a613-b6f6-470a-b1f0-a21174b596d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>isDisabled</value>
      <webElementGuid>a8872470-3b79-4ff0-ac7e-ed2ad80d08d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Time Out
                                                
                                                :
                                                
                                            </value>
      <webElementGuid>5615f787-6e44-49c8-a4cd-0b257b925e88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/fieldset[2]/div[@class=&quot;row line-height30px&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-9 col-lg-9&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-8 col-lg-8&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@id=&quot;TimeOut&quot;]</value>
      <webElementGuid>5c5ad3df-3ef3-4cae-9caa-8ef4b53d84f1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//div[@id='TimeOut'])[2]</value>
      <webElementGuid>e98321d7-f143-4940-bdd5-b9928d364181</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[3]/following::div[3]</value>
      <webElementGuid>b8a8461a-2b2a-4675-8556-01757574d4fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div[2]/div/div/div[2]/div/div</value>
      <webElementGuid>736b44ec-d428-4367-a5aa-65eaa7f8c280</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'TimeOut' and (text() = '
                                                Time Out
                                                
                                                :
                                                
                                            ' or . = '
                                                Time Out
                                                
                                                :
                                                
                                            ')]</value>
      <webElementGuid>92e2fd66-3c96-4207-b5e1-dbd6b4b40349</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
