<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Time In</name>
   <tag></tag>
   <elementGuidId>3f8906d7-e50f-47bc-8755-41ed4fa458d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@id='TimeIn'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > #TimeIn</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9fb97b04-ec0f-44c5-86fc-2c6d92d00653</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-group input-group-sm</value>
      <webElementGuid>e8343e30-ca87-4ed7-8b1f-304e6e1101c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>TimeIn</value>
      <webElementGuid>9be6182c-f120-4a0e-a32f-03bbb1a7c66e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Time In   
                                                
                                                :
                                                
                                            </value>
      <webElementGuid>f2a7a146-3a67-488d-9157-b01da50e5bae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/fieldset[2]/div[@class=&quot;row line-height30px&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-9 col-lg-9&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-8 col-lg-8&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@id=&quot;TimeIn&quot;]</value>
      <webElementGuid>be69daba-cb14-4568-923b-853bf89e4c11</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//div[@id='TimeIn'])[2]</value>
      <webElementGuid>ab196a14-b176-43f1-931e-57a3b25e65c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Military time format)'])[1]/following::div[5]</value>
      <webElementGuid>4852d2e4-db23-45d4-a3e5-1f693aecdb57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Location'])[3]/following::div[9]</value>
      <webElementGuid>32232502-79f9-445f-b50d-8847feb252c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div[2]/div/div/div/div/div</value>
      <webElementGuid>f91dc207-c66f-48d9-a41a-e0bf275f0438</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'TimeIn' and (text() = '
                                                Time In   
                                                
                                                :
                                                
                                            ' or . = '
                                                Time In   
                                                
                                                :
                                                
                                            ')]</value>
      <webElementGuid>802bb645-a609-4da0-ac08-aa2f121c5426</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
