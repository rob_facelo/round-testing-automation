<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_---                                (_ce7194</name>
   <tag></tag>
   <elementGuidId>2a83e703-4e5e-4218-9db8-93fa1c2b33a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlChangeLocation']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ddlChangeLocation</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b66772e8-1c5c-4813-84ff-cc4fdbf2d6f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>7</value>
      <webElementGuid>13c7fe27-bd6c-48e8-a02b-5b9bfc597665</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>9f6bd98e-3d7f-4ae4-9722-fd0ac548ed6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlChangeLocation</value>
      <webElementGuid>b3fce904-f665-4a81-8151-f9fd86dabd16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>ServiceLocation</value>
      <webElementGuid>d47699e3-36b8-4ce2-9a65-725ea6605b69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>isDisabled || Pagefields.CalendarChangeCareStaff.AccessType == 1</value>
      <webElementGuid>35e260cc-3824-4e06-a163-d9e1dd3b31c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            </value>
      <webElementGuid>87e8fb34-18b1-479f-9cb1-695a2f1a1f51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlChangeLocation&quot;)</value>
      <webElementGuid>997934e1-1dbe-481b-ace5-bdbee6dbef86</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlChangeLocation']</value>
      <webElementGuid>59592c24-e9fb-43f4-8a63-0874192b8f1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Location'])[3]/following::select[1]</value>
      <webElementGuid>25ccc719-a2b4-4e75-94ee-bd8237434cd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Module:'])[1]/following::select[1]</value>
      <webElementGuid>c3a15b33-d084-4d66-a3cc-44e046acdb9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Military time format)'])[1]/preceding::select[1]</value>
      <webElementGuid>4772df54-57fa-4b9f-9d69-3637477f74fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div[2]/select</value>
      <webElementGuid>abfe0012-edcf-4f2e-8693-c1110baf909b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlChangeLocation' and (text() = '
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            ' or . = '
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            ')]</value>
      <webElementGuid>2fa2bdf0-c93a-4f82-a92f-051164199b48</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
