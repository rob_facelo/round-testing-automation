<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538</name>
   <tag></tag>
   <elementGuidId>e76c1974-cfc5-4b3a-9f48-e66be3ae61df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtAgencyId']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolderLogonPage_LoginControl_txtAgencyId</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b55b381a-3d1f-47f9-8a31-0e8b9191673a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolderLogonPage$LoginControl$txtAgencyId</value>
      <webElementGuid>ae70be2f-a169-43b7-a045-2dd8f29e14be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>93924197-fea4-488c-8c55-bc6867f0117c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolderLogonPage_LoginControl_txtAgencyId</value>
      <webElementGuid>01f8096f-c44f-4960-9ae7-26ec0655016d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>f8cc6420-42f1-4c25-adde-2aed4d84f0ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolderLogonPage_LoginControl_txtAgencyId&quot;)</value>
      <webElementGuid>06de8f64-19a5-45b1-93d9-c7fe052304c8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtAgencyId']</value>
      <webElementGuid>4173aff9-814a-4130-8dfb-2569816d6c8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]/fieldset/input</value>
      <webElementGuid>d0551cb8-a98c-4b94-809a-0d292d3c80c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/input</value>
      <webElementGuid>90b26e71-54cb-40c6-930a-22af388c014f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$ContentPlaceHolderLogonPage$LoginControl$txtAgencyId' and @type = 'text' and @id = 'ContentPlaceHolderLogonPage_LoginControl_txtAgencyId']</value>
      <webElementGuid>494d3da3-1b1d-4a8e-8250-aca404b49fbc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
