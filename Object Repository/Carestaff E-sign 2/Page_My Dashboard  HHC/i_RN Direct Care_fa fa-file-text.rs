<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_RN Direct Care_fa fa-file-text</name>
   <tag></tag>
   <elementGuidId>52316e1d-fc2c-4602-afde-e9f86cc81c67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow1']/td[6]/span/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow1 > td.dxgv > span.showpanelclassholder > i.fa.fa-file-text</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>2555c2d9-d948-4237-a6cc-b19080ed7bb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-file-text</value>
      <webElementGuid>5e86b19c-5c36-4f9b-9743-2916e856e7c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow1&quot;)/td[@class=&quot;dxgv&quot;]/span[@class=&quot;showpanelclassholder&quot;]/i[@class=&quot;fa fa-file-text&quot;]</value>
      <webElementGuid>580a61e9-e3ea-4291-8f84-23f394a0996c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow1']/td[6]/span/i</value>
      <webElementGuid>638279bf-7085-4241-a577-f469e7497d0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[6]/span/i</value>
      <webElementGuid>9e795fb7-b02f-4e1d-8e2b-eb6a6dbf92f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
