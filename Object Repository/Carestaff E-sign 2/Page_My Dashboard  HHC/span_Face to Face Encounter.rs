<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Face to Face Encounter</name>
   <tag></tag>
   <elementGuidId>682205d3-4296-4e5e-8066-54499e0ef857</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@onclick=concat(&quot;angular.element($('#taskworkflow_container').scope().showNoteModal({ patientId:&quot;,'&quot;6&quot;, episodeId:&quot;7710&quot;, patintakeId:&quot;55849&quot;, noteId:&quot;78322&quot;, noteType:&quot;FTFEncounter&quot;, calDate: &quot;07-01-2022&quot; }, ',&quot;'othernote'))&quot;)]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.showpanelclassholder</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[@onclick = concat(&quot;angular.element($(&quot; , &quot;'&quot; , &quot;#taskworkflow_container&quot; , &quot;'&quot; , &quot;).scope().showNoteModal({ patientId:&quot;6&quot;, episodeId:&quot;7710&quot;, patintakeId:&quot;55849&quot;, noteId:&quot;78322&quot;, noteType:&quot;FTFEncounter&quot;, calDate: &quot;07-01-2022&quot; }, &quot; , &quot;'&quot; , &quot;othernote&quot; , &quot;'&quot; , &quot;))&quot;) and (text() = ' Face to Face Encounter' or . = ' Face to Face Encounter')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>f37136cc-bbd6-4cd9-ae93-90d67f7a9ab6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>showpanelclassholder</value>
      <webElementGuid>109f280b-e040-42c1-87f1-ce38d166057c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>angular.element($('#taskworkflow_container').scope().showNoteModal({ patientId:&quot;6&quot;, episodeId:&quot;7710&quot;, patintakeId:&quot;55849&quot;, noteId:&quot;78322&quot;, noteType:&quot;FTFEncounter&quot;, calDate: &quot;07-01-2022&quot; }, 'othernote'))</value>
      <webElementGuid>55ce7fb2-4a5e-45aa-a874-ef7972f6e45d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Face to Face Encounter</value>
      <webElementGuid>21274985-66e5-4678-b4a1-61ff5b6d7ad6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow0&quot;)/td[@class=&quot;dxgv&quot;]/span[@class=&quot;showpanelclassholder&quot;]</value>
      <webElementGuid>2539649f-5eff-4fbe-8662-c1f02bb9d2e9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@onclick=concat(&quot;angular.element($('#taskworkflow_container').scope().showNoteModal({ patientId:&quot;,'&quot;6&quot;, episodeId:&quot;7710&quot;, patintakeId:&quot;55849&quot;, noteId:&quot;78322&quot;, noteType:&quot;FTFEncounter&quot;, calDate: &quot;07-01-2022&quot; }, ',&quot;'othernote'))&quot;)]</value>
      <webElementGuid>ade5b77e-2c23-44e5-9e4a-c2c3caa627bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow0']/td[6]/span</value>
      <webElementGuid>cfa0525d-050c-44e0-bb4b-15b1214e37bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='STYLES, FURIOUS'])[1]/following::span[1]</value>
      <webElementGuid>9502d4b1-4d60-40c7-b900-186cc86a5620</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Note Status'])[3]/following::span[2]</value>
      <webElementGuid>cdae73cb-fd6f-41fb-84b3-7058f9416f34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='N/A'])[1]/preceding::span[1]</value>
      <webElementGuid>2c033691-5d74-4026-9567-e06e03877a5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Face to Face Encounter'])[2]/preceding::span[1]</value>
      <webElementGuid>f9e83f37-f6f9-4d33-be8d-01e8477f6ff7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Face to Face Encounter']/parent::*</value>
      <webElementGuid>54bd8842-898e-4abe-aefb-c0826ee60119</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[6]/span</value>
      <webElementGuid>66cfe582-6aec-44f0-97dc-69ee6e8f1ff0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Face to Face Encounter' or . = ' Face to Face Encounter')]</value>
      <webElementGuid>bb182677-8cb2-4d10-9be8-25f280707f86</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
