<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_PIN</name>
   <tag></tag>
   <elementGuidId>c4b8ef3b-1b35-40ef-8087-55005b818ca5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='password'])[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.boxShadow.VisitNoteEsignPanel > div.modal-body > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.col-xs-12.col-sm-12.col-md-8.col-lg-8 > input.form-control.input-sm.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'password' and @ng-model = 'vstNote.pin']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5eb211d4-1bb1-49cd-a5f3-588f17b6b122</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>16514692-d5e5-419e-9a43-5ca292159e2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>b5a17599-c862-4113-8649-44d7a356195e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>vstNote.pin</value>
      <webElementGuid>ad12cf54-489c-4481-82ef-d391048cda57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;VisitNoteContainer&quot;)/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body visit-modal-body&quot;]/div[@class=&quot;boxShadow VisitNoteEsignPanel&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-8 col-lg-8&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>ddfb751d-cb29-475c-a0b7-4629e6a34153</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='password'])[6]</value>
      <webElementGuid>d4be84a3-1be3-47f5-9eb3-41b6f85af157</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='VisitNoteContainer']/div/div[2]/div[12]/div[2]/div/div/div[2]/input</value>
      <webElementGuid>91006db5-3795-4cbe-b85c-50b5e240d109</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/div[2]/div/div/div[2]/input</value>
      <webElementGuid>3b4ec55c-e78f-4b00-b95d-154b71d25791</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password']</value>
      <webElementGuid>bfed7353-79bf-44a0-ba69-4fc282da1038</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
