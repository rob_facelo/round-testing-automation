<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Electronically Sign</name>
   <tag></tag>
   <elementGuidId>c26f1d34-2484-44a7-8c79-6f7e7945ebc1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='VisitNoteContainer']/div/div[2]/div/div/div/button[3]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = ' Electronically Sign' or . = ' Electronically Sign')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3e0c8a67-52c2-4255-b6b7-24b4e602f9e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Electronically Sign</value>
      <webElementGuid>4c6a7617-3638-4660-be72-24fb43039594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;VisitNoteContainer&quot;)/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body visit-modal-body&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;row modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12 ng-scope&quot;]/span[1]</value>
      <webElementGuid>8efebec4-b5e7-4d8b-90cd-58f60210e0e1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='VisitNoteContainer']/div/div[2]/div/div/div/button[3]/span</value>
      <webElementGuid>4aed30ab-d7cb-4c4d-b13a-1fe8850da16c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More Options'])[1]/preceding::span[1]</value>
      <webElementGuid>6f8a68b1-7ed6-41d7-9ed6-47effc606c87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/button[3]/span</value>
      <webElementGuid>92437f12-211c-406f-aed6-feef9744c771</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Electronically Sign' or . = ' Electronically Sign')]</value>
      <webElementGuid>bc27b4ce-aa77-45bd-aea4-35fa6388b7ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
