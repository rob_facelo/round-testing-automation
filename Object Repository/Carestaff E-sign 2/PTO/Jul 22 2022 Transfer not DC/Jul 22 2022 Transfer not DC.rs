<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Jul 22 2022 Transfer not DC</name>
   <tag></tag>
   <elementGuidId>791ed74d-4d05-4b61-ad4c-60696b5f30cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1']/td[7]/div/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1 > td.dxgv > div.esigpopupcolumnholder > i.fa.fa-pencil-square-o</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;][count(. | //*[@class = 'fa fa-pencil-square-o']) = count(//*[@class = 'fa fa-pencil-square-o'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>d3c5f6a1-772d-465e-b12d-e272e4491e6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-pencil-square-o</value>
      <webElementGuid>1077a644-ae64-422a-956b-4c4dcc2cd8fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;]</value>
      <webElementGuid>3370b358-1b8e-4d4f-a626-bbf98dff1e0a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1']/td[7]/div/i</value>
      <webElementGuid>17ec9f16-a468-48ec-83ae-bd1423aad6af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[7]/div/i</value>
      <webElementGuid>e7355d83-857c-434a-bdc0-343028efcba9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
