<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Jul 22 2022 1125AM_fa fa-pencil-square-o</name>
   <tag></tag>
   <elementGuidId>4076e341-a2d9-4ec6-8752-c36019604ef6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;][count(. | //*[@class = 'fa fa-pencil-square-o']) = count(//*[@class = 'fa fa-pencil-square-o'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1 > td.dxgv > div.esigpopupcolumnholder > i.fa.fa-pencil-square-o</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1']/td[7]/div/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>45216b44-b361-4dfc-8c78-91366cadd19e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-pencil-square-o</value>
      <webElementGuid>ce8f1324-e1fd-44f7-900a-fa04af09c8fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1&quot;)/td[@class=&quot;dxgv&quot;]/div[@class=&quot;esigpopupcolumnholder&quot;]/i[@class=&quot;fa fa-pencil-square-o&quot;]</value>
      <webElementGuid>ce3fa921-e36f-48de-b6e9-dc1a077b55f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_ptoTbl_DXDataRow1']/td[7]/div/i</value>
      <webElementGuid>018c52be-4d0b-4c71-b1ed-dad850a69f34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[7]/div/i</value>
      <webElementGuid>1ca2bf48-758b-4e99-b5f6-89a1c4f1a6d7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
