<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Face to Face Encounter_fa fa-file-text</name>
   <tag></tag>
   <elementGuidId>a67e4d6d-a44b-41a9-a21b-f9724ebeaf08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow0']/td[6]/span/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-file-text</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow0&quot;)/td[@class=&quot;dxgv&quot;]/span[@class=&quot;showpanelclassholder&quot;]/i[@class=&quot;fa fa-file-text&quot;][count(. | //*[@class = 'fa fa-file-text']) = count(//*[@class = 'fa fa-file-text'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>2de60f12-4022-4c45-8efc-e169ad1a8774</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-file-text</value>
      <webElementGuid>716213c6-f461-434e-96c3-7d5cac671d57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow0&quot;)/td[@class=&quot;dxgv&quot;]/span[@class=&quot;showpanelclassholder&quot;]/i[@class=&quot;fa fa-file-text&quot;]</value>
      <webElementGuid>384e2754-3a7e-4b02-85db-6f4191836f22</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_HomeControl_TaskWorkflowControl_visitAndOtherNotesInProgTbl_DXDataRow0']/td[6]/span/i</value>
      <webElementGuid>44f4a412-aaad-4148-93c7-dc8d8feb15e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[6]/span/i</value>
      <webElementGuid>e17b178d-658b-49df-875b-75e1142265ef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
