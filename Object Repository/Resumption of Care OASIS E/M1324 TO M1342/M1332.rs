<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1332</name>
   <tag></tag>
   <elementGuidId>aaf13e7b-20c4-411e-a01f-b484ce6f00fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1332 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1332']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1332_NUM_STAS_ULCR']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>9cf8808b-656c-41d0-85a5-f992a8d6edc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>190ddc69-f83d-4747-ae11-9a67d36f6d36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1332_NUM_STAS_ULCR</value>
      <webElementGuid>b6455519-0eab-4707-a698-40b464322e38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1332_NUM_STAS_ULCR</value>
      <webElementGuid>9c7eab48-09bb-446b-9003-4c6f42a3e699</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1332_NUM_STAS_ULCR', oasis.oasisDetails.M1332_NUM_STAS_ULCR)</value>
      <webElementGuid>7b28b535-1164-4fa4-a5c4-28c23dab3278</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1330_STAS_ULCR_PRSNT == '00' || oasis.oasisDetails.M1330_STAS_ULCR_PRSNT == '03' || oasis.isLocked</value>
      <webElementGuid>e948b09a-3053-4d83-b341-ae8c1b935cad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                </value>
      <webElementGuid>10650fe0-9198-493f-808d-83318fb121e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1332&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>1d5ba062-81c7-400a-99a6-60d483213bbf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1332']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>f18f2a4c-bd5c-4c7a-9270-c879d3813670</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[48]/following::select[1]</value>
      <webElementGuid>bbf39bad-f204-44a1-ad3a-e6d5a41f7a3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M1332. Current Number of Stasis Ulcer(s) that are Observable'])[1]/following::select[1]</value>
      <webElementGuid>37cc2017-6bf0-45fd-9c23-6baf662b982e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='One'])[2]/preceding::select[1]</value>
      <webElementGuid>c9b4f94e-a904-4eae-acfd-f2d6be045c49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Two'])[2]/preceding::select[1]</value>
      <webElementGuid>ee800043-eadc-46ba-bef7-559a97795c41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[13]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>b2d3fd73-fde9-4bb0-b3df-ba5d1e5022f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                ' or . = '
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                ')]</value>
      <webElementGuid>e41d0659-d165-4e9c-b467-ca7e14426695</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
