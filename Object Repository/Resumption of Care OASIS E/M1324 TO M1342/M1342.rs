<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1342</name>
   <tag></tag>
   <elementGuidId>8eb9f02e-64d3-4d1d-8daa-d745e10cf6f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1342 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1342']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1342_STUS_PRBLM_SRGCL_WND']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>08e862a6-2dea-4964-b39e-93577ec3e302</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>6c259473-905b-4d58-aefa-4a4bde11f01b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1342_STUS_PRBLM_SRGCL_WND</value>
      <webElementGuid>48ec373d-585d-4a08-90c4-a414223234ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1342_STUS_PRBLM_SRGCL_WND</value>
      <webElementGuid>e3450b0c-a476-4f7f-8fc3-18621d7593a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1342_STUS_PRBLM_SRGCL_WND', oasis.oasisDetails.M1342_STUS_PRBLM_SRGCL_WND)</value>
      <webElementGuid>f41d0ae3-6a3c-4ad9-9a6f-a3a93fba57f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1340_SRGCL_WND_PRSNT == '00' || oasis.oasisDetails.M1340_SRGCL_WND_PRSNT == '02' || oasis.isLocked</value>
      <webElementGuid>d8135826-6bdb-4d5b-8fb9-5ba0a6757fcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    
                                </value>
      <webElementGuid>482b434b-7b8b-4adf-b33f-b30e64df0c13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1342&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>63f616b5-13ba-4990-be81-f94257c72aa1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1342']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>c19a8539-491f-4980-8956-073468ac5bcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[51]/following::select[1]</value>
      <webElementGuid>83f54864-0ad3-4dde-8184-5c35f8ca0d85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M1342. Status of Most Problematic Surgical Wound that is Observable'])[1]/following::select[1]</value>
      <webElementGuid>c80357ec-9dc9-4673-a606-bfcc770db74b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newly epithelialized'])[2]/preceding::select[1]</value>
      <webElementGuid>9047b866-e5c2-47bc-b864-5ac77144b108</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fully granulating'])[3]/preceding::select[1]</value>
      <webElementGuid>865117ac-04bb-4ddb-83e1-5c33eddc7d66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[13]/div/div[2]/div[9]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>dec353fd-78ea-41a4-9383-fca5dba939a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    
                                ')]</value>
      <webElementGuid>149c9ed8-6538-4de1-882e-8a4926473a2a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
