<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>m1322</name>
   <tag></tag>
   <elementGuidId>c09391ea-bbc8-4195-8d5a-fee1ae7f890e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1322 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1322']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1322_NBR_PRSULC_STG1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f3553ac4-eccf-443b-b2f7-ad774ac89346</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>4bf3b01c-012b-4370-bcc2-36deb2a43297</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1322_NBR_PRSULC_STG1</value>
      <webElementGuid>2417a570-09b0-453c-8126-59b3ff7b2cf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1322_NBR_PRSULC_STG1</value>
      <webElementGuid>53834e43-098c-4e70-98e4-6f0885392e66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1322_NBR_PRSULC_STG1', oasis.oasisDetails.M1322_NBR_PRSULC_STG1)</value>
      <webElementGuid>520c756c-1e69-4bc0-ae1d-4979198fe356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>c5d1340c-9006-4de2-aaf1-dc6cca6d18e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                </value>
      <webElementGuid>d8999962-80ca-450a-8a6d-2f9aae02be23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1322&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>947139f5-c8e9-4d1c-ae62-f374c47a2747</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1322']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>26eaa723-e204-41c9-a075-1a335a1c63bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[45]/following::select[1]</value>
      <webElementGuid>0e31ff39-248a-4477-a39e-c827c8ff2ead</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Number of unstageable pressure injuries presenting as deep tissue injury'])[1]/following::select[1]</value>
      <webElementGuid>04143175-dec2-47f5-8810-74c04e26a991</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[46]/preceding::select[1]</value>
      <webElementGuid>875bcd66-8c1b-4c4c-bb53-960136745809</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stage 1'])[1]/preceding::select[2]</value>
      <webElementGuid>3b4169c0-afa3-4702-bd97-16764af9d0b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[13]/div/div[2]/div[3]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>28627adc-64cd-4ae5-a4e0-4bb9441a0cd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                ')]</value>
      <webElementGuid>9971577a-e7a3-4f73-acf2-991543f3246b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
