<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _313b6d</name>
   <tag></tag>
   <elementGuidId>b9e15375-4bf4-47c1-bebb-fda89aedef0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1324 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1324']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1324_STG_PRBLM_ULCER']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>42c4b681-f6ef-43c8-b375-25bd0c828709</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>6488d7d0-a5db-4366-9b6b-59065ab3d5f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1324_STG_PRBLM_ULCER</value>
      <webElementGuid>324a26d5-8b7d-433d-af2d-6bf89673ff64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1324_STG_PRBLM_ULCER</value>
      <webElementGuid>b5edd2db-1647-4890-adc6-a5b5aee6d7e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1324_STG_PRBLM_ULCER', oasis.oasisDetails.M1324_STG_PRBLM_ULCER)</value>
      <webElementGuid>2e0a47d1-d844-4103-9c76-a403880f4f0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>5bc56f97-6349-4864-ac6a-e431886d6b33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    1
                                    2
                                    3
                                    4
                                    NA
                                    
                                </value>
      <webElementGuid>9a4af3f2-9d77-441f-bf3f-2bd5a42c2e4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1324&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>76a0c5e2-de66-4440-bffc-c2797cb6bd99</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1324']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>0b627052-75c4-47ce-9da2-24906f0ff096</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[43]/following::select[1]</value>
      <webElementGuid>cbeff5d2-f6c9-4307-907e-2474cc1d1850</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[42]/following::select[2]</value>
      <webElementGuid>2ca9fa71-fd38-46e9-bd84-49a405e8d333</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stage 1'])[1]/preceding::select[1]</value>
      <webElementGuid>507801fe-c10f-4734-af7a-78686c420491</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stage 2'])[1]/preceding::select[1]</value>
      <webElementGuid>e343f63d-72d5-48e1-9f6f-36a534f49ccb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[13]/div/div[2]/div[4]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>b105cf5f-9630-4104-83df-bc7ca046f346</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    1
                                    2
                                    3
                                    4
                                    NA
                                    
                                ' or . = '
                                    
                                    1
                                    2
                                    3
                                    4
                                    NA
                                    
                                ')]</value>
      <webElementGuid>0318edab-9676-401c-b49b-fb8bfcc170de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
