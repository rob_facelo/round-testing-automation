<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_M1005. Inpatient Discharge Date (most r_632126</name>
   <tag></tag>
   <elementGuidId>c55ec358-114f-43ae-aa20-260f2f4a4d14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.input-group.date</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1005']/div/div[2]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>75981ef0-68aa-418b-907b-c65d10beca7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-group date</value>
      <webElementGuid>55c2a6af-f487-4395-8940-cee5d1de3350</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1005&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;input-group date&quot;]</value>
      <webElementGuid>b57f9e5e-d13e-4a24-ada8-0a3cfe12d3ca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1005']/div/div[2]/div/div/div</value>
      <webElementGuid>a4b6b988-5045-4d28-8b6a-eef78f0b7fe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M1005. Inpatient Discharge Date (most recent):'])[1]/following::div[4]</value>
      <webElementGuid>21a05ec0-dd68-412f-9357-9afa8901c2cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B1300'])[1]/following::div[8]</value>
      <webElementGuid>d41d7478-2854-4db2-8f86-11e39b447153</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SECTION B - HEARING, SPEECH, AND VISION'])[1]/preceding::div[3]</value>
      <webElementGuid>ff84a8ef-fb6d-4012-a183-12366a26e54f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div/div[2]/div/div/div</value>
      <webElementGuid>09bac04a-1c01-4e2c-a63d-a577fa4736ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
