<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _5524e8</name>
   <tag></tag>
   <elementGuidId>0dc6ad42-977a-4cef-a7d0-b664d341db18</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='C1310']/div/div[2]/div/div/div[3]/div/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.C1310A' and (text() = '
                                                    
                                                    0
                                                    1
                                                    -
                                                ' or . = '
                                                    
                                                    0
                                                    1
                                                    -
                                                ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>84fd8de4-5406-4e52-8d85-85d214629d4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>d1f6dd6e-4b73-4bd0-9e3f-fd8eb11262fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>C1310A</value>
      <webElementGuid>fe02cc36-9993-4dbb-80e5-47410b0b9a32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>f373d0a4-de30-4f93-a29a-4c7aa5a4cdee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.C1310A</value>
      <webElementGuid>04ed6810-199f-4d75-bab9-0c58c6dee42b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('C1310A', oasis.oasisDetails.C1310A)</value>
      <webElementGuid>0cbe5c92-7ca5-4c2c-855b-571fda1ded1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>8f33d87c-9c6d-4951-8118-5a3b70c88c03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    
                                                    0
                                                    1
                                                    -
                                                </value>
      <webElementGuid>e82c08e4-7670-407b-808a-9d4e3228b426</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;C1310&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-8 col-md-10 col-lg-10&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>e4544929-f042-40ad-8f3c-39dfd7b5e159</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='C1310']/div/div[2]/div/div/div[3]/div/div/div[2]/select</value>
      <webElementGuid>a8135d58-7961-4359-998a-9ceeb201f47c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[13]/following::select[1]</value>
      <webElementGuid>91a5fd8d-e8e6-49d1-b8bd-281f87b2bce3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Acute Onset of Mental Status Change'])[1]/following::select[1]</value>
      <webElementGuid>b5f431ed-9c6a-4e53-8f0e-0f0128602924</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Is there evidence of an acute change in mental status'])[1]/preceding::select[1]</value>
      <webElementGuid>46f78646-54ea-44e1-8e33-25e87e95fd72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[3]/div/div/div[2]/select</value>
      <webElementGuid>d15d7a01-f5b5-472f-9124-60e97e929b9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                    
                                                    0
                                                    1
                                                    -
                                                ' or . = '
                                                    
                                                    0
                                                    1
                                                    -
                                                ')]</value>
      <webElementGuid>52bf84d4-7ab9-48e1-b742-065ce01a2071</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
