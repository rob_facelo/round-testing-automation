<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_A01.00</name>
   <tag></tag>
   <elementGuidId>36fd5a34-3ae7-49dc-8d9d-b03907626b42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//autocomplete-directive[@id='M0230_PRIMARY_DIAG_ICD']/div/div[2]/ul/li[4]/a/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>b8ecfd75-4a82-4363-8b26-f4c2e040e406</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-primary ng-binding</value>
      <webElementGuid>4b18e2cd-d28e-46df-92fb-596a0fbbb1d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>A01.00</value>
      <webElementGuid>3825d149-e041-4240-a55e-35b3dc187838</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1025&quot;)/table[@class=&quot;table table-bordered tblM1021&quot;]/tbody[1]/tr[4]/td[2]/div[@class=&quot;col-xs-9 col-sm-9 col-md-9 col-lg-9 row&quot;]/div[@class=&quot;input-group space&quot;]/autocomplete-directive[@id=&quot;M0230_PRIMARY_DIAG_ICD&quot;]/div[@class=&quot;dropdown&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/p[@class=&quot;text-primary ng-binding&quot;]</value>
      <webElementGuid>87b8001c-b46a-41ff-9aac-41070f75a052</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//autocomplete-directive[@id='M0230_PRIMARY_DIAG_ICD']/div/div[2]/ul/li[4]/a/div/p</value>
      <webElementGuid>1c0ce0f3-a247-493f-939e-83d1333a3498</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='a.'])[2]/following::p[7]</value>
      <webElementGuid>5d5709bc-c5c1-45a4-9461-54118d8556f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='V, W, X, Y codes NOT allowed'])[1]/following::p[7]</value>
      <webElementGuid>bb331c19-62c0-4760-8b64-727aa92aff3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M1023) Other Diagnoses'])[1]/preceding::p[14]</value>
      <webElementGuid>9f527d2c-b8f5-4c13-9bbb-35ddcfac2940</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='b.'])[1]/preceding::p[14]</value>
      <webElementGuid>a1ec2905-311c-4a06-8367-c2396d3ec0ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='A01.00']/parent::*</value>
      <webElementGuid>7a9b4ee5-f471-4174-9ad0-80e2302dadac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a/div/p</value>
      <webElementGuid>390a04f9-9e5d-44c2-9289-65e5b07e10b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'A01.00' or . = 'A01.00')]</value>
      <webElementGuid>9bf8b816-6392-4649-8e95-c21d13e63055</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
