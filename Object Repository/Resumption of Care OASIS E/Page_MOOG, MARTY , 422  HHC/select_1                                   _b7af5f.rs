<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _b7af5f</name>
   <tag></tag>
   <elementGuidId>e4e3e415-4af5-46dc-95fb-51e9bd553232</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#GG0170RR1 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170RR1']/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>ae88c71b-8aa3-43fe-8f55-9d58508323d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>c7251155-69b9-4f68-ab2e-69afaa4ae880</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170RR1</value>
      <webElementGuid>62958b0a-dc40-41d1-ad67-dfe2fdd9a70a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>8a236973-82d0-4833-828e-35732f84a00c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170RR1</value>
      <webElementGuid>cc2c5c0c-c9da-43cf-9d8b-fd64bafa9439</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0170RR1', oasis.oasisDetails.GG0170RR1)</value>
      <webElementGuid>4313e46e-b6f7-45f9-8e83-c7eb4a299766</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170Q1 == '0' || oasis.isLocked</value>
      <webElementGuid>bffec7aa-6d5e-42a4-ad99-5d7190892403</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            
                                                            1
                                                            2
                                                            -
                                                        </value>
      <webElementGuid>7130b4b5-e068-42dd-9ef3-6ae9bf37551e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170RR1&quot;)/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>8f3916a8-f5c8-466b-8768-e75d67a9dc8e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170RR1']/div/select</value>
      <webElementGuid>3a40787d-c2aa-4a7c-baf8-936d9a001b1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wheel 50 feet with two turns:'])[1]/following::select[1]</value>
      <webElementGuid>193674b2-5745-4177-8af3-4f17c6be35ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RR1. Indicate the type of wheelchair or scooter used.'])[1]/preceding::select[1]</value>
      <webElementGuid>04ce785e-9dad-4c6c-b001-ad569fda2672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manual'])[1]/preceding::select[1]</value>
      <webElementGuid>ab6f3346-c187-46ce-b5e2-f296e3a055db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[19]/div/div[2]/div/div/div/select</value>
      <webElementGuid>1d5b5532-f002-4c6d-8dc1-40b3b3fc7cae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                            
                                                            1
                                                            2
                                                            -
                                                        ' or . = '
                                                            
                                                            1
                                                            2
                                                            -
                                                        ')]</value>
      <webElementGuid>b6631914-cd3d-4a3a-a3a8-52535d98f998</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
