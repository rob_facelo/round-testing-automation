<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _e338a2_1</name>
   <tag></tag>
   <elementGuidId>078c46f8-dd8f-4a5f-a95f-63ca6e7d14ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M2030 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2030']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>8103ae5c-4b7a-4de2-8722-e8f7f42e05e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>5458f9a0-cd71-470e-b331-0a5d2f1f647e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2030_CRNT_MGMT_INJCTN_MDCTN</value>
      <webElementGuid>9f2e6efd-cb69-45fe-a803-e092ab7ff222</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN</value>
      <webElementGuid>88dc8233-cd2d-431f-9d32-c44edc7f310b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2030_CRNT_MGMT_INJCTN_MDCTN', oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN)</value>
      <webElementGuid>4806e538-4240-4146-90f9-adf019cdd3c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>(oasis.oasisDetails.M2000_DRUG_RGMN_RVW == '9' &amp;&amp; (oasis.oasisDetails.M0100_ASSMT_REASON == '01' || oasis.oasisDetails.M0100_ASSMT_REASON =='03')) || oasis.isLocked</value>
      <webElementGuid>af0378c7-135c-4588-b7b2-ac44add204bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                </value>
      <webElementGuid>4c52b822-f90b-48e1-8106-43306f9cbed7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2030&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>a35fdea8-5dc8-4182-a2fb-890451bee399</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2030']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>b23a23d9-ea33-4a5d-b662-f8a93c146c8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[52]/following::select[1]</value>
      <webElementGuid>324b9d1f-f107-4474-8861-d40f5b28599e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Excludes'])[2]/following::select[1]</value>
      <webElementGuid>1e3091f8-0b55-4166-a90e-f97b5e36bbf3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to independently take the correct medication(s) and proper dosage(s) at the correct times.'])[1]/preceding::select[1]</value>
      <webElementGuid>75f9a107-afe6-4a61-b798-1438fce2d56c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to take injectable medication(s) at the correct times if:'])[1]/preceding::select[1]</value>
      <webElementGuid>2df78567-7b6e-4712-b5e8-770c248315d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[14]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>fe721d27-dc7b-42ed-83e3-4caa3d36d5bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                ')]</value>
      <webElementGuid>f7320e13-4b48-49ee-a8c0-7deb6daefd03</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
