<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Long-term nursing facility (NF)_ng-pr_971f70</name>
   <tag></tag>
   <elementGuidId>e33a1652-0f1a-49bc-9c6f-8b6e3866b43d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[51]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'checkbox' and @ng-model = 'oasis.oasisDetails.M1000_DC_SNF_14_DA']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5e7e8c45-d97e-4fce-b947-79f3ba6fe748</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>d6e2ba30-339d-4d73-a61d-a1bc6570e090</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1000_DC_SNF_14_DA</value>
      <webElementGuid>e4aff1d8-e33c-4814-9015-0a5dca5c225a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1000_DC_SNF_14_DA</value>
      <webElementGuid>a1adad0b-031e-42c3-b10f-e1dd9e042a7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>'1'</value>
      <webElementGuid>cf026614-4150-400e-9135-2ff86aabfd91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-false-value</name>
      <type>Main</type>
      <value>'0'</value>
      <webElementGuid>231d0bde-a831-48df-a8ec-84238ff943fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.clearMItemField([$event,'M1000_DC_NONE_14_DA'],true)</value>
      <webElementGuid>78ed48aa-5ddc-4ab3-a575-895f12f87f3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>297299ff-f231-4bcf-8d54-517b6dea3640</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>c5473abf-6a2e-4505-9934-1463f9c7a29d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1000&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>863590f5-f67d-4078-922c-ec645c94353e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[51]</value>
      <webElementGuid>c60e3bb7-3139-4205-979c-a59ae06eb274</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1000']/div/div[2]/div/div[2]/input</value>
      <webElementGuid>5e3439d1-02be-47bc-9326-b27a17d254ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[15]/div/div/div[2]/div/div[2]/input</value>
      <webElementGuid>eec3c1f3-154e-4cb9-83b6-34c5e2731463</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>a029c4fd-aa35-4f9a-9f53-665f5a72a7f1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
