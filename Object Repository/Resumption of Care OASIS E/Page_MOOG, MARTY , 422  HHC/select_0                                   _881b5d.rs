<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _881b5d</name>
   <tag></tag>
   <elementGuidId>d505dd54-f5f6-484b-b2d5-c0bbc5dee029</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#B1300 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='B1300']/div/div[2]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>12c8a495-be27-4c17-a56a-ea1b029e8833</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>58055418-67a3-412e-9293-a0eedef3361b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>B1300</value>
      <webElementGuid>ebf3d239-2fb0-4f38-9373-64f00e0b6935</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>7cbcbb39-b07f-4b27-808a-47464d3e3034</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.B1300</value>
      <webElementGuid>009cdb72-7443-4fb7-8425-c92bc188e46a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('B1300', oasis.oasisDetails.B1300)</value>
      <webElementGuid>e1b2dfa8-3f3c-43b3-94a5-89ea192889d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>c1aff06d-43e2-450c-acda-a36d92337731</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                        7
                                        8
                                    </value>
      <webElementGuid>bb17e423-cbc3-4c48-9aa2-1b943033d31f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;B1300&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>cdcff309-6763-49bd-95f9-78a44381c285</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='B1300']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>17538fe6-cf13-4438-bce1-b10e96209c69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[4]/following::select[1]</value>
      <webElementGuid>bf6abc0d-ea2d-49fc-84f5-d3713b314ed4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SECTION B - HEARING, SPEECH, AND VISION'])[1]/following::select[1]</value>
      <webElementGuid>b8313789-0bbd-49e2-bf8d-b36548175e3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Never'])[1]/preceding::select[1]</value>
      <webElementGuid>f895384b-0ab6-4fa9-ad39-2b346bd05904</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rarely'])[1]/preceding::select[1]</value>
      <webElementGuid>758436ef-2b07-4c6f-a634-7288a60364b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]/div/div[2]/div/div/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>eaaea7dc-90bf-4940-a886-84d1fe5b7d3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                        7
                                        8
                                    ' or . = '
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                        7
                                        8
                                    ')]</value>
      <webElementGuid>53d7a555-abda-40b0-8ac1-cde7ebbb6d93</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
