<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _b53a97</name>
   <tag></tag>
   <elementGuidId>a23cad7d-1820-4cdf-a913-fd44a7b4f47f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-4.col-md-1.col-lg-1.row > div.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='C1310']/div/div[2]/div[2]/div/div[2]/div/div[2]/div/div/div/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.C1310B' and (text() = '
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        ' or . = '
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>db023040-12a7-4278-815b-c387b9c37804</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>f321158e-7e94-46f0-b9cc-2aee7c04a07d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>C1310B</value>
      <webElementGuid>1aa12e1d-8368-4cc8-b381-805332b5053b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>2a6d8a25-951b-4fd7-883e-2e8feb5b40fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.C1310B</value>
      <webElementGuid>3673f784-22d9-4863-93e8-45eaad50ae57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('C1310B', oasis.oasisDetails.C1310B)</value>
      <webElementGuid>af1dc84b-34c0-488f-a287-dd1af27be8e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>55bc866e-91c1-4c1c-9307-ac7c0b844406</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        </value>
      <webElementGuid>9646e102-837b-4b71-95be-63c440192e11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;C1310&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-8 col-md-9 col-lg-9&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-1 col-lg-1 row&quot;]/div[@class=&quot;row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>efcc34a4-17c9-4891-a37f-95ceedd70612</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='C1310']/div/div[2]/div[2]/div/div[2]/div/div[2]/div/div/div/select</value>
      <webElementGuid>e9e1e6b8-cade-48b2-ba8e-dc0e11411018</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[14]/following::select[1]</value>
      <webElementGuid>a04de7eb-935c-494b-b0e7-242f2cc6fd57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inattention'])[1]/preceding::select[1]</value>
      <webElementGuid>831b8cde-ecd0-4e52-a440-3b34af896e90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Disorganized thinking'])[1]/preceding::select[2]</value>
      <webElementGuid>c93859f1-6938-44f0-a283-4280349b5ec0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/div/select</value>
      <webElementGuid>dd79d79d-f249-4c00-bc45-d926aa2a2ffc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        ' or . = '
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        ')]</value>
      <webElementGuid>1675b56e-28d7-4f67-b7d8-a15a92743658</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
