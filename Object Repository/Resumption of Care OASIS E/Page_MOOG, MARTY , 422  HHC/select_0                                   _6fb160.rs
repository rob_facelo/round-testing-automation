<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _6fb160</name>
   <tag></tag>
   <elementGuidId>1b0a81a9-ffdf-460e-b826-b734382605fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1600 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1600']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>677a0cf3-f69e-49d9-b586-0b85b8834fba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>8207e0ef-94e5-4f54-ae34-85611c68a35f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1600_UTI</value>
      <webElementGuid>8d4e4a13-2189-462c-9ee5-0030c0983d6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1600_UTI</value>
      <webElementGuid>160a3930-d1fe-4996-a817-8498a7e5e231</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1600_UTI', oasis.oasisDetails.M1600_UTI)</value>
      <webElementGuid>1defff7a-7052-4208-9e66-7ce2e52568cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>b0d89490-123c-4851-a06c-d26c43f72c6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    NA
                                    UK
                                </value>
      <webElementGuid>b6bb12d2-815e-434d-b172-a24ee2fe226f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1600&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>f1a3792a-7c97-4584-b094-ada26fa4c847</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1600']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>a6b48985-606f-461d-9e8d-daf4773e02b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[32]/following::select[1]</value>
      <webElementGuid>0c7bcc37-ef82-4fa8-904a-74697568aac7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M1600. Has this patient been treated for a Urinary Tract Infection in the past 14 days?'])[1]/following::select[1]</value>
      <webElementGuid>4984fabb-02b0-43d9-ba2c-904fec429aff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No'])[19]/preceding::select[1]</value>
      <webElementGuid>25e3fe6b-9944-400a-9b31-293d8f4d0f53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[11]/preceding::select[1]</value>
      <webElementGuid>a1bf994c-f82c-416d-a5e0-eb50b64a8b23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[9]/div/div[2]/div/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>43400bcd-9725-4b9a-a40d-dd3642b82faa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    NA
                                    UK
                                ' or . = '
                                    
                                    0
                                    1
                                    NA
                                    UK
                                ')]</value>
      <webElementGuid>52b8b8d5-3f51-43ec-83ed-eb611290f2af</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
