<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>D0150_E1</name>
   <tag></tag>
   <elementGuidId>9ea68aea-12e2-49b3-88c2-fc821dd8652d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='D0150']/div/div[2]/div[11]/div/div/div[2]/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>62fcdebe-6684-4ed5-81d0-d66f03e7a861</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>675999fb-5be7-43b3-99b6-0fa7244d0ac5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>D0150E1</value>
      <webElementGuid>6da3a859-ac9c-4091-86fb-b5c30dcfa30e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>e8b3aa2f-84c5-4d82-b81e-613a0a50378a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.D0150E1</value>
      <webElementGuid>7a922dcb-78e7-49cf-a533-1356f73ff491</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('D0150E1', oasis.oasisDetails.D0150E1); oasis.autoCalculateD0160(1);</value>
      <webElementGuid>7d505722-2593-469e-af65-4fe67c8d83ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked || (oasis.oasisDetails.D0150A1 == '9' &amp;&amp; oasis.oasisDetails.D0150B1 == '9') || ((oasis.oasisDetails.D0150A2 == '0' || oasis.oasisDetails.D0150A2 == '1') &amp;&amp; (oasis.oasisDetails.D0150B2 == '0' || oasis.oasisDetails.D0150B2 == '1'))</value>
      <webElementGuid>2d765bb5-986f-498c-a9e7-cabc36085b53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        0
                                                        1
                                                        9
                                                        -
                                                    </value>
      <webElementGuid>2228e280-eec0-4a95-bbc9-26e6635f1307</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;D0150&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-3 col-md-2 col-lg-2&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>bebf8d91-c33d-4889-a2ad-f3af2c6fd8c4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='D0150']/div/div[2]/div[11]/div/div/div[2]/div/div/div/select</value>
      <webElementGuid>fdecba1c-d21d-41c8-a640-c4eef14801af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Poor appetite or overeating'])[1]/following::select[1]</value>
      <webElementGuid>05285975-c8e4-42e9-b34b-ceb89ac5730f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Feeling bad about yourself – or that you are a failure or have let yourself or your family down'])[1]/preceding::select[2]</value>
      <webElementGuid>cb567918-4967-45fe-88e9-de6f64e938c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]/div[11]/div/div/div[2]/div/div/div/select</value>
      <webElementGuid>59ca42a7-5d6f-46e5-abcf-1da51b140040</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        
                                                        0
                                                        1
                                                        9
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        9
                                                        -
                                                    ')]</value>
      <webElementGuid>82a1646e-c76d-4e07-b78c-8c74c6c692f2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
