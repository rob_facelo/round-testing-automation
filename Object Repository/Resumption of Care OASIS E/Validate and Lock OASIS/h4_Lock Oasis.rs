<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Lock Oasis</name>
   <tag></tag>
   <elementGuidId>68d0db48-e117-480f-838b-9d96b298c291</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal.fade.ng-scope.in > div.modal-dialog.modal-sm > div.modal-content > div.modal-header.hhc-blue > h4.modal-title.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/following::h4[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Lock Oasis' or . = ' Lock Oasis')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>94f7a377-4461-4c42-ac23-d9244bfcf535</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-title ng-binding</value>
      <webElementGuid>717f9231-3948-4a9b-a42e-c39340af7644</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Lock Oasis</value>
      <webElementGuid>8d1f90e3-52a8-44db-a6ac-d316afd09d17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header hhc-blue&quot;]/h4[@class=&quot;modal-title ng-binding&quot;]</value>
      <webElementGuid>95c999de-d370-47f2-b8d9-e4535fb47588</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/following::h4[1]</value>
      <webElementGuid>00c4b83f-a13c-4b1b-9611-af6acacc1642</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock OASIS'])[1]/following::h4[1]</value>
      <webElementGuid>289c9c86-add0-4853-89ef-3a0dbd2ec1ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[2]/preceding::h4[1]</value>
      <webElementGuid>eda9349b-dfbb-45fa-986e-4607ebae9102</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock OASIS'])[2]/preceding::h4[1]</value>
      <webElementGuid>36d571eb-a6b7-4717-8bb4-1b1c9d6b06a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/h4</value>
      <webElementGuid>1e033b38-ea83-4c64-a6f2-6f1ec43882dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = ' Lock Oasis' or . = ' Lock Oasis')]</value>
      <webElementGuid>c045273d-f3b5-412e-adb3-51fad52eb405</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
