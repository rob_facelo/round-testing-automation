<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Lock Oasis</name>
   <tag></tag>
   <elementGuidId>c18f0865-743d-42ab-aff6-45dfa0a4da3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[55]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'oasis.lockOasis()' and (text() = '
                
                Lock Oasis
            ' or . = '
                
                Lock Oasis
            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>32e52e6c-6518-4cab-bb28-9ce884cb544f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6486cd5f-46f1-46ab-a2c6-effe6f203f4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btn-sm toolbar-btn-default font12 ng-scope</value>
      <webElementGuid>539d85ab-04df-4014-9196-4a42746fa8eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.lockOasis()</value>
      <webElementGuid>e051975a-1bf4-44f5-a0fe-c421d80bf55f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>(!oasis.isLocked &amp;&amp; ((oasis.isInUse &amp;&amp; oasis.oasisDetails.inuse_by_user_id == oasis.userCredentials.UserId) || !oasis.isInUse)) &amp;&amp; !oasis.isViewOtherOasisVersion</value>
      <webElementGuid>d3168c88-9ffb-4bae-8840-fa766337c604</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>!oasis.permissions.LockUnlock || oasis.isOnSaveState || oasis.isValidating</value>
      <webElementGuid>c8782c8c-4dfe-4a1a-89f8-a4ac232d2a90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                Lock Oasis
            </value>
      <webElementGuid>b16bf5ff-cfa7-45de-9fc1-28335cf15f10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 header_nav_container_oasis&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]</value>
      <webElementGuid>846052da-e44d-4422-9d2c-41693fa8be6b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[55]</value>
      <webElementGuid>67d6fff7-873d-4ab5-b27e-8004340c12de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[3]</value>
      <webElementGuid>129ec355-d23d-4649-942a-83e0df2580e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PPS Calculator'])[1]/preceding::button[1]</value>
      <webElementGuid>c93240d5-48db-4801-a3a3-2740bf4ca2af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDGM Calculator'])[1]/preceding::button[2]</value>
      <webElementGuid>e9645e1e-cd0b-4369-a7f5-2cf13991f28c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lock Oasis']/parent::*</value>
      <webElementGuid>946feaf0-8b69-42f1-a5a3-85da423394af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]</value>
      <webElementGuid>a8af6c33-e155-4935-b276-79ba2d569d04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                
                Lock Oasis
            ' or . = '
                
                Lock Oasis
            ')]</value>
      <webElementGuid>6c2888ed-00f7-4d95-9ece-00f15dbce25e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
