<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Number D1</name>
   <tag></tag>
   <elementGuidId>1cd2d0a0-6483-4aff-972b-86b39d52df0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @ng-model = 'oasis.oasisDetails.M1308_NSTG_DRSG']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[163]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5367dc34-92e3-4e2c-9792-1e70425df9f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e1421f51-1bee-4f84-9c6c-f83f5c501452</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-valid ng-valid-maxlength ng-touched</value>
      <webElementGuid>6b509f42-a961-4c14-a152-bdbc06d05438</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1308_NSTG_DRSG</value>
      <webElementGuid>c5d63987-9971-4928-8b58-250d5ee3a53b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>3fce66e1-3dc2-428e-9874-ff0564fe36a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1308_NSTG_DRSG</value>
      <webElementGuid>14ff1ac0-0a03-44d2-ab60-46bf3c5f9101</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1308_NSTG_DRSG', oasis.oasisDetails.M1308_NSTG_DRSG)</value>
      <webElementGuid>a504d89c-7b66-4a10-b2f4-7c1402dcac5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1306_UNHLD_STG2_PRSR_ULCR == '0' || oasis.isLocked</value>
      <webElementGuid>6846f9b0-87bf-4cfd-be58-442cf878729f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1311&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space content-box content-box-m1311&quot;]/table[@class=&quot;table table-bordered tbl1311&quot;]/tbody[1]/tr[5]/td[2]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-valid ng-valid-maxlength ng-touched&quot;]</value>
      <webElementGuid>35675bb9-bb17-461f-b92f-e99da14632a0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[163]</value>
      <webElementGuid>0326fb14-f520-4dbc-b931-53c631367f67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1311']/div/div[2]/table/tbody/tr[5]/td[2]/div/input</value>
      <webElementGuid>afd1fb64-7855-46ce-8f9c-9710131b9ee5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/table/tbody/tr[5]/td[2]/div/input</value>
      <webElementGuid>eaaefdda-11a5-43a6-acab-8dcf77b7eadb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>9cca7280-5bd8-4d56-af61-19835694ca5b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
