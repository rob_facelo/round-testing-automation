<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0130C1</name>
   <tag></tag>
   <elementGuidId>371d5260-aab9-4fa0-84d5-4cdfc342b722</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#GG0130 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.ng-scope > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.row > div.col-xs-4.col-sm-4.col-md-3.col-lg-3.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.row > div.col-xs-6.col-sm-6.col-md-6.col-lg-6 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0130C1']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>570dd59f-b79e-4348-87be-f8d6631c8745</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>4cc7a080-caff-403d-a866-312bd5d4f5af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0130C1</value>
      <webElementGuid>9622bb72-b060-4c1f-b4ca-a2a9d6ba0f53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>df7da10f-8602-4c84-a644-918654710ab3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0130C1</value>
      <webElementGuid>08cc82cc-529e-492b-9905-5ef587e99081</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0130C1', oasis.oasisDetails.GG0130C1)</value>
      <webElementGuid>4596dff0-ac41-4696-abfc-6ad5ddab8cdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>d63e6d56-5403-4f84-9e9d-117010642f91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        </value>
      <webElementGuid>7d1df8c8-29b1-4e0b-b4ba-3a03e7e1ce7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0130&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-3 col-lg-3 row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-6 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>b6ffeb87-52a0-45dd-9ffd-b67035bd237a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      <webElementGuid>56fd0b74-6027-474f-aaa9-452ea2d36a85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Oral Hygiene:'])[1]/following::select[1]</value>
      <webElementGuid>fb655462-4fa6-4c1d-bc69-d9eed9365045</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. Eating:'])[1]/following::select[3]</value>
      <webElementGuid>49775478-a9a8-42cc-b6d1-3277e677ccf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C. Toileting Hygiene:'])[1]/preceding::select[2]</value>
      <webElementGuid>3f1f3719-b34a-4b45-afad-769f299a8c7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='E. Shower/bathe self:'])[1]/preceding::select[4]</value>
      <webElementGuid>7a3b2d59-d945-4081-9e22-2aebf71875b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      <webElementGuid>dd1a5434-739f-4b86-bb68-cfbb4574daf0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ' or . = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ')]</value>
      <webElementGuid>16adf860-89f9-47e7-8fae-5f82e8587628</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
