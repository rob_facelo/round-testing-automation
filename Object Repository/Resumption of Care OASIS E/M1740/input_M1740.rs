<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_M1740</name>
   <tag></tag>
   <elementGuidId>29cb31cb-9a33-42ee-88f0-8abecdcd8252</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.ng-valid.ng-dirty.ng-valid-parse.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'checkbox' and @ng-model = 'oasis.oasisDetails.M1740_BD_6']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[110]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1d0e2352-14a2-4059-a277-0219f1dd5c5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>5609e2cb-6faf-4133-9471-710ce3ca1cb9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1740_BD_6</value>
      <webElementGuid>8619bdaa-b53f-4deb-9f21-c071f9755659</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1740_BD_6</value>
      <webElementGuid>5eacbf9e-c3c7-4345-8bf0-ff1cdede2ef4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>'1'</value>
      <webElementGuid>48bdbdb0-8b1b-43aa-a3dc-9b2e2ae2d830</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-false-value</name>
      <type>Main</type>
      <value>'0'</value>
      <webElementGuid>476d56eb-fd23-4016-949d-12772bd1c438</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.clearMItemFieldsValues($event,'input',['M1740'])</value>
      <webElementGuid>5c32fa5b-1c7b-4c8d-aa39-8ffd5a21f251</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>76f04f47-cc1e-42d6-ae0a-8de13b1fac4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-valid ng-dirty ng-valid-parse ng-touched</value>
      <webElementGuid>dbb7c122-39b0-4133-bb08-294cabb1e451</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1740&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;ng-valid ng-dirty ng-valid-parse ng-touched&quot;]</value>
      <webElementGuid>9c7ef793-ccb0-45c4-b1a5-b9ddfba994e8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[110]</value>
      <webElementGuid>ab00d2a5-ae7b-4eb9-a971-dd3dad855b4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1740']/div/div[2]/div[8]/input</value>
      <webElementGuid>ec73b593-96d6-4392-9291-d16d74d7f983</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[5]/div/div[2]/div/div/div/div[2]/div[8]/input</value>
      <webElementGuid>df1608f2-6e23-4041-8303-0197151d19a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>3206ab8d-2a58-4f9b-924b-3a30a0ba03d2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
