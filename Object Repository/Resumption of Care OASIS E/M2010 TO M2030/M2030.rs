<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M2030</name>
   <tag></tag>
   <elementGuidId>b598d6fc-ae2d-4b93-b641-d90354146a64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M2030 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2030']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>5a30b742-a15f-485c-ab6b-93c65aac0751</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>8062e377-bde8-4cbb-90bb-f0e6629ebdb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2030_CRNT_MGMT_INJCTN_MDCTN</value>
      <webElementGuid>dc873954-e7c1-4200-9ac9-e1280f6f1a48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN</value>
      <webElementGuid>3c47a5b6-9165-40e0-b0eb-0cf4b66b7131</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2030_CRNT_MGMT_INJCTN_MDCTN', oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN)</value>
      <webElementGuid>29757867-fe0a-4674-a02b-fda075e91fde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>(oasis.oasisDetails.M2000_DRUG_RGMN_RVW == '9' &amp;&amp; (oasis.oasisDetails.M0100_ASSMT_REASON == '01' || oasis.oasisDetails.M0100_ASSMT_REASON =='03')) || oasis.isLocked</value>
      <webElementGuid>74e236e0-eb5f-4cb6-b1b1-51c6d3d28175</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                </value>
      <webElementGuid>d98c4691-9882-4d4c-9623-4b24b680e83b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2030&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>f5c86aca-0b4b-42cc-b2f4-801f620f5793</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2030']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>1868fce0-773e-4d8a-aec5-1a3a6dbba45f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[56]/following::select[1]</value>
      <webElementGuid>42d8527c-f06c-48d4-aeb2-44bbba9320c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Excludes'])[3]/following::select[1]</value>
      <webElementGuid>a7e780c1-ef89-44d8-b3a3-65fe7d5f79a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to independently take the correct medication(s) and proper dosage(s) at the correct times.'])[1]/preceding::select[1]</value>
      <webElementGuid>dc18f329-2eda-4fa5-9f80-28d2724f0812</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to take injectable medication(s) at the correct times if:'])[1]/preceding::select[1]</value>
      <webElementGuid>a74b9d19-905e-4343-943e-8f3491ca52ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[14]/div/div[2]/div[7]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>bb23b0b8-ef9e-4b32-a11c-124694a1b63f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                ')]</value>
      <webElementGuid>09e414ab-c762-4724-8e07-d0051b8764ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
