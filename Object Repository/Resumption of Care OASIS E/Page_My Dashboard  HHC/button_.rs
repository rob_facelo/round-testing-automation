<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_</name>
   <tag></tag>
   <elementGuidId>0ce1591e-35a3-4c45-804c-b8b814b1d468</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#WhatsNewContent > div.modal-dialog.modal-lg > div.modal-content > div.modal-header > button.close</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[66]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d34479de-ddb7-4f1e-9f09-6dccbc7d3f6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5054cb1c-8d49-49f0-84ba-fa5b274a7482</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>close</value>
      <webElementGuid>d5805cb1-2f3d-43a8-b3cf-a8bc4e672da5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dismiss</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>e94f39e8-92e1-48e1-be46-25257fbb6d1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>21dae260-6706-48d2-8b62-22bed859791a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>×</value>
      <webElementGuid>bce72a83-98be-4ec8-9c60-1e13c115311e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;WhatsNewContent&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]/button[@class=&quot;close&quot;]</value>
      <webElementGuid>c2c18109-4912-4c49-b7c6-5149b36a6ec6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[66]</value>
      <webElementGuid>f1fe342d-3151-4144-a956-45c2274ad4e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='WhatsNewContent']/div/div/div/button</value>
      <webElementGuid>c2500516-abd7-4df3-89c9-e7581dbf404a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Don', &quot;'&quot;, 't show this content again.')])[1]/following::button[1]</value>
      <webElementGuid>045d1fb6-294d-48f6-a8bd-588d155cdb4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Referral Program'])[1]/following::button[1]</value>
      <webElementGuid>0dbf95a7-c9b6-4300-ad45-009c4a31aa4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVV is now available!'])[2]/preceding::button[1]</value>
      <webElementGuid>ff2c88f6-a338-404a-82c6-5e8c70372a02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/button</value>
      <webElementGuid>99f46c4c-0c0a-4124-9d09-d234f6108040</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '×' or . = '×')]</value>
      <webElementGuid>287e5e70-5ca1-43dd-b162-7a8303832d12</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
