<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1610</name>
   <tag></tag>
   <elementGuidId>6af2ec99-2ead-423c-9976-4b73fad84615</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1610_UR_INCONT']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M1610 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1610']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b54bc49b-9f27-4777-9345-bf66579f4f34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>ae61ea03-5a9a-4d79-8bb1-910a455ae03e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1610_UR_INCONT</value>
      <webElementGuid>d06c8bae-2d65-49c2-a76f-6fc4afb45705</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1610_UR_INCONT</value>
      <webElementGuid>4d775a88-ecf3-4aca-b38a-f2905b8b3949</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.clearMItemFieldsValues('M1610_UR_INCONT','select',['00,M1615','02,M1615'])</value>
      <webElementGuid>e4888a90-6a3c-405d-94bf-915c4f5e40c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>56c19e97-4c0b-4034-8ac4-b8e259f6efb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                
                                            </value>
      <webElementGuid>a1ce8f4e-bad0-485d-a795-6c263ebd6cec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1610&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>a2af36b3-9680-4e0c-86f6-bfcb7ccc242e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1610']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>20e0b37c-aa25-4c3a-91e5-3625470f8cfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[16]/following::select[1]</value>
      <webElementGuid>0d36432b-fb35-4edd-8f75-7eb87f53ca75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M1610) Urinary Incontinence or Urinary Catheter Presence:'])[1]/following::select[1]</value>
      <webElementGuid>5b80f92b-892b-4f9e-bfa2-cab96492d8a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No incontinence or catheter (includes anuria or ostomy for urinary drainage)'])[1]/preceding::select[1]</value>
      <webElementGuid>84ccc7f8-ed30-42a2-9665-29cf7918bac7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient is incontinent'])[1]/preceding::select[1]</value>
      <webElementGuid>2fe2cd6c-515f-4912-97e5-30a84d71b845</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[8]/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>501a2521-c8a8-4493-9f96-6e8662ee3606</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                
                                            ')]</value>
      <webElementGuid>fc3d3c75-1649-4b7f-acb4-a6122200f1de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
