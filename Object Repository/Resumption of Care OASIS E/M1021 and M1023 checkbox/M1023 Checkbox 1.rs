<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1023 Checkbox 1</name>
   <tag></tag>
   <elementGuidId>fb8a5824-c36d-42b6-a8e0-4b4761f433de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td[name=&quot;M1023&quot;] > div.form-inline.col-xs-12.col-sm-12.col-md-12.col-lg-12.col-col-xs-offset-2.col-sm-offset-2.col-md-offset-2.col-lg-offset-2 > input.ng-valid.ng-dirty.ng-valid-parse.ng-touched</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[57]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'checkbox' and @ng-model = 'oasis.oasisDetails.M0240_OTH_DIAG1_SEVERITY' and @ng-true-value = concat(&quot;'&quot; , &quot;01&quot; , &quot;'&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ed1dc086-e3b0-4500-a160-65ab7ad1f114</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>6ae741d4-28d0-4afc-953d-2fe68c60ae64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M0240_OTH_DIAG1_SEVERITY</value>
      <webElementGuid>8b5bd51f-e1da-4f30-9dce-a85d051378b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>'01'</value>
      <webElementGuid>fa9235ee-603d-4848-a605-c3521b17b8c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M0240_OTH_DIAG1_SEVERITY', oasis.oasisDetails.M0240_OTH_DIAG1_SEVERITY)</value>
      <webElementGuid>50b49474-6991-471e-84f9-16d3b39d4cd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked || oasis.oasisDetails.M0240_OTH_DIAG1_ICD == '='</value>
      <webElementGuid>0577e891-222a-47a9-9158-3bc5f40fc789</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-valid ng-dirty ng-valid-parse ng-touched</value>
      <webElementGuid>83264799-3ff9-4087-beb2-3c1045022fcc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1023&quot;)/td[2]/div[@class=&quot;form-inline col-xs-12 col-sm-12 col-md-12 col-lg-12 col-col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2&quot;]/input[@class=&quot;ng-valid ng-dirty ng-valid-parse ng-touched&quot;]</value>
      <webElementGuid>da267a3a-82a4-4c8e-8fd1-9649ba31e5cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[57]</value>
      <webElementGuid>e794d0cf-bc00-4954-b667-5a2d16dbe280</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='M1023']/td[2]/div[4]/input[2]</value>
      <webElementGuid>6e6bd68c-e0c6-4c93-bd22-e7352f41e18a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/input[2]</value>
      <webElementGuid>d7089fff-4023-4838-9d8e-7a16765f4a63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>0392f954-949b-4de5-8c6e-c2275b4ba997</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
