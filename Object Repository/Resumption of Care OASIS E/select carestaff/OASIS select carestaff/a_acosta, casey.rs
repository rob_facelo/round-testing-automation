<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_acosta, casey</name>
   <tag></tag>
   <elementGuidId>1407711c-95d8-4b7a-8310-d6ac0dfea15d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#searchCaregiverOasis > ul.dropdown-menu > li.ng-scope > a.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchCaregiverOasis']/ul/li/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = 'oasis.setCompletingCaregiver(caregiver.caregiver_name, caregiver.caregiver_id)' and (text() = 'acosta, casey ' or . = 'acosta, casey ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>be062998-e6d8-42fc-b617-54c960b75bdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.setCompletingCaregiver(caregiver.caregiver_name, caregiver.caregiver_id)</value>
      <webElementGuid>234f5e48-de3b-4bce-8efc-03d93e162288</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-binding</value>
      <webElementGuid>34b58ab6-cfff-47c5-80bc-0b3fd0f2fe9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>acosta, casey </value>
      <webElementGuid>fffc014f-e33e-4f94-bcdb-c007efc7b8d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchCaregiverOasis&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-binding&quot;]</value>
      <webElementGuid>5c5fae3b-4670-4c77-9816-6fabb5775141</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchCaregiverOasis']/ul/li/a</value>
      <webElementGuid>9574f96b-90be-4793-b5f3-73a490e57c3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'acosta, casey')]</value>
      <webElementGuid>446ebb80-b655-450a-ba6e-d07ec05f819a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Care Staff Name:'])[1]/following::a[1]</value>
      <webElementGuid>21768056-1350-49ff-b746-a9d5410d3884</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OT'])[1]/following::a[1]</value>
      <webElementGuid>47d589cd-7d0b-4a60-a2d4-65eca36e30a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0090. Date Assessment Completed'])[1]/preceding::a[1]</value>
      <webElementGuid>47e79073-2cb8-4b19-ad72-498c46870aa8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0100. This Assessment is Currently Being Completed for the Following Reason'])[1]/preceding::a[1]</value>
      <webElementGuid>3a5c892d-bcd0-471d-9308-574ea62ea298</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='acosta, casey']/parent::*</value>
      <webElementGuid>5e227a77-475a-4344-8c93-3fb65fa0e77b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div[2]/ul/li/a</value>
      <webElementGuid>59747bac-cf4d-496e-87d8-84ad135da75e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'acosta, casey ' or . = 'acosta, casey ')]</value>
      <webElementGuid>aaccc30e-2553-4a21-ae48-91c210b908c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
