<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_HR RequirementsGeoPayrates</name>
   <tag></tag>
   <elementGuidId>4d159f31-f103-4aaf-a196-120dcaa13734</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='ContentPlaceHolder_pgConCarestaffDetail_T1']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_pgConCarestaffDetail_T1 > div.tabTemplate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4c0064cc-e422-4736-ae1c-41f1da225fb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tabTemplate</value>
      <webElementGuid>6a37b4f3-7d71-4d3e-b205-c326d1211e8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    HR Requirements/Geo/Payrates
                </value>
      <webElementGuid>dea2c3f6-f8b9-47a4-950e-642b417cb4aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_pgConCarestaffDetail_T1&quot;)/div[@class=&quot;tabTemplate&quot;]</value>
      <webElementGuid>f95b52e3-ceaa-414d-9ea4-be7d2802b9c7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='ContentPlaceHolder_pgConCarestaffDetail_T1']/div</value>
      <webElementGuid>394891e3-e71f-40bb-8020-0587514a21bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Info'])[2]/following::div[1]</value>
      <webElementGuid>ef3abeb3-befc-4fe4-81c9-d0da0717db6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Info'])[1]/following::div[2]</value>
      <webElementGuid>19d92196-6b5d-4aa1-9b0c-e41b426df7d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HR Requirements/Geo/Payrates'])[2]/preceding::div[1]</value>
      <webElementGuid>769f2e28-68e2-4b45-ac05-3d8374f9ef92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Companies'])[1]/preceding::div[2]</value>
      <webElementGuid>a7f3d593-3386-43de-89ea-f2499a7730ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='HR Requirements/Geo/Payrates']/parent::*</value>
      <webElementGuid>b34b81c2-8387-4df2-8374-384cef8fd972</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/ul/li[5]/div</value>
      <webElementGuid>3aebe892-0d07-448b-8769-e1f2aa032f9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    HR Requirements/Geo/Payrates
                ' or . = '
                    HR Requirements/Geo/Payrates
                ')]</value>
      <webElementGuid>f017ba56-c9e8-42a6-9232-56d494e3e521</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
