<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_New</name>
   <tag></tag>
   <elementGuidId>80ada60f-4c0a-4790-913d-0e6dc983cf2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXCBtn0']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXCBtn0 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e0113786-e4a7-4a00-8e95-bd28825f4064</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New</value>
      <webElementGuid>f70e67e4-5459-45d7-9897-b88dc67fe839</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXCBtn0&quot;)/span[1]</value>
      <webElementGuid>f96d82b5-db79-4a27-a98b-ee52e152e99f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXCBtn0']/span</value>
      <webElementGuid>5ca9d152-a747-4c14-af94-32c0c8dca94c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payrate'])[1]/following::span[1]</value>
      <webElementGuid>c09854c1-d053-4efc-9b9f-77c51b6fedfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[4]/following::span[1]</value>
      <webElementGuid>33c23d0a-d241-41bd-ab71-693c95c7aca9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Type'])[1]/preceding::span[1]</value>
      <webElementGuid>98c72734-4c53-474b-8099-5e9159c26cb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay Rate'])[1]/preceding::span[2]</value>
      <webElementGuid>a7638e28-0371-4d2b-8002-deeff4305153</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/a/span</value>
      <webElementGuid>586dda22-ce23-4d99-a611-461d3e0b0c2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'New' or . = 'New')]</value>
      <webElementGuid>c848a94b-3251-48c7-aec9-01f63f19d849</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
