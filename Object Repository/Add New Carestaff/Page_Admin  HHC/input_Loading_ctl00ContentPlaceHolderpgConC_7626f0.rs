<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Loading_ctl00ContentPlaceHolderpgConC_7626f0</name>
   <tag></tag>
   <elementGuidId>373f15f5-6313-45ea-8340-f7d9fb8b7b04</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3_I']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3_I</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ccc65832-a4df-49d8-b869-dd9413a38291</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeEditArea_Moderno1 dxeEditAreaSys dxh0</value>
      <webElementGuid>db3755db-67cb-4368-b123-e4b3b9d8ce2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3_I</value>
      <webElementGuid>a59ce19c-86e8-4750-a738-fa4a28aec4dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolder$pgConCarestaffDetail$CarestaffCertGeoPay$grvPayrate$DXEditor3</value>
      <webElementGuid>9a4cb865-75ed-42d7-ab03-4769dafbd9bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onfocus</name>
      <type>Main</type>
      <value>ASPx.EGotFocus('ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3')</value>
      <webElementGuid>7cc3890c-030e-450a-9f01-e28909ef7b7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>ASPx.ELostFocus('ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3')</value>
      <webElementGuid>b3bbae72-0d96-4f41-86ba-9e7659405846</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ASPx.ETextChanged('ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3')</value>
      <webElementGuid>293cbb4c-9d7c-4e6d-9d4d-576fcad0dd16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d280ca4f-e2db-4ac2-87aa-0100b97445b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>5fb41fc5-9490-44c2-9753-c9d81f87c785</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3_I&quot;)</value>
      <webElementGuid>5a11bb84-15eb-4462-ae9b-f37abf918080</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3_I']</value>
      <webElementGuid>c533173e-8563-4ed0-89c5-f91d2f6d96f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3']/tbody/tr/td/input</value>
      <webElementGuid>d49196c3-fead-4eaf-a601-e7261689b3f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table[2]/tbody/tr/td/input</value>
      <webElementGuid>86d4ab79-d343-42cd-8e5c-82e507157b9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder_pgConCarestaffDetail_CarestaffCertGeoPay_grvPayrate_DXEditor3_I' and @name = 'ctl00$ContentPlaceHolder$pgConCarestaffDetail$CarestaffCertGeoPay$grvPayrate$DXEditor3' and @type = 'text']</value>
      <webElementGuid>d9dca9d6-c0e6-4a5e-964c-f364eb6878a1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
