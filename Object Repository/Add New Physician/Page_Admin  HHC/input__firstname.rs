<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__firstname</name>
   <tag></tag>
   <elementGuidId>512f8887-edef-4956-adeb-e1adc2bdd11e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='firstname']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#firstname</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>bdb5051f-1708-4869-9a0e-b6420a3a9da0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e5300af6-eb95-455c-a503-85f804f1a45c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>bb0c9fe2-3ba3-46d8-8079-4396587592bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>firstname</value>
      <webElementGuid>d095af48-ee6f-4caf-a984-a7d59cdf9e36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>First name*</value>
      <webElementGuid>4f7a8bb0-6ef5-4a99-9d30-2e7d522b2362</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>PhysicianCtrl.physDetails[0].First_Name</value>
      <webElementGuid>7279120e-8255-4250-9c36-0f669e66f894</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>30</value>
      <webElementGuid>55434cb6-b545-46dc-a314-a1038ffd8d97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;firstname&quot;)</value>
      <webElementGuid>acbe85a6-b6f2-424a-bd89-a2cfb6f1584d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='firstname']</value>
      <webElementGuid>a54ae48d-6a6c-4a25-bd7e-56404ca5431a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='physicianModal']/div/div/div[3]/div/div/div/div/div[2]/div/div[2]/input</value>
      <webElementGuid>5ba99962-8a7d-458c-b06d-5db4e7cab8fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/input</value>
      <webElementGuid>2ba8fe36-33df-4457-bc4c-8b06884cab91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'firstname' and @placeholder = 'First name*']</value>
      <webElementGuid>bc2f1c8d-8904-4831-b18a-3d88031f7195</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
