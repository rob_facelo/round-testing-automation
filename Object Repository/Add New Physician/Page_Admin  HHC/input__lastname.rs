<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__lastname</name>
   <tag></tag>
   <elementGuidId>3e3b860f-8111-4508-9575-29e055067d0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='lastname']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#lastname</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0e2a2e56-b93a-4ad0-bed0-cfcb06834a5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e691dc52-7742-4c7f-9166-ec7bc48bd5de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>1d3abf7c-c042-4d78-b129-3ab6a6dd2a8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>lastname</value>
      <webElementGuid>18d08533-12c0-4dd5-8bba-a0ed9cd12e9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Last name*</value>
      <webElementGuid>216ac790-2759-4796-ae66-9814d8b7abf3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>PhysicianCtrl.physDetails[0].Last_Name</value>
      <webElementGuid>76e5e34b-51a3-4c2b-8850-32188e035251</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>30</value>
      <webElementGuid>9c5bbd26-3996-43b6-b6eb-d0503a529b94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;lastname&quot;)</value>
      <webElementGuid>2627baff-199c-49bf-ace5-e72a691fd599</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='lastname']</value>
      <webElementGuid>62830d04-25df-4227-8492-4c627c022c37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='physicianModal']/div/div/div[3]/div/div/div/div/div[2]/div/div[4]/input</value>
      <webElementGuid>d400f3e7-6d52-4aed-a206-916986a8248c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/input</value>
      <webElementGuid>4d2f96e1-11a0-4816-be3e-f665b34fba3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'lastname' and @placeholder = 'Last name*']</value>
      <webElementGuid>b945ff7b-54bd-46d7-8cbc-31b3567265ec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
