<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save</name>
   <tag></tag>
   <elementGuidId>14268fdb-cf3e-4d64-9dc2-4f2e4bf803bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_PhysicianControl_PhysicianAddressView_DXEFL_DXCBtn1']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PhysicianControl_PhysicianAddressView_DXEFL_DXCBtn1 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0d2de582-6964-4d41-8982-aebbf48e52ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Save</value>
      <webElementGuid>6ae708e4-618e-45e8-96fd-0940357710d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PhysicianControl_PhysicianAddressView_DXEFL_DXCBtn1&quot;)/span[1]</value>
      <webElementGuid>9371ef38-7c55-4192-9060-270ae45ad421</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_PhysicianControl_PhysicianAddressView_DXEFL_DXCBtn1']/span</value>
      <webElementGuid>638d3246-1347-47c3-9c3e-2ebed1bb7f75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax:'])[1]/following::span[1]</value>
      <webElementGuid>fcbcbfa8-dd1c-4b2a-8f13-f5bd184585cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone 2:'])[1]/following::span[1]</value>
      <webElementGuid>36034ecf-b23c-48d0-ab41-dbe856bcea31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[2]/preceding::span[1]</value>
      <webElementGuid>9077dfe5-f1de-4d0b-b6a3-ae12aeacf550</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[7]/preceding::span[2]</value>
      <webElementGuid>5c4409e6-3afe-4066-9868-4d4c3d325146</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/div/div/a/span</value>
      <webElementGuid>45fb648a-d37c-4992-9191-24f506458489</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Save' or . = 'Save')]</value>
      <webElementGuid>cbabf2a5-2edd-469c-b161-8be8223c2c81</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
