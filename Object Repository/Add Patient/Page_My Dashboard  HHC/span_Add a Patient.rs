<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add a Patient</name>
   <tag></tag>
   <elementGuidId>900c9a66-2798-436a-8ac2-e24554215706</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#headerMenu_DXI2i1_T > span.dx-vam</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='headerMenu_DXI2i1_T']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e165cc47-abdf-45db-97eb-82cc0fec3189</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam</value>
      <webElementGuid>b9779d77-412a-4ee5-8f00-8fe5c454b93f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add a Patient</value>
      <webElementGuid>4bc7016e-2d0c-4248-a688-3ae1b922db93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;headerMenu_DXI2i1_T&quot;)/span[@class=&quot;dx-vam&quot;]</value>
      <webElementGuid>c8cadd87-62cb-4145-9aad-2eb60c43638c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='headerMenu_DXI2i1_T']/span</value>
      <webElementGuid>4c18870e-8f38-4a85-94d4-2722fdd063de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search for Patients'])[1]/following::span[1]</value>
      <webElementGuid>ea3170a0-68a2-4812-a7bc-32105f41ff7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assign Patient to Users'])[1]/following::span[2]</value>
      <webElementGuid>e2581ab4-01ab-4b0e-a702-c71fda4a3ef5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Intake Wizard'])[1]/preceding::span[1]</value>
      <webElementGuid>92477afb-bc6f-4b30-a723-ade949e3cb6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Compliance'])[1]/preceding::span[2]</value>
      <webElementGuid>ed37667a-5755-4494-8d2f-2989ee5810d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add a Patient']/parent::*</value>
      <webElementGuid>5cc1d190-d009-4c64-8c61-f69af632cdac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/ul/li[3]/a/span</value>
      <webElementGuid>fc87c466-e265-4e3f-8b2f-3f49232ad64f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Add a Patient' or . = 'Add a Patient')]</value>
      <webElementGuid>26f846f0-83f1-403d-82d2-47d2dd6bc7f5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
