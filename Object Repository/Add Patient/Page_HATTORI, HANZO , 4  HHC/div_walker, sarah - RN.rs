<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_walker, sarah - RN</name>
   <tag></tag>
   <elementGuidId>78046dfe-7f82-47ba-a77b-f6fe70d32897</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>fieldset > autocomplete-directive.ng-isolate-scope > div.dropdown > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='selectcarestaffadmissionpage']/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f35f9386-1431-492f-928e-3e349b96205f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>eb028a50-886c-44c9-b8ae-dd829a4c2a43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        walker, sarah - RN
                    </value>
      <webElementGuid>b0d43930-9082-4a6b-9e73-670c2ba19737</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectcarestaffadmissionpage&quot;)/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>7ac6a98c-c7d6-4492-90fa-ef51b98363bd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='selectcarestaffadmissionpage']/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>11b64f9d-f3f2-433d-a27d-152fdcaa59db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/following::div[7]</value>
      <webElementGuid>ce74512d-ac48-4972-a52a-fa4658a8e877</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[3]/following::div[7]</value>
      <webElementGuid>5c3d7a66-56cc-40e3-90ef-5f09a4726c9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[6]/preceding::div[3]</value>
      <webElementGuid>c8b0a975-120b-4041-b2e0-47b2d8e6accf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks:'])[2]/preceding::div[3]</value>
      <webElementGuid>a03a6ce7-1eb7-4da1-b81c-0c1dc5fa0ffe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>df3f4740-45c9-4bfe-9145-e1092371bb28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        walker, sarah - RN
                    ' or . = '
                        walker, sarah - RN
                    ')]</value>
      <webElementGuid>813bff56-488c-453c-90a6-3aa8838fe29e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
