<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Order Date_OrderDateNoValidation</name>
   <tag></tag>
   <elementGuidId>6fb68d27-ed3d-46ff-81ed-f3643bf0ab85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#CallerInformationOrderDate</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='CallerInformationOrderDate']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d124ce99-80ea-461f-80e5-4978f563bb2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>ptoCtrl.selectedCurrentOrderType == ''</value>
      <webElementGuid>5caedca8-cac7-4657-8f86-49c25367e1d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>OrderDateNoValidation</value>
      <webElementGuid>9090a0f4-3ba6-4519-a862-94e0e6596ea9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>429978a5-20f2-461c-9129-e8376a02f39c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>showtoday</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9204584f-31da-483b-827f-db0f1eddc0ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>2229e964-e3e7-43bb-a91c-b60a7db6d444</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-untouched ng-valid ng-scope ng-valid-pattern ng-valid-maxlength</value>
      <webElementGuid>3aef59df-b173-432a-9403-5ba0f6fec0be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>CallerInformationOrderDate</value>
      <webElementGuid>a5b62bd7-3585-42c5-9263-3709a7964396</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>ptoCtrl.ptoInput.CallDate</value>
      <webElementGuid>8444dc2a-f9b8-474d-a896-5e40fc4f9c9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>ptoCtrl.ptoPermission.IsSignCarestaff</value>
      <webElementGuid>2e9e0097-e9fe-41d0-bb07-82129ca2f94e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>7325ff89-8b7d-4554-a925-c5551f819134</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>model-view-value</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>4167e4cc-ce3c-4c68-8bdd-b78cf3b3114f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-pattern</name>
      <type>Main</type>
      <value>/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/</value>
      <webElementGuid>14542b87-ea44-48f4-bd5b-035d34336fe5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,false,'1')</value>
      <webElementGuid>93931b37-9bdf-4260-b32f-485fb28c595f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>32d351d7-4f30-45d1-83ce-9fc93cd2e7cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;CallerInformationOrderDate&quot;)</value>
      <webElementGuid>d1ab3a56-0047-4e19-b06c-f01e610fb26c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='CallerInformationOrderDate']</value>
      <webElementGuid>048dd667-3c6a-4f82-84ef-4296be820f3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[4]/div/div/div[2]/fieldset/div[3]/div/div[2]/div/input</value>
      <webElementGuid>42ddeccb-1140-4438-b66c-7b1f12693273</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/input</value>
      <webElementGuid>4f7c3770-ca1d-4033-a1ea-b09d7e5f5d4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'OrderDateNoValidation' and @type = 'text' and @id = 'CallerInformationOrderDate']</value>
      <webElementGuid>3c12bc13-0dbc-41cb-9ff1-73a1e34202ca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
