<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_walker, sarah SN -</name>
   <tag></tag>
   <elementGuidId>8711b3ce-6b0a-43cb-889e-7b20c7ca9a97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.form-group.form-group-marginZero.ng-scope > fieldset > autocomplete-directive.ng-isolate-scope > div.dropdown > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div[2]/div/div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6ca8fb2f-80a1-4e90-ae50-192bb7c655a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>1bc123a3-4db5-4ea7-9d94-dc5c5333dc4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        walker, sarah SN - 
                    </value>
      <webElementGuid>b3d4506a-d093-4138-bedd-cbd8d854c657</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-valid-mask ng-valid-maxlength ng-valid-pattern ng-valid-minlength ng-dirty ng-valid-parse ng-valid ng-valid-validation&quot;]/div[@class=&quot;row margin-top-10px text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;form-group form-group-marginZero ng-scope&quot;]/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>d7a7f793-23fc-493f-ac83-890957824f56</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div[2]/div/div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>1442a6e4-7863-46a7-b404-e5d06cf6f4aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Staff(s) for this order*:'])[1]/following::div[3]</value>
      <webElementGuid>93f99c12-299f-4662-85cf-a5f1bbd65ad9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[3]/preceding::div[3]</value>
      <webElementGuid>d1aa965e-a585-49ee-9443-4365830e41c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[4]/preceding::div[5]</value>
      <webElementGuid>f054ec6b-73c2-4eb4-9507-5a2cb7de5187</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='walker, sarah SN -']/parent::*</value>
      <webElementGuid>02b84cfe-06b2-410b-8775-c2e3cf950986</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>dfd6906f-6500-4db0-bc26-e75262d88937</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        walker, sarah SN - 
                    ' or . = '
                        walker, sarah SN - 
                    ')]</value>
      <webElementGuid>ccad6518-c8bb-4d86-9c32-b1cdc957c6a6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
