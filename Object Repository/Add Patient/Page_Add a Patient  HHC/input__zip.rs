<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__zip</name>
   <tag></tag>
   <elementGuidId>e67af000-55c1-40b4-8242-138ab5e2e99e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-9.col-lg-9 > autocomplete-directive.ng-isolate-scope > div.dropdown > input[name=&quot;zip&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@name='zip'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b55e45b3-3777-4eab-88bf-7d86476e6379</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>147af306-cfea-477a-9220-095a7e6b86f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>zip</value>
      <webElementGuid>6ba7b245-928e-4044-aba8-c1ba39821210</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm autoCompleteInput font14 ng-pristine ng-untouched ng-valid-mask ng-invalid ng-invalid-required ng-valid-maxlength</value>
      <webElementGuid>82ab3b4b-cebf-4c0f-ae69-0482643eede9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>943f084e-19b2-4aa5-b58b-6b69939845d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ui-mask</name>
      <type>Main</type>
      <value>99999</value>
      <webElementGuid>39efdb70-cd33-490e-bff0-d682215f5a6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ui-mask-placeholder-char</name>
      <type>Main</type>
      <value>space</value>
      <webElementGuid>ec4bd449-d04e-47f4-8093-8ea472eb0b3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>model</value>
      <webElementGuid>a154e8d8-5279-4ac0-b299-baf66919e818</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>click-outside</name>
      <type>Main</type>
      <value>closeThis()</value>
      <webElementGuid>23f8eb8c-10ad-486c-bcf8-2dd4dba43a89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>searchValue(model,searchtype)</value>
      <webElementGuid>80507d89-2be2-462d-a3a7-d31f0330b808</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-keydown</name>
      <type>Main</type>
      <value>selected=false</value>
      <webElementGuid>6ed1c378-ec20-4ceb-8211-e72f164bbda2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 500}</value>
      <webElementGuid>cdc119a9-7b7f-442c-8f98-86d6c4a054cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>disabled</value>
      <webElementGuid>0cf72d6a-32c6-48ff-8457-27880c68a17b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>checkofselected()</value>
      <webElementGuid>fc4d6895-6f5f-4e13-bcc1-04276b791e80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>a7f5a799-02be-4729-8604-d182360475f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-required</name>
      <type>Main</type>
      <value>isrequired</value>
      <webElementGuid>bb599d4f-a6b1-4e07-be9c-4e6eb840849f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>b27e195f-04cc-49b2-a009-bd8805891836</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/form[@class=&quot;pure-form ng-valid-mask ng-valid-maxlength ng-invalid ng-invalid-required ng-dirty ng-valid-parse ng-valid-pattern&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 fontsize-13-weight-600&quot;]/div[@class=&quot;row margin-top-10px&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-9 col-lg-9&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/input[@class=&quot;form-control input-sm autoCompleteInput font14 ng-pristine ng-untouched ng-valid-mask ng-invalid ng-invalid-required ng-valid-maxlength&quot;]</value>
      <webElementGuid>8f0ccc27-eb1f-4eaf-b353-75a3b185fdd6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@name='zip'])[2]</value>
      <webElementGuid>660cafcf-7b05-4233-a353-424515a7e1f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div/form/div/fieldset/div/div[3]/div/div/div[2]/div/div/div[6]/div[2]/autocomplete-directive/div/input</value>
      <webElementGuid>7db1fec9-3016-4fa1-8aaf-22999c01dc26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/autocomplete-directive/div/input</value>
      <webElementGuid>d39050f5-ae3b-4488-9948-c77b37fffc7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'zip']</value>
      <webElementGuid>043999a2-68cf-44d7-93e2-dfcdafda9bef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
