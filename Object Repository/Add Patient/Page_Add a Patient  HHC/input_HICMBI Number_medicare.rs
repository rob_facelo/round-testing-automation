<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_HICMBI Number_medicare</name>
   <tag></tag>
   <elementGuidId>8955ea77-7a4d-4dea-b896-0d2021cc830b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#basicInfoMedicare</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='basicInfoMedicare']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9e2f5f02-5625-4cd3-bb86-4da7c5f15fcc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9c4be96d-1c82-41ba-be1a-9e4493ca7066</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>medicare</value>
      <webElementGuid>204bd948-4b15-4576-8b0a-41e492ed443b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>basicInfoMedicare</value>
      <webElementGuid>7bc7cc59-af45-4208-a6a4-0ca25474da03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>12</value>
      <webElementGuid>08e26b2f-a4cc-4989-9f3b-982c3cc2bb52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-blur</name>
      <type>Main</type>
      <value>bsc.CheckMedicareFormat()</value>
      <webElementGuid>f9f6b3fd-301e-4bb1-8e8a-f2235bd615fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>bsc.medicareNumber('inputtext')</value>
      <webElementGuid>d5683b55-e529-41e4-b528-40f92a4c0276</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>bsc.CheckMedicareFormat($event)</value>
      <webElementGuid>711797c5-df82-477b-b96d-9f07c21acd22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>bsc.PatientInfo.MedicareNum</value>
      <webElementGuid>c835a19e-4bf1-4871-834f-a4e80b3db98d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-pattern</name>
      <type>Main</type>
      <value>/^[\w|\s|\-|\.|\,]{0,}$/</value>
      <webElementGuid>76371c69-33bf-4d04-acc8-c9d63546bc86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-untouched ng-valid ng-valid-pattern ng-valid-maxlength</value>
      <webElementGuid>698fbf0b-af22-4459-b0e1-c8317e1dcd5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>basic-addon3</value>
      <webElementGuid>7d88cf89-e78e-4348-800b-61b5f3ae47da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicInfoMedicare&quot;)</value>
      <webElementGuid>35ac6ce9-0902-4163-96f5-086b2f48fc69</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='basicInfoMedicare']</value>
      <webElementGuid>39aa3345-9db7-4a39-8c0e-fe1175d2cd00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div/form/div/fieldset/div/div[6]/div/div/div/div/input</value>
      <webElementGuid>b3933710-48a7-4e02-b5ac-df96917641e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div/div/input</value>
      <webElementGuid>15316c4d-855b-4819-a2e5-eb5d0e5d7319</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'medicare' and @id = 'basicInfoMedicare']</value>
      <webElementGuid>958c8d2b-a609-4c70-b108-ec45ade151d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
