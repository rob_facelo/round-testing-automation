<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_COMPTON</name>
   <tag></tag>
   <elementGuidId>f65113ae-a7c1-4d2f-bfd4-a15a93536d29</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td.font12.tdFlatPadding.verticalAlignMiddle.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div/form/div/fieldset/div/div[3]/div/div/div[2]/div/div/div[6]/div[2]/autocomplete-directive/div/div/table/tbody/tr/td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>59212882-b802-4e2b-80a7-83e384244b36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>font12 tdFlatPadding verticalAlignMiddle ng-binding</value>
      <webElementGuid>95bbfe0d-ab64-422c-8308-8386e5fa0c11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-bind</name>
      <type>Main</type>
      <value>item.city</value>
      <webElementGuid>015664d5-7a90-4dcf-ba30-b72f358b4454</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>COMPTON</value>
      <webElementGuid>b1d57a7b-bf49-4117-8097-e7badabfaafc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/form[@class=&quot;pure-form ng-valid-mask ng-valid-maxlength ng-invalid ng-invalid-required ng-dirty ng-valid-parse ng-valid-pattern&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 fontsize-13-weight-600&quot;]/div[@class=&quot;row margin-top-10px&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-9 col-lg-9&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/div[@class=&quot;dropdown-menu autocomplete-directive&quot;]/table[@class=&quot;table table-bordered table-responsive table-striped margin-bottom-sm&quot;]/tbody[1]/tr[@class=&quot;zipItem ng-scope&quot;]/td[@class=&quot;font12 tdFlatPadding verticalAlignMiddle ng-binding&quot;]</value>
      <webElementGuid>83f0f9e9-75b4-4643-b8d1-5f71f3475c03</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div/form/div/fieldset/div/div[3]/div/div/div[2]/div/div/div[6]/div[2]/autocomplete-directive/div/div/table/tbody/tr/td</value>
      <webElementGuid>da379249-7b4a-42b8-9253-efcc8ff65615</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip Code'])[2]/following::td[1]</value>
      <webElementGuid>c78f0721-5319-4c2c-bff3-132bd2a84855</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State'])[3]/following::td[2]</value>
      <webElementGuid>80258ad9-eca0-40c0-9ebb-ade26272280e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CA'])[1]/preceding::td[1]</value>
      <webElementGuid>12c628bd-aefc-4cdb-a887-cb9c9626c425</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EAST RANCHO DOMINGUEZ'])[1]/preceding::td[3]</value>
      <webElementGuid>0d0e06c4-d743-46f1-aca5-5ba148455751</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='COMPTON']/parent::*</value>
      <webElementGuid>22b25656-68a2-49d7-92ef-6061fa789575</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//autocomplete-directive/div/div/table/tbody/tr/td</value>
      <webElementGuid>83642f63-ec74-4dd6-8b68-c27df44f4b3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'COMPTON' or . = 'COMPTON')]</value>
      <webElementGuid>c482d714-db3a-4d73-a38c-e2485c307e66</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
