<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Social Security Number_basicInfoSSN</name>
   <tag></tag>
   <elementGuidId>3b6baaa2-d0d5-4e42-85fa-bdfdd53da606</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#basicInfoSSN</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='basicInfoSSN']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a73845f0-31e1-401c-a64b-02fa2717d363</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d924f9d2-a4ee-4820-93dc-942ef8236538</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>basicInfoSSN</value>
      <webElementGuid>3cc9d901-fb21-411c-bbe0-a36bbe804072</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>basicInfoSSN</value>
      <webElementGuid>df955634-ff4d-4a1a-a5c5-2ad6f00ea7f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-blur</name>
      <type>Main</type>
      <value>bsc.CheckSSN()</value>
      <webElementGuid>7c75d01c-4d7b-4b28-b809-22564aae379d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>9</value>
      <webElementGuid>0db94df3-b776-44ae-aea9-9b21dc5af32d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>bsc.PatientInfo.SSN</value>
      <webElementGuid>cfab50ca-1dd9-426c-b325-fe45445291b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>bsc.ssnValidation()</value>
      <webElementGuid>4fdba55c-8d55-46d5-80c2-b0664d8b42ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>bsc.ssnRequired()</value>
      <webElementGuid>cd9c6d95-17e1-4571-a935-471b945a1fbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>00ff026c-6dd9-4451-88c1-fc8cd654428c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>basic-addon3</value>
      <webElementGuid>fe639012-6ec7-434f-8688-22b6d81c85ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicInfoSSN&quot;)</value>
      <webElementGuid>78b66df3-ee0b-467a-b360-8a08545030d5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='basicInfoSSN']</value>
      <webElementGuid>2db30b4e-4aa3-4ce5-94a1-6c22e8a9ba9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div/form/div/fieldset/div/div[6]/div/div/div[2]/div/input</value>
      <webElementGuid>09ea5152-c002-42db-a65a-d3d9c5d70e62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div[2]/div/input</value>
      <webElementGuid>81538450-28c3-4dca-afa3-fd5b5e4a954c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'basicInfoSSN' and @name = 'basicInfoSSN']</value>
      <webElementGuid>da769696-926a-4c4b-b20c-4a91dc7b14d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
