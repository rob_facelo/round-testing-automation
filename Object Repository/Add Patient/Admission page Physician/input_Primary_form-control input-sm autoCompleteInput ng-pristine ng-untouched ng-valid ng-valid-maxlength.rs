<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Primary_form-control input-sm autoCompleteInput ng-pristine ng-untouched ng-valid ng-valid-maxlength</name>
   <tag></tag>
   <elementGuidId>2edf01f5-20f9-4129-9703-e44d6c3e5162</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#primaryphysician > autocomplete-directive.ng-isolate-scope > div.dropdown > input.form-control.input-sm.autoCompleteInput.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlength</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[16]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7fddbe11-f80b-431c-89e6-20566637e0c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>b4475c84-dbfb-441b-afb7-582a2b9bd7e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm autoCompleteInput ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>07e4ff18-18d1-43e7-9a50-2735043f1547</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Select Physician</value>
      <webElementGuid>7b445f8e-78b5-4e21-9366-25b45cee8a5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>model</value>
      <webElementGuid>c8c1da7c-7577-44fa-8c82-ec5c9f6262b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>click-outside</name>
      <type>Main</type>
      <value>closeThis()</value>
      <webElementGuid>29410122-9342-425b-96bf-191fd9ed46e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>searchValue(model,searchtype)</value>
      <webElementGuid>cc32bc43-99b0-4818-9ba9-99772de95594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-keydown</name>
      <type>Main</type>
      <value>selected=false</value>
      <webElementGuid>49c1d89e-25c8-4cb0-ad8e-03ff9f8f1c74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 500}</value>
      <webElementGuid>fb8c4730-3182-4662-ae99-1a44f98d78f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>disabled</value>
      <webElementGuid>7efc94ac-f197-4e29-aec7-ffef7e09ff7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>50</value>
      <webElementGuid>5426d3ac-f2a5-4c33-b5cf-a8edeef91ce4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>checkofselected()</value>
      <webElementGuid>0531fe13-86fb-4072-8856-b43684bec8ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>f70390f0-219c-41d6-9dc9-afdd51bd4d2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;primaryphysician&quot;)/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/input[@class=&quot;form-control input-sm autoCompleteInput ng-pristine ng-untouched ng-valid ng-valid-maxlength&quot;]</value>
      <webElementGuid>be920847-cbb6-403c-8242-4e980de38076</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[16]</value>
      <webElementGuid>5f472aaf-fa3e-4943-8428-9ee5c785e91d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='primaryphysician']/autocomplete-directive/div/input</value>
      <webElementGuid>af1c8bbb-cf83-4c6e-86a7-71ebc52f5370</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div[2]/div/div/div/autocomplete-directive/div/input</value>
      <webElementGuid>64e526a0-b472-407a-a298-f3dd420124f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = 'Select Physician']</value>
      <webElementGuid>89406508-7325-40c1-a5ac-a896f9bacf09</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
