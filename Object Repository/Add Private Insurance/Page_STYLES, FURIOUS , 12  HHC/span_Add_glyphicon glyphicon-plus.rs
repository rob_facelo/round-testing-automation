<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add_glyphicon glyphicon-plus</name>
   <tag></tag>
   <elementGuidId>dc32f18f-c9d9-4228-904a-60a0cece6434</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_PatientPayee_btnNew_CD']/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-plus</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>bd96ed67-6a0e-4ff0-8f66-c11c2954a960</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-plus</value>
      <webElementGuid>7819ab8f-36fb-4781-a48b-1aaed901295c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PatientPayee_btnNew_CD&quot;)/span[@class=&quot;dx-vam&quot;]/span[@class=&quot;glyphicon glyphicon-plus&quot;]</value>
      <webElementGuid>bf5cdfb2-70c1-48a9-b0ca-465bb6e4ffb2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_PatientPayee_btnNew_CD']/span/span</value>
      <webElementGuid>c180ab5e-d56c-4ef5-926a-c5b6fd60afbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/span/span</value>
      <webElementGuid>d58e9c97-b0ae-41b2-a750-79924bfcf91a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
