<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Loading_ContentPlaceHolder_PatientPayee_3bd735</name>
   <tag></tag>
   <elementGuidId>ab8c22d3-07ca-4c93-b317-f1b55645e4b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_1_0_ddlCareStaffType_B-1Img']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_1_0_ddlCareStaffType_B-1Img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>ca14007f-264b-4928-8b00-a8f25d7f66db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_1_0_ddlCareStaffType_B-1Img</value>
      <webElementGuid>1e8395b3-02d4-4c1c-8128-6845a7ca375c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxEditors_edtDropDown</value>
      <webElementGuid>95dbbb3c-e73e-4455-96c6-e7edfce7cec9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/DXR.axd?r=1_37-8cvGm</value>
      <webElementGuid>cdd83fe1-a8fc-4d9a-abb6-7655ce17cce4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>v</value>
      <webElementGuid>2277830b-3ec4-471b-bbca-328fd3e3d3c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_1_0_ddlCareStaffType_B-1Img&quot;)</value>
      <webElementGuid>c44eef38-8844-44e7-9bce-1610079c35c5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//img[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_1_0_ddlCareStaffType_B-1Img']</value>
      <webElementGuid>16e5857b-602f-4527-aa06-5f48309086f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_1_0_ddlCareStaffType_B-1']/img</value>
      <webElementGuid>2427db1b-e4db-4cf4-a149-146cf5e8df3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[@alt='v'])[7]</value>
      <webElementGuid>00b5cc24-f2e2-4c6b-bc0f-b2ebb20adb3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/img</value>
      <webElementGuid>9e1ed9d2-0a40-43a3-bac7-89865086d8ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@id = 'ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_1_0_ddlCareStaffType_B-1Img' and @src = '/DXR.axd?r=1_37-8cvGm' and @alt = 'v']</value>
      <webElementGuid>fed5b21e-99ec-48dd-86e7-dc0dc5985e11</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
