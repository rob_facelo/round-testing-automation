<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_ctl00ContentPlaceHolderLogon_023c19</name>
   <tag></tag>
   <elementGuidId>703a5d58-bac1-47e8-a439-7c948592ec23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolderLogonPage_LoginControl_loginbtn</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>36b31f76-4e33-4c77-9e0c-cfd65141d9ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>24d7f636-6eb2-4c35-91c4-2f4b5776f6cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolderLogonPage$LoginControl$loginbtn</value>
      <webElementGuid>c7fd2461-040f-481c-91e0-2e17121eeba7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>LOGIN</value>
      <webElementGuid>2fe49e2a-66de-4c85-8322-863006f1ba87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolderLogonPage_LoginControl_loginbtn</value>
      <webElementGuid>880198b6-2dbf-48f2-82d4-c32b622c06d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-btn-style btn btn-primary</value>
      <webElementGuid>48d55a88-f065-4b70-946b-ed5923cce6a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolderLogonPage_LoginControl_loginbtn&quot;)</value>
      <webElementGuid>6d7d3f87-520d-40b0-9ae6-b90c8a9ca4ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      <webElementGuid>aff86d21-b80c-454c-8d49-9c1cedd47462</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]/input</value>
      <webElementGuid>f41afefb-3da2-4c66-b189-3a89a97b1296</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>5a0a54bc-55db-4e78-a54e-83332e3de264</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'ctl00$ContentPlaceHolderLogonPage$LoginControl$loginbtn' and @id = 'ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      <webElementGuid>7b33712d-6136-4d07-ab3d-eee70bc3e553</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
