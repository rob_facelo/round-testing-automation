<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>save TAC detail</name>
   <tag></tag>
   <elementGuidId>bb55cd78-9760-43ec-80c7-dae49b5536a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_DXCBtn2']/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Save' or . = 'Save')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_DXCBtn2 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b5a7ad5e-0478-4a25-8cc4-a9b50761e626</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Save</value>
      <webElementGuid>fa06b1c2-6b5f-41ed-81ed-19ac31e62410</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_DXCBtn2&quot;)/span[1]</value>
      <webElementGuid>c3a82675-b243-4ded-b323-9c014eeff910</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_DXCBtn2']/span</value>
      <webElementGuid>a270c6c2-f85b-423b-825c-347a8de476ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EC Limit:'])[1]/following::span[1]</value>
      <webElementGuid>3b9521bb-9cdb-49e9-a6d2-788a9c644473</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Travel Time'])[1]/following::span[2]</value>
      <webElementGuid>24c86428-b34f-4095-86e7-47142a59b97c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[2]/preceding::span[1]</value>
      <webElementGuid>e82102f5-39fb-4f08-bb85-ff2f272808fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[6]/preceding::span[2]</value>
      <webElementGuid>48e19efc-6544-4c73-91ee-41927879b861</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Save']/parent::*</value>
      <webElementGuid>f6daed2e-5d71-4def-8c23-64e28b42a656</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/a/span</value>
      <webElementGuid>b11ceee3-d183-4be3-a4d0-675656185231</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Save' or . = 'Save')]</value>
      <webElementGuid>dfd2acf3-b029-4d04-8373-3b4511b928f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
