<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>whats new</name>
   <tag></tag>
   <elementGuidId>32c2421e-c3b3-4126-a2dd-b4094edf5b6b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[66]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#WhatsNewContent > div.modal-dialog.modal-lg > div.modal-content > div.modal-header > button.close</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>dd7dad8c-b700-4d42-801e-db1f7dcff98e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d10d8e8b-6178-48f9-8b87-ee4a9187f107</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>close</value>
      <webElementGuid>52d989c4-d9f3-4cfe-b7b9-b0650b515a47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dismiss</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>5762e2f2-3fad-4f93-ab1a-b0ad6a7a8621</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>ffbe6c7a-7938-4a15-9212-2dc6d5bddba0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>×</value>
      <webElementGuid>8128cf7f-eda9-4a8f-ac98-67bea66b41db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;WhatsNewContent&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]/button[@class=&quot;close&quot;]</value>
      <webElementGuid>91f42f93-6a20-456e-95e2-25943b0880c9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[66]</value>
      <webElementGuid>379d30c1-18f9-42a8-9c98-0268a2fed774</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='WhatsNewContent']/div/div/div/button</value>
      <webElementGuid>4d0bfb64-4c3d-4f29-8c7e-1b2f9e68186a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Don', &quot;'&quot;, 't show this content again.')])[1]/following::button[1]</value>
      <webElementGuid>7ffdc686-ff1d-421b-b85e-f3ed2c374c1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Referral Program'])[1]/following::button[1]</value>
      <webElementGuid>212d5e1c-b07f-4c4d-8030-a7623c02f63f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OASIS E Preview'])[3]/preceding::button[1]</value>
      <webElementGuid>fa7b4ccb-f188-4a6a-a2e9-2bfc863da882</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/button</value>
      <webElementGuid>bd14603b-1cd6-44fc-b711-2b5bf7b1ffe8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '×' or . = '×')]</value>
      <webElementGuid>ae4386da-7f79-493f-baf2-521bcd891dc8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
