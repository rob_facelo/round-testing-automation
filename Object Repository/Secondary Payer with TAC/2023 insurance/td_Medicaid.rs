<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Medicaid</name>
   <tag></tag>
   <elementGuidId>65d4b669-e6f8-473a-b447-1fe271c88e4c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_edit1_4_ddlInsurance_1_DDD_L_LBI19T0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_edit1_4_ddlInsurance_1_DDD_L_LBI19T0']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//td[@id = 'ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_edit1_4_ddlInsurance_1_DDD_L_LBI19T0' and (text() = 'Medicaid' or . = 'Medicaid')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>695e1aa0-7b70-4e55-bbb6-340a9f5fcd05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeListBoxItem dxeListBoxItemHover</value>
      <webElementGuid>1e69696a-ec09-4308-9419-6c73eabf8f35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_edit1_4_ddlInsurance_1_DDD_L_LBI19T0</value>
      <webElementGuid>6edf05e4-caa9-44db-adc2-6007b92093cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Medicaid</value>
      <webElementGuid>5946c31d-ff5f-4052-be4c-dd221e5f4005</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_edit1_4_ddlInsurance_1_DDD_L_LBI19T0&quot;)</value>
      <webElementGuid>17d1878b-6a93-47a5-9d25-21fd676f0cc8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_edit1_4_ddlInsurance_1_DDD_L_LBI19T0']</value>
      <webElementGuid>37ab3f28-79fe-4e08-9e7c-2e9d049e3572</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_edit1_4_ddlInsurance_1_DDD_L_LBT']/tbody/tr[20]/td</value>
      <webElementGuid>91eb0b8c-895b-47c9-9401-1fdc460e7443</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Medi-Cal'])[1]/following::td[1]</value>
      <webElementGuid>5b2ea897-04b5-4aac-90b4-ea0a2488b8c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MAXI INSURANCE'])[1]/following::td[2]</value>
      <webElementGuid>cf07713b-bcd1-4d7e-b8cd-f4941622e968</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Medicare'])[2]/preceding::td[1]</value>
      <webElementGuid>268f05fb-6fdc-4626-b04e-578f0c6bde1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MEDICARE ADVANTAGE'])[1]/preceding::td[2]</value>
      <webElementGuid>be008475-7fb1-4302-bcb9-bc4d03520528</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Medicaid']/parent::*</value>
      <webElementGuid>e55b93de-45bf-4b44-b1de-c00cc7f0eb31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[20]/td</value>
      <webElementGuid>5728e939-804b-45fa-bb7a-05042a6745a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_edit1_4_ddlInsurance_1_DDD_L_LBI19T0' and (text() = 'Medicaid' or . = 'Medicaid')]</value>
      <webElementGuid>2ab43850-05eb-4229-b773-0b6c73fd7aac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
