<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Limit_ctl00ContentPlaceHolderPatientP_57e7cd</name>
   <tag></tag>
   <elementGuidId>b8c3224e-4f44-4976-977a-5c4beb931692</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>20926f35-8621-49e4-bcdf-b9ec6fd3e1d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeEditArea dxeEditAreaSys</value>
      <webElementGuid>5461989b-e028-42ad-baa6-eb24ecc4e56f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I</value>
      <webElementGuid>f7ef6229-3914-44b5-90b5-7fc3c1f111f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolder$PatientPayee$pnlPatientPayee$grvPatientInsurance$dxdt1$TC$grvPatientInsuranceDetail$dxdt0$TC$grvTarDetail$DXPEForm$DXEFL$editnew_5$TC$txtLimit</value>
      <webElementGuid>2ba2b762-cc85-4790-ad8c-079d781095c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onfocus</name>
      <type>Main</type>
      <value>ASPx.EGotFocus('ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit')</value>
      <webElementGuid>3324e58b-cca1-41c8-a84a-1d05c439dbef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>ASPx.ELostFocus('ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit')</value>
      <webElementGuid>6f4f20a7-f611-4e04-aecd-c5f4388920f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ASPx.ETextChanged('ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit')</value>
      <webElementGuid>d9d7e560-1af9-48aa-9e0e-6b6554c5e67e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>71732cc9-06da-4ed0-91df-39c5bb053b15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9492fcf8-bed0-4ada-9627-5eca1f7c1934</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>acc49617-d277-45ee-baa9-100901a2749e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I&quot;)</value>
      <webElementGuid>a4dfcd6a-f697-4110-aa61-35344d99e02a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I']</value>
      <webElementGuid>2092e07f-c5ce-460d-aae6-173867fabf7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit']/tbody/tr/td/input</value>
      <webElementGuid>400a99c9-f510-478d-b120-aeb7f009f169</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/table/tbody/tr/td/input</value>
      <webElementGuid>61b4d722-39a2-4ced-99b2-500e8edc60ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I' and @name = 'ctl00$ContentPlaceHolder$PatientPayee$pnlPatientPayee$grvPatientInsurance$dxdt1$TC$grvPatientInsuranceDetail$dxdt0$TC$grvTarDetail$DXPEForm$DXEFL$editnew_5$TC$txtLimit' and @type = 'text']</value>
      <webElementGuid>cf9b9188-bb17-4b25-bd69-476685acb215</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
