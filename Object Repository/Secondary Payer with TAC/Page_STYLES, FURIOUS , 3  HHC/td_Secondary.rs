<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Secondary</name>
   <tag></tag>
   <elementGuidId>968b2e88-f503-4ac8-94cd-69d0b8b9f1db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0&quot;)[count(. | //*[@id = 'ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0' and (text() = 'Secondary' or . = 'Secondary')]) = count(//*[@id = 'ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0' and (text() = 'Secondary' or . = 'Secondary')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>c2a192df-8824-4cd5-8c55-a68a6af6d87f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeListBoxItem dxeListBoxItemHover dxeListBoxItemSelected</value>
      <webElementGuid>a39e1c26-f448-409e-8edd-f56fa41ffdbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0</value>
      <webElementGuid>45d71a67-0950-48df-b61d-66a8a2aa062a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Secondary</value>
      <webElementGuid>8c363ec2-88eb-4688-a142-7c0b1cf90255</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0&quot;)</value>
      <webElementGuid>4cfb94e3-c823-40ec-a1e0-ffb0eb7fe252</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0']</value>
      <webElementGuid>0a5d2735-b583-4b47-ae7e-64a249828e00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBT']/tbody/tr[2]/td</value>
      <webElementGuid>b206eea5-0d23-40b5-b363-1e8191744435</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Primary'])[1]/following::td[1]</value>
      <webElementGuid>850a9c7b-1311-4b3f-bf19-21c83977639d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[4]/following::td[7]</value>
      <webElementGuid>ce1305dc-b046-4daf-b20a-9f08c4d067ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Others'])[1]/preceding::td[1]</value>
      <webElementGuid>4133397e-3bd7-4e5a-abdf-df7d5eee8bb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Insurance:'])[1]/preceding::td[2]</value>
      <webElementGuid>e7ab455e-6e6d-47f7-af14-608b6f532a6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Secondary']/parent::*</value>
      <webElementGuid>88cd407c-fb02-4d63-8515-49dc93428cd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table[2]/tbody/tr[2]/td</value>
      <webElementGuid>acfbe54e-3d84-4803-847c-3d3210b6cd2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_DXEFL_editnew_3_ddlInsuranceType_DDD_L_LBI1T0' and (text() = 'Secondary' or . = 'Secondary')]</value>
      <webElementGuid>f61bcce2-5454-47d8-97b6-32b710ba328d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
