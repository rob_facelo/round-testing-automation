<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Limit                                  _dd8cba</name>
   <tag></tag>
   <elementGuidId>65b5eae8-1dd8-44a2-9365-82b1184adef8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_3']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_3 > div.dxflGroupCell_Moderno1 > div.dxflCLLSys.dxflItemSys.dxflTextEditItemSys.dxflItem_Moderno1.dxflElConSys</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>add9073b-4dca-4adc-81e5-6253c3d37921</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxflCLLSys dxflItemSys dxflTextEditItemSys dxflItem_Moderno1 dxflElConSys</value>
      <webElementGuid>29b48e88-fb74-44fb-858c-f9af76ce7828</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
														
															Limit:
														
															
                                                            
&lt;!--
(function(){var a = ({'numNegInf':'-∞','percentPattern':1,'numPosInf':'∞'});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

&lt;!--
ASPx.NumberDecimalSeparator = '.';

//-->

																
																	
																
															The number must be in the range 0...2147483647
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit',[[['dxeDisabled'],[''],['','I']]]);
ASPx.RemoveDisabledItems('ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit',[[['B-100','B-1','B-2','B-3','B-4'],]]);
document.getElementById(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientSpinEdit('ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit');
dxo.InitGlobalVariable('ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$PatientPayee$pnlPatientPayee$grvPatientInsurance$dxdt1$TC$grvPatientInsuranceDetail$dxdt0$TC$grvTarDetail$DXPEForm$DXEFL$editnew_5$TC$txtLimit';
dxo.KeyPress.AddHandler(function(s, e) { return blockNonNumbers(this, event, false, false); });
dxo.stateObject = ({'rawValue':'0'});
dxo.heightCorrectionRequired = true;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.styleDecoration.AddStyle('N','dxeNullText','');
dxo.nullText = 'Enter limit';
dxo.outOfRangeWarningClassName='dxeOutOfRWarn dxeOutOfRWarnRight';
dxo.outOfRangeWarningMessages=['The number must be in the range {0}...{1}', 'The number must be greater than or equal to {0}', 'The number must be less than or equal to {0}'];
dxo.allowMouseWheel = false;
dxo.allowNull = false;
dxo.number = 0;
dxo.maxValue = 2147483647;
dxo.decimalPlaces = 2;
dxo.AfterCreate();

//-->

                                                        
														
													</value>
      <webElementGuid>70f3c372-593e-4c91-bf72-fc55767a350d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_3&quot;)/div[@class=&quot;dxflGroupCell_Moderno1&quot;]/div[@class=&quot;dxflCLLSys dxflItemSys dxflTextEditItemSys dxflItem_Moderno1 dxflElConSys&quot;]</value>
      <webElementGuid>2b4f96c8-2f3d-4335-bd72-e53ad2d09ecc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_3']/div/div</value>
      <webElementGuid>8dffdb59-666b-4fe1-9261-5969749ee59e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Units'])[1]/following::div[3]</value>
      <webElementGuid>f532992a-9c26-4b9c-b983-74baf89ff529</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visits'])[1]/following::div[3]</value>
      <webElementGuid>7f3dcfc1-73da-4068-b8f5-14aaa08c5565</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div/div/div[4]/div/div</value>
      <webElementGuid>97ce1570-6cf2-408a-8301-4284a3d52497</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
														
															Limit:
														
															
                                                            
&lt;!--
(function(){var a = ({&quot; , &quot;'&quot; , &quot;numNegInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;-∞&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;percentPattern&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;numPosInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;∞&quot; , &quot;'&quot; , &quot;});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

&lt;!--
ASPx.NumberDecimalSeparator = &quot; , &quot;'&quot; , &quot;.&quot; , &quot;'&quot; , &quot;;

//-->

																
																	
																
															The number must be in the range 0...2147483647
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;I&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;B-2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;B-3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;B-4&quot; , &quot;'&quot; , &quot;],]]);
document.getElementById(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientSpinEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$PatientPayee$pnlPatientPayee$grvPatientInsurance$dxdt1$TC$grvPatientInsuranceDetail$dxdt0$TC$grvTarDetail$DXPEForm$DXEFL$editnew_5$TC$txtLimit&quot; , &quot;'&quot; , &quot;;
dxo.KeyPress.AddHandler(function(s, e) { return blockNonNumbers(this, event, false, false); });
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;0&quot; , &quot;'&quot; , &quot;});
dxo.heightCorrectionRequired = true;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;N&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeNullText&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.nullText = &quot; , &quot;'&quot; , &quot;Enter limit&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The number must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The number must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The number must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.allowMouseWheel = false;
dxo.allowNull = false;
dxo.number = 0;
dxo.maxValue = 2147483647;
dxo.decimalPlaces = 2;
dxo.AfterCreate();

//-->

                                                        
														
													&quot;) or . = concat(&quot;
														
															Limit:
														
															
                                                            
&lt;!--
(function(){var a = ({&quot; , &quot;'&quot; , &quot;numNegInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;-∞&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;percentPattern&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;numPosInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;∞&quot; , &quot;'&quot; , &quot;});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

&lt;!--
ASPx.NumberDecimalSeparator = &quot; , &quot;'&quot; , &quot;.&quot; , &quot;'&quot; , &quot;;

//-->

																
																	
																
															The number must be in the range 0...2147483647
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;I&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;B-2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;B-3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;B-4&quot; , &quot;'&quot; , &quot;],]]);
document.getElementById(&quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientSpinEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_PatientPayee_pnlPatientPayee_grvPatientInsurance_dxdt1_grvPatientInsuranceDetail_1_dxdt0_1_grvTarDetail_0_DXPEForm_0_DXEFL_0_editnew_5_0_txtLimit&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$PatientPayee$pnlPatientPayee$grvPatientInsurance$dxdt1$TC$grvPatientInsuranceDetail$dxdt0$TC$grvTarDetail$DXPEForm$DXEFL$editnew_5$TC$txtLimit&quot; , &quot;'&quot; , &quot;;
dxo.KeyPress.AddHandler(function(s, e) { return blockNonNumbers(this, event, false, false); });
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;0&quot; , &quot;'&quot; , &quot;});
dxo.heightCorrectionRequired = true;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;N&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeNullText&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.nullText = &quot; , &quot;'&quot; , &quot;Enter limit&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The number must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The number must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The number must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.allowMouseWheel = false;
dxo.allowNull = false;
dxo.number = 0;
dxo.maxValue = 2147483647;
dxo.decimalPlaces = 2;
dxo.AfterCreate();

//-->

                                                        
														
													&quot;))]</value>
      <webElementGuid>4f56b1bb-97e9-4818-b344-c3c5b9613a65</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
