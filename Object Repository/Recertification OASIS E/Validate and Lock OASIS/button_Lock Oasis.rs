<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Lock Oasis</name>
   <tag></tag>
   <elementGuidId>de1c6023-f2da-43e6-9306-c3ed8d5c0697</elementGuidId>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[55]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'oasis.lockOasis()' and (text() = '
                
                Lock Oasis
            ' or . = '
                
                Lock Oasis
            ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>be20a50f-624e-4eea-8cfe-6cc70b0f0514</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>57d5598c-b022-4303-88f7-df72dfa12c6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btn-sm toolbar-btn-default font12 ng-scope</value>
      <webElementGuid>d6f385e5-2d08-4e21-a13d-3e9e036f5392</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.lockOasis()</value>
      <webElementGuid>653071f0-1dc2-4018-a856-08c34a7d2410</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>(!oasis.isLocked &amp;&amp; ((oasis.isInUse &amp;&amp; oasis.oasisDetails.inuse_by_user_id == oasis.userCredentials.UserId) || !oasis.isInUse)) &amp;&amp; !oasis.isViewOtherOasisVersion</value>
      <webElementGuid>fe4e9403-1189-4027-90d6-7e3856e40fb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>!oasis.permissions.LockUnlock || oasis.isOnSaveState || oasis.isValidating</value>
      <webElementGuid>063d56c4-1ea6-411b-a5f9-34d404124235</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                Lock Oasis
            </value>
      <webElementGuid>d37a1814-1494-4610-8ece-9cc9ca47c04f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 header_nav_container_oasis&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]</value>
      <webElementGuid>a6475614-4f1d-48d4-9b54-435e9c5ff763</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[55]</value>
      <webElementGuid>f98fb394-30e8-45aa-90fe-1fcc0a7a6c33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[3]</value>
      <webElementGuid>3729949f-4825-4ca0-9280-d94056f10dd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PPS Calculator'])[1]/preceding::button[1]</value>
      <webElementGuid>23b90e36-7242-40bc-afce-85ad23b32afa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDGM Calculator'])[1]/preceding::button[2]</value>
      <webElementGuid>566f2667-4ce6-4998-a9a9-384b4ee5f5a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lock Oasis']/parent::*</value>
      <webElementGuid>0aaef85d-8436-46f5-9a8f-ef954b0b8163</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]</value>
      <webElementGuid>9c79d557-0842-4645-9c66-c494b845f9c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                
                Lock Oasis
            ' or . = '
                
                Lock Oasis
            ')]</value>
      <webElementGuid>7c8f4e87-110c-42cd-8c5f-74c4c2d87106</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
