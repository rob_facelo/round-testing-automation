<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Lock Oasis</name>
   <tag></tag>
   <elementGuidId>6a136dce-08f5-4f14-af65-2b7db9dbe1a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/following::h4[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Lock Oasis' or . = ' Lock Oasis')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal.fade.ng-scope.in > div.modal-dialog.modal-sm > div.modal-content > div.modal-header.hhc-blue > h4.modal-title.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>2f4b27f3-1f0d-4f9f-9dae-07d0d129a348</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-title ng-binding</value>
      <webElementGuid>899a4ffb-4037-49b3-ba3a-ca491f92d8e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Lock Oasis</value>
      <webElementGuid>6efa15c0-4fb2-4ae0-a017-777ebdd10c31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header hhc-blue&quot;]/h4[@class=&quot;modal-title ng-binding&quot;]</value>
      <webElementGuid>2751d813-baf9-49be-99ee-223dade6676f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/following::h4[1]</value>
      <webElementGuid>5d5450d1-3d6a-427b-899d-e96a0d1ea729</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock OASIS'])[1]/following::h4[1]</value>
      <webElementGuid>d023b51f-126c-4cae-bb44-61d172334577</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[2]/preceding::h4[1]</value>
      <webElementGuid>3a1317d5-1c4a-47ea-943a-94a13cef1fc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock OASIS'])[2]/preceding::h4[1]</value>
      <webElementGuid>0b2f4143-65ce-490d-8cb7-e8ce1104be72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/h4</value>
      <webElementGuid>50510efa-4607-476b-b7db-6d29777e6705</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = ' Lock Oasis' or . = ' Lock Oasis')]</value>
      <webElementGuid>37dbe677-921d-4b25-925a-d7cd54cb257d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
