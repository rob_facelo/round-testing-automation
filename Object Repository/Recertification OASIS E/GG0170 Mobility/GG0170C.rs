<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0170C</name>
   <tag></tag>
   <elementGuidId>b25c140e-33a2-437c-8601-569e283beaac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0170C4' and (text() = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ' or . = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>ca699fe6-4345-486d-93be-d1f4857b8dae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>1d6ba5ca-e635-4eab-8520-fc6a2700a12b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170C4</value>
      <webElementGuid>22265de9-ec72-4804-a973-304fdba5d73f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>edc7ccaa-639f-49f6-a028-8053a794cf35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170C4</value>
      <webElementGuid>997261da-082c-4823-a15e-85df6f1d8b57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0170C4', oasis.oasisDetails.GG0170C4)</value>
      <webElementGuid>195bb745-4b3b-4716-b7ec-ecf8d30071f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>50afba35-b334-4c5b-a02a-cb5dc8730835</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            </value>
      <webElementGuid>18ec8835-90ae-4c15-9b6b-55a5d80bad31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-11 col-sm-11 col-md-11 col-lg-11&quot;]/div[@class=&quot;col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>1cdca128-2064-4a73-90b7-bb8f70d61449</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      <webElementGuid>e87d3a99-ac1b-4587-a66e-518185972ee3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Sit to lying:'])[1]/following::select[1]</value>
      <webElementGuid>dac96019-5f5f-4164-8031-7ff81fa35431</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lying to sitting on side of bed:'])[1]/preceding::select[1]</value>
      <webElementGuid>b1d30de4-0b5a-4d91-9b77-b43daa796612</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sit to stand:'])[1]/preceding::select[2]</value>
      <webElementGuid>d4d52fd1-7fab-4861-a258-45d10489e727</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      <webElementGuid>6b602ff2-c12b-4fdb-9d1a-c55dcb8525f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ' or . = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ')]</value>
      <webElementGuid>8010ea4d-488f-4127-bbe4-d6dfd5ea6843</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
