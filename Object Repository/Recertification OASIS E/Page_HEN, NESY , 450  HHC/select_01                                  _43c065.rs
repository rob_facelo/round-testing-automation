<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_01                                  _43c065</name>
   <tag></tag>
   <elementGuidId>6e1dca31-58cd-4866-9fe0-0fde4c85c632</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-offset-4.col-sm-offset-4.col-md-offset-4.col-lg-offset-4.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>632bd65a-f949-4c7e-9409-f6c153f5d1f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>251a8b49-8f8b-4754-b442-104fde96d1c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0130A4</value>
      <webElementGuid>51b55c2a-cc96-419b-8cce-cd1122fc584a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>6b7e7f4e-261e-4d2c-9ffe-2ae11fcebe72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0130A4</value>
      <webElementGuid>cc542e24-1989-4a6b-9824-5e547d204547</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0130A4', oasis.oasisDetails.GG0130A4)</value>
      <webElementGuid>2d8ab82b-8467-4ffc-810a-a4a39bb31d0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>8ce8a802-1ddb-4733-b377-2b6b8fa00b38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            </value>
      <webElementGuid>fc585a79-8d31-4776-be89-fe78edcff4db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0130&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-11 col-sm-11 col-md-11 col-lg-11&quot;]/div[@class=&quot;col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>af357007-fe54-4508-8289-50e6586799d9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/select</value>
      <webElementGuid>159a9386-10bd-4b18-8bf1-25698910d854</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[11]/following::select[1]</value>
      <webElementGuid>f0f193c5-ac1e-4994-9675-6a40869eca1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Eating:'])[1]/preceding::select[1]</value>
      <webElementGuid>2e37726f-e242-4a56-b7e0-f45ecf77d40d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oral Hygiene:'])[1]/preceding::select[2]</value>
      <webElementGuid>8f1c348b-78b0-42f2-b995-71925c06c680</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div/div/div/select</value>
      <webElementGuid>301cccc7-a6c2-4735-a943-0f2807793619</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ' or . = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ')]</value>
      <webElementGuid>12e1c69d-ed04-4c5e-8a95-417c9846d504</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
