<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_M0Set Lookup                           _81d67f</name>
   <tag></tag>
   <elementGuidId>6c255289-d6d6-410b-a709-aafc39e452e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row.footer_nav_container_oasis > div.col-xs-12.col-sm-12.col-md-12.col-lg-12</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>06057b87-f9d6-4b34-8818-86564037d89d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xs-12 col-sm-12 col-md-12 col-lg-12</value>
      <webElementGuid>3da6db76-989f-46e4-9caf-cb26cf25a9a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        </value>
      <webElementGuid>b47e024e-e6d4-460a-99f4-5a957f174253</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]</value>
      <webElementGuid>b11349e5-5518-4a6e-9921-b04bfdb641db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div</value>
      <webElementGuid>9c8ac774-2e8b-4026-82b7-d6ab8cd4b888</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Current Condition'])[1]/following::div[4]</value>
      <webElementGuid>a4538e0b-2dc0-48ef-983f-79b41e4bc827</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient Current Status'])[1]/following::div[8]</value>
      <webElementGuid>a15d7849-343d-427f-825c-060f3953d3a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div[7]/div</value>
      <webElementGuid>ee543292-0246-4303-bce8-bc774b7190d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        ' or . = '
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        ')]</value>
      <webElementGuid>d8a7f296-c428-4a7a-bdaa-59fc04c2faf1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
