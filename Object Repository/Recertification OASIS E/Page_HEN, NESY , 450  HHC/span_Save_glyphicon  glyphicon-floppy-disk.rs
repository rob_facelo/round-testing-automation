<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save_glyphicon  glyphicon-floppy-disk</name>
   <tag></tag>
   <elementGuidId>af103091-b24f-4d90-8ac6-3dc658ff8ee8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-floppy-disk</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div/div/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0503000c-6764-47cc-866b-7b9040736722</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon  glyphicon-floppy-disk</value>
      <webElementGuid>51be8fdd-5333-4d17-ba0f-fbbb935ab16b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-valid-mask ng-valid-maxlength ng-valid-minlength ng-dirty ng-valid-validation ng-valid-parse ng-valid ng-valid-pattern&quot;]/div[@class=&quot;form-actions&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-9 col-lg-9&quot;]/div[@class=&quot;btn-group&quot;]/button[@class=&quot;btn btn-default&quot;]/span[@class=&quot;glyphicon  glyphicon-floppy-disk&quot;]</value>
      <webElementGuid>27b7d0c3-4781-4dc2-b9ea-29fa3a895fb2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div/div/div/div/button/span</value>
      <webElementGuid>d6964b7e-914e-41e0-8c0d-2ea2964fb7e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ng-form/div/div/div/div/button/span</value>
      <webElementGuid>474825fb-3710-4649-b4cb-de69c7991ac8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
