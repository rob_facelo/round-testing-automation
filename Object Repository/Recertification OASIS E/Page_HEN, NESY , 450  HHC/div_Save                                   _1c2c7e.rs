<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Save                                   _1c2c7e</name>
   <tag></tag>
   <elementGuidId>86521db2-cfdd-43d2-abc9-399970aa44a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > div.row.ng-scope</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3d0ee693-5480-416c-a084-a810b2329032</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row ng-scope</value>
      <webElementGuid>d54b0741-d9c9-4e8c-8fac-868abc11e307</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-controller</name>
      <type>Main</type>
      <value>OasisController as oasis</value>
      <webElementGuid>792dddfb-ac58-439a-b65a-60939c2306ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
                PPS Calculator
            
            
                PDGM Calculator
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
                
                OBQI Measure
            
            
                
                FRA
            
            
                
                HRA
            
            
                
                Upload File
            
            
            
                
                Fax Oasis
            
            
            
            

        
    

    
        Recertification (follow-up) assessment (REC: 01/30/2023 - 03/30/2023) (Oasis E)
    
    
        450 - HEN, NESY 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            

                
                    
                        
                            
                            
                                
                                    A - ADMINISTRATIVE INFORMATION
                                
                                    G - FUNCTIONAL STATUS
                                
                                    GG - FUNCTIONAL ABILITIES AND GOALS
                                
                                    J - HEALTH CONDITIONS
                                
                                    M - SKIN CONDITIONS
                                
                                    ACTIVE DIAGNOSES
                                
                            
                        
                    
                

            
            
                
                    

                        
                        
                            
    
        
            SECTION A - ADMINISTRATIVE INFORMATION
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                                
                                
                                
                                    
                                    Non Visit/Non Billable Episode
                                
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Suffix:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

        
            

                
                    
                    
                        LEGAL REPRESENTATIVE
                        

                            
                                
                                    
                                        Name:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Contact No.:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Email Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Relationship:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                
                                
                                    
                                         DPOA           
                                         Conservator
                                    
                                
                            

                        
                    
                

                
                    
                    
                        PATIENT CONTACT PERSON
                        

                            
                                
                                    
                                        Name:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Home Phone:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Mobile Phone:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Email Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Relationship:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                        
                    
                

            
        

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        

        
        

        
        
            
            
                
                    M0150. Current Payment Sources for Home Care
                    
                    ↓ Check all that apply
                
                
                    
                        
                            
                                
                                0. None; no charge for current services
                            
                        
                        
                            
                                
                                4. Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8. Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1. Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5. Workers' compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2. Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6. Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10. Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7. Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11. Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK. Unknown
                            
                        
                    
                
            
        

        
        

        
        
            COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

            
                
                    
                        M0080 Discipline of Person Completing Assessment. 
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        1
                                        2
                                        3
                                        4
                                    
                                
                            
                            
                                
                                    
                                        1.
                                        RN
                                    
                                    
                                        2.
                                        PT
                                    
                                    
                                        3.
                                        SLP/ST
                                    
                                    
                                        4.
                                        OT
                                    
                                
                            
                        
                        
                            
                                Care Staff Name:
                                
                                    
                                    
                                    
                                    
                                        
                                    
                                
                            
                        
                    
                
            

             
        

        
        
             

            
                
                    
                        M0090. Date Assessment Completed 
                    
                    
                        
                            
                                
                                    
                                        
                                        
                                    
                                
                            
                        
                    
                
            

            
            EMERGENCY PREPAREDNESSPriority Code:   Patient has an Advance Directives Order:   Yes No Time In:    Time Out:     Emergency Contact Information:  Name:   Relationship:    Phone:   Email:   Address:   
        

        
        
            
            

            
                
                    
                        M0100. This Assessment is Currently Being Completed for the Following Reason 
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        
                                        4
                                        5
                                        
                                        
                                        
                                        
                                    
                                
                            
                            
                            
                                
                                    Follow-Up
                                
                                
                                    
                                        
                                            4.
                                            Recertification (follow-up) reassessment
                                        
                                        
                                            5.
                                            Other follow-up
                                        
                                    
                                
                            
                            
                            
                        
                    
                
            

            
        

        
        
        

        
        

        
        

        
        
            

            
                
                    
                        M0110. Episode Timing 
                        
                        Is the Medicare home health payment episode for which this assessment will define a case mix group an “early” episode or a “later” episode in the patient’s current sequence of adjacent Medicare home health payment episodes?
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    
                                
                            
                            
                                
                                    
                                        1.
                                        Early
                                    
                                    
                                        2.
                                        Later
                                    
                                    
                                        UK.
                                        Unknown
                                    
                                    
                                        NA.
                                        Not Applicable: No Medicare case mix group to be defined by this assessment.
                                    
                                
                            
                        
                    
                
            

            
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

    



    
        
            SECTION G - FUNCTIONAL STATUS
        
    
    

    

        
        

            
                
                    
                        Mental Status (goes to POX Box #19)
                    
                    
                        
                             1 - Oriented
                        
                        
                             2 - Comatose
                        
                        
                             3 - Forgetful
                        
                        
                             4 - Depressed
                        
                        
                             5 - Disoriented
                        
                        
                             6 - Lethargic
                        
                        
                             7 - Agitated
                        
                        
                             8 - Other
                            
                        
                    
                
            
        

        
        

            
                
                    
                        Functional Limitation (goes to POC Box #18A)
                    
                    
                        
                             1 - Amputation
                        
                        
                             2 - Bowel/Bladder
                        
                        
                             3 - Contracture
                        
                        
                             4 - Hearing
                        
                        
                             5 - Paralysis
                        
                        
                             6 - Endurance
                        
                        
                             7 - Ambulation
                        
                        
                             8 - Speech
                        
                        
                             9 - Legally Blind
                        
                        
                             A - Dyspnea With Minimal Exertion
                        
                        
                             B - Other
                            
                        
                    
                
            

        

        
        
            

            
                
                    
                        M1800 Grooming

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to tend safely to personal hygiene needs (specifically: washing face and hands, hair care, shaving or make up,
                        teeth or denture care, fingernail care).
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to groom self unaided, with or without the use of assistive devices or adapted methods.
                                
                                
                                    1.
                                    Grooming utensils must be placed within reach before able to complete grooming activities.
                                
                                
                                    2.
                                    Someone must assist the patient to groom self.
                                
                                
                                    3.
                                    Patient depends entirely upon someone else for grooming needs.
                                
                            
                        
                    
                
            

            
        

        
        
            MAHC 10 - Fall Risk Assessment ToolRequired Core ElementsAssess one point for each core element &quot;yes&quot;.Information may be gathered from medical record, assessment and if applicable, the patient/caregiver. Beyond protocols listed below, scoring should be based on your clinical judgment.  Points Age 65+   Diagnosis (3 or more co-existing)   Includes only documented medical diagnosis Prior history of falls within 3 months   An unintentional change in position resulting in coming to rest on the ground or at a lower level Incontinence   Inability to make it to the bathroom or commode in timely manner Includes frequency, urgency, and/or nocturia. Visual impairment   Includes but not limited to, macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription. Impaired functional mobility   May include patients who need help with IADLS or ADLS or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices. Environmental hazards   May include but not limited to, poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits. Poly Pharmacy (4 or more prescriptions – any type)   All PRESCRIPTIONS including prescriptions for OTC meds. Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs. Pain affecting level of function   Pain often affects an individual’s desire or ability to move or pain can be a factor in depression or ompliance with safety recommendations. Cognitive impairment   Could include patients with dementia, Alzheimer’s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patients ability to adhere to the plan of care. A score of 4 or more is considered at risk for falling Total   Missouri Alliance for HOME CARE2420 Hyde Park, Suite A, Jefferson City, MO 65109-4731  /  (573) 634-7772  /  (573) 634-4374 Fax MUSCULOSKELETAL No ProblemDisorders of musculoskeletal system:   textAreaAdjust(false,12747); Fracture (location)    Swollen, painful joints (specify)    Contractures: Joint    Location   Hand Grips:   Equal Unequal   Strong Weak  specify   Dominant side:   R L Motor Changes:  Fine Gross  specify    Weakness:  UE LE  details    Atrophy        Paresthesia      Shuffling Wild-based gait Amputation: BK  R  L  AK  R  L  UE R L Specify:    textAreaAdjust(false,9292); Other (specify)  textAreaAdjust(false,9309);How does the patient's condition affect their function ability and safety?    MENTAL STATUSDescribe the mental status of the patient:  Include the patient's general appearance, behaviors, emotional responses, mental functioning and their overall social interaction. Include both the clinical objective observations and subjective descriptions during this visit.   textAreaAdjust(false,15639);Was there a sudden/acute change in the patient's mental status?  Yes  NoIf yes, did the change coincide with something else?  No Yes, explain:  textAreaAdjust(false,15647);Mental status changes reported by:   Patient  Representative Caregiver Other:    

            
                
                    
                        
                        M1810. Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts
                        and blouses, managing zippers, buttons, and snaps

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.
                                
                                
                                    1.
                                    Able to dress upper body without assistance if clothing is laid out or handed to the patient.
                                
                                
                                    2.
                                    Someone must help the patient put on upper body clothing.
                                
                                
                                    3.
                                    Patient depends entirely upon another person to dress the upper body.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1820. Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes.

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to obtain, put on, and remove clothing and shoes without assistance.
                                
                                
                                    1.
                                    Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.
                                
                                
                                    2.
                                    Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.
                                
                                
                                    3.
                                    Patient depends entirely upon another person to dress lower body.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1830. Bathing

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                    6
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to bathe self in shower or tub independently, including getting in and out of tub/shower.
                                
                                
                                    1.
                                    With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.
                                
                                
                                    2.
                                    
                                        Able to bathe in shower or tub with the intermittent assistance of another person:
                                        
                                            (a) for intermittent supervision or encouragement or reminders, OR
                                        
                                        
                                            (b) to get in and out of the shower or tub, OR
                                        
                                        
                                            (c) for washing difficult to reach areas.
                                        
                                    
                                
                                
                                    3.
                                    Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.
                                
                                
                                    4.
                                    Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.
                                
                                
                                    5.
                                    
                                        
                                            Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the
                                            assistance or supervision of another person.
                                        
                                    
                                
                                
                                    6.
                                    Unable to participate effectively in bathing and is bathed totally by another person.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1840. Toilet Transferring

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to get to and from the toilet and transfer independently with or without a device.
                                
                                
                                    1.
                                    When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.
                                
                                
                                    2.
                                    Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).
                                
                                
                                    3.
                                    Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.
                                
                                
                                    4.
                                    Is totally dependent in toileting.
                                
                            
                        
                    
                
            

            
        

        
        

        
        
            

            
                
                    
                        M1850. Transferring

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to independently transfer.
                                
                                
                                    1.
                                    Able to transfer with minimal human assistance or with use of an assistive device.
                                
                                
                                    2.
                                    Able to bear weight and pivot during the transfer process but unable to transfer self.
                                
                                
                                    3.
                                    Unable to transfer self and is unable to bear weight or pivot when transferred by another person.
                                
                                
                                    4.
                                    Bedfast, unable to transfer but is able to turn and position self in bed.
                                
                                
                                    5.
                                    Bedfast, unable to transfer and is unable to turn and position self.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    

                        M1860. Ambulation/Locomotion

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                    6
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (specifically: needs no human assistance or assistive device).
                                
                                
                                    1.
                                    With the use of a one-handed device (for example, cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.
                                
                                
                                    2.
                                    Requires use of a two-handed device (for example, walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.
                                
                                
                                    3.
                                    Able to walk only with the supervision or assistance of another person at all times.
                                
                                
                                    4.
                                    Chairfast, unable to ambulate but is able to wheel self independently.
                                
                                
                                    5.
                                    Chairfast, unable to ambulate and is unable to wheel self.
                                
                                
                                    6.
                                    Bedfast, unable to ambulate or be up in a chair.
                                
                            
                        
                    
                
            

             Indications for Home Health Aides:   Yes  No  RefusedReferred to HHA:   Yes No Reason for need:     
        

    



    
        
            SECTION GG - FUNCTIONAL ABILITIES AND GOALS
        
    
    

    

        
        

            
                
                    
                        Activities Permitted (goes to POC Box #18B)
                    
                    
                        
                             1. Complete Bedrest
                        
                        
                             2. Bedrest BRP
                        
                        
                             3. Up As Tolerated
                        
                        
                             4. Transfer Bed/Chair
                        
                        
                             5. Exercises Prescribed
                        
                        
                             6. Partial Weight Bearing
                        
                        
                             7. Independent At Home
                        
                        
                             8. Crutches
                        
                        
                             9. Cane
                        
                        
                             A. Wheelchair
                        
                        
                             B. Walker
                        
                        
                             C. No Restrictions
                        
                        
                             D. Other (Specify)
                            
                        
                    
                
            
        

        
        

            
                
                    
                        Allergies (goes to POC Box #17)
                    
                    
                        
                             1. No Known Allergies
                        
                        
                             2. Aspirin
                        
                        
                             3. Penicillin
                        
                        
                             4. Sulfa
                        
                        
                             5. Pollen
                        
                        
                             6. Eggs
                        
                        
                             7. Milk products
                        
                        
                             8. Insect Bites
                        
                        
                             9. Other
                            
                        
                    
                
            

        

        
        

        
        

        
        
            

            
                
                    
                        GG0130. Self-Care 
                        
                        
                        
                            Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up, code the reason.
                        
                        
                    
                    
                        
                            
                                
                                    Coding:
                                
                                
                                    Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                    score according to amount of assistance provided.
                                
                                
                                    Activities may be completed with or without assistive devices.
                                
                                
                                    
                                        06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                    
                                
                                
                                    
                                        05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                    
                                
                                
                                    
                                        04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as patient completes activity.
                                        Assistance may be provided throughout the activity or intermittently.
                                    
                                
                                
                                    
                                        03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides less than half the effort.
                                    
                                
                                
                                    
                                        02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than half the effort.
                                    
                                
                                
                                    
                                        01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more helpers is required for the patient to complete the activity.
                                    
                                
                                
                                    If activity was not attempted, code reason:
                                
                                
                                    
                                        07.    Patient refused
                                    
                                
                                
                                    
                                        09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                    
                                
                                
                                    
                                        10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                    
                                
                                
                                    
                                        88.    Not attempted due to medical conditions or safety concerns
                                    
                                
                            
                            
                            
                                
                                    
                                        
                                            
                                                
                                                    4. Follow-up Performance
                                                
                                            
                                            
                                                
                                                    
                                                        Enter Codes in Boxes
                                                    
                                                
                                            
                                        
                                        
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            A. Eating: The ability to use suitable utensils to bring food and/or liquid to the mouth and swallow food and/or liquid once the meal is placed before the patient.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            B. Oral Hygiene:
                                            The ability to use suitable items to clean teeth. Dentures (if applicable): The ability to insert and remove dentures into and from the mouth, and manage denture soaking and rinsing with use of equipment.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            C. Toileting Hygiene: The ability to maintain perineal hygiene, adjust clothes before and after voiding or having a bowel movement. If managing an ostomy, include wiping the opening but not managing equipment.
                                        
                                    
                                
                            
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        GG0170. Mobility 
                        
                        
                        
                            Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up code the reason.
                        
                        
                    
                    
                        
                            
                                
                                    Coding:
                                
                                
                                    Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                    score according to amount of assistance provided.
                                
                                
                                    Activities may be completed with or without assistive devices.
                                
                                
                                    
                                        06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                    
                                
                                
                                    
                                        05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                    
                                
                                
                                    
                                        04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as
                                        patient completes activity. Assistance may be provided throughout the activity or intermittently.
                                    
                                
                                
                                    
                                        03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides
                                        less than half the effort.
                                    
                                
                                
                                    
                                        02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than
                                        half the effort.
                                    
                                
                                
                                    
                                        01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more
                                        helpers is required for the patient to complete the activity.
                                    
                                
                                
                                    If activity was not attempted, code reason:
                                
                                
                                    
                                        07.    Patient refused
                                    
                                
                                
                                    
                                        09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                    
                                
                                
                                    
                                        10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                    
                                
                                
                                    
                                        88.    Not attempted due to medical conditions or safety concerns
                                    
                                
                            
                            
                            
                                
                                    
                                        
                                            
                                                
                                                    4. Follow-up Performance
                                                
                                            
                                            
                                                
                                                    Enter Codes in Boxes
                                                
                                            
                                        
                                        
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            A. Roll left and right: The ability to roll from lying on back to left and right side, and return to lying on back on the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            B. Sit to lying: The ability to move from sitting on side of bed to lying flat on the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            C. Lying to sitting on side of bed: The ability to move from lying on the back to sitting on the side of the bed with no back support.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            D. Sit to stand: The ability to come to a standing position from sitting in a chair, wheelchair, or on the side of the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            E. Chair/bed-to-chair transfer: The ability to transfer to and from a bed to a chair (or wheelchair).
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            F. Toilet transfer: The ability to get on and off a toilet or commode.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            I. Walk 10 feet: Once standing, the ability to walk at least 10 feet in a room, corridor, or similar space.
                                            If Follow-up performance is coded 07, 09, 10 or 88 → skip to GG0170M, 1 step (curb)
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            J. Walk 50 feet with two turns: Once standing, the ability to walk 50 feet and make two turns.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            L. Walking 10 feet on uneven surfaces: The ability to walk 10 feet on uneven or sloping surfaces (indoor or outdoor), such as turf or gravel.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            M. 1 step (curb): The ability to go up and down a curb and/or up and down one step.
                                            If Follow-up performance is coded 07, 09, 10 or 88, skip to GG0170Q, Does patient use wheelchair and/or scooter?
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            N. 4 steps: The ability to go up and down four steps with or without a rail.
                                        
                                    
                                
                                
                                    
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                            0
                                                            1
                                                            -
                                                        
                                                    
                                                
                                                
                                                    
                                                        Q. Does patient use wheelchair/scooter?
                                                    
                                                    
                                                        
                                                                      0. No → Skip to M1033, Risk for Hospitalization
                                                        
                                                        
                                                                      1. Yes → Continue to GG0170R, Wheel 50 feet with two turns.
                                                        
                                                    
                                                
                                            

                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            R. Wheel 50 feet with two turns: Once seated in wheelchair/scooter, the ability to wheel at least 50 feet and make two turns.
                                        
                                    
                                
                            
                            
                        
                    
                
            

            
        

        
        

            
                
                    
                        Safety Measures (goes to POC Box #15)
                    
                    
                        
                             1. Bleeding precautions
                        
                        
                             2. Oxygen precautions
                        
                        
                             3. Seizure precautions
                        
                        
                             4. Fall precautions
                        
                        
                             5. Aspiration precautions
                        
                        
                             6. Siderails up
                        
                        
                             7. Elevate head of bed
                        
                        
                             8. 24 hr. supervision
                        
                        
                             9. Clear pathways
                        
                        
                             10. Lock w/c with transfer
                        
                        
                             11. Infection Control Measures
                        
                        
                             12. Walker/cane
                        
                        
                             13. Other
                            
                        
                    
                
            

        

    




    
        
            SECTION J - HEALTH CONDITIONS
        
    
    

    

        
        
            

            
                
                    
                        M1033. Risk for Hospitalization 
                        
                        Which of the following signs or symptoms characterize this patient as at risk for hospitalization? (Mark all that apply.)
                    
                    
                        
                            🡓 Check all that apply
                        
                        
                             1. History of falls (2 or more falls - or any fall with an injury - in the past 12 months)
                        
                        
                             2. Unintentional weight loss of a total of 10 pounds or more in the past 12 months
                        
                        
                             3. Multiple hospitalizations (2 or more) in the past 6 months
                        
                        
                             4. Multiple emergency department visits (2 or more) in the past 6 months
                        
                        
                             5. Decline in mental, emotional, or behavioral status in the past 3 months
                        
                        
                             6. Reported or observed history of difficulty complying with any medical instructions (for example, medications, diet, exercise) in the past 3 months
                        
                        
                             7. Currently taking 5 or more medications
                        
                        
                             8. Currently reports exhaustion
                        
                        
                             9. Other risk(s) not listed in 1 – 8
                        
                        
                             10. None of the above
                        
                    
                
            

            
        

        
        

            
                
                    
                        Cognitive Functioning
                    
                    
                        
                            Patient's current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.
                        

                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                
                            
                        
                        
                            
                                
                                    0
                                    Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.
                                
                                
                                    1
                                    Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.
                                
                                
                                    2
                                    
                                        Requires assistance and some direction in specific situations (for example, on all tasks involving shifting
                                        of attention) or consistently requires low stimulus environment due to distractibility.
                                    
                                
                                
                                    3
                                    
                                        Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift
                                        attention and recall directions more than half the time.
                                    
                                
                                
                                    4
                                    Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.
                                
                            
                        
                        
                            
                                Cognitive Functioning Narrative
                            
                            
                        
                    
                
            

        

        
        
            
                
                    
                        PROGNOSIS (goes to POC Box #20)
                    
                    
                        
                             1 - Poor
                        
                        
                             2 - Guarded
                        
                        
                             3 - Fair
                        
                        
                             4 - Good
                        
                        
                             5 - Excellent
                        
                    
                
            
        

        
        

            
                
                    
                        ADVANCE DIRECTIVES
                    
                    
                        
                             Living will
                        
                        
                             Do not resuscitate
                        
                        
                             Organ Donor
                        
                        
                             Education needed
                        
                        
                             Copies on File
                        
                        
                             Funeral arrangements made
                        
                        
                             Others: 
                            
                        
                        
                             Power of Attorney
                        
                    
                
            
        

        
        

        
        

        
        

        
        

        
        

        
        

    




    
        
            
                SECTION M - SKIN CONDITIONS
            
        
    
    

    

        
        
             Definitions:  Unhealed:  The absence of the skin's original integrity.  Non-epithelialized:  The absence of regenerated epidermis across a wound surface.  Pressure Ulcer:  A   pressure ulcer   is localized injury to the skin and/or underlying tissue, usually over a bony prominence, as a result of pressure or pressure in combination with shear and/or friction.  A number of contributing or confounding factors are also associated with pressure ulcers; the significance of these factors is yet to be elucidated.  Newly epithelialized wound bed completely covered with new epithelium no exudate no avascular tissue (eschar and/or slough) no signs or symptoms of infection Fully granulating wound bed filled with granulation tissue to the level of the surrounding skin no dead space no avascular tissue (eschar and/or slough) wound edges open Early/ partial granulation >=25% of wound bed is covered with granulation tissue &lt;25% of the wound bed is covered with avascular tissue (eschar and/or slough) no signs or symptoms of infection wound edges open 3 - Not healing wound with >=25% avascular tissue (eschar and/or slough) OR  signs/symptoms of infection OR  clean but non-granulating wound bed OR  closed/ hyperkeratotic wound edged OR  Persistent failure to improve despite appropriate comprehensive wound management This guidance applies to surgical wounds closed by either primary intention (i.e. approximated incisions) or secondary intention (i.e. open surgical wounds). PRESSURE ULCER STAGES (NATIONAL PRESSURE ULCER ADVISORY PANEL TOOL): Stage I.  A Stage I pressure ulcer presents as intact skin with non-blanchable redness of a localized area, usually over a bony prominence. Darkly pigmented skin may not have visible blanching. Its color may differ from the surrounding area.  Further description. This area may be painful, firm, soft, and warmer or cooler as compared to adjacent tissue. Stage I ulcers may be difficult to detect in individuals with dark skin tones and may indicate &quot;at risk&quot; persons (a heralding sign of risk). Stage II.  A Stage II pressure ulcer is characterized by partial-thickness loss of dermis presenting as a shallow open ulcer with a red-pink wound bed without slough. It may also present as an intact/ ruptured serum-filled blister. Further description. A Stage II ulcer also may present as a shiny or dry shallow ulcer without slough or bruising.* This stage should not be used to describe skin tears, tape burns, perineal dermatitis, maceration, or excoriation. *Bruising indicates suspected deep tissue injury. Stage III.  A Stage III pressure ulcer is characterized by full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon or muscle are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.  Further description: The depth of a stage III pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and stage III ulcers can be shallow. In contrast, areas of significant adiposity can develop extremely deep stage III pressure ulcers. Bone/tendon is not visible or directly palpable. Stage IV.  A Stage IV pressure ulcer is characterized by Full thickness tissue loss with exposed bone, tendon or muscle. Slough or eschar may be present on some parts of the wound bed. Often include undermining and tunneling. Further description:The depth of a stage IV pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and these ulcers can be shallow. Stage IV ulcers can extend into muscle and/or supporting structures (e.g., fascia, tendon or joint capsule) making osteomyelitis possible. Exposed bone/tendon is visible or directly palpable. Unstageable.  Full-thickness tissue loss in which the base of the ulcer is covered by slough (yellow, tan, gray, green or brown) and/or eschar (tan, brown or black) in the wound bed may render a wound unstageable. Further description. Until enough slough and/or eschar is removed to expose the base of the wound, the true depth (and therefore, the stage) cannot be determined. Stable (dry, adherent, intact without erythema or fluctuance) eschar on the heels serves as &quot;the body's natural (biological) cover&quot; and should not be removed. Suspected Deep Tissue Injury:  Deep tissue injury may be characterized by a purple or maroon localized area of discolored intact skin or a blood-filled  blister due to damage of underlying soft tissue from pressure and/or shear. Presentation may be preceded by tissue that is painful, firm, mushy, boggy, and warmer or cooler as compared to adjacent tissue. Further description. Deep tissue injury may be difficult to detect in individuals with dark skin tones. Evolution may include a thin blister over a dark wound bed. The wound may further evolve and become covered by thin eschar. Evolution may be rapid, exposing additional layers of tissue even with optimal treatment.  

            
                
                    
                        M1306. Does this patient have at least one Unhealed Pressure Ulcer/Injury at Stage 2 or Higher or designated as Unstageable?
                        (Excludes Stage 1 pressure injuries and all healed Stage 2 pressure ulcers/injuries)
                        
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                
                            
                        
                        
                            
                                
                                    0.
                                    
                                        No
                                        
                                        
                                    
                                
                                
                                    1.
                                    Yes
                                
                            
                        
                    
                
            

            
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

    




    
        
            ACTIVE DIAGNOSES
        
    
    

    

        
            HOMEBOUND REASON Needs assistance for all activities Residual weakness Requires max assistance/taxing effort to leave home Confusion, unable to go out of home alone Unable to safely leave home unassisted Severe SOB, SOB upon exertion         Dependent upon adaptive device(s) Medical restrictions Other (specify)  

            

                
                    
                        Primary Diagnosis &amp; Other Diagnoses
                    
                    
                        
                            
                                
                                    Column 1
                                    Column 2
                                
                                
                                    
                                        Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.)
                                    
                                    
                                        ICD-10-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.
                                    
                                
                                
                                    Description
                                    ICD-10-C M / Symptom Control Rating
                                
                                
                                    
                                        Primary Diagnosis
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        V, W, X, Y codes NOT allowed
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        Other Diagnoses
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        

                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        All ICD-10–CM codes allowed
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                
                            
                        

                        
                        
                            

                                
                                
                                
                                    
                                        
                                            
                                                
                                                    g. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    h. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    i. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    j. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    k. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    l. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    m. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    n. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    o. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    p. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    q. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    r. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    s. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    t. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    u. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    v. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    w. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    x. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    y. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                

                            

                            
                        
                        

                    
                

            

            Patient Current Status  Current Condition   
        

    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

</value>
      <webElementGuid>94e75135-5d9e-45b4-8738-c2533c37274a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]</value>
      <webElementGuid>5797d68f-a780-40c4-b874-e35666b3923a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]</value>
      <webElementGuid>19978c35-68bf-46c3-9bea-434c6db89209</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Products/Agencies'])[1]/following::div[9]</value>
      <webElementGuid>70a147ab-60e5-42a2-a780-a49e59b1dc3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DDE / CWF Connectivity'])[1]/following::div[15]</value>
      <webElementGuid>823cd5e4-34b2-4146-bd89-d28e83d99418</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div/div[2]</value>
      <webElementGuid>8221c714-db85-4e49-b091-97c67a621ead</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
                PPS Calculator
            
            
                PDGM Calculator
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
                
                OBQI Measure
            
            
                
                FRA
            
            
                
                HRA
            
            
                
                Upload File
            
            
            
                
                Fax Oasis
            
            
            
            

        
    

    
        Recertification (follow-up) assessment (REC: 01/30/2023 - 03/30/2023) (Oasis E)
    
    
        450 - HEN, NESY 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            

                
                    
                        
                            
                            
                                
                                    A - ADMINISTRATIVE INFORMATION
                                
                                    G - FUNCTIONAL STATUS
                                
                                    GG - FUNCTIONAL ABILITIES AND GOALS
                                
                                    J - HEALTH CONDITIONS
                                
                                    M - SKIN CONDITIONS
                                
                                    ACTIVE DIAGNOSES
                                
                            
                        
                    
                

            
            
                
                    

                        
                        
                            
    
        
            SECTION A - ADMINISTRATIVE INFORMATION
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                                
                                
                                
                                    
                                    Non Visit/Non Billable Episode
                                
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Suffix:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

        
            

                
                    
                    
                        LEGAL REPRESENTATIVE
                        

                            
                                
                                    
                                        Name:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Contact No.:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Email Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Relationship:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                
                                
                                    
                                         DPOA           
                                         Conservator
                                    
                                
                            

                        
                    
                

                
                    
                    
                        PATIENT CONTACT PERSON
                        

                            
                                
                                    
                                        Name:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Home Phone:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Mobile Phone:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Email Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Relationship:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                        
                    
                

            
        

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        

        
        

        
        
            
            
                
                    M0150. Current Payment Sources for Home Care
                    
                    ↓ Check all that apply
                
                
                    
                        
                            
                                
                                0. None; no charge for current services
                            
                        
                        
                            
                                
                                4. Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8. Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1. Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5. Workers&quot; , &quot;'&quot; , &quot; compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2. Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6. Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10. Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7. Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11. Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK. Unknown
                            
                        
                    
                
            
        

        
        

        
        
            COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

            
                
                    
                        M0080 Discipline of Person Completing Assessment. 
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        1
                                        2
                                        3
                                        4
                                    
                                
                            
                            
                                
                                    
                                        1.
                                        RN
                                    
                                    
                                        2.
                                        PT
                                    
                                    
                                        3.
                                        SLP/ST
                                    
                                    
                                        4.
                                        OT
                                    
                                
                            
                        
                        
                            
                                Care Staff Name:
                                
                                    
                                    
                                    
                                    
                                        
                                    
                                
                            
                        
                    
                
            

             
        

        
        
             

            
                
                    
                        M0090. Date Assessment Completed 
                    
                    
                        
                            
                                
                                    
                                        
                                        
                                    
                                
                            
                        
                    
                
            

            
            EMERGENCY PREPAREDNESSPriority Code:   Patient has an Advance Directives Order:   Yes No Time In:    Time Out:     Emergency Contact Information:  Name:   Relationship:    Phone:   Email:   Address:   
        

        
        
            
            

            
                
                    
                        M0100. This Assessment is Currently Being Completed for the Following Reason 
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        
                                        4
                                        5
                                        
                                        
                                        
                                        
                                    
                                
                            
                            
                            
                                
                                    Follow-Up
                                
                                
                                    
                                        
                                            4.
                                            Recertification (follow-up) reassessment
                                        
                                        
                                            5.
                                            Other follow-up
                                        
                                    
                                
                            
                            
                            
                        
                    
                
            

            
        

        
        
        

        
        

        
        

        
        
            

            
                
                    
                        M0110. Episode Timing 
                        
                        Is the Medicare home health payment episode for which this assessment will define a case mix group an “early” episode or a “later” episode in the patient’s current sequence of adjacent Medicare home health payment episodes?
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    
                                
                            
                            
                                
                                    
                                        1.
                                        Early
                                    
                                    
                                        2.
                                        Later
                                    
                                    
                                        UK.
                                        Unknown
                                    
                                    
                                        NA.
                                        Not Applicable: No Medicare case mix group to be defined by this assessment.
                                    
                                
                            
                        
                    
                
            

            
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

    



    
        
            SECTION G - FUNCTIONAL STATUS
        
    
    

    

        
        

            
                
                    
                        Mental Status (goes to POX Box #19)
                    
                    
                        
                             1 - Oriented
                        
                        
                             2 - Comatose
                        
                        
                             3 - Forgetful
                        
                        
                             4 - Depressed
                        
                        
                             5 - Disoriented
                        
                        
                             6 - Lethargic
                        
                        
                             7 - Agitated
                        
                        
                             8 - Other
                            
                        
                    
                
            
        

        
        

            
                
                    
                        Functional Limitation (goes to POC Box #18A)
                    
                    
                        
                             1 - Amputation
                        
                        
                             2 - Bowel/Bladder
                        
                        
                             3 - Contracture
                        
                        
                             4 - Hearing
                        
                        
                             5 - Paralysis
                        
                        
                             6 - Endurance
                        
                        
                             7 - Ambulation
                        
                        
                             8 - Speech
                        
                        
                             9 - Legally Blind
                        
                        
                             A - Dyspnea With Minimal Exertion
                        
                        
                             B - Other
                            
                        
                    
                
            

        

        
        
            

            
                
                    
                        M1800 Grooming

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to tend safely to personal hygiene needs (specifically: washing face and hands, hair care, shaving or make up,
                        teeth or denture care, fingernail care).
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to groom self unaided, with or without the use of assistive devices or adapted methods.
                                
                                
                                    1.
                                    Grooming utensils must be placed within reach before able to complete grooming activities.
                                
                                
                                    2.
                                    Someone must assist the patient to groom self.
                                
                                
                                    3.
                                    Patient depends entirely upon someone else for grooming needs.
                                
                            
                        
                    
                
            

            
        

        
        
            MAHC 10 - Fall Risk Assessment ToolRequired Core ElementsAssess one point for each core element &quot;yes&quot;.Information may be gathered from medical record, assessment and if applicable, the patient/caregiver. Beyond protocols listed below, scoring should be based on your clinical judgment.  Points Age 65+   Diagnosis (3 or more co-existing)   Includes only documented medical diagnosis Prior history of falls within 3 months   An unintentional change in position resulting in coming to rest on the ground or at a lower level Incontinence   Inability to make it to the bathroom or commode in timely manner Includes frequency, urgency, and/or nocturia. Visual impairment   Includes but not limited to, macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription. Impaired functional mobility   May include patients who need help with IADLS or ADLS or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices. Environmental hazards   May include but not limited to, poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits. Poly Pharmacy (4 or more prescriptions – any type)   All PRESCRIPTIONS including prescriptions for OTC meds. Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs. Pain affecting level of function   Pain often affects an individual’s desire or ability to move or pain can be a factor in depression or ompliance with safety recommendations. Cognitive impairment   Could include patients with dementia, Alzheimer’s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patients ability to adhere to the plan of care. A score of 4 or more is considered at risk for falling Total   Missouri Alliance for HOME CARE2420 Hyde Park, Suite A, Jefferson City, MO 65109-4731  /  (573) 634-7772  /  (573) 634-4374 Fax MUSCULOSKELETAL No ProblemDisorders of musculoskeletal system:   textAreaAdjust(false,12747); Fracture (location)    Swollen, painful joints (specify)    Contractures: Joint    Location   Hand Grips:   Equal Unequal   Strong Weak  specify   Dominant side:   R L Motor Changes:  Fine Gross  specify    Weakness:  UE LE  details    Atrophy        Paresthesia      Shuffling Wild-based gait Amputation: BK  R  L  AK  R  L  UE R L Specify:    textAreaAdjust(false,9292); Other (specify)  textAreaAdjust(false,9309);How does the patient&quot; , &quot;'&quot; , &quot;s condition affect their function ability and safety?    MENTAL STATUSDescribe the mental status of the patient:  Include the patient&quot; , &quot;'&quot; , &quot;s general appearance, behaviors, emotional responses, mental functioning and their overall social interaction. Include both the clinical objective observations and subjective descriptions during this visit.   textAreaAdjust(false,15639);Was there a sudden/acute change in the patient&quot; , &quot;'&quot; , &quot;s mental status?  Yes  NoIf yes, did the change coincide with something else?  No Yes, explain:  textAreaAdjust(false,15647);Mental status changes reported by:   Patient  Representative Caregiver Other:    

            
                
                    
                        
                        M1810. Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts
                        and blouses, managing zippers, buttons, and snaps

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.
                                
                                
                                    1.
                                    Able to dress upper body without assistance if clothing is laid out or handed to the patient.
                                
                                
                                    2.
                                    Someone must help the patient put on upper body clothing.
                                
                                
                                    3.
                                    Patient depends entirely upon another person to dress the upper body.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1820. Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes.

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to obtain, put on, and remove clothing and shoes without assistance.
                                
                                
                                    1.
                                    Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.
                                
                                
                                    2.
                                    Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.
                                
                                
                                    3.
                                    Patient depends entirely upon another person to dress lower body.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1830. Bathing

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                    6
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to bathe self in shower or tub independently, including getting in and out of tub/shower.
                                
                                
                                    1.
                                    With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.
                                
                                
                                    2.
                                    
                                        Able to bathe in shower or tub with the intermittent assistance of another person:
                                        
                                            (a) for intermittent supervision or encouragement or reminders, OR
                                        
                                        
                                            (b) to get in and out of the shower or tub, OR
                                        
                                        
                                            (c) for washing difficult to reach areas.
                                        
                                    
                                
                                
                                    3.
                                    Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.
                                
                                
                                    4.
                                    Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.
                                
                                
                                    5.
                                    
                                        
                                            Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the
                                            assistance or supervision of another person.
                                        
                                    
                                
                                
                                    6.
                                    Unable to participate effectively in bathing and is bathed totally by another person.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1840. Toilet Transferring

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to get to and from the toilet and transfer independently with or without a device.
                                
                                
                                    1.
                                    When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.
                                
                                
                                    2.
                                    Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).
                                
                                
                                    3.
                                    Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.
                                
                                
                                    4.
                                    Is totally dependent in toileting.
                                
                            
                        
                    
                
            

            
        

        
        

        
        
            

            
                
                    
                        M1850. Transferring

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to independently transfer.
                                
                                
                                    1.
                                    Able to transfer with minimal human assistance or with use of an assistive device.
                                
                                
                                    2.
                                    Able to bear weight and pivot during the transfer process but unable to transfer self.
                                
                                
                                    3.
                                    Unable to transfer self and is unable to bear weight or pivot when transferred by another person.
                                
                                
                                    4.
                                    Bedfast, unable to transfer but is able to turn and position self in bed.
                                
                                
                                    5.
                                    Bedfast, unable to transfer and is unable to turn and position self.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    

                        M1860. Ambulation/Locomotion

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                    6
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (specifically: needs no human assistance or assistive device).
                                
                                
                                    1.
                                    With the use of a one-handed device (for example, cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.
                                
                                
                                    2.
                                    Requires use of a two-handed device (for example, walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.
                                
                                
                                    3.
                                    Able to walk only with the supervision or assistance of another person at all times.
                                
                                
                                    4.
                                    Chairfast, unable to ambulate but is able to wheel self independently.
                                
                                
                                    5.
                                    Chairfast, unable to ambulate and is unable to wheel self.
                                
                                
                                    6.
                                    Bedfast, unable to ambulate or be up in a chair.
                                
                            
                        
                    
                
            

             Indications for Home Health Aides:   Yes  No  RefusedReferred to HHA:   Yes No Reason for need:     
        

    



    
        
            SECTION GG - FUNCTIONAL ABILITIES AND GOALS
        
    
    

    

        
        

            
                
                    
                        Activities Permitted (goes to POC Box #18B)
                    
                    
                        
                             1. Complete Bedrest
                        
                        
                             2. Bedrest BRP
                        
                        
                             3. Up As Tolerated
                        
                        
                             4. Transfer Bed/Chair
                        
                        
                             5. Exercises Prescribed
                        
                        
                             6. Partial Weight Bearing
                        
                        
                             7. Independent At Home
                        
                        
                             8. Crutches
                        
                        
                             9. Cane
                        
                        
                             A. Wheelchair
                        
                        
                             B. Walker
                        
                        
                             C. No Restrictions
                        
                        
                             D. Other (Specify)
                            
                        
                    
                
            
        

        
        

            
                
                    
                        Allergies (goes to POC Box #17)
                    
                    
                        
                             1. No Known Allergies
                        
                        
                             2. Aspirin
                        
                        
                             3. Penicillin
                        
                        
                             4. Sulfa
                        
                        
                             5. Pollen
                        
                        
                             6. Eggs
                        
                        
                             7. Milk products
                        
                        
                             8. Insect Bites
                        
                        
                             9. Other
                            
                        
                    
                
            

        

        
        

        
        

        
        
            

            
                
                    
                        GG0130. Self-Care 
                        
                        
                        
                            Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up, code the reason.
                        
                        
                    
                    
                        
                            
                                
                                    Coding:
                                
                                
                                    Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                    score according to amount of assistance provided.
                                
                                
                                    Activities may be completed with or without assistive devices.
                                
                                
                                    
                                        06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                    
                                
                                
                                    
                                        05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                    
                                
                                
                                    
                                        04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as patient completes activity.
                                        Assistance may be provided throughout the activity or intermittently.
                                    
                                
                                
                                    
                                        03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides less than half the effort.
                                    
                                
                                
                                    
                                        02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than half the effort.
                                    
                                
                                
                                    
                                        01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more helpers is required for the patient to complete the activity.
                                    
                                
                                
                                    If activity was not attempted, code reason:
                                
                                
                                    
                                        07.    Patient refused
                                    
                                
                                
                                    
                                        09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                    
                                
                                
                                    
                                        10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                    
                                
                                
                                    
                                        88.    Not attempted due to medical conditions or safety concerns
                                    
                                
                            
                            
                            
                                
                                    
                                        
                                            
                                                
                                                    4. Follow-up Performance
                                                
                                            
                                            
                                                
                                                    
                                                        Enter Codes in Boxes
                                                    
                                                
                                            
                                        
                                        
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            A. Eating: The ability to use suitable utensils to bring food and/or liquid to the mouth and swallow food and/or liquid once the meal is placed before the patient.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            B. Oral Hygiene:
                                            The ability to use suitable items to clean teeth. Dentures (if applicable): The ability to insert and remove dentures into and from the mouth, and manage denture soaking and rinsing with use of equipment.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            C. Toileting Hygiene: The ability to maintain perineal hygiene, adjust clothes before and after voiding or having a bowel movement. If managing an ostomy, include wiping the opening but not managing equipment.
                                        
                                    
                                
                            
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        GG0170. Mobility 
                        
                        
                        
                            Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up code the reason.
                        
                        
                    
                    
                        
                            
                                
                                    Coding:
                                
                                
                                    Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                    score according to amount of assistance provided.
                                
                                
                                    Activities may be completed with or without assistive devices.
                                
                                
                                    
                                        06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                    
                                
                                
                                    
                                        05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                    
                                
                                
                                    
                                        04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as
                                        patient completes activity. Assistance may be provided throughout the activity or intermittently.
                                    
                                
                                
                                    
                                        03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides
                                        less than half the effort.
                                    
                                
                                
                                    
                                        02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than
                                        half the effort.
                                    
                                
                                
                                    
                                        01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more
                                        helpers is required for the patient to complete the activity.
                                    
                                
                                
                                    If activity was not attempted, code reason:
                                
                                
                                    
                                        07.    Patient refused
                                    
                                
                                
                                    
                                        09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                    
                                
                                
                                    
                                        10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                    
                                
                                
                                    
                                        88.    Not attempted due to medical conditions or safety concerns
                                    
                                
                            
                            
                            
                                
                                    
                                        
                                            
                                                
                                                    4. Follow-up Performance
                                                
                                            
                                            
                                                
                                                    Enter Codes in Boxes
                                                
                                            
                                        
                                        
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            A. Roll left and right: The ability to roll from lying on back to left and right side, and return to lying on back on the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            B. Sit to lying: The ability to move from sitting on side of bed to lying flat on the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            C. Lying to sitting on side of bed: The ability to move from lying on the back to sitting on the side of the bed with no back support.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            D. Sit to stand: The ability to come to a standing position from sitting in a chair, wheelchair, or on the side of the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            E. Chair/bed-to-chair transfer: The ability to transfer to and from a bed to a chair (or wheelchair).
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            F. Toilet transfer: The ability to get on and off a toilet or commode.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            I. Walk 10 feet: Once standing, the ability to walk at least 10 feet in a room, corridor, or similar space.
                                            If Follow-up performance is coded 07, 09, 10 or 88 → skip to GG0170M, 1 step (curb)
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            J. Walk 50 feet with two turns: Once standing, the ability to walk 50 feet and make two turns.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            L. Walking 10 feet on uneven surfaces: The ability to walk 10 feet on uneven or sloping surfaces (indoor or outdoor), such as turf or gravel.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            M. 1 step (curb): The ability to go up and down a curb and/or up and down one step.
                                            If Follow-up performance is coded 07, 09, 10 or 88, skip to GG0170Q, Does patient use wheelchair and/or scooter?
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            N. 4 steps: The ability to go up and down four steps with or without a rail.
                                        
                                    
                                
                                
                                    
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                            0
                                                            1
                                                            -
                                                        
                                                    
                                                
                                                
                                                    
                                                        Q. Does patient use wheelchair/scooter?
                                                    
                                                    
                                                        
                                                                      0. No → Skip to M1033, Risk for Hospitalization
                                                        
                                                        
                                                                      1. Yes → Continue to GG0170R, Wheel 50 feet with two turns.
                                                        
                                                    
                                                
                                            

                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            R. Wheel 50 feet with two turns: Once seated in wheelchair/scooter, the ability to wheel at least 50 feet and make two turns.
                                        
                                    
                                
                            
                            
                        
                    
                
            

            
        

        
        

            
                
                    
                        Safety Measures (goes to POC Box #15)
                    
                    
                        
                             1. Bleeding precautions
                        
                        
                             2. Oxygen precautions
                        
                        
                             3. Seizure precautions
                        
                        
                             4. Fall precautions
                        
                        
                             5. Aspiration precautions
                        
                        
                             6. Siderails up
                        
                        
                             7. Elevate head of bed
                        
                        
                             8. 24 hr. supervision
                        
                        
                             9. Clear pathways
                        
                        
                             10. Lock w/c with transfer
                        
                        
                             11. Infection Control Measures
                        
                        
                             12. Walker/cane
                        
                        
                             13. Other
                            
                        
                    
                
            

        

    




    
        
            SECTION J - HEALTH CONDITIONS
        
    
    

    

        
        
            

            
                
                    
                        M1033. Risk for Hospitalization 
                        
                        Which of the following signs or symptoms characterize this patient as at risk for hospitalization? (Mark all that apply.)
                    
                    
                        
                            🡓 Check all that apply
                        
                        
                             1. History of falls (2 or more falls - or any fall with an injury - in the past 12 months)
                        
                        
                             2. Unintentional weight loss of a total of 10 pounds or more in the past 12 months
                        
                        
                             3. Multiple hospitalizations (2 or more) in the past 6 months
                        
                        
                             4. Multiple emergency department visits (2 or more) in the past 6 months
                        
                        
                             5. Decline in mental, emotional, or behavioral status in the past 3 months
                        
                        
                             6. Reported or observed history of difficulty complying with any medical instructions (for example, medications, diet, exercise) in the past 3 months
                        
                        
                             7. Currently taking 5 or more medications
                        
                        
                             8. Currently reports exhaustion
                        
                        
                             9. Other risk(s) not listed in 1 – 8
                        
                        
                             10. None of the above
                        
                    
                
            

            
        

        
        

            
                
                    
                        Cognitive Functioning
                    
                    
                        
                            Patient&quot; , &quot;'&quot; , &quot;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.
                        

                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                
                            
                        
                        
                            
                                
                                    0
                                    Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.
                                
                                
                                    1
                                    Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.
                                
                                
                                    2
                                    
                                        Requires assistance and some direction in specific situations (for example, on all tasks involving shifting
                                        of attention) or consistently requires low stimulus environment due to distractibility.
                                    
                                
                                
                                    3
                                    
                                        Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift
                                        attention and recall directions more than half the time.
                                    
                                
                                
                                    4
                                    Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.
                                
                            
                        
                        
                            
                                Cognitive Functioning Narrative
                            
                            
                        
                    
                
            

        

        
        
            
                
                    
                        PROGNOSIS (goes to POC Box #20)
                    
                    
                        
                             1 - Poor
                        
                        
                             2 - Guarded
                        
                        
                             3 - Fair
                        
                        
                             4 - Good
                        
                        
                             5 - Excellent
                        
                    
                
            
        

        
        

            
                
                    
                        ADVANCE DIRECTIVES
                    
                    
                        
                             Living will
                        
                        
                             Do not resuscitate
                        
                        
                             Organ Donor
                        
                        
                             Education needed
                        
                        
                             Copies on File
                        
                        
                             Funeral arrangements made
                        
                        
                             Others: 
                            
                        
                        
                             Power of Attorney
                        
                    
                
            
        

        
        

        
        

        
        

        
        

        
        

        
        

    




    
        
            
                SECTION M - SKIN CONDITIONS
            
        
    
    

    

        
        
             Definitions:  Unhealed:  The absence of the skin&quot; , &quot;'&quot; , &quot;s original integrity.  Non-epithelialized:  The absence of regenerated epidermis across a wound surface.  Pressure Ulcer:  A   pressure ulcer   is localized injury to the skin and/or underlying tissue, usually over a bony prominence, as a result of pressure or pressure in combination with shear and/or friction.  A number of contributing or confounding factors are also associated with pressure ulcers; the significance of these factors is yet to be elucidated.  Newly epithelialized wound bed completely covered with new epithelium no exudate no avascular tissue (eschar and/or slough) no signs or symptoms of infection Fully granulating wound bed filled with granulation tissue to the level of the surrounding skin no dead space no avascular tissue (eschar and/or slough) wound edges open Early/ partial granulation >=25% of wound bed is covered with granulation tissue &lt;25% of the wound bed is covered with avascular tissue (eschar and/or slough) no signs or symptoms of infection wound edges open 3 - Not healing wound with >=25% avascular tissue (eschar and/or slough) OR  signs/symptoms of infection OR  clean but non-granulating wound bed OR  closed/ hyperkeratotic wound edged OR  Persistent failure to improve despite appropriate comprehensive wound management This guidance applies to surgical wounds closed by either primary intention (i.e. approximated incisions) or secondary intention (i.e. open surgical wounds). PRESSURE ULCER STAGES (NATIONAL PRESSURE ULCER ADVISORY PANEL TOOL): Stage I.  A Stage I pressure ulcer presents as intact skin with non-blanchable redness of a localized area, usually over a bony prominence. Darkly pigmented skin may not have visible blanching. Its color may differ from the surrounding area.  Further description. This area may be painful, firm, soft, and warmer or cooler as compared to adjacent tissue. Stage I ulcers may be difficult to detect in individuals with dark skin tones and may indicate &quot;at risk&quot; persons (a heralding sign of risk). Stage II.  A Stage II pressure ulcer is characterized by partial-thickness loss of dermis presenting as a shallow open ulcer with a red-pink wound bed without slough. It may also present as an intact/ ruptured serum-filled blister. Further description. A Stage II ulcer also may present as a shiny or dry shallow ulcer without slough or bruising.* This stage should not be used to describe skin tears, tape burns, perineal dermatitis, maceration, or excoriation. *Bruising indicates suspected deep tissue injury. Stage III.  A Stage III pressure ulcer is characterized by full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon or muscle are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.  Further description: The depth of a stage III pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and stage III ulcers can be shallow. In contrast, areas of significant adiposity can develop extremely deep stage III pressure ulcers. Bone/tendon is not visible or directly palpable. Stage IV.  A Stage IV pressure ulcer is characterized by Full thickness tissue loss with exposed bone, tendon or muscle. Slough or eschar may be present on some parts of the wound bed. Often include undermining and tunneling. Further description:The depth of a stage IV pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and these ulcers can be shallow. Stage IV ulcers can extend into muscle and/or supporting structures (e.g., fascia, tendon or joint capsule) making osteomyelitis possible. Exposed bone/tendon is visible or directly palpable. Unstageable.  Full-thickness tissue loss in which the base of the ulcer is covered by slough (yellow, tan, gray, green or brown) and/or eschar (tan, brown or black) in the wound bed may render a wound unstageable. Further description. Until enough slough and/or eschar is removed to expose the base of the wound, the true depth (and therefore, the stage) cannot be determined. Stable (dry, adherent, intact without erythema or fluctuance) eschar on the heels serves as &quot;the body&quot; , &quot;'&quot; , &quot;s natural (biological) cover&quot; and should not be removed. Suspected Deep Tissue Injury:  Deep tissue injury may be characterized by a purple or maroon localized area of discolored intact skin or a blood-filled  blister due to damage of underlying soft tissue from pressure and/or shear. Presentation may be preceded by tissue that is painful, firm, mushy, boggy, and warmer or cooler as compared to adjacent tissue. Further description. Deep tissue injury may be difficult to detect in individuals with dark skin tones. Evolution may include a thin blister over a dark wound bed. The wound may further evolve and become covered by thin eschar. Evolution may be rapid, exposing additional layers of tissue even with optimal treatment.  

            
                
                    
                        M1306. Does this patient have at least one Unhealed Pressure Ulcer/Injury at Stage 2 or Higher or designated as Unstageable?
                        (Excludes Stage 1 pressure injuries and all healed Stage 2 pressure ulcers/injuries)
                        
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                
                            
                        
                        
                            
                                
                                    0.
                                    
                                        No
                                        
                                        
                                    
                                
                                
                                    1.
                                    Yes
                                
                            
                        
                    
                
            

            
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

    




    
        
            ACTIVE DIAGNOSES
        
    
    

    

        
            HOMEBOUND REASON Needs assistance for all activities Residual weakness Requires max assistance/taxing effort to leave home Confusion, unable to go out of home alone Unable to safely leave home unassisted Severe SOB, SOB upon exertion         Dependent upon adaptive device(s) Medical restrictions Other (specify)  

            

                
                    
                        Primary Diagnosis &amp; Other Diagnoses
                    
                    
                        
                            
                                
                                    Column 1
                                    Column 2
                                
                                
                                    
                                        Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.)
                                    
                                    
                                        ICD-10-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.
                                    
                                
                                
                                    Description
                                    ICD-10-C M / Symptom Control Rating
                                
                                
                                    
                                        Primary Diagnosis
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        V, W, X, Y codes NOT allowed
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        Other Diagnoses
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        

                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        All ICD-10–CM codes allowed
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                
                            
                        

                        
                        
                            

                                
                                
                                
                                    
                                        
                                            
                                                
                                                    g. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    h. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    i. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    j. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    k. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    l. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    m. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    n. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    o. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    p. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    q. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    r. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    s. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    t. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    u. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    v. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    w. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    x. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    y. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                

                            

                            
                        
                        

                    
                

            

            Patient Current Status  Current Condition   
        

    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

&quot;) or . = concat(&quot;
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
                PPS Calculator
            
            
                PDGM Calculator
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
                
                OBQI Measure
            
            
                
                FRA
            
            
                
                HRA
            
            
                
                Upload File
            
            
            
                
                Fax Oasis
            
            
            
            

        
    

    
        Recertification (follow-up) assessment (REC: 01/30/2023 - 03/30/2023) (Oasis E)
    
    
        450 - HEN, NESY 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            

                
                    
                        
                            
                            
                                
                                    A - ADMINISTRATIVE INFORMATION
                                
                                    G - FUNCTIONAL STATUS
                                
                                    GG - FUNCTIONAL ABILITIES AND GOALS
                                
                                    J - HEALTH CONDITIONS
                                
                                    M - SKIN CONDITIONS
                                
                                    ACTIVE DIAGNOSES
                                
                            
                        
                    
                

            
            
                
                    

                        
                        
                            
    
        
            SECTION A - ADMINISTRATIVE INFORMATION
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                                
                                
                                
                                    
                                    Non Visit/Non Billable Episode
                                
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Suffix:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

        
            

                
                    
                    
                        LEGAL REPRESENTATIVE
                        

                            
                                
                                    
                                        Name:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Contact No.:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Email Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Relationship:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                
                                
                                    
                                         DPOA           
                                         Conservator
                                    
                                
                            

                        
                    
                

                
                    
                    
                        PATIENT CONTACT PERSON
                        

                            
                                
                                    
                                        Name:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Home Phone:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Mobile Phone:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Email Address:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                            
                                
                                    
                                        Relationship:
                                    
                                
                                
                                    
                                        
                                    
                                
                            

                        
                    
                

            
        

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        

        
        

        
        
            
            
                
                    M0150. Current Payment Sources for Home Care
                    
                    ↓ Check all that apply
                
                
                    
                        
                            
                                
                                0. None; no charge for current services
                            
                        
                        
                            
                                
                                4. Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8. Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1. Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5. Workers&quot; , &quot;'&quot; , &quot; compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2. Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6. Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10. Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7. Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11. Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK. Unknown
                            
                        
                    
                
            
        

        
        

        
        
            COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

            
                
                    
                        M0080 Discipline of Person Completing Assessment. 
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        1
                                        2
                                        3
                                        4
                                    
                                
                            
                            
                                
                                    
                                        1.
                                        RN
                                    
                                    
                                        2.
                                        PT
                                    
                                    
                                        3.
                                        SLP/ST
                                    
                                    
                                        4.
                                        OT
                                    
                                
                            
                        
                        
                            
                                Care Staff Name:
                                
                                    
                                    
                                    
                                    
                                        
                                    
                                
                            
                        
                    
                
            

             
        

        
        
             

            
                
                    
                        M0090. Date Assessment Completed 
                    
                    
                        
                            
                                
                                    
                                        
                                        
                                    
                                
                            
                        
                    
                
            

            
            EMERGENCY PREPAREDNESSPriority Code:   Patient has an Advance Directives Order:   Yes No Time In:    Time Out:     Emergency Contact Information:  Name:   Relationship:    Phone:   Email:   Address:   
        

        
        
            
            

            
                
                    
                        M0100. This Assessment is Currently Being Completed for the Following Reason 
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        
                                        4
                                        5
                                        
                                        
                                        
                                        
                                    
                                
                            
                            
                            
                                
                                    Follow-Up
                                
                                
                                    
                                        
                                            4.
                                            Recertification (follow-up) reassessment
                                        
                                        
                                            5.
                                            Other follow-up
                                        
                                    
                                
                            
                            
                            
                        
                    
                
            

            
        

        
        
        

        
        

        
        

        
        
            

            
                
                    
                        M0110. Episode Timing 
                        
                        Is the Medicare home health payment episode for which this assessment will define a case mix group an “early” episode or a “later” episode in the patient’s current sequence of adjacent Medicare home health payment episodes?
                    
                    
                        
                            
                                Enter Code
                                
                                    
                                    
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    
                                
                            
                            
                                
                                    
                                        1.
                                        Early
                                    
                                    
                                        2.
                                        Later
                                    
                                    
                                        UK.
                                        Unknown
                                    
                                    
                                        NA.
                                        Not Applicable: No Medicare case mix group to be defined by this assessment.
                                    
                                
                            
                        
                    
                
            

            
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

    



    
        
            SECTION G - FUNCTIONAL STATUS
        
    
    

    

        
        

            
                
                    
                        Mental Status (goes to POX Box #19)
                    
                    
                        
                             1 - Oriented
                        
                        
                             2 - Comatose
                        
                        
                             3 - Forgetful
                        
                        
                             4 - Depressed
                        
                        
                             5 - Disoriented
                        
                        
                             6 - Lethargic
                        
                        
                             7 - Agitated
                        
                        
                             8 - Other
                            
                        
                    
                
            
        

        
        

            
                
                    
                        Functional Limitation (goes to POC Box #18A)
                    
                    
                        
                             1 - Amputation
                        
                        
                             2 - Bowel/Bladder
                        
                        
                             3 - Contracture
                        
                        
                             4 - Hearing
                        
                        
                             5 - Paralysis
                        
                        
                             6 - Endurance
                        
                        
                             7 - Ambulation
                        
                        
                             8 - Speech
                        
                        
                             9 - Legally Blind
                        
                        
                             A - Dyspnea With Minimal Exertion
                        
                        
                             B - Other
                            
                        
                    
                
            

        

        
        
            

            
                
                    
                        M1800 Grooming

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to tend safely to personal hygiene needs (specifically: washing face and hands, hair care, shaving or make up,
                        teeth or denture care, fingernail care).
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to groom self unaided, with or without the use of assistive devices or adapted methods.
                                
                                
                                    1.
                                    Grooming utensils must be placed within reach before able to complete grooming activities.
                                
                                
                                    2.
                                    Someone must assist the patient to groom self.
                                
                                
                                    3.
                                    Patient depends entirely upon someone else for grooming needs.
                                
                            
                        
                    
                
            

            
        

        
        
            MAHC 10 - Fall Risk Assessment ToolRequired Core ElementsAssess one point for each core element &quot;yes&quot;.Information may be gathered from medical record, assessment and if applicable, the patient/caregiver. Beyond protocols listed below, scoring should be based on your clinical judgment.  Points Age 65+   Diagnosis (3 or more co-existing)   Includes only documented medical diagnosis Prior history of falls within 3 months   An unintentional change in position resulting in coming to rest on the ground or at a lower level Incontinence   Inability to make it to the bathroom or commode in timely manner Includes frequency, urgency, and/or nocturia. Visual impairment   Includes but not limited to, macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription. Impaired functional mobility   May include patients who need help with IADLS or ADLS or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices. Environmental hazards   May include but not limited to, poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits. Poly Pharmacy (4 or more prescriptions – any type)   All PRESCRIPTIONS including prescriptions for OTC meds. Drugs highly associated with fall risk include but not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs. Pain affecting level of function   Pain often affects an individual’s desire or ability to move or pain can be a factor in depression or ompliance with safety recommendations. Cognitive impairment   Could include patients with dementia, Alzheimer’s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patients ability to adhere to the plan of care. A score of 4 or more is considered at risk for falling Total   Missouri Alliance for HOME CARE2420 Hyde Park, Suite A, Jefferson City, MO 65109-4731  /  (573) 634-7772  /  (573) 634-4374 Fax MUSCULOSKELETAL No ProblemDisorders of musculoskeletal system:   textAreaAdjust(false,12747); Fracture (location)    Swollen, painful joints (specify)    Contractures: Joint    Location   Hand Grips:   Equal Unequal   Strong Weak  specify   Dominant side:   R L Motor Changes:  Fine Gross  specify    Weakness:  UE LE  details    Atrophy        Paresthesia      Shuffling Wild-based gait Amputation: BK  R  L  AK  R  L  UE R L Specify:    textAreaAdjust(false,9292); Other (specify)  textAreaAdjust(false,9309);How does the patient&quot; , &quot;'&quot; , &quot;s condition affect their function ability and safety?    MENTAL STATUSDescribe the mental status of the patient:  Include the patient&quot; , &quot;'&quot; , &quot;s general appearance, behaviors, emotional responses, mental functioning and their overall social interaction. Include both the clinical objective observations and subjective descriptions during this visit.   textAreaAdjust(false,15639);Was there a sudden/acute change in the patient&quot; , &quot;'&quot; , &quot;s mental status?  Yes  NoIf yes, did the change coincide with something else?  No Yes, explain:  textAreaAdjust(false,15647);Mental status changes reported by:   Patient  Representative Caregiver Other:    

            
                
                    
                        
                        M1810. Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts
                        and blouses, managing zippers, buttons, and snaps

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.
                                
                                
                                    1.
                                    Able to dress upper body without assistance if clothing is laid out or handed to the patient.
                                
                                
                                    2.
                                    Someone must help the patient put on upper body clothing.
                                
                                
                                    3.
                                    Patient depends entirely upon another person to dress the upper body.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1820. Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes.

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to obtain, put on, and remove clothing and shoes without assistance.
                                
                                
                                    1.
                                    Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.
                                
                                
                                    2.
                                    Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.
                                
                                
                                    3.
                                    Patient depends entirely upon another person to dress lower body.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1830. Bathing

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                    6
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to bathe self in shower or tub independently, including getting in and out of tub/shower.
                                
                                
                                    1.
                                    With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.
                                
                                
                                    2.
                                    
                                        Able to bathe in shower or tub with the intermittent assistance of another person:
                                        
                                            (a) for intermittent supervision or encouragement or reminders, OR
                                        
                                        
                                            (b) to get in and out of the shower or tub, OR
                                        
                                        
                                            (c) for washing difficult to reach areas.
                                        
                                    
                                
                                
                                    3.
                                    Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.
                                
                                
                                    4.
                                    Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.
                                
                                
                                    5.
                                    
                                        
                                            Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the
                                            assistance or supervision of another person.
                                        
                                    
                                
                                
                                    6.
                                    Unable to participate effectively in bathing and is bathed totally by another person.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        
                        M1840. Toilet Transferring

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to get to and from the toilet and transfer independently with or without a device.
                                
                                
                                    1.
                                    When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.
                                
                                
                                    2.
                                    Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).
                                
                                
                                    3.
                                    Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.
                                
                                
                                    4.
                                    Is totally dependent in toileting.
                                
                            
                        
                    
                
            

            
        

        
        

        
        
            

            
                
                    
                        M1850. Transferring

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to independently transfer.
                                
                                
                                    1.
                                    Able to transfer with minimal human assistance or with use of an assistive device.
                                
                                
                                    2.
                                    Able to bear weight and pivot during the transfer process but unable to transfer self.
                                
                                
                                    3.
                                    Unable to transfer self and is unable to bear weight or pivot when transferred by another person.
                                
                                
                                    4.
                                    Bedfast, unable to transfer but is able to turn and position self in bed.
                                
                                
                                    5.
                                    Bedfast, unable to transfer and is unable to turn and position self.
                                
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    

                        M1860. Ambulation/Locomotion

                        
                            HHVBP
                            
                                This item relates to HHVBP (Home Health Value-Based Purchasing). For more information click here.
                            
                            
                        

                        
                        Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                    6
                                
                            
                        
                        
                            
                                
                                    0.
                                    Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (specifically: needs no human assistance or assistive device).
                                
                                
                                    1.
                                    With the use of a one-handed device (for example, cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.
                                
                                
                                    2.
                                    Requires use of a two-handed device (for example, walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.
                                
                                
                                    3.
                                    Able to walk only with the supervision or assistance of another person at all times.
                                
                                
                                    4.
                                    Chairfast, unable to ambulate but is able to wheel self independently.
                                
                                
                                    5.
                                    Chairfast, unable to ambulate and is unable to wheel self.
                                
                                
                                    6.
                                    Bedfast, unable to ambulate or be up in a chair.
                                
                            
                        
                    
                
            

             Indications for Home Health Aides:   Yes  No  RefusedReferred to HHA:   Yes No Reason for need:     
        

    



    
        
            SECTION GG - FUNCTIONAL ABILITIES AND GOALS
        
    
    

    

        
        

            
                
                    
                        Activities Permitted (goes to POC Box #18B)
                    
                    
                        
                             1. Complete Bedrest
                        
                        
                             2. Bedrest BRP
                        
                        
                             3. Up As Tolerated
                        
                        
                             4. Transfer Bed/Chair
                        
                        
                             5. Exercises Prescribed
                        
                        
                             6. Partial Weight Bearing
                        
                        
                             7. Independent At Home
                        
                        
                             8. Crutches
                        
                        
                             9. Cane
                        
                        
                             A. Wheelchair
                        
                        
                             B. Walker
                        
                        
                             C. No Restrictions
                        
                        
                             D. Other (Specify)
                            
                        
                    
                
            
        

        
        

            
                
                    
                        Allergies (goes to POC Box #17)
                    
                    
                        
                             1. No Known Allergies
                        
                        
                             2. Aspirin
                        
                        
                             3. Penicillin
                        
                        
                             4. Sulfa
                        
                        
                             5. Pollen
                        
                        
                             6. Eggs
                        
                        
                             7. Milk products
                        
                        
                             8. Insect Bites
                        
                        
                             9. Other
                            
                        
                    
                
            

        

        
        

        
        

        
        
            

            
                
                    
                        GG0130. Self-Care 
                        
                        
                        
                            Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up, code the reason.
                        
                        
                    
                    
                        
                            
                                
                                    Coding:
                                
                                
                                    Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                    score according to amount of assistance provided.
                                
                                
                                    Activities may be completed with or without assistive devices.
                                
                                
                                    
                                        06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                    
                                
                                
                                    
                                        05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                    
                                
                                
                                    
                                        04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as patient completes activity.
                                        Assistance may be provided throughout the activity or intermittently.
                                    
                                
                                
                                    
                                        03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides less than half the effort.
                                    
                                
                                
                                    
                                        02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than half the effort.
                                    
                                
                                
                                    
                                        01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more helpers is required for the patient to complete the activity.
                                    
                                
                                
                                    If activity was not attempted, code reason:
                                
                                
                                    
                                        07.    Patient refused
                                    
                                
                                
                                    
                                        09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                    
                                
                                
                                    
                                        10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                    
                                
                                
                                    
                                        88.    Not attempted due to medical conditions or safety concerns
                                    
                                
                            
                            
                            
                                
                                    
                                        
                                            
                                                
                                                    4. Follow-up Performance
                                                
                                            
                                            
                                                
                                                    
                                                        Enter Codes in Boxes
                                                    
                                                
                                            
                                        
                                        
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            A. Eating: The ability to use suitable utensils to bring food and/or liquid to the mouth and swallow food and/or liquid once the meal is placed before the patient.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            B. Oral Hygiene:
                                            The ability to use suitable items to clean teeth. Dentures (if applicable): The ability to insert and remove dentures into and from the mouth, and manage denture soaking and rinsing with use of equipment.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            C. Toileting Hygiene: The ability to maintain perineal hygiene, adjust clothes before and after voiding or having a bowel movement. If managing an ostomy, include wiping the opening but not managing equipment.
                                        
                                    
                                
                            
                            
                        
                    
                
            

            
        

        
        
            

            
                
                    
                        GG0170. Mobility 
                        
                        
                        
                            Code the patient’s usual performance at Follow-Up for each activity using the 6-point scale. If activity was not attempted at Follow-Up code the reason.
                        
                        
                    
                    
                        
                            
                                
                                    Coding:
                                
                                
                                    Safety and Quality of Performance – If helper assistance is required because patient’s performance is unsafe or of poor quality,
                                    score according to amount of assistance provided.
                                
                                
                                    Activities may be completed with or without assistive devices.
                                
                                
                                    
                                        06.    Independent – Patient completes the activity by him/herself with no assistance from a helper.
                                    
                                
                                
                                    
                                        05.    Setup or clean-up assistance – Helper sets up or cleans up; patient completes activity. Helper assists only prior to or following the activity.
                                    
                                
                                
                                    
                                        04.    Supervision or touching assistance – Helper provides verbal cues and/or touching/steadying and/or contact guard assistance as
                                        patient completes activity. Assistance may be provided throughout the activity or intermittently.
                                    
                                
                                
                                    
                                        03.    Partial/moderate assistance – Helper does LESS THAN HALF the effort. Helper lifts, holds or supports trunk or limbs, but provides
                                        less than half the effort.
                                    
                                
                                
                                    
                                        02.    Substantial/maximal assistance – Helper does MORE THAN HALF the effort. Helper lifts or holds trunk or limbs and provides more than
                                        half the effort.
                                    
                                
                                
                                    
                                        01.    Dependent – Helper does ALL of the effort. Patient does none of the effort to complete the activity. Or, the assistance of 2 or more
                                        helpers is required for the patient to complete the activity.
                                    
                                
                                
                                    If activity was not attempted, code reason:
                                
                                
                                    
                                        07.    Patient refused
                                    
                                
                                
                                    
                                        09.    Not applicable – Not attempted and the patient did not perform this activity prior to the current illness, exacerbation or injury.
                                    
                                
                                
                                    
                                        10.    Not attempted due to environmental limitations (e.g., lack of equipment, weather constraints)
                                    
                                
                                
                                    
                                        88.    Not attempted due to medical conditions or safety concerns
                                    
                                
                            
                            
                            
                                
                                    
                                        
                                            
                                                
                                                    4. Follow-up Performance
                                                
                                            
                                            
                                                
                                                    Enter Codes in Boxes
                                                
                                            
                                        
                                        
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            A. Roll left and right: The ability to roll from lying on back to left and right side, and return to lying on back on the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            B. Sit to lying: The ability to move from sitting on side of bed to lying flat on the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            C. Lying to sitting on side of bed: The ability to move from lying on the back to sitting on the side of the bed with no back support.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            D. Sit to stand: The ability to come to a standing position from sitting in a chair, wheelchair, or on the side of the bed.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            E. Chair/bed-to-chair transfer: The ability to transfer to and from a bed to a chair (or wheelchair).
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            F. Toilet transfer: The ability to get on and off a toilet or commode.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            I. Walk 10 feet: Once standing, the ability to walk at least 10 feet in a room, corridor, or similar space.
                                            If Follow-up performance is coded 07, 09, 10 or 88 → skip to GG0170M, 1 step (curb)
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            J. Walk 50 feet with two turns: Once standing, the ability to walk 50 feet and make two turns.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            L. Walking 10 feet on uneven surfaces: The ability to walk 10 feet on uneven or sloping surfaces (indoor or outdoor), such as turf or gravel.
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            M. 1 step (curb): The ability to go up and down a curb and/or up and down one step.
                                            If Follow-up performance is coded 07, 09, 10 or 88, skip to GG0170Q, Does patient use wheelchair and/or scooter?
                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            N. 4 steps: The ability to go up and down four steps with or without a rail.
                                        
                                    
                                
                                
                                    
                                        
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                            0
                                                            1
                                                            -
                                                        
                                                    
                                                
                                                
                                                    
                                                        Q. Does patient use wheelchair/scooter?
                                                    
                                                    
                                                        
                                                                      0. No → Skip to M1033, Risk for Hospitalization
                                                        
                                                        
                                                                      1. Yes → Continue to GG0170R, Wheel 50 feet with two turns.
                                                        
                                                    
                                                
                                            

                                        
                                    
                                
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            R. Wheel 50 feet with two turns: Once seated in wheelchair/scooter, the ability to wheel at least 50 feet and make two turns.
                                        
                                    
                                
                            
                            
                        
                    
                
            

            
        

        
        

            
                
                    
                        Safety Measures (goes to POC Box #15)
                    
                    
                        
                             1. Bleeding precautions
                        
                        
                             2. Oxygen precautions
                        
                        
                             3. Seizure precautions
                        
                        
                             4. Fall precautions
                        
                        
                             5. Aspiration precautions
                        
                        
                             6. Siderails up
                        
                        
                             7. Elevate head of bed
                        
                        
                             8. 24 hr. supervision
                        
                        
                             9. Clear pathways
                        
                        
                             10. Lock w/c with transfer
                        
                        
                             11. Infection Control Measures
                        
                        
                             12. Walker/cane
                        
                        
                             13. Other
                            
                        
                    
                
            

        

    




    
        
            SECTION J - HEALTH CONDITIONS
        
    
    

    

        
        
            

            
                
                    
                        M1033. Risk for Hospitalization 
                        
                        Which of the following signs or symptoms characterize this patient as at risk for hospitalization? (Mark all that apply.)
                    
                    
                        
                            🡓 Check all that apply
                        
                        
                             1. History of falls (2 or more falls - or any fall with an injury - in the past 12 months)
                        
                        
                             2. Unintentional weight loss of a total of 10 pounds or more in the past 12 months
                        
                        
                             3. Multiple hospitalizations (2 or more) in the past 6 months
                        
                        
                             4. Multiple emergency department visits (2 or more) in the past 6 months
                        
                        
                             5. Decline in mental, emotional, or behavioral status in the past 3 months
                        
                        
                             6. Reported or observed history of difficulty complying with any medical instructions (for example, medications, diet, exercise) in the past 3 months
                        
                        
                             7. Currently taking 5 or more medications
                        
                        
                             8. Currently reports exhaustion
                        
                        
                             9. Other risk(s) not listed in 1 – 8
                        
                        
                             10. None of the above
                        
                    
                
            

            
        

        
        

            
                
                    
                        Cognitive Functioning
                    
                    
                        
                            Patient&quot; , &quot;'&quot; , &quot;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.
                        

                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                
                            
                        
                        
                            
                                
                                    0
                                    Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.
                                
                                
                                    1
                                    Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.
                                
                                
                                    2
                                    
                                        Requires assistance and some direction in specific situations (for example, on all tasks involving shifting
                                        of attention) or consistently requires low stimulus environment due to distractibility.
                                    
                                
                                
                                    3
                                    
                                        Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift
                                        attention and recall directions more than half the time.
                                    
                                
                                
                                    4
                                    Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.
                                
                            
                        
                        
                            
                                Cognitive Functioning Narrative
                            
                            
                        
                    
                
            

        

        
        
            
                
                    
                        PROGNOSIS (goes to POC Box #20)
                    
                    
                        
                             1 - Poor
                        
                        
                             2 - Guarded
                        
                        
                             3 - Fair
                        
                        
                             4 - Good
                        
                        
                             5 - Excellent
                        
                    
                
            
        

        
        

            
                
                    
                        ADVANCE DIRECTIVES
                    
                    
                        
                             Living will
                        
                        
                             Do not resuscitate
                        
                        
                             Organ Donor
                        
                        
                             Education needed
                        
                        
                             Copies on File
                        
                        
                             Funeral arrangements made
                        
                        
                             Others: 
                            
                        
                        
                             Power of Attorney
                        
                    
                
            
        

        
        

        
        

        
        

        
        

        
        

        
        

    




    
        
            
                SECTION M - SKIN CONDITIONS
            
        
    
    

    

        
        
             Definitions:  Unhealed:  The absence of the skin&quot; , &quot;'&quot; , &quot;s original integrity.  Non-epithelialized:  The absence of regenerated epidermis across a wound surface.  Pressure Ulcer:  A   pressure ulcer   is localized injury to the skin and/or underlying tissue, usually over a bony prominence, as a result of pressure or pressure in combination with shear and/or friction.  A number of contributing or confounding factors are also associated with pressure ulcers; the significance of these factors is yet to be elucidated.  Newly epithelialized wound bed completely covered with new epithelium no exudate no avascular tissue (eschar and/or slough) no signs or symptoms of infection Fully granulating wound bed filled with granulation tissue to the level of the surrounding skin no dead space no avascular tissue (eschar and/or slough) wound edges open Early/ partial granulation >=25% of wound bed is covered with granulation tissue &lt;25% of the wound bed is covered with avascular tissue (eschar and/or slough) no signs or symptoms of infection wound edges open 3 - Not healing wound with >=25% avascular tissue (eschar and/or slough) OR  signs/symptoms of infection OR  clean but non-granulating wound bed OR  closed/ hyperkeratotic wound edged OR  Persistent failure to improve despite appropriate comprehensive wound management This guidance applies to surgical wounds closed by either primary intention (i.e. approximated incisions) or secondary intention (i.e. open surgical wounds). PRESSURE ULCER STAGES (NATIONAL PRESSURE ULCER ADVISORY PANEL TOOL): Stage I.  A Stage I pressure ulcer presents as intact skin with non-blanchable redness of a localized area, usually over a bony prominence. Darkly pigmented skin may not have visible blanching. Its color may differ from the surrounding area.  Further description. This area may be painful, firm, soft, and warmer or cooler as compared to adjacent tissue. Stage I ulcers may be difficult to detect in individuals with dark skin tones and may indicate &quot;at risk&quot; persons (a heralding sign of risk). Stage II.  A Stage II pressure ulcer is characterized by partial-thickness loss of dermis presenting as a shallow open ulcer with a red-pink wound bed without slough. It may also present as an intact/ ruptured serum-filled blister. Further description. A Stage II ulcer also may present as a shiny or dry shallow ulcer without slough or bruising.* This stage should not be used to describe skin tears, tape burns, perineal dermatitis, maceration, or excoriation. *Bruising indicates suspected deep tissue injury. Stage III.  A Stage III pressure ulcer is characterized by full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon or muscle are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.  Further description: The depth of a stage III pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and stage III ulcers can be shallow. In contrast, areas of significant adiposity can develop extremely deep stage III pressure ulcers. Bone/tendon is not visible or directly palpable. Stage IV.  A Stage IV pressure ulcer is characterized by Full thickness tissue loss with exposed bone, tendon or muscle. Slough or eschar may be present on some parts of the wound bed. Often include undermining and tunneling. Further description:The depth of a stage IV pressure ulcer varies by anatomical location. The bridge of the nose, ear, occiput and malleolus do not have subcutaneous tissue and these ulcers can be shallow. Stage IV ulcers can extend into muscle and/or supporting structures (e.g., fascia, tendon or joint capsule) making osteomyelitis possible. Exposed bone/tendon is visible or directly palpable. Unstageable.  Full-thickness tissue loss in which the base of the ulcer is covered by slough (yellow, tan, gray, green or brown) and/or eschar (tan, brown or black) in the wound bed may render a wound unstageable. Further description. Until enough slough and/or eschar is removed to expose the base of the wound, the true depth (and therefore, the stage) cannot be determined. Stable (dry, adherent, intact without erythema or fluctuance) eschar on the heels serves as &quot;the body&quot; , &quot;'&quot; , &quot;s natural (biological) cover&quot; and should not be removed. Suspected Deep Tissue Injury:  Deep tissue injury may be characterized by a purple or maroon localized area of discolored intact skin or a blood-filled  blister due to damage of underlying soft tissue from pressure and/or shear. Presentation may be preceded by tissue that is painful, firm, mushy, boggy, and warmer or cooler as compared to adjacent tissue. Further description. Deep tissue injury may be difficult to detect in individuals with dark skin tones. Evolution may include a thin blister over a dark wound bed. The wound may further evolve and become covered by thin eschar. Evolution may be rapid, exposing additional layers of tissue even with optimal treatment.  

            
                
                    
                        M1306. Does this patient have at least one Unhealed Pressure Ulcer/Injury at Stage 2 or Higher or designated as Unstageable?
                        (Excludes Stage 1 pressure injuries and all healed Stage 2 pressure ulcers/injuries)
                        
                    
                    
                        
                            Enter Code
                            
                                
                                
                                    
                                    0
                                    1
                                
                            
                        
                        
                            
                                
                                    0.
                                    
                                        No
                                        
                                        
                                    
                                
                                
                                    1.
                                    Yes
                                
                            
                        
                    
                
            

            
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

    




    
        
            ACTIVE DIAGNOSES
        
    
    

    

        
            HOMEBOUND REASON Needs assistance for all activities Residual weakness Requires max assistance/taxing effort to leave home Confusion, unable to go out of home alone Unable to safely leave home unassisted Severe SOB, SOB upon exertion         Dependent upon adaptive device(s) Medical restrictions Other (specify)  

            

                
                    
                        Primary Diagnosis &amp; Other Diagnoses
                    
                    
                        
                            
                                
                                    Column 1
                                    Column 2
                                
                                
                                    
                                        Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided.)
                                    
                                    
                                        ICD-10-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.
                                    
                                
                                
                                    Description
                                    ICD-10-C M / Symptom Control Rating
                                
                                
                                    
                                        Primary Diagnosis
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        V, W, X, Y codes NOT allowed
                                        
                                            
                                                
                                                    a.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        Other Diagnoses
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        

                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        All ICD-10–CM codes allowed
                                        
                                            
                                                
                                                    b.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    c.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    d.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    e.
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                    
                                                    
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                

                                
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             Onset
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                                    f. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
                                            
                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                        
                                             0
                                             1
                                             2
                                             3
                                             4
                                        
                                    
                                
                            
                        

                        
                        
                            

                                
                                
                                
                                    
                                        
                                            
                                                
                                                    g. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    h. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    i. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    j. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    k. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    l. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    m. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    n. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    o. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    p. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    q. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    r. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    s. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    t. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    u. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    v. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    w. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    x. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                
                                
                                
                                    
                                        
                                            
                                                
                                                    y. 
                                                
                                                
    
        
            
        
    
    
        
        
            
        
    



                                            
                                        
                                        
                                            
    
        
            
        
    
    
        
        
            
        
    



                                        
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                                

                            

                            
                        
                        

                    
                

            

            Patient Current Status  Current Condition   
        

    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

&quot;))]</value>
      <webElementGuid>b146ef97-4b5e-4fef-9cbe-686a0263386d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
