<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_A01.00</name>
   <tag></tag>
   <elementGuidId>176ff2ce-7dcd-4334-9163-e51a66eb05e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.text-primary.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//autocomplete-directive[@id='M0230_PRIMARY_DIAG_ICD']/div/div[2]/ul/li/a/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>ad2bf73e-cd27-4de8-90e1-420789cb9e8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-primary ng-binding</value>
      <webElementGuid>36986e42-3f74-4fdd-aee0-32229a8e37ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>A01.00</value>
      <webElementGuid>281813e4-f0e7-4005-8eb3-9ba7631a1c02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1025&quot;)/table[@class=&quot;table table-bordered tblM1021&quot;]/tbody[1]/tr[4]/td[2]/div[@class=&quot;col-xs-9 col-sm-9 col-md-9 col-lg-9 row&quot;]/div[@class=&quot;input-group space&quot;]/autocomplete-directive[@id=&quot;M0230_PRIMARY_DIAG_ICD&quot;]/div[@class=&quot;dropdown&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/p[@class=&quot;text-primary ng-binding&quot;]</value>
      <webElementGuid>6d2fc4d5-aff8-4c9d-a7e7-cfdedf13e59a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//autocomplete-directive[@id='M0230_PRIMARY_DIAG_ICD']/div/div[2]/ul/li/a/div/p</value>
      <webElementGuid>a01244bb-f8e1-418c-852c-97e8a8402f72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='a.'])[2]/following::p[1]</value>
      <webElementGuid>14fd225c-a764-4c1f-ab39-0b94efa27dbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='V, W, X, Y codes NOT allowed'])[1]/following::p[1]</value>
      <webElementGuid>b5ecf566-f478-434b-953a-1546be631dd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other Diagnoses'])[1]/preceding::p[20]</value>
      <webElementGuid>92941dde-940d-4a00-a2ed-00f55828b046</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='b.'])[1]/preceding::p[20]</value>
      <webElementGuid>f63561ea-bf97-435c-8ca8-7f7153a53391</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='A01.00']/parent::*</value>
      <webElementGuid>ee26bf1d-7800-4cf3-a9e4-243834a173b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/p</value>
      <webElementGuid>42bd01b5-9dbd-446b-9ee6-538328418939</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'A01.00' or . = 'A01.00')]</value>
      <webElementGuid>788c5e9d-86c5-44fc-8252-2cac2db12694</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
