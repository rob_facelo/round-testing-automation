<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Agency ID_ctl00ContentPlaceHolderLogo_f0f538</name>
   <tag></tag>
   <elementGuidId>1e4ddfcc-8478-48df-81ba-9c722b812c4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolderLogonPage_LoginControl_txtAgencyId</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtAgencyId']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>953de61a-a3ac-420c-bd97-159996662ae9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolderLogonPage$LoginControl$txtAgencyId</value>
      <webElementGuid>b7c1ed38-8682-4e81-a5aa-04b7e41e8c95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>68205ae4-74b6-42ef-b11a-4af11f99388e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolderLogonPage_LoginControl_txtAgencyId</value>
      <webElementGuid>ad4246df-46bc-4175-a544-dd3ec57e8858</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>3ddadff1-8865-4cf2-a31e-83aeec850b7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolderLogonPage_LoginControl_txtAgencyId&quot;)</value>
      <webElementGuid>0fe464ba-e64c-4111-9921-5f8766869651</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtAgencyId']</value>
      <webElementGuid>935e4125-0bff-4f51-9553-5c76b611e458</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]/fieldset/input</value>
      <webElementGuid>b6e217c2-988e-402c-8092-091c0ca5c410</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/input</value>
      <webElementGuid>3193349b-daa2-4b63-863e-4763f84dd24e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$ContentPlaceHolderLogonPage$LoginControl$txtAgencyId' and @type = 'text' and @id = 'ContentPlaceHolderLogonPage_LoginControl_txtAgencyId']</value>
      <webElementGuid>0b11486c-ad26-40e1-9264-deba427a3a91</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
