<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_STYLES, FURIOUS</name>
   <tag></tag>
   <elementGuidId>393a630a-7df8-48a9-8e76-b42d4393ca8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.list-group-item-heading.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='widgetholder']/section/div[4]/div/div/div[2]/ul/li/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>db2b182a-181b-4ba1-8960-e1ba2129174d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item-heading ng-binding</value>
      <webElementGuid>115abd1b-f4ae-4924-9b20-e2841fb86f2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>STYLES, FURIOUS </value>
      <webElementGuid>21070e73-a736-4ce4-abc2-42e796cdbdca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;widgetholder&quot;)/section[@class=&quot;ng-scope&quot;]/div[@class=&quot;clearfix&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-primary&quot;]/div[@class=&quot;panel-body recentPatientActivity&quot;]/ul[@class=&quot;list-group&quot;]/li[@class=&quot;list-group-item ng-scope&quot;]/a[1]/span[@class=&quot;list-group-item-heading ng-binding&quot;]</value>
      <webElementGuid>41039833-6df8-4fc1-a1fe-4a25e5831e80</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='widgetholder']/section/div[4]/div/div/div[2]/ul/li/a/span</value>
      <webElementGuid>ddfdd07a-3393-436a-936d-3656454a087c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::span[1]</value>
      <webElementGuid>55f6060d-717e-4537-8d2e-715b86e5e764</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/following::span[3]</value>
      <webElementGuid>3550751d-d7e4-4217-b169-4b218f4e6c9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RABBIT, B'])[1]/preceding::span[1]</value>
      <webElementGuid>8760b3f9-4695-4b22-9a38-86c5b25a8cbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient Alerts'])[1]/preceding::span[3]</value>
      <webElementGuid>244ea387-71f0-4b0c-90bf-fdb39ac5d886</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='STYLES, FURIOUS']/parent::*</value>
      <webElementGuid>d3db2c87-2127-4a6b-94e1-c30cf5977ec6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/ul/li/a/span</value>
      <webElementGuid>2d09265c-4c82-42e6-b90f-9c813b6387ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'STYLES, FURIOUS ' or . = 'STYLES, FURIOUS ')]</value>
      <webElementGuid>e0a85a96-68da-468a-98bf-00558997cbc9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
