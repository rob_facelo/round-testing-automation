<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Stephen  Strange General Practice -</name>
   <tag></tag>
   <elementGuidId>85d5830e-f5c8-4315-9e48-e5731cf1cdc6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-10.col-sm-8.col-md-8.col-lg-8 > autocomplete-directive.ng-isolate-scope > div.dropdown > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OtherNoteContainer']/div/div[2]/div[2]/div/fieldset/div/div[2]/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9ee4ce49-be1b-473d-a6d8-5d2e86568217</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>187b1939-7508-4ed7-bf78-f40820c9ce31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Stephen  Strange General Practice - 
                    </value>
      <webElementGuid>b6194589-bd98-46ba-9b54-5b304ba4cf0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OtherNoteContainer&quot;)/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body other-modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/fieldset[1]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6 ng-scope&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-heading hhc-blue-lighter&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-10 col-sm-8 col-md-8 col-lg-8&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>22dd086d-6884-4140-863b-8d333001e733</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OtherNoteContainer']/div/div[2]/div[2]/div/fieldset/div/div[2]/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>d28e76da-71d8-42d0-b11e-e78c4794fc9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physician'])[3]/following::div[4]</value>
      <webElementGuid>5938d655-e25f-4e9c-9a99-963caf9af31e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Casey Acosta SN'])[1]/following::div[11]</value>
      <webElementGuid>ae53273b-b79d-467a-9c1a-a1504c3609c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[43]/preceding::div[3]</value>
      <webElementGuid>8674b2a6-551c-4455-a473-f67fe0acb88f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>b335ddf0-a005-4416-bd1a-ed269b84a4b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Stephen  Strange General Practice - 
                    ' or . = '
                        Stephen  Strange General Practice - 
                    ')]</value>
      <webElementGuid>c20dee6d-de1b-4dff-a5a3-a42220c92d4f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
