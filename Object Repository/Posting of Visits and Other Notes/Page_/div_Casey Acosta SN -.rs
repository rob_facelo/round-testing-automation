<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Casey Acosta SN -</name>
   <tag></tag>
   <elementGuidId>b2ac29e9-fc36-4e07-a402-7a5200669c1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OtherNoteContainer']/div/div[2]/div[2]/div/fieldset/div/div/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0da99db7-fee1-4808-b592-bb0194c7a7c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>504c5933-598c-4781-874c-ad7ddaeb0808</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Casey Acosta SN - 
                    </value>
      <webElementGuid>3b4d8a8c-71b6-4ebf-81f3-273eaff5a9db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OtherNoteContainer&quot;)/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body other-modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/fieldset[1]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-heading hhc-blue-lighter&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-8 col-sm-8 col-md-8 col-lg-8&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>6189e54a-0c13-4478-8823-24c92d4ce482</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OtherNoteContainer']/div/div[2]/div[2]/div/fieldset/div/div/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>d860f6c2-5aa9-4de8-b501-ad868a0297e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff'])[1]/following::div[4]</value>
      <webElementGuid>c034b156-60cd-451e-9c68-764d4f6006a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[42]/preceding::div[3]</value>
      <webElementGuid>d0725cde-611d-4445-93df-7831397c6422</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physician'])[3]/preceding::div[6]</value>
      <webElementGuid>5c31038f-1ace-4c51-80c1-711a5e5f07a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Casey Acosta SN -']/parent::*</value>
      <webElementGuid>430f608a-1c84-4046-a985-2e7d70af2f0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>fd339ee6-c47e-4756-99c5-75b94389daf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Casey Acosta SN - 
                    ' or . = '
                        Casey Acosta SN - 
                    ')]</value>
      <webElementGuid>0979611d-cd3c-425b-a98a-bb6f6464140c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
