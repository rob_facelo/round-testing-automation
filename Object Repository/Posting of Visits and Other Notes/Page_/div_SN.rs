<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_SN</name>
   <tag></tag>
   <elementGuidId>082677dc-d88e-44c7-8eeb-cebf87242fb6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#planPanel_6</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='planPanel_6']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>07806919-ab35-479a-bf79-37c50dd19232</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cellContent cellContentPlan</value>
      <webElementGuid>10b31e76-7c9f-4897-9713-60cb9a45f98e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>planPanel_6</value>
      <webElementGuid>9bb9e36f-d831-4161-90fd-96781b2382f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>cl.ShowOtherNotePanel($event, weekRow.weekNumber, dayRow, 'containerplan')</value>
      <webElementGuid>88b52712-ad16-479e-b724-36008b95eb45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-drop</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>09f11bf0-adf4-4cc9-9029-7d04edb2309c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jqyoui-droppable</name>
      <type>Main</type>
      <value>{beforeDrop: 'beforeDrop(dayRow.ItemDate, weekRow.weekNumber)'}</value>
      <webElementGuid>e5d2a59a-8291-4ae5-8b49-65deaed87891</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jqyoui-options</name>
      <type>Main</type>
      <value>{revertDuration: 500}</value>
      <webElementGuid>c669b519-1e6c-4c78-9e1f-1370ca23a04c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                                
                                    SN

                                     
                                    

                                    
                                    

                                    
                                

                                

                            </value>
      <webElementGuid>624d7f64-a4e0-458c-ba76-5a5c72720c2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;planPanel_6&quot;)</value>
      <webElementGuid>78722d0e-3d2c-4c64-a8c5-188f3fefba7e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='planPanel_6']</value>
      <webElementGuid>61e0b4b8-9545-4ca9-9974-185b71f11bb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='visitCalendarBody']/tr/td[7]/div[2]</value>
      <webElementGuid>6dd03016-0bfb-4e29-9d0a-52d3be130ebd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jul 1'])[1]/following::div[1]</value>
      <webElementGuid>c1ddc498-350f-4b50-9f8e-2bff4bc23a95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actual'])[1]/following::div[29]</value>
      <webElementGuid>394d3486-b45f-461a-a5ee-74ba0ff9bc3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SN'])[2]/preceding::div[2]</value>
      <webElementGuid>35233033-4352-4c13-bd81-0bb92acccfb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[7]/div[2]</value>
      <webElementGuid>86d8fe28-80c1-4184-ba37-0c1d3e6fcb76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'planPanel_6' and (text() = '

                                
                                    SN

                                     
                                    

                                    
                                    

                                    
                                

                                

                            ' or . = '

                                
                                    SN

                                     
                                    

                                    
                                    

                                    
                                

                                

                            ')]</value>
      <webElementGuid>33b0f43c-3e78-4415-8ebd-5252f8fc175a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
