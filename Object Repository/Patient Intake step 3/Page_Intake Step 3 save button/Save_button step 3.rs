<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Save_button step 3</name>
   <tag></tag>
   <elementGuidId>44257c36-2a0d-4040-b504-5fd8b8b53f9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='intakewinfo']/div[4]/div/div/div/div/div/button/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.well > div > div.modal-header > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > button.btn.btn-default.font12 > i.fa.fa-floppy-o.font-weight-600</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[1]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12&quot;]/i[@class=&quot;fa fa-floppy-o font-weight-600&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>732a5299-71b2-49a9-b150-415eb5fe5153</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-floppy-o font-weight-600</value>
      <webElementGuid>370f8df0-a277-48c4-81d6-5be875b174e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[1]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12&quot;]/i[@class=&quot;fa fa-floppy-o font-weight-600&quot;]</value>
      <webElementGuid>62d9b297-0e1f-4315-b17e-573e89e0631a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[4]/div/div/div/div/div/button/i</value>
      <webElementGuid>161a8e95-3b00-410b-a854-e87091f111f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div/div/button/i</value>
      <webElementGuid>6b7c34ba-ecbd-4e22-b86e-cde3345d9d47</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
