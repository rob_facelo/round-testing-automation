<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_32275  styles private</name>
   <tag></tag>
   <elementGuidId>6f83ba56-83b6-4943-b6c5-09fbff665ec4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_PrivateSubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PrivateSubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a6f4300b-2556-4146-acf5-7b240d36d98f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>32275 / styles private</value>
      <webElementGuid>49b5c4e7-9da3-4325-835e-ba7e55097520</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PrivateSubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0&quot;)/span[1]</value>
      <webElementGuid>a3707452-0897-4cf3-8417-3fe7387648eb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_PrivateSubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0']/span</value>
      <webElementGuid>d6c307b5-772f-42fd-a5fe-276fd5308ff3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[2]/following::span[4]</value>
      <webElementGuid>854b9cf4-acc8-4e05-8f65-0bc7a919524a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unbatch'])[1]/following::span[5]</value>
      <webElementGuid>4748c9f1-a4f4-4c0f-8ec0-d541ab0d0c76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download 837 File'])[1]/preceding::span[1]</value>
      <webElementGuid>a6326061-f3d3-4e0c-b5e2-eaf78babffe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$450.00'])[1]/preceding::span[2]</value>
      <webElementGuid>c0708aae-fb15-4be1-b2c7-fbc92021ca01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='32275 / styles private']/parent::*</value>
      <webElementGuid>0612d04b-07ef-4687-b7a7-16c1c57dc3c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/a/span</value>
      <webElementGuid>ea787d3c-0f20-4d9d-b8ac-80b1b37ff571</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '32275 / styles private' or . = '32275 / styles private')]</value>
      <webElementGuid>cdeb3494-4c5b-491b-b145-06112bd161a2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
