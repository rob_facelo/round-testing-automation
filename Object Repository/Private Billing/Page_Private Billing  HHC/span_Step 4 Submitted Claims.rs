<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Step 4 Submitted Claims</name>
   <tag></tag>
   <elementGuidId>c0c41517-ea44-4c1b-b76d-740015e53a6a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i3_T']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#applicationLeftNav_LeftPanel_nbLeftNavigation_I0i3_T > span.dx-vam.dx-wrap</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0e1e2806-6646-41c3-9155-203d9268a3a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam dx-wrap</value>
      <webElementGuid>99ae328d-8d6c-4518-97e6-426def6397f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Step 4: Submitted Claims</value>
      <webElementGuid>27e24ad9-e8a4-437f-a792-f74bfb961729</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;applicationLeftNav_LeftPanel_nbLeftNavigation_I0i3_T&quot;)/span[@class=&quot;dx-vam dx-wrap&quot;]</value>
      <webElementGuid>a425d96c-5072-4333-bfdb-a0ac5355ceb7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i3_T']/span</value>
      <webElementGuid>9a4a8cb7-f8b6-4d7f-bcd3-16b8efaba50f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 3: Export Claims'])[1]/following::span[1]</value>
      <webElementGuid>1959f0f0-786d-41dc-ada3-d50137e59bfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 2: Batch Claims'])[1]/following::span[2]</value>
      <webElementGuid>c431e461-5140-4ad2-b0e9-f536fe742c8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EDI Report Files'])[1]/preceding::span[1]</value>
      <webElementGuid>194882e5-9c40-40ca-b144-780fcaf1cbd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Claims Reconciliation'])[1]/preceding::span[2]</value>
      <webElementGuid>5b24d72a-90c9-4df8-b7b6-aaadfa2b0569</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Step 4: Submitted Claims']/parent::*</value>
      <webElementGuid>13f3e635-b787-4e27-9434-195efff5cd11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a/span</value>
      <webElementGuid>fe768fb6-76d0-4778-92e4-862144172707</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Step 4: Submitted Claims' or . = 'Step 4: Submitted Claims')]</value>
      <webElementGuid>5214365e-223f-4aeb-a940-5714e94b8867</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
