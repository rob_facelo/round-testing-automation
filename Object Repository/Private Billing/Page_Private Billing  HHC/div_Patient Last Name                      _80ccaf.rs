<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Patient Last Name                      _80ccaf</name>
   <tag></tag>
   <elementGuidId>e5e5fecb-1998-486a-8d5e-bb941f74bf43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='angularElement']/div/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.panel-body.font-weight-600</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e7fd81ab-f14b-4df6-ae5e-8c9a5d3783cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>panel-body font-weight-600</value>
      <webElementGuid>2d55b971-2a7f-4451-96fc-e5eb081d8b20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            
                                
                                    Patient Last Name:
                                
                                
                                    
		
			
		
	
&lt;!--
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientTextBox('ContentPlaceHolder_BatchClaimsControl_txtPatientLastName');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_txtPatientLastName');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$txtPatientLastName';
dxo.stateObject = ({'rawValue':''});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.styleDecoration.AddStyle('N','dxeNullText','');
dxo.nullText = 'Enter Patient Last Name';
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                                    From:
                                
                                
                                    
&lt;!--
(function(){var a = ({'numNegInf':'-∞','percentPattern':1,'numPosInf':'∞'});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2021
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											4431123456
										
											4578910111213
										
											4614151617181920
										
											4721222324252627
										
											482829301234
										
											49567891011
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C$FNP';
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='fade';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C',[[['dxeCalendarButtonHover'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[['dxeCalendarFastNavMonthHover'],[''],['FNP_M0','FNP_M1','FNP_M2','FNP_M3','FNP_M4','FNP_M5','FNP_M6','FNP_M7','FNP_M8','FNP_M9','FNP_M10','FNP_M11']],[['dxeCalendarFastNavYearHover'],[''],['FNP_Y0','FNP_Y1','FNP_Y2','FNP_Y3','FNP_Y4','FNP_Y5','FNP_Y6','FNP_Y7','FNP_Y8','FNP_Y9']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C',[[['dxeCalendarButtonPressed'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C',[[['dxeDisabled'],[''],['']],[['dxeDisabled dxeButtonDisabled'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[[''],[''],['PYC','PMC','NMC','NYC'],,[[{'spriteCssClass':'dxEditors_edtCalendarPrevYearDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarPrevMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextYearDisabled'}]],['Img']]]);

var dxo = new ASPxClientCalendar('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C';
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2021,10,16);
dxo.selection.AddArray([new Date(2021,10,16,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD';
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown('ContentPlaceHolder_BatchClaimsControl_dteFromDate', e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='slide';
dxo.closeAction='CloseButton';
dxo.popupHorizontalAlign='LeftSides';
dxo.popupVerticalAlign='Below';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate',[[['dxeButtonEditButtonHover'],[''],['B-1']]]);
ASPx.RemoveHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate',[[['B-100']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate',[[['dxeButtonEditButtonPressed'],[''],['B-1']]]);
ASPx.RemovePressedItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate',[[['B-100']]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit('ContentPlaceHolder_BatchClaimsControl_dteFromDate');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteFromDate');
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate',arg,ASPx.Callback,'ContentPlaceHolder_BatchClaimsControl_dteFromDate',ASPx.CallbackError,true); };
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate';
dxo.stateObject = ({'rawValue':'1637020800000'});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.outOfRangeWarningClassName='dxeOutOfRWarn dxeOutOfRWarnRight';
dxo.outOfRangeWarningMessages=['The date must be in the range {0}...{1}', 'The date must be greater than or equal to {0}', 'The date must be less than or equal to {0}'];
dxo.date = new Date(2021,10,16);
dxo.dateFormatter = ASPx.DateFormatter.Create('M/d/yyyy');
dxo.AfterCreate();

//-->

                                
                            
                        
                        
                            
                                
                                    To:
                                
                                
                                    
		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2022
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											44303112345
										
											456789101112
										
											4613141516171819
										
											4720212223242526
										
											4827282930123
										
											4945678910
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD$C$FNP';
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='fade';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C',[[['dxeCalendarButtonHover'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[['dxeCalendarFastNavMonthHover'],[''],['FNP_M0','FNP_M1','FNP_M2','FNP_M3','FNP_M4','FNP_M5','FNP_M6','FNP_M7','FNP_M8','FNP_M9','FNP_M10','FNP_M11']],[['dxeCalendarFastNavYearHover'],[''],['FNP_Y0','FNP_Y1','FNP_Y2','FNP_Y3','FNP_Y4','FNP_Y5','FNP_Y6','FNP_Y7','FNP_Y8','FNP_Y9']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C',[[['dxeCalendarButtonPressed'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C',[[['dxeDisabled'],[''],['']],[['dxeDisabled dxeButtonDisabled'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[[''],[''],['PYC','PMC','NMC','NYC'],,[[{'spriteCssClass':'dxEditors_edtCalendarPrevYearDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarPrevMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextYearDisabled'}]],['Img']]]);

var dxo = new ASPxClientCalendar('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD$C';
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2022,10,23);
dxo.selection.AddArray([new Date(2022,10,23,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD';
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown('ContentPlaceHolder_BatchClaimsControl_dteToDate', e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='slide';
dxo.closeAction='CloseButton';
dxo.popupHorizontalAlign='LeftSides';
dxo.popupVerticalAlign='Below';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteToDate',[[['dxeButtonEditButtonHover'],[''],['B-1']]]);
ASPx.RemoveHoverItems('ContentPlaceHolder_BatchClaimsControl_dteToDate',[[['B-100']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_dteToDate',[[['dxeButtonEditButtonPressed'],[''],['B-1']]]);
ASPx.RemovePressedItems('ContentPlaceHolder_BatchClaimsControl_dteToDate',[[['B-100']]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit('ContentPlaceHolder_BatchClaimsControl_dteToDate');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteToDate');
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate',arg,ASPx.Callback,'ContentPlaceHolder_BatchClaimsControl_dteToDate',ASPx.CallbackError,true); };
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate';
dxo.stateObject = ({'rawValue':'1669161600000'});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.outOfRangeWarningClassName='dxeOutOfRWarn dxeOutOfRWarnRight';
dxo.outOfRangeWarningMessages=['The date must be in the range {0}...{1}', 'The date must be greater than or equal to {0}', 'The date must be less than or equal to {0}'];
dxo.date = new Date(2022,10,23);
dxo.dateFormatter = ASPx.DateFormatter.Create('M/d/yyyy');
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                                    
                                
                                
                                    
		
			
				
			Search
		
	
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_btnSearch',[[['dxbButtonHover'],[''],[''],['','TC']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_btnSearch',[[['dxbButtonPressed'],[''],[''],['','TC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_btnSearch',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_btnSearch');
dxo.InitGlobalVariable('btnSearch');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$btnSearch';
dxo.Init.AddHandler(function (s, e) { AddGlyphiconToDevexpressButton(s, 'glyphicon-search') ;} );
dxo.Click.AddHandler(SearchClaims);
ASPx.AddSelectedItems('ContentPlaceHolder_BatchClaimsControl_btnSearch',[[['dxbf'],[''],['CD']]]);
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                                    
		
			
				
			Batch Claims
		
	
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_btnBatchPopup',[[['dxbButtonHover'],[''],[''],['','TC']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_btnBatchPopup',[[['dxbButtonPressed'],[''],[''],['','TC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_btnBatchPopup',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_btnBatchPopup');
dxo.InitGlobalVariable('btnBatchPopup');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$btnBatchPopup';
dxo.Init.AddHandler(function (s, e) { AddGlyphiconToDevexpressButton(s, 'glyphicon-send') ;});
dxo.Click.AddHandler(BatchClaimPopup);
ASPx.AddSelectedItems('ContentPlaceHolder_BatchClaimsControl_btnBatchPopup',[[['dxbf'],[''],['CD']]]);
dxo.AfterCreate();

//-->

                                    
		
			
				
			Undraft Claims
		
	
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_btnUndraft',[[['dxbButtonHover'],[''],[''],['','TC']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_btnUndraft',[[['dxbButtonPressed'],[''],[''],['','TC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_btnUndraft',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_btnUndraft');
dxo.InitGlobalVariable('btnPrinterFriendly');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$btnUndraft';
dxo.Click.AddHandler(UndraftClaim);
ASPx.AddSelectedItems('ContentPlaceHolder_BatchClaimsControl_btnUndraft',[[['dxbf'],[''],['CD']]]);
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
		
			
				
					
						
							
						
					
						
							Claim No. 
						
					
						
							Patient Name 
						
					
						
							Payor 
						
					
						
							Episode Dates 
						
					
						
							Billable Visable Dates 
						
					
						
							Total Charges 
						
					
						
							History 
						
					
				
					 
				
					8816
                                            PAYNE, LLYNA F.
                                            
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_4_btnVisitCompliance_0',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_4_btnVisitCompliance_0');
dxo.InitGlobalVariable('btnVisitCompliance');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_4$TC$btnVisitCompliance';
dxo.Click.AddHandler(function(s, e) { bootbox.alert(&quot;Under construction...&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_4$TC$btnVisitCompliance&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        
                                            Medicaid
                                            
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_5_btnUB04AdjustmentPanel_0',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_5_btnUB04AdjustmentPanel_0');
dxo.InitGlobalVariable('btnUB04AdjustmentPanel');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_5$TC$btnUB04AdjustmentPanel';
dxo.Click.AddHandler(function(s, e) { ShowUB04AdjustmentPanelModal(&quot;8816&quot;,&quot;8237&quot;,&quot;bbstep2&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_5$TC$btnUB04AdjustmentPanel&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        08/10/2022-10/08/202208/30/2022 - 09/03/2022
                                            $750.00
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0',[[['dxeDisabled'],[''],['']]]);

var dxo = new ASPxClientHyperLink('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_8$TC$hyplnkShowUB04Modal';
dxo.Click.AddHandler(function(s, e) { ShowUB04Modal(&quot;8816&quot;, &quot;private&quot;); });
dxo.AfterCreate();

//-->

                                        
                                        
						
							
								
							View Claim History
						
					
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0',[[['dxbButtonHover'],[''],[''],['','TC']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0',[[['dxbButtonPressed'],[''],[''],['','TC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0');
dxo.InitGlobalVariable('btnHistory');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_9$TC$btnHistory';
dxo.Click.AddHandler(function(s, e) { ViewClaimHistory(&quot;PAYNE, LLYNA F.&quot;,&quot;8237&quot;); });
ASPx.AddSelectedItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0',[[['dxbf'],[''],['CD']]]);
dxo.AfterCreate();

//-->

                                    
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn0',[[['dxbDisabled_Moderno1'],[''],[''],['','TC']]]);

//-->

&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn1',[[['dxbDisabled_Moderno1'],[''],[''],['','TC']]]);

//-->

				
					8866
                                            STYLES, FURIOUS
                                            
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_4_btnVisitCompliance_1',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_4_btnVisitCompliance_1');
dxo.InitGlobalVariable('btnVisitCompliance');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_4$TC$btnVisitCompliance';
dxo.Click.AddHandler(function(s, e) { bootbox.alert(&quot;Under construction...&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_4$TC$btnVisitCompliance&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        
                                            Medicaid
                                            
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_5_btnUB04AdjustmentPanel_1',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_5_btnUB04AdjustmentPanel_1');
dxo.InitGlobalVariable('btnUB04AdjustmentPanel');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_5$TC$btnUB04AdjustmentPanel';
dxo.Click.AddHandler(function(s, e) { ShowUB04AdjustmentPanelModal(&quot;8866&quot;,&quot;8257&quot;,&quot;bbstep2&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_5$TC$btnUB04AdjustmentPanel&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        07/01/2022-08/29/202207/03/2022 - 07/07/2022
                                            $450.00
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1',[[['dxeDisabled'],[''],['']]]);

var dxo = new ASPxClientHyperLink('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_8$TC$hyplnkShowUB04Modal';
dxo.Click.AddHandler(function(s, e) { ShowUB04Modal(&quot;8866&quot;, &quot;private&quot;); });
dxo.AfterCreate();

//-->

                                        
                                        
						
							
								
							View Claim History
						
					
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1',[[['dxbButtonHover'],[''],[''],['','TC']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1',[[['dxbButtonPressed'],[''],[''],['','TC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1',[[['dxbDisabled'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1');
dxo.InitGlobalVariable('btnHistory');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_9$TC$btnHistory';
dxo.Click.AddHandler(function(s, e) { ViewClaimHistory(&quot;STYLES, FURIOUS&quot;,&quot;8257&quot;); });
ASPx.AddSelectedItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1',[[['dxbf'],[''],['CD']]]);
dxo.AfterCreate();

//-->

                                    
&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn2',[[['dxbDisabled_Moderno1'],[''],[''],['','TC']]]);

//-->

&lt;!--
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn3',[[['dxbDisabled_Moderno1'],[''],[''],['','TC']]]);

//-->

				
			
				
					Page 1 of 1 (2 items)1 Page size:
				
					
						
							
								
									10
								
									20
								
									50
								
							
						
					
				
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP',[[[''],[''],['DXME_']],[['dxm-hovered',''],['',''],['DXI0_','DXI1_','DXI2_'],['','T']]]);
ASPx.AddSelectedItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP',[[['dxm-selected',''],['',''],['DXI0_','DXI1_','DXI2_'],['','T']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP',[[['dxm-disabled'],[''],['DXI0_','DXI1_','DXI2_'],['','T']]]);

var dxo = new ASPxClientPopupMenu('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$DXPagerBottom$PSP';
dxo.ItemClick.AddHandler(function(s,e) { ASPx.POnPageSizePopupItemClick('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom', e.item); });
dxo.renderData={'':[[0],[1],[2]]};
dxo.subMenuFIXOffset=1;
dxo.subMenuFIYOffset=-2;
dxo.subMenuLIXOffset=1;
dxo.subMenuLIYOffset=-2;
dxo.subMenuXOffset=1;
dxo.subMenuYOffset=-2;
dxo.rootSubMenuFIXOffset=1;
dxo.rootSubMenuFIYOffset=-2;
dxo.rootSubMenuLIXOffset=1;
dxo.rootSubMenuLIYOffset=-2;
dxo.rootSubMenuXOffset=1;
dxo.rootSubMenuYOffset=-2;
dxo.allowSelectItem=true;
dxo.selectedItemIndexPath='1';
dxo.CreateItems([{'name':'10'},{'name':'20'},{'name':'50'}]);
dxo.popupElementIDList=['ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSB'];
dxo.popupHorizontalAlign='LeftSides'
dxo.popupVerticalAlign='Below';
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom',[[['dxp-hoverComboBox'],[''],['PSB']],[['dxp-hoverDropDownButton'],[''],['DDB']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom',[[['dxp-pressedComboBox'],[''],['PSB']],[['dxp-pressedDropDownButton'],[''],['DDB']]]);

var dxo = new ASPxClientPager('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$DXPagerBottom';
dxo.hasOwnerControl=true;
dxo.requireInlineLayout=true;
dxo.enableAdaptivity=true;
dxo.pageSizeItems = [{'text':'10','value':'10'},{'text':'20','value':'20'},{'text':'50','value':'50'}];
dxo.pageSizeSelectedItem = ({'text':'20','value':20});
dxo.pageSizeChangedHandler.AddHandler(function(s, e) { ASPx.GVPagerOnClick('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim',e.value); });
dxo.AfterCreate();

//-->

			
				
					Loading…
				
			

			
		
	
&lt;!--

var dxo = new ASPxClientGridView('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim');
dxo.InitGlobalVariable('grvBatchClaim');
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim',arg,ASPx.Callback,'ContentPlaceHolder_BatchClaimsControl_grvBatchClaim',ASPx.CallbackError,true); };
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim';
dxo.EndCallback.AddHandler(grvBatchClaim_EndCallback);
dxo.stateObject = ({'selection':'','callbackState':'BwQHAwIERGF0YQahAgAAAAACAAAAAgAAAAAAAAACAAAAAAoAAAAHYmlsbF9pZAdiaWxsX2lkAwAAAQxtMDAyMF9wYXRfaWQMbTAwMjBfcGF0X2lkBAAAAQplcGlzb2RlX2lkCmVwaXNvZGVfaWQDAAABCVBheW9yTmFtZQpQYXlvciBOYW1lBwAAAQtQYXRpZW50TmFtZQxQYXRpZW50IE5hbWUHAAABDEVwaXNvZGVEYXRlcw1FcGlzb2RlIERhdGVzBwAAARJCaWxsYWJsZVZpc2l0RGF0ZXMUQmlsbGFibGUgVmlzaXQgRGF0ZXMHAAABDFRvdGFsQ2hhcmdlcw1Ub3RhbCBDaGFyZ2VzBQAAARBsYXN0X3VwZGF0ZV9kYXRlEGxhc3RfdXBkYXRlX2RhdGUIAAABE2xhc3RfdXBkYXRlX3VzZXJfaWQTbGFzdF91cGRhdGVfdXNlcl9pZAcAAAEDAAAACGNsYWltX25vDGY2X2Zyb21fZGF0ZQ9mNF90eXBlX29mX2JpbGwAAAAABwAHAAcABwAHAAb//wMGcCIEBnsBAwYtIAcCCE1lZGljYWlkBwIPUEFZTkUsIExMWU5BIEYuBwIVMDgvMTAvMjAyMi0xMC8wOC8yMDIyBwIXMDgvMzAvMjAyMiAtIDA5LzAzLzIwMjIFA+BwcgAAAAAAAAAAAAAABAAIAgTgZUrKQqfaCAcCCW1lLm9mZmljZQcABwAG//8DBqIiBAaQAQMGQSAHAghNZWRpY2FpZAcCD1NUWUxFUywgRlVSSU9VUwcCFTA3LzAxLzIwMjItMDgvMjkvMjAyMgcCFzA3LzAzLzIwMjIgLSAwNy8wNy8yMDIyBQMgqkQAAAAAAAAAAAAAAAQACAIEwJNrixjI2ggHAglkc2xmYWNlbG8CC0Zvcm1hdFN0YXRlBwACBVN0YXRlB24HCgcAAgEHAQIABwECAAcBAgEHAgIBBwMCAQcEAgEHBQIBBwYCAQcHAgEHAAcABwAHAAIABQAAAIAJAgdiaWxsX2lkBwECB2JpbGxfaWQDCQIAAgADBwQCAAcAAgEHAgcAAgEJAgEHAAcABwAHAAIIUGFnZVNpemUDBxQ=','keys':['8816','8866']});
dxo.callBacksEnabled=true;
dxo.pageRowCount=2;
dxo.pageRowSize=20;
dxo.pageIndex=0;
dxo.pageCount=1;
dxo.selectedWithoutPageRowCount=0;
dxo.visibleStartIndex=0;
dxo.focusedRowIndex=-1;
dxo.allowFocusedRow=false;
dxo.checkBoxImageProperties = {'4':['dxWeb_edtCheckBoxChecked_Moderno1','dxWeb_edtCheckBoxUnchecked_Moderno1','dxWeb_edtCheckBoxGrayed_Moderno1'],'8':['dxWeb_edtCheckBoxCheckedDisabled_Moderno1','dxWeb_edtCheckBoxUncheckedDisabled_Moderno1','dxWeb_edtCheckBoxGrayedDisabled_Moderno1']};
dxo.icbFocusedStyle = ['dxICBFocused_Moderno1',''];
dxo.allowSelectByItemClick=false;
dxo.allowSelectSingleRowOnly=false;
dxo.callbackOnFocusedRowChanged=false;
dxo.callbackOnSelectionChanged=false;
dxo.columns = [new ASPxClientGridViewColumn('',0,-1,null,1,'',0,0,0,1,0,0,0,1),
new ASPxClientGridViewColumn('',1,-1,'last_update_date',0,'',0,1,1,1,1,1,0,0),
new ASPxClientGridViewColumn('',2,-1,'last_update_user_id',0,'',0,1,1,1,0,1,0,0),
new ASPxClientGridViewColumn('',3,-1,'bill_id',1,'',0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn('',4,-1,'',1,'',0,0,0,1,0,0,0,0),
new ASPxClientGridViewColumn('',5,-1,'PayorName',1,'',0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn('',6,-1,'EpisodeDates',1,'',0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn('',7,-1,'BillableVisitDates',1,'',0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn('',8,-1,'',1,'',0,0,0,1,0,0,0,0),
new ASPxClientGridViewColumn('',9,-1,'',1,'',0,0,0,1,0,0,0,0)];
dxo.editState=0;
dxo.editItemVisibleIndex=-1;
dxo.searchPanelFilter='';
dxo.editMode=2;
dxo.indentColumnCount=0;
dxo.allowMultiColumnAutoFilter=false;
dxo.SetAdaptiveMode(({'hideDataCellsWindowInnerWidth':0,'allowTextTruncationInAdaptiveMode':[false,true,true,true,false,false,false,false],'allowOnlyOneAdaptiveDetailExpanded':true,'adaptivityMode':1,'adaptiveDetailColumnCount':1,'adaptiveColumnsOrder':[9,8,7,5,6,4,3,0]}));
dxo.selectAllBtnStateWithoutPage=null;
dxo.selectAllSettings=[{'index':0,'mode':1,'selectText':'Select all rows on this page','unselectText':'Deselect all rows on this page'}];
dxo.AfterCreate();

//-->

&lt;!--
ASPxClientGridBase.InitializeStyles('ContentPlaceHolder_BatchClaimsControl_grvBatchClaim',({'sel':{'css':'dxgvSelectedRow_Moderno1'},'fi':{'css':'dxgvFocusedRow_Moderno1'},'bec':{'css':'dxgvBatchEditCell_Moderno1 dxgv'},'bemc':{'css':'dxgvBatchEditModifiedCell_Moderno1 dxgv'},'bemergmc':{'css':'dxgvBatchEditCell_Moderno1 dxgvBatchEditModifiedCell_Moderno1 dxgv'},'ei':'&lt;tr class=&quot;dxgvEditingErrorRow_Moderno1&quot;>\r\n\t&lt;td class=&quot;dxgv&quot; data-colSpan=&quot;8&quot; style=&quot;border-right-width:0px;&quot;>&lt;/td>&lt;td class=&quot;dxgvAIC dxgv&quot; style=&quot;border-left-width:0px;border-right-width:0px;&quot;>&amp;nbsp;&lt;/td>\r\n&lt;/tr>','fgi':{'css':'dxgvFocusedGroupRow_Moderno1'}}),['ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn0','ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn1','ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn2','ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn3'])
//-->

                        
                    
                </value>
      <webElementGuid>a3687f51-aadc-447a-9187-cba6b092838a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;angularElement&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-body font-weight-600&quot;]</value>
      <webElementGuid>85dd94da-0675-4e72-b602-b7114c7c362c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='angularElement']/div/div/div/div[2]</value>
      <webElementGuid>395bf8e8-65d0-4528-a567-812dfb09d638</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drafted Bill(s) - Ready for Batching'])[1]/following::div[1]</value>
      <webElementGuid>82eef3df-c78e-4955-af2b-e036844f6017</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bill Search'])[1]/following::div[13]</value>
      <webElementGuid>ef1e9987-de9c-4dbd-8b25-1d479317e563</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div[3]/div/div/div/div/div/div[2]</value>
      <webElementGuid>6e8f3684-31b5-4194-b792-678a8d53ec9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                    
                        
                            
                                
                                    Patient Last Name:
                                
                                
                                    
		
			
		
	
&lt;!--
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientTextBox(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_txtPatientLastName&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_txtPatientLastName&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$txtPatientLastName&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;N&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeNullText&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.nullText = &quot; , &quot;'&quot; , &quot;Enter Patient Last Name&quot; , &quot;'&quot; , &quot;;
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                                    From:
                                
                                
                                    
&lt;!--
(function(){var a = ({&quot; , &quot;'&quot; , &quot;numNegInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;-∞&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;percentPattern&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;numPosInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;∞&quot; , &quot;'&quot; , &quot;});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2021
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											4431123456
										
											4578910111213
										
											4614151617181920
										
											4721222324252627
										
											482829301234
										
											49567891011
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2021,10,16);
dxo.selection.AddArray([new Date(2021,10,16,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1637020800000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2021,10,16);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;M/d/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                
                            
                        
                        
                            
                                
                                    To:
                                
                                
                                    
		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2022
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											44303112345
										
											456789101112
										
											4613141516171819
										
											4720212223242526
										
											4827282930123
										
											4945678910
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2022,10,23);
dxo.selection.AddArray([new Date(2022,10,23,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1669161600000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2022,10,23);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;M/d/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                                    
                                
                                
                                    
		
			
				
			Search
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnSearch&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$btnSearch&quot; , &quot;'&quot; , &quot;;
dxo.Init.AddHandler(function (s, e) { AddGlyphiconToDevexpressButton(s, &quot; , &quot;'&quot; , &quot;glyphicon-search&quot; , &quot;'&quot; , &quot;) ;} );
dxo.Click.AddHandler(SearchClaims);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                                    
		
			
				
			Batch Claims
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnBatchPopup&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$btnBatchPopup&quot; , &quot;'&quot; , &quot;;
dxo.Init.AddHandler(function (s, e) { AddGlyphiconToDevexpressButton(s, &quot; , &quot;'&quot; , &quot;glyphicon-send&quot; , &quot;'&quot; , &quot;) ;});
dxo.Click.AddHandler(BatchClaimPopup);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                    
		
			
				
			Undraft Claims
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnPrinterFriendly&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$btnUndraft&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(UndraftClaim);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
		
			
				
					
						
							
						
					
						
							Claim No. 
						
					
						
							Patient Name 
						
					
						
							Payor 
						
					
						
							Episode Dates 
						
					
						
							Billable Visable Dates 
						
					
						
							Total Charges 
						
					
						
							History 
						
					
				
					 
				
					8816
                                            PAYNE, LLYNA F.
                                            
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_4_btnVisitCompliance_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_4_btnVisitCompliance_0&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnVisitCompliance&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_4$TC$btnVisitCompliance&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { bootbox.alert(&quot;Under construction...&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_4$TC$btnVisitCompliance&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        
                                            Medicaid
                                            
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_5_btnUB04AdjustmentPanel_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_5_btnUB04AdjustmentPanel_0&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnUB04AdjustmentPanel&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_5$TC$btnUB04AdjustmentPanel&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ShowUB04AdjustmentPanelModal(&quot;8816&quot;,&quot;8237&quot;,&quot;bbstep2&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_5$TC$btnUB04AdjustmentPanel&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        08/10/2022-10/08/202208/30/2022 - 09/03/2022
                                            $750.00
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientHyperLink(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_8$TC$hyplnkShowUB04Modal&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ShowUB04Modal(&quot;8816&quot;, &quot;private&quot;); });
dxo.AfterCreate();

//-->

                                        
                                        
						
							
								
							View Claim History
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnHistory&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_9$TC$btnHistory&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ViewClaimHistory(&quot;PAYNE, LLYNA F.&quot;,&quot;8237&quot;); });
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                    
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled_Moderno1&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

//-->

&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled_Moderno1&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

//-->

				
					8866
                                            STYLES, FURIOUS
                                            
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_4_btnVisitCompliance_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_4_btnVisitCompliance_1&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnVisitCompliance&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_4$TC$btnVisitCompliance&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { bootbox.alert(&quot;Under construction...&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_4$TC$btnVisitCompliance&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        
                                            Medicaid
                                            
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_5_btnUB04AdjustmentPanel_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_5_btnUB04AdjustmentPanel_1&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnUB04AdjustmentPanel&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_5$TC$btnUB04AdjustmentPanel&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ShowUB04AdjustmentPanelModal(&quot;8866&quot;,&quot;8257&quot;,&quot;bbstep2&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_5$TC$btnUB04AdjustmentPanel&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        07/01/2022-08/29/202207/03/2022 - 07/07/2022
                                            $450.00
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientHyperLink(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_8$TC$hyplnkShowUB04Modal&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ShowUB04Modal(&quot;8866&quot;, &quot;private&quot;); });
dxo.AfterCreate();

//-->

                                        
                                        
						
							
								
							View Claim History
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnHistory&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_9$TC$btnHistory&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ViewClaimHistory(&quot;STYLES, FURIOUS&quot;,&quot;8257&quot;); });
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                    
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn2&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled_Moderno1&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

//-->

&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn3&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled_Moderno1&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

//-->

				
			
				
					Page 1 of 1 (2 items)1 Page size:
				
					
						
							
								
									10
								
									20
								
									50
								
							
						
					
				
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DXME_&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxm-hovered&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DXI0_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI1_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI2_&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;T&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxm-selected&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DXI0_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI1_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI2_&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;T&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxm-disabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DXI0_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI1_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI2_&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;T&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupMenu(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$DXPagerBottom$PSP&quot; , &quot;'&quot; , &quot;;
dxo.ItemClick.AddHandler(function(s,e) { ASPx.POnPageSizePopupItemClick(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;, e.item); });
dxo.renderData={&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;:[[0],[1],[2]]};
dxo.subMenuFIXOffset=1;
dxo.subMenuFIYOffset=-2;
dxo.subMenuLIXOffset=1;
dxo.subMenuLIYOffset=-2;
dxo.subMenuXOffset=1;
dxo.subMenuYOffset=-2;
dxo.rootSubMenuFIXOffset=1;
dxo.rootSubMenuFIYOffset=-2;
dxo.rootSubMenuLIXOffset=1;
dxo.rootSubMenuLIYOffset=-2;
dxo.rootSubMenuXOffset=1;
dxo.rootSubMenuYOffset=-2;
dxo.allowSelectItem=true;
dxo.selectedItemIndexPath=&quot; , &quot;'&quot; , &quot;1&quot; , &quot;'&quot; , &quot;;
dxo.CreateItems([{&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;10&quot; , &quot;'&quot; , &quot;},{&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;20&quot; , &quot;'&quot; , &quot;},{&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;50&quot; , &quot;'&quot; , &quot;}]);
dxo.popupElementIDList=[&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSB&quot; , &quot;'&quot; , &quot;];
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxp-hoverComboBox&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PSB&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxp-hoverDropDownButton&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DDB&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxp-pressedComboBox&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PSB&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxp-pressedDropDownButton&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DDB&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPager(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$DXPagerBottom&quot; , &quot;'&quot; , &quot;;
dxo.hasOwnerControl=true;
dxo.requireInlineLayout=true;
dxo.enableAdaptivity=true;
dxo.pageSizeItems = [{&quot; , &quot;'&quot; , &quot;text&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;10&quot; , &quot;'&quot; , &quot;},{&quot; , &quot;'&quot; , &quot;text&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;20&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;20&quot; , &quot;'&quot; , &quot;},{&quot; , &quot;'&quot; , &quot;text&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;50&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;50&quot; , &quot;'&quot; , &quot;}];
dxo.pageSizeSelectedItem = ({&quot; , &quot;'&quot; , &quot;text&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;20&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;:20});
dxo.pageSizeChangedHandler.AddHandler(function(s, e) { ASPx.GVPagerOnClick(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim&quot; , &quot;'&quot; , &quot;,e.value); });
dxo.AfterCreate();

//-->

			
				
					Loading…
				
			

			
		
	
&lt;!--

var dxo = new ASPxClientGridView(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;grvBatchClaim&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim&quot; , &quot;'&quot; , &quot;;
dxo.EndCallback.AddHandler(grvBatchClaim_EndCallback);
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;selection&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;callbackState&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;BwQHAwIERGF0YQahAgAAAAACAAAAAgAAAAAAAAACAAAAAAoAAAAHYmlsbF9pZAdiaWxsX2lkAwAAAQxtMDAyMF9wYXRfaWQMbTAwMjBfcGF0X2lkBAAAAQplcGlzb2RlX2lkCmVwaXNvZGVfaWQDAAABCVBheW9yTmFtZQpQYXlvciBOYW1lBwAAAQtQYXRpZW50TmFtZQxQYXRpZW50IE5hbWUHAAABDEVwaXNvZGVEYXRlcw1FcGlzb2RlIERhdGVzBwAAARJCaWxsYWJsZVZpc2l0RGF0ZXMUQmlsbGFibGUgVmlzaXQgRGF0ZXMHAAABDFRvdGFsQ2hhcmdlcw1Ub3RhbCBDaGFyZ2VzBQAAARBsYXN0X3VwZGF0ZV9kYXRlEGxhc3RfdXBkYXRlX2RhdGUIAAABE2xhc3RfdXBkYXRlX3VzZXJfaWQTbGFzdF91cGRhdGVfdXNlcl9pZAcAAAEDAAAACGNsYWltX25vDGY2X2Zyb21fZGF0ZQ9mNF90eXBlX29mX2JpbGwAAAAABwAHAAcABwAHAAb//wMGcCIEBnsBAwYtIAcCCE1lZGljYWlkBwIPUEFZTkUsIExMWU5BIEYuBwIVMDgvMTAvMjAyMi0xMC8wOC8yMDIyBwIXMDgvMzAvMjAyMiAtIDA5LzAzLzIwMjIFA+BwcgAAAAAAAAAAAAAABAAIAgTgZUrKQqfaCAcCCW1lLm9mZmljZQcABwAG//8DBqIiBAaQAQMGQSAHAghNZWRpY2FpZAcCD1NUWUxFUywgRlVSSU9VUwcCFTA3LzAxLzIwMjItMDgvMjkvMjAyMgcCFzA3LzAzLzIwMjIgLSAwNy8wNy8yMDIyBQMgqkQAAAAAAAAAAAAAAAQACAIEwJNrixjI2ggHAglkc2xmYWNlbG8CC0Zvcm1hdFN0YXRlBwACBVN0YXRlB24HCgcAAgEHAQIABwECAAcBAgEHAgIBBwMCAQcEAgEHBQIBBwYCAQcHAgEHAAcABwAHAAIABQAAAIAJAgdiaWxsX2lkBwECB2JpbGxfaWQDCQIAAgADBwQCAAcAAgEHAgcAAgEJAgEHAAcABwAHAAIIUGFnZVNpemUDBxQ=&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;keys&quot; , &quot;'&quot; , &quot;:[&quot; , &quot;'&quot; , &quot;8816&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;8866&quot; , &quot;'&quot; , &quot;]});
dxo.callBacksEnabled=true;
dxo.pageRowCount=2;
dxo.pageRowSize=20;
dxo.pageIndex=0;
dxo.pageCount=1;
dxo.selectedWithoutPageRowCount=0;
dxo.visibleStartIndex=0;
dxo.focusedRowIndex=-1;
dxo.allowFocusedRow=false;
dxo.checkBoxImageProperties = {&quot; , &quot;'&quot; , &quot;4&quot; , &quot;'&quot; , &quot;:[&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxChecked_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxUnchecked_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxGrayed_Moderno1&quot; , &quot;'&quot; , &quot;],&quot; , &quot;'&quot; , &quot;8&quot; , &quot;'&quot; , &quot;:[&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxCheckedDisabled_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxUncheckedDisabled_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxGrayedDisabled_Moderno1&quot; , &quot;'&quot; , &quot;]};
dxo.icbFocusedStyle = [&quot; , &quot;'&quot; , &quot;dxICBFocused_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;];
dxo.allowSelectByItemClick=false;
dxo.allowSelectSingleRowOnly=false;
dxo.callbackOnFocusedRowChanged=false;
dxo.callbackOnSelectionChanged=false;
dxo.columns = [new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,-1,null,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,0,0,1,0,0,0,1),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,1,-1,&quot; , &quot;'&quot; , &quot;last_update_date&quot; , &quot;'&quot; , &quot;,0,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,1,1,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,2,-1,&quot; , &quot;'&quot; , &quot;last_update_user_id&quot; , &quot;'&quot; , &quot;,0,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,1,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,3,-1,&quot; , &quot;'&quot; , &quot;bill_id&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,4,-1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,0,0,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,5,-1,&quot; , &quot;'&quot; , &quot;PayorName&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,6,-1,&quot; , &quot;'&quot; , &quot;EpisodeDates&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,7,-1,&quot; , &quot;'&quot; , &quot;BillableVisitDates&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,8,-1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,0,0,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,9,-1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,0,0,1,0,0,0,0)];
dxo.editState=0;
dxo.editItemVisibleIndex=-1;
dxo.searchPanelFilter=&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
dxo.editMode=2;
dxo.indentColumnCount=0;
dxo.allowMultiColumnAutoFilter=false;
dxo.SetAdaptiveMode(({&quot; , &quot;'&quot; , &quot;hideDataCellsWindowInnerWidth&quot; , &quot;'&quot; , &quot;:0,&quot; , &quot;'&quot; , &quot;allowTextTruncationInAdaptiveMode&quot; , &quot;'&quot; , &quot;:[false,true,true,true,false,false,false,false],&quot; , &quot;'&quot; , &quot;allowOnlyOneAdaptiveDetailExpanded&quot; , &quot;'&quot; , &quot;:true,&quot; , &quot;'&quot; , &quot;adaptivityMode&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;adaptiveDetailColumnCount&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;adaptiveColumnsOrder&quot; , &quot;'&quot; , &quot;:[9,8,7,5,6,4,3,0]}));
dxo.selectAllBtnStateWithoutPage=null;
dxo.selectAllSettings=[{&quot; , &quot;'&quot; , &quot;index&quot; , &quot;'&quot; , &quot;:0,&quot; , &quot;'&quot; , &quot;mode&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;selectText&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;Select all rows on this page&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;unselectText&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;Deselect all rows on this page&quot; , &quot;'&quot; , &quot;}];
dxo.AfterCreate();

//-->

&lt;!--
ASPxClientGridBase.InitializeStyles(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim&quot; , &quot;'&quot; , &quot;,({&quot; , &quot;'&quot; , &quot;sel&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvSelectedRow_Moderno1&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;fi&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvFocusedRow_Moderno1&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;bec&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvBatchEditCell_Moderno1 dxgv&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;bemc&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvBatchEditModifiedCell_Moderno1 dxgv&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;bemergmc&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvBatchEditCell_Moderno1 dxgvBatchEditModifiedCell_Moderno1 dxgv&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;ei&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&lt;tr class=&quot;dxgvEditingErrorRow_Moderno1&quot;>\r\n\t&lt;td class=&quot;dxgv&quot; data-colSpan=&quot;8&quot; style=&quot;border-right-width:0px;&quot;>&lt;/td>&lt;td class=&quot;dxgvAIC dxgv&quot; style=&quot;border-left-width:0px;border-right-width:0px;&quot;>&amp;nbsp;&lt;/td>\r\n&lt;/tr>&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;fgi&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvFocusedGroupRow_Moderno1&quot; , &quot;'&quot; , &quot;}}),[&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn3&quot; , &quot;'&quot; , &quot;])
//-->

                        
                    
                &quot;) or . = concat(&quot;
                    
                        
                            
                                
                                    Patient Last Name:
                                
                                
                                    
		
			
		
	
&lt;!--
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientTextBox(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_txtPatientLastName&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_txtPatientLastName&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$txtPatientLastName&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;N&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeNullText&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.nullText = &quot; , &quot;'&quot; , &quot;Enter Patient Last Name&quot; , &quot;'&quot; , &quot;;
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                                    From:
                                
                                
                                    
&lt;!--
(function(){var a = ({&quot; , &quot;'&quot; , &quot;numNegInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;-∞&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;percentPattern&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;numPosInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;∞&quot; , &quot;'&quot; , &quot;});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2021
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											4431123456
										
											4578910111213
										
											4614151617181920
										
											4721222324252627
										
											482829301234
										
											49567891011
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2021,10,16);
dxo.selection.AddArray([new Date(2021,10,16,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1637020800000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2021,10,16);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;M/d/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                
                            
                        
                        
                            
                                
                                    To:
                                
                                
                                    
		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2022
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											44303112345
										
											456789101112
										
											4613141516171819
										
											4720212223242526
										
											4827282930123
										
											4945678910
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2022,10,23);
dxo.selection.AddArray([new Date(2022,10,23,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteToDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteToDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteToDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1669161600000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2022,10,23);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;M/d/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                                    
                                
                                
                                    
		
			
				
			Search
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnSearch&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$btnSearch&quot; , &quot;'&quot; , &quot;;
dxo.Init.AddHandler(function (s, e) { AddGlyphiconToDevexpressButton(s, &quot; , &quot;'&quot; , &quot;glyphicon-search&quot; , &quot;'&quot; , &quot;) ;} );
dxo.Click.AddHandler(SearchClaims);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnSearch&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
                                
                                    
		
			
				
			Batch Claims
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnBatchPopup&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$btnBatchPopup&quot; , &quot;'&quot; , &quot;;
dxo.Init.AddHandler(function (s, e) { AddGlyphiconToDevexpressButton(s, &quot; , &quot;'&quot; , &quot;glyphicon-send&quot; , &quot;'&quot; , &quot;) ;});
dxo.Click.AddHandler(BatchClaimPopup);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                    
		
			
				
			Undraft Claims
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnPrinterFriendly&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$btnUndraft&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(UndraftClaim);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_btnUndraft&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                
                            
                        
                    
                    
                        
                            
		
			
				
					
						
							
						
					
						
							Claim No. 
						
					
						
							Patient Name 
						
					
						
							Payor 
						
					
						
							Episode Dates 
						
					
						
							Billable Visable Dates 
						
					
						
							Total Charges 
						
					
						
							History 
						
					
				
					 
				
					8816
                                            PAYNE, LLYNA F.
                                            
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_4_btnVisitCompliance_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_4_btnVisitCompliance_0&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnVisitCompliance&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_4$TC$btnVisitCompliance&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { bootbox.alert(&quot;Under construction...&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_4$TC$btnVisitCompliance&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        
                                            Medicaid
                                            
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_5_btnUB04AdjustmentPanel_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_5_btnUB04AdjustmentPanel_0&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnUB04AdjustmentPanel&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_5$TC$btnUB04AdjustmentPanel&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ShowUB04AdjustmentPanelModal(&quot;8816&quot;,&quot;8237&quot;,&quot;bbstep2&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_5$TC$btnUB04AdjustmentPanel&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        08/10/2022-10/08/202208/30/2022 - 09/03/2022
                                            $750.00
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientHyperLink(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_8_hyplnkShowUB04Modal_0&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_8$TC$hyplnkShowUB04Modal&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ShowUB04Modal(&quot;8816&quot;, &quot;private&quot;); });
dxo.AfterCreate();

//-->

                                        
                                        
						
							
								
							View Claim History
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnHistory&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell0_9$TC$btnHistory&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ViewClaimHistory(&quot;PAYNE, LLYNA F.&quot;,&quot;8237&quot;); });
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell0_9_btnHistory_0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                    
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn0&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled_Moderno1&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

//-->

&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled_Moderno1&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

//-->

				
					8866
                                            STYLES, FURIOUS
                                            
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_4_btnVisitCompliance_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_4_btnVisitCompliance_1&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnVisitCompliance&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_4$TC$btnVisitCompliance&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { bootbox.alert(&quot;Under construction...&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_4$TC$btnVisitCompliance&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        
                                            Medicaid
                                            
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_5_btnUB04AdjustmentPanel_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_5_btnUB04AdjustmentPanel_1&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnUB04AdjustmentPanel&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_5$TC$btnUB04AdjustmentPanel&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ShowUB04AdjustmentPanelModal(&quot;8866&quot;,&quot;8257&quot;,&quot;bbstep2&quot;); } );
dxo.isTextEmpty = true;
dxo.autoPostBackFunction = function(postBackArg) { WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_5$TC$btnUB04AdjustmentPanel&quot;,  postBackArg, true, &quot;&quot;, &quot;&quot;, false, true)); };
dxo.AfterCreate();

//-->

                                        07/01/2022-08/29/202207/03/2022 - 07/07/2022
                                            $450.00
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientHyperLink(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_8_hyplnkShowUB04Modal_1&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_8$TC$hyplnkShowUB04Modal&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ShowUB04Modal(&quot;8866&quot;, &quot;private&quot;); });
dxo.AfterCreate();

//-->

                                        
                                        
						
							
								
							View Claim History
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientButton(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;btnHistory&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$cell1_9$TC$btnHistory&quot; , &quot;'&quot; , &quot;;
dxo.Click.AddHandler(function(s, e) { ViewClaimHistory(&quot;STYLES, FURIOUS&quot;,&quot;8257&quot;); });
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_cell1_9_btnHistory_1&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbf&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;CD&quot; , &quot;'&quot; , &quot;]]]);
dxo.AfterCreate();

//-->

                                    
&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn2&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled_Moderno1&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

//-->

&lt;!--
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn3&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxbDisabled_Moderno1&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;TC&quot; , &quot;'&quot; , &quot;]]]);

//-->

				
			
				
					Page 1 of 1 (2 items)1 Page size:
				
					
						
							
								
									10
								
									20
								
									50
								
							
						
					
				
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DXME_&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxm-hovered&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DXI0_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI1_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI2_&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;T&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxm-selected&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DXI0_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI1_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI2_&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;T&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxm-disabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DXI0_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI1_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DXI2_&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;T&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupMenu(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$DXPagerBottom$PSP&quot; , &quot;'&quot; , &quot;;
dxo.ItemClick.AddHandler(function(s,e) { ASPx.POnPageSizePopupItemClick(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;, e.item); });
dxo.renderData={&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;:[[0],[1],[2]]};
dxo.subMenuFIXOffset=1;
dxo.subMenuFIYOffset=-2;
dxo.subMenuLIXOffset=1;
dxo.subMenuLIYOffset=-2;
dxo.subMenuXOffset=1;
dxo.subMenuYOffset=-2;
dxo.rootSubMenuFIXOffset=1;
dxo.rootSubMenuFIYOffset=-2;
dxo.rootSubMenuLIXOffset=1;
dxo.rootSubMenuLIYOffset=-2;
dxo.rootSubMenuXOffset=1;
dxo.rootSubMenuYOffset=-2;
dxo.allowSelectItem=true;
dxo.selectedItemIndexPath=&quot; , &quot;'&quot; , &quot;1&quot; , &quot;'&quot; , &quot;;
dxo.CreateItems([{&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;10&quot; , &quot;'&quot; , &quot;},{&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;20&quot; , &quot;'&quot; , &quot;},{&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;50&quot; , &quot;'&quot; , &quot;}]);
dxo.popupElementIDList=[&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom_PSB&quot; , &quot;'&quot; , &quot;];
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxp-hoverComboBox&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PSB&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxp-hoverDropDownButton&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DDB&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxp-pressedComboBox&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PSB&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxp-pressedDropDownButton&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;DDB&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPager(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXPagerBottom&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim$DXPagerBottom&quot; , &quot;'&quot; , &quot;;
dxo.hasOwnerControl=true;
dxo.requireInlineLayout=true;
dxo.enableAdaptivity=true;
dxo.pageSizeItems = [{&quot; , &quot;'&quot; , &quot;text&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;10&quot; , &quot;'&quot; , &quot;},{&quot; , &quot;'&quot; , &quot;text&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;20&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;20&quot; , &quot;'&quot; , &quot;},{&quot; , &quot;'&quot; , &quot;text&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;50&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;50&quot; , &quot;'&quot; , &quot;}];
dxo.pageSizeSelectedItem = ({&quot; , &quot;'&quot; , &quot;text&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;20&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;:20});
dxo.pageSizeChangedHandler.AddHandler(function(s, e) { ASPx.GVPagerOnClick(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim&quot; , &quot;'&quot; , &quot;,e.value); });
dxo.AfterCreate();

//-->

			
				
					Loading…
				
			

			
		
	
&lt;!--

var dxo = new ASPxClientGridView(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;grvBatchClaim&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$grvBatchClaim&quot; , &quot;'&quot; , &quot;;
dxo.EndCallback.AddHandler(grvBatchClaim_EndCallback);
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;selection&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;callbackState&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;BwQHAwIERGF0YQahAgAAAAACAAAAAgAAAAAAAAACAAAAAAoAAAAHYmlsbF9pZAdiaWxsX2lkAwAAAQxtMDAyMF9wYXRfaWQMbTAwMjBfcGF0X2lkBAAAAQplcGlzb2RlX2lkCmVwaXNvZGVfaWQDAAABCVBheW9yTmFtZQpQYXlvciBOYW1lBwAAAQtQYXRpZW50TmFtZQxQYXRpZW50IE5hbWUHAAABDEVwaXNvZGVEYXRlcw1FcGlzb2RlIERhdGVzBwAAARJCaWxsYWJsZVZpc2l0RGF0ZXMUQmlsbGFibGUgVmlzaXQgRGF0ZXMHAAABDFRvdGFsQ2hhcmdlcw1Ub3RhbCBDaGFyZ2VzBQAAARBsYXN0X3VwZGF0ZV9kYXRlEGxhc3RfdXBkYXRlX2RhdGUIAAABE2xhc3RfdXBkYXRlX3VzZXJfaWQTbGFzdF91cGRhdGVfdXNlcl9pZAcAAAEDAAAACGNsYWltX25vDGY2X2Zyb21fZGF0ZQ9mNF90eXBlX29mX2JpbGwAAAAABwAHAAcABwAHAAb//wMGcCIEBnsBAwYtIAcCCE1lZGljYWlkBwIPUEFZTkUsIExMWU5BIEYuBwIVMDgvMTAvMjAyMi0xMC8wOC8yMDIyBwIXMDgvMzAvMjAyMiAtIDA5LzAzLzIwMjIFA+BwcgAAAAAAAAAAAAAABAAIAgTgZUrKQqfaCAcCCW1lLm9mZmljZQcABwAG//8DBqIiBAaQAQMGQSAHAghNZWRpY2FpZAcCD1NUWUxFUywgRlVSSU9VUwcCFTA3LzAxLzIwMjItMDgvMjkvMjAyMgcCFzA3LzAzLzIwMjIgLSAwNy8wNy8yMDIyBQMgqkQAAAAAAAAAAAAAAAQACAIEwJNrixjI2ggHAglkc2xmYWNlbG8CC0Zvcm1hdFN0YXRlBwACBVN0YXRlB24HCgcAAgEHAQIABwECAAcBAgEHAgIBBwMCAQcEAgEHBQIBBwYCAQcHAgEHAAcABwAHAAIABQAAAIAJAgdiaWxsX2lkBwECB2JpbGxfaWQDCQIAAgADBwQCAAcAAgEHAgcAAgEJAgEHAAcABwAHAAIIUGFnZVNpemUDBxQ=&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;keys&quot; , &quot;'&quot; , &quot;:[&quot; , &quot;'&quot; , &quot;8816&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;8866&quot; , &quot;'&quot; , &quot;]});
dxo.callBacksEnabled=true;
dxo.pageRowCount=2;
dxo.pageRowSize=20;
dxo.pageIndex=0;
dxo.pageCount=1;
dxo.selectedWithoutPageRowCount=0;
dxo.visibleStartIndex=0;
dxo.focusedRowIndex=-1;
dxo.allowFocusedRow=false;
dxo.checkBoxImageProperties = {&quot; , &quot;'&quot; , &quot;4&quot; , &quot;'&quot; , &quot;:[&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxChecked_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxUnchecked_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxGrayed_Moderno1&quot; , &quot;'&quot; , &quot;],&quot; , &quot;'&quot; , &quot;8&quot; , &quot;'&quot; , &quot;:[&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxCheckedDisabled_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxUncheckedDisabled_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxWeb_edtCheckBoxGrayedDisabled_Moderno1&quot; , &quot;'&quot; , &quot;]};
dxo.icbFocusedStyle = [&quot; , &quot;'&quot; , &quot;dxICBFocused_Moderno1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;];
dxo.allowSelectByItemClick=false;
dxo.allowSelectSingleRowOnly=false;
dxo.callbackOnFocusedRowChanged=false;
dxo.callbackOnSelectionChanged=false;
dxo.columns = [new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,-1,null,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,0,0,1,0,0,0,1),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,1,-1,&quot; , &quot;'&quot; , &quot;last_update_date&quot; , &quot;'&quot; , &quot;,0,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,1,1,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,2,-1,&quot; , &quot;'&quot; , &quot;last_update_user_id&quot; , &quot;'&quot; , &quot;,0,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,1,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,3,-1,&quot; , &quot;'&quot; , &quot;bill_id&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,4,-1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,0,0,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,5,-1,&quot; , &quot;'&quot; , &quot;PayorName&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,6,-1,&quot; , &quot;'&quot; , &quot;EpisodeDates&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,7,-1,&quot; , &quot;'&quot; , &quot;BillableVisitDates&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,1,1,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,8,-1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,0,0,1,0,0,0,0),
new ASPxClientGridViewColumn(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,9,-1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,1,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,0,0,0,1,0,0,0,0)];
dxo.editState=0;
dxo.editItemVisibleIndex=-1;
dxo.searchPanelFilter=&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
dxo.editMode=2;
dxo.indentColumnCount=0;
dxo.allowMultiColumnAutoFilter=false;
dxo.SetAdaptiveMode(({&quot; , &quot;'&quot; , &quot;hideDataCellsWindowInnerWidth&quot; , &quot;'&quot; , &quot;:0,&quot; , &quot;'&quot; , &quot;allowTextTruncationInAdaptiveMode&quot; , &quot;'&quot; , &quot;:[false,true,true,true,false,false,false,false],&quot; , &quot;'&quot; , &quot;allowOnlyOneAdaptiveDetailExpanded&quot; , &quot;'&quot; , &quot;:true,&quot; , &quot;'&quot; , &quot;adaptivityMode&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;adaptiveDetailColumnCount&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;adaptiveColumnsOrder&quot; , &quot;'&quot; , &quot;:[9,8,7,5,6,4,3,0]}));
dxo.selectAllBtnStateWithoutPage=null;
dxo.selectAllSettings=[{&quot; , &quot;'&quot; , &quot;index&quot; , &quot;'&quot; , &quot;:0,&quot; , &quot;'&quot; , &quot;mode&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;selectText&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;Select all rows on this page&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;unselectText&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;Deselect all rows on this page&quot; , &quot;'&quot; , &quot;}];
dxo.AfterCreate();

//-->

&lt;!--
ASPxClientGridBase.InitializeStyles(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim&quot; , &quot;'&quot; , &quot;,({&quot; , &quot;'&quot; , &quot;sel&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvSelectedRow_Moderno1&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;fi&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvFocusedRow_Moderno1&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;bec&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvBatchEditCell_Moderno1 dxgv&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;bemc&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvBatchEditModifiedCell_Moderno1 dxgv&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;bemergmc&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvBatchEditCell_Moderno1 dxgvBatchEditModifiedCell_Moderno1 dxgv&quot; , &quot;'&quot; , &quot;},&quot; , &quot;'&quot; , &quot;ei&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&lt;tr class=&quot;dxgvEditingErrorRow_Moderno1&quot;>\r\n\t&lt;td class=&quot;dxgv&quot; data-colSpan=&quot;8&quot; style=&quot;border-right-width:0px;&quot;>&lt;/td>&lt;td class=&quot;dxgvAIC dxgv&quot; style=&quot;border-left-width:0px;border-right-width:0px;&quot;>&amp;nbsp;&lt;/td>\r\n&lt;/tr>&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;fgi&quot; , &quot;'&quot; , &quot;:{&quot; , &quot;'&quot; , &quot;css&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxgvFocusedGroupRow_Moderno1&quot; , &quot;'&quot; , &quot;}}),[&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaim_DXCBtn3&quot; , &quot;'&quot; , &quot;])
//-->

                        
                    
                &quot;))]</value>
      <webElementGuid>1d3c0afd-d0cf-452e-a308-84d2567d84ca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
