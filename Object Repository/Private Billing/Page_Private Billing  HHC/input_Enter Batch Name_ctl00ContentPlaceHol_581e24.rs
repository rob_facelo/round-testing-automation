<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Enter Batch Name_ctl00ContentPlaceHol_581e24</name>
   <tag></tag>
   <elementGuidId>fd63b1e4-7882-458e-ae4f-63289be09768</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder_BatchClaimsControl_txtBatchName_I']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchClaimsControl_txtBatchName_I</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>41d701cf-4f36-4d21-8db9-90cbc3cee728</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeEditArea dxeEditAreaSys dxh0</value>
      <webElementGuid>de842a86-d219-4e06-8184-4378ba97f6e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_BatchClaimsControl_txtBatchName_I</value>
      <webElementGuid>4083e7e3-a383-4e32-be9a-6214b926bd4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolder$BatchClaimsControl$txtBatchName</value>
      <webElementGuid>c0bf94fd-9d71-4852-bc9a-e4fce1574663</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onfocus</name>
      <type>Main</type>
      <value>ASPx.EGotFocus('ContentPlaceHolder_BatchClaimsControl_txtBatchName')</value>
      <webElementGuid>646b08cd-ee1c-4d37-9f32-33bef35ae8ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>ASPx.ELostFocus('ContentPlaceHolder_BatchClaimsControl_txtBatchName')</value>
      <webElementGuid>97b46fbd-ce65-43b5-8d36-0b396b10de0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d001bc79-2ed9-4969-8f95-c2fbe84e7b8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchClaimsControl_txtBatchName_I&quot;)</value>
      <webElementGuid>66e89fcc-46b9-406d-bbbd-e5ad560f2ce4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder_BatchClaimsControl_txtBatchName_I']</value>
      <webElementGuid>fc56a795-2939-4043-9785-95e052b1b71e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_BatchClaimsControl_txtBatchName']/tbody/tr/td/input</value>
      <webElementGuid>d4557b26-5c13-43ee-9b48-d0f40c4751d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]/div/div[2]/table/tbody/tr/td/input</value>
      <webElementGuid>dcb692be-ae5a-4b3e-b578-13a46080e131</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder_BatchClaimsControl_txtBatchName_I' and @name = 'ctl00$ContentPlaceHolder$BatchClaimsControl$txtBatchName' and @type = 'text']</value>
      <webElementGuid>1ecf0c9d-0244-4489-b15e-ed9bb9e276e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
