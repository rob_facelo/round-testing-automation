<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Recertification Assessment</name>
   <tag></tag>
   <elementGuidId>3650f3e2-3861-42d3-bced-6838fb47c8a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_grvOrderList_cell0_2_hlnkPatientName_0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_grvOrderList_cell0_2_hlnkPatientName_0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d93df96c-0470-4231-959a-fe8847148e33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeHyperlink</value>
      <webElementGuid>c98b8560-be94-4b91-a3c4-9ea108a62ccb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return ASPx.SEClick('ContentPlaceHolder_grvOrderList_cell0_2_hlnkPatientName_0', event)</value>
      <webElementGuid>05fb7c2d-6fe0-4d1b-a92a-5afdce95a10b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_grvOrderList_cell0_2_hlnkPatientName_0</value>
      <webElementGuid>2f2abe29-b683-442e-a3b4-b3d6e0d56963</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Recertification Assessment</value>
      <webElementGuid>58494fe4-4e4f-4073-97a0-b51d8beb2577</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_grvOrderList_cell0_2_hlnkPatientName_0&quot;)</value>
      <webElementGuid>c7ab063f-3d9f-459f-9344-9b0cdcf903bd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_grvOrderList_cell0_2_hlnkPatientName_0']</value>
      <webElementGuid>ff03683f-8773-412d-b1c1-435c040d851b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_grvOrderList_tccell0_2']/a</value>
      <webElementGuid>b4531c0c-aeb5-447b-9f95-0661f4f2210c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Recertification Assessment')]</value>
      <webElementGuid>5f36a571-99ec-49c2-8835-09829af4e129</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Updated By'])[1]/following::a[1]</value>
      <webElementGuid>351d37ff-8633-4122-8053-9e58284f1665</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[2]/following::a[1]</value>
      <webElementGuid>b848ae22-e04c-4e04-b5ef-3855f7c0a2cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aug 26 2022 11:31AM'])[1]/preceding::a[1]</value>
      <webElementGuid>47f07797-aaca-44da-8a9b-e505169f3c34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Strange, Stephen ,'])[1]/preceding::a[1]</value>
      <webElementGuid>60cbd85d-495b-47ca-90f8-118689323fda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Recertification Assessment']/parent::*</value>
      <webElementGuid>326db89b-94eb-441d-b13f-91ef3ad5b40c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/a</value>
      <webElementGuid>360de3b1-3bcb-4d1d-9892-c0af53409c13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'ContentPlaceHolder_grvOrderList_cell0_2_hlnkPatientName_0' and (text() = 'Recertification Assessment' or . = 'Recertification Assessment')]</value>
      <webElementGuid>17ec86cd-18d5-4e03-94f6-49959bfe2598</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
