<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save_1_2</name>
   <tag></tag>
   <elementGuidId>f12efcbc-8463-4274-b2fd-554c672a8aa8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.well > div > div.modal-header > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > button.btn.btn-default.font12</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[233]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>eceac092-7b08-4fc4-a9eb-68f04ef1e6f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>aa5963d0-4226-49ce-90e5-8ac75b75de10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default font12</value>
      <webElementGuid>4a5fb158-d647-43be-ae17-b270b1da0e41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>piCtrl.Step3()</value>
      <webElementGuid>578bf375-b039-4e50-a7bf-58fd5c6d03f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>(intakewinfo.StartOfCareDate.$error.pattern || intakewinfo.DischargeDate.$error.pattern || intakewinfo.TrackingSentDate.$error.pattern || piCtrl.MedicareHasError || piCtrl.secondaryMedicareHasError )</value>
      <webElementGuid>212843ad-77ee-4bae-9787-d2fee28ccfb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                 Save
                                            </value>
      <webElementGuid>dcdcc3c2-dd76-4df3-9855-7afd2b658d75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[1]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12&quot;]</value>
      <webElementGuid>b82aefbb-8e3a-4edf-ad22-f7c04785e8d2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[233]</value>
      <webElementGuid>8bd7917c-0d3b-467e-b69f-9ccae81005e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[4]/div/div/div/div/div/button</value>
      <webElementGuid>8be1ac94-ad14-45a5-b3cf-a7f57571e0fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Referral Intake'])[3]/following::button[1]</value>
      <webElementGuid>3cdc08b0-b87d-4ec6-a285-7379c0d834fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Referral Intake'])[2]/following::button[1]</value>
      <webElementGuid>e05007fc-8142-4a1f-897e-84377b2a1f66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[4]/div/div/div/div/div/button</value>
      <webElementGuid>f83f830c-fe47-4df5-95ef-27be18f70216</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                                                 Save
                                            ' or . = '
                                                 Save
                                            ')]</value>
      <webElementGuid>1070c1ca-2638-4d26-b9fd-53e94b0990c5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
