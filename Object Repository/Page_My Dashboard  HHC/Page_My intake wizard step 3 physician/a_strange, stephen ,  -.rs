<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_strange, stephen ,  -</name>
   <tag></tag>
   <elementGuidId>7e489f28-11b4-4a95-a212-17914e02a636</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='intakeprimaryphysician']/autocomplete-directive/div/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#intakeprimaryphysician > autocomplete-directive.ng-isolate-scope > div.dropdown > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5d04999e-1cf8-43d2-9ad8-ec6618d4a028</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                        strange, stephen ,  - 
                    
                
                
                    
                        rodeo drive  BEVERLY HILLS CA 90210
                    
                    
                
            </value>
      <webElementGuid>ba27aa4e-f774-4bcb-a230-e0a89220853a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeprimaryphysician&quot;)/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]</value>
      <webElementGuid>4bc8867b-d034-48a3-b832-51b4a15f6042</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='intakeprimaryphysician']/autocomplete-directive/div/ul/li/a</value>
      <webElementGuid>51a31c0a-2a6a-4b0b-be5b-01219cc0c14b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Primary:'])[1]/following::a[1]</value>
      <webElementGuid>f4028983-4143-44dc-9088-3ae979679c24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physician'])[3]/following::a[1]</value>
      <webElementGuid>f1f36551-85ee-45fa-b095-e6527386a37f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/div[2]/div/div/div/div/autocomplete-directive/div/ul/li/a</value>
      <webElementGuid>5ae42fe8-9cd1-4779-830e-2efbe370f9bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                
                    
                        strange, stephen ,  - 
                    
                
                
                    
                        rodeo drive  BEVERLY HILLS CA 90210
                    
                    
                
            ' or . = '
                
                    
                        strange, stephen ,  - 
                    
                
                
                    
                        rodeo drive  BEVERLY HILLS CA 90210
                    
                    
                
            ')]</value>
      <webElementGuid>57baa3c0-6692-4558-a0dd-646b0b276c59</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
