<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_strange, stephen ,  -</name>
   <tag></tag>
   <elementGuidId>d29bda83-831d-4711-8b5b-b80973c72381</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//autocomplete-directive[@id='intakeWizardPhys']/div/ul/li/a/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
                        strange, stephen ,  - 
                    ' or . = '
                        strange, stephen ,  - 
                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7dc01cca-7442-41d3-ba07-2373cfd8e45b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>89b05fca-0fe8-403f-9f65-ddfb6742cdb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        strange, stephen ,  - 
                    </value>
      <webElementGuid>fe5b6cfe-2714-46a5-8ee8-9a723d2f6849</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeWizardPhys&quot;)/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>df742476-58d4-4b72-a187-f3416bf146f3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//autocomplete-directive[@id='intakeWizardPhys']/div/ul/li/a/div/div</value>
      <webElementGuid>07573f2e-8dfc-4b97-a968-1fd80206fb6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[14]/following::div[4]</value>
      <webElementGuid>96a306f9-4caa-4ff0-8a44-83e9cade9984</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Start of Care - Referred for Admission Order'])[1]/following::div[8]</value>
      <webElementGuid>36f93297-2288-4813-92bd-2e088d55a34d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[43]/preceding::div[3]</value>
      <webElementGuid>26657655-6c0f-4200-bed7-7142fa318f1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a/div/div</value>
      <webElementGuid>a71fd701-8a8a-442b-bacb-e59b709cd51d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        strange, stephen ,  - 
                    ' or . = '
                        strange, stephen ,  - 
                    ')]</value>
      <webElementGuid>19bd76e3-1104-4a84-a0e5-da15328849db</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
