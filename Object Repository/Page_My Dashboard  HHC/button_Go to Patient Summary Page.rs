<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Go to Patient Summary Page</name>
   <tag></tag>
   <elementGuidId>df79a80d-b14a-45bb-8c45-2053a3b0b95e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[name=&quot;intakwsummarypage&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@name='intakwsummarypage']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>29174bb2-d268-4305-bf8e-f2cfaba125c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-12 font-weight-600</value>
      <webElementGuid>41012554-7fed-41bc-aa73-bc9b895c732c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>intakwsummarypage</value>
      <webElementGuid>f988053b-58d8-46df-8e26-db7e1c35dca5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>piCtrl.gotoSummaryPage()</value>
      <webElementGuid>28ad651b-01ee-4701-a759-63e27feef8ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Go to Patient Summary Page</value>
      <webElementGuid>df7ac07d-f288-456b-9648-6056354822c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/button[@class=&quot;col-lg-12 font-weight-600&quot;]</value>
      <webElementGuid>459b650f-11d0-4b04-b7d4-81fbdd4fa9b1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@name='intakwsummarypage']</value>
      <webElementGuid>3f45f88e-c806-464e-b416-1f088d1ae4e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[5]/div/div/button</value>
      <webElementGuid>e419e938-170e-478a-b920-fe8c499008fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Address:'])[5]/following::button[1]</value>
      <webElementGuid>455e87a4-8f33-4066-bda4-b0986715cad2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax:'])[2]/following::button[1]</value>
      <webElementGuid>cda6dc91-078e-44d5-bf63-d24555242bbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Admission Order'])[1]/preceding::button[1]</value>
      <webElementGuid>555a4dcd-b296-427d-ae20-e71fd4417591</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go to SOC OASIS Page'])[1]/preceding::button[2]</value>
      <webElementGuid>a1b02f01-ed75-4f80-91dd-b0706c9fa2c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Go to Patient Summary Page']/parent::*</value>
      <webElementGuid>dba37b80-c39f-4db0-9f19-9d741debc622</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[5]/div/div/button</value>
      <webElementGuid>0c02e0b1-f277-44e7-89ef-49900a92645a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@name = 'intakwsummarypage' and (text() = 'Go to Patient Summary Page' or . = 'Go to Patient Summary Page')]</value>
      <webElementGuid>d5bd0bb9-c437-42d7-b679-5d4ab79a325d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
