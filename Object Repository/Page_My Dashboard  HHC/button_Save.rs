<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save</name>
   <tag></tag>
   <elementGuidId>6b4d838d-ed44-4638-bb1f-57bb3307498e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-default.font12.font-weight-600</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[226]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6cd7dab5-ed14-4cdf-8571-80eaaa67d18a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6684a523-ade9-4cfb-b9c9-53f4032d46d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default font12 font-weight-600</value>
      <webElementGuid>706116fa-0923-464c-8e8f-116e49a5f56e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>piCtrl.ssnVal || intakewinfo.bdate.$error.pattern || intakewinfo.lredate.$error.pattern || intakewinfo.bdate.$error.pattern || intakewinfo.email2.$error.pattern || intakewinfo.email1.$error.pattern || intakewinfo.lremail.$error.pattern</value>
      <webElementGuid>e8fdbdff-f633-4134-81ac-a820b2633c6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>piCtrl.Step1(true)</value>
      <webElementGuid>ffcd42d6-f322-4343-8438-fa5acc04bd3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                     Save
                                                </value>
      <webElementGuid>35ae7a4b-efb8-4ac5-950a-97359c129e4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12 font-weight-600&quot;]</value>
      <webElementGuid>88595c8e-e7ac-4103-b905-8671721d6af9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[226]</value>
      <webElementGuid>1b9f8703-dd9f-4457-9c20-b22f38d481be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div/div/button</value>
      <webElementGuid>ed207e31-f5a6-4c6a-bec4-e5c6705511c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 3'])[1]/following::button[1]</value>
      <webElementGuid>ab26a732-7cd3-467a-9eed-81379c502a2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 2'])[1]/following::button[1]</value>
      <webElementGuid>5dcc047f-4b52-4f67-bb1a-3507b03724cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]/div/div/div/div/div/div/button</value>
      <webElementGuid>222a9c62-28ef-4da5-a8a2-937303c9ed7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                                                     Save
                                                ' or . = '
                                                     Save
                                                ')]</value>
      <webElementGuid>a51c412a-710b-448f-a95c-95c95a3f9094</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
