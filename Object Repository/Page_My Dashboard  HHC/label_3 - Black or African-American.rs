<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_3 - Black or African-American</name>
   <tag></tag>
   <elementGuidId>e8d19213-34a3-420c-be56-f96812baf189</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Race_EthnicityPanelintakew']/div[2]/div/div[2]/div/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>18272d0c-1948-4be0-b86b-422cc5d9939a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox-inline</value>
      <webElementGuid>45ca3f2b-6e8c-4cc1-b72c-65e94f777ea8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                    3 - Black or African-American
                                                                                </value>
      <webElementGuid>cb340c25-e03f-41b6-bfc1-e7266ce219f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Race_EthnicityPanelintakew&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-3 col-lg-3&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/label[@class=&quot;checkbox-inline&quot;]</value>
      <webElementGuid>8282c421-0a5e-4cc0-bf14-d3b063cfed4e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Race_EthnicityPanelintakew']/div[2]/div/div[2]/div/div/label</value>
      <webElementGuid>94838248-1eff-4090-82a6-37c4e2c3c733</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mark all that apply'])[1]/following::label[3]</value>
      <webElementGuid>1c4cfd16-8a6b-4a25-8aa2-632dae755abc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='3 - Black or African-American']/parent::*</value>
      <webElementGuid>a30a9839-7b65-49a0-a812-9422fd60317b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div[2]/div/div[2]/div/div/label</value>
      <webElementGuid>830f2bca-cd1a-4f32-b1dc-271047213ce1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                                                                                    3 - Black or African-American
                                                                                ' or . = '
                                                                                    3 - Black or African-American
                                                                                ')]</value>
      <webElementGuid>f5d70258-8817-4f73-9cde-720d7598b66f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
