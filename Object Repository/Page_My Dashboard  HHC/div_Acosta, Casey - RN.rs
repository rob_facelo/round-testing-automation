<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Acosta, Casey - RN</name>
   <tag></tag>
   <elementGuidId>4ca24a9f-9d4b-4e2a-81e5-d282ccdebee6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>fieldset > autocomplete-directive.ng-isolate-scope > div.dropdown > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='selectcarestaffintakewizard']/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8d75e431-a445-45eb-a648-7a08fc89fc59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>19af3470-00d6-4a90-b10b-ba2223f6788a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Acosta, Casey - RN
                    </value>
      <webElementGuid>ec9d5269-0c55-4956-9df0-033530e26d39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectcarestaffintakewizard&quot;)/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>78503134-5f8c-4d72-b845-adb9f48964ca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='selectcarestaffintakewizard']/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>06152806-8d7b-46c6-a651-92ded2b4298f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/following::div[7]</value>
      <webElementGuid>1a9c1833-00c8-45ba-903e-72f5c22f8e96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[6]/following::div[7]</value>
      <webElementGuid>44ee777e-8e54-4488-91ef-d80706406864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[49]/preceding::div[3]</value>
      <webElementGuid>830a12cc-439f-4a71-9703-9d38a8019ba4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks:'])[2]/preceding::div[3]</value>
      <webElementGuid>888f36fd-9a03-425a-a275-c6f7b51d1e38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Acosta, Casey - RN']/parent::*</value>
      <webElementGuid>ead75b97-4e96-4ed7-a8e3-3ff3b1e2b698</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>20cb938f-8156-4871-b2d1-7fa164407671</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Acosta, Casey - RN
                    ' or . = '
                        Acosta, Casey - RN
                    ')]</value>
      <webElementGuid>d9b23f59-9987-4eae-8c5b-0a50461af8c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
