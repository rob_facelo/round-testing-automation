<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Select Caregiver--Admitting Care _132ebe</name>
   <tag></tag>
   <elementGuidId>e235385b-ddd2-427a-8820-987158d94d50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='cgtypecodeintake']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#cgtypecodeintake</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'cgtypecodeintake' and (text() = '-- Select Caregiver--Admitting Care StaffCare CoordinatorCase ManagerDieticianDischarge Care StaffHome Health AideMedical Social WorkerOccupational TherapyOffice StaffPhysical TherapyQuality Assurance (QA)Recert Care StaffRegistered DietitianRespiratory TherapyROC Care StaffSkilled NursingSpeech Therapy' or . = '-- Select Caregiver--Admitting Care StaffCare CoordinatorCase ManagerDieticianDischarge Care StaffHome Health AideMedical Social WorkerOccupational TherapyOffice StaffPhysical TherapyQuality Assurance (QA)Recert Care StaffRegistered DietitianRespiratory TherapyROC Care StaffSkilled NursingSpeech Therapy')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a22be8d2-0b16-4d57-8663-4e71e802a5eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control hhc-option-small ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>d036bc54-f00d-4c5d-96b9-4f584adffcc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>cgtypecodeintake</value>
      <webElementGuid>e3d5ad04-b1c3-4ef5-a11c-c53b6a87de61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>piCtrl.SelectedCarestaffType</value>
      <webElementGuid>4d3e7139-6560-4016-aaca-015f27e78e54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-options</name>
      <type>Main</type>
      <value>caregivertype.CaregiverTypeId as caregivertype.Description for caregivertype in piCtrl.caregiverTypeList</value>
      <webElementGuid>5acbe75d-0920-4226-9a68-7d6b71d04f23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>piCtrl.onChangeCgType()</value>
      <webElementGuid>6d1d38da-1528-451b-a07d-73e94e101966</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>-- Select Caregiver--Admitting Care StaffCare CoordinatorCase ManagerDieticianDischarge Care StaffHome Health AideMedical Social WorkerOccupational TherapyOffice StaffPhysical TherapyQuality Assurance (QA)Recert Care StaffRegistered DietitianRespiratory TherapyROC Care StaffSkilled NursingSpeech Therapy</value>
      <webElementGuid>77bad418-963a-4f53-9fdf-03b57fbcb59d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cgtypecodeintake&quot;)</value>
      <webElementGuid>960cce31-c83e-4df7-a203-45612b9cd035</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='cgtypecodeintake']</value>
      <webElementGuid>da13ba4b-a2ee-46ad-af7e-39a0b24973b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='carestaffinformationtabintake1']/div/div[2]/div/div/select</value>
      <webElementGuid>05eba95f-f4b2-4a57-beb9-c0f0a6f02ee5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/following::select[1]</value>
      <webElementGuid>d0da6517-74e6-4e3d-9e4e-2cd60ce28d4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[6]/following::select[1]</value>
      <webElementGuid>c55a5db7-0c90-467d-9ae6-989871fcd245</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[49]/preceding::select[1]</value>
      <webElementGuid>b9cba528-5285-4234-b21a-36b718d7a052</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks:'])[2]/preceding::select[1]</value>
      <webElementGuid>ef2dff45-ef8b-4802-a589-4ffe85b74a10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div[2]/div/div/select</value>
      <webElementGuid>3dd445e9-1ce4-4187-b665-6498d046680b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'cgtypecodeintake' and (text() = '-- Select Caregiver--Admitting Care StaffCare CoordinatorCase ManagerDieticianDischarge Care StaffHome Health AideMedical Social WorkerOccupational TherapyOffice StaffPhysical TherapyQuality Assurance (QA)Recert Care StaffRegistered DietitianRespiratory TherapyROC Care StaffSkilled NursingSpeech Therapy' or . = '-- Select Caregiver--Admitting Care StaffCare CoordinatorCase ManagerDieticianDischarge Care StaffHome Health AideMedical Social WorkerOccupational TherapyOffice StaffPhysical TherapyQuality Assurance (QA)Recert Care StaffRegistered DietitianRespiratory TherapyROC Care StaffSkilled NursingSpeech Therapy')]</value>
      <webElementGuid>5f511b91-bdd0-4650-9e58-cec7f223be15</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
