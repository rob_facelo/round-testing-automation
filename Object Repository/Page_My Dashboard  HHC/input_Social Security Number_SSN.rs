<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Social Security Number_SSN</name>
   <tag></tag>
   <elementGuidId>ead1f03e-704e-4cfc-9738-1339c5f11ddd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#patientIntakeSSN</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='patientIntakeSSN']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ad254747-e09c-457b-a547-18646517abdf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>4f7d83e8-6f2a-4181-ab94-f70dcf282a4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>SSN</value>
      <webElementGuid>c02dfe17-e85a-42c0-b2a9-3b5e7fe178c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>patientIntakeSSN</value>
      <webElementGuid>88736321-4fd6-4b7f-973d-3329cd6acd26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-blur</name>
      <type>Main</type>
      <value>piCtrl.CheckSSN()</value>
      <webElementGuid>91efaccd-0293-42d0-a9ef-fe377be69d8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>9</value>
      <webElementGuid>d92733d1-2e84-4f98-ad0e-f89dbda57983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>piCtrl.ssnValidation()</value>
      <webElementGuid>782dd3be-7cb9-4c90-95ce-557ce8cb5ba4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>piCtrl.ssnRequired()</value>
      <webElementGuid>9d7413dc-4f4f-4171-ac33-87506e75cb07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>piCtrl.PatientInfo.SSN</value>
      <webElementGuid>bf8782e8-e8b0-4cc9-813c-46e34790e4fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>db2fc9ad-fd21-4cca-81a5-99eb7fed63a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>basic-addon3</value>
      <webElementGuid>6e65ea16-6591-4d81-8ea7-07a1d64cb890</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;patientIntakeSSN&quot;)</value>
      <webElementGuid>b46d291a-f89e-473b-8ba2-cb22122d09d0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='patientIntakeSSN']</value>
      <webElementGuid>366b6716-2951-49bd-8de3-f397a204fbf2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div[6]/div/div/div[2]/div/input</value>
      <webElementGuid>942661d1-c861-4e24-8b86-e1cf275c293f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div[2]/div/input</value>
      <webElementGuid>b6782116-3331-465c-8ac4-6ef3dccd2bea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'SSN' and @id = 'patientIntakeSSN']</value>
      <webElementGuid>5b6cf32a-8b4a-4481-838e-1a4f43502439</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
