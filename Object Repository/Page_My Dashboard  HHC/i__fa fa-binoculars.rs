<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i__fa fa-binoculars</name>
   <tag></tag>
   <elementGuidId>8e232ea0-974d-49eb-8e19-c44c5d28f61a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-binoculars</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='intakeorderbtn']/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>b23f115a-848c-4fd1-a78d-dd4fb1d0319a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-binoculars</value>
      <webElementGuid>4c230995-96ed-4181-804d-f9e18210566c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeorderbtn&quot;)/i[@class=&quot;fa fa-binoculars&quot;]</value>
      <webElementGuid>3c1efe8b-0a1d-44ee-adae-53a6c4cdb3c7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='intakeorderbtn']/i</value>
      <webElementGuid>29703167-c965-4023-a6d4-c4497cca6d09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p/button/i</value>
      <webElementGuid>e343e063-3e20-48b3-a8ca-ac35f48d6858</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
