<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save</name>
   <tag></tag>
   <elementGuidId>b0c7a753-5ba1-4741-b029-bf4e6f63a8d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-9.col-lg-9 > div.btn-group > button.btn.btn-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[61]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>060fd706-1217-48ae-a497-c59419269b87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>231df8e1-c276-47e7-afc0-83dea35803ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
      <webElementGuid>c25f68c8-baf8-4e7e-a0ed-219115e1c380</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>ptoCtrl.submitForm()</value>
      <webElementGuid>0248877c-815a-465e-b356-bf26b927c027</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>ptoCtrl.pto_form.ptoTrackingSentDate.$error.pattern || ptoCtrl.pto_form.ptoTrackingReceivedDate.$error.pattern || ptoCtrl.pto_form.OrderDate.$error.pattern || ptoCtrl.pto_form.OrderDateNoValidation.$error.pattern || ptoCtrl.pto_form.OrderDateNoValidation.$error.pattern || ptoCtrl.pto_form.OrderDateNoValidation.$error.pattern || ptoCtrl.pto_form.OrderDate.$error.pattern || ptoCtrl.pto_form.LRPTOReceivedDate.$error.pattern || ptoCtrl.pto_form.LRPTOSentDate.$error.pattern || ptoCtrl.pto_form.LREffectiveDate.$error.pattern</value>
      <webElementGuid>9fd6e33b-dcb9-4001-91f2-2af99c1696b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Save</value>
      <webElementGuid>2ac5a965-9071-42ac-971a-1b5a1ffcbdbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-valid-mask ng-valid-maxlength ng-valid-pattern ng-valid-minlength ng-dirty ng-valid ng-valid-validation&quot;]/div[@class=&quot;form-actions&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-9 col-lg-9&quot;]/div[@class=&quot;btn-group&quot;]/button[@class=&quot;btn btn-default&quot;]</value>
      <webElementGuid>e865dd76-a3e5-461e-a136-3c5062b5ff85</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[61]</value>
      <webElementGuid>6a11131a-c5ec-4bca-b2be-fd5ac508cf09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div/div/div/div/button</value>
      <webElementGuid>4bcf7285-de52-4234-9744-56b38843fd34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physician Order For Signature'])[1]/following::button[1]</value>
      <webElementGuid>8ea03b43-d07d-42fe-af6d-8580fcfd31e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[4]/following::button[1]</value>
      <webElementGuid>0fc6b43b-f43f-4e48-8fc1-e0af21214381</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ng-form/div/div/div/div/button</value>
      <webElementGuid>c244b83f-1af6-494c-9848-350cd316f5e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = ' Save' or . = ' Save')]</value>
      <webElementGuid>9c9c8b10-a467-4cac-885a-9458aeccb130</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
