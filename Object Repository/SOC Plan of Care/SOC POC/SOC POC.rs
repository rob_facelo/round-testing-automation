<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SOC POC</name>
   <tag></tag>
   <elementGuidId>17f44645-3e34-4c04-8197-5cd0f1c6fd82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OasisPlan']/div/div/table/tbody/tr[2]/td/table/tbody/tr/td/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdFlatPadding&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13 cursorPointer ng-binding&quot;][count(. | //*[(text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')]) = count(//*[(text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.fontcolorDark13.cursorPointer.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fcf51317-18c3-4d0f-bf12-82c18fbfeea7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fontcolorDark13 cursorPointer ng-binding</value>
      <webElementGuid>c8700c9e-feef-412a-88a5-9c02c355ac0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>../../POC/Poc485.aspx?pocId=62001</value>
      <webElementGuid>bc1b6e36-eb6f-4e46-9bdf-6d5d2b60b071</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Plan Of Care / 485</value>
      <webElementGuid>7e4a71de-ff70-413c-af1f-7b1d081ca02b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdFlatPadding&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13 cursorPointer ng-binding&quot;]</value>
      <webElementGuid>234d345f-0030-4669-a83c-7c6ce47ec897</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OasisPlan']/div/div/table/tbody/tr[2]/td/table/tbody/tr/td/a</value>
      <webElementGuid>feda1470-2407-428e-a7d6-a05c2b07d6cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Plan Of Care / 485')]</value>
      <webElementGuid>47068e38-c80f-417c-95d1-be43d5ab4241</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Locked: 01/01/2023'])[1]/following::a[1]</value>
      <webElementGuid>fd9f7992-d982-47c7-b1d6-fdf30abf1949</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Start of care - further visits planned'])[1]/following::a[1]</value>
      <webElementGuid>7d52f0bd-9dde-412e-8770-42768ce7aea5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Progress'])[1]/preceding::a[1]</value>
      <webElementGuid>95df5659-2e2f-4f64-8b4a-09a60a96def8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Updated By: dslfacelo'])[1]/preceding::a[1]</value>
      <webElementGuid>bbda6f4d-321d-4c35-a469-84fdf0f350e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Plan Of Care / 485']/parent::*</value>
      <webElementGuid>315f2c2c-8d4c-4fc8-bba3-7942530e7ecb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '../../POC/Poc485.aspx?pocId=62001')]</value>
      <webElementGuid>e744f754-1f56-434f-8b78-b6824ea68330</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td/table/tbody/tr/td/a</value>
      <webElementGuid>3efbf60a-8f53-470f-a265-7faa690fc67f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '../../POC/Poc485.aspx?pocId=62001' and (text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')]</value>
      <webElementGuid>d63c52a6-409c-4e98-8d64-406a062f7712</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
