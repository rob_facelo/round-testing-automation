<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_walker, sarah SN -</name>
   <tag></tag>
   <elementGuidId>eae26503-fe00-483d-824b-83c4a4ff12af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//fieldset[@id='dpPocSelectCarestaff']/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d5f55a8e-9f0b-47be-9518-e59955c52422</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>97b28107-6c73-470b-a0af-f801478b6642</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        walker, sarah SN - 
                    </value>
      <webElementGuid>18163599-46e2-4d80-933c-da0b12a5cfdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dpPocSelectCarestaff&quot;)/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>4276053b-cf3b-418f-998d-7fcd3e971d7d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//fieldset[@id='dpPocSelectCarestaff']/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>92c8a7e3-5485-499f-b50b-6340409cdb70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='bartowski, ellie, MD'])[1]/following::div[6]</value>
      <webElementGuid>4fc1d15b-4011-455f-a436-33554ece9a38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='WHITE, WALTER'])[1]/following::div[8]</value>
      <webElementGuid>c61adc63-d233-4b2c-8d2c-08a571b97622</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[1]/preceding::div[3]</value>
      <webElementGuid>20c49c31-ea20-4fb7-8c0c-1716a46bfd4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='walker, sarah SN -']/parent::*</value>
      <webElementGuid>f665b8a4-5677-4b19-947b-55f13b495bcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>25adc41d-2c4b-43f3-8f7d-b6737a3034ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        walker, sarah SN - 
                    ' or . = '
                        walker, sarah SN - 
                    ')]</value>
      <webElementGuid>6762fa26-2286-4b28-b5b8-6ce3af929a04</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
