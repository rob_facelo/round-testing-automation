<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Acosta, Casey SN -</name>
   <tag></tag>
   <elementGuidId>5bd9b4d4-95fd-429d-935f-f9e1c39819c5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//fieldset[@id='dpPocSelectCarestaff']/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
                        Acosta, Casey SN - 
                    ' or . = '
                        Acosta, Casey SN - 
                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a71d61ee-454a-418d-95f4-6c43e1a93cfb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>3395cc79-e21e-4db7-887b-6116c7cf5197</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Acosta, Casey SN - 
                    </value>
      <webElementGuid>615d8d95-d883-48c0-896f-8a8f3e30e5cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dpPocSelectCarestaff&quot;)/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>ab8e0a80-737d-4634-b2ad-073e611a5bda</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//fieldset[@id='dpPocSelectCarestaff']/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>1076205b-58bd-435d-86a7-f3eca4ebd3a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Strange, Stephen'])[1]/following::div[6]</value>
      <webElementGuid>ed697872-4b23-434b-9edb-3b56d6a21709</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='STYLES, FURIOUS'])[1]/following::div[8]</value>
      <webElementGuid>9d3d7723-724a-4ef6-8222-3577a94fa8c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[1]/preceding::div[3]</value>
      <webElementGuid>4d3aae56-1975-474d-b539-f55c27b68aee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Acosta, Casey SN -']/parent::*</value>
      <webElementGuid>654cf4cd-ccf0-4868-bd71-cae3a435e211</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>07c08f7f-6f6b-42c2-9bf3-4d53ddd2dc03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Acosta, Casey SN - 
                    ' or . = '
                        Acosta, Casey SN - 
                    ')]</value>
      <webElementGuid>d6426b6d-bca3-4690-8999-5968be2e6ec9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
