<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Plan Of Care  485</name>
   <tag></tag>
   <elementGuidId>fc79eef6-7325-4d9d-8cb4-9c08bf5617b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdFlatPadding&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13 cursorPointer ng-binding&quot;][count(. | //*[(text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')]) = count(//*[(text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OasisPlan']/div/div/table/tbody/tr[2]/td/table/tbody/tr/td/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.fontcolorDark13.cursorPointer.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>465fe20f-eafc-4cd4-8023-205dff372718</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fontcolorDark13 cursorPointer ng-binding</value>
      <webElementGuid>191760b2-d122-4417-9250-bb236c792872</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>../../POC/Poc485.aspx?pocId=62739</value>
      <webElementGuid>d8481afc-8c2f-4afd-b769-8c3f8a527f5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Plan Of Care / 485</value>
      <webElementGuid>73daf882-c370-4148-b0fa-de60c3be0935</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdFlatPadding&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13 cursorPointer ng-binding&quot;]</value>
      <webElementGuid>6496bc9f-7b12-4c6c-bfa5-f3ed9ac59e86</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OasisPlan']/div/div/table/tbody/tr[2]/td/table/tbody/tr/td/a</value>
      <webElementGuid>11938ed3-06b2-4303-b301-44520ce4c433</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Plan Of Care / 485')]</value>
      <webElementGuid>01db8372-4666-4b9e-aa68-365985eff0a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Locked: 07/01/2022'])[1]/following::a[1]</value>
      <webElementGuid>6371809e-648b-4bc0-9944-ffb8e6e7ef31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Start of care - further visits planned'])[1]/following::a[1]</value>
      <webElementGuid>2c1d9cd2-d9a0-4856-9be7-02da85103152</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Progress'])[1]/preceding::a[1]</value>
      <webElementGuid>29555df8-0326-41a7-844d-c85511319ad5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Updated By: dslfacelo'])[1]/preceding::a[1]</value>
      <webElementGuid>a1cbdc69-ade6-4d54-af02-f0cf25a48c4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Plan Of Care / 485']/parent::*</value>
      <webElementGuid>b81182e1-dd8e-46c7-8631-69b6db92b8e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '../../POC/Poc485.aspx?pocId=61996')]</value>
      <webElementGuid>e750561a-c535-44c2-a10a-922114e255f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/table/tbody/tr/td/a</value>
      <webElementGuid>7b58cb18-a520-4cee-9819-cd8584aae823</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '../../POC/Poc485.aspx?pocId=61996' and (text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')]</value>
      <webElementGuid>f339697a-fedf-4b66-9252-ea55d69c6cb9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
