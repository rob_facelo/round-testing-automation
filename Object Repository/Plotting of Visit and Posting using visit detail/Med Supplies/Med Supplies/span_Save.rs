<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save</name>
   <tag></tag>
   <elementGuidId>abed1f2c-523c-4363-8e8b-5f5ec416637b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-9.col-md-9.col-lg-9 > button.btn.btn-default.font12 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Medical Supplies'])[1]/following::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ba640765-e269-4c78-835e-53744bdac4d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Save</value>
      <webElementGuid>0fb23536-f396-4b79-922f-e585d288b1fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[1]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-9 col-md-9 col-lg-9&quot;]/button[@class=&quot;btn btn-default font12&quot;]/span[1]</value>
      <webElementGuid>3a6a43a3-3c9f-43dc-a8f0-44f8591a0d67</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Medical Supplies'])[1]/following::span[1]</value>
      <webElementGuid>a23facad-2ef7-4128-be75-dd5f3d504d25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[10]/following::span[2]</value>
      <webElementGuid>500f82e7-531c-4503-9aab-09a073160a70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='null'])[1]/preceding::span[2]</value>
      <webElementGuid>eba62bc7-1224-44f2-8bf5-ac7f10cfb407</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div[2]/div/div/button/span</value>
      <webElementGuid>6b815089-3626-452f-96c6-4cbb779c0953</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Save' or . = ' Save')]</value>
      <webElementGuid>d23d743f-fe07-41a1-a6e9-3e5c13a6918a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
