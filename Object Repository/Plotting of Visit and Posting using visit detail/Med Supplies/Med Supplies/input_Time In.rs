<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Time In</name>
   <tag></tag>
   <elementGuidId>a2ef4724-420c-4906-b8c6-14c8513926e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.form-control.ng-pristine.ng-valid.ng-valid-mask.ng-valid-maxlength.ng-touched</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[127]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7b3c11be-4709-42a2-a661-532d649b4a9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>bbd75f89-4635-4708-a214-ad466905f10c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>4</value>
      <webElementGuid>5e6cb872-6f3b-452c-9417-a7cbd112093b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>extractNumber(this, 0, false); isvalidMilhour(this);</value>
      <webElementGuid>d6617517-3d7d-409a-92ff-2a495bf05c52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeydown</name>
      <type>Main</type>
      <value>extractNumber(this,0,false);</value>
      <webElementGuid>b8194d55-fb09-4409-975e-a12675269df4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeypress</name>
      <type>Main</type>
      <value>return blockNonNumbers(this, event, false, false);</value>
      <webElementGuid>30c50ee9-0144-4b7d-9dc2-4855732fd975</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>StartTimeHour</value>
      <webElementGuid>9f7fcac7-a6d5-4fa2-8832-fea0c4d882b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ui-mask</name>
      <type>Main</type>
      <value>99</value>
      <webElementGuid>196f1c45-be38-4b6e-9e93-20e9cd7356d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-valid ng-valid-mask ng-valid-maxlength ng-touched</value>
      <webElementGuid>91759310-d075-44d0-9d47-86a39e3abd1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>00</value>
      <webElementGuid>10a299a4-27b2-49bc-84b4-b06c873bcc53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-4 col-lg-4&quot;]/div[@class=&quot;row line-height30px&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@id=&quot;TimeIn&quot;]/input[@class=&quot;form-control ng-pristine ng-valid ng-valid-mask ng-valid-maxlength ng-touched&quot;]</value>
      <webElementGuid>ad676270-adca-4989-9ca3-639afb291bc1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[127]</value>
      <webElementGuid>d0c1fc3f-c894-4477-9ae8-a9500c3b5762</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//div[@id='TimeIn']/input)[3]</value>
      <webElementGuid>07f37508-6cd2-4d35-b37a-76010a139341</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[3]/div/div/div/input</value>
      <webElementGuid>05a589ea-3b5a-4b14-a5f5-b795f9640973</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = '00']</value>
      <webElementGuid>de7cc34c-7815-486a-9f51-4b2b58db9ee6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
