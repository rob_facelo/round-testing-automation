<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add</name>
   <tag></tag>
   <elementGuidId>a17c297c-0241-47aa-a769-bb3c11575556</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[46]/following::span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>02484402-3f3e-4e8c-8d48-b79197157862</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Add</value>
      <webElementGuid>9f44a849-8c92-4381-863f-cb2758075cc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[1]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim&quot;]/div[@class=&quot;panel-body&quot;]/table[@class=&quot;table table-bordered table-striped margin-bottom-sm&quot;]/tbody[1]/tr[1]/td[@class=&quot;font12 tdFlatPadding alignCenter&quot;]/button[@class=&quot;btn btn-default font12 alignCenter&quot;]/span[2]</value>
      <webElementGuid>8db8929c-8c29-4961-b46a-02d97418c1a2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[46]/following::span[2]</value>
      <webElementGuid>4915af6a-6fba-4d11-9e05-78b5844a5b17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Cost'])[2]/following::span[3]</value>
      <webElementGuid>909db978-81a8-4341-a6a5-ecaeecc9cbb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Exit Medical Supply'])[1]/preceding::span[1]</value>
      <webElementGuid>ed7d9e6f-5b02-4ac3-aba1-4211d63458c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[5]/button/span[2]</value>
      <webElementGuid>9cf96840-5a31-4a16-bf7a-22fb3eef6fac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Add' or . = ' Add')]</value>
      <webElementGuid>bd2b311a-c397-4809-b5b2-514864cd8b21</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
