<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_SN  - Acosta, Casey</name>
   <tag></tag>
   <elementGuidId>d78cc1af-5ed2-49d9-8202-8731d439cef3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-9.col-md-9.col-lg-9 > autocomplete-directive.ng-isolate-scope > div.dropdown > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Employee'])[1]/following::div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ce3e5cf1-a7a2-4951-8cf2-fef2e0a4e69e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>d3a07cc8-fb91-4fc5-b24b-952269f88d02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        SN  - Acosta, Casey  - 
                    </value>
      <webElementGuid>f011aaef-e487-4fe4-9ea7-88f69f594bc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-7 col-md-7 col-lg-7&quot;]/div[@class=&quot;row line-height30px&quot;]/div[@class=&quot;col-xs-12 col-sm-9 col-md-9 col-lg-9&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>a8e8c45a-917c-412f-8a89-ac3bf0b2a820</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Employee'])[1]/following::div[4]</value>
      <webElementGuid>a1f8590e-bba3-4c2c-89aa-27bb0ac2d38f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payer'])[2]/following::div[7]</value>
      <webElementGuid>2c21d26d-1684-4be6-9168-7fa3e6cbdb00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[45]/preceding::div[3]</value>
      <webElementGuid>d5f9de56-5461-47d1-830e-c17b955e8649</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>9c555aca-642c-41b4-abc5-7aa076407814</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        SN  - Acosta, Casey  - 
                    ' or . = '
                        SN  - Acosta, Casey  - 
                    ')]</value>
      <webElementGuid>3441800d-cde2-41e3-9744-b40ebfec7b6b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
