<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Med Supply</name>
   <tag></tag>
   <elementGuidId>690aa9b8-391e-4b1a-90dc-b705181b7ea0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover576783']/div[2]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst' and @ng-click = 'ShowMedSupplyNew()' and (text() = '
                  Med Supply' or . = '
                  Med Supply')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > a.list-group-item.list-group-item-action.cursorPointer.font13.ActionItemPopOverFirst</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ed29abe5-c934-424f-964d-d023cc8e9356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst</value>
      <webElementGuid>c3620bf4-4794-4506-a52e-9b2b021e0b85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>ShowMedSupplyNew()</value>
      <webElementGuid>23830f8e-9002-4f27-82f4-43cab157ccb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Med Supply</value>
      <webElementGuid>0ad83914-4931-43c3-bd02-8bbd2513924a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover576783&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst&quot;]</value>
      <webElementGuid>6c7bcc31-6126-4e05-8d29-143a98647d86</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover576783']/div[2]/a</value>
      <webElementGuid>c98b79ac-28e5-4477-bcfc-c51866ffce97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::a[1]</value>
      <webElementGuid>0b08980d-477b-4bdf-af4d-05ac9c9a5512</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::a[1]</value>
      <webElementGuid>c3e7b581-e4a7-4bd3-b8e8-4468e849f2a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/a</value>
      <webElementGuid>449d6c46-146c-4f11-8c14-8b868067b22f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  Med Supply' or . = '
                  Med Supply')]</value>
      <webElementGuid>69d3866a-e36d-4808-a84b-84a4df2e8d23</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
