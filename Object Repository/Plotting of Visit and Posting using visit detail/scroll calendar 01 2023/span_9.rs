<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_9</name>
   <tag></tag>
   <elementGuidId>20e6b356-714f-45a4-92c1-048af462745f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tbody[@id='visitCalendarBody']/tr[17]/td/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>82757861-7d6f-4247-aaa2-cb1118e95698</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-bold ng-binding</value>
      <webElementGuid>2cf65ff5-d26b-41e1-83ce-e56e968ddb4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>9</value>
      <webElementGuid>6fce193e-d408-49cd-a11f-92c900b68709</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;visitCalendarBody&quot;)/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdDateContent&quot;]/div[@class=&quot;cellTopContent&quot;]/span[@class=&quot;text-bold ng-binding&quot;]</value>
      <webElementGuid>d31e4963-5092-4d86-9fcd-abcf433c5204</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='visitCalendarBody']/tr[17]/td/div/span</value>
      <webElementGuid>d66380d4-6bbb-481f-8fd4-6f8b0e074d0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transfer Summary Note'])[8]/following::span[1]</value>
      <webElementGuid>2aeeb6f3-2641-4d45-a4c6-65eb750c2e90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Supervisory Visit Note'])[8]/following::span[2]</value>
      <webElementGuid>f34a280a-c3c3-4de1-89c1-21d892d9e6cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Planned'])[9]/preceding::span[1]</value>
      <webElementGuid>10f8a1f3-2c14-4add-ab8a-233c45334290</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actual'])[9]/preceding::span[2]</value>
      <webElementGuid>d51da2bf-ee96-4c7b-8a39-54899ed8b23c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='9']/parent::*</value>
      <webElementGuid>d442b13a-a3b1-4d3c-9c5a-8e4e85725d45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[17]/td/div/span</value>
      <webElementGuid>b4d656db-12dc-44af-a3f1-d6981be76b7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '9' or . = '9')]</value>
      <webElementGuid>b7f5def3-4ade-493e-9907-9223241bc886</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
