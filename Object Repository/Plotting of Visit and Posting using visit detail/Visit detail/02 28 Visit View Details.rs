<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>02 28 Visit View Details</name>
   <tag></tag>
   <elementGuidId>261d6479-8035-4061-b59e-b15238cfb286</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = 'showVisitDetails()' and (text() = '
                  View Details' or . = '
                  View Details')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > a.list-group-item.list-group-item-action.cursorPointer.font13.ActionItemPopOverLast</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover511871']/div[2]/a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>da5197af-24ed-46aa-8051-b3ab148d86de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast</value>
      <webElementGuid>c97b99f4-4382-497e-bdc5-8fe42fcd2a30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>showVisitDetails()</value>
      <webElementGuid>896c2081-4c8b-4267-95a1-ff8f409e358d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  View Details</value>
      <webElementGuid>0ed44bc5-a4fa-4080-b861-e6148220fa53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover155687&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast&quot;]</value>
      <webElementGuid>91ca9d60-76a8-42c8-ac39-fadf7357b79c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover155687']/div[2]/a[2]</value>
      <webElementGuid>f01b1a83-470b-4115-b0f2-c3e7687fae23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::a[2]</value>
      <webElementGuid>53f10740-8732-480c-b75f-9bc2e2a1b472</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/a[2]</value>
      <webElementGuid>53034f9c-03db-4b33-a843-bb46c2451d5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  View Details' or . = '
                  View Details')]</value>
      <webElementGuid>c2965c45-2b5b-4ec3-9186-c41c8d1b7f95</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
