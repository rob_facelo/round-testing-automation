<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click SN</name>
   <tag></tag>
   <elementGuidId>06f2097c-7f6f-4433-89d5-eef59c3a0bcc</elementGuidId>
   <imagePath>Screenshots/Targets/Page_/click SN.png</imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value>Screenshots/Targets/Page_/click SN.png</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.visited.ng-binding</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'visited ng-binding' and @ng-click = concat(&quot;cl.showPopOver($event, dayRow, &quot; , &quot;'&quot; , &quot;visitPosted&quot; , &quot;'&quot; , &quot;, vPosted, weekRow.weekNumber)&quot;) and (text() = 'SN' or . = 'SN')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tbody[@id='visitCalendarBody']/tr/td[7]/div[3]/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4d0f0977-74db-4cd3-ad04-baaeb49048f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>visited ng-binding</value>
      <webElementGuid>a36f2821-d386-4083-b175-31a64d7a3092</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>cl.showPopOver($event, dayRow, 'visitPosted', vPosted, weekRow.weekNumber)</value>
      <webElementGuid>d58ef5fc-0b0c-4278-8345-8a4f4798f8dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>SN</value>
      <webElementGuid>15c74b09-af9e-4b23-8bae-e24ce489acbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;visitCalendarBody&quot;)/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdDateContent pdgmPeriodHoverClass&quot;]/div[@class=&quot;cellContent cellContentPosted&quot;]/div[@class=&quot;ng-scope&quot;]/a[@class=&quot;visited ng-binding&quot;]</value>
      <webElementGuid>f24739cc-e6e3-46ad-8548-cdaa185731e1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='visitCalendarBody']/tr/td[7]/div[3]/div/a</value>
      <webElementGuid>c1b331b9-3794-4539-89b9-aad4480879c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'SN')])[2]</value>
      <webElementGuid>2e2f4a33-e406-4efb-88ea-fcaa0acc06c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SN'])[1]/following::a[1]</value>
      <webElementGuid>18a6c529-fc54-423a-8f91-df61863dcfae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jul 1'])[1]/following::a[2]</value>
      <webElementGuid>ab244f94-ef01-48e2-84c6-96f1fb06579d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jul 2'])[1]/preceding::a[1]</value>
      <webElementGuid>69d3c0c0-7753-49e1-9af5-e87210f6c923</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create New Other Note :'])[1]/preceding::a[2]</value>
      <webElementGuid>636be432-9661-4857-9f20-17e12a9466b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/a</value>
      <webElementGuid>3d1c17cc-7298-4f93-a392-ee0bf04d819d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'SN' or . = 'SN')]</value>
      <webElementGuid>8b7b4948-602d-46df-be78-42e8a395feae</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
