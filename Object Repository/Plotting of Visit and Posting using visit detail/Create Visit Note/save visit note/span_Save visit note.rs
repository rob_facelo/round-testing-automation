<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save visit note</name>
   <tag></tag>
   <elementGuidId>fae6ffdb-552b-4b53-8952-4d5c771fd2c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > button.btn.btn-default.font12.ng-scope > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='VisitNoteContainer']/div/div[2]/div/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>cb8d9489-9a15-4931-b3bd-95ad11a2e5fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Save</value>
      <webElementGuid>78f9e0f1-5929-42f7-9180-924d8fa3a9e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;VisitNoteContainer&quot;)/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body visit-modal-body&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;row modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12 ng-scope&quot;]/span[1]</value>
      <webElementGuid>5371ca16-7953-476e-bb1d-0e023166d2b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='VisitNoteContainer']/div/div[2]/div/div/div/button/span</value>
      <webElementGuid>72da1fae-9a20-4406-8df3-39ce54f406aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comments:'])[1]/following::span[3]</value>
      <webElementGuid>b3deb464-53b8-4382-8024-cf228f7f2c97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='QA Comments'])[2]/preceding::span[1]</value>
      <webElementGuid>86af0a64-aec2-4b21-9a72-b533c1a27841</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More Options'])[1]/preceding::span[2]</value>
      <webElementGuid>8c121341-85ba-4d10-bb30-c8b435b3c704</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]/div/div/div/button/span</value>
      <webElementGuid>ff9287e7-40b9-4b5c-8a2e-6fd8babaaef3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Save' or . = ' Save')]</value>
      <webElementGuid>aea9d614-915b-47b7-aeca-0f6d93b039fb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
