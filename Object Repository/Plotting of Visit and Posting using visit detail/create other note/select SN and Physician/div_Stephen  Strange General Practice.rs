<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Stephen  Strange General Practice</name>
   <tag></tag>
   <elementGuidId>66d6e9f9-a55c-4052-bb19-4792ff751c55</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OtherNoteContainer']/div/div[2]/div[2]/div/fieldset/div/div[2]/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a05bc1b6-208d-47f8-b30b-ac79de093fad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>a340e2a7-7678-4d4d-b843-b21cbf6ad496</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Stephen  Strange General Practice - 
                    </value>
      <webElementGuid>6fedccb0-f15d-449f-a4f2-4c2d01a019e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OtherNoteContainer&quot;)/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body other-modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/fieldset[1]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6 ng-scope&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-heading hhc-blue-lighter&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-10 col-sm-8 col-md-8 col-lg-8&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>074bee75-488b-46c8-b4cb-9d3175c3c4ea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OtherNoteContainer']/div/div[2]/div[2]/div/fieldset/div/div[2]/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>a49e52bf-c4a9-4cc4-a717-14d822f9c93f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physician'])[3]/following::div[4]</value>
      <webElementGuid>84c82338-2045-4b78-b371-7d944a09ce8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[42]/following::div[12]</value>
      <webElementGuid>73a5317e-b0c7-4daa-923e-8b0736b6e49e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[43]/preceding::div[3]</value>
      <webElementGuid>d555ac9c-8f87-4c53-b595-fec6dd1c4fb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>4d2b3f32-aebc-40d7-9dc0-4783c0c0af69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Stephen  Strange General Practice - 
                    ' or . = '
                        Stephen  Strange General Practice - 
                    ')]</value>
      <webElementGuid>f40da06c-15f5-4ad5-9793-5ad0c8f230bb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
