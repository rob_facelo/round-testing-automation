<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Casey Acosta SN -</name>
   <tag></tag>
   <elementGuidId>83d5e0b8-17ac-4704-bd3d-c7cafbccbf5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OtherNoteContainer']/div/div[2]/div[2]/div/fieldset/div/div/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>efff5e30-3291-430c-bb80-f430da22e460</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>5d668912-eca8-4a81-80bc-fe6c248b7530</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Casey Acosta SN - 
                    </value>
      <webElementGuid>c8e1fcb2-bbc3-44a8-a64b-bcbfa90f4944</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OtherNoteContainer&quot;)/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body other-modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/fieldset[1]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-heading hhc-blue-lighter&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-8 col-sm-8 col-md-8 col-lg-8&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>3b64567a-22cd-45ad-b7fe-b3d57498fca2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OtherNoteContainer']/div/div[2]/div[2]/div/fieldset/div/div/div/div/div/div[2]/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>2341405e-f0b5-4ac9-a874-804c6288457d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff'])[1]/following::div[4]</value>
      <webElementGuid>5e7cae15-3de0-4cae-bb7a-4dfb84ebe4ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[42]/preceding::div[3]</value>
      <webElementGuid>8cf42f55-19d2-47b8-b2f7-595514cbc8e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physician'])[3]/preceding::div[6]</value>
      <webElementGuid>1a030b49-cc5a-43e1-b0c6-afb8b777dad6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Casey Acosta SN -']/parent::*</value>
      <webElementGuid>7e2686f4-efc6-41bd-956d-d0f93cc533b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>8e47dbd4-0583-494a-bd26-8d79c2df5444</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Casey Acosta SN - 
                    ' or . = '
                        Casey Acosta SN - 
                    ')]</value>
      <webElementGuid>7b4ee43f-8b50-486e-9d88-15211d3c6940</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
