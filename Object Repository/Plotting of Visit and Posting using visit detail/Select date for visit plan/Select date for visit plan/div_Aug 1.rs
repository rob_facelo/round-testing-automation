<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Aug 1</name>
   <tag></tag>
   <elementGuidId>fb10f48f-9f1c-493e-b8da-f8aef77d1a9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[8]/div[2]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>476b57c3-9e42-4ac5-ac1f-7cc5c95705fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.DayClick('btnDate', 0, 0, '8-1-2022')</value>
      <webElementGuid>fe3438b8-6d09-4908-9cfd-b99b1221fd8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursorPointer</value>
      <webElementGuid>c0e6fdc3-cc53-452b-a678-b5d9f40ce45b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;calendarWidth font11 ng-scope&quot;]/div[@class=&quot;cursorPointer&quot;]</value>
      <webElementGuid>0874b0c6-18fa-4c4e-bb0b-b9ec7bd58751</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[8]/div[2]/div[2]</value>
      <webElementGuid>4a3f618a-dc13-4d2f-8099-aa30557613c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[8]/div[2]/div[2]</value>
      <webElementGuid>9f8a4cd4-7e43-453a-8926-287719480354</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
