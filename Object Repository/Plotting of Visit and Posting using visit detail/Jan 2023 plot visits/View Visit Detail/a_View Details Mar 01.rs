<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_View Details Mar 01</name>
   <tag></tag>
   <elementGuidId>377a77ee-21cf-43bd-84eb-d46d49b9a9aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver' and @ng-click = 'showVisitDetails()' and (text() = '
                  View Details' or . = '
                  View Details')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover29697']/div[2]/a[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>f7d79e2a-3a97-44f7-aa16-dd5ddc90d518</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver</value>
      <webElementGuid>ae47fdf0-c9af-434e-989f-382bdc53d29d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>showVisitDetails()</value>
      <webElementGuid>2066937e-2c17-4e55-935a-bd519acd223c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  View Details</value>
      <webElementGuid>e445fcae-a9ef-4c2d-931e-a10b9ff93d8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover29697&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver&quot;]</value>
      <webElementGuid>dc8a787c-1011-41f9-96d6-c43f1a40aaaf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover29697']/div[2]/a[3]</value>
      <webElementGuid>accd2b9f-6fc4-4fe1-a3da-14f113672aa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[3]</value>
      <webElementGuid>1179f7c5-c85a-4aa2-b630-340c59a47365</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  View Details' or . = '
                  View Details')]</value>
      <webElementGuid>9730bcc7-f7ee-4054-abc4-ca63c4b2c60d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
