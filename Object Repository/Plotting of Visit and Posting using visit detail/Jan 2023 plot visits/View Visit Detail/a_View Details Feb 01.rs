<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_View Details Feb 01</name>
   <tag></tag>
   <elementGuidId>9c7a5605-ae4b-49d0-ab70-f851ac44f503</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver' and @ng-click = 'showVisitDetails()' and (text() = '
                  View Details' or . = '
                  View Details')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover314532']/div[2]/a[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d5006110-0a7c-4058-af9a-1cbaa4bd5d03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver</value>
      <webElementGuid>fed5b0dc-2a7d-4a4d-bfb7-d9b4c40907b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>showVisitDetails()</value>
      <webElementGuid>5e7e281e-841f-4cde-867b-0e39f2e2a1ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  View Details</value>
      <webElementGuid>e4ec811b-c1a6-42af-8070-95230376d90c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover314532&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver&quot;]</value>
      <webElementGuid>981bee7b-52ee-44d5-a590-90808d9c1977</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover314532']/div[2]/a[3]</value>
      <webElementGuid>87d5fda2-766c-4ac7-bdb9-d666ab60f3f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[3]</value>
      <webElementGuid>6e9b5732-ec67-4871-8538-656f3a86146b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  View Details' or . = '
                  View Details')]</value>
      <webElementGuid>106bcda1-8390-4f1a-8d4c-e79095fbcd23</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
