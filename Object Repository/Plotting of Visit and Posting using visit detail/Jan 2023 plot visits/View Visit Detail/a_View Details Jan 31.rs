<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_View Details Jan 31</name>
   <tag></tag>
   <elementGuidId>6fd07e36-780a-45a4-8756-222686a91a8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver' and @ng-click = 'showVisitDetails()' and (text() = '
                  View Details' or . = '
                  View Details')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover668822']/div[2]/a[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d0d0bed2-f9cd-4737-ace4-83a8a621a4ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver</value>
      <webElementGuid>e978d4c2-4726-4e45-9dc0-b1b263f79862</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>showVisitDetails()</value>
      <webElementGuid>85de730b-38de-41da-b2a0-b757ca5f5c38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  View Details</value>
      <webElementGuid>ee57456d-83c5-4bea-a712-45254e05ab89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover668822&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver&quot;]</value>
      <webElementGuid>26f8ee55-84df-4900-85c9-737901cc9fe4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover668822']/div[2]/a[3]</value>
      <webElementGuid>10540f67-10a8-42ec-899a-3f4817fa7fad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[3]</value>
      <webElementGuid>75dce2ec-c4d6-47d1-bfdc-9c234924b51b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  View Details' or . = '
                  View Details')]</value>
      <webElementGuid>2ee22e29-adbb-438a-8e6b-93a679ae6c94</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
