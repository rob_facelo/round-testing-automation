<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Jan 30_cursorPointer</name>
   <tag></tag>
   <elementGuidId>3012a982-25b2-44e0-a09d-22f4ff0b1bf7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[7]/div[2]/div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = concat(&quot;plotVisit.DayClick(&quot; , &quot;'&quot; , &quot;btnDate&quot; , &quot;'&quot; , &quot;, 0, 0, &quot; , &quot;'&quot; , &quot;1-30-2023&quot; , &quot;'&quot; , &quot;)&quot;)]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c2c05b1b-9a57-4eeb-80be-e791175c0299</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.DayClick('btnDate', 0, 0, '1-30-2023')</value>
      <webElementGuid>f952f118-fba1-4d4b-8bad-5ea0eb4861a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursorPointer</value>
      <webElementGuid>ff45c607-01a2-4a5b-850e-fd63811e3804</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;calendarWidth font11 ng-scope&quot;]/div[@class=&quot;cursorPointer&quot;]</value>
      <webElementGuid>fd2468f0-aa53-41c1-afaa-f986aa08d316</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[7]/div[2]/div[2]</value>
      <webElementGuid>0d2417a1-b5b3-4b53-ba4d-4feb858923b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[7]/div[2]/div[2]</value>
      <webElementGuid>306dfc1b-c531-43a9-952c-3a7a8f8ab5e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
