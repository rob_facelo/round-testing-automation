<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Feb 1_cursorPointer</name>
   <tag></tag>
   <elementGuidId>42cf2f45-dfed-4f38-a4c5-3b7e2d3ad304</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[7]/div[4]/div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = concat(&quot;plotVisit.DayClick(&quot; , &quot;'&quot; , &quot;btnDate&quot; , &quot;'&quot; , &quot;, 0, 0, &quot; , &quot;'&quot; , &quot;2-1-2023&quot; , &quot;'&quot; , &quot;)&quot;)]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d4223989-4a6d-4abd-9bdb-42c1b489e798</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.DayClick('btnDate', 0, 0, '2-1-2023')</value>
      <webElementGuid>44879b66-ac89-49f2-8c43-f615275e028c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursorPointer</value>
      <webElementGuid>5910a038-e03c-4eae-8059-67a58f39a68b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;calendarWidth font11 ng-scope&quot;]/div[@class=&quot;cursorPointer&quot;]</value>
      <webElementGuid>d7fed747-b6e0-40af-a309-93885dcc644b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[7]/div[4]/div[2]</value>
      <webElementGuid>152416ab-2296-4ba8-9951-763f3f93ef46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[4]/div[2]</value>
      <webElementGuid>463cb2a2-8e2c-426c-9e6f-5b44800c6c53</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
