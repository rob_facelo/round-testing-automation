<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_View Details</name>
   <tag></tag>
   <elementGuidId>9ccc6731-8af2-4c16-ae36-9acf9af7436d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;popover155687&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver&quot;][count(. | //*[@ng-click = 'showVisitDetails()' and (text() = '
                  View Details' or . = '
                  View Details')]) = count(//*[@ng-click = 'showVisitDetails()' and (text() = '
                  View Details' or . = '
                  View Details')])]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover155687']/div[2]/a[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9c2f90c9-9221-4f0c-8858-f089d6c6d7cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver</value>
      <webElementGuid>84f2c9c8-7612-4219-9a67-2642555748d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>showVisitDetails()</value>
      <webElementGuid>4515564b-02a0-4aad-81f4-8dc0e29b5b47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  View Details</value>
      <webElementGuid>ce7d593a-2d91-4168-8023-32934b1df0b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover155687&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOver&quot;]</value>
      <webElementGuid>7e845de5-85df-408f-bd92-562bae71b737</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover155687']/div[2]/a[3]</value>
      <webElementGuid>1c6effa8-bc14-4adc-a592-6890d8c498c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[3]</value>
      <webElementGuid>e866db39-c27d-4d3f-8453-88f797b42263</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  View Details' or . = '
                  View Details')]</value>
      <webElementGuid>8a5195b1-8c9b-4ac4-a187-2c54629d348a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
