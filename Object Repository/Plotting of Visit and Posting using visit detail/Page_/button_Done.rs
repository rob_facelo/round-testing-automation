<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Done</name>
   <tag></tag>
   <elementGuidId>724fb7e6-36c8-4622-aa69-9e35a23e059d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[203]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = 'plotVisit.showGenerateVisitPanel()']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ng-scope > button.btn.btn-success</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>30083e56-e470-42f9-aecc-be2f19fee661</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a7da30e9-b003-4d67-9d29-de64d8e7f797</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.showGenerateVisitPanel()</value>
      <webElementGuid>dd8569f4-9dc8-4823-a0d2-ad8e94c8746c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-success</value>
      <webElementGuid>428ab8d2-6f18-4e67-be5e-0e7b0f2a8408</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Done</value>
      <webElementGuid>bfa421b4-b36c-444a-adf9-60fa8288d984</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer&quot;]/div[@class=&quot;ng-scope&quot;]/button[@class=&quot;btn btn-success&quot;]</value>
      <webElementGuid>bd6ff3f1-5479-4817-aec8-596a88b399b7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[203]</value>
      <webElementGuid>d203eaab-9aea-4299-9996-5cf1f97e85ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[3]/div/button[3]</value>
      <webElementGuid>ad19b3f1-6520-4e59-86fa-1c337aba1896</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plot'])[1]/following::button[1]</value>
      <webElementGuid>b3e9f76b-361e-47df-826b-81db20e62b9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear Selected'])[1]/following::button[2]</value>
      <webElementGuid>7c0ff885-0e01-4ab6-951d-108b3061d3a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fill-up Another Form'])[1]/preceding::button[2]</value>
      <webElementGuid>2efa92b4-0943-40fd-ab86-bb7814b85184</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Done']/parent::*</value>
      <webElementGuid>15728cdb-412d-4eb1-bfb0-29b5221ff143</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/button[3]</value>
      <webElementGuid>f8d6b486-3a99-4276-a588-5a0481be9143</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Done' or . = 'Done')]</value>
      <webElementGuid>a1e79079-bea4-4be6-8ebb-240ed591adea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
