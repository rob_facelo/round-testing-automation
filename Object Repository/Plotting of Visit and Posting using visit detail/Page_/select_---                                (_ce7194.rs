<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_---                                (_ce7194</name>
   <tag></tag>
   <elementGuidId>b2a6dbf8-9b34-49c8-8a4c-321eb1eb6c11</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ddlChangeLocation</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlChangeLocation']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>50195889-d437-4f0e-b943-c0b9d6ea5f03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>7</value>
      <webElementGuid>7bff9cc8-793b-4518-97ce-f954e808d182</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>40148dc2-69ea-4c55-b5df-a8784e0ab039</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlChangeLocation</value>
      <webElementGuid>6debc33b-1dbf-4232-b2e8-3fd7aecf3cb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>ServiceLocation</value>
      <webElementGuid>e70b2ccb-985c-4422-a749-ace468dad005</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>isDisabled || Pagefields.CalendarChangeCareStaff.AccessType == 1</value>
      <webElementGuid>020581f2-34f9-4e98-b8ce-d8ab1807be28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            </value>
      <webElementGuid>1f449fd4-cdb2-45d3-984f-84681d6ee968</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlChangeLocation&quot;)</value>
      <webElementGuid>5f1bdce3-0939-4dbf-af9e-005ba1c1f857</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlChangeLocation']</value>
      <webElementGuid>64a6883b-d019-4537-950a-b3669c712416</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Location'])[3]/following::select[1]</value>
      <webElementGuid>830d6ff1-6a8f-4a7d-aa85-666bff945052</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Module:'])[1]/following::select[1]</value>
      <webElementGuid>ab52f822-a96b-4eb8-9ffb-a2f47a9fc6dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Military time format)'])[2]/preceding::select[1]</value>
      <webElementGuid>72d00884-b35f-4efb-bf16-1519a6097845</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div[2]/select</value>
      <webElementGuid>e8f5b81f-2213-45aa-9fec-ac7f89a4785c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlChangeLocation' and (text() = '
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            ' or . = '
                                ---
                                (Q5001) Patient’s home/residence
                                (Q5002) Assisted living facility
                                (Q5009) Place not otherwise specified (NOS)
                            ')]</value>
      <webElementGuid>2fb450ba-8d1c-479b-8e93-06f543cc7169</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
