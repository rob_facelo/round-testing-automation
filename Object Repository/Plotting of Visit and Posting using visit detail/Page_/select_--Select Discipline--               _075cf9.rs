<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Select Discipline--               _075cf9</name>
   <tag></tag>
   <elementGuidId>d2e85d1e-b9e2-4bc4-9012-7d50c2c7e81e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ddlCaregiverType</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlCaregiverType']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'plotVisit.SelectedCgType' and (text() = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ' or . = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>8b315188-5232-49d0-a361-b2511fee7521</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm fontsize-13-weight-600 ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>4280feff-4858-4f96-91e9-8dcbd8651794</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlCaregiverType</value>
      <webElementGuid>5e5eaf11-3cac-4178-a196-e08187f238e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>plotVisit.OnCgTypeChange()</value>
      <webElementGuid>a3bb207d-c71a-44f6-b844-0c2a2c426621</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>plotVisit.SelectedCgType</value>
      <webElementGuid>d6786f52-1c57-4e2b-b304-530057d613da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                </value>
      <webElementGuid>190cf4fd-5258-4433-baca-b82599da6435</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlCaregiverType&quot;)</value>
      <webElementGuid>0f899c87-34f3-42cf-88be-6a90382b0854</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlCaregiverType']</value>
      <webElementGuid>0eea5099-33a5-47bd-bfcf-c000ac40a329</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>e281eb24-0378-4bed-80e3-43159b06fbdf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff Type'])[1]/following::select[1]</value>
      <webElementGuid>2ecac4a1-2e3b-4af0-95ae-bd9f82beafa0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payer'])[1]/following::select[2]</value>
      <webElementGuid>715457c0-ec3a-435f-b46e-0e7a370f51c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff'])[2]/preceding::select[1]</value>
      <webElementGuid>918d146b-cb40-44e3-8be5-0eaef56ab1e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Type'])[1]/preceding::select[2]</value>
      <webElementGuid>abad3380-358b-4080-9655-5115b426888b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>330f8470-e9e4-464a-ae7e-4ae6cc784894</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlCaregiverType' and (text() = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ' or . = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ')]</value>
      <webElementGuid>c9192048-baf4-487f-854b-5dd710bff855</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
