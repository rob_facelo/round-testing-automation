<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Display time inout</name>
   <tag></tag>
   <elementGuidId>2f769daf-d644-49cf-94c6-6da21b6fa8a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[55]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.ng-untouched.ng-valid.ng-dirty.ng-valid-parse</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c841344e-2827-4283-bda3-3f2c462dd056</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>added7ed-3b86-4b9c-bcba-e18dee5f8a1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>AgencyDetails.VisitCalendarPlottingShowTime</value>
      <webElementGuid>96400d20-396e-4a74-aed5-dfc226170e45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-untouched ng-valid ng-dirty ng-valid-parse</value>
      <webElementGuid>80474b57-c5d5-450e-8e6e-1fca82d319d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlVisitCalendar_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/label[@class=&quot;checkbox-inline&quot;]/input[@class=&quot;ng-untouched ng-valid ng-dirty ng-valid-parse&quot;]</value>
      <webElementGuid>7d0861e8-8e23-4b3a-bbfd-b3f6469b7251</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[55]</value>
      <webElementGuid>98d23588-9d73-4589-ba53-637cdc80e1c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlVisitCalendar_RPC']/div[4]/div/label/input</value>
      <webElementGuid>905b6472-9397-492e-9840-0022a1aa7671</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[17]/div/table/tbody/tr/td/div[4]/div/label/input</value>
      <webElementGuid>850d7d1d-6892-441f-bcd3-3d7f150a604f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>aa6a2d41-fb9d-4a24-9f1f-bcb0779f9a63</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
