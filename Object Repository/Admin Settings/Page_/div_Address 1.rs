<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Address 1</name>
   <tag></tag>
   <elementGuidId>3e7b2fee-cc4d-491a-a4c4-b0fb11029f2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>fe712b22-110b-4ba1-8c8f-e0e59b058350</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>99e23445-ebb3-432a-a957-bc54bfffa613</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    Address 1:*
                                
                                
                                    
                                
                            </value>
      <webElementGuid>7b517769-5883-4d05-a57d-7a8e19cdfc89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>09fe8296-6324-4b9c-80b9-ea097b1667f7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[4]</value>
      <webElementGuid>209a5881-57a3-4e49-9afa-19164f2ffc09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/following::div[2]</value>
      <webElementGuid>437aa4d4-7543-4b83-adf1-841fd44bbf54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div/div/table/tbody/tr/td/div[4]</value>
      <webElementGuid>57fed450-252f-4486-9703-6f380f15bdec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    Address 1:*
                                
                                
                                    
                                
                            ' or . = '
                                
                                    Address 1:*
                                
                                
                                    
                                
                            ')]</value>
      <webElementGuid>19ab2000-0f8d-4143-b626-800349dc02ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
