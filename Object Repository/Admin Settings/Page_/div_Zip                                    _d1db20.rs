<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Zip                                    _d1db20</name>
   <tag></tag>
   <elementGuidId>968e345d-8d5a-4bc1-a747-07aab8ccd894</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[8]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e423231e-c5c6-46e0-8cef-71ac5eab66ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>d341edd2-2bbb-4e50-87f2-4226317ad5a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            </value>
      <webElementGuid>865bd2ca-7b74-401d-a97d-90efa28d6c72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>bacaf7e4-2f32-494a-860a-ed71097ab8de</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[8]</value>
      <webElementGuid>c3c33112-b5d2-4705-b135-f3f2cf8d70f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::div[2]</value>
      <webElementGuid>1fd2855e-f277-4592-8951-a6e253949876</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div/div/table/tbody/tr/td/div[8]</value>
      <webElementGuid>874b0319-0f92-4de1-ba31-6e538801b7fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            ' or . = '
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            ')]</value>
      <webElementGuid>5b7a4df7-ca09-4db6-b6a9-5b728f9a59d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
