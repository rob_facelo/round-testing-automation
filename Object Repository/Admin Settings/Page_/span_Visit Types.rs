<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Visit Types</name>
   <tag></tag>
   <elementGuidId>7a96c7ba-4cee-43a4-8b53-26e0909ade22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i38_T']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#applicationLeftNav_LeftPanel_nbLeftNavigation_I0i38_T > span.dx-vam.dx-wrap</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0145253b-a4cb-4346-a5c9-e83dbf2f79af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam dx-wrap</value>
      <webElementGuid>82219471-a9e8-4528-bfa9-a761ab94e109</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Visit Types</value>
      <webElementGuid>39d43467-5cab-4d4f-9834-b8bf1a5f9b83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;applicationLeftNav_LeftPanel_nbLeftNavigation_I0i38_T&quot;)/span[@class=&quot;dx-vam dx-wrap&quot;]</value>
      <webElementGuid>4f157463-df23-48d4-9ded-080a02ae3385</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i38_T']/span</value>
      <webElementGuid>80f7e3bd-2035-4ab9-9b88-ccc06908e2c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Note Template'])[2]/following::span[1]</value>
      <webElementGuid>18e3c900-e510-4e35-a94a-bde1ca32c59f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Surgical Codes'])[2]/following::span[2]</value>
      <webElementGuid>a4dbd725-3318-4ed1-972d-c10bedf3b18c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('What', &quot;'&quot;, 's New')])[3]/preceding::span[1]</value>
      <webElementGuid>b2d5e777-2a71-40a8-b055-c26fbdf29725</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Announcement'])[2]/preceding::span[2]</value>
      <webElementGuid>cecc10cc-620b-4a7c-a934-2dcdfe67abb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/ul/li[39]/a/span</value>
      <webElementGuid>77ac828e-309a-4df7-a855-f459d1102260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Visit Types' or . = 'Visit Types')]</value>
      <webElementGuid>dcd93fa9-825f-4201-aba0-e0d57b36e05a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
