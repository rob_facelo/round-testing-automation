<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Phone and Extension</name>
   <tag></tag>
   <elementGuidId>aad91dc0-8f65-461c-bb46-bad107868274</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[9]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c8a91985-1a62-44e9-bf28-5c11b9d413f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>988c9273-456c-496d-9162-407b41e648be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    Phone and Extension*
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                    
                                
                            </value>
      <webElementGuid>6672af04-603a-4694-8188-92ea38a29788</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>2d2cb291-fd3e-4ac4-8336-5f4cc9c7842d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[9]</value>
      <webElementGuid>81f42b95-8b8e-4cf6-8497-699aa525887a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip Code'])[2]/following::div[2]</value>
      <webElementGuid>34fd18e9-31d8-47d3-8d1e-27c3ad011670</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State'])[3]/following::div[2]</value>
      <webElementGuid>8bf7acc4-8818-4599-8496-be6168a9df58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/table/tbody/tr/td/div[9]</value>
      <webElementGuid>aa0cd33f-38f9-4018-9bdf-c45deef3920d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    Phone and Extension*
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                    
                                
                            ' or . = '
                                
                                    Phone and Extension*
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                    
                                
                            ')]</value>
      <webElementGuid>3fbdabe8-2286-4d2e-8602-86de1c2c5d22</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
