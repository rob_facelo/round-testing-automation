<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_( Go to Map )</name>
   <tag></tag>
   <elementGuidId>c1d47c38-8f13-4383-89d4-d019db8c1366</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[3]/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5535208f-fd1c-4f05-979e-eb7cfb707c6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xs-12 col-sm-12 col-md-6 col-lg-6</value>
      <webElementGuid>1371c41d-6e8b-40d6-824a-9476e2bb2ec1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    ( Go to Map )
                                </value>
      <webElementGuid>050a61d9-b03f-4f94-bf7d-25574e483de3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]</value>
      <webElementGuid>190705c4-0aed-4e54-b827-2f6b95efbdd8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[3]/div[2]</value>
      <webElementGuid>9c0ad1c7-1c14-4492-a2f0-509f7239b513</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[2]/following::div[1]</value>
      <webElementGuid>b8f1330d-d0c4-47e9-8871-c4d7ede4467f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Address 2'])[1]/preceding::div[1]</value>
      <webElementGuid>13e808e0-f32c-4e43-ab50-0a9b9ae1dc23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/table/tbody/tr/td/div[3]/div[2]</value>
      <webElementGuid>5ba772e9-8d99-4306-af3c-264fcc2d4365</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                    
                                    ( Go to Map )
                                ' or . = '
                                    
                                    ( Go to Map )
                                ')]</value>
      <webElementGuid>fa5f5dd0-08fa-46bc-b8c4-37d9da71a090</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
