<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_NoneCOVID-19 Employee Monitoring For_c18ed7</name>
   <tag></tag>
   <elementGuidId>a7d1319c-d942-4469-8b95-a64ec8cc76e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlUsers_RPC']/div[13]/div/table/tbody/tr/td/label/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>label.checkbox-inline > select.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>62e0873e-34aa-47b8-bb6a-9149025c7c63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>AgencyDetails.ActiveForm</value>
      <webElementGuid>96118a3e-8ca0-4f84-8dd1-9c31ced74b6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>e90b2277-b69e-45cd-abe7-8fce66d9e7b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        NoneCOVID-19 Employee Monitoring FormCOVID-19 PUI Screening Form (CDC)
                                                    </value>
      <webElementGuid>4c070eca-44e7-4c03-a0a3-a03e51af5350</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlUsers_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/table[1]/tbody[1]/tr[1]/td[1]/label[@class=&quot;checkbox-inline&quot;]/select[@class=&quot;ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>a56261c3-2189-497c-96f9-0640c0b7a642</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlUsers_RPC']/div[13]/div/table/tbody/tr/td/label/select</value>
      <webElementGuid>684ebbec-a6e6-4ef9-a140-e1f92a8d77c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active Form'])[1]/following::select[1]</value>
      <webElementGuid>f09346f4-8000-4b57-8187-96a68b5a41ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Calendar'])[1]/preceding::select[1]</value>
      <webElementGuid>ecf1c707-0a73-46ba-a77b-c2f267e557d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/select</value>
      <webElementGuid>d33e2d94-cb53-45cb-8690-2e262d98b14a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        NoneCOVID-19 Employee Monitoring FormCOVID-19 PUI Screening Form (CDC)
                                                    ' or . = '
                                                        NoneCOVID-19 Employee Monitoring FormCOVID-19 PUI Screening Form (CDC)
                                                    ')]</value>
      <webElementGuid>91d4e7fb-d90c-4152-8435-3ee8289fb745</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
