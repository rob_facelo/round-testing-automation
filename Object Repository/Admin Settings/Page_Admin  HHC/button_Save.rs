<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save</name>
   <tag></tag>
   <elementGuidId>60bd8176-1311-4ab7-99ef-b9162681db65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[49]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.btn-group.ng-scope > button.btn.btn-default</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>69354089-dce5-401f-b21a-65ada62b5ab5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a325996d-ed5f-4dc0-ba64-a88bc7aad4cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>ac.updateAgencyDetails()</value>
      <webElementGuid>133315f5-1509-47aa-9fdc-f135280a72a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
      <webElementGuid>af2ace33-8d9b-44d7-883a-79ebc8c98cc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Save</value>
      <webElementGuid>2051d9c5-e152-425a-96d9-a7084b53bdb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;myNav&quot;)/div[@class=&quot;col-xs-12 col-sm-8 col-md-8 col-lg-8&quot;]/div[@class=&quot;btn-group ng-scope&quot;]/button[@class=&quot;btn btn-default&quot;]</value>
      <webElementGuid>211176b1-6071-4cc9-a897-672668eae397</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[49]</value>
      <webElementGuid>fc095c8c-3b02-4c22-a210-969f90a166a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='myNav']/div/div/button</value>
      <webElementGuid>e0e5aee6-7f54-42a2-9f0b-15e52475135c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assign Patient to Users'])[2]/following::button[1]</value>
      <webElementGuid>8ecfdc47-f5e8-4449-a749-af9bbef8c594</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assign Rule to Role'])[2]/following::button[1]</value>
      <webElementGuid>6ff7a568-ad8d-4929-b6fd-cb2f67a53e71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div/div/div/button</value>
      <webElementGuid>c0c41eef-a8a4-4fb2-89ec-063925ac4c40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = ' Save' or . = ' Save')]</value>
      <webElementGuid>b02a1058-2f9c-444e-bfcf-3a3d78d22371</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
