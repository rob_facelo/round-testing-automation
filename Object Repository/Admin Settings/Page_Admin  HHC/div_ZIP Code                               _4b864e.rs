<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_ZIP Code                               _4b864e</name>
   <tag></tag>
   <elementGuidId>7eeebf32-786f-4777-8396-7e2045fddf14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[8]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5b2e00e2-2551-43f9-ade3-ea99b5aade81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>dd4fdb9e-36f6-4875-b383-0ef9254aa3e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    ZIP Code *
                                
                                
                                    
                                        
                                            
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                        
                                        
                                            
                                        
                                    
                                
                            </value>
      <webElementGuid>44886411-a77d-43f7-9454-299557bb7bb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>ddad89a6-e453-4e03-a56f-c39699ac5237</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[8]</value>
      <webElementGuid>a35d6bf3-ba7e-47b1-a9a8-447d96403548</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NOTE: City and State will be populated automatically by entering the ZIP code.'])[1]/following::div[1]</value>
      <webElementGuid>16c668e1-7d4d-44ea-8038-b1a9b031df9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State'])[2]/following::div[4]</value>
      <webElementGuid>e3b8fd93-a962-4a32-a676-e620073d37d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/table/tbody/tr/td/div[8]</value>
      <webElementGuid>f110d50d-f7cf-4ec9-a084-dae5a4df8f91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    ZIP Code *
                                
                                
                                    
                                        
                                            
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                        
                                        
                                            
                                        
                                    
                                
                            ' or . = '
                                
                                    ZIP Code *
                                
                                
                                    
                                        
                                            
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                        
                                        
                                            
                                        
                                    
                                
                            ')]</value>
      <webElementGuid>cfcd5c28-c0b0-46c2-a06f-f34f85248a56</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
