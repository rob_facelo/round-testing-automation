<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Copy Agency Address</name>
   <tag></tag>
   <elementGuidId>d649beca-942f-4483-a775-d4c95273685c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlBillingMailingInformation_RPC']/div[2]/div/div/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-xs</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1f2d5940-8927-4fcb-bac6-05692718d7ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-xs</value>
      <webElementGuid>22331e7d-9add-4c24-b577-c8deac6dd6da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>ac.copyAgencyAddressToBilling()</value>
      <webElementGuid>d988fb1a-68bd-43be-b5e3-03a132105f7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Copy Agency Address
                                            </value>
      <webElementGuid>9041da52-2cc0-4f43-896b-d503a8016d10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlBillingMailingInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row margin-bottom&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/button[@class=&quot;btn btn-primary btn-xs&quot;]</value>
      <webElementGuid>4a9499c6-4da5-4539-a226-a143e6b22486</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlBillingMailingInformation_RPC']/div[2]/div/div/div/button</value>
      <webElementGuid>559f6929-d61d-4a6a-8f44-85e1a9ba7bec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing - Mailing Information'])[1]/following::button[1]</value>
      <webElementGuid>c500e83e-da70-4ae7-a02c-e52780a41fb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact / Department'])[1]/preceding::button[1]</value>
      <webElementGuid>3bdaf522-7b75-47c3-a245-5170b5f119ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Copy Agency Address']/parent::*</value>
      <webElementGuid>9cbbe877-df97-414f-a0da-e2042d8b094e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div[2]/div/div/div/button</value>
      <webElementGuid>95c94b06-3489-4c6d-8c24-0280b074bc0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                                Copy Agency Address
                                            ' or . = '
                                                Copy Agency Address
                                            ')]</value>
      <webElementGuid>65a9dc8d-e5bc-4db3-bc04-8ede5b8570fc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
