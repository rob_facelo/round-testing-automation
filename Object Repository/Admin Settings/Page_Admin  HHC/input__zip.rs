<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__zip</name>
   <tag></tag>
   <elementGuidId>68414ce5-38c5-4577-b495-c4565f299d0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@name='zip'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-9 > autocomplete-directive.ng-isolate-scope > div.dropdown > input[name=&quot;zip&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f40bae4c-67f4-4d8f-9cf9-bcf9b9d0cfd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>07554237-933c-4cc3-896f-2d99a5517c6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>zip</value>
      <webElementGuid>8c422b66-6075-4a45-a797-ef79f499d1b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm autoCompleteInput font14 ng-pristine ng-untouched ng-valid-mask ng-valid-maxlength ng-valid ng-valid-required</value>
      <webElementGuid>1ced3d5d-67db-4d18-92f6-db3bc690a07f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>96e3bed5-3f4a-4f12-b017-6437fbad2fe5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ui-mask</name>
      <type>Main</type>
      <value>99999</value>
      <webElementGuid>96df10da-3778-4c01-b580-5b72b5e26f66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ui-mask-placeholder-char</name>
      <type>Main</type>
      <value>space</value>
      <webElementGuid>7511f36c-ab3e-4677-ac5f-1b47502f6cca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>model</value>
      <webElementGuid>5f7225a1-5054-40ee-b989-58bcca982e1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>90011</value>
      <webElementGuid>05f39137-24f1-41d3-9645-264468a56650</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>click-outside</name>
      <type>Main</type>
      <value>closeThis()</value>
      <webElementGuid>76712458-9a41-46bb-b9f8-ee58abcad4f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>searchValue(model,searchtype)</value>
      <webElementGuid>a4f24330-dfe8-44b5-912e-7d4c633f311d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-keydown</name>
      <type>Main</type>
      <value>selected=false</value>
      <webElementGuid>bc4c6224-2259-44aa-9528-b8390ddc0827</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 500}</value>
      <webElementGuid>9c2926d2-d76c-41c3-806b-ccbf9660475b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>disabled</value>
      <webElementGuid>8597084b-98f6-400c-bb16-df06855413b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>checkofselected()</value>
      <webElementGuid>d614e486-8ad0-4fe1-854b-a01b0489053c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>756fd66e-d2db-4b81-b1ec-2113f8e85cad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-required</name>
      <type>Main</type>
      <value>isrequired</value>
      <webElementGuid>eaadd386-6ba7-49a6-8a37-9d72d389f562</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>2c5da4f4-819a-4277-bd20-836dcdb532bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/div[@class=&quot;input-group&quot;]/div[@class=&quot;col-xs-9&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/input[@class=&quot;form-control input-sm autoCompleteInput font14 ng-pristine ng-untouched ng-valid-mask ng-valid-maxlength ng-valid ng-valid-required&quot;]</value>
      <webElementGuid>7d790717-a587-4960-933f-214d3d91f5ac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@name='zip'])[2]</value>
      <webElementGuid>ffc4f37d-0a2b-44d7-9758-10f687b8cb50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[8]/div[2]/div/div/autocomplete-directive/div/input</value>
      <webElementGuid>a5df21db-78ce-4c56-b3b2-10ed1bc9880d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/autocomplete-directive/div/input</value>
      <webElementGuid>25cd2d70-4c71-46ad-bed6-5b65b6ccefc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'zip']</value>
      <webElementGuid>411a2775-d422-4494-b6a6-43337cff7dea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
