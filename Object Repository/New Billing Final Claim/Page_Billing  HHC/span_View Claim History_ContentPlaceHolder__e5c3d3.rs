<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_View Claim History_ContentPlaceHolder__e5c3d3</name>
   <tag></tag>
   <elementGuidId>fd7cdd32-94fe-4e20-b609-ce42de5eb66e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchClaimsControl_grvBatchClaims_DXSelBtn3_D</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ContentPlaceHolder_BatchClaimsControl_grvBatchClaims_DXSelBtn3_D']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>80c81688-857c-4618-bafe-2a7751fabf28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxWeb_edtCheckBoxUnchecked_Moderno1 dxICheckBox_Moderno1 dxichSys</value>
      <webElementGuid>288d7d6f-bb6d-4f4b-91f8-5ecf137b8dfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_BatchClaimsControl_grvBatchClaims_DXSelBtn3_D</value>
      <webElementGuid>23024d82-c4d8-4d63-8a96-03cec39c3e3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchClaimsControl_grvBatchClaims_DXSelBtn3_D&quot;)</value>
      <webElementGuid>fd906a84-64d8-40a3-a9e8-9621dfa6515d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder_BatchClaimsControl_grvBatchClaims_DXSelBtn3_D']</value>
      <webElementGuid>249f5b42-e471-44ec-9331-d0b9c24bad77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_BatchClaimsControl_grvBatchClaims_DXDataRow3']/td/span</value>
      <webElementGuid>ad5deb98-f0ca-4bd2-b885-00458080714c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Claim History'])[3]/following::span[1]</value>
      <webElementGuid>58b4e67a-bcd1-4bc2-b52c-a566c32d9c06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Flag as Late Submission'])[3]/following::span[2]</value>
      <webElementGuid>1651ab04-11fe-4e6c-98bc-b530f8dd9be6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TAYLOR, ARSON'])[4]/preceding::span[2]</value>
      <webElementGuid>d61d3ff1-9da2-4f04-8434-69080e9a2fe1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Medicare'])[4]/preceding::span[2]</value>
      <webElementGuid>e788ac16-292a-4f4c-8af5-3a688124f774</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[6]/td/span</value>
      <webElementGuid>b38052eb-90d8-4333-997e-773b5a952900</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ContentPlaceHolder_BatchClaimsControl_grvBatchClaims_DXSelBtn3_D']</value>
      <webElementGuid>8f9b8d51-46fe-4a07-81f1-f0ad3dbb8b52</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
