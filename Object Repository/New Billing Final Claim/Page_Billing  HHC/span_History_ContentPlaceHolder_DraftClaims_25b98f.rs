<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_History_ContentPlaceHolder_DraftClaims_25b98f</name>
   <tag></tag>
   <elementGuidId>8a78dbd0-a247-4dc0-bfd8-ea2141367cac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_DraftClaimsControl_grvDraftClaim_DXSelBtn0_D</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ContentPlaceHolder_DraftClaimsControl_grvDraftClaim_DXSelBtn0_D']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d1e5aeec-6970-4b30-8ee5-79db4d1ef46e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxWeb_edtCheckBoxUnchecked_Moderno1 dxICheckBox_Moderno1 dxichSys</value>
      <webElementGuid>af9581ff-3725-4960-b1e2-957693cbefc6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_DraftClaimsControl_grvDraftClaim_DXSelBtn0_D</value>
      <webElementGuid>fcb8a369-8a6e-49af-99ab-47bed1fc4ee6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_DraftClaimsControl_grvDraftClaim_DXSelBtn0_D&quot;)</value>
      <webElementGuid>8606227d-1d28-454c-ae3b-8a6b07d4e4ca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder_DraftClaimsControl_grvDraftClaim_DXSelBtn0_D']</value>
      <webElementGuid>22c66f4d-e614-4231-9003-f005f921a304</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ContentPlaceHolder_DraftClaimsControl_grvDraftClaim_DXDataRow0']/td/span</value>
      <webElementGuid>9c34a0c5-d7c0-4c19-9f46-ccb4ea70fe0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='History'])[1]/following::span[2]</value>
      <webElementGuid>44813d62-165d-4459-8c7e-5648e1509479</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Non Billable'])[1]/following::span[3]</value>
      <webElementGuid>4ae997fc-e362-439c-8ed7-1460328b47b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Medicare'])[1]/preceding::span[2]</value>
      <webElementGuid>f264056b-dcdc-4c88-becb-5598dd98db21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Community Early'])[3]/preceding::span[2]</value>
      <webElementGuid>8b347894-140c-47ef-bd2b-bf369cfa9330</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td/span</value>
      <webElementGuid>7b844a85-086f-4ad2-8166-a8f8d26d8a74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ContentPlaceHolder_DraftClaimsControl_grvDraftClaim_DXSelBtn0_D']</value>
      <webElementGuid>835c99e8-6650-4130-bfea-33a51344dfd1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
