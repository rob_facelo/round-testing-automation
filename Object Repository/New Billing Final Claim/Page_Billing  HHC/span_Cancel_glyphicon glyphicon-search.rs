<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Cancel_glyphicon glyphicon-search</name>
   <tag></tag>
   <elementGuidId>6a7ec10f-c465-452a-9906-c87feb0f289b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-search</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_DraftClaimsControl_btnSearch_CD']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>26e85f8c-e091-483a-aa40-3583fb8b6d24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-search</value>
      <webElementGuid>52bc5301-83c7-4d1b-ab74-e45367b4ed4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_DraftClaimsControl_btnSearch_CD&quot;)/span[@class=&quot;glyphicon glyphicon-search&quot;]</value>
      <webElementGuid>fac34540-0ff5-4746-8030-cc86654d4b23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_DraftClaimsControl_btnSearch_CD']/span</value>
      <webElementGuid>85670e54-10a7-4f3b-a059-0c9017d667f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div/div/div/span</value>
      <webElementGuid>ef3c3633-e184-4a83-87cc-da6f8de058c5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
