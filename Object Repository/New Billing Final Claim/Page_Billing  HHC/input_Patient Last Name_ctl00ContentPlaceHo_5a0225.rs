<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Patient Last Name_ctl00ContentPlaceHo_5a0225</name>
   <tag></tag>
   <elementGuidId>58554ada-9901-41a4-b94b-ac99df5d7335</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>319402bb-4bb2-4425-9dd1-6afce9c897c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeEditArea dxeEditAreaSys</value>
      <webElementGuid>b7299838-b574-4f66-8e81-7416cf162535</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I</value>
      <webElementGuid>95f4aeaa-43bb-431b-85ea-d1fd1826d777</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolder$BatchClaimsControl$txtPatientLastName</value>
      <webElementGuid>14d5531d-2839-4698-b23a-4c1feaecbf7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onfocus</name>
      <type>Main</type>
      <value>ASPx.EGotFocus('ContentPlaceHolder_BatchClaimsControl_txtPatientLastName')</value>
      <webElementGuid>cd996052-ae19-4bda-ab32-43a5b6fecfae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>ASPx.ELostFocus('ContentPlaceHolder_BatchClaimsControl_txtPatientLastName')</value>
      <webElementGuid>cf30e2ec-268d-4cef-be59-348f5c9ba00d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ASPx.EValueChanged('ContentPlaceHolder_BatchClaimsControl_txtPatientLastName')</value>
      <webElementGuid>cd747344-9de2-4db8-bdaa-4875bc046bcc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Enter Patient Last Name</value>
      <webElementGuid>0e204861-6933-4bb5-9b45-dbd2bbe76584</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>8496e9c2-0647-4541-8d14-505965dab969</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>684b95ca-83bd-4e08-a60a-d5cd305f5b16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I&quot;)</value>
      <webElementGuid>5f513e25-d14c-44af-9335-2bd2b29a375a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I']</value>
      <webElementGuid>a39a28e7-aa06-41d1-b48d-c12e3265e5a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_BatchClaimsControl_txtPatientLastName']/tbody/tr/td/input</value>
      <webElementGuid>c4224ffa-9eb8-43c8-b231-fe95f203631d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/table/tbody/tr/td/input</value>
      <webElementGuid>5c32344a-6910-4553-b5ce-6e0de6cd2515</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder_BatchClaimsControl_txtPatientLastName_I' and @name = 'ctl00$ContentPlaceHolder$BatchClaimsControl$txtPatientLastName' and @type = 'text']</value>
      <webElementGuid>3be441b4-26ca-40cb-8e48-6adceca36f09</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
