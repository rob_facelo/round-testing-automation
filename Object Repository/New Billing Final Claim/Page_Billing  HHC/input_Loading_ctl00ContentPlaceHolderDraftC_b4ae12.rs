<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Loading_ctl00ContentPlaceHolderDraftC_b4ae12</name>
   <tag></tag>
   <elementGuidId>7a4295f4-281a-4c69-a575-41b15cfd07df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_DraftClaimsControl_dteFromDate_I</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder_DraftClaimsControl_dteFromDate_I']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f793a0ec-109d-40af-9393-83fcb939ac15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeEditArea dxeEditAreaSys dxh0</value>
      <webElementGuid>2543304d-a79b-40ed-9dec-fa1d7811f792</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_DraftClaimsControl_dteFromDate_I</value>
      <webElementGuid>3e0226fc-c552-4492-8f09-858383d8d221</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate</value>
      <webElementGuid>576cb16b-e777-49a2-883a-b9288c71826e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onfocus</name>
      <type>Main</type>
      <value>ASPx.EGotFocus('ContentPlaceHolder_DraftClaimsControl_dteFromDate')</value>
      <webElementGuid>7815546e-01dd-4cf4-9876-67d44b401c33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>ASPx.ELostFocus('ContentPlaceHolder_DraftClaimsControl_dteFromDate')</value>
      <webElementGuid>0424cd19-ecee-4af4-908a-535105935268</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ASPx.ETextChanged('ContentPlaceHolder_DraftClaimsControl_dteFromDate')</value>
      <webElementGuid>9c27e6da-23c4-44da-bb7b-8c69cb9b15a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>12/30/2022</value>
      <webElementGuid>6b843e70-f6f5-440d-ae66-d55c737875d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>7fb9fa25-63e3-487c-b130-a6271d2f9098</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>76090b6f-2d78-4ce2-9ac4-3787325b9765</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_I&quot;)</value>
      <webElementGuid>2a4c65ba-5603-4480-9a5e-3c03ca9e24c8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder_DraftClaimsControl_dteFromDate_I']</value>
      <webElementGuid>7f146b19-bfa4-4496-9aeb-d093aaeea488</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_DraftClaimsControl_dteFromDate']/tbody/tr/td/input</value>
      <webElementGuid>d13047c6-d766-4665-8266-4522fb044523</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/table[2]/tbody/tr/td/input</value>
      <webElementGuid>af8c7a4c-b464-4e96-99bb-692921c4a302</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder_DraftClaimsControl_dteFromDate_I' and @name = 'ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate' and @type = 'text']</value>
      <webElementGuid>110beb32-f2c5-4b56-95bd-407a65097f8a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
