<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Navigate to New layout</name>
   <tag></tag>
   <elementGuidId>4858733a-6211-4740-9faf-060e78207239</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_DraftClaimsControl_lnkNavigateTemplate</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_DraftClaimsControl_lnkNavigateTemplate']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7abc0e00-8928-4f77-91da-42565f26eda0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>NavigateTemplate_Click_JS();</value>
      <webElementGuid>0fb97cdb-8a10-465b-b7e6-f160a91c95d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_DraftClaimsControl_lnkNavigateTemplate</value>
      <webElementGuid>c8b0d1b8-7bc6-4b00-9950-e7ea042098fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:__doPostBack('ctl00$ContentPlaceHolder$DraftClaimsControl$lnkNavigateTemplate','')</value>
      <webElementGuid>6ae43961-9ff7-453a-80d5-92536db561dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Navigate to New layout</value>
      <webElementGuid>cc9b628c-bd67-4fda-b1c1-48d90aab4109</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_DraftClaimsControl_lnkNavigateTemplate&quot;)</value>
      <webElementGuid>0fb0b7f1-f212-4d18-a469-08e330d771d6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_DraftClaimsControl_lnkNavigateTemplate']</value>
      <webElementGuid>db4f470d-835b-41f2-b2d1-aea86a3b90de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='angularElement']/div/div/div/div[2]/a</value>
      <webElementGuid>696062fc-8b41-4c72-8877-b374331d3474</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Navigate to New layout')]</value>
      <webElementGuid>873f7cee-c146-44a4-85d6-86af67003ecd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Undrafted Bills -'])[1]/following::a[1]</value>
      <webElementGuid>6cc8d7e5-cbe0-4fd0-b312-dd45a56e5579</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Medicare Interactive Access EDS Data Center (ca-TPX)'])[2]/following::a[1]</value>
      <webElementGuid>aaa23ca6-42d3-4ee6-9663-bc72436699b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Type:'])[1]/preceding::a[1]</value>
      <webElementGuid>6bbe2657-db63-4208-98f2-f96ade51de0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[3]/preceding::a[1]</value>
      <webElementGuid>be75fa91-6b8d-49ac-8bb7-bfb36ca28b40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Navigate to New layout']/parent::*</value>
      <webElementGuid>956db661-9f3a-4371-89df-50e4d94e8a49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:__doPostBack('ctl00$ContentPlaceHolder$DraftClaimsControl$lnkNavigateTemplate','')&quot;)]</value>
      <webElementGuid>b6f79468-4fea-4abd-b859-9337fa0ee544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]/a</value>
      <webElementGuid>7ff4564d-1ce3-4c65-8873-191fd976cbb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'ContentPlaceHolder_DraftClaimsControl_lnkNavigateTemplate' and @href = concat(&quot;javascript:__doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$lnkNavigateTemplate&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Navigate to New layout' or . = 'Navigate to New layout')]</value>
      <webElementGuid>5f6ebfa6-7cbe-445c-bcd4-b34405d0c8e8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
