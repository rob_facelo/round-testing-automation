<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_</name>
   <tag></tag>
   <elementGuidId>cb90a16c-066e-4e2a-8f14-f2ad4f3d15e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#HomeContent > div.modal-dialog.modal-lg > div.modal-content > div.modal-header > button.close > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='HomeContent']/div/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9ece194c-d52d-4490-ac39-d0a7c60f0e5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>ec38bca2-f50c-41c5-83a4-4659e672b95e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>×</value>
      <webElementGuid>04bde09b-616e-4448-b274-df270c81472c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;HomeContent&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]/button[@class=&quot;close&quot;]/span[1]</value>
      <webElementGuid>6b49e574-a257-4e00-97b3-d4cd3514a3bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HomeContent']/div/div/div/button/span</value>
      <webElementGuid>73c8ddca-de60-4de0-bf88-971054ddef04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[3]/following::span[1]</value>
      <webElementGuid>389eb2f8-6aad-43fb-890f-98003fc9f19c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Printer Friendly'])[1]/following::span[1]</value>
      <webElementGuid>34582f6f-5a3a-4992-ab83-9da8e57b0c3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CENTRE'])[1]/preceding::span[2]</value>
      <webElementGuid>623c0471-54e1-4125-b1a5-3696f354fa33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/button/span</value>
      <webElementGuid>b54193fe-c446-410f-a7c7-bbad6aef72b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '×' or . = '×')]</value>
      <webElementGuid>22e2cb51-97f0-49f7-95d8-f4dd5a5a1cb2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
