<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Print Batch Payroll</name>
   <tag></tag>
   <elementGuidId>25f62eb9-74c3-490a-9e5c-d23a075bf683</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i2_T']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#applicationLeftNav_LeftPanel_nbLeftNavigation_I0i2_T > span.dx-vam.dx-wrap</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>08a4cc6f-558b-4141-afec-97365a207360</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam dx-wrap</value>
      <webElementGuid>e12a60c0-eec7-48f7-a4c4-43dfb8c7f956</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Print Batch Payroll</value>
      <webElementGuid>fe4288c0-daff-485f-be77-bae706f3d16f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;applicationLeftNav_LeftPanel_nbLeftNavigation_I0i2_T&quot;)/span[@class=&quot;dx-vam dx-wrap&quot;]</value>
      <webElementGuid>02cd7044-156f-42ab-a529-65420281e373</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i2_T']/span</value>
      <webElementGuid>c7c7fa59-acad-4464-b1f4-1f2f895302ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 2: Generated Payroll'])[1]/following::span[1]</value>
      <webElementGuid>e4cf516d-af2a-42a1-82ea-e4a96229b89a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 1: Generate Pay Visits'])[1]/following::span[2]</value>
      <webElementGuid>595f77ea-f1cc-4ad5-93d9-2c51d409931f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Look up By Check'])[1]/preceding::span[1]</value>
      <webElementGuid>143d9b19-46e5-4dbc-8e2c-68e610f60423</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Generated Payrolls'])[1]/preceding::span[2]</value>
      <webElementGuid>2ca2eb12-f06c-43b4-8751-3e37df45af6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Print Batch Payroll']/parent::*</value>
      <webElementGuid>189900e6-e006-4a5f-b9f6-667b44a38f1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/ul/li[3]/a/span</value>
      <webElementGuid>698e8914-1078-44ac-bee0-f5bda237c2e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Print Batch Payroll' or . = 'Print Batch Payroll')]</value>
      <webElementGuid>46bafa03-baac-4fb5-a3bb-af8045fab9bf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
