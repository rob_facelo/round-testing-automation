<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_To                                     _5eb9ea</name>
   <tag></tag>
   <elementGuidId>e1fafaeb-ecca-4777-bc0a-1c88f1a90f4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_RPC']/div[2]/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>67e09de4-0911-4ac1-ae6f-6218f41b2606</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>946f35b7-6b09-4ad9-a5cb-af8773161779</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                            To:
                                                        
                                                        
                                                            
				
					Loading…
				
			
				
					
				
			
				
					
						
							
								
									
								
							
								
									
										
											
												
													September 2022
												
											
										
											
												
													SunMonTueWedThuFriSat
												
													3528293031123
												
													3645678910
												
													3711121314151617
												
													3818192021222324
												
													392526272829301
												
													402345678
												
											
										
									
								
									
										
											TodayClear
										
									
								
							
								
									
										
											
												
													
														
															JanFebMarApr
														
															MayJunJulAug
														
															SepOctNovDec
														
													
												
													
														
															
														
															
														
													
												
											
												
													
														OKCancel
													
												
											
										
									
								
							
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP');
dxo.InitGlobalVariable('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD$C$FNP';
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='fade';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C',[[['dxeCalendarButtonHover'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[['dxeCalendarFastNavMonthHover'],[''],['FNP_M0','FNP_M1','FNP_M2','FNP_M3','FNP_M4','FNP_M5','FNP_M6','FNP_M7','FNP_M8','FNP_M9','FNP_M10','FNP_M11']],[['dxeCalendarFastNavYearHover'],[''],['FNP_Y0','FNP_Y1','FNP_Y2','FNP_Y3','FNP_Y4','FNP_Y5','FNP_Y6','FNP_Y7','FNP_Y8','FNP_Y9']]]);
ASPx.AddPressedItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C',[[['dxeCalendarButtonPressed'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C',[[['dxeDisabled'],[''],['']],[['dxeDisabled dxeButtonDisabled'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[[''],[''],['PYC','PMC','NMC','NYC'],,[[{'spriteCssClass':'dxEditors_edtCalendarPrevYearDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarPrevMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextYearDisabled'}]],['Img']]]);

var dxo = new ASPxClientCalendar('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C');
dxo.InitGlobalVariable('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD$C';
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.serverCurrentDate=new Date(2022,8,21);
dxo.visibleDate = new Date(2022,8,21);
dxo.selection.AddArray([new Date(2022,8,21,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

						
					
				
			
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD');
dxo.InitGlobalVariable('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD';
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate', e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='slide';
dxo.closeAction='CloseButton';
dxo.popupHorizontalAlign='LeftSides';
dxo.popupVerticalAlign='Below';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate',[[['dxeButtonEditButtonHover'],[''],['B-1']]]);
ASPx.RemoveHoverItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate',[[['B-100']]]);
ASPx.AddPressedItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate',[[['dxeButtonEditButtonPressed'],[''],['B-1']]]);
ASPx.RemovePressedItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate',[[['B-100']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate',[[['dxeDisabled'],[''],['','I']],[['dxeDisabled dxeButtonDisabled'],[''],['B-1'],,[[{'spriteCssClass':'dxEditors_edtDropDownDisabled'}]],['Img']]]);
ASPx.RemoveDisabledItems('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate',[[['B-100'],]]);
document.getElementById(&quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate');
dxo.InitGlobalVariable('dteToDate');
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate',arg,ASPx.Callback,'ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate',ASPx.CallbackError,true); };
dxo.uniqueID = 'ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate';
dxo.stateObject = ({'rawValue':'1663718400000'});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.maskInfo = ASPx.MaskInfo.Create('MM/dd/yyyy',true);
dxo.displayFormat='{0:MM/dd/yyyy}';
dxo.outOfRangeWarningClassName='dxeOutOfRWarn dxeOutOfRWarnRight';
dxo.outOfRangeWarningMessages=['The date must be in the range {0}...{1}', 'The date must be greater than or equal to {0}', 'The date must be less than or equal to {0}'];
dxo.date = new Date(2022,8,21);
dxo.dateFormatter = ASPx.DateFormatter.Create('MM/dd/yyyy');
dxo.AfterCreate();

//-->

                                                        
                                                    </value>
      <webElementGuid>152ede85-1d13-4a9f-a1bc-61874855d04e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>aedaad79-241b-43c3-9418-469d72e72860</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_RPC']/div[2]/div[2]/div</value>
      <webElementGuid>5e0ffb32-d009-4c74-83d7-6ac5843c5dd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[2]/following::div[2]</value>
      <webElementGuid>306c31ba-e04c-470e-a285-6d4539fa4747</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/following::div[2]</value>
      <webElementGuid>0d6b9375-0324-48f6-a780-ed8cad2fb8ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div[2]/div[2]/div</value>
      <webElementGuid>74af2051-dde4-4be0-9b04-61eaaabe62bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                                                        
                                                            To:
                                                        
                                                        
                                                            
				
					Loading…
				
			
				
					
				
			
				
					
						
							
								
									
								
							
								
									
										
											
												
													September 2022
												
											
										
											
												
													SunMonTueWedThuFriSat
												
													3528293031123
												
													3645678910
												
													3711121314151617
												
													3818192021222324
												
													392526272829301
												
													402345678
												
											
										
									
								
									
										
											TodayClear
										
									
								
							
								
									
										
											
												
													
														
															JanFebMarApr
														
															MayJunJulAug
														
															SepOctNovDec
														
													
												
													
														
															
														
															
														
													
												
											
												
													
														OKCancel
													
												
											
										
									
								
							
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,8,21);
dxo.visibleDate = new Date(2022,8,21);
dxo.selection.AddArray([new Date(2022,8,21,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

						
					
				
			
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;I&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtDropDownDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;],]]);
document.getElementById(&quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;dteToDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1663718400000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.maskInfo = ASPx.MaskInfo.Create(&quot; , &quot;'&quot; , &quot;MM/dd/yyyy&quot; , &quot;'&quot; , &quot;,true);
dxo.displayFormat=&quot; , &quot;'&quot; , &quot;{0:MM/dd/yyyy}&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2022,8,21);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;MM/dd/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                                        
                                                    &quot;) or . = concat(&quot;
                                                        
                                                            To:
                                                        
                                                        
                                                            
				
					Loading…
				
			
				
					
				
			
				
					
						
							
								
									
								
							
								
									
										
											
												
													September 2022
												
											
										
											
												
													SunMonTueWedThuFriSat
												
													3528293031123
												
													3645678910
												
													3711121314151617
												
													3818192021222324
												
													392526272829301
												
													402345678
												
											
										
									
								
									
										
											TodayClear
										
									
								
							
								
									
										
											
												
													
														
															JanFebMarApr
														
															MayJunJulAug
														
															SepOctNovDec
														
													
												
													
														
															
														
															
														
													
												
											
												
													
														OKCancel
													
												
											
										
									
								
							
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,8,21);
dxo.visibleDate = new Date(2022,8,21);
dxo.selection.AddArray([new Date(2022,8,21,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

						
					
				
			
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;I&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtDropDownDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;],]]);
document.getElementById(&quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;dteToDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$GeneratedPayrollControl$pnlGeneratedPayrollDate$dteToDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1663718400000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.maskInfo = ASPx.MaskInfo.Create(&quot; , &quot;'&quot; , &quot;MM/dd/yyyy&quot; , &quot;'&quot; , &quot;,true);
dxo.displayFormat=&quot; , &quot;'&quot; , &quot;{0:MM/dd/yyyy}&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2022,8,21);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;MM/dd/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                                        
                                                    &quot;))]</value>
      <webElementGuid>5bd56f16-492a-4835-8bcf-3bfb3f2afbd8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
