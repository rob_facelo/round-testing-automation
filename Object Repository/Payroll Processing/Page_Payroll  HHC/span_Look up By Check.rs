<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Look up By Check</name>
   <tag></tag>
   <elementGuidId>6bd0b004-fcb3-421c-80c7-52edf5bf8a41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i3_T']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#applicationLeftNav_LeftPanel_nbLeftNavigation_I0i3_T > span.dx-vam.dx-wrap</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>451da28d-9596-4bb4-b5cd-9a6c2ad2ec0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam dx-wrap</value>
      <webElementGuid>75cd0d22-9a27-4b7a-b127-970755081f19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Look up By Check</value>
      <webElementGuid>8cff72e4-b065-4bd8-9585-1030d7370a4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;applicationLeftNav_LeftPanel_nbLeftNavigation_I0i3_T&quot;)/span[@class=&quot;dx-vam dx-wrap&quot;]</value>
      <webElementGuid>8c8ccfda-92a5-48c0-96ab-5a6945838b1b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i3_T']/span</value>
      <webElementGuid>d3020c35-eb59-4c61-abc0-b35cf9d93608</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print Batch Payroll'])[1]/following::span[1]</value>
      <webElementGuid>25edfab6-7434-49fd-bd35-5c7941e759ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 2: Generated Payroll'])[1]/following::span[2]</value>
      <webElementGuid>313855ed-15a3-4a80-8b8c-0cb008361a02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print Batch Payroll'])[2]/preceding::span[1]</value>
      <webElementGuid>a16b75ca-0a7d-4253-9e0b-4d91904b1596</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payroll Date:'])[1]/preceding::span[1]</value>
      <webElementGuid>a331f568-9301-4164-80d1-67ffef33d069</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Look up By Check']/parent::*</value>
      <webElementGuid>783c2bbd-b5e0-4ff8-8f40-69a3b7ee5b77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a/span</value>
      <webElementGuid>2c8f6965-220b-4397-9794-84c25b1326a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Look up By Check' or . = 'Look up By Check')]</value>
      <webElementGuid>8cd682b2-6662-4e2d-8b2c-7de1e3bcdaef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
