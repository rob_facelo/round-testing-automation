<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Acosta, Casey SN_1_2</name>
   <tag></tag>
   <elementGuidId>bda85eb3-550d-4c76-8bc4-33a91b86b79e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_PayrollCheckControl_ddlSearchByCarestaff_DDD_L_LBI0T0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PayrollCheckControl_ddlSearchByCarestaff_DDD_L_LBI0T0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>f2af2004-661e-46e2-a955-a665a5185ec6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_PayrollCheckControl_ddlSearchByCarestaff_DDD_L_LBI0T0</value>
      <webElementGuid>2403cfb3-5c4a-4b72-8a7c-d42c3adb23ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeListBoxItem dxeLTM dxeListBoxItemHover dxeListBoxItemSelected</value>
      <webElementGuid>7a8c8bab-bef0-4344-bb57-8a539dbf0d5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Acosta, Casey SN</value>
      <webElementGuid>d0a3c0c0-bd54-4371-93ef-14c90c7b03cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PayrollCheckControl_ddlSearchByCarestaff_DDD_L_LBI0T0&quot;)</value>
      <webElementGuid>008b7470-a166-4130-85c1-bce3165d0221</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_PayrollCheckControl_ddlSearchByCarestaff_DDD_L_LBI0T0']</value>
      <webElementGuid>08443daa-0209-4c06-987b-c690acb49ff1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_PayrollCheckControl_ddlSearchByCarestaff_DDD_L_LBT']/tbody/tr/td</value>
      <webElementGuid>52b2f82d-00cb-4f25-b84e-c1d42d41546a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::td[2]</value>
      <webElementGuid>03ff3f9b-23e1-4c59-88c8-bac628fb4017</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[4]/following::td[7]</value>
      <webElementGuid>c16cf9c5-f426-4c7f-8f74-3275990bfd30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='walker, sarah SN'])[1]/preceding::td[1]</value>
      <webElementGuid>49c34865-8198-4b4d-bc3d-9060b755f599</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[5]/preceding::td[3]</value>
      <webElementGuid>2b277262-2245-4123-879b-700120d503ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Acosta, Casey SN']/parent::*</value>
      <webElementGuid>b99efde4-b2d8-4fd8-828a-64eb0ba67dfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div[2]/table[2]/tbody/tr/td</value>
      <webElementGuid>0afb69a6-7a2c-46ef-a8d3-c2da12ae6860</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_PayrollCheckControl_ddlSearchByCarestaff_DDD_L_LBI0T0' and (text() = 'Acosta, Casey SN' or . = 'Acosta, Casey SN')]</value>
      <webElementGuid>d0b26168-352e-4c30-8891-f5dc37730534</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
