<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Look up by Check search</name>
   <tag></tag>
   <elementGuidId>029a0805-74a4-45ff-862b-2d285c95cb4b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_PayrollCheckControl_btnSearchCheck_CD']/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PayrollCheckControl_btnSearchCheck_CD > span.dx-vam.dx-wrap</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2cd27d98-235f-4809-aac8-a8354fdc7b0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam dx-wrap</value>
      <webElementGuid>bc3e8f8c-0871-4f21-bd71-fbf950848a38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Search</value>
      <webElementGuid>d679f40c-ad15-4549-8afd-e61a5882e811</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PayrollCheckControl_btnSearchCheck_CD&quot;)/span[@class=&quot;dx-vam dx-wrap&quot;]</value>
      <webElementGuid>f76e4e6f-88c8-42b3-bf21-c222cf916b8b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_PayrollCheckControl_btnSearchCheck_CD']/span[2]</value>
      <webElementGuid>ceadbe9e-9440-441c-87f4-638d07aa9618</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[3]/following::span[2]</value>
      <webElementGuid>31e92ea1-6211-4f93-940c-1e835994059f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[2]/following::span[2]</value>
      <webElementGuid>7924dd9c-feca-4118-8282-ab7f93f4da2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Caregiver Name'])[1]/preceding::span[1]</value>
      <webElementGuid>7b0cfaf5-4862-4cc4-bfdf-1d37c87d93bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check Number'])[3]/preceding::span[2]</value>
      <webElementGuid>5056b4bb-320b-4830-b8ff-45afb2691bec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span[2]</value>
      <webElementGuid>bb047fe1-eec6-4828-8bca-0943c3ece164</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Search' or . = 'Search')]</value>
      <webElementGuid>44d3595b-0438-403f-965d-05707696cf00</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
