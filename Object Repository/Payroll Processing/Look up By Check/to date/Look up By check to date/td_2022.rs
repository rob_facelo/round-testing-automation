<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_2022</name>
   <tag></tag>
   <elementGuidId>a68d2fe2-0ce4-4ca6-ab36-bb3bdbc84958</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_Y2']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_Y2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>b0aaf44f-d263-4bc5-b299-eaa7dd318fed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeCalendarFastNavYear dxeCalendarFastNavYearSelected dxeCalendarFastNavYearHover</value>
      <webElementGuid>2572d635-bc52-4b8c-9e6c-7804d850559f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_Y2</value>
      <webElementGuid>cf778f47-e12e-4afe-966f-fb15031610af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2022</value>
      <webElementGuid>44744863-f7ad-4564-9e42-87eb3f9581a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_Y2&quot;)</value>
      <webElementGuid>0fdc8fdf-55ac-4bfb-a62c-5dcf8556be4c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_Y2']</value>
      <webElementGuid>462bdc2b-20be-4b01-ab98-c743c5ff765c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_y']/tbody/tr/td[4]</value>
      <webElementGuid>fb70ddd3-1a3e-4748-9c19-914823c514e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dec'])[2]/following::td[4]</value>
      <webElementGuid>713c1c87-b093-4544-b520-a8064b2438a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nov'])[2]/following::td[5]</value>
      <webElementGuid>e8701a6b-8fd0-4410-a647-838e032ecaa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[2]/preceding::td[9]</value>
      <webElementGuid>ae14e49d-8d0d-4d37-aae9-f8b1438579b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[3]/preceding::td[11]</value>
      <webElementGuid>009ed0b7-e9a8-4929-a84e-5bc22c921929</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='2022']/parent::*</value>
      <webElementGuid>e8bf92c4-0e53-4772-a6ce-ec648b779eef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/div/div/div/div/div/div/div/div[2]/table/tbody/tr/td[4]</value>
      <webElementGuid>52e6cf91-6d69-4433-a625-4bc59abb2211</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_Y2' and (text() = '2022' or . = '2022')]</value>
      <webElementGuid>584cbdbf-e864-41cb-b255-0b2325b9996a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
