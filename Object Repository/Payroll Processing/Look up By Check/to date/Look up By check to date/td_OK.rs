<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_OK</name>
   <tag></tag>
   <elementGuidId>97404cba-7a41-46b1-83ad-37dd806262da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_BO']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_BO</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>1a5ea7e2-e8f7-4fe7-b91d-965c6daf4cb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_BO</value>
      <webElementGuid>ccff502e-9680-45e7-9d83-308ee019c977</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeCalendarButton dxeCalendarButtonHover</value>
      <webElementGuid>2a1943dd-1b53-4050-bad9-ee5a4d154aca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>ASPx.CalFNBClick('ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C', 'ok')</value>
      <webElementGuid>52ab6197-a90c-4fba-906e-07f537d1f41e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>cbf45578-7b97-4040-a11f-12762ae2eae7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_BO&quot;)</value>
      <webElementGuid>2f8bb359-aa24-4525-a61c-8ec2b696f8fc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_BO']</value>
      <webElementGuid>2917649f-cbcd-4547-82c7-083ff0cc0135</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_PWC-1']/div[2]/table/tbody/tr/td</value>
      <webElementGuid>44e517dc-e354-40d3-950e-c7cb60361438</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dec'])[2]/following::td[13]</value>
      <webElementGuid>292547ed-fd37-41c3-9091-529ac518c93e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nov'])[2]/following::td[14]</value>
      <webElementGuid>fd5f2295-0eb1-4204-b0c9-de915a61609c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[3]/preceding::td[2]</value>
      <webElementGuid>9125d77a-bb66-4ed6-8afe-07340ec7e72b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[2]/preceding::td[3]</value>
      <webElementGuid>cb7b5118-ca6c-4fe9-aa35-68f226214745</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/div/div/div/div/div/div/div[2]/table/tbody/tr/td</value>
      <webElementGuid>b0e42537-d8d2-42c7-b4e1-3a68342fbba0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_PayrollCheckControl_pnlCheckDate_dteToDate_DDD_C_FNP_BO' and (text() = 'OK' or . = 'OK')]</value>
      <webElementGuid>e57b115a-22c6-4726-a832-a0ae349ef05b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
