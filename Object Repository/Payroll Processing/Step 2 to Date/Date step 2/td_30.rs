<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_30</name>
   <tag></tag>
   <elementGuidId>38628e5d-9155-4a06-a79d-80d1f7c8cbfe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_mt']/tbody/tr[6]/td[7]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.dxeCalendarDay.dxeCalendarSelected</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>2c4efdc3-82d2-4734-9d1d-2e3eae1a0173</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeCalendarDay dxeCalendarSelected</value>
      <webElementGuid>f73975c4-0ffa-4ff9-b9ca-81d2f5a74258</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>savedcursor</name>
      <type>Main</type>
      <value>[object Object]</value>
      <webElementGuid>a8cf12a4-2472-4672-8821-05b211037aad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>30</value>
      <webElementGuid>bf87d9e1-5bbe-4134-b1f1-f771d5fdf094</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_mt&quot;)/tbody[1]/tr[6]/td[@class=&quot;dxeCalendarDay dxeCalendarSelected&quot;]</value>
      <webElementGuid>2fe0773a-1be7-4acd-8c51-24bf93ff42ca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_mt']/tbody/tr[6]/td[7]</value>
      <webElementGuid>c1e09569-23c7-45de-90c0-21b55686f20b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sat'])[2]/following::td[39]</value>
      <webElementGuid>ad0c1609-86bf-4389-8511-e4a5180d038e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fri'])[2]/following::td[40]</value>
      <webElementGuid>13db96cb-78a9-45d3-9416-e62cc1d8948a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Today'])[2]/preceding::td[10]</value>
      <webElementGuid>b96ffeb5-cd6c-41ea-a2da-92f7046d8996</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear'])[2]/preceding::td[12]</value>
      <webElementGuid>7594ad0c-9e16-4132-8c97-d81e35ddde86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[6]/td[7]</value>
      <webElementGuid>f89c0090-616d-4ca8-babc-f6b112f14692</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '30' or . = '30')]</value>
      <webElementGuid>459510f7-aa37-405d-9876-663b5017d2bf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
