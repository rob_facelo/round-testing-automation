<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Date icon</name>
   <tag></tag>
   <elementGuidId>70d79e8e-a806-4088-ae46-a894cb1a747a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_B-1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_B-1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>4b76dc58-aee2-44b9-ac2e-f49326bf2ba4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_B-1</value>
      <webElementGuid>d0b40240-cbbe-4c06-b0d2-0243503ab577</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeButton dxeButtonEditButton dxeButtonEditButtonHover</value>
      <webElementGuid>3e887bd8-5841-4d22-9c85-67d329c33806</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onmousedown</name>
      <type>Main</type>
      <value>return ASPx.DDDropDown('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate', event)</value>
      <webElementGuid>3c7df1fa-e76e-4e18-ad88-c79832ebd9ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_B-1&quot;)</value>
      <webElementGuid>7274aed1-8867-439d-b60e-58af11a6df62</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_B-1']</value>
      <webElementGuid>6c9ed1e2-cdc3-4be5-9082-0073fe518bcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate']/tbody/tr/td[2]</value>
      <webElementGuid>dcc006dc-30df-44f8-b716-5e948515d711</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[6]/following::td[2]</value>
      <webElementGuid>a817acca-90b3-4142-9f35-54e7bff8395a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='To:'])[1]/following::td[4]</value>
      <webElementGuid>4e9716fa-38e8-4d86-b7ee-b12f31868b2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='September 2022'])[2]/preceding::td[16]</value>
      <webElementGuid>953e14c0-99e1-4066-b3f9-d61e3a618f35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sun'])[2]/preceding::td[22]</value>
      <webElementGuid>230983d7-1c4e-4afb-88f1-bc226dc6d322</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div[2]/div[2]/div/div[2]/table[2]/tbody/tr/td[2]</value>
      <webElementGuid>fd2e1f56-2af5-476d-bf49-87eebebd635a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_B-1']</value>
      <webElementGuid>161c95a5-5ff5-4141-9af0-30235b4a3e17</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
