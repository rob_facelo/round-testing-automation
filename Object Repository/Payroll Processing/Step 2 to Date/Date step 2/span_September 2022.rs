<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_September 2022</name>
   <tag></tag>
   <elementGuidId>a5d9a4db-d8f9-4872-8cf3-463c265b025b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_T']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_T</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2725cf9d-3329-4275-a6d8-e1f46341c420</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_T</value>
      <webElementGuid>a9336bc0-ebfc-460c-9fca-b2d86d41a25d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>ASPx.CalTitleClick('ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C', 0, 0)</value>
      <webElementGuid>cde15f29-05de-42d8-a93d-f80af3d463f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>September 2022</value>
      <webElementGuid>737b8579-2e19-4372-acc8-06583d670839</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_T&quot;)</value>
      <webElementGuid>e5801c5a-b7f3-4183-9b67-adf2ca966fe1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_T']</value>
      <webElementGuid>2bcecedd-fa68-4bb4-97be-f6bb1848a8ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_TC']/span</value>
      <webElementGuid>2f6ce59a-5dfc-436f-b486-b2428e0f6c24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[6]/following::span[1]</value>
      <webElementGuid>90004f77-8ce1-494e-95b7-d2c49810f0a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='To:'])[1]/following::span[2]</value>
      <webElementGuid>1f98a3b9-a997-4829-bf93-02329dc60617</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sun'])[2]/preceding::span[1]</value>
      <webElementGuid>24481138-a90c-4200-a566-f69c9062a490</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mon'])[2]/preceding::span[1]</value>
      <webElementGuid>a43678f4-8365-4a44-a4f4-3f97c934da5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/div/div/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[4]/span</value>
      <webElementGuid>a9780b63-bbe9-45cb-8b6d-8b5ddd9930e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ContentPlaceHolder_GeneratedPayrollControl_pnlGeneratedPayrollDate_dteToDate_DDD_C_T' and (text() = 'September 2022' or . = 'September 2022')]</value>
      <webElementGuid>35419ec2-d932-4831-9e3f-ecd0d6fe5bf9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
