<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Batch Tools</name>
   <tag></tag>
   <elementGuidId>12b3645a-992e-44d8-b15c-37253d527144</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='headerMenu_DXI3_T']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#headerMenu_DXI3_T</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ec39ec75-8a59-4f71-be17-0219d09c3c70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxm-content dxm-hasText dx </value>
      <webElementGuid>eef95b79-7c02-43ba-9f74-37e3665a4c1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(3)</value>
      <webElementGuid>b1b03154-49a1-45a9-a2b1-27218eeb01e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>headerMenu_DXI3_T</value>
      <webElementGuid>9e1b75d0-07f0-4bd6-a678-927ce58f71d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Batch Tools</value>
      <webElementGuid>f584aa38-8eb4-49c9-9e83-8deff6adbfd0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;headerMenu_DXI3_T&quot;)</value>
      <webElementGuid>b3b8abd4-0c19-44e7-8115-3c1e6556f7a1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='headerMenu_DXI3_T']</value>
      <webElementGuid>f326b2bb-a7f3-4269-8632-d91cc4ee85d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='headerMenu_DXI3_']/a</value>
      <webElementGuid>9767ba8a-3056-45af-99ae-20a98cc5a940</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient'])[2]/following::a[1]</value>
      <webElementGuid>0257ac75-28e8-4695-84c9-5eb7b5514d64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Admin'])[1]/following::a[2]</value>
      <webElementGuid>f0250f61-91c5-4c93-883f-886c298626da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reports'])[1]/preceding::a[1]</value>
      <webElementGuid>f81363c7-1f88-47c3-b46d-f9834f644d17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:void(3)')]</value>
      <webElementGuid>338fa8d8-aa1f-4aa8-a751-26500dcd50fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/ul/li[7]/a</value>
      <webElementGuid>8d4f653f-7d0d-44d2-b5a7-3b22b2e76b07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(3)' and @id = 'headerMenu_DXI3_T' and (text() = 'Batch Tools' or . = 'Batch Tools')]</value>
      <webElementGuid>fc3d32ec-37b3-4772-a964-b3eff5b28120</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
