<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_STYLES, FURIOUS</name>
   <tag></tag>
   <elementGuidId>7b1e4992-37d1-446b-97d9-6bf727e5e06a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='widgetholder']/section/div[4]/div/div/div[2]/ul/li/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.list-group-item-heading.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>815b4a20-0f50-44cf-b373-914e9ea8eeec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item-heading ng-binding</value>
      <webElementGuid>1e56c886-1bee-4914-93c4-565766daaa7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>STYLES, FURIOUS </value>
      <webElementGuid>10f23dd2-6fd2-4dbd-9e38-d9236e8cca4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;widgetholder&quot;)/section[@class=&quot;ng-scope&quot;]/div[@class=&quot;clearfix&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-primary&quot;]/div[@class=&quot;panel-body recentPatientActivity&quot;]/ul[@class=&quot;list-group&quot;]/li[@class=&quot;list-group-item ng-scope&quot;]/a[1]/span[@class=&quot;list-group-item-heading ng-binding&quot;]</value>
      <webElementGuid>9f18bc73-c078-4d68-ae89-4a84081c3c8a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='widgetholder']/section/div[4]/div/div/div[2]/ul/li/a/span</value>
      <webElementGuid>bb4fdfc3-40d7-40e8-8118-74eef256b69e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::span[1]</value>
      <webElementGuid>41d6cce9-3a68-4ddf-9be9-db4466d24820</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/following::span[3]</value>
      <webElementGuid>ddde7163-a751-431f-8181-9286bd993e45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient Alerts'])[1]/preceding::span[2]</value>
      <webElementGuid>e993171d-91dd-4e99-b6c4-905d66914c88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[2]/preceding::span[2]</value>
      <webElementGuid>4e363d61-250d-40ae-830f-4ac2cab53e0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='STYLES, FURIOUS']/parent::*</value>
      <webElementGuid>a865e705-734f-4803-8b7b-279689ebb4f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/ul/li/a/span</value>
      <webElementGuid>a70beb5d-b76b-448d-8d0d-00ab60f79269</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'STYLES, FURIOUS ' or . = 'STYLES, FURIOUS ')]</value>
      <webElementGuid>f7c3ff40-15cf-45be-8b4b-fe81030d2486</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
