<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Jul 31_cursorPointer bgPlot</name>
   <tag></tag>
   <elementGuidId>6923c2e7-4d53-45c9-8155-d3ba6a23bc51</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[8]/div/div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@ng-click = concat(&quot;plotVisit.DayClick(&quot; , &quot;'&quot; , &quot;btnDate&quot; , &quot;'&quot; , &quot;, 0, 0, &quot; , &quot;'&quot; , &quot;7-31-2022&quot; , &quot;'&quot; , &quot;)&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4e6fcd48-5e50-4345-ab30-c5768f29aae3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.DayClick('btnDate', 0, 0, '7-31-2022')</value>
      <webElementGuid>7a9e91ab-5741-44ce-8a5f-9b4eaed50626</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursorPointer bgPlot</value>
      <webElementGuid>0be8339e-d1e4-483c-9414-94ce78b7f3d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;calendarWidth font11 ng-scope&quot;]/div[@class=&quot;cursorPointer bgPlot&quot;]</value>
      <webElementGuid>a5ec40c1-4757-412c-82e8-e16fdbf4d24b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[8]/div/div[2]</value>
      <webElementGuid>0d7e4238-8679-4abb-8cb8-0f18c8d93117</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[8]/div/div[2]</value>
      <webElementGuid>666b3726-3169-4734-8793-486cd7207a03</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
