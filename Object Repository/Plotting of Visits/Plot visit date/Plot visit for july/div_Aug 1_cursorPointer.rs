<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Aug 1_cursorPointer</name>
   <tag></tag>
   <elementGuidId>3e7e9554-6932-43c6-bf29-3cc1177b4475</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[8]/div[2]/div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@ng-click = concat(&quot;plotVisit.DayClick(&quot; , &quot;'&quot; , &quot;btnDate&quot; , &quot;'&quot; , &quot;, 0, 0, &quot; , &quot;'&quot; , &quot;8-1-2022&quot; , &quot;'&quot; , &quot;)&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7705328e-574f-4fe9-bc2a-e24e3bdd517f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.DayClick('btnDate', 0, 0, '8-1-2022')</value>
      <webElementGuid>6e815e07-7f10-475d-af90-cba162b512a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursorPointer</value>
      <webElementGuid>9788e863-06f1-4470-b028-a68c6dd19271</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;calendarWidth font11 ng-scope&quot;]/div[@class=&quot;cursorPointer&quot;]</value>
      <webElementGuid>6c62ea2d-7c83-46a7-8b44-8f594b47e5dc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[8]/div[2]/div[2]</value>
      <webElementGuid>44b16dfb-131d-4161-aaac-32ef96bd06d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[8]/div[2]/div[2]</value>
      <webElementGuid>d4d90a87-c13c-4337-9558-7d6cfc5ea748</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
