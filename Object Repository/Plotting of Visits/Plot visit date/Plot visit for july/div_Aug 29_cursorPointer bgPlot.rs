<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Aug 29_cursorPointer bgPlot</name>
   <tag></tag>
   <elementGuidId>44fab1c3-0f84-4e33-9169-1e672a2cf897</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[12]/div[2]/div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@ng-click = concat(&quot;plotVisit.DayClick(&quot; , &quot;'&quot; , &quot;btnDate&quot; , &quot;'&quot; , &quot;, 0, 0, &quot; , &quot;'&quot; , &quot;8-29-2022&quot; , &quot;'&quot; , &quot;)&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b0c9a6b2-7608-47c0-9843-e5865da691b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.DayClick('btnDate', 0, 0, '8-29-2022')</value>
      <webElementGuid>6f3c3665-6a90-47ca-be8a-19d292afef44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursorPointer bgPlot</value>
      <webElementGuid>ad2b7b4c-d49b-4418-83a6-f1031f2be545</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;calendarWidth font11 ng-scope&quot;]/div[@class=&quot;cursorPointer bgPlot&quot;]</value>
      <webElementGuid>8e94425d-f31f-42e2-adbd-7d99b7ea2a2d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[12]/div[2]/div[2]</value>
      <webElementGuid>3673f60a-4884-447c-8621-10c160871df0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Details'])[1]/preceding::div[17]</value>
      <webElementGuid>8810457b-7dbe-46a8-927d-38d1fcd76907</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payer'])[1]/preceding::div[18]</value>
      <webElementGuid>d3799b68-d2c4-4e14-90b5-e7cb6c0c500d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[12]/div[2]/div[2]</value>
      <webElementGuid>badd0a4b-64aa-43a4-9e17-1985343bb925</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
