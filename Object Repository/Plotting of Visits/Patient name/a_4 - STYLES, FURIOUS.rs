<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_4 - STYLES, FURIOUS</name>
   <tag></tag>
   <elementGuidId>bc7da088-ed25-4a2e-b166-04d3f3a4cba5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchresultslist']/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.ng-scope</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d8b5172e-c6db-4793-af5f-c455b5c90287</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>searchresult.no_summary_access == false &amp;&amp; search.searchType == 'patient'</value>
      <webElementGuid>bdc35753-5ecf-44bf-9f2f-f6395e675f50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Patient/Patient.aspx?patientId=4</value>
      <webElementGuid>50389226-460f-4041-9e1f-7918792423d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-scope</value>
      <webElementGuid>bcf68af1-d08b-4784-95dc-d858a3425f65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        4 - STYLES, FURIOUS
                                        rodeo drive Beverly Hills CA 90210
                                        
                                        SOC: 07/01/2022 Status: Admit
                                    </value>
      <webElementGuid>90a8dd07-fd4a-4c47-98bd-fa1bf023654e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchresultslist&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-scope&quot;]</value>
      <webElementGuid>a1ad7c49-9091-475c-aba4-3778a3ae05ee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/ul/li/a</value>
      <webElementGuid>35cfcdf4-db4e-4e95-895d-32475919633e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chat Support'])[1]/following::a[8]</value>
      <webElementGuid>d88aa7e7-4f6e-4f76-a1f2-725efab337f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::a[1]</value>
      <webElementGuid>46728cb4-a52c-4e1b-8736-11178a915da6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Admin'])[1]/preceding::a[1]</value>
      <webElementGuid>143ea9aa-2353-4a30-8d76-b8b3f810049d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Patient/Patient.aspx?patientId=4')]</value>
      <webElementGuid>c25441a2-1e68-4d62-9cce-d15d9e3850f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/ul/li/a</value>
      <webElementGuid>d8c2dbfe-504e-42af-9355-b1b64fb241c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Patient/Patient.aspx?patientId=4' and (text() = '
                                        4 - STYLES, FURIOUS
                                        rodeo drive Beverly Hills CA 90210
                                        
                                        SOC: 07/01/2022 Status: Admit
                                    ' or . = '
                                        4 - STYLES, FURIOUS
                                        rodeo drive Beverly Hills CA 90210
                                        
                                        SOC: 07/01/2022 Status: Admit
                                    ')]</value>
      <webElementGuid>ae84b30e-5e6d-40b4-9d7a-ef5e6a4da1a0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
