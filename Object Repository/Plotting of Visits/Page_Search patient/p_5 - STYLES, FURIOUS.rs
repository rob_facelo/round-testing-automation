<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_5 - STYLES, FURIOUS</name>
   <tag></tag>
   <elementGuidId>1e42bcf1-b4fa-4aa4-bb8e-02b295d1c859</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchresultslist']/ul/li/a/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.text-primary.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>4a18f65a-49c3-4c19-a04a-4750b1d40d96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-primary ng-binding</value>
      <webElementGuid>42b30174-ba14-48a7-b798-ad7248bd32f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>5 - STYLES, FURIOUS</value>
      <webElementGuid>74e867b4-9870-4c72-9d22-fd3f4ac83724</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchresultslist&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-scope&quot;]/p[@class=&quot;text-primary ng-binding&quot;]</value>
      <webElementGuid>d754639d-be06-4200-bc79-fa09bb15ee8b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/ul/li/a/p</value>
      <webElementGuid>da4405de-4e0e-427f-871e-e2899aa257ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chat Support'])[1]/following::p[1]</value>
      <webElementGuid>0d00ac31-11f4-4c7c-94d5-036ae808bcff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::p[4]</value>
      <webElementGuid>7d355f3b-b352-45c2-9292-bd0cabd40408</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Admin'])[1]/preceding::p[4]</value>
      <webElementGuid>085922cd-e25e-43e3-a2d1-368f5ca2a7a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='5 - STYLES, FURIOUS']/parent::*</value>
      <webElementGuid>c81bae41-24e2-4a05-afb6-fd19c05e755a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/p</value>
      <webElementGuid>7d8d1e04-70c7-4675-a9d2-89d8ace2e544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '5 - STYLES, FURIOUS' or . = '5 - STYLES, FURIOUS')]</value>
      <webElementGuid>436927c7-e687-48cf-9a19-77ac0263b606</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
