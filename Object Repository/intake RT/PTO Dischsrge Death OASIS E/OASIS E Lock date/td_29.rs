<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_29</name>
   <tag></tag>
   <elementGuidId>0541b1a9-9015-4dae-b586-68e509ffb8c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '29' or . = '29')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>ea5e7e82-21ef-46fa-9964-5200b6897e90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>day</value>
      <webElementGuid>8d75cb37-eeb5-40c7-89e2-837d18b45758</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>29</value>
      <webElementGuid>6625da2c-e087-4e69-9883-93e673210827</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/tr[1]/td[@class=&quot;day&quot;]</value>
      <webElementGuid>3de721d7-f8aa-4532-81ba-f0f1bfb425b7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '29' or . = '29')]</value>
      <webElementGuid>91ba966d-fbf5-4806-8b82-1047734b2bd0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
