<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_29</name>
   <tag></tag>
   <elementGuidId>137d47e1-5e4e-41c3-812f-a6d455a3d9e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '29' or . = '29')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>f1935069-05ab-4951-bc7a-76ee5ec65703</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>active day</value>
      <webElementGuid>aa2d37ae-4e8b-4264-be73-a09aa16417c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>29</value>
      <webElementGuid>bdb5e104-2fc3-4334-b973-cd247334cee6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/tr[1]/td[@class=&quot;active day&quot;]</value>
      <webElementGuid>f1bfe485-8bc5-433f-8e36-00bc186b9b0a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '29' or . = '29')]</value>
      <webElementGuid>fdfa08c4-7989-45d0-bf66-cc1c923a37b2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
