<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_CODE</name>
   <tag></tag>
   <elementGuidId>bea5d0b3-a03d-4582-9417-f0baf234daf9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M0080_ASSESSOR_DISCIPLINE' and (text() = '
                                        
                                        1
                                        2
                                        3
                                        4
                                    ' or . = '
                                        
                                        1
                                        2
                                        3
                                        4
                                    ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M0080']/div/div[2]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>8c64cf31-3684-4390-ad54-137ad2e6fd9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>13903b3d-2cec-47d6-8d6f-54add5ade614</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M0080_ASSESSOR_DISCIPLINE</value>
      <webElementGuid>8a08c395-9b46-40d2-85fd-7f4d45511e1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M0080_ASSESSOR_DISCIPLINE</value>
      <webElementGuid>41e80523-0981-4300-9eea-6fe9b86f6eea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M0080_ASSESSOR_DISCIPLINE', oasis.oasisDetails.M0080_ASSESSOR_DISCIPLINE)</value>
      <webElementGuid>c3ef67d2-5211-4305-a7be-17c1328ccc61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>b71cca33-49f5-4b86-9363-2d63729a4f28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        1
                                        2
                                        3
                                        4
                                    </value>
      <webElementGuid>7bb7f3d3-3d0a-4834-b984-3ce403182940</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M0080&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>75008bbc-8360-4a5f-8e71-34e25da7506c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M0080']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>33752f72-ecb8-4621-8fea-8e0dd9985814</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[1]/following::select[1]</value>
      <webElementGuid>434bb454-6625-48a3-8a25-1f7e49e55654</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0080 Discipline of Person Completing Assessment.'])[1]/following::select[1]</value>
      <webElementGuid>77668f3c-4d5c-4078-92e9-689fcb7fe83a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RN'])[1]/preceding::select[1]</value>
      <webElementGuid>a4a85ecb-05fc-49bb-9a68-69eee772faaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PT'])[1]/preceding::select[1]</value>
      <webElementGuid>f71e4d99-f555-4560-8761-a4774a6f02b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/select</value>
      <webElementGuid>d4408358-cd96-435a-a823-1c9e23fc4363</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        1
                                        2
                                        3
                                        4
                                    ' or . = '
                                        
                                        1
                                        2
                                        3
                                        4
                                    ')]</value>
      <webElementGuid>f5aeb369-15d2-43b0-930b-9003a63fc970</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
