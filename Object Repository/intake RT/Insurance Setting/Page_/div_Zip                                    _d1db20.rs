<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Zip                                    _d1db20</name>
   <tag></tag>
   <elementGuidId>5c92a9cb-cf8a-4911-9b46-aa2e11e61cbf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[8]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5a7ff39e-54c7-4d4c-a605-7a98245048a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>f108c87e-708c-438a-bd6e-338fa511756b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            </value>
      <webElementGuid>506fdf1a-f258-410a-90c2-34c200e7e453</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>c27b176e-202e-4e47-bcf6-613bea292fc7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[8]</value>
      <webElementGuid>fbbfe93b-b70b-41eb-8f85-3662d9775456</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::div[2]</value>
      <webElementGuid>cd183516-8b1b-4fda-b7f5-aa1da83607e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div/div/table/tbody/tr/td/div[8]</value>
      <webElementGuid>8c686a35-2c66-4751-ae2f-5cc6822bd3de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            ' or . = '
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            ')]</value>
      <webElementGuid>45716e07-2867-488c-a5bc-aad8f4cee9c7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
