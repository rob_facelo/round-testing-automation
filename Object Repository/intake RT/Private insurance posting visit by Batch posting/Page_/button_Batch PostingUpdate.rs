<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Batch PostingUpdate</name>
   <tag></tag>
   <elementGuidId>a6075c87-fd70-40d9-a16b-3476245531ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[57]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>507d7cdf-cc64-4f45-8888-5de8039334b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>!isParentSummary &amp;&amp; cl.Pagefields.CalendarBatchPost.AccessType === 3</value>
      <webElementGuid>1e5e6f18-414e-4231-929d-acaa3d63224f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c2a09c08-d44f-485e-afab-4e1158b2ecb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default ng-scope</value>
      <webElementGuid>8f81392d-5b3b-47b2-be42-d947e1531d75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>RedirectToBatchPosting()</value>
      <webElementGuid>7babe303-44ce-4a80-93b1-449e2b21244f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Batch Posting/Update  </value>
      <webElementGuid>81a9bad9-37b9-49b4-8a41-ac7626f8b9c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row margin-top-sm ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-9 col-sm-9 col-md-9 col-lg-9&quot;]/span[@class=&quot;pull-right&quot;]/div[@class=&quot;btn-group&quot;]/button[@class=&quot;btn btn-default ng-scope&quot;]</value>
      <webElementGuid>c6d6c0ef-b96a-4c1f-a62d-f3acf13951fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[57]</value>
      <webElementGuid>266388ce-7916-4ab0-a8b9-4f3d1f94bba8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div/div[2]/div/div/div/div/div[2]/span/div/button[4]</value>
      <webElementGuid>b4382943-0409-43e1-8e74-a78124900172</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Week (7 Day)'])[1]/following::button[1]</value>
      <webElementGuid>3caf0bee-cb6c-446c-9fe9-787975857857</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batch Print Notes'])[1]/following::button[2]</value>
      <webElementGuid>a0cbb285-6e72-4532-af04-03c81e68e414</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[4]</value>
      <webElementGuid>80abf25f-7d08-4dc8-8dbe-8106887eb523</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Batch Posting/Update  ' or . = 'Batch Posting/Update  ')]</value>
      <webElementGuid>723d5e17-1256-4f41-b3ce-1440e8f44ef5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
