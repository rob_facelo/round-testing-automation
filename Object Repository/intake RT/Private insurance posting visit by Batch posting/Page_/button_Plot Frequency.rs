<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Plot Frequency</name>
   <tag></tag>
   <elementGuidId>1927a671-9f6e-4553-acda-71d410a09571</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-sm.font13</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'plotVisit.PlotFrequency(1)' and (text() = 'Plot Frequency' or . = 'Plot Frequency')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[196]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ef000185-a87e-4737-8c4b-ec29d50f1a3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c80c9c0e-3d93-4a86-bdb3-9bae0256a32b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.PlotFrequency(1)</value>
      <webElementGuid>e6743ce3-8186-42ba-b5d7-2f549f597af3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-sm font13</value>
      <webElementGuid>4fc31dde-d8ae-4287-97a5-c8340aacf987</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Plot Frequency</value>
      <webElementGuid>98142cc6-ad25-40f1-973c-43d562acacc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow plotFrequencyPanel&quot;]/div[@class=&quot;modal-footer&quot;]/div[@class=&quot;ng-scope&quot;]/button[@class=&quot;btn btn-primary btn-sm font13&quot;]</value>
      <webElementGuid>cefc3108-e44d-43f1-9580-f48567efa7fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[196]</value>
      <webElementGuid>ec829492-1b92-4e00-93f1-9ef2e5836677</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div[2]/div[3]/div/button</value>
      <webElementGuid>26e0e38a-5976-4970-9f77-108222b3814f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[4]/following::button[1]</value>
      <webElementGuid>3c01d293-b9b5-4053-8ec7-9063e94a44de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Time Out'])[2]/following::button[1]</value>
      <webElementGuid>73b8971b-2ee5-464c-b832-feee07e422e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Details'])[2]/preceding::button[2]</value>
      <webElementGuid>4eff158e-24b2-4954-8478-e40f0bd80490</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Discipline :'])[1]/preceding::button[2]</value>
      <webElementGuid>2652b224-1a92-46a3-b71a-ac0213a3edd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Plot Frequency']/parent::*</value>
      <webElementGuid>10eaaa30-8f0b-4585-be49-0c32155a430d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div[2]/div[2]/div[3]/div/button</value>
      <webElementGuid>faa94964-d11f-4b99-b92d-2f313b2ea9fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Plot Frequency' or . = 'Plot Frequency')]</value>
      <webElementGuid>cd20f88f-91a8-465b-add8-bd7acb1b92ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
