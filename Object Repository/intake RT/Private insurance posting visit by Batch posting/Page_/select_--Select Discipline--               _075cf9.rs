<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Select Discipline--               _075cf9</name>
   <tag></tag>
   <elementGuidId>342dab00-f382-4065-bec2-8376273f7c6d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ddlCaregiverType</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlCaregiverType']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a6d719b9-7a5d-4f02-bc68-3a0cf5f6457d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm fontsize-13-weight-600 ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>6499ba7f-a116-4718-b535-12fabfb8fbe9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlCaregiverType</value>
      <webElementGuid>9df9117c-c77f-4033-aa75-9eda0fd58463</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>plotVisit.OnCgTypeChange()</value>
      <webElementGuid>b82e44cf-83c3-4aa8-a830-a9d6186c96ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>plotVisit.SelectedCgType</value>
      <webElementGuid>384eecf3-7dc7-42d6-8e8e-4ab170baf920</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                </value>
      <webElementGuid>a94f3dcc-013d-4b42-a2c0-0f957050aed0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlCaregiverType&quot;)</value>
      <webElementGuid>a47d5bb1-f50b-4d35-9e90-09ce243f6e8c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlCaregiverType']</value>
      <webElementGuid>f87d4bd7-b105-4824-bf68-b2fd187b5035</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>b671547e-c38b-4340-be04-7f689c496b34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff Type'])[1]/following::select[1]</value>
      <webElementGuid>2a1f6e85-3b23-4895-aee7-4a1802cc4971</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payer'])[1]/following::select[2]</value>
      <webElementGuid>9434f26d-053c-4142-8b06-5f8f30f40fc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff'])[2]/preceding::select[1]</value>
      <webElementGuid>de15ced1-3eb9-438a-869e-c896dc4002fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Type'])[1]/preceding::select[2]</value>
      <webElementGuid>04ea989b-9548-45dd-9511-1b20acc22cfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>84720ac2-1dd9-4030-9a77-464845e27988</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlCaregiverType' and (text() = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ' or . = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ')]</value>
      <webElementGuid>2a83f422-6899-4aa4-b589-b83b3cb3095c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
