<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_HomeContent .modal-body         padding_cd5a72</name>
   <tag></tag>
   <elementGuidId>6a81479d-e775-4246-97ce-207ab65a1516</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#HomeContent</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='HomeContent']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f70a8407-e0e5-4104-88cc-9a4ea3348b2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal fade in</value>
      <webElementGuid>62f143d7-9f98-4c51-b9ae-9e5b34d651ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>b0a17456-cd02-4541-880d-98a0244e8ddf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>8441223e-038c-4071-ad99-80caa528d1d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-backdrop</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4d5b863f-50cc-456a-a557-2cde939df918</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>HomeContent</value>
      <webElementGuid>2bc93cc2-67c1-47ad-9710-d463d1f80259</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>HomeContent</value>
      <webElementGuid>fb64b789-8d23-4565-b3bd-403cd63609a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            
                
                    ×
                
                
                    

    #HomeContent .modal-body {
        padding: 0 !important;
    }

    .homecontentitemholder {
        padding: 0px !important;
        margin: 0px !important;
        min-width: 100%;
        min-height: 500px;
        font-family: Arial;
    }

    .homecontentitemholder .carousel-item-header-logo {
        width: 100%;
        padding: 0 20px;
    }

    .homecontentitemholder .carousel-item-header-logo .banner {
        background-image: url(&quot;data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACWAAMDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACivJP+E08Qf9BD/yDH/8TRQBg0UUUAFFdb/wrzV/+fiy/wC+3/8AiaKAPTKKKKACiiigAooooAKKKKAP/9k=&quot;);
        background-repeat: repeat-x;
        width: 100%;
        height: 100px;
        text-align: center;
        background-position: center;
        padding: 0 20px;
    }

    .homecontentitemholder .carousel-item-header-logo .banner img {
        height: 100px;
    }

    .homecontentitemholder .carousel-item-header {
        padding-top: 5px;
        text-align: center;
        width: 100%;
    }

    .homecontentitemholder .carousel-item-body {
        padding: 20px 20px 20px 20px;

        font-family: Arial;
    }

    .homecontentitemholder .carousel-item-body p {
        text-align: justify;
    }

    .img-responsive {
        display: inline !important;
        height: 185px;
    }

    .bannerReferral {
        width: 100%;
        text-align: center;

        padding: 0 20px;
    }

    .carousel-indicators li.active {
        background-color: #a3c739 !important;
        width: 13px !important;
        height: 13px !important;
    }

    .carousel-indicators {
        bottom: -27px;
    }

    .carousel-indicators li {
        width: 13px !important;
        height: 13px !important;
        background-color: #868686 !important;
    }

    .referral-btn {
        position: relative;
        z-index: 2;
        font-weight: bold;
        letter-spacing: 2px;
        margin-top: -45px;
        border: 1px solid transparent;
        color: #fff;
        background-color: #00a65a;
        text-transform: uppercase;
        font-family: Arial;
    }

    .referral-btn:hover {
        color: #ffffff !important;
    }

    .referral-btn:active {
        color: #ffffff !important;
    }

    .referral-btn:focus {
        color: #ffffff !important;
    }



    
    
        
        
        
        
        
    

    

        
        
            
            
			
				
			
			
		

        
        
            
                
                    
                
            

            
                
                    HOME
                        HEALTH
                    
                    CENTRE
                
                ULTRA IS HERE
            
            
                
                    
                        We're excited to announce that we have made some updates to Home Health Centre
                        and we're calling
                        it Ultra. A fresh new look and added features are coming your way. You don't
                        have to switch right
                        away but after you see it we think you will want to make the switch as soon as you can. You're
                        going to be receiving
                        an email with dates and times of upcoming webinar trainings so please be on the look out for
                        that.
                    
                
            
        

        
        
            
                
                    
                
            

            
                HOME
                    HEALTH
                
                SOFTWARE
            
            
                
                    
                        Data Soft Logic?s Home Health Centre for Home Health Agencies is your complete
                        web-based software solution anytime, anywhere, and across all administrative and clinical
                        disciplines of your agency.
                        Data Soft Logic?s Home Health Centre is here to maximize your efficiencies and
                        increase your profits.
                        We are experts in innovating fast, simple, and intelligent software solutions.
                    
                    
                
            
        

        

        
            
                
                    
                
            
            
                
                    
                        HOSPICE
                        CENTRE
                    
                
                DID YOU KNOW?
            
            
                
                    
                        Did you know that Data Soft Logic has a great hospice software! It's called Hospice
                            Centre and it was developed and built specifically for hospice agencies using the
                        hospice flow.
                        Hospice Centre has the same great integrations you currently utilize in Home
                        Health Centre. Ability Services, Pecos, Pepid, and PMIC are all there for you in Hospice
                            Centre.
                        Multi-signature IDT, direct import of NDC codes, and a combined NCA &amp; HIS are featured in
                        our Hospice Centre complete cloud software.
                        Call 866.430.0263 or click to schedule your demo of Hospice Centre today!
                    
                
            
        


        
        
            
                
                    
                
            
            
                REFERRAL PROGRAM
            
            
                
                    
                        Did you know that you can earn $1000 for every client you refer to Data Soft
                        Logic! Just click and refer it's so easy.
                        If your referral becomes our client we will reward you with $1000. We
                        know what you say matters and we appreciate it when you say good things!
                        See our website for full test. 
                    
                    
                
            
        
    Referral Program


    $(function () {
        $(&quot;#HomeContent .form-check&quot;).find(&quot;button&quot;).remove();
        $(&quot;#HomeContent .modal-body&quot;).children().first().removeAttr(&quot;style&quot;);
        $(&quot;#content-carousel-popup&quot;).carousel({
            interval: 999999,
            pause: false
        });
        $(&quot;#HomeContent .carousel-inner&quot;).append('&lt;a class=&quot;btn referral-btn&quot; href=&quot;http://datasoftlogic.com/spread-the-word&quot; target=&quot;_blank&quot; class=&quot;btn&quot;>Referral Program&lt;/a>');
    });

	var host = window.location.hostname;
    var whatsNewLink = &quot;https://&quot; + host + &quot;/WhatsNew/WhatsNewList.aspx&quot;;
    document.getElementById(&quot;whatsNewBtn&quot;).href = whatsNewLink;


                
                
                    
                        
                        
                            Don't show this content again.
                        
                        
                    

                
            
        
    </value>
      <webElementGuid>b39706fd-3696-46f4-86d6-999bbacb2e9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;HomeContent&quot;)</value>
      <webElementGuid>ed9ff4f2-cc95-4147-b801-67cfdf30d03e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='HomeContent']</value>
      <webElementGuid>51981028-7c19-4a51-9672-1056831b4769</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='taskworkflow_container']/div[3]</value>
      <webElementGuid>65d47206-e4ab-4734-ab96-8f48f2920128</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[3]/following::div[1]</value>
      <webElementGuid>6bc272df-f11a-4830-b203-3af8780e9567</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Printer Friendly'])[1]/following::div[5]</value>
      <webElementGuid>93abfd17-ec56-4055-8c59-68054d201855</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div/div/div[3]</value>
      <webElementGuid>bd00852e-c402-49ad-ac9a-47e680deef65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'HomeContent' and (text() = concat(&quot;
        
            
                
                    ×
                
                
                    

    #HomeContent .modal-body {
        padding: 0 !important;
    }

    .homecontentitemholder {
        padding: 0px !important;
        margin: 0px !important;
        min-width: 100%;
        min-height: 500px;
        font-family: Arial;
    }

    .homecontentitemholder .carousel-item-header-logo {
        width: 100%;
        padding: 0 20px;
    }

    .homecontentitemholder .carousel-item-header-logo .banner {
        background-image: url(&quot;data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACWAAMDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACivJP+E08Qf9BD/yDH/8TRQBg0UUUAFFdb/wrzV/+fiy/wC+3/8AiaKAPTKKKKACiiigAooooAKKKKAP/9k=&quot;);
        background-repeat: repeat-x;
        width: 100%;
        height: 100px;
        text-align: center;
        background-position: center;
        padding: 0 20px;
    }

    .homecontentitemholder .carousel-item-header-logo .banner img {
        height: 100px;
    }

    .homecontentitemholder .carousel-item-header {
        padding-top: 5px;
        text-align: center;
        width: 100%;
    }

    .homecontentitemholder .carousel-item-body {
        padding: 20px 20px 20px 20px;

        font-family: Arial;
    }

    .homecontentitemholder .carousel-item-body p {
        text-align: justify;
    }

    .img-responsive {
        display: inline !important;
        height: 185px;
    }

    .bannerReferral {
        width: 100%;
        text-align: center;

        padding: 0 20px;
    }

    .carousel-indicators li.active {
        background-color: #a3c739 !important;
        width: 13px !important;
        height: 13px !important;
    }

    .carousel-indicators {
        bottom: -27px;
    }

    .carousel-indicators li {
        width: 13px !important;
        height: 13px !important;
        background-color: #868686 !important;
    }

    .referral-btn {
        position: relative;
        z-index: 2;
        font-weight: bold;
        letter-spacing: 2px;
        margin-top: -45px;
        border: 1px solid transparent;
        color: #fff;
        background-color: #00a65a;
        text-transform: uppercase;
        font-family: Arial;
    }

    .referral-btn:hover {
        color: #ffffff !important;
    }

    .referral-btn:active {
        color: #ffffff !important;
    }

    .referral-btn:focus {
        color: #ffffff !important;
    }



    
    
        
        
        
        
        
    

    

        
        
            
            
			
				
			
			
		

        
        
            
                
                    
                
            

            
                
                    HOME
                        HEALTH
                    
                    CENTRE
                
                ULTRA IS HERE
            
            
                
                    
                        We&quot; , &quot;'&quot; , &quot;re excited to announce that we have made some updates to Home Health Centre
                        and we&quot; , &quot;'&quot; , &quot;re calling
                        it Ultra. A fresh new look and added features are coming your way. You don&quot; , &quot;'&quot; , &quot;t
                        have to switch right
                        away but after you see it we think you will want to make the switch as soon as you can. You&quot; , &quot;'&quot; , &quot;re
                        going to be receiving
                        an email with dates and times of upcoming webinar trainings so please be on the look out for
                        that.
                    
                
            
        

        
        
            
                
                    
                
            

            
                HOME
                    HEALTH
                
                SOFTWARE
            
            
                
                    
                        Data Soft Logic?s Home Health Centre for Home Health Agencies is your complete
                        web-based software solution anytime, anywhere, and across all administrative and clinical
                        disciplines of your agency.
                        Data Soft Logic?s Home Health Centre is here to maximize your efficiencies and
                        increase your profits.
                        We are experts in innovating fast, simple, and intelligent software solutions.
                    
                    
                
            
        

        

        
            
                
                    
                
            
            
                
                    
                        HOSPICE
                        CENTRE
                    
                
                DID YOU KNOW?
            
            
                
                    
                        Did you know that Data Soft Logic has a great hospice software! It&quot; , &quot;'&quot; , &quot;s called Hospice
                            Centre and it was developed and built specifically for hospice agencies using the
                        hospice flow.
                        Hospice Centre has the same great integrations you currently utilize in Home
                        Health Centre. Ability Services, Pecos, Pepid, and PMIC are all there for you in Hospice
                            Centre.
                        Multi-signature IDT, direct import of NDC codes, and a combined NCA &amp; HIS are featured in
                        our Hospice Centre complete cloud software.
                        Call 866.430.0263 or click to schedule your demo of Hospice Centre today!
                    
                
            
        


        
        
            
                
                    
                
            
            
                REFERRAL PROGRAM
            
            
                
                    
                        Did you know that you can earn $1000 for every client you refer to Data Soft
                        Logic! Just click and refer it&quot; , &quot;'&quot; , &quot;s so easy.
                        If your referral becomes our client we will reward you with $1000. We
                        know what you say matters and we appreciate it when you say good things!
                        See our website for full test. 
                    
                    
                
            
        
    Referral Program


    $(function () {
        $(&quot;#HomeContent .form-check&quot;).find(&quot;button&quot;).remove();
        $(&quot;#HomeContent .modal-body&quot;).children().first().removeAttr(&quot;style&quot;);
        $(&quot;#content-carousel-popup&quot;).carousel({
            interval: 999999,
            pause: false
        });
        $(&quot;#HomeContent .carousel-inner&quot;).append(&quot; , &quot;'&quot; , &quot;&lt;a class=&quot;btn referral-btn&quot; href=&quot;http://datasoftlogic.com/spread-the-word&quot; target=&quot;_blank&quot; class=&quot;btn&quot;>Referral Program&lt;/a>&quot; , &quot;'&quot; , &quot;);
    });

	var host = window.location.hostname;
    var whatsNewLink = &quot;https://&quot; + host + &quot;/WhatsNew/WhatsNewList.aspx&quot;;
    document.getElementById(&quot;whatsNewBtn&quot;).href = whatsNewLink;


                
                
                    
                        
                        
                            Don&quot; , &quot;'&quot; , &quot;t show this content again.
                        
                        
                    

                
            
        
    &quot;) or . = concat(&quot;
        
            
                
                    ×
                
                
                    

    #HomeContent .modal-body {
        padding: 0 !important;
    }

    .homecontentitemholder {
        padding: 0px !important;
        margin: 0px !important;
        min-width: 100%;
        min-height: 500px;
        font-family: Arial;
    }

    .homecontentitemholder .carousel-item-header-logo {
        width: 100%;
        padding: 0 20px;
    }

    .homecontentitemholder .carousel-item-header-logo .banner {
        background-image: url(&quot;data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACWAAMDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACivJP+E08Qf9BD/yDH/8TRQBg0UUUAFFdb/wrzV/+fiy/wC+3/8AiaKAPTKKKKACiiigAooooAKKKKAP/9k=&quot;);
        background-repeat: repeat-x;
        width: 100%;
        height: 100px;
        text-align: center;
        background-position: center;
        padding: 0 20px;
    }

    .homecontentitemholder .carousel-item-header-logo .banner img {
        height: 100px;
    }

    .homecontentitemholder .carousel-item-header {
        padding-top: 5px;
        text-align: center;
        width: 100%;
    }

    .homecontentitemholder .carousel-item-body {
        padding: 20px 20px 20px 20px;

        font-family: Arial;
    }

    .homecontentitemholder .carousel-item-body p {
        text-align: justify;
    }

    .img-responsive {
        display: inline !important;
        height: 185px;
    }

    .bannerReferral {
        width: 100%;
        text-align: center;

        padding: 0 20px;
    }

    .carousel-indicators li.active {
        background-color: #a3c739 !important;
        width: 13px !important;
        height: 13px !important;
    }

    .carousel-indicators {
        bottom: -27px;
    }

    .carousel-indicators li {
        width: 13px !important;
        height: 13px !important;
        background-color: #868686 !important;
    }

    .referral-btn {
        position: relative;
        z-index: 2;
        font-weight: bold;
        letter-spacing: 2px;
        margin-top: -45px;
        border: 1px solid transparent;
        color: #fff;
        background-color: #00a65a;
        text-transform: uppercase;
        font-family: Arial;
    }

    .referral-btn:hover {
        color: #ffffff !important;
    }

    .referral-btn:active {
        color: #ffffff !important;
    }

    .referral-btn:focus {
        color: #ffffff !important;
    }



    
    
        
        
        
        
        
    

    

        
        
            
            
			
				
			
			
		

        
        
            
                
                    
                
            

            
                
                    HOME
                        HEALTH
                    
                    CENTRE
                
                ULTRA IS HERE
            
            
                
                    
                        We&quot; , &quot;'&quot; , &quot;re excited to announce that we have made some updates to Home Health Centre
                        and we&quot; , &quot;'&quot; , &quot;re calling
                        it Ultra. A fresh new look and added features are coming your way. You don&quot; , &quot;'&quot; , &quot;t
                        have to switch right
                        away but after you see it we think you will want to make the switch as soon as you can. You&quot; , &quot;'&quot; , &quot;re
                        going to be receiving
                        an email with dates and times of upcoming webinar trainings so please be on the look out for
                        that.
                    
                
            
        

        
        
            
                
                    
                
            

            
                HOME
                    HEALTH
                
                SOFTWARE
            
            
                
                    
                        Data Soft Logic?s Home Health Centre for Home Health Agencies is your complete
                        web-based software solution anytime, anywhere, and across all administrative and clinical
                        disciplines of your agency.
                        Data Soft Logic?s Home Health Centre is here to maximize your efficiencies and
                        increase your profits.
                        We are experts in innovating fast, simple, and intelligent software solutions.
                    
                    
                
            
        

        

        
            
                
                    
                
            
            
                
                    
                        HOSPICE
                        CENTRE
                    
                
                DID YOU KNOW?
            
            
                
                    
                        Did you know that Data Soft Logic has a great hospice software! It&quot; , &quot;'&quot; , &quot;s called Hospice
                            Centre and it was developed and built specifically for hospice agencies using the
                        hospice flow.
                        Hospice Centre has the same great integrations you currently utilize in Home
                        Health Centre. Ability Services, Pecos, Pepid, and PMIC are all there for you in Hospice
                            Centre.
                        Multi-signature IDT, direct import of NDC codes, and a combined NCA &amp; HIS are featured in
                        our Hospice Centre complete cloud software.
                        Call 866.430.0263 or click to schedule your demo of Hospice Centre today!
                    
                
            
        


        
        
            
                
                    
                
            
            
                REFERRAL PROGRAM
            
            
                
                    
                        Did you know that you can earn $1000 for every client you refer to Data Soft
                        Logic! Just click and refer it&quot; , &quot;'&quot; , &quot;s so easy.
                        If your referral becomes our client we will reward you with $1000. We
                        know what you say matters and we appreciate it when you say good things!
                        See our website for full test. 
                    
                    
                
            
        
    Referral Program


    $(function () {
        $(&quot;#HomeContent .form-check&quot;).find(&quot;button&quot;).remove();
        $(&quot;#HomeContent .modal-body&quot;).children().first().removeAttr(&quot;style&quot;);
        $(&quot;#content-carousel-popup&quot;).carousel({
            interval: 999999,
            pause: false
        });
        $(&quot;#HomeContent .carousel-inner&quot;).append(&quot; , &quot;'&quot; , &quot;&lt;a class=&quot;btn referral-btn&quot; href=&quot;http://datasoftlogic.com/spread-the-word&quot; target=&quot;_blank&quot; class=&quot;btn&quot;>Referral Program&lt;/a>&quot; , &quot;'&quot; , &quot;);
    });

	var host = window.location.hostname;
    var whatsNewLink = &quot;https://&quot; + host + &quot;/WhatsNew/WhatsNewList.aspx&quot;;
    document.getElementById(&quot;whatsNewBtn&quot;).href = whatsNewLink;


                
                
                    
                        
                        
                            Don&quot; , &quot;'&quot; , &quot;t show this content again.
                        
                        
                    

                
            
        
    &quot;))]</value>
      <webElementGuid>7186c0f9-2296-43a3-bf7a-33e422a8a94f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
