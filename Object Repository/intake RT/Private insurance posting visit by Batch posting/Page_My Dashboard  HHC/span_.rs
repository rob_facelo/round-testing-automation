<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_</name>
   <tag></tag>
   <elementGuidId>06cce684-e75b-4525-ba0e-7f61e55bec0a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#HomeContent > div.modal-dialog.modal-lg > div.modal-content > div.modal-header > button.close > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='HomeContent']/div/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ff705534-6101-440d-a47c-25e2641b3657</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d8f7407e-d599-4fcf-be52-b3c9f8c4bc0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>×</value>
      <webElementGuid>01e3da48-163f-436e-be3a-401b43f42e32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;HomeContent&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]/button[@class=&quot;close&quot;]/span[1]</value>
      <webElementGuid>a1088316-5729-4712-a117-39fd4a7ad5cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HomeContent']/div/div/div/button/span</value>
      <webElementGuid>444d13ef-feb9-4244-953d-02bf969c5364</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[3]/following::span[1]</value>
      <webElementGuid>9a1da682-59ca-482b-a039-983b3220b29e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Printer Friendly'])[1]/following::span[1]</value>
      <webElementGuid>9bdae98f-e282-4934-858b-6a51a093c58c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CENTRE'])[1]/preceding::span[2]</value>
      <webElementGuid>ab8c66a6-8180-4199-a467-539e403c1714</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/button/span</value>
      <webElementGuid>5116e854-a975-4ced-a210-3f4f2b13b1d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '×' or . = '×')]</value>
      <webElementGuid>701b79e5-3bcd-465c-af0d-6b33e81ebeda</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
