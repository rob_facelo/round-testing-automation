<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Post</name>
   <tag></tag>
   <elementGuidId>3ee86db6-ca08-49ce-9a6d-c35726186955</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell0_8_btnPosted_0_CD > span.dx-vam</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Post' or . = 'Post')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell0_8_btnPosted_0_CD']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a2508e11-b0aa-4207-891a-682792577982</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam</value>
      <webElementGuid>98e9d573-2d16-4a96-928e-a4549acb0b26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Post</value>
      <webElementGuid>57cc46bc-f5c9-4f94-9662-9a14de8f76aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell0_8_btnPosted_0_CD&quot;)/span[@class=&quot;dx-vam&quot;]</value>
      <webElementGuid>3afe08ff-bdc2-473e-91eb-653cd8bdcf03</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell0_8_btnPosted_0_CD']/span</value>
      <webElementGuid>92f6e6f9-7ce6-4362-8cf3-5c3f59aa5d4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Started'])[1]/following::span[1]</value>
      <webElementGuid>5fda4e35-5dc9-4573-900b-606438ca5a34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The number must be in the range 0...999'])[3]/following::span[1]</value>
      <webElementGuid>e0466551-da79-4be7-90af-1abd3a62eca8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/preceding::span[1]</value>
      <webElementGuid>4e80ed35-ec30-4168-b28f-21a5f50f5339</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Post']/parent::*</value>
      <webElementGuid>0f573202-b929-4338-8c3d-a146eb6bf4a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[9]/div/div/div/div/span</value>
      <webElementGuid>8be6a42d-5718-4c1f-9da4-1a301cefb581</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Post' or . = 'Post')]</value>
      <webElementGuid>752e964a-e37c-40e7-921f-e5818db081de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
