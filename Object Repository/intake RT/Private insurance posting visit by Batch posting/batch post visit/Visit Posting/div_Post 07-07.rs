<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Post 07-07</name>
   <tag></tag>
   <elementGuidId>0b3d8ff2-5557-4c7c-be28-7f1e7a3551e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'dxb' and (text() = '
						
							
						Post
					' or . = '
						
							
						Post
					')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>54bcbc50-086c-465a-93e6-a46a647e3dc6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxb</value>
      <webElementGuid>a9906d89-1b49-4aa3-ab68-043813536a95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD</value>
      <webElementGuid>fdb0c10a-b37c-4021-8986-1483c2107bec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						
							
						Post
					</value>
      <webElementGuid>a8d95d45-3052-406c-a11d-eac37234f343</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD&quot;)</value>
      <webElementGuid>ea428d68-c8d0-44ab-8c93-f5c969a6f14d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD']</value>
      <webElementGuid>582fb19c-3011-4eb8-90b9-990295d4e923</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1']/div</value>
      <webElementGuid>5c8ec7df-413f-4ba7-ab8d-71d2c0509226</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Started'])[2]/following::div[4]</value>
      <webElementGuid>af5cc491-e412-4705-b508-062bea11dee6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The number must be in the range 0...999'])[6]/following::div[4]</value>
      <webElementGuid>cc30fcba-4dcf-477e-ae53-8af50026f1ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[2]/preceding::div[3]</value>
      <webElementGuid>946a167a-aa1f-4344-b932-0fb760fdea19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[9]/div/div/div/div</value>
      <webElementGuid>d312cf06-ef83-4ff9-98dc-2d76be4365f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD' and (text() = '
						
							
						Post
					' or . = '
						
							
						Post
					')]</value>
      <webElementGuid>cca3d727-845f-4740-9b69-161acfe14ffd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
