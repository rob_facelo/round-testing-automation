<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Acosta, Casey SN -</name>
   <tag></tag>
   <elementGuidId>0e3be649-3212-4af9-b279-38b903cb532f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'col-md-12 col-lg-12 ng-binding' and (text() = '
                        Acosta, Casey SN - 
                    ' or . = '
                        Acosta, Casey SN - 
                    ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div[2]/div/div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d282d0dd-334d-46df-94b8-3e735900d51f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>b606c6c7-8411-4077-995b-804ce6062250</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Acosta, Casey SN - 
                    </value>
      <webElementGuid>03844599-b9d7-4612-b327-f60c23e03530</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-valid-mask ng-valid-maxlength ng-valid-pattern ng-valid-minlength ng-dirty ng-valid-parse ng-valid ng-valid-validation&quot;]/div[@class=&quot;row margin-top-10px text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;form-group form-group-marginZero ng-scope&quot;]/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>528756f8-fb2a-46b7-923f-db43d54472ca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div[2]/div/div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>f9bbd285-fd77-4f42-bb46-7f50e1081f70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Staff(s) for this order*:'])[1]/following::div[3]</value>
      <webElementGuid>5bee96ce-a7ab-4f63-8e9e-53d815184337</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[3]/preceding::div[3]</value>
      <webElementGuid>3e292a45-a517-470d-a057-5c08ea2cd7ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[4]/preceding::div[5]</value>
      <webElementGuid>bd48d993-e0b7-49a7-b64a-6313f77f28fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Acosta, Casey SN -']/parent::*</value>
      <webElementGuid>772b0c14-de31-4352-946f-199693790286</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>a39935c8-a623-44f7-8ad2-4b3608311fdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Acosta, Casey SN - 
                    ' or . = '
                        Acosta, Casey SN - 
                    ')]</value>
      <webElementGuid>5b648e95-f311-4068-aa24-63d25487bfdb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
