<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Chat Support_searchKey</name>
   <tag></tag>
   <elementGuidId>23d6b8ad-36fd-46e3-8613-993fd6ed81a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#searchKey</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='searchKey']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e5207c22-6e45-4fa7-a966-e810a4a3573b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>dd0b8294-d5fb-4316-8c21-79b77e830a91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>3f001f40-5924-4823-97a2-5c90a692d4fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control search-field-holder searchbox ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>17df6746-d2d1-4158-98eb-200d38445b7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search for patient...</value>
      <webElementGuid>13a38cd9-1fb2-4162-9e54-27cb20ecfdb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>c05d27bc-a75d-427c-985d-b6f2174102cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>search.startSearch()</value>
      <webElementGuid>8ad60243-c0e8-48e8-8018-cc68734982ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>search.searchvalue</value>
      <webElementGuid>c972a387-32ce-4439-981a-bc299cdb1f9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-init</name>
      <type>Main</type>
      <value>search.searchvalue=''</value>
      <webElementGuid>17ecea16-3985-428b-8859-4da53ee5da5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 750}</value>
      <webElementGuid>76fd1805-7fe3-41a7-a34b-357deb73a5e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>eda763cd-43a7-43d9-bdb5-82aa0806951e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>50</value>
      <webElementGuid>211d8dfb-628e-4603-88b3-37f705817ef6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchKey&quot;)</value>
      <webElementGuid>e0e3a5f1-ccba-49f8-804f-f4711f16bfaa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='searchKey']</value>
      <webElementGuid>2e6f7b5f-3af3-4870-b6bd-ea9dc52b8485</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/input[3]</value>
      <webElementGuid>c2518b25-a42b-4de1-9525-44f03eeae99e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input[3]</value>
      <webElementGuid>21b71a9e-90f8-46bb-99e4-0bf8be54380b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'searchKey' and @id = 'searchKey' and @placeholder = 'Search for patient...' and @type = 'text']</value>
      <webElementGuid>9f597ffa-33b2-4d7c-9ebf-8f7d0859e95f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
