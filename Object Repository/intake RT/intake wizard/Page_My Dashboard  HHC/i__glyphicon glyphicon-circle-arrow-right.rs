<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i__glyphicon glyphicon-circle-arrow-right</name>
   <tag></tag>
   <elementGuidId>4ace5be3-1336-48f5-bd3b-ca9eda70522c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.glyphicon.glyphicon-circle-arrow-right</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div[3]/div/div/div[2]/div/div[2]/div[2]/div/span/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>78825010-b713-49ec-a1aa-0b02a81ab686</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>piCtrl.copyAddressToMail()</value>
      <webElementGuid>3244e4b8-5723-4033-8f82-b3e773e60b1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-circle-arrow-right</value>
      <webElementGuid>44dc0e71-3404-4780-b0b6-5b68168b9440</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row margin-top-10px&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-4 col-lg-4 line-height30px&quot;]/span[@class=&quot;pull-left&quot;]/i[@class=&quot;glyphicon glyphicon-circle-arrow-right&quot;]</value>
      <webElementGuid>b7d7bb5d-9c8d-4bce-a4a8-8440bd87ed63</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div[3]/div/div/div[2]/div/div[2]/div[2]/div/span/i</value>
      <webElementGuid>f0f6dd43-9d63-4740-a1f8-09ce852690a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/span/i</value>
      <webElementGuid>dbb37382-e76f-4132-93f6-eb59960eb054</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
