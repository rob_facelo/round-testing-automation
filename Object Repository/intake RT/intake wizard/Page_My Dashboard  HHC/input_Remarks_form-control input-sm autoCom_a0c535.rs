<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Remarks_form-control input-sm autoCom_a0c535</name>
   <tag></tag>
   <elementGuidId>4f3fd5dc-6c67-4a83-b1b5-55ae9dcef520</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#selectcarestaffintakewizard > fieldset > autocomplete-directive.ng-isolate-scope > div.dropdown > input.form-control.input-sm.autoCompleteInput.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlength</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[138]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d7e5be12-f399-4735-8e5c-b4eb3c6dbaf7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>03b32805-88e8-45d2-839a-5036293c64e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm autoCompleteInput ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>c2db2e5e-d626-42e5-865a-439a54d1e6f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Select Carestaff</value>
      <webElementGuid>6c0eefcc-f6d9-4269-b179-5acf5017fcac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>model</value>
      <webElementGuid>b7da9a97-7cb7-488e-8ece-4c8fdb690d0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>click-outside</name>
      <type>Main</type>
      <value>closeThis()</value>
      <webElementGuid>1c5780d6-6326-4812-840f-986ecc3ccef0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>searchValue(model,searchtype)</value>
      <webElementGuid>9213af40-411d-4f6f-b240-5be4b00b8525</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-keydown</name>
      <type>Main</type>
      <value>selected=false</value>
      <webElementGuid>81b5ca1b-6c5e-4d64-9da1-88cb973b12bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 500}</value>
      <webElementGuid>c145ea55-1eac-479f-a8db-2cdacedb6c19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>disabled</value>
      <webElementGuid>e73b071d-f07d-4d44-8f16-bf903356b4a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>50</value>
      <webElementGuid>bd46456b-b55b-4ec1-a324-6eb1bd526b6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>checkofselected()</value>
      <webElementGuid>a913526e-7814-4f1b-8ceb-34ddab1a0705</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>9f902d0b-fe79-4cb6-b180-d22ad1997623</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectcarestaffintakewizard&quot;)/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/input[@class=&quot;form-control input-sm autoCompleteInput ng-pristine ng-untouched ng-valid ng-valid-maxlength&quot;]</value>
      <webElementGuid>b08fab4c-ed0d-4c1e-9457-54fb25a7f85b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[138]</value>
      <webElementGuid>7ea58a3a-ed8d-491f-9236-03898160c82d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='selectcarestaffintakewizard']/fieldset/autocomplete-directive/div/input</value>
      <webElementGuid>dbf114db-a300-4baa-a56b-f713ccf30136</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div[2]/div/div[2]/fieldset/autocomplete-directive/div/input</value>
      <webElementGuid>69194b84-f6dc-41f4-bc48-382aebe110b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = 'Select Carestaff']</value>
      <webElementGuid>34dbea51-599a-4838-a6fa-a22a403f08a2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
