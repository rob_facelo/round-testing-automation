<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_3 - Black or African-American</name>
   <tag></tag>
   <elementGuidId>d5c78f3d-10de-45e9-899c-331f536ecb53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Race_EthnicityPanelintakew']/div[2]/div/div[2]/div/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>a02bbee5-b28b-4278-9b5c-20671b2d60a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox-inline</value>
      <webElementGuid>74923181-3c53-4b21-8807-0857496abc47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                    3 - Black or African-American
                                                                                </value>
      <webElementGuid>004636cd-f8ca-46b0-815e-3ec853443cb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Race_EthnicityPanelintakew&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-3 col-lg-3&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/label[@class=&quot;checkbox-inline&quot;]</value>
      <webElementGuid>edfe61ef-4f24-4add-a25e-dfb52105a8e4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Race_EthnicityPanelintakew']/div[2]/div/div[2]/div/div/label</value>
      <webElementGuid>773de733-b163-4bfa-a5f9-7698e7d47149</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mark all that apply'])[1]/following::label[3]</value>
      <webElementGuid>27894f4e-1a1b-4201-8d68-fa2b7436e527</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='3 - Black or African-American']/parent::*</value>
      <webElementGuid>d5e9b7ff-9ab3-4bdd-81ae-1081f8d4f981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div[2]/div/div[2]/div/div/label</value>
      <webElementGuid>6e6342f8-fab9-49c7-a2e1-04780ac9ee80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                                                                                    3 - Black or African-American
                                                                                ' or . = '
                                                                                    3 - Black or African-American
                                                                                ')]</value>
      <webElementGuid>07caa156-8576-434e-912b-238c4fcf492c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
