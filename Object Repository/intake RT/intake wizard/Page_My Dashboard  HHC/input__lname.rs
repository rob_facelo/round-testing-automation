<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__lname</name>
   <tag></tag>
   <elementGuidId>56eb8b8d-3db0-4113-9567-05853e845303</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;lname&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='lname']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9ee83501-8f90-4d8f-be95-9cce8122e19c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>6de2db07-29f1-4792-9b31-2932a12935e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>lname</value>
      <webElementGuid>cff34f17-147c-400c-a233-79ef007b941d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>25</value>
      <webElementGuid>322b7cda-ea61-49a9-9834-04203bca3ad9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>piCtrl.PatientInfo.LastName</value>
      <webElementGuid>64d8b1fe-1698-44da-b1d4-5e0ab3d5e713</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-pattern</name>
      <type>Main</type>
      <value>/^[\w|\s|\ñ|\Ñ|\-|\.|\,|\']{0,}$/</value>
      <webElementGuid>b7af3a20-3dd5-49dd-90c1-e4d698f5e354</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm font14 ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern ng-valid-maxlength</value>
      <webElementGuid>6eff8c26-2112-4455-b13d-2a9f675a6ed2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-8 col-lg-8&quot;]/input[@class=&quot;form-control input-sm font14 ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern ng-valid-maxlength&quot;]</value>
      <webElementGuid>242294e2-6626-4bb5-bc09-816fd3d2fca7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='lname']</value>
      <webElementGuid>62773bfc-9f30-4c00-a280-810ddd2a11ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div[2]/div[2]/div[2]/input</value>
      <webElementGuid>16f0f347-f093-4879-8578-a1a2d67d8399</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/div[2]/input</value>
      <webElementGuid>5e53295f-c15f-41ab-99f6-2a9d20669952</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'lname']</value>
      <webElementGuid>a12f1113-5a8b-459e-b1b7-2fec1b21f52f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
