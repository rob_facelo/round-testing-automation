<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_walker, sarah - RN</name>
   <tag></tag>
   <elementGuidId>2d40ebaf-938b-4008-85c0-f6d3acf3c9e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>fieldset > autocomplete-directive.ng-isolate-scope > div.dropdown > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
                        walker, sarah - RN
                    ' or . = '
                        walker, sarah - RN
                    ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='selectcarestaffintakewizard']/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3bd7cd14-c272-4896-9d64-e411a66b45c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>fe9e972c-0c54-4340-a2bc-0188ee1e1b91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        walker, sarah - RN
                    </value>
      <webElementGuid>dc4cdfdf-ca11-4244-bd3b-7dd04fc4054c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectcarestaffintakewizard&quot;)/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>5efb8d34-ed32-4a59-bd24-35f1bd45f94b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='selectcarestaffintakewizard']/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>0deecfba-281a-45bd-a070-44a79bedbec1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/following::div[7]</value>
      <webElementGuid>4f553bb9-1799-40fa-8a95-a64a76d72a92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[7]/following::div[7]</value>
      <webElementGuid>f194e4fb-280d-4d68-83e8-f8c8ade1fd17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[7]/preceding::div[3]</value>
      <webElementGuid>7fb1cf26-a9c5-4df3-8bbc-76ba9ddab352</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks:'])[2]/preceding::div[3]</value>
      <webElementGuid>2ae06905-36c2-4feb-92d3-875fd7e6bf26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='walker, sarah - RN']/parent::*</value>
      <webElementGuid>2c700c8a-2113-4c99-847e-9bee751a2193</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>3a96fa9a-5681-478e-96da-c8a52a407b47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        walker, sarah - RN
                    ' or . = '
                        walker, sarah - RN
                    ')]</value>
      <webElementGuid>eb7de4c7-1d30-402b-ad29-f794877ea8fc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
