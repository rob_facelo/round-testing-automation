<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Save_button</name>
   <tag></tag>
   <elementGuidId>7c22a16a-f47e-4107-8a93-080934a5c6e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-floppy-o</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div/div/button/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>94b1ee9f-b093-45da-8437-e535d33ac056</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-floppy-o</value>
      <webElementGuid>5cb58f93-93e1-451a-b6e2-2cd4b6b35b1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12 font-weight-600&quot;]/i[@class=&quot;fa fa-floppy-o&quot;]</value>
      <webElementGuid>d434fbcd-680b-41c5-abd7-680a05198cb1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div/div/button/i</value>
      <webElementGuid>653925f8-9572-4914-8b07-cc80c501f77d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]/div/div/div/div/div/div/button/i</value>
      <webElementGuid>32046a37-e25b-494a-b001-0a636529e358</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
