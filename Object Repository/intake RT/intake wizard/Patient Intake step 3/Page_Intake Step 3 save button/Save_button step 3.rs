<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Save_button step 3</name>
   <tag></tag>
   <elementGuidId>e68372f3-8726-415e-923a-2ba7a3ce571b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.well > div > div.modal-header > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > button.btn.btn-default.font12 > i.fa.fa-floppy-o.font-weight-600</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[1]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12&quot;]/i[@class=&quot;fa fa-floppy-o font-weight-600&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='intakewinfo']/div[4]/div/div/div/div/div/button/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>0f99c1f0-bc6d-4222-85b6-baaf18bad1af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-floppy-o font-weight-600</value>
      <webElementGuid>1e09642c-2a1b-4ecb-96eb-d12c10253c86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[1]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12&quot;]/i[@class=&quot;fa fa-floppy-o font-weight-600&quot;]</value>
      <webElementGuid>0f1c894b-5927-4367-9553-ab8a5fcf0a23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[4]/div/div/div/div/div/button/i</value>
      <webElementGuid>6373e6d8-fa6d-4221-9f00-603ab026252a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div/div/button/i</value>
      <webElementGuid>6b7e38e7-bf71-4968-975a-d5892fa38e0e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
