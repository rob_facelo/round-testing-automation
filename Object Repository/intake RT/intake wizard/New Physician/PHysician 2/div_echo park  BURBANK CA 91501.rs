<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_echo park  BURBANK CA 91501</name>
   <tag></tag>
   <elementGuidId>b26137c6-9d4c-4092-a4bc-2295065a3ebc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='intakeprimaryphysician']/autocomplete-directive/div/ul/li/a/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>514b1cf8-bef2-4ea2-addb-483feaeff244</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>011a0c43-ebde-444e-bfc9-203284b8749d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        echo park  BURBANK CA 91501
                    </value>
      <webElementGuid>e830315c-5faa-4964-ab7c-437926561ad9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeprimaryphysician&quot;)/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>0dff4329-26bb-4d94-acd6-22a0292a56a3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='intakeprimaryphysician']/autocomplete-directive/div/ul/li/a/div[2]/div</value>
      <webElementGuid>9cf1ad82-2389-4b3b-980c-fb29d56ddd17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='bartowski, ellie , MD -'])[2]/following::div[2]</value>
      <webElementGuid>926122a0-2133-4a6c-9d61-1bf34ea19869</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Primary:'])[1]/following::div[6]</value>
      <webElementGuid>53a92611-1520-4a44-996f-b09e99bcdde8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[44]/preceding::div[1]</value>
      <webElementGuid>45c31be0-023c-48a3-983a-71f6912469f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Secondary:'])[1]/preceding::div[3]</value>
      <webElementGuid>a1f17218-f762-4ae2-8462-15832804e4ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/div[2]/div/div/div/div/autocomplete-directive/div/ul/li/a/div[2]/div</value>
      <webElementGuid>102dac1f-3f02-4665-a7fb-e2c36eb4e63e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        echo park  BURBANK CA 91501
                    ' or . = '
                        echo park  BURBANK CA 91501
                    ')]</value>
      <webElementGuid>00266d9e-6cbd-438d-a32b-d6c9040281cb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
