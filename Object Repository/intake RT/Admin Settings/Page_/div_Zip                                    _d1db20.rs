<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Zip                                    _d1db20</name>
   <tag></tag>
   <elementGuidId>aa09b196-1d75-4fad-9947-c27a7d138823</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[8]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d3586764-4976-42de-b462-a1ad11c1d901</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>bad20d79-915a-44b7-82bc-4699cabab78c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            </value>
      <webElementGuid>7c3facf6-3c60-4607-8f04-ba4cba19e4d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>3c5f37ed-4b59-42c4-bdf6-25fcea6a2623</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[8]</value>
      <webElementGuid>e71fab8c-b16e-4867-84b6-d63ca72e2ed9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::div[2]</value>
      <webElementGuid>e4a125ed-a0cb-4206-8240-f922c8d1af65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div/div/table/tbody/tr/td/div[8]</value>
      <webElementGuid>93d1a263-fd71-4037-868c-00c905897bc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            ' or . = '
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            ')]</value>
      <webElementGuid>c3f71eef-e142-494f-90c1-889e16e53524</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
