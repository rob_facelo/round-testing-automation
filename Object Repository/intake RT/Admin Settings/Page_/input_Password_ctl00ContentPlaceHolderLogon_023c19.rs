<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_ctl00ContentPlaceHolderLogon_023c19</name>
   <tag></tag>
   <elementGuidId>f44cfc25-e37a-416f-ad5e-1a4ec20a088a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolderLogonPage_LoginControl_loginbtn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>bb6b90d9-0781-4f2d-8706-fe4534604375</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>6bbfe511-71ae-4401-bdea-4f35583cf497</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolderLogonPage$LoginControl$loginbtn</value>
      <webElementGuid>0b299ed2-74f3-454f-9cdf-4639df4c14aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>LOGIN</value>
      <webElementGuid>34f11bd4-ba3c-413c-bf1b-12f18453c1c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolderLogonPage_LoginControl_loginbtn</value>
      <webElementGuid>634a6d52-fd6d-4630-ae8d-9fc15cd954b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-btn-style btn btn-primary</value>
      <webElementGuid>42b3063e-6ab8-42e7-8e64-32528d223d2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolderLogonPage_LoginControl_loginbtn&quot;)</value>
      <webElementGuid>68a0dd6a-26c3-442e-b1f7-88d56cf110f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      <webElementGuid>b24ecc74-b80d-4044-818b-d4617dd9c621</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]/input</value>
      <webElementGuid>16056eaf-3469-4786-8c2a-aa68030e3dd7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>89f0f303-3169-4f08-aa72-c4f66edd6bad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'ctl00$ContentPlaceHolderLogonPage$LoginControl$loginbtn' and @id = 'ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      <webElementGuid>505a421c-c791-45ec-b956-fd3549375b6a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
