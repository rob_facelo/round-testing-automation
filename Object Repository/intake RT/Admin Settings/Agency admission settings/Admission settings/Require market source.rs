<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Require market source</name>
   <tag></tag>
   <elementGuidId>42742da1-d998-43ff-ab8c-1ada242def35</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.ng-valid.ng-dirty.ng-valid-parse.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'checkbox' and @ng-model = 'AgencyDetails.RequireReferralMarketSource']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[25]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>26e3a2df-ba2e-49b1-93dd-fc7940fc1833</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>c42eeee1-7a15-435a-94af-450c0bbfc142</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>AgencyDetails.RequireReferralMarketSource</value>
      <webElementGuid>e2c3dbd5-db8c-47f7-afe2-577faeea3a5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-valid ng-dirty ng-valid-parse ng-touched</value>
      <webElementGuid>da826fb2-2c0d-441a-b5ec-e2ed1c96c946</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlAdmission_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/label[@class=&quot;checkbox-inline&quot;]/input[@class=&quot;ng-valid ng-dirty ng-valid-parse ng-touched&quot;]</value>
      <webElementGuid>465aef4b-7285-413f-b873-89767fe2d5cd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[25]</value>
      <webElementGuid>2f0db32e-08c8-43c0-a972-432a4dd4d3af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAdmission_RPC']/div[2]/div/label/input</value>
      <webElementGuid>fdd0ee5a-34d2-4a23-afdd-60f16b805975</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/div/table/tbody/tr/td/div[2]/div/label/input</value>
      <webElementGuid>a21f2616-3964-4247-9921-5fe543c8d1d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>9cfa339c-a5b8-4c9f-aba1-3055f1e7c9e3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
