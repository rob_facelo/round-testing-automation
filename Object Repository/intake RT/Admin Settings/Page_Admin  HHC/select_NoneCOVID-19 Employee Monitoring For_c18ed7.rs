<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_NoneCOVID-19 Employee Monitoring For_c18ed7</name>
   <tag></tag>
   <elementGuidId>56f0d8b1-ae92-4d7f-8c5b-744a1783aa47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.checkbox-inline > select.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlUsers_RPC']/div[13]/div/table/tbody/tr/td/label/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>28c80cfa-428d-4cf9-b4c8-8cf50140a248</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>AgencyDetails.ActiveForm</value>
      <webElementGuid>f24fc852-e79b-473e-9e1f-10b35432e229</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>42a25ebe-da0d-45d8-8f21-d20b4598ad74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        NoneCOVID-19 Employee Monitoring FormCOVID-19 PUI Screening Form (CDC)
                                                    </value>
      <webElementGuid>6a441e13-a550-4c3c-8f92-ae7dd424be0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlUsers_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/table[1]/tbody[1]/tr[1]/td[1]/label[@class=&quot;checkbox-inline&quot;]/select[@class=&quot;ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>d00a0df0-ef29-49cf-bd77-635404c93812</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlUsers_RPC']/div[13]/div/table/tbody/tr/td/label/select</value>
      <webElementGuid>b5c10a3f-a3b7-4b07-80eb-0387f6578557</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active Form'])[1]/following::select[1]</value>
      <webElementGuid>fd900cf5-1dff-44ac-a4e0-ee578b65545b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Calendar'])[1]/preceding::select[1]</value>
      <webElementGuid>2b6d4169-f112-4f3e-972a-9b1c4f4f3ffe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/select</value>
      <webElementGuid>996e0233-872f-4fac-bb37-093f3730f714</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        NoneCOVID-19 Employee Monitoring FormCOVID-19 PUI Screening Form (CDC)
                                                    ' or . = '
                                                        NoneCOVID-19 Employee Monitoring FormCOVID-19 PUI Screening Form (CDC)
                                                    ')]</value>
      <webElementGuid>e4922feb-0eb1-457b-966d-d9f3e615a3ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
