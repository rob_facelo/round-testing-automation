<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_RN Direct Care</name>
   <tag></tag>
   <elementGuidId>0e9afcb5-71ae-44e8-9432-943009ad1213</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_VisitTypeControl_grvVisitTypes_cell26_0_hlnkVisitTypeId_26</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_VisitTypeControl_grvVisitTypes_cell26_0_hlnkVisitTypeId_26']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>39e7849d-fbf2-4786-967c-1d56fa8940a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeHyperlink</value>
      <webElementGuid>cfb225e5-d754-43f7-a15d-568a5223224e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_VisitTypeControl_grvVisitTypes_cell26_0_hlnkVisitTypeId_26</value>
      <webElementGuid>541f0df9-63ba-4fea-a2dd-ad8b910937e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>VisitTypeDetails.aspx?visitTypeId=1065</value>
      <webElementGuid>95a540a6-e21a-47fc-b1ca-766e3ab24ccc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>RN Direct Care</value>
      <webElementGuid>e942be24-d447-4cbf-90d1-d48e15b484de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_VisitTypeControl_grvVisitTypes_cell26_0_hlnkVisitTypeId_26&quot;)</value>
      <webElementGuid>c64ad116-caf1-4814-b01c-7cdabeef117b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_VisitTypeControl_grvVisitTypes_cell26_0_hlnkVisitTypeId_26']</value>
      <webElementGuid>a7b580db-f8f7-40f2-b079-ffce5ad4e6ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_VisitTypeControl_grvVisitTypes_tccell26_0']/a</value>
      <webElementGuid>b64b0767-6bdf-42d6-87c9-c04332c766a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'RN Direct Care')]</value>
      <webElementGuid>3b4ef487-3b87-49d0-87f6-46df32918bba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='G0299'])[1]/following::a[3]</value>
      <webElementGuid>147d6626-4cbf-48f0-84c5-1337679c1ac7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[11]/following::a[3]</value>
      <webElementGuid>64f6a297-fc7f-4a5b-93f0-71ca2624a11d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SN'])[2]/preceding::a[1]</value>
      <webElementGuid>70e60463-e06d-4c88-ab98-dfcdbcc5deb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active'])[21]/preceding::a[1]</value>
      <webElementGuid>5f692aff-9776-487c-8cea-87c4fa044240</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='RN Direct Care']/parent::*</value>
      <webElementGuid>a9f8929f-b655-45cd-bd25-9886c7b9f990</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'VisitTypeDetails.aspx?visitTypeId=1065')]</value>
      <webElementGuid>4b6d9404-bfcd-4e56-b4bd-b1b2d3f80b69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[29]/td[2]/a</value>
      <webElementGuid>bdd6bf2f-6179-4cc5-b9ad-e3043c70a544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'ContentPlaceHolder_VisitTypeControl_grvVisitTypes_cell26_0_hlnkVisitTypeId_26' and @href = 'VisitTypeDetails.aspx?visitTypeId=1065' and (text() = 'RN Direct Care' or . = 'RN Direct Care')]</value>
      <webElementGuid>d88d8371-261a-41e0-b135-94b236a56276</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
