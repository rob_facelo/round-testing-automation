<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Copy Agency Address</name>
   <tag></tag>
   <elementGuidId>ce888b30-f66c-4cfb-a7aa-120880e86155</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-xs</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlBillingMailingInformation_RPC']/div[2]/div/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e23ee592-78d3-4caf-a331-60778396ae5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-xs</value>
      <webElementGuid>91bff455-5942-45a7-a3c4-7e25f86fc32e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>ac.copyAgencyAddressToBilling()</value>
      <webElementGuid>f9b89dbb-d0a5-4945-a52a-047f1d1b038a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Copy Agency Address
                                            </value>
      <webElementGuid>fd498f79-0353-43d9-bbfe-cae78399c2ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlBillingMailingInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row margin-bottom&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/button[@class=&quot;btn btn-primary btn-xs&quot;]</value>
      <webElementGuid>d7bb09bf-2e58-4b91-b9c6-f26f5f98dd06</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlBillingMailingInformation_RPC']/div[2]/div/div/div/button</value>
      <webElementGuid>7ad01c01-f0f8-4f2b-9bf8-8623de4f6425</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing - Mailing Information'])[1]/following::button[1]</value>
      <webElementGuid>8ae7d1d0-9fd4-48e4-95d9-ec9f6202b016</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact / Department'])[1]/preceding::button[1]</value>
      <webElementGuid>9dc56b4d-9e23-4993-992e-5563bedd6c3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Copy Agency Address']/parent::*</value>
      <webElementGuid>2ddf6a92-8d8b-40cd-9391-8938b688ccaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div[2]/div/div/div/button</value>
      <webElementGuid>45d0e7eb-924f-454a-ad53-e167ad334f10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                                Copy Agency Address
                                            ' or . = '
                                                Copy Agency Address
                                            ')]</value>
      <webElementGuid>82ba4164-9fce-48dd-88fd-5a34df7604e0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
