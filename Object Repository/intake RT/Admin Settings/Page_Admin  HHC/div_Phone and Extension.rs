<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Phone and Extension</name>
   <tag></tag>
   <elementGuidId>c52d478a-00de-4926-8eb6-9afc02016d8b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[9]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ac8eb3b4-7c51-40f5-9f92-80fd80c5b8c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>cff6797e-492d-4051-8ca5-a98f36e87829</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    Phone and Extension*
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                    
                                
                            </value>
      <webElementGuid>d44fd128-43a7-42c8-af9a-45469cf306d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>1728de92-7526-4642-b11b-459177393864</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[9]</value>
      <webElementGuid>a9e2069a-a934-42a5-9297-d3d1ba728642</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip Code'])[2]/following::div[2]</value>
      <webElementGuid>2c23adaa-175b-477e-a3cb-e65a87e57953</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State'])[3]/following::div[2]</value>
      <webElementGuid>a697cc84-67d0-4181-a5d0-3d9e509348ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/table/tbody/tr/td/div[9]</value>
      <webElementGuid>b5e3bda5-f205-4b67-ac42-7913ee7f9f1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    Phone and Extension*
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                    
                                
                            ' or . = '
                                
                                    Phone and Extension*
                                
                                
                                    
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                    
                                
                            ')]</value>
      <webElementGuid>47f733b1-b340-4d64-912d-1a73c8e24d09</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
