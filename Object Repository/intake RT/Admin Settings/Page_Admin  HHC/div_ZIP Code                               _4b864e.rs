<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_ZIP Code                               _4b864e</name>
   <tag></tag>
   <elementGuidId>957c1f81-9dbf-4ecb-8bdf-107a0b4ce2e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[8]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>009a7947-fa8a-4176-8aea-ee5b0396d6a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>62ff4142-11ee-4eff-9a3f-a63a8fe65282</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    ZIP Code *
                                
                                
                                    
                                        
                                            
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                        
                                        
                                            
                                        
                                    
                                
                            </value>
      <webElementGuid>c4e3da10-87c7-4f5d-9caa-60db0a6e49cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>dc423a02-3a7a-4347-94be-12e6f6923d09</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlAgencyInformation_RPC']/div[8]</value>
      <webElementGuid>a06b7740-13bd-4cbe-b39a-dd1899981311</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NOTE: City and State will be populated automatically by entering the ZIP code.'])[1]/following::div[1]</value>
      <webElementGuid>88d64919-1c79-4a77-bb5c-f100f850243f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State'])[2]/following::div[4]</value>
      <webElementGuid>ce0c3cb5-2f35-4a7b-bd8c-a39a47b8839e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/table/tbody/tr/td/div[8]</value>
      <webElementGuid>ae508c52-f5cb-46f1-bc8e-54186edc730f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    ZIP Code *
                                
                                
                                    
                                        
                                            
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                        
                                        
                                            
                                        
                                    
                                
                            ' or . = '
                                
                                    ZIP Code *
                                
                                
                                    
                                        
                                            
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                        
                                        
                                            
                                        
                                    
                                
                            ')]</value>
      <webElementGuid>9f8fba96-d7c5-4a61-8c97-795c09724c91</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
