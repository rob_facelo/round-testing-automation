<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_State Agency ID</name>
   <tag></tag>
   <elementGuidId>dae2a35a-26db-4b3c-a9ec-027b871a1595</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-6.col-lg-6 > input.form-control.input-sm.font14.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlength</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[34]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5858bfaa-b607-499c-94eb-72daba7474a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>75cacc8b-1b38-4f8f-acb2-4c2b62b07efe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm font14 ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>410d67df-b9a7-4a2b-9a4b-b5b6658c6148</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>16</value>
      <webElementGuid>9396d31d-2b6f-4e3d-8858-eaf9eebbe830</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>AgencyDetails.HHA_Agency_Id</value>
      <webElementGuid>e32748ee-e9bd-46c8-9791-60aac64f66e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_AgencyControl_pnlStateInformation_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/input[@class=&quot;form-control input-sm font14 ng-pristine ng-untouched ng-valid ng-valid-maxlength&quot;]</value>
      <webElementGuid>4ebbd04e-fa2c-4313-b8c8-267dc92891a2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[34]</value>
      <webElementGuid>5ce370fa-c10c-440c-be8f-82797e08fafb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_AgencyControl_pnlStateInformation_RPC']/div[2]/div[2]/input</value>
      <webElementGuid>0dcd7310-c057-4150-be59-089eebb583b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div[2]/div[2]/input</value>
      <webElementGuid>1a1affaf-2e15-4bab-8558-5176d4c6940f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>55ee5257-9a75-4955-a568-512b9c00abe9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
