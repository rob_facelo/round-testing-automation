<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_walker, sarah</name>
   <tag></tag>
   <elementGuidId>5194808a-8e7a-450c-ae6b-964ed6643e97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#searchCaregiverOasis > ul.dropdown-menu > li.ng-scope > a.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchCaregiverOasis']/ul/li/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>54bb2339-948f-490d-ae80-9a5d9480316a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.setCompletingCaregiver(caregiver.caregiver_name, caregiver.caregiver_id)</value>
      <webElementGuid>3c2e85ca-df24-4ecf-8cee-f5c6c9ef4bfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-binding</value>
      <webElementGuid>c3a98fa0-bc01-4449-83de-6e789768da5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>walker, sarah </value>
      <webElementGuid>32852c9b-4eb8-4f9c-8978-2c1585011f47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchCaregiverOasis&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-binding&quot;]</value>
      <webElementGuid>a43d83f2-617e-41e4-aa7b-27874912d045</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchCaregiverOasis']/ul/li/a</value>
      <webElementGuid>f70788d7-b8b7-456a-aa66-0fdcb5884530</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'walker, sarah')]</value>
      <webElementGuid>7edf8230-9bed-43e2-a14e-f38960082a69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Care Staff Name:'])[1]/following::a[1]</value>
      <webElementGuid>7338dd7a-c165-43bf-8a9e-65cc069ec954</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OT'])[1]/following::a[1]</value>
      <webElementGuid>eaa78709-4b76-4da2-bcac-a9cf32a692e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0090. Date Assessment Completed'])[1]/preceding::a[1]</value>
      <webElementGuid>a8c9d2c0-585c-4f10-b7b0-291c12f173fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0100. This Assessment is Currently Being Completed for the Following Reason'])[1]/preceding::a[1]</value>
      <webElementGuid>c2311cc9-debf-4351-9e7e-0b631d083274</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='walker, sarah']/parent::*</value>
      <webElementGuid>7450c7dc-2030-44a6-8539-67dd767c0e44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div[2]/ul/li/a</value>
      <webElementGuid>678ecfb2-d5ef-451c-a23e-f4f053682fbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'walker, sarah ' or . = 'walker, sarah ')]</value>
      <webElementGuid>0bbc3821-2eb7-4017-ba0d-67338c9e2150</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
