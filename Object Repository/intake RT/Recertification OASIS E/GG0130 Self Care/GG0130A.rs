<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0130A</name>
   <tag></tag>
   <elementGuidId>030eecd2-8239-4466-a98a-6eadb135ccc9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-offset-4.col-sm-offset-4.col-md-offset-4.col-lg-offset-4.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0130A4' and (text() = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ' or . = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>20460de3-1c70-4f74-b20d-e6775e311520</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>c699213a-3774-41d8-a179-3c20859d19f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0130A4</value>
      <webElementGuid>58399d47-1c89-4229-a0fd-f63a934bb114</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>f533ac36-013e-4bb5-abd7-ec5a67b0c192</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0130A4</value>
      <webElementGuid>c609202b-ddd6-49dc-9258-d8c973d6e40f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0130A4', oasis.oasisDetails.GG0130A4)</value>
      <webElementGuid>88a921d8-d106-42b5-a951-1a9651fb0f70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>21e4378e-daef-4262-947c-8ed83f3e2ce1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            </value>
      <webElementGuid>d2fbe64c-c551-4bb0-9679-4b3dff3498ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0130&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-11 col-sm-11 col-md-11 col-lg-11&quot;]/div[@class=&quot;col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>2ffc9f6c-32fc-4fff-927b-308bf2f28fd9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/select</value>
      <webElementGuid>6f2ebe32-96c4-404b-8628-a0d9566e11d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[11]/following::select[1]</value>
      <webElementGuid>a43f8da7-e7fa-4396-9774-0deb5bc35474</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Eating:'])[1]/preceding::select[1]</value>
      <webElementGuid>69f35b91-07b4-47e0-98cc-56cad2043e83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oral Hygiene:'])[1]/preceding::select[2]</value>
      <webElementGuid>4aed5c08-0a2d-44f9-8bec-6f7ec9ae896a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div/div/div/select</value>
      <webElementGuid>48c78495-5705-45d9-96e7-c4346c17ccd7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ' or . = '
                                                                
                                                                01
                                                                02
                                                                03
                                                                04
                                                                05
                                                                06
                                                                07
                                                                09
                                                                10
                                                                88
                                                                -
                                                            ')]</value>
      <webElementGuid>0b356952-4c91-454e-8ffc-856476801bf1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
