<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1023 Checkbox 1</name>
   <tag></tag>
   <elementGuidId>e5717bac-f392-4d5a-9836-2c8a5abc48f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td[name=&quot;M1023&quot;] > div.form-inline.col-xs-12.col-sm-12.col-md-12.col-lg-12.col-col-xs-offset-2.col-sm-offset-2.col-md-offset-2.col-lg-offset-2 > input.ng-valid.ng-dirty.ng-valid-parse.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'checkbox' and @ng-model = 'oasis.oasisDetails.M0240_OTH_DIAG1_SEVERITY' and @ng-true-value = concat(&quot;'&quot; , &quot;01&quot; , &quot;'&quot;)]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[57]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d699ef1a-b886-4452-9713-4eba47508b5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>d4b3e32d-28f8-4e8a-b8b6-5c17b3cf5f8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M0240_OTH_DIAG1_SEVERITY</value>
      <webElementGuid>3aa9f676-e703-43c6-90b5-681dd63e5752</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>'01'</value>
      <webElementGuid>95ad0d92-5345-4614-bf04-763a475a2de5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M0240_OTH_DIAG1_SEVERITY', oasis.oasisDetails.M0240_OTH_DIAG1_SEVERITY)</value>
      <webElementGuid>52dcdaa8-bacc-4d63-8e50-be44c3ff6731</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked || oasis.oasisDetails.M0240_OTH_DIAG1_ICD == '='</value>
      <webElementGuid>3bdc0644-df62-495f-8f97-8b583297aa9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-valid ng-dirty ng-valid-parse ng-touched</value>
      <webElementGuid>511724b3-c190-4939-b2f8-94b6a4a4c2fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1023&quot;)/td[2]/div[@class=&quot;form-inline col-xs-12 col-sm-12 col-md-12 col-lg-12 col-col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2&quot;]/input[@class=&quot;ng-valid ng-dirty ng-valid-parse ng-touched&quot;]</value>
      <webElementGuid>c84679b8-620e-4477-9f94-4c14badbfc37</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[57]</value>
      <webElementGuid>25aedadf-292d-4611-8baf-2caab1527579</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='M1023']/td[2]/div[4]/input[2]</value>
      <webElementGuid>2dc2c37f-7b26-4967-9202-02c0c159f595</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/input[2]</value>
      <webElementGuid>4b6fae34-77ef-48a9-be1a-4f3b172ec5ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>c61ffe3d-9208-436e-81b5-cc5af6e5345c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
