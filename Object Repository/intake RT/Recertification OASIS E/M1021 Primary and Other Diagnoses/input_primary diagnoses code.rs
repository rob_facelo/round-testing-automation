<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_primary diagnoses code</name>
   <tag></tag>
   <elementGuidId>f4a40d2f-1c68-4fc4-9268-4fdc494da5a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.form-control.input-sm.ng-valid.ng-valid-minlength.ng-valid-maxlength.ng-touched.ng-dirty.ng-valid-parse</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;M1021&quot;)/td[2]/div[@class=&quot;col-xs-9 col-sm-9 col-md-9 col-lg-9 row&quot;]/div[@class=&quot;input-group space&quot;]/autocomplete-directive[@id=&quot;M0230_PRIMARY_DIAG_ICD&quot;]/div[@class=&quot;dropdown&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;form-control input-sm ng-valid ng-valid-minlength ng-valid-maxlength ng-touched ng-dirty ng-valid-parse&quot;][count(. | //*[@type = 'text' and @class = 'form-control input-sm ng-valid ng-valid-minlength ng-valid-maxlength ng-touched ng-dirty ng-valid-parse']) = count(//*[@type = 'text' and @class = 'form-control input-sm ng-valid ng-valid-minlength ng-valid-maxlength ng-touched ng-dirty ng-valid-parse'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[86]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>411e4732-4efd-422f-bdfc-071cd0f5b2e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>c624bca0-60e7-4646-8ff2-ec5c52633265</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M0230_PRIMARY_DIAG_ICD</value>
      <webElementGuid>cdab26f8-6e4b-4e8a-8b2e-a4a5a691c016</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-valid ng-valid-minlength ng-valid-maxlength ng-touched ng-dirty ng-valid-parse</value>
      <webElementGuid>30b7b49d-ccb7-47dd-b33d-fd40c7f141a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>model</value>
      <webElementGuid>c724d7ed-c571-4960-8310-17f6f43b5899</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>click-outside</name>
      <type>Main</type>
      <value>closeThis()</value>
      <webElementGuid>fb713197-80ef-4412-81a5-c6ef38e8f4e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>searchValue(model,searchtype)</value>
      <webElementGuid>dbed932a-8d0a-4464-9c5e-69de42b16231</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>searchValue(model,searchtype)</value>
      <webElementGuid>9c555db9-34a0-48fc-9803-26d032ce2e4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-keydown</name>
      <type>Main</type>
      <value>selected=false</value>
      <webElementGuid>3e354f11-3ba6-46bb-93dc-88da48c63396</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-minlength</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>5154d0c5-5aac-4dcc-82c8-8f321e0fc856</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>customDisabled</value>
      <webElementGuid>587cdb4c-0d5c-416f-a3ad-f7e7e43ee8a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 500}</value>
      <webElementGuid>bd713a8f-bad2-42ba-a059-c609de61bda7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>9f115045-1eef-4ca5-a089-e534edc4fc37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1021&quot;)/td[2]/div[@class=&quot;col-xs-9 col-sm-9 col-md-9 col-lg-9 row&quot;]/div[@class=&quot;input-group space&quot;]/autocomplete-directive[@id=&quot;M0230_PRIMARY_DIAG_ICD&quot;]/div[@class=&quot;dropdown&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;form-control input-sm ng-valid ng-valid-minlength ng-valid-maxlength ng-touched ng-dirty ng-valid-parse&quot;]</value>
      <webElementGuid>52583b89-118d-465d-a31a-87b3eb1aa174</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[86]</value>
      <webElementGuid>1bd111d8-d6f4-45b5-98c4-3caa2c7b38dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//autocomplete-directive[@id='M0230_PRIMARY_DIAG_ICD']/div/div/div/input)[2]</value>
      <webElementGuid>f7cf15fd-d5db-4b61-8b53-5b2ecaeb777e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/autocomplete-directive/div/div/div/input</value>
      <webElementGuid>9ccdf78f-f4d0-4323-a818-15bff2850a60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>e3460ec2-b784-4c43-a5d6-03a92ec00908</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
