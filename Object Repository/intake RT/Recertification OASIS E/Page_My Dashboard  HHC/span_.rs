<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_</name>
   <tag></tag>
   <elementGuidId>b250d2ba-c612-4fbf-b666-64bcefb4e2e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#HomeContent > div.modal-dialog.modal-lg > div.modal-content > div.modal-header > button.close > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='HomeContent']/div/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6e1cf48c-7a72-4e57-a796-14edb9a8d177</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>40a34ca9-773a-47bf-ae0b-c06acc2a9e90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>×</value>
      <webElementGuid>a1e0dd18-4b5c-4e34-b0f2-aa7d32f8db7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;HomeContent&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]/button[@class=&quot;close&quot;]/span[1]</value>
      <webElementGuid>af3f53f7-d69d-4d70-981b-651de4f6b4cb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='HomeContent']/div/div/div/button/span</value>
      <webElementGuid>cf9b64ad-bcc6-4245-a39c-702765bdfe45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[3]/following::span[1]</value>
      <webElementGuid>e9aaf75e-e57b-4ed8-abd4-c2203be36383</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Printer Friendly'])[1]/following::span[1]</value>
      <webElementGuid>44d95bd1-e355-472c-a56e-b015a6905e30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CENTRE'])[1]/preceding::span[2]</value>
      <webElementGuid>bd7b93fa-e19b-4239-a586-4c8808ab9cff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/button/span</value>
      <webElementGuid>19bc0098-1c77-47de-a74d-884e4f30ece5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '×' or . = '×')]</value>
      <webElementGuid>dc1a4c84-eee2-4a96-af22-e3bf89e534ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
