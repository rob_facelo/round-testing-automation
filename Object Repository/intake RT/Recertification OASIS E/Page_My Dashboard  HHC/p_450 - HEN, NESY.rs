<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_450 - HEN, NESY</name>
   <tag></tag>
   <elementGuidId>fdca7b1c-78c7-4c2f-9af5-217b2e710da3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.text-primary.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchresultslist']/ul/li/a/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>5f82d88e-24c3-4f80-8613-261e814475a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-primary ng-binding</value>
      <webElementGuid>c3272cec-df07-420c-80f1-a3063f542d57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>450 - HEN, NESY</value>
      <webElementGuid>28d33135-3f33-4517-b484-dd2ffa71c264</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchresultslist&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-scope&quot;]/p[@class=&quot;text-primary ng-binding&quot;]</value>
      <webElementGuid>b8952c35-9217-445f-8ab8-e28ad5873265</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/ul/li/a/p</value>
      <webElementGuid>f30c3d93-027f-46d9-8d5f-66fa09da0fe5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chat Support'])[1]/following::p[1]</value>
      <webElementGuid>49b3a41a-cecf-49d3-8c36-b97cd5cd1247</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::p[4]</value>
      <webElementGuid>45053f75-b07a-4795-b73f-bca04773cf6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Admin'])[1]/preceding::p[4]</value>
      <webElementGuid>25373c77-2f68-4be3-8bf4-ada5374fac25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='450 - HEN, NESY']/parent::*</value>
      <webElementGuid>75af129f-a9c6-41fa-933f-403b0ed20caa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/p</value>
      <webElementGuid>9881ac47-cef5-4530-a505-b327b60b9c7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '450 - HEN, NESY' or . = '450 - HEN, NESY')]</value>
      <webElementGuid>b1806a01-1b15-4842-817a-4ed5f65cb3ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
