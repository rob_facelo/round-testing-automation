<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Chat Support_searchKey</name>
   <tag></tag>
   <elementGuidId>472aeea1-025e-4ad9-92bd-4b71e42b56f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#searchKey</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='searchKey']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d320e85d-38c7-414c-bb6f-ddea3ebc3ef5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>69485d0e-847e-4e41-becb-9311fc8adc0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>9aa43020-8220-4076-a27c-50f122e50728</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control search-field-holder searchbox ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>272fe5db-52ed-4ffa-98f6-12f5a4b80065</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search for patient...</value>
      <webElementGuid>2e49d05e-4c09-4747-9c89-d394f0f94ae0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>4c5d975b-6bba-4954-a52d-d591fc0e78fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>search.startSearch()</value>
      <webElementGuid>5970d01c-6332-4f10-9c55-f9ec9247d213</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>search.searchvalue</value>
      <webElementGuid>06ea4df3-0e82-47c9-be14-e3aeab93b27f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-init</name>
      <type>Main</type>
      <value>search.searchvalue=''</value>
      <webElementGuid>81091e95-b44e-4981-bd9c-047bffeb1def</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 750}</value>
      <webElementGuid>2f6f2eb4-2424-4adf-98f9-5805d1e9364a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>6ab9f613-174c-43c8-a984-10e554472140</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>50</value>
      <webElementGuid>78494ec3-0378-4ab0-9cd2-e752f32c7530</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchKey&quot;)</value>
      <webElementGuid>28c776a3-7200-42be-bf55-2805ca6e06cb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='searchKey']</value>
      <webElementGuid>2a6d7c21-f1e8-48ed-a6c0-44dc3ca22829</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/input[3]</value>
      <webElementGuid>bf0b70ce-0993-46bb-9c86-39398d58a54b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input[3]</value>
      <webElementGuid>c5038ad0-cc1f-4fd7-b220-2800837ff948</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'searchKey' and @id = 'searchKey' and @placeholder = 'Search for patient...' and @type = 'text']</value>
      <webElementGuid>c2293509-c5e2-468b-8752-c2a4fc60c004</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
