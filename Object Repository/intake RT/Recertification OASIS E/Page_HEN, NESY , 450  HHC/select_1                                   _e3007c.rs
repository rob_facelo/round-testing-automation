<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _e3007c</name>
   <tag></tag>
   <elementGuidId>885cc54a-a0c6-4652-a997-86ab68180597</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M0110 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M0110']/div/div[2]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>fdf18696-e055-49f7-8e64-574ddf5beecb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>e91c2dec-0ac7-4f42-a538-c76e536517e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M0110_EPISODE_TIMING</value>
      <webElementGuid>c4110531-8bb7-4500-b2b4-21d239281fc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M0110_EPISODE_TIMING</value>
      <webElementGuid>0d3a16ad-f5f6-4384-891f-fdef972533c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M0110_EPISODE_TIMING', oasis.oasisDetails.M0110_EPISODE_TIMING)</value>
      <webElementGuid>f801ed1f-2d69-45a0-a247-86b4132a7f0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>ecaa0392-e8c2-4dbb-b904-e4af0bed32ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    </value>
      <webElementGuid>4e66b7ed-7cfb-4fd0-93ce-e505cb880758</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M0110&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>84fed430-fca4-47a0-bf33-3ce0686e0ea5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M0110']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>61c18c43-69ac-4182-bd9c-f404a0de8913</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[3]/following::select[1]</value>
      <webElementGuid>a2942ef9-a826-497c-b361-e8306d14937b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other follow-up'])[1]/following::select[1]</value>
      <webElementGuid>84e1d20c-abd0-48de-936f-e8c7ca9e7de7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Early'])[1]/preceding::select[1]</value>
      <webElementGuid>f8027609-eee6-4e32-af1c-3c6518d1c012</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Later'])[1]/preceding::select[1]</value>
      <webElementGuid>d46f8372-078d-4944-a708-cba72e0742af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/div/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>aec4bb14-f0d0-4bde-b96a-0c48fb056748</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    ' or . = '
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    ')]</value>
      <webElementGuid>7d31c427-1247-4e5c-8e57-b8c9dc3125c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
