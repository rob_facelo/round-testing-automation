<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Validate Oasis_glyphicon glyphicon-ok</name>
   <tag></tag>
   <elementGuidId>d184073b-f396-474c-9667-980d88c04f0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-ok</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>13e989a9-b9a8-4e02-b0e1-e878b0b016d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-ok</value>
      <webElementGuid>148dd605-fd2e-42f6-b8b6-4854347c467d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 header_nav_container_oasis&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]/span[@class=&quot;glyphicon glyphicon-ok&quot;]</value>
      <webElementGuid>527e8f7c-d4b4-4c6f-bf23-b2e235f59b37</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[2]/span</value>
      <webElementGuid>c8243cbb-6536-4e15-b5ed-14f9c6f05f4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/span</value>
      <webElementGuid>e45a0c3b-99fb-4aea-ad4b-ea01a939a28c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
