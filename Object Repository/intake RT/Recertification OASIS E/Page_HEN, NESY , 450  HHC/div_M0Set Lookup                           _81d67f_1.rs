<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_M0Set Lookup                           _81d67f_1</name>
   <tag></tag>
   <elementGuidId>c9fa988c-b151-41d6-a6dd-2979973b331b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row.footer_nav_container_oasis</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1cd7fce9-84cc-4bcc-b1b2-6e8e0bd80347</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row footer_nav_container_oasis</value>
      <webElementGuid>728355cb-3920-4a12-ac2a-9f1c4a190375</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    </value>
      <webElementGuid>30594d30-2186-455a-8e4a-7ad68bef5108</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]</value>
      <webElementGuid>66311b86-7d4f-46f0-b698-3b1f1342017e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]</value>
      <webElementGuid>526208c8-94fa-46f8-a6c7-0c2681f9e6b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Current Condition'])[1]/following::div[3]</value>
      <webElementGuid>77c872aa-90d8-4748-b447-da1fec8391b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient Current Status'])[1]/following::div[7]</value>
      <webElementGuid>00759cc1-b36b-4e63-806a-8676ab6d16c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div[7]</value>
      <webElementGuid>cd7fcb41-d4d0-45eb-9a6e-28d8f572ffc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    ' or . = '
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    ')]</value>
      <webElementGuid>9adda294-756e-4013-9695-77556565c8e2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
