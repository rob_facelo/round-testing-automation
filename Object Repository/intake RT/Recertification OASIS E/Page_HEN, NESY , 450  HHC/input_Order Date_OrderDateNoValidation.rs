<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Order Date_OrderDateNoValidation</name>
   <tag></tag>
   <elementGuidId>2a4d08ac-3cde-49c0-8b1d-b0096fda94b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#CallerInformationOrderDate</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='CallerInformationOrderDate']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ce7e044d-a781-4991-bb68-2e61dc9f976a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>ptoCtrl.selectedCurrentOrderType == ''</value>
      <webElementGuid>1273a1b5-2677-4b6b-b2bc-c6ec7c795aff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>OrderDateNoValidation</value>
      <webElementGuid>43951337-c95d-4703-96b7-45539188374e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>2b008e8f-e6ed-4ebb-90bb-11b6fbb0500d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>showtoday</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>77b7d882-5f81-4374-a486-3cd5eaec9041</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>0e29bbda-1587-48d1-ba70-2ecade39773a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-untouched ng-valid ng-scope ng-valid-pattern ng-valid-maxlength</value>
      <webElementGuid>5e77d6d2-e913-4fe6-a2d4-32e3b7001be8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>CallerInformationOrderDate</value>
      <webElementGuid>8de9bd9c-1524-4297-8ed6-278b3d564fca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>ptoCtrl.ptoInput.CallDate</value>
      <webElementGuid>630911b5-cd49-44bc-a55d-7717b43d7074</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>ptoCtrl.ptoPermission.IsSignCarestaff</value>
      <webElementGuid>3e38f84c-6b22-4316-88b1-b3c4a4b22e43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>b5ddd8de-42ff-40bf-9b66-83232a7ff4d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>model-view-value</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>2bacd862-c551-4f88-a07c-b914d92b48cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-pattern</name>
      <type>Main</type>
      <value>/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/</value>
      <webElementGuid>685a24cc-5577-435f-b0e3-a46db41f1c5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,false,'1')</value>
      <webElementGuid>b31e0d44-75ef-4a7a-ab55-1c515a4810a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>e891e762-8256-4ef4-aee1-e37dde5c703f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;CallerInformationOrderDate&quot;)</value>
      <webElementGuid>1efdf2d2-7254-49e6-b9eb-ee7d795b9abb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='CallerInformationOrderDate']</value>
      <webElementGuid>fb339e57-6f31-4f05-8299-2e8b04015556</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[4]/div/div/div[2]/fieldset/div[3]/div/div[2]/div/input</value>
      <webElementGuid>5fc2c244-e6d5-40c1-b8f6-0ad5c684674f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/input</value>
      <webElementGuid>b6c5ce3a-9693-46aa-acd6-72802dc61321</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'OrderDateNoValidation' and @type = 'text' and @id = 'CallerInformationOrderDate']</value>
      <webElementGuid>1f2984bd-4626-4807-813c-c08f6e4b30f3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
