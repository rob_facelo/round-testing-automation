<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Lock Oasis</name>
   <tag></tag>
   <elementGuidId>65584e69-d03e-45f3-a583-2fb2c6a66247</elementGuidId>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'oasis.lockOasis()' and (text() = '
                
                Lock Oasis
            ' or . = '
                
                Lock Oasis
            ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[55]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9f57e472-c010-4d9b-bc92-f9df472f0929</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2db78f15-000e-4ba1-9c37-a843dee7bffc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btn-sm toolbar-btn-default font12 ng-scope</value>
      <webElementGuid>47ecb6f0-5eeb-46b1-83e7-c24ea653cf6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.lockOasis()</value>
      <webElementGuid>f72174f6-6d5a-46c8-b617-37a00c07a3d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>(!oasis.isLocked &amp;&amp; ((oasis.isInUse &amp;&amp; oasis.oasisDetails.inuse_by_user_id == oasis.userCredentials.UserId) || !oasis.isInUse)) &amp;&amp; !oasis.isViewOtherOasisVersion</value>
      <webElementGuid>34ea59d2-3b31-4132-8cb5-aa3c3dc193a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>!oasis.permissions.LockUnlock || oasis.isOnSaveState || oasis.isValidating</value>
      <webElementGuid>5e5db0e3-c537-413c-8c46-10dfc1221295</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                Lock Oasis
            </value>
      <webElementGuid>e0c38a42-8886-4008-abf2-3c1293db00ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 header_nav_container_oasis&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]</value>
      <webElementGuid>4c43a273-00aa-4fda-a55b-c6e783d8050f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[55]</value>
      <webElementGuid>f65a6969-d017-4eb2-8448-c6929d92ab9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[3]</value>
      <webElementGuid>86fda2d5-de0e-49a0-8867-2309a2e9288d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PPS Calculator'])[1]/preceding::button[1]</value>
      <webElementGuid>62a47950-43f2-44ec-ad5a-5228aadd6472</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDGM Calculator'])[1]/preceding::button[2]</value>
      <webElementGuid>4c83069d-c5bf-4618-b655-8297a79f8864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lock Oasis']/parent::*</value>
      <webElementGuid>531a8d93-f4ac-4449-aa66-ebbd8f88fd36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]</value>
      <webElementGuid>f0c3e36c-65aa-4705-b1a8-1e00aa912ce9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                
                Lock Oasis
            ' or . = '
                
                Lock Oasis
            ')]</value>
      <webElementGuid>da0d48c8-c41d-49af-8d32-78b9a6a2c257</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
