<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_walker, sarah SN -</name>
   <tag></tag>
   <elementGuidId>fde1abfe-8744-4c0f-83ac-89a13946267e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div[2]/div/div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8a127518-41b7-4cf6-9f26-d174597c55c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>7a5cb8c7-1a90-486d-aa96-3e290cf83100</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        walker, sarah SN - 
                    </value>
      <webElementGuid>dc04b36a-d0bf-4f60-a791-4ab7821e7a90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-valid-mask ng-valid-maxlength ng-valid-pattern ng-valid-minlength ng-dirty ng-valid-parse ng-valid ng-valid-validation&quot;]/div[@class=&quot;row margin-top-10px text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;form-group form-group-marginZero ng-scope&quot;]/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>ed47255d-f528-43b7-8a8c-dd04a9f5ebb3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div[2]/div/div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>caf4a059-95ff-4f25-90ee-5195474fc7f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Staff(s) for this order*:'])[1]/following::div[3]</value>
      <webElementGuid>df58f391-993c-4d40-bc0e-3b68e5ef568b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[3]/preceding::div[3]</value>
      <webElementGuid>cc2c3c65-1783-4e80-a6cd-70770d45ec1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[4]/preceding::div[5]</value>
      <webElementGuid>c7438d5c-b9fe-4908-a7e3-913b7b8de5cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='walker, sarah SN -']/parent::*</value>
      <webElementGuid>f7dd0208-4013-48f2-acbb-21f6efd5701e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>31b0b497-3917-4644-b1c3-c3f336ba0509</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        walker, sarah SN - 
                    ' or . = '
                        walker, sarah SN - 
                    ')]</value>
      <webElementGuid>c76bb811-cb18-453b-8b4e-6cca9fb993fe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
