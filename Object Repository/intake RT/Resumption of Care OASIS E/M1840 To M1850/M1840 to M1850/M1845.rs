<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1845</name>
   <tag></tag>
   <elementGuidId>2d800f4f-abb9-4d5a-8a2b-29d7172089bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1845 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1845_CUR_TOILTG_HYGN']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1845']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>eda2db2e-0f63-4162-8f8d-66efeac5dd77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>dae76f40-756d-446a-be7a-bd069d040a34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1845_CUR_TOILTG_HYGN</value>
      <webElementGuid>f78b56e1-68c6-4d23-8311-7f50468e1c42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1845_CUR_TOILTG_HYGN</value>
      <webElementGuid>c62a79b8-3867-4c37-a854-6b8b5335d4e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1845_CUR_TOILTG_HYGN', oasis.oasisDetails.M1845_CUR_TOILTG_HYGN)</value>
      <webElementGuid>f87cf606-7191-492a-8631-0ef7a6b7df75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>9385c6ae-fc86-4463-aff1-2bc718e04349</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                            </value>
      <webElementGuid>8b135d8d-8fa5-41e7-b73d-7152ff5cc4e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1845&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>3c2b2e4c-564c-413a-a104-76533e1cf31d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1845']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>3023980f-99ec-4668-989c-378ad05bc77f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[29]/following::select[1]</value>
      <webElementGuid>3570c824-dc49-4fa7-a895-51f66663ce00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Is totally dependent in toileting.'])[1]/following::select[1]</value>
      <webElementGuid>6e224b42-49d3-41b1-80e3-252e2d724131</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to manage toileting hygiene and clothing management without assistance.'])[1]/preceding::select[1]</value>
      <webElementGuid>416004d5-1448-4ec1-8f53-7f5a59c27789</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Someone must help the patient to maintain toileting hygiene and/or adjust clothing.'])[1]/preceding::select[1]</value>
      <webElementGuid>327250c6-49dd-42ac-a878-80f7654e9b39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[11]/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>51d07461-9806-498b-b944-6ed5a94c127f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                            ')]</value>
      <webElementGuid>7693a6c8-a215-4b13-8a12-338d3d1bfb9a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
