<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1850</name>
   <tag></tag>
   <elementGuidId>229be3bc-e33f-46b9-acc2-297d9ba3999d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1850 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1850_CUR_TRNSFRNG']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1850']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>28d465b0-4f83-4abf-93da-32c9eebef224</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>53268066-1fe7-4c44-bc3f-17d784d9264a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1850_CUR_TRNSFRNG</value>
      <webElementGuid>bd1e3355-32de-40a4-ac28-d68731207e0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1850_CUR_TRNSFRNG</value>
      <webElementGuid>f2f47596-a90c-4eb3-a120-b26343470240</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1850_CUR_TRNSFRNG', oasis.oasisDetails.M1850_CUR_TRNSFRNG)</value>
      <webElementGuid>730df331-e4b3-494d-8b43-8c3a35ef9d5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>2605c380-c032-4cdf-a957-686f139d30e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            </value>
      <webElementGuid>b7fd63e7-9c79-4985-ac0e-11e517d54a94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1850&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>7fe85826-e129-4f8e-beb1-25338086bd9c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1850']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>d127387f-1d4a-41b6-8638-506ce61c0582</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[30]/following::select[1]</value>
      <webElementGuid>7271f8bf-fb53-4bff-8a55-66435cb122b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient depends entirely upon another person to maintain toileting hygiene.'])[1]/following::select[1]</value>
      <webElementGuid>bfde6319-30f6-402f-949d-53cb504f6ad3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to independently transfer.'])[1]/preceding::select[1]</value>
      <webElementGuid>b492764c-8ddc-426b-be53-5ab6a6c74b96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to transfer with minimal human assistance or with use of an assistive device.'])[1]/preceding::select[1]</value>
      <webElementGuid>848e8c3f-3bbe-49a4-bd20-e9f79019fabf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[11]/div/div[2]/div/div/div[2]/div[3]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>16b8a921-74b7-4cd9-a004-4e48c589f074</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            ')]</value>
      <webElementGuid>9183329f-ec29-4a9c-acfe-81d853b0c1c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
