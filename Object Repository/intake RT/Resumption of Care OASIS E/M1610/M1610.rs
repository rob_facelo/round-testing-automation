<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1610</name>
   <tag></tag>
   <elementGuidId>ecdb45fa-71a7-4318-b6d4-2251f7bf0bcb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1610 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1610_UR_INCONT']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1610']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>586f14a9-52d3-4086-b392-39cea9dac4c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>a8da603f-e4ca-4016-a96d-9a6d1c15201d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1610_UR_INCONT</value>
      <webElementGuid>95c425fb-f211-43b9-b2ef-11c786e3939f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1610_UR_INCONT</value>
      <webElementGuid>8245830f-316e-4fd5-8d5c-1f5589f82201</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.clearMItemFieldsValues('M1610_UR_INCONT','select',['00,M1615','02,M1615'])</value>
      <webElementGuid>6c9bbd4e-dffa-419d-be59-a78cefcaedb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>9df10f98-571f-4e02-8a4d-05d5f994e994</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                
                                            </value>
      <webElementGuid>ffe32989-228d-4cab-8101-727c9a273d55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1610&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>51dd78ef-83ab-4a4a-830f-2dfa657de1a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1610']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>15a139e6-81b9-44e7-aed3-06964666992c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[16]/following::select[1]</value>
      <webElementGuid>8d786dff-37af-47d2-9df3-7a86fb3eeed2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M1610) Urinary Incontinence or Urinary Catheter Presence:'])[1]/following::select[1]</value>
      <webElementGuid>cb296ed3-2ed5-4ab6-94b7-62ac643527d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No incontinence or catheter (includes anuria or ostomy for urinary drainage)'])[1]/preceding::select[1]</value>
      <webElementGuid>a72b6249-31a7-4d95-b229-36c7add9ebeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient is incontinent'])[1]/preceding::select[1]</value>
      <webElementGuid>388e920f-6d3e-4fe2-b22e-5e8bd3743911</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[8]/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>dc090bfe-3ce6-4b55-a195-00084bdf07bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                
                                            ')]</value>
      <webElementGuid>65fe16d6-55bb-4ccc-9b83-f1482db66c8d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
