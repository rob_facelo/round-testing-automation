<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>supervision safety</name>
   <tag></tag>
   <elementGuidId>6a4af931-1530-4326-b517-4725b30f0127</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space.ng-scope > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2102']/div/div[2]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>34ec6870-4f2f-4cc6-8ef9-86b66abcd163</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>8b2c86c8-3f7d-404c-954d-536d8fcd6744</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2100_CARE_TYPE_SRC_SPRVSN</value>
      <webElementGuid>a09ad937-87cf-45a5-a414-bb3be00e6363</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN</value>
      <webElementGuid>3812c1f7-93de-4436-ae8f-b9486e79e065</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2100_CARE_TYPE_SRC_SPRVSN', oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN)</value>
      <webElementGuid>c46c10e9-12e6-4e64-b8dc-8360320aa6ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>f1644dcc-ff8e-4f34-aad3-2daf5fd41611</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                    </value>
      <webElementGuid>f1425040-81da-45a0-ae7b-f099d25024d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2102&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>def756cd-cb49-42ff-80b6-1d44d3bc1a55</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2102']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>62912656-e11b-4c6e-ad97-01ced5c39ccb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[20]/following::select[1]</value>
      <webElementGuid>f0d10fe6-e233-484e-9a2c-ad33b82f1d55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plan/Comments:'])[1]/following::select[1]</value>
      <webElementGuid>3836c6ee-a158-4047-927c-d521d75b5584</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Supervision and safety'])[1]/preceding::select[1]</value>
      <webElementGuid>43cbf533-4979-4b1b-a9d7-3bb83dadc6e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>640801eb-5c8b-463c-9fe1-252cf6c7c8ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                    ' or . = '
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                    ')]</value>
      <webElementGuid>c45cd307-5666-4a0f-9103-b198fe436634</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
