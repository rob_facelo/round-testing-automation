<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save_glyphicon  glyphicon-floppy-disk</name>
   <tag></tag>
   <elementGuidId>c84aa43d-1de6-45df-9c23-5e11dad307f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-floppy-disk</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div/div/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c53c21ec-305f-4453-9a49-e692a1e4f957</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon  glyphicon-floppy-disk</value>
      <webElementGuid>f2679eb0-4fe2-49c5-bed9-8c99760d5110</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-pristine ng-valid-mask ng-invalid ng-invalid-validation ng-valid-maxlength ng-valid-pattern ng-valid-minlength&quot;]/div[@class=&quot;form-actions&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-9 col-lg-9&quot;]/div[@class=&quot;btn-group&quot;]/button[@class=&quot;btn btn-default&quot;]/span[@class=&quot;glyphicon  glyphicon-floppy-disk&quot;]</value>
      <webElementGuid>190be76e-0cd7-4771-9fa5-fee109b2cc6f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div/div/div/div/button/span</value>
      <webElementGuid>b010dbfb-5110-41f8-b537-aad82ef57448</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ng-form/div/div/div/div/button/span</value>
      <webElementGuid>3e392a54-88d3-46e1-9f31-8a169f2bfa9b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
