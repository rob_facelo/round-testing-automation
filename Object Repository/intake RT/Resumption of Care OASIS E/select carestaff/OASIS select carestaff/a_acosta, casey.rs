<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_acosta, casey</name>
   <tag></tag>
   <elementGuidId>fd7538e7-1b43-4631-a5b0-2f4ba588e22c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#searchCaregiverOasis > ul.dropdown-menu > li.ng-scope > a.ng-binding</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = 'oasis.setCompletingCaregiver(caregiver.caregiver_name, caregiver.caregiver_id)' and (text() = 'acosta, casey ' or . = 'acosta, casey ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchCaregiverOasis']/ul/li/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>dc3106f8-6a32-4eab-86f3-69db5e75e4a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.setCompletingCaregiver(caregiver.caregiver_name, caregiver.caregiver_id)</value>
      <webElementGuid>6a0b4a8c-fd6b-44f1-af18-ebfbaabc2760</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-binding</value>
      <webElementGuid>0642abe1-1185-42be-86b0-fa58a29020c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>acosta, casey </value>
      <webElementGuid>5d073e17-2947-42d9-a854-b5591e0361e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchCaregiverOasis&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-binding&quot;]</value>
      <webElementGuid>55b03994-8ea9-4e1d-8a71-d384f6bd9dc9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchCaregiverOasis']/ul/li/a</value>
      <webElementGuid>c05cee47-4843-4cc3-8001-363f6fee5c23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'acosta, casey')]</value>
      <webElementGuid>80de7bd7-4a87-4454-be7d-5404049ab089</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Care Staff Name:'])[1]/following::a[1]</value>
      <webElementGuid>21ad2a50-08b0-4086-bd01-c81564e70239</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OT'])[1]/following::a[1]</value>
      <webElementGuid>ad7c82f1-54e5-4578-9418-39dfabe1a10e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0090. Date Assessment Completed'])[1]/preceding::a[1]</value>
      <webElementGuid>7fdad075-dafc-4ebd-84d8-02af0d73ef5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0100. This Assessment is Currently Being Completed for the Following Reason'])[1]/preceding::a[1]</value>
      <webElementGuid>87a14ab3-de34-4e15-b909-54cbadcac00f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='acosta, casey']/parent::*</value>
      <webElementGuid>e7214dce-fe78-46fc-8e11-3067f90a368d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div[2]/ul/li/a</value>
      <webElementGuid>c4381c3c-3cb8-4e40-802e-87ba815daabe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'acosta, casey ' or . = 'acosta, casey ')]</value>
      <webElementGuid>660898da-f770-4f34-8791-cfdb80077fc4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
