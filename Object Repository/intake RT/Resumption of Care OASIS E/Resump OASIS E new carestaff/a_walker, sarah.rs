<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_walker, sarah</name>
   <tag></tag>
   <elementGuidId>a0b85c5c-dead-4479-9f0d-37f54d2de2a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#searchCaregiverOasis > ul.dropdown-menu > li.ng-scope > a.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchCaregiverOasis']/ul/li/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e0a3af2e-1017-49c9-88c2-b32f42bd3625</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.setCompletingCaregiver(caregiver.caregiver_name, caregiver.caregiver_id)</value>
      <webElementGuid>2156fb12-3cdb-4c72-becb-803bdd18a0f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-binding</value>
      <webElementGuid>557c1628-af44-4447-9729-e419896d97df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>walker, sarah </value>
      <webElementGuid>1bdb884f-73ab-440c-8a7e-6ceed8cc2fae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchCaregiverOasis&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-binding&quot;]</value>
      <webElementGuid>bf7cbdcc-487c-44d8-9a1b-1da3a9e9ff4d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchCaregiverOasis']/ul/li/a</value>
      <webElementGuid>a431892b-04de-4565-b5ba-e836588b4b67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'walker, sarah')]</value>
      <webElementGuid>de8fe891-2244-46d1-8031-6f294cc640cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Care Staff Name:'])[1]/following::a[1]</value>
      <webElementGuid>6168e0c4-8fd4-4bb0-a7f0-693a14addbbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OT'])[1]/following::a[1]</value>
      <webElementGuid>c0595105-54d1-4f90-b2c2-1d9522913e07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0090. Date Assessment Completed'])[1]/preceding::a[1]</value>
      <webElementGuid>800b3800-62f9-4728-bc86-c616d4c0fe06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0100. This Assessment is Currently Being Completed for the Following Reason'])[1]/preceding::a[1]</value>
      <webElementGuid>e1677094-6888-4118-9ce5-18cc6a08d6f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='walker, sarah']/parent::*</value>
      <webElementGuid>abb3f15b-5224-4ba3-b65b-92bec89f9fc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div[2]/ul/li/a</value>
      <webElementGuid>85093cf6-0902-4376-869d-27b5e7bd9592</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'walker, sarah ' or . = 'walker, sarah ')]</value>
      <webElementGuid>3707c4e0-772a-4989-9579-064d0f1e1d10</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
