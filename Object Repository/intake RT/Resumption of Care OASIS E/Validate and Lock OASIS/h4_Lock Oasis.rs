<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Lock Oasis</name>
   <tag></tag>
   <elementGuidId>4cb84a94-4a96-43e1-9422-a042bb3ab91e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal.fade.ng-scope.in > div.modal-dialog.modal-sm > div.modal-content > div.modal-header.hhc-blue > h4.modal-title.ng-binding</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Lock Oasis' or . = ' Lock Oasis')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/following::h4[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>ceb62bf1-4718-4798-bf7e-8aa40460bb21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-title ng-binding</value>
      <webElementGuid>98683119-fa1d-4828-a6fd-7dfaeaf7e2c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Lock Oasis</value>
      <webElementGuid>d7ee7537-b36f-41b8-a7c8-364d65125982</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header hhc-blue&quot;]/h4[@class=&quot;modal-title ng-binding&quot;]</value>
      <webElementGuid>50e54bee-98a6-407b-9091-bb91418bd0c9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/following::h4[1]</value>
      <webElementGuid>f6a99e5f-76ca-409b-b80e-5b580334f257</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock OASIS'])[1]/following::h4[1]</value>
      <webElementGuid>deff455d-4db0-4a07-b809-5083479926c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[2]/preceding::h4[1]</value>
      <webElementGuid>15a7267e-f22b-428d-a148-945b98ae7272</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock OASIS'])[2]/preceding::h4[1]</value>
      <webElementGuid>d56ebf59-a0b2-4e81-b5e7-87f433192fa3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/h4</value>
      <webElementGuid>1c8912ad-0d90-42ca-bd19-a3f5d4330311</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = ' Lock Oasis' or . = ' Lock Oasis')]</value>
      <webElementGuid>54406bba-adf0-4e19-8efb-0b8e2b4d0e81</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
