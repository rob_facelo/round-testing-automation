<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0100C</name>
   <tag></tag>
   <elementGuidId>806e6045-0307-4516-91af-ba2f7bcea0fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#GG0100C > div.row > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0100C']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0100C']/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>af53a5e8-516e-4bdf-b0f1-8a20a42bcf72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>c168088b-907b-4fc7-b49b-cb06dec06cc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0100C</value>
      <webElementGuid>aaf3c0ed-de89-4ed2-be27-61beab750e86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>06cbf7ff-2360-4a14-bde5-0165a2fc1c64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0100C</value>
      <webElementGuid>08675dc1-1c2e-4de7-8beb-e9cee6d8a6d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0100C', oasis.oasisDetails.GG0100C)</value>
      <webElementGuid>4aa415f7-070d-46a2-a9d0-a6bf007103c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>495630f6-f1ac-4f94-91a5-af36b4cf3ace</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            </value>
      <webElementGuid>e30f3026-d2c4-4e99-af0f-798c1afb69fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0100C&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>179a737e-1408-41b7-8a7b-e3637ca714b8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0100C']/div/div/div/select</value>
      <webElementGuid>7d1465c7-6ed7-4ca2-8d4f-ae473e8af7fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Indoor Mobility (Ambulation):'])[1]/following::select[1]</value>
      <webElementGuid>e9253421-078f-4ce7-a4b5-bd8fbeb654a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. Self Care:'])[1]/following::select[2]</value>
      <webElementGuid>d950177a-14e5-4402-b4c6-fafa05ad8d9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C. Stairs:'])[1]/preceding::select[1]</value>
      <webElementGuid>74e70475-a89a-4e6f-a117-f0e123518d36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='D. Functional Cognition:'])[1]/preceding::select[2]</value>
      <webElementGuid>74c3cb24-38f1-47ee-90ab-1457a2258e52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/select</value>
      <webElementGuid>86ab9661-cec2-4a22-8648-b59b9b55a74c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ' or . = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ')]</value>
      <webElementGuid>25cb17aa-f50b-44c6-8734-86590dd1cdd3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
