<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Restrictions_btnAgree</name>
   <tag></tag>
   <elementGuidId>e6eefa5a-b8f0-43b2-bc31-cc3447967c1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#btnAgree</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='btnAgree']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>61744c6b-a96c-4e53-a3c6-75fe5d10c833</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>3ca7cdbd-bafd-4b88-955c-b5aef64bd4d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>btnAgree</value>
      <webElementGuid>3a5d6d71-4f9c-4d8c-a440-a0d236990b34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>I/We Agree</value>
      <webElementGuid>7c9099e2-97ab-468d-bd4f-8fcbd19cf7ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>clearUserLogsStorage();</value>
      <webElementGuid>e07c7a2f-b1d9-49d0-a5ba-6e9e001ce47f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnAgree</value>
      <webElementGuid>e1714e8a-dda2-4fa7-bca3-7a4d6629d5c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>agree-btn form-control</value>
      <webElementGuid>924fe477-8078-4154-b4b8-0a6f6592fbd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnAgree&quot;)</value>
      <webElementGuid>b1daa76c-92aa-4d90-80e9-d4b4df98c5d6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='btnAgree']</value>
      <webElementGuid>9ddc862a-bc61-497a-8c6c-2785a46c9907</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]/div[2]/div/div[3]/input</value>
      <webElementGuid>646ebd53-b396-4358-b020-60555092acd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/input</value>
      <webElementGuid>8fe2cd43-addb-405b-abd0-87d44dbf5934</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'btnAgree' and @id = 'btnAgree']</value>
      <webElementGuid>4dbb0a61-8317-4a74-b109-fa7b063b040b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
