<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _e338a2_1</name>
   <tag></tag>
   <elementGuidId>25deeb60-93af-4624-89db-addd11c580c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M2030 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2030']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f6b70baf-419e-4820-86ca-c2e09f2a8361</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>54dbb688-5a68-4273-86de-a5521ed85474</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2030_CRNT_MGMT_INJCTN_MDCTN</value>
      <webElementGuid>1069eb6a-c9df-4f2a-9718-65fe6dc8657c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN</value>
      <webElementGuid>1c7779a0-fbc6-408f-90ac-64e8b1b8f008</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2030_CRNT_MGMT_INJCTN_MDCTN', oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN)</value>
      <webElementGuid>d24dbc69-8f8c-43b6-a066-b6b3a8d0507a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>(oasis.oasisDetails.M2000_DRUG_RGMN_RVW == '9' &amp;&amp; (oasis.oasisDetails.M0100_ASSMT_REASON == '01' || oasis.oasisDetails.M0100_ASSMT_REASON =='03')) || oasis.isLocked</value>
      <webElementGuid>eec210db-b972-4c97-bd38-1b00570c49e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                </value>
      <webElementGuid>70c0a43c-8c6b-421d-8f95-da8e5fc51e5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2030&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>343f782b-67c6-4d5e-a886-6530090b3111</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2030']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>79244e8a-e93c-40f7-bd25-fe0e7956be30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[52]/following::select[1]</value>
      <webElementGuid>b5ec58c2-649f-46af-90ad-2231c985afd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Excludes'])[2]/following::select[1]</value>
      <webElementGuid>7f8a5b47-8a09-4b38-a35e-481cff0e46f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to independently take the correct medication(s) and proper dosage(s) at the correct times.'])[1]/preceding::select[1]</value>
      <webElementGuid>642dca97-4583-45e9-b8e9-846b00c4905b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to take injectable medication(s) at the correct times if:'])[1]/preceding::select[1]</value>
      <webElementGuid>3cfd9089-39e1-4bf2-8b6a-39d748ef264b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[14]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>2dd7fecd-d5ed-4bf3-8c1c-5cbd1ab38009</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                ')]</value>
      <webElementGuid>533722b2-7a3a-43fc-8a3b-1573019e8109</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
