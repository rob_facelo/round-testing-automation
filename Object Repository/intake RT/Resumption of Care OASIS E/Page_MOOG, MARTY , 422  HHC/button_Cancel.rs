<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Cancel</name>
   <tag></tag>
   <elementGuidId>c7511615-2c6f-43ca-b9ee-c66a1dff7f7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.bootbox.modal.fade.bootbox-prompt.in > div.modal-dialog > div.modal-content > div.modal-footer > button.btn.btn-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[196]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>97790d14-b3d3-4537-a26e-d021dccdffb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bb-handler</name>
      <type>Main</type>
      <value>cancel</value>
      <webElementGuid>6857f7d3-4c12-4939-868e-b67a7266dc2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9f438c17-c323-433d-88fd-e4829798b82d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
      <webElementGuid>f689f293-5234-48d4-b4c5-bb9ec78bd647</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cancel</value>
      <webElementGuid>489f4016-25fd-499b-929c-f6fae1ebb105</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-109 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;bootbox modal fade bootbox-prompt in&quot;]/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer&quot;]/button[@class=&quot;btn btn-default&quot;]</value>
      <webElementGuid>cddc120e-1c97-48d9-a441-18959fe4397d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[196]</value>
      <webElementGuid>3630ad6f-c26f-4f72-985e-36607ffeb922</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Empty medication status.'])[1]/following::button[1]</value>
      <webElementGuid>29959555-cd44-4371-a794-8f56b6088cc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CHANGE medication status.'])[1]/following::button[1]</value>
      <webElementGuid>0a459d46-1305-426f-87e4-47520db660b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[4]/preceding::button[1]</value>
      <webElementGuid>844e75fa-4601-4ea5-8820-5c0664d277fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div[3]/button</value>
      <webElementGuid>cf19e62f-2550-44a3-a74a-b41217c21654</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Cancel' or . = 'Cancel')]</value>
      <webElementGuid>87d787ff-df62-4efb-93ce-18488bd4bcf6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
