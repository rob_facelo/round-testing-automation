<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_M1005. Inpatient Discharge Date (most r_632126</name>
   <tag></tag>
   <elementGuidId>72cd1c80-a271-408c-81eb-5c8a652099b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.input-group.date</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1005']/div/div[2]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f1816db7-32f3-4f64-b25d-4b6be45c8b7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-group date</value>
      <webElementGuid>89a8af8c-a734-4fa6-93e2-282985bcb4b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1005&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;input-group date&quot;]</value>
      <webElementGuid>7338c31b-3578-48f9-a8ce-0d85853e3ded</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1005']/div/div[2]/div/div/div</value>
      <webElementGuid>11fd4049-8f1d-4d8e-b835-29f70884b170</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M1005. Inpatient Discharge Date (most recent):'])[1]/following::div[4]</value>
      <webElementGuid>b3ed69a2-4c7b-4d95-84f9-70dcc9bf116f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B1300'])[1]/following::div[8]</value>
      <webElementGuid>be90c91a-1837-4056-bf71-d6f5f3ffb9fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SECTION B - HEARING, SPEECH, AND VISION'])[1]/preceding::div[3]</value>
      <webElementGuid>0313b76e-f027-485d-a5f3-e7c48ec09719</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div/div[2]/div/div/div</value>
      <webElementGuid>fe018658-3580-4f42-82a9-a60f22441b1d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
