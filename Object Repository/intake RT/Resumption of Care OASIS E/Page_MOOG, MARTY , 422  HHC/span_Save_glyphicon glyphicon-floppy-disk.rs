<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save_glyphicon glyphicon-floppy-disk</name>
   <tag></tag>
   <elementGuidId>c11c9157-17e7-4eea-9996-1882643fa01f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > button.btn.btn-default.btn-sm.toolbar-btn-default.font12.ng-scope > span.glyphicon.glyphicon-floppy-disk</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/fieldset[1]/div[@class=&quot;row&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]/span[@class=&quot;glyphicon glyphicon-floppy-disk&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>98a242d9-fb43-4b68-8a41-ffea7a865d30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-floppy-disk</value>
      <webElementGuid>d07fd729-a549-4c6c-af4b-cd99397ae3c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/fieldset[1]/div[@class=&quot;row&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]/span[@class=&quot;glyphicon glyphicon-floppy-disk&quot;]</value>
      <webElementGuid>f7718b9d-b6cd-4abb-bc01-0404b4bd9677</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      <webElementGuid>e25f31d6-9510-43fb-818a-4d362dabfbaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      <webElementGuid>77bebd13-15bc-4edc-b7ba-757ea54aa489</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
