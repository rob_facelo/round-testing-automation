<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _6bfb23</name>
   <tag></tag>
   <elementGuidId>e0ebeae9-25d7-498e-871c-daa4ef97d4e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1745_BEH_PROB_FREQ' and (text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1745']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>0333ec0b-d3d5-4171-bcf5-a82f3f966110</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>d5974b0c-81c3-459c-b8c6-5c92b5f292ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1745_BEH_PROB_FREQ</value>
      <webElementGuid>4133302a-e6f4-41d1-b547-5ffc3c88da14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1745_BEH_PROB_FREQ</value>
      <webElementGuid>c9f50b05-770e-4021-887e-54bb2a70fca3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1745_BEH_PROB_FREQ', oasis.oasisDetails.M1745_BEH_PROB_FREQ)</value>
      <webElementGuid>b1cb0561-6bd8-457f-a89e-59ba4aa090cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>5062ad98-e208-4094-9a11-25b25d51b836</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                </value>
      <webElementGuid>40008ca1-573f-4500-af9c-15039fbc259b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1745&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>3cfabe6a-9f4d-425f-b731-cb0a8004ec07</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1745']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>0b2a4bd6-c108-4fd2-9df0-e31803d0e400</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[19]/following::select[1]</value>
      <webElementGuid>cd07243c-7234-4b00-beea-ad35884b4459</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='None of the above behaviors demonstrated'])[1]/following::select[1]</value>
      <webElementGuid>0f4a9ada-4909-4c52-ba05-9dea97fd74ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Never'])[4]/preceding::select[1]</value>
      <webElementGuid>1445e1dd-6d79-4c06-82ae-1ba271f5927e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Less than once a month'])[1]/preceding::select[1]</value>
      <webElementGuid>9e28b849-816e-406d-bf64-bc353bc635c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>1887f018-b19b-4e28-8daf-64b8d512a952</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    5
                                ')]</value>
      <webElementGuid>7a54b4f1-e630-42dd-8abe-3498f085c4d7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
