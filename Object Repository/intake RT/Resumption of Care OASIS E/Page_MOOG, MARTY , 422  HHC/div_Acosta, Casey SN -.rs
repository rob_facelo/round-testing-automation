<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Acosta, Casey SN -</name>
   <tag></tag>
   <elementGuidId>0695ad7d-f7ca-479e-9fdd-dcb042b40717</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-valid-mask ng-valid-maxlength ng-valid-pattern ng-valid-minlength ng-dirty ng-valid-parse ng-valid ng-valid-validation&quot;]/div[@class=&quot;row margin-top-10px text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;form-group form-group-marginZero ng-scope&quot;]/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;][count(. | //*[(text() = '
                        Acosta, Casey SN - 
                    ' or . = '
                        Acosta, Casey SN - 
                    ')]) = count(//*[(text() = '
                        Acosta, Casey SN - 
                    ' or . = '
                        Acosta, Casey SN - 
                    ')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div[2]/div/div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0dd671b9-9d0d-4d1b-952a-5a28d9cf7a1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>d629d04e-9f8a-40b1-bd5f-b23b92cd13a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Acosta, Casey SN - 
                    </value>
      <webElementGuid>ec511be7-1f68-44c9-baf3-a373905b344b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-valid-mask ng-valid-maxlength ng-valid-pattern ng-valid-minlength ng-dirty ng-valid-parse ng-valid ng-valid-validation&quot;]/div[@class=&quot;row margin-top-10px text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-lg-6&quot;]/div[@class=&quot;form-group form-group-marginZero ng-scope&quot;]/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>26b3ddd5-2f92-4774-935a-7047773b648b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div[2]/div/div[2]/div/div/div/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>2f073c66-eacd-4e39-a632-b05295af222b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Staff(s) for this order*:'])[1]/following::div[3]</value>
      <webElementGuid>0eee5af9-bd04-4d8f-b469-16007ad91cb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[3]/preceding::div[3]</value>
      <webElementGuid>339f01c4-0378-42a8-882d-4c458a824f82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[4]/preceding::div[5]</value>
      <webElementGuid>76fc6699-e2b4-4739-89bd-5b18ba4b13c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Acosta, Casey SN -']/parent::*</value>
      <webElementGuid>9fcbdb3d-6d02-4e27-bbc0-294958fab023</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>d7c6efa4-b8c0-4961-9eb4-5584c3e3fcb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Acosta, Casey SN - 
                    ' or . = '
                        Acosta, Casey SN - 
                    ')]</value>
      <webElementGuid>83404d01-da2c-463d-9d14-e4dfd25592d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
