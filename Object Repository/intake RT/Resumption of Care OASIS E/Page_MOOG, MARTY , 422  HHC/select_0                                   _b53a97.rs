<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _b53a97</name>
   <tag></tag>
   <elementGuidId>59a86e57-26dc-4071-bd73-0d59f24ef415</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-4.col-md-1.col-lg-1.row > div.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.C1310B' and (text() = '
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        ' or . = '
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='C1310']/div/div[2]/div[2]/div/div[2]/div/div[2]/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>25aef21f-35af-4400-9dee-e78eca73103e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>68e37bb8-1311-48dd-b8ff-3321ffc2b978</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>C1310B</value>
      <webElementGuid>90a7108a-2221-4a9f-ade2-64ca7cc51d2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>0ef51c55-94d0-4a45-99db-66b09db696a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.C1310B</value>
      <webElementGuid>ada02c67-551e-4e2f-94bb-ab1d87098ebc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('C1310B', oasis.oasisDetails.C1310B)</value>
      <webElementGuid>f1cd6b6b-2fda-41a0-9c80-6045a1dfe9f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>992dd688-a5e1-4707-93b2-e9fd6ad6e044</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        </value>
      <webElementGuid>e1b1c26f-e6e9-4800-98d3-ad28454cdce2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;C1310&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-8 col-md-9 col-lg-9&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-1 col-lg-1 row&quot;]/div[@class=&quot;row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>db7f207b-451d-454a-8abb-905c57a00477</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='C1310']/div/div[2]/div[2]/div/div[2]/div/div[2]/div/div/div/select</value>
      <webElementGuid>12f63d4d-bef7-4da0-aa04-9c70a627b86b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[14]/following::select[1]</value>
      <webElementGuid>257e6417-f9d2-4caa-8263-7564424e0714</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inattention'])[1]/preceding::select[1]</value>
      <webElementGuid>e3803d0e-7c38-4df6-98b7-18f9e4ec62db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Disorganized thinking'])[1]/preceding::select[2]</value>
      <webElementGuid>8e58c744-fbc3-416e-a47a-dcd29c1d900f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/div/select</value>
      <webElementGuid>e0972dec-754a-4ed2-8260-8e336918d8e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        ' or . = '
                                                            
                                                            0
                                                            1
                                                            2
                                                            -
                                                        ')]</value>
      <webElementGuid>659e53a3-c2a2-47e6-9f93-f5fb6bb40a0e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
