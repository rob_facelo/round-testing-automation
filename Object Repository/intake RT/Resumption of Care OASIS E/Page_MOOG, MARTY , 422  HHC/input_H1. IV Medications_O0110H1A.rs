<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_H1. IV Medications_O0110H1A</name>
   <tag></tag>
   <elementGuidId>db865c1b-35b2-4d30-8dee-ff6b68ddb04b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#O0110H1A</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'checkbox' and @id = 'O0110H1A' and @ng-model = 'oasis.oasisDetails.O0110H1A']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='O0110H1A']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3819d3f4-f993-4329-82a5-6ad3dde16633</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>a20a598b-0d91-4a8d-aed6-c3262130b6a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>O0110H1A</value>
      <webElementGuid>fb1e00ee-61cf-481d-ad7d-630c0f35b0c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>O0110H1A</value>
      <webElementGuid>bdc82101-ba9c-4a9e-80ca-d4b3e76e0b21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>f0330115-f631-428f-81e5-f110ce17429d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.O0110H1A</value>
      <webElementGuid>eb44b9ad-a459-41ed-b457-510f2869e0b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>'1'</value>
      <webElementGuid>fc2ad849-d159-4694-95ee-d023464a535c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-false-value</name>
      <type>Main</type>
      <value>'0'</value>
      <webElementGuid>160f12f0-7c1f-499d-877c-fab2dd327a15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.clearMItemField([$event,'O0110Z1A'],true)</value>
      <webElementGuid>30fea22a-46bc-40ba-8fd5-5e3ff2fad442</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>ec23226f-dd3f-4168-ae89-0e4d1139d7a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>6337df38-4648-4ebc-b7e7-e9d71b52dfd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;O0110H1A&quot;)</value>
      <webElementGuid>7cc2ce6d-e26b-480a-9793-ec02201e0b14</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='O0110H1A']</value>
      <webElementGuid>ea6e4f95-89b0-495e-b4ec-ea85417e043e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='O0110']/div/div[2]/div/div[22]/div[2]/div/input</value>
      <webElementGuid>1da50d61-714f-4610-8d07-33ecef86e765</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[22]/div[2]/div/input</value>
      <webElementGuid>2a19cbea-e77c-482e-97dc-cb585d1ee981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'O0110H1A']</value>
      <webElementGuid>c6301dcc-3196-4e62-a2fb-1417a839536f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
