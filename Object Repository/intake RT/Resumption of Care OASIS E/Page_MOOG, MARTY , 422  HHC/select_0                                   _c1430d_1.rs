<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _c1430d_1</name>
   <tag></tag>
   <elementGuidId>769ccbc4-1d30-4b58-9eec-54914c1569ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1322 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1322_NBR_PRSULC_STG1']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1322']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>2c3eef5f-26fc-449f-895e-1720bbfe0f58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>2bd4d069-2fd8-47d9-932c-5f6705519824</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1322_NBR_PRSULC_STG1</value>
      <webElementGuid>b60d1320-d17d-412b-bb0a-132fffb3f424</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1322_NBR_PRSULC_STG1</value>
      <webElementGuid>56972f11-7da1-463b-8581-53cc07689dd0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1322_NBR_PRSULC_STG1', oasis.oasisDetails.M1322_NBR_PRSULC_STG1)</value>
      <webElementGuid>7ee25dd1-bd8e-4a40-b5cd-a241a00795e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>e4da5c47-8b0e-4d46-9b10-dd26c97f3f70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                </value>
      <webElementGuid>c95cfa66-ecb7-4d15-bd91-dfe175c7e4dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1322&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>348981be-afe0-46e8-b85b-adf3415b33ff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1322']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>9a260fda-e520-41ef-b47a-4c0e9e80882b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[42]/following::select[1]</value>
      <webElementGuid>aad231e6-af74-480a-91a1-56fa90b6e110</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Number of unstageable pressure injuries presenting as deep tissue injury'])[1]/following::select[1]</value>
      <webElementGuid>13272ceb-6b30-4916-a311-57e629725c0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[43]/preceding::select[1]</value>
      <webElementGuid>0806cf67-73e7-40fc-9a94-818fc3269a75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stage 1'])[1]/preceding::select[2]</value>
      <webElementGuid>2ccc1243-0605-4b09-8e6e-e38f23cb2cd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[13]/div/div[2]/div[3]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>0cf83495-ed23-4bbb-9a4b-e0eed4df1715</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                ')]</value>
      <webElementGuid>66766905-468e-4397-a5f0-0d711a0f5d71</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
