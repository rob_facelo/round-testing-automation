<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Select an option--Community61 Acu_f1af28</name>
   <tag></tag>
   <elementGuidId>83e619dd-86fb-47ea-a184-f8e9b52dc8a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select.form-control.hhc-input-small.hhc-option.hhc-option-small.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[7]/div/div/div[2]/fieldset[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f27c2872-4f04-4555-8d9f-794178322ebf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control hhc-input-small hhc-option hhc-option-small ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>b85336c3-d715-47c7-9952-ee2440d1aa0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>ptoCtrl.ptoInput.SourceType</value>
      <webElementGuid>bb898276-357c-4279-8b7d-9a0b5d458380</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-options</name>
      <type>Main</type>
      <value>sourceType.id as sourceType.name for sourceType in ptoCtrl.SourceTypeList</value>
      <webElementGuid>81791190-eb77-4aa5-9642-ee72c9cad324</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>-- Select an option--Community61 Acute Hospital - Institutional62 SNF or Other - Institutional</value>
      <webElementGuid>d38e4cf3-8fab-4677-95e0-3c7d5318df33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-valid-mask ng-valid-maxlength ng-valid-minlength ng-dirty ng-valid-parse ng-valid-pattern ng-valid ng-invalid-validation&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/fieldset[2]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-3 col-lg-3&quot;]/select[@class=&quot;form-control hhc-input-small hhc-option hhc-option-small ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>bc5dbfc5-a5f4-4c3a-9d0c-a402518b6bca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[7]/div/div/div[2]/fieldset[2]/div/div[2]/select</value>
      <webElementGuid>106638ae-ffd6-4e9b-bdd6-fc860cc299c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Source Type:'])[1]/following::select[1]</value>
      <webElementGuid>2c4f9967-0e69-4b04-aa6d-da31dcb9301c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Care Plan'])[1]/preceding::select[1]</value>
      <webElementGuid>453a8202-dd6f-4cf7-9a3d-2f0f68a07011</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add New Problem'])[1]/preceding::select[1]</value>
      <webElementGuid>2436592f-0c5c-4bd8-be4d-99e7ac1eb5aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]/div/div[2]/select</value>
      <webElementGuid>6043ac70-6d27-4fa1-af6b-242c14438ad5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '-- Select an option--Community61 Acute Hospital - Institutional62 SNF or Other - Institutional' or . = '-- Select an option--Community61 Acute Hospital - Institutional62 SNF or Other - Institutional')]</value>
      <webElementGuid>42846a15-e431-4a59-a6ca-6c6ff6c52c2b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
