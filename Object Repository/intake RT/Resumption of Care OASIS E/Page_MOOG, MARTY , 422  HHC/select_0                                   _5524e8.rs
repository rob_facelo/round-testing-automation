<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _5524e8</name>
   <tag></tag>
   <elementGuidId>ec5ec159-a7a4-46ae-a0d8-10b12b76630b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.C1310A' and (text() = '
                                                    
                                                    0
                                                    1
                                                    -
                                                ' or . = '
                                                    
                                                    0
                                                    1
                                                    -
                                                ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='C1310']/div/div[2]/div/div/div[3]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b69bf0a5-20ba-4a31-a270-623358bb45f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>f5c3cf0f-ccee-4ec7-81cc-304cc143b6b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>C1310A</value>
      <webElementGuid>5f815f07-817a-4ff3-9501-729f120e2242</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>35c34685-8521-4407-8741-1a6990ccbd9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.C1310A</value>
      <webElementGuid>9b6ac12a-ebaa-4fce-9d11-b6c35d1057e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('C1310A', oasis.oasisDetails.C1310A)</value>
      <webElementGuid>e00d747c-7aea-48ab-8efb-b737b53c3e11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>84106417-ab8e-46aa-ab73-09b3e092ec82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    
                                                    0
                                                    1
                                                    -
                                                </value>
      <webElementGuid>3ee0d12e-4260-4f11-8646-cbaa4ee9d4e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;C1310&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-8 col-md-10 col-lg-10&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>e6510e1b-59f6-4153-8c5c-5221e8142191</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='C1310']/div/div[2]/div/div/div[3]/div/div/div[2]/select</value>
      <webElementGuid>9113431c-2a3b-486e-9215-800b78ac0612</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[13]/following::select[1]</value>
      <webElementGuid>abf7df2b-8c57-4d0e-8459-1b5581928268</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Acute Onset of Mental Status Change'])[1]/following::select[1]</value>
      <webElementGuid>78084458-1e86-4fad-9e2a-6b337cca459b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Is there evidence of an acute change in mental status'])[1]/preceding::select[1]</value>
      <webElementGuid>b1ff3263-e250-483f-94dd-10a42652c0fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[3]/div/div/div[2]/select</value>
      <webElementGuid>995d30c4-650e-4414-9e23-fefda5b0a3af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                    
                                                    0
                                                    1
                                                    -
                                                ' or . = '
                                                    
                                                    0
                                                    1
                                                    -
                                                ')]</value>
      <webElementGuid>cc5fd3b9-633f-4ce8-b732-81d413a406d8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
