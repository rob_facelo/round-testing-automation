<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _b7af5f</name>
   <tag></tag>
   <elementGuidId>ff6c7b80-acca-46fe-9ee1-593c73bdf209</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#GG0170RR1 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170RR1']/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>362bbf17-00e3-47ba-a96c-63a804224d57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>a25e4abe-877a-465b-82f9-a86b1490b9e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170RR1</value>
      <webElementGuid>c0418c0d-5d3e-4d3a-9a25-d883f3545bd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>284007f4-d5c9-43e8-9e17-784e16fbef0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170RR1</value>
      <webElementGuid>0c61e914-f12a-4630-853b-9a6d1b5d9dc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0170RR1', oasis.oasisDetails.GG0170RR1)</value>
      <webElementGuid>3dd0e7e8-9f30-4c2b-ba85-934d8ee5e930</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170Q1 == '0' || oasis.isLocked</value>
      <webElementGuid>c1864b7e-4f63-4253-b412-cfd2fe3f0999</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            
                                                            1
                                                            2
                                                            -
                                                        </value>
      <webElementGuid>cfea8904-292e-4a7b-883f-453e6eb74eda</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170RR1&quot;)/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>65f06604-ab66-47f3-b164-f62d53080148</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170RR1']/div/select</value>
      <webElementGuid>71d2d2ca-eb3a-4f50-ae28-7d27023cc1be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wheel 50 feet with two turns:'])[1]/following::select[1]</value>
      <webElementGuid>39d90044-8483-4857-a91b-2ca5d9d37914</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RR1. Indicate the type of wheelchair or scooter used.'])[1]/preceding::select[1]</value>
      <webElementGuid>3b21c0e0-d28e-42f4-ad13-bc46c6b17e87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manual'])[1]/preceding::select[1]</value>
      <webElementGuid>657c3b89-f0b9-4813-89c0-bfd979158817</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[19]/div/div[2]/div/div/div/select</value>
      <webElementGuid>e915e9c2-cef1-4475-9fbe-86215f9b58db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                            
                                                            1
                                                            2
                                                            -
                                                        ' or . = '
                                                            
                                                            1
                                                            2
                                                            -
                                                        ')]</value>
      <webElementGuid>1ae7dbf0-320a-4641-a2dd-fdd8de288ea5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
