<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1332</name>
   <tag></tag>
   <elementGuidId>7fc70236-daa9-4a8a-8182-4129d29a863e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1332 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1332_NUM_STAS_ULCR']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1332']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>721422c3-caf9-42f8-8644-1dc4778c9b1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>3fd66ffa-3851-4079-8696-401229f1cefb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1332_NUM_STAS_ULCR</value>
      <webElementGuid>34519c02-9341-4426-a4c2-e0636c9eb5d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1332_NUM_STAS_ULCR</value>
      <webElementGuid>5488d744-7ff2-48c3-b860-4b9d3d257d36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1332_NUM_STAS_ULCR', oasis.oasisDetails.M1332_NUM_STAS_ULCR)</value>
      <webElementGuid>dc9888ae-9371-4648-9c8f-1bb5fc09eada</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1330_STAS_ULCR_PRSNT == '00' || oasis.oasisDetails.M1330_STAS_ULCR_PRSNT == '03' || oasis.isLocked</value>
      <webElementGuid>34e37aaf-de37-46d7-915e-23218aeca7d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                </value>
      <webElementGuid>ff8af451-6fd9-4424-8ddb-e9732423c454</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1332&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>a9ba5d64-4131-450c-89ee-281636b9e04c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1332']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>ff77a2c9-e6dd-4ecf-abe4-e6d81c13dc6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[48]/following::select[1]</value>
      <webElementGuid>5bcf3e83-dd23-4415-b21b-9740174a490a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M1332. Current Number of Stasis Ulcer(s) that are Observable'])[1]/following::select[1]</value>
      <webElementGuid>c6f3b344-b816-437e-9b3f-b5577a94cb66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='One'])[2]/preceding::select[1]</value>
      <webElementGuid>1e1b53ab-49e2-4e86-91f1-e82f4c989350</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Two'])[2]/preceding::select[1]</value>
      <webElementGuid>5f9c6c04-bd59-4ae3-a102-3cd22c6c8b90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[13]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>f426071c-e360-49d0-bc7f-e53b0ee95b21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                ' or . = '
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                ')]</value>
      <webElementGuid>c0700062-a05f-4f4a-a9da-2f419cfb69ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
