<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>J0510</name>
   <tag></tag>
   <elementGuidId>aafc8fd6-9671-4b44-a21a-85579ec7a052</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select.form-control.enter-code.ng-valid.ng-dirty.ng-valid-parse.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.J0510']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='J0510']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>4ec4b87a-f89b-4f37-91e4-c879c9a11892</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-valid ng-dirty ng-valid-parse ng-touched</value>
      <webElementGuid>26175e28-b819-447c-b5fa-9302c6cd3ba1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>J0510</value>
      <webElementGuid>68478ba5-0e1d-4e3e-baa5-60a548823508</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>176aeefc-7c8d-4cb6-94ff-7f1c95c4ac20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J0510</value>
      <webElementGuid>9fdc991f-caa1-475c-ad56-92d51653544f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('J0510', oasis.oasisDetails.J0510)</value>
      <webElementGuid>2a2a338e-5999-4353-b4d8-74eac28a54d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>e5e486ef-6245-4bf4-9a40-5ae13f34ed6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    8
                                </value>
      <webElementGuid>5b655bce-dfb6-42d2-af25-44e93ab2f521</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;J0510&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-valid ng-dirty ng-valid-parse ng-touched&quot;]</value>
      <webElementGuid>4942874c-815f-45c8-b2fa-0959e13ef5b6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='J0510']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>3b054d9c-de00-4060-8eeb-3e458f6594a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[39]/following::select[1]</value>
      <webElementGuid>9ec2cbc0-c883-4f09-bf8f-9215601c0648</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='J0510. Pain Effect on Sleep'])[1]/following::select[1]</value>
      <webElementGuid>b015f291-09dc-43bb-af43-df4f3d1b4ece</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[11]/div/div[2]/div[2]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>79bac44d-5da7-48c3-bdc8-268df691987e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    8
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    8
                                ')]</value>
      <webElementGuid>cb636893-b212-4c34-afa1-0977ab541387</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
