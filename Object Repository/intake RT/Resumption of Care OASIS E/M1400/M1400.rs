<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1400</name>
   <tag></tag>
   <elementGuidId>96bc33b6-695b-48cb-a24a-d771ffb83b68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1400 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1400_WHEN_DYSPNEIC']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1400']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a85dbbf5-9c63-4220-b0dd-eeda8f914cdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>0c3b0fe0-b990-477a-bb7d-527e16a7094c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1400_WHEN_DYSPNEIC</value>
      <webElementGuid>20c5e745-ede3-4b04-83ad-e23ae9c7c8dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1400_WHEN_DYSPNEIC</value>
      <webElementGuid>c87eb8c3-430f-4e29-a9fa-3a70a5f323ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1400_WHEN_DYSPNEIC', oasis.oasisDetails.M1400_WHEN_DYSPNEIC)</value>
      <webElementGuid>8f924191-2023-4e2d-af2e-6e3f9de49331</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>665058e8-792d-491e-8495-56ea65908f35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                
                                            </value>
      <webElementGuid>a2d2c77e-3a53-4948-a0db-84729be3d53d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1400&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>beab1068-382f-49ec-bf08-0be63bdbf7a2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1400']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>22089e03-f0b8-460c-9385-9552f7be8cf2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[14]/following::select[1]</value>
      <webElementGuid>1aa10279-8bc5-43b8-8b7f-33b588fa63b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M1400) When is the patient dyspneic or noticeably Short of Breath?'])[1]/following::select[1]</value>
      <webElementGuid>48a81285-ab6d-4471-b395-e9a6ef0e2729</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient is not short of breath'])[1]/preceding::select[1]</value>
      <webElementGuid>97c6509e-8243-4585-8d23-633970bb34ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='When walking more than 20 feet, climbing stairs'])[1]/preceding::select[1]</value>
      <webElementGuid>533c3e28-16dc-4683-ac0b-ceddff524f3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[7]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>7590d3a9-5a95-4a38-be38-3c1b93e6a181</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                
                                            ')]</value>
      <webElementGuid>1e790ab1-5ad6-48fe-adc8-8d6adf19570b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
