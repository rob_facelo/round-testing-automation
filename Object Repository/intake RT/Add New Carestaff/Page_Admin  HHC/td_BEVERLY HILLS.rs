<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_BEVERLY HILLS</name>
   <tag></tag>
   <elementGuidId>5910afae-2185-42be-8c49-9e6bc24fe4ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td.font12.tdFlatPadding.verticalAlignMiddle.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_pgConCarestaffDetail_C0']/div/div/div/div[2]/div[2]/fieldset/div[2]/div[2]/div/div/div[6]/div[2]/autocomplete-directive/div/div/table/tbody/tr/td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>adbd7243-95c5-4971-8b55-cfca2df436ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>font12 tdFlatPadding verticalAlignMiddle ng-binding</value>
      <webElementGuid>1ffa8bee-8f81-4196-ae44-2cf3e2597e88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-bind</name>
      <type>Main</type>
      <value>item.city</value>
      <webElementGuid>e12f9d0d-8aea-4458-890e-f881ae4fa9ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>BEVERLY HILLS</value>
      <webElementGuid>4f8e01bb-7549-47eb-a835-dc91e057b8cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_pgConCarestaffDetail_C0&quot;)/div[1]/div[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-9 col-md-9 col-lg-9&quot;]/fieldset[1]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter margin-top-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row font14&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-7 col-lg-7&quot;]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/div[@class=&quot;dropdown-menu autocomplete-directive&quot;]/table[@class=&quot;table table-bordered table-responsive table-striped margin-bottom-sm&quot;]/tbody[1]/tr[@class=&quot;zipItem ng-scope&quot;]/td[@class=&quot;font12 tdFlatPadding verticalAlignMiddle ng-binding&quot;]</value>
      <webElementGuid>7b5b3d3f-60ad-4931-ae89-e4d44bac088a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_pgConCarestaffDetail_C0']/div/div/div/div[2]/div[2]/fieldset/div[2]/div[2]/div/div/div[6]/div[2]/autocomplete-directive/div/div/table/tbody/tr/td</value>
      <webElementGuid>2d43fc12-77e3-4182-8dc5-6debbe861c6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip Code'])[2]/following::td[1]</value>
      <webElementGuid>96f0af0f-0dde-44a9-9e5a-c610b364d7e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State'])[2]/following::td[2]</value>
      <webElementGuid>846c61a6-3085-483a-b635-d4dfc5d161b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CA'])[1]/preceding::td[1]</value>
      <webElementGuid>a19b2283-3213-4f0a-9c60-ebaf8d4f1e02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home Phone'])[1]/preceding::td[3]</value>
      <webElementGuid>687f7f0b-8876-40a0-91e8-45a25513c13f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='BEVERLY HILLS']/parent::*</value>
      <webElementGuid>fc8e52dc-e0be-453f-ace2-b905b28c085b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//autocomplete-directive/div/div/table/tbody/tr/td</value>
      <webElementGuid>9345940a-1ee8-40f6-aba9-86826734439d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'BEVERLY HILLS' or . = 'BEVERLY HILLS')]</value>
      <webElementGuid>80db4554-0278-4cfb-80cc-ecbe96760b88</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
