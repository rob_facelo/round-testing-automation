<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_HR RequirementsGeoPayrates</name>
   <tag></tag>
   <elementGuidId>99593bb6-8150-4fce-87ed-d3c94efd6809</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_pgConCarestaffDetail_T1 > div.tabTemplate</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='ContentPlaceHolder_pgConCarestaffDetail_T1']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6f3db993-70a1-44ab-904b-10bdfef59523</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tabTemplate</value>
      <webElementGuid>28bb6c67-4295-4cb1-8947-2fd4ff51bfce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    HR Requirements/Geo/Payrates
                </value>
      <webElementGuid>8dca629a-1871-4304-9a55-a71e63151896</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_pgConCarestaffDetail_T1&quot;)/div[@class=&quot;tabTemplate&quot;]</value>
      <webElementGuid>c58a05b9-bb49-47e8-bd0c-01146475e2ce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='ContentPlaceHolder_pgConCarestaffDetail_T1']/div</value>
      <webElementGuid>a0489731-3983-4cb9-b9f2-74d8abb6d2e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Info'])[2]/following::div[1]</value>
      <webElementGuid>a7233350-9a89-41bf-8e69-b847a22d58e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Info'])[1]/following::div[2]</value>
      <webElementGuid>b49a3572-1cfd-4c21-ab3c-14a138fc9922</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HR Requirements/Geo/Payrates'])[2]/preceding::div[1]</value>
      <webElementGuid>ab0887be-ec78-4377-b86a-36137b686932</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Companies'])[1]/preceding::div[2]</value>
      <webElementGuid>0fbbc600-73f2-428e-9d24-0b7a76164444</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='HR Requirements/Geo/Payrates']/parent::*</value>
      <webElementGuid>54ab8eca-341b-439a-b365-e4172d024432</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/ul/li[5]/div</value>
      <webElementGuid>b0b569fe-9cd9-4d97-a9e8-c7b60f601e60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    HR Requirements/Geo/Payrates
                ' or . = '
                    HR Requirements/Geo/Payrates
                ')]</value>
      <webElementGuid>c7d7ddd2-9a3e-4b9c-acfd-71b773072ab2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
