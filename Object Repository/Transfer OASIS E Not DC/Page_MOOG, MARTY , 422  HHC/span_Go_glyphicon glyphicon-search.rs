<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Go_glyphicon glyphicon-search</name>
   <tag></tag>
   <elementGuidId>bf394b32-a7f5-4dd8-b119-43d923d68826</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-search</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>55ba8027-3358-456a-bf37-edc28c297b3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-search</value>
      <webElementGuid>e799c300-acbb-4af4-b1d3-25698c00c847</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/fieldset[1]/div[@class=&quot;row&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12&quot;]/span[@class=&quot;glyphicon glyphicon-search&quot;]</value>
      <webElementGuid>b70711f1-0eff-43ad-958d-94ce0d471eb4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button/span</value>
      <webElementGuid>99f173c3-9d85-43c3-b9b4-d560db8e0aea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div[2]/fieldset/div/button/span</value>
      <webElementGuid>3a050dd3-b72a-4c9b-b351-ca3fd97fc923</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
