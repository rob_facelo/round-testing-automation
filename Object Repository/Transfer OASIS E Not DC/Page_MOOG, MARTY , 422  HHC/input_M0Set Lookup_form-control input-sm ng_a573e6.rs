<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_M0Set Lookup_form-control input-sm ng_a573e6</name>
   <tag></tag>
   <elementGuidId>96f8c79c-5d35-430c-98a4-ff2dbeb9e684</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @ng-model = 'oasis.goToMosetVal']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-3.col-md-2.col-lg-2 > div.form-inline.input-group > input.form-control.input-sm.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[82]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d20e2963-c38e-4367-82f0-31ee4432f242</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>47a3ec39-b33f-41e3-8b26-260fb67c5b21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>a253d921-4947-4001-b4e0-3c689ecbaff6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.goToMosetVal</value>
      <webElementGuid>22ea461a-6f44-45ff-8abd-6b16486a39d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>!oasis.oasisDetails.FULL_OASIS</value>
      <webElementGuid>b5801aa7-b8ae-47dc-abc1-a7c87a016236</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-3 col-md-2 col-lg-2&quot;]/div[@class=&quot;form-inline input-group&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>e38f55b3-ad49-41f0-a0e1-57a1f7f925af</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[82]</value>
      <webElementGuid>6d60047c-d4e5-4b0a-a51a-3678317047e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div/div/input</value>
      <webElementGuid>f3fb9fb8-85d5-4955-9c94-5e5b3e293ef0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/input</value>
      <webElementGuid>41b14c0f-4f7f-4def-9b37-dc467c8aed17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>ef851d30-b706-4326-909a-4019645eb3fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
