<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _5d470c</name>
   <tag></tag>
   <elementGuidId>483e5c0a-d3f7-4c34-ac27-1d1ca1c539c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M2410 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2410']/div/div[2]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>dccab519-990a-4b67-a568-253eb8557323</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>db011da1-0fb1-4cf5-978a-a00e23f54dba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2410_INPAT_FACILITY</value>
      <webElementGuid>973be892-a4bd-46d7-a1b4-1850111c0e64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2410_INPAT_FACILITY</value>
      <webElementGuid>4d7e5b65-e659-480b-bed0-f0c5b3fc3e6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2410_INPAT_FACILITY', oasis.oasisDetails.M2410_INPAT_FACILITY)</value>
      <webElementGuid>74b7f503-b5d2-4577-89ff-8ebd1a88a48c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>d92689f5-0c45-447a-87f7-829e2a0a86a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        1
                                        2
                                        3
                                        4
                                        
                                    </value>
      <webElementGuid>329a0d23-aac6-4a43-aca9-f3c859e9432c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2410&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>a1ae9020-d814-457f-8a78-49748863bd32</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2410']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>682fa8e6-d454-42a9-8483-ce4795d2c378</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[4]/following::select[1]</value>
      <webElementGuid>ae428b49-1f10-4cbc-bfdb-1ec344d9f5c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M2410. To which Inpatient Facility has the patient been admitted?'])[1]/following::select[1]</value>
      <webElementGuid>21c83143-6e1a-4a9b-beca-b6bd4ba91551</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hospital'])[1]/preceding::select[1]</value>
      <webElementGuid>de673563-e941-405b-b4dd-b66528bdf0d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rehabilitation facility'])[1]/preceding::select[1]</value>
      <webElementGuid>b736d725-56eb-4e45-ad76-faf4f5c59abf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[14]/div/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>d9c68820-b430-4247-b5ae-bd448f566642</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        1
                                        2
                                        3
                                        4
                                        
                                    ' or . = '
                                        
                                        1
                                        2
                                        3
                                        4
                                        
                                    ')]</value>
      <webElementGuid>c9fac4a4-62ec-490a-ac59-aa066ea9feec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
