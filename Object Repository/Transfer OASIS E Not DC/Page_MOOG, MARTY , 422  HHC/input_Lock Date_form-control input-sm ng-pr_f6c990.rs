<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Lock Date_form-control input-sm ng-pr_f6c990</name>
   <tag></tag>
   <elementGuidId>ec3df7c2-ac97-4b00-9846-fca5ef0949c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @ng-model = 'lockDate' and @placeholder = 'MM/dd/yyyy']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.form-control.input-sm.ng-pristine.ng-untouched.ng-invalid.ng-invalid-required.ng-valid-pattern.ng-valid-maxlength</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[97]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>52eecf43-191b-4235-93f1-d069e35fa866</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e49ca3c5-bdad-42b7-9d54-6ccd7e6b20ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern ng-valid-maxlength</value>
      <webElementGuid>14a5189e-3347-43df-8779-20aa3cb5e28d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>lockDate</value>
      <webElementGuid>55bc56b1-53c0-4530-9da4-6efcd21920f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,false,'1')</value>
      <webElementGuid>0f0d426c-9e11-4cf3-9732-5983bfd7db73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>e2f99b0e-927b-44e1-b6ad-d5c80cc185e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>3e7df36d-e257-4e5a-b201-e96c492baf85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-pattern</name>
      <type>Main</type>
      <value>/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/</value>
      <webElementGuid>4cbdf079-241e-4c07-831d-3af731efa3e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>date</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>5db6095a-43bb-43cb-95e4-01dc1469ae87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>a961c22e-6365-437d-bacf-37864399c0b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-109 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body form-inline&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern ng-valid-maxlength&quot;]</value>
      <webElementGuid>348817df-6d14-4b7f-a5d8-3169510a7d42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[97]</value>
      <webElementGuid>f73da78d-24cf-4141-94b1-36ec6106e30c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div[2]/div/input</value>
      <webElementGuid>02166882-e224-4333-9b80-fd0f8f6fcec0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = 'MM/dd/yyyy']</value>
      <webElementGuid>9c757b81-de17-429c-81b8-883874b90994</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
