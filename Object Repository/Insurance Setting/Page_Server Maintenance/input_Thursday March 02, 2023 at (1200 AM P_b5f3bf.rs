<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Thursday March 02, 2023 at (1200 AM P_b5f3bf</name>
   <tag></tag>
   <elementGuidId>db3e2103-c3a5-4985-904b-b0e5b5f63e36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#btnContinue</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='btnContinue']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e35e12ca-2b71-48c7-80fe-8a72d4c47dc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>359a2a8e-7c16-471f-9f40-de919f320cd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>btnContinue</value>
      <webElementGuid>4d2ad8b8-2df3-4661-a87c-62d2023303d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>4bb57998-ceff-422e-bb26-00b6efda9d93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnContinue</value>
      <webElementGuid>4a739185-99bb-4765-82f9-e32b235416ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>LogoffButton</value>
      <webElementGuid>0ad42472-c538-4cc3-a396-d05896ca1ec9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnContinue&quot;)</value>
      <webElementGuid>391839ec-8b34-467e-9cae-c51e925a3998</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='btnContinue']</value>
      <webElementGuid>db05b80c-7445-42fb-9e37-ce775d51fc60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='formBox']/div[2]/input</value>
      <webElementGuid>d9ade12e-9dfa-4ec4-958a-395e4b2f42bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/input</value>
      <webElementGuid>0d6afdf8-7a2e-46eb-af91-9271c9858522</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'btnContinue' and @id = 'btnContinue']</value>
      <webElementGuid>74135a2c-a312-43bb-88f3-965bb4ac473c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
