<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Zip                                    _d1db20</name>
   <tag></tag>
   <elementGuidId>377d9355-6513-4b78-96b2-112e7c4d6d33</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[8]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3abf1a9f-77b0-4927-9cbc-c64d9816cc55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>d71c3b51-444a-48a2-968f-89798e8e8da7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            </value>
      <webElementGuid>bb368bf4-76c4-4c69-a79c-6b07aa09f0ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC&quot;)/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>de5f56b9-f698-4772-a6b4-f4bfb4886604</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_InsuranceDetailsControl_pnlInsuranceDetails_RPC']/div[8]</value>
      <webElementGuid>3d96e474-1301-4ed8-bf3d-dca1c6a91fcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::div[2]</value>
      <webElementGuid>d7c5f2d0-d221-46d2-bc61-ef75d2a105eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div/div/table/tbody/tr/td/div[8]</value>
      <webElementGuid>d9292bc4-89f6-44e4-978d-63d6ca72e96d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            ' or . = '
                                
                                    Zip:*
                                
                                
                                    
                                    
    
    
        
            
                
                    City
                    State
                    Zip Code
                
            
            
                
            
        
    


                                
                            ')]</value>
      <webElementGuid>1dbe8a8b-4368-4eed-871c-5fa5e075c441</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
