<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Save_button step 3</name>
   <tag></tag>
   <elementGuidId>077f5c0c-dbcd-44a4-8e18-c55c191f7b76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='intakewinfo']/div[4]/div/div/div/div/div/button/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.well > div > div.modal-header > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > button.btn.btn-default.font12 > i.fa.fa-floppy-o.font-weight-600</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[1]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12&quot;]/i[@class=&quot;fa fa-floppy-o font-weight-600&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>502e16d8-0926-4910-988c-a202071b3702</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-floppy-o font-weight-600</value>
      <webElementGuid>1a32e41c-4015-415b-98bc-d319aef46066</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[1]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12&quot;]/i[@class=&quot;fa fa-floppy-o font-weight-600&quot;]</value>
      <webElementGuid>c63d05e6-f8c9-45bd-bc37-e9590f6fcfa2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[4]/div/div/div/div/div/button/i</value>
      <webElementGuid>9e873fbc-1e48-42e4-a6b0-af4f89d738de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div/div/button/i</value>
      <webElementGuid>9b0bb0dc-17fe-4221-9e9c-0f4e67f1533b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
