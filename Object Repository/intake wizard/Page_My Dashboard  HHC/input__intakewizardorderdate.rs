<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__intakewizardorderdate</name>
   <tag></tag>
   <elementGuidId>7675fb9b-84b3-446c-bbb6-e826682a5152</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='intakeOrderDate']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#intakeOrderDate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>92e539be-fa4f-48d6-9440-1b590b979e06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>intakeOrderDate</value>
      <webElementGuid>b7971007-e38d-4148-b4bc-71c3ea0654a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>intakewizardorderdate</value>
      <webElementGuid>acebb30d-a3df-4a49-a39a-891d91c635c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>0e71206f-a5dc-4972-9c99-6eb3e3fe0786</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>model-view-value</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>799cc596-97bd-4921-a544-47a6531fedfe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>showtoday</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7124dfc0-6b7b-4c2d-b089-4c2f8444b3a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>2c8afe99-c6df-47a0-95b4-4b08741c6e9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-pattern</name>
      <type>Main</type>
      <value>/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/</value>
      <webElementGuid>12704030-2811-4d36-95af-b84826cdc760</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>4af9b4dc-629e-4d4d-bbae-dd0e7cd713fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control font14 ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern ng-valid-maxlength</value>
      <webElementGuid>d926a33e-fb70-44ec-9960-e7bd73a72d91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>piCtrl.ptoInput.CallDate</value>
      <webElementGuid>9a09ec14-b7d7-42c0-8af2-b48ef7d08f41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>date</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>7fe9ff43-be7d-4097-bc6b-5295cbb61ee1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this, this.value, event, false, '1')</value>
      <webElementGuid>dc4c5d8f-a3b3-4166-96d5-fb5c5b0f0a19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>c79b5e31-ccd6-4ef6-8310-d7395ad533e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeOrderDate&quot;)</value>
      <webElementGuid>e1e5603e-761d-45da-81e2-6a87709b420f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='intakeOrderDate']</value>
      <webElementGuid>4f1c30a6-2407-4804-bfa6-6063af395c9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[3]/div/div/div[3]/div[2]/div/div/div[3]/div/div/input</value>
      <webElementGuid>789d5768-3233-4e70-92c8-f03094e24b7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/div/div[3]/div/div/input</value>
      <webElementGuid>69bfdcb2-c46a-46fa-acf2-4eb4f1a55a8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'intakeOrderDate' and @name = 'intakewizardorderdate' and @type = 'text' and @placeholder = 'MM/dd/yyyy']</value>
      <webElementGuid>49e35264-86e7-49c1-897e-e4e31ffb9a26</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
