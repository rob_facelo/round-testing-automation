<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__address</name>
   <tag></tag>
   <elementGuidId>914a3e82-2b57-4950-8897-80a3c630fd06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='address']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;address&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7e32dd77-3f6d-46b2-8d11-6a824a3131fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>03ecb2ac-8bd4-4935-a4c5-175326d70e65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>address</value>
      <webElementGuid>9cd7f3c5-7669-4e0d-a5fb-aada386f5692</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>50</value>
      <webElementGuid>6e624aa9-8908-48f8-8534-1297759e0a06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>piCtrl.PatientInfo.Address</value>
      <webElementGuid>037e7d43-cb62-40bb-8732-cc17bf278321</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm font14 ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-maxlength</value>
      <webElementGuid>9eb4ff5c-7e7a-49bc-aa21-7ca0bdf5f019</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row margin-top-10px&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-8 col-lg-8&quot;]/input[@class=&quot;form-control input-sm font14 ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-maxlength&quot;]</value>
      <webElementGuid>4cae53f6-dda2-4589-a130-dc71b63ddcb5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='address']</value>
      <webElementGuid>6b4515f6-92fd-48bf-9f34-344380fad53a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div[3]/div/div/div[2]/div/div/div[3]/div[2]/input</value>
      <webElementGuid>68dfc7a6-036b-4860-9281-6ca6d5e44ef7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[2]/div/div/div[3]/div[2]/input</value>
      <webElementGuid>93f704ad-50c3-44b3-a711-84c6206abf2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'address']</value>
      <webElementGuid>94543a66-59af-4711-9dc5-4fff18e2aefa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
