<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_3 - Black or African-American</name>
   <tag></tag>
   <elementGuidId>271db74f-0f13-42a0-8e25-6a7cde0afbd0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Race_EthnicityPanelintakew']/div[2]/div/div[2]/div/div/label</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>dcb69671-6022-40d1-835c-6c4dfbb849e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox-inline</value>
      <webElementGuid>043440dd-06b1-4666-804f-fd6303df6f16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                    3 - Black or African-American
                                                                                </value>
      <webElementGuid>b74dc655-c7a7-49c8-af06-36b7be726605</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Race_EthnicityPanelintakew&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-3 col-lg-3&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/label[@class=&quot;checkbox-inline&quot;]</value>
      <webElementGuid>bfbc829e-981b-4e24-860b-855a347957ce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Race_EthnicityPanelintakew']/div[2]/div/div[2]/div/div/label</value>
      <webElementGuid>b75c6138-3ac8-450b-bac0-5683e0162834</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mark all that apply'])[1]/following::label[3]</value>
      <webElementGuid>c5dc9692-f86f-4950-b514-28d1c008abab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='3 - Black or African-American']/parent::*</value>
      <webElementGuid>941c7fd9-1647-49c0-a14c-f9acc9b58b03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div[2]/div/div[2]/div/div/label</value>
      <webElementGuid>18596257-7054-4298-a69f-a5bccab499ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                                                                                    3 - Black or African-American
                                                                                ' or . = '
                                                                                    3 - Black or African-American
                                                                                ')]</value>
      <webElementGuid>abac3894-e447-4bc5-b124-6827408d0f30</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
