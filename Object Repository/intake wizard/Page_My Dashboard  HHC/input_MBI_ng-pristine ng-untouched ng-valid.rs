<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_MBI_ng-pristine ng-untouched ng-valid</name>
   <tag></tag>
   <elementGuidId>9291839d-2160-4994-bb34-663599d3b3fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[95]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.input-group-addon > label.checkbox-inline > input.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>6033cf3d-6aa9-409a-aaf3-00cb2942a6d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>piCtrl.PatientInfo.IsMBI</value>
      <webElementGuid>49a1d090-1e38-4bf1-9626-ef8194f4439d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>piCtrl.CheckMedicareFormat()</value>
      <webElementGuid>8a104179-2f1f-4f66-8162-a66339e9b7f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>da85d148-23eb-45f4-ba35-3521fbe4f55c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>8273ef7a-d2b2-4d53-8810-be6a216a80b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakewinfo&quot;)/div[@class=&quot;row setup-content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;col-md-12 well&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row margin-top-15px margin-bottom-15px&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-4 col-lg-4&quot;]/div[@class=&quot;input-group&quot;]/span[@class=&quot;input-group-addon&quot;]/label[@class=&quot;checkbox-inline&quot;]/input[@class=&quot;ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>5fffb0b0-9319-4d5f-b08d-f73c53171d89</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[95]</value>
      <webElementGuid>b82b769e-9f57-4cf8-8421-68e86f4c641c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[2]/div/div/div/div/div[6]/div/div/div/div/span/label/input</value>
      <webElementGuid>a71cb7f9-d3a1-40d7-ba65-018b7ca243e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/label/input</value>
      <webElementGuid>47183e2d-d78d-4da5-96fb-662dcc91761e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>0ba2f979-8d6c-4528-b9f2-796588b45725</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
