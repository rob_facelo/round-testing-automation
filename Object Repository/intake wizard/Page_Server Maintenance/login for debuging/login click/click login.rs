<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click login</name>
   <tag></tag>
   <elementGuidId>d970373e-bf50-4b40-a37f-b75b551b3028</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolderLogonPage_LoginControl_loginbtn</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a3f8c165-1c79-4a26-bf9e-6c29531cecfb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>feb914f1-99a5-47f7-91df-58808f84d5a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolderLogonPage$LoginControl$loginbtn</value>
      <webElementGuid>5ab8cb96-bc16-4bc4-8e17-d40754b994bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>LOGIN</value>
      <webElementGuid>e7a99b63-234b-4ea3-a340-7174ba28aa6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolderLogonPage_LoginControl_loginbtn</value>
      <webElementGuid>0350eb96-bef5-4ba6-9751-9c1204d3eac6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-btn-style btn btn-primary</value>
      <webElementGuid>2c099735-441f-4863-b97e-f743a49ffebe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolderLogonPage_LoginControl_loginbtn&quot;)</value>
      <webElementGuid>2038df74-1a89-47a2-b886-5283facd87b9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      <webElementGuid>0919f75b-7083-47cc-bbb6-58f079f9db36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]/input</value>
      <webElementGuid>1297a8d2-e039-4230-ad30-97514a3bc2f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>4c1e159b-0256-47f1-b18e-7bf588ae6381</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'ctl00$ContentPlaceHolderLogonPage$LoginControl$loginbtn' and @id = 'ContentPlaceHolderLogonPage_LoginControl_loginbtn']</value>
      <webElementGuid>adc7e62f-e9bb-4b64-ae01-f07116a58d1c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
