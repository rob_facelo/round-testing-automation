<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Tuesday July 12, 2022 at (0100 AM PST_103555</name>
   <tag></tag>
   <elementGuidId>adc62f53-eb89-41fa-a880-34bab1741db7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='btnContinue']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#btnContinue</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7a84f5b8-c120-4f39-84f5-370d530f8ad8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>e777967f-bcc3-47fe-adc9-7b7b98c6227b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>btnContinue</value>
      <webElementGuid>f58fe9cf-0f22-4464-bbfa-085251e7ab74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>527f5127-84f1-4944-903a-735ab47a8ad2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnContinue</value>
      <webElementGuid>1a434c79-df58-4344-b342-76eb599ad7ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>LogoffButton</value>
      <webElementGuid>5b11e14b-3dbc-4662-9475-2bfc08de7ae1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnContinue&quot;)</value>
      <webElementGuid>d5ec1430-83fe-4b82-94f0-fe5a46ede622</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='btnContinue']</value>
      <webElementGuid>1c5d3ed1-2304-4297-821d-e5560120e471</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='formBox']/div[2]/input</value>
      <webElementGuid>61e60fb8-b4e5-4df4-8e63-ec5775b85392</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/input</value>
      <webElementGuid>1c15c55d-ae64-4c42-b005-556f836f14b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'btnContinue' and @id = 'btnContinue']</value>
      <webElementGuid>62d11335-0b56-4834-9004-2d7fb5f2e10b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
