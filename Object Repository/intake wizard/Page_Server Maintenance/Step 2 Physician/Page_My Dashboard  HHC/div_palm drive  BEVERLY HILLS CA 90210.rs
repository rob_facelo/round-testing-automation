<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_palm drive  BEVERLY HILLS CA 90210</name>
   <tag></tag>
   <elementGuidId>a2591a81-1298-4de7-ae12-e094a54c30d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//autocomplete-directive[@id='intakeWizardPhys']/div/ul/li/a/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>647deb8c-29e8-4c4f-a885-438a2a9b84bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>86725625-655b-435b-8335-d837caf86c9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        palm drive  BEVERLY HILLS CA 90210
                    </value>
      <webElementGuid>fabcd2d6-bdf5-4497-8a60-939e2e6ba09e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeWizardPhys&quot;)/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>71c4b9f4-8369-42b8-aba0-53410fab49b9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//autocomplete-directive[@id='intakeWizardPhys']/div/ul/li/a/div[2]/div</value>
      <webElementGuid>5b0e655d-2641-4a4c-bda1-471d8c89fee1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[14]/following::div[6]</value>
      <webElementGuid>d0499f29-5aa4-45be-878e-d7bd79f7e0ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[43]/preceding::div[1]</value>
      <webElementGuid>4f892db4-37df-4663-8803-d6bf789715c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Caller Information'])[1]/preceding::div[4]</value>
      <webElementGuid>e277bed2-0215-46b1-8651-141e62f043c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div[2]/div</value>
      <webElementGuid>7e8ad1a6-472a-4898-9acc-a29a959e4220</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        palm drive  BEVERLY HILLS CA 90210
                    ' or . = '
                        palm drive  BEVERLY HILLS CA 90210
                    ')]</value>
      <webElementGuid>dc22d09f-7614-4f65-b6ab-d77d340f923b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
