<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_walker, sarah - RN</name>
   <tag></tag>
   <elementGuidId>0492a90c-dfac-4303-89ad-f5e62570cdd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>fieldset > autocomplete-directive.ng-isolate-scope > div.dropdown > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a > div.row > div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='selectcarestaffintakewizard']/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
                        walker, sarah - RN
                    ' or . = '
                        walker, sarah - RN
                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c039f28c-a793-4663-a0dc-7513b855772d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>3249e79b-b09e-4c93-a835-078bf18b0987</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        walker, sarah - RN
                    </value>
      <webElementGuid>3ffd898c-3d28-42dd-8032-20b80ce64a99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selectcarestaffintakewizard&quot;)/fieldset[1]/autocomplete-directive[@class=&quot;ng-isolate-scope&quot;]/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>b761d1a6-785c-41f3-82ac-81b9a4d4293e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='selectcarestaffintakewizard']/fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>5267a1c6-30e3-4bda-be3e-d84572d22ad7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/following::div[7]</value>
      <webElementGuid>2e7c966c-d1c7-4b6d-88f8-2ba69465e631</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[7]/following::div[7]</value>
      <webElementGuid>c38cce3c-08c5-4dd2-ad38-1bf3273de358</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[7]/preceding::div[3]</value>
      <webElementGuid>2c9b6096-8ad4-4d11-ba8e-e42988817cdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks:'])[2]/preceding::div[3]</value>
      <webElementGuid>b453ab30-7456-4128-9411-65efc421959e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='walker, sarah - RN']/parent::*</value>
      <webElementGuid>a3e3c66e-6ad0-4a1c-957f-33fcb14f9ed8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/autocomplete-directive/div/ul/li/a/div/div</value>
      <webElementGuid>6ca9ff06-db3a-4ccd-a45f-0453f5e14d78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        walker, sarah - RN
                    ' or . = '
                        walker, sarah - RN
                    ')]</value>
      <webElementGuid>cb1b7ac6-b6ff-4d12-b42c-24e0523cd70c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
