<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_palm drive  BEVERLY HILLS CA 90210</name>
   <tag></tag>
   <elementGuidId>10354f8f-59f1-453c-b2ca-415a59479425</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//autocomplete-directive[@id='intakeWizardPhys']/div/ul/li/a/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;intakeWizardPhys&quot;)/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7ba83188-50d5-46d1-aa35-9152306f3ee2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>1eb60da1-e7b6-4e5a-bd33-c629cce31ef2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        palm drive  BEVERLY HILLS CA 90210
                    </value>
      <webElementGuid>5b6de8eb-ca4d-489b-92c4-21c7116e3b94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeWizardPhys&quot;)/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>c451fd0b-7fcd-491b-8505-91cfdfc6f34d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//autocomplete-directive[@id='intakeWizardPhys']/div/ul/li/a/div[2]/div</value>
      <webElementGuid>47836149-4cd4-41f9-8b62-e9ed4d7e2c8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[14]/following::div[6]</value>
      <webElementGuid>3a53e640-44b6-4588-a789-40530c1f97c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[43]/preceding::div[1]</value>
      <webElementGuid>2a7939df-2bf5-4628-bac0-a4bc3c43d283</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Caller Information'])[1]/preceding::div[4]</value>
      <webElementGuid>25217424-e2aa-447b-b89c-ac575e81b924</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div[2]/div</value>
      <webElementGuid>59e4275c-be19-4c02-8a69-72b4b1112347</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        palm drive  BEVERLY HILLS CA 90210
                    ' or . = '
                        palm drive  BEVERLY HILLS CA 90210
                    ')]</value>
      <webElementGuid>5ac84c57-c9f4-40ee-a2fa-bfb0ae5ddaf5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
