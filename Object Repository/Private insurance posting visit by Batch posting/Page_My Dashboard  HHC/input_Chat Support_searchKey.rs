<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Chat Support_searchKey</name>
   <tag></tag>
   <elementGuidId>02cc0804-71d4-4e68-947b-2e5e677fc88f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='searchKey']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#searchKey</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>950e1a53-907f-4481-be62-1ee5b57f5c2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>c22eaf97-7390-4b07-851a-06ac978735a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>cdde87aa-c85f-4178-8c6b-f48c8e7999b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control search-field-holder searchbox ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>24140ecc-e320-44ed-952c-f23220b775cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search for patient...</value>
      <webElementGuid>6975a335-692e-44a4-b7c6-1270f01466ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>644b5b58-c8d4-4cff-a7c9-4a78cc8f3657</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>search.startSearch()</value>
      <webElementGuid>98b0cca3-df37-4a49-85bf-039670945a8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>search.searchvalue</value>
      <webElementGuid>75822f40-7cb4-4593-9409-c5bfee2d7cec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-init</name>
      <type>Main</type>
      <value>search.searchvalue=''</value>
      <webElementGuid>e4909e76-9be9-48f3-90b9-538473fe4bd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 750}</value>
      <webElementGuid>f12c1dd1-4050-4bd4-8ef9-efd4ce9ea962</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>6fd4adac-e19d-46a7-ae61-fb3b5b549734</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>50</value>
      <webElementGuid>613c7db7-4698-4230-9c84-a79d11359600</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchKey&quot;)</value>
      <webElementGuid>42175ffd-9fe4-4940-b1f4-0d96331bb6ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='searchKey']</value>
      <webElementGuid>a0d06096-ed8e-49ef-b055-5387577dd83e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/input[3]</value>
      <webElementGuid>783f4680-1b4f-4415-8847-409022e39bac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input[3]</value>
      <webElementGuid>6f8a69a6-cf42-4a2f-a5d2-1df3a04a3673</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'searchKey' and @id = 'searchKey' and @placeholder = 'Search for patient...' and @type = 'text']</value>
      <webElementGuid>93995d6a-e1dd-4380-8f7f-c2ffd28d7c23</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
