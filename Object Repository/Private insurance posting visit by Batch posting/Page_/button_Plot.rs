<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Plot</name>
   <tag></tag>
   <elementGuidId>d7fdc8f9-2137-4adb-b610-4666f3bdbbe0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[202]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'plotVisit.showPlotFrequency()' and (text() = 'Plot' or . = 'Plot')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-content > div.modal-footer > div.ng-scope > button.btn.btn-primary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>22fa7f7d-c898-4724-9234-5fe730862dfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>503ddf58-205a-41e8-9770-ae8c32bd6a84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.showPlotFrequency()</value>
      <webElementGuid>cb636187-68fc-4003-90ed-2d8341a82d78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>028b7457-9246-401d-8d35-c247930df4e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Plot</value>
      <webElementGuid>6d3b6bb9-d613-4083-a2b1-713099c2c8f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer&quot;]/div[@class=&quot;ng-scope&quot;]/button[@class=&quot;btn btn-primary&quot;]</value>
      <webElementGuid>53676c9d-b8c5-4520-bab5-0e621475025a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[202]</value>
      <webElementGuid>f05e2e9e-1759-40bd-b40f-a5c1eecdc393</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[3]/div/button[2]</value>
      <webElementGuid>73b0d648-600c-4da1-977a-a423aac7cd59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear Selected'])[1]/following::button[1]</value>
      <webElementGuid>c37a12a2-7b7a-4ecb-a7c9-c840e126e949</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Generate Planned Visits'])[1]/following::button[2]</value>
      <webElementGuid>08a6771e-d810-426b-b508-4a31af308acd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Done'])[1]/preceding::button[1]</value>
      <webElementGuid>0a7a4710-4213-4524-a20c-eb9535528873</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Plot']/parent::*</value>
      <webElementGuid>e7749754-bd05-4a65-80df-e54103e36980</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div[3]/div/button[2]</value>
      <webElementGuid>6b118d6f-5acf-41de-8f2b-f89c1ea98058</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Plot' or . = 'Plot')]</value>
      <webElementGuid>a5eaf685-7cb3-4966-830a-6b194d13270f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
