<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Generate Planned Visits</name>
   <tag></tag>
   <elementGuidId>822dd817-e967-429c-abd2-e919ca749432</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[200]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'plotVisit.PlotVisits()' and (text() = 'Generate Planned Visits' or . = 'Generate Planned Visits')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.boxShadow.GenerateVisitPanel > div.modal-footer > button.btn.btn-primary.btn-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>38638c80-8dfa-49e4-b651-0b0fcd51108f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>51b016d8-5b50-4358-821c-4ee585568011</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.PlotVisits()</value>
      <webElementGuid>eeb0adc2-2a7e-404f-93a2-24bd91bd3968</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dismiss</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>edbfa984-404d-47f9-901c-5babfab43afa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-sm</value>
      <webElementGuid>3d5cadea-7500-4be6-961e-6895d6985cc0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Generate Planned Visits</value>
      <webElementGuid>0aca8aa5-ea8c-4bad-92c7-aa5bdc9d0ca5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow GenerateVisitPanel&quot;]/div[@class=&quot;modal-footer&quot;]/button[@class=&quot;btn btn-primary btn-sm&quot;]</value>
      <webElementGuid>297e1ae6-bb4e-45e6-88b6-68d40b461f91</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[200]</value>
      <webElementGuid>62a21f50-05ae-4020-8ce7-e19084742ffc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div[4]/div[3]/button</value>
      <webElementGuid>08d0a7b4-76bf-43c3-9437-b01fe6baddd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PTO Reason:'])[1]/following::button[1]</value>
      <webElementGuid>9010829d-17fa-4922-b1ed-2419d94b4941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear Selected'])[1]/preceding::button[1]</value>
      <webElementGuid>6c8d8f1e-002c-4b0e-a5bc-7683171528ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plot'])[1]/preceding::button[2]</value>
      <webElementGuid>42942468-7788-4404-887c-eb3e0ede2f52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Generate Planned Visits']/parent::*</value>
      <webElementGuid>72fab82d-098e-4503-bf1b-6e4e4a7e3558</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div[2]/div[4]/div[3]/button</value>
      <webElementGuid>5dac77c1-51c5-4d0b-997b-d96a945c3e3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Generate Planned Visits' or . = 'Generate Planned Visits')]</value>
      <webElementGuid>64af7afd-a462-4a18-84e2-24f26b385fd4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
