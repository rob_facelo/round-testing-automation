<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Time In_form-control fontsize-13-weig_ecf399</name>
   <tag></tag>
   <elementGuidId>ed9aad2c-1c87-429c-9793-a6dbe9c34f1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[113]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @ng-model = 'plotVisit.TimeInHr' and @placeholder = '00']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.form-control.fontsize-13-weight-600.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlength</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>46287142-daca-4c9b-94dd-0af905c3f0f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>bba0578d-6b90-4673-9ec7-e383c4eb83cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>332bb39a-fd16-41a5-a436-72ba39182103</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>extractNumber(this, 0, false); isvalidMilhour(this);</value>
      <webElementGuid>5b443b03-8578-446f-b878-55dddf442fac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeydown</name>
      <type>Main</type>
      <value>extractNumber(this,0,false);</value>
      <webElementGuid>01c615a8-183e-4921-a303-903e88698dd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeypress</name>
      <type>Main</type>
      <value>return blockNonNumbers(this, event, false, false);</value>
      <webElementGuid>0548ceb8-91cb-4586-8432-1931fe2f6f33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>plotVisit.TimeInHr</value>
      <webElementGuid>e675d57c-6f73-4bad-b3a9-4f3852752a71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control fontsize-13-weight-600 ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>dafc2b90-24c2-46a9-987c-0d10c1e1695e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>00</value>
      <webElementGuid>a98a7db8-23d7-486f-86c2-fbc47321fd96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow plotFrequencyPanel&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-9 col-lg-9&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-6 col-lg-6&quot;]/div[@id=&quot;TimeIn&quot;]/input[@class=&quot;form-control fontsize-13-weight-600 ng-pristine ng-untouched ng-valid ng-valid-maxlength&quot;]</value>
      <webElementGuid>53c0e525-0e25-465b-af7c-f196a154d314</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[113]</value>
      <webElementGuid>bca11630-251e-4b6d-86fb-26388e42d862</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//div[@id='TimeIn']/input)[3]</value>
      <webElementGuid>3b07991b-97d3-40cb-9be9-e6a1bf61d7a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[6]/div[2]/div/div/div/input</value>
      <webElementGuid>c44d0812-7829-44de-aefc-93b545252236</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = '00']</value>
      <webElementGuid>bd939960-5599-4eb7-81a7-1eeded263462</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
