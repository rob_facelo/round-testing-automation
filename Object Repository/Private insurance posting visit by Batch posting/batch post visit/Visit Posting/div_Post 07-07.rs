<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Post 07-07</name>
   <tag></tag>
   <elementGuidId>96157a2d-6807-4801-80ec-86cef7646569</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'dxb' and (text() = '
						
							
						Post
					' or . = '
						
							
						Post
					')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>89a77dcd-afc1-4bd7-a58d-47bb4910a31f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxb</value>
      <webElementGuid>cd7367b9-c074-4164-9ebb-8a704f9ea790</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD</value>
      <webElementGuid>4ad1cdcb-2c22-4af0-8388-9a5122a6f935</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						
							
						Post
					</value>
      <webElementGuid>dc3e8bb0-23f6-4f57-97fe-d265c872625c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD&quot;)</value>
      <webElementGuid>8c4eb693-1496-433f-a02f-aa418a8349af</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD']</value>
      <webElementGuid>86f49c28-8bed-4e82-b8c0-fc8c1550db02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1']/div</value>
      <webElementGuid>f9157cfb-5280-4a89-b626-e98d05871887</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Started'])[2]/following::div[4]</value>
      <webElementGuid>390f4db1-6afe-4f48-91b3-8ab9931014a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The number must be in the range 0...999'])[6]/following::div[4]</value>
      <webElementGuid>3a5f99e6-74ed-4513-981a-b4fd501cf1f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[2]/preceding::div[3]</value>
      <webElementGuid>7733db88-1c70-4e0e-a949-51fa534ed86d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[9]/div/div/div/div</value>
      <webElementGuid>ed6150e4-40a7-4b96-a280-bf01f9d0c97f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ContentPlaceHolder_BatchPostingControl_grvBatchPost_cell1_8_btnPosted_1_CD' and (text() = '
						
							
						Post
					' or . = '
						
							
						Post
					')]</value>
      <webElementGuid>45398277-4fe3-4c38-9afc-fccb76301b5b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
