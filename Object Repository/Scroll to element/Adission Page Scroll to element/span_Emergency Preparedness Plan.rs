<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Emergency Preparedness Plan</name>
   <tag></tag>
   <elementGuidId>2c4d9a8d-7db3-4f1e-a286-c42ba52d6c97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.panel-heading.panelTitle.hhc-blue.hhc-blue-border > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='section-eppcoordinator']/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>165de01e-97fd-4c50-9c52-30e0cc6f603d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Emergency Preparedness Plan
                            </value>
      <webElementGuid>ba299a43-af5b-4ae8-9171-8f43a91489f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;section-eppcoordinator&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;form-group panel panel-primary hhc-blue-border form-group-marginZero margin-bottom-10px&quot;]/div[@class=&quot;panel-heading panelTitle hhc-blue hhc-blue-border&quot;]/span[1]</value>
      <webElementGuid>23c51a0a-bc1d-4ca9-a7f0-fa04df8eff07</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='section-eppcoordinator']/div/div/div/span</value>
      <webElementGuid>09d85ea8-150d-4ebf-b676-416dc548e07d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[4]/following::span[1]</value>
      <webElementGuid>1e0ea9e2-e8d1-4696-b442-161c0ff6679e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='walker, sarah - RN'])[3]/following::span[1]</value>
      <webElementGuid>f4fffcb2-278a-4ad5-95b1-dc9c829ca401</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy EPP from other admission'])[1]/preceding::span[1]</value>
      <webElementGuid>c03c149c-97ce-4c8c-a031-72a06b3327f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Acuity:'])[1]/preceding::span[1]</value>
      <webElementGuid>a9e705e4-b870-4cd2-84e9-eb44b70bd23f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div/span</value>
      <webElementGuid>5f6a5f7e-3989-4dbb-8576-e26fb914fa25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Emergency Preparedness Plan
                            ' or . = 'Emergency Preparedness Plan
                            ')]</value>
      <webElementGuid>b6997755-7e78-4a1f-879f-20578f9b3393</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
