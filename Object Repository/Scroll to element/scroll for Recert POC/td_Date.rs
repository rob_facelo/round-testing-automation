<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Date</name>
   <tag></tag>
   <elementGuidId>e98ffa99-1d8c-422e-83e5-90bed219b1e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OasisPlan']/div/div/table/thead/tr/td</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.tdFlatPadding13.fontcolorDark13Bold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>ab5a6dcc-ae0a-401d-8a54-b5af15de1f8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tdFlatPadding13 fontcolorDark13Bold</value>
      <webElementGuid>2e0ac959-80bb-4afa-92bb-8d0e8f962495</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Date</value>
      <webElementGuid>4e2bf803-5e02-48dc-b177-e69f964e799c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/thead[1]/tr[1]/td[@class=&quot;tdFlatPadding13 fontcolorDark13Bold&quot;]</value>
      <webElementGuid>04d6bf5e-6817-4837-a0c7-3b04b25f1811</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OasisPlan']/div/div/table/thead/tr/td</value>
      <webElementGuid>a3ff7095-6b86-4543-bca9-f8a037150993</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OASIS / Plan Of Care / Change Diagnosis PDGM Tool'])[1]/following::td[1]</value>
      <webElementGuid>802f4604-8f81-49ac-91c5-ab84d353fea7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DC Date:'])[1]/following::td[1]</value>
      <webElementGuid>dc3ba67f-05a8-4f2a-ad46-efb8f885ca7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reason for Assessment'])[2]/preceding::td[1]</value>
      <webElementGuid>afff05f1-6e1c-44a4-b3f5-f299b58866b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cor #'])[1]/preceding::td[2]</value>
      <webElementGuid>5f995694-1a74-4cdf-8ea8-05368c134129</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Date']/parent::*</value>
      <webElementGuid>37c1abf5-2a27-4a5c-affd-85f0c9626437</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/table/thead/tr/td</value>
      <webElementGuid>e60379ec-fcce-4eff-8855-fdd0f8fedaa8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'Date' or . = 'Date')]</value>
      <webElementGuid>ff792ba8-49c2-4091-bbd0-6a785970bd60</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
