<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Staff Information</name>
   <tag></tag>
   <elementGuidId>2ee54f2a-acb9-44c0-9f77-0c04045eda94</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row.margin-top-10px.text-sm-modified > div.col-md-12.col-lg-12 > div.panel.panel-warning.panelPrim.hhc-blue-border.margin-bottom-10px > div.panel-heading.hhc-blue</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>af01434d-be0f-412e-9818-e16b17f0de2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>panel-heading hhc-blue</value>
      <webElementGuid>6ad6484b-7d94-4366-9286-3125f1c5433b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Staff Information
                                            </value>
      <webElementGuid>310fd109-ad3a-43fd-b07e-91f40b5e93ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-pristine ng-valid-mask ng-invalid ng-invalid-validation ng-valid-maxlength ng-valid-pattern ng-valid-minlength&quot;]/div[@class=&quot;row margin-top-10px text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-heading hhc-blue&quot;]</value>
      <webElementGuid>66dc9548-7e80-47f1-94ca-24c4478fea62</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[3]/div/div/div</value>
      <webElementGuid>2fb052d4-3fb2-417a-afcb-1c1211cdcb00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SelectedCarestaff1: Field is required.'])[1]/following::div[4]</value>
      <webElementGuid>295becba-a8f8-40d1-8987-89b52ee29608</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Validation Errors!'])[1]/following::div[4]</value>
      <webElementGuid>608cec66-c02d-4b74-ac89-b031335fa1aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physician for this Order*:'])[1]/preceding::div[1]</value>
      <webElementGuid>8bbf079f-c19c-4a6c-826f-2335d3d5fefa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[2]/preceding::div[1]</value>
      <webElementGuid>b70c8df7-7b69-40a1-835c-a2a7a8a76406</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Staff Information']/parent::*</value>
      <webElementGuid>fb24a558-0fe5-4b23-8ecb-e801ad55356a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ng-form/div[3]/div/div/div</value>
      <webElementGuid>c197914f-56f5-40e0-ab47-427b73dc3046</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                                Staff Information
                                            ' or . = '
                                                Staff Information
                                            ')]</value>
      <webElementGuid>b5ec9c56-b76d-4989-ad58-b6218042d820</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
