<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Notice of Admission Status</name>
   <tag></tag>
   <elementGuidId>a9a6c1ab-5fbf-4827-be03-8983ba23feaf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div/div[2]/div[3]/div/div/div/div[2]/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.panel.panel-primary.panelPrim > div.panel-heading.panelHeader.active</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a6128a6d-5bca-426b-b446-1b62d1722d89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>panel-heading panelHeader active</value>
      <webElementGuid>92f777fa-3c5b-4745-9086-74d2a814bc61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>2ba728a8-22b7-42ac-9279-abfabba8a4b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#noaPanel</value>
      <webElementGuid>cb32d8f0-20d0-48d4-87f9-914b03190a6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Notice of Admission Status
                    </value>
      <webElementGuid>b72d9c34-05b4-420b-9881-93771280cad1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;margin-bottom-10px&quot;]/div[@class=&quot;MainPanel displayNone&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-5&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-primary panelPrim&quot;]/div[@class=&quot;panel-heading panelHeader active&quot;]</value>
      <webElementGuid>12a98747-8eef-4bd4-a38b-31cc48098743</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div/div[2]/div[3]/div/div/div/div[2]/div/div/div</value>
      <webElementGuid>f75db048-4f72-41ba-bea6-7d5c57d43a35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDGM Alerts'])[1]/following::div[27]</value>
      <webElementGuid>cd706caf-c10d-45de-a4f6-c75f50b2556d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alerts'])[1]/following::div[33]</value>
      <webElementGuid>9ad882ab-ee31-471d-a86a-1d894b2bd4d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='N/A'])[1]/preceding::div[1]</value>
      <webElementGuid>0031fe7f-0df9-4627-bacd-cc303d19210d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Notice of Admission Status']/parent::*</value>
      <webElementGuid>f949764e-b938-449c-8a63-34dd7907a933</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div/div[2]/div/div/div</value>
      <webElementGuid>4dcaf81d-a4c0-45a9-9e86-69183287a0d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Notice of Admission Status
                    ' or . = '
                        Notice of Admission Status
                    ')]</value>
      <webElementGuid>8e273df3-ea4a-4dfb-8a77-506d324911fc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
