<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Click here to view patient task</name>
   <tag></tag>
   <elementGuidId>ce2d2e8f-0dde-4719-825a-6101435965fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PatientTaskFlow']/div/div/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1618ba8b-437d-4169-90b8-046be93213f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Click here to view patient task</value>
      <webElementGuid>14cd80d9-9da8-4ca5-8a23-a1db9ffa6570</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PatientTaskFlow&quot;)/div[@class=&quot;panel panel-primary panelPrim&quot;]/div[@class=&quot;panel-heading panelHeader text-sm active collapsed&quot;]/span[2]</value>
      <webElementGuid>a595a383-5340-43f0-b1c5-b1bb673c1de6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PatientTaskFlow']/div/div/span[2]</value>
      <webElementGuid>388981e5-5765-4947-a8ea-a50d65219495</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[1]/following::span[2]</value>
      <webElementGuid>1dab4511-8e88-4657-8d28-332b474edff3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient Demographics'])[1]/following::span[3]</value>
      <webElementGuid>6c31f383-c134-4357-aede-d75c25b221c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SOC Oasis'])[1]/preceding::span[1]</value>
      <webElementGuid>d3800d84-4e04-4d3e-9b75-fdc6d6916aec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Input Start of Care Oasis'])[1]/preceding::span[1]</value>
      <webElementGuid>659ea709-ebd0-4aba-9914-a0a7dfa407b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Click here to view patient task']/parent::*</value>
      <webElementGuid>3080b6ff-7070-4529-b3a0-b9e3b9651516</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span[2]</value>
      <webElementGuid>18f712b6-3417-42f3-a6fb-73524adaec2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Click here to view patient task' or . = 'Click here to view patient task')]</value>
      <webElementGuid>557bd7c4-9973-4e33-a9b6-4e6035d3fe38</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
