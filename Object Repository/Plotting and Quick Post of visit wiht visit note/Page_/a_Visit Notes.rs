<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Visit Notes</name>
   <tag></tag>
   <elementGuidId>70992779-b888-4c26-81a7-f622f893bd6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > a.list-group-item.list-group-item-action.cursorPointer.font13.ActionItemPopOverLast</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover627459']/div[2]/a[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = 'ShowVisitNewNoteModal()' and (text() = '
                 Visit Notes' or . = '
                 Visit Notes')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ca8f5cdd-a76f-4cc6-bc9e-8a0bfd9fb269</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast</value>
      <webElementGuid>a2d8ec80-8b5b-4b12-a20c-646652cff433</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>ShowVisitNewNoteModal()</value>
      <webElementGuid>26c88345-75ba-4f22-8716-2bdb517f3365</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                 Visit Notes</value>
      <webElementGuid>0147d3ed-b57c-422a-8d47-d6c76b585427</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover627459&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast&quot;]</value>
      <webElementGuid>4c0a2bd3-bcb8-4d3b-8f4e-c13822734d96</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover627459']/div[2]/a[3]</value>
      <webElementGuid>98c272f3-5ff0-4716-b3e3-f1349949b666</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[3]</value>
      <webElementGuid>6c3c604a-f97a-4e6b-83eb-98d4c3ebca05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                 Visit Notes' or . = '
                 Visit Notes')]</value>
      <webElementGuid>b3642a17-64ba-4455-895c-9894279a00cb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
