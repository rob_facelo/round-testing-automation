<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Select Discipline--               _075cf9</name>
   <tag></tag>
   <elementGuidId>6482594f-724a-44e4-9bf9-bd7fa5df5816</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ddlCaregiverType</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ddlCaregiverType']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>6fec29b5-584a-4154-9740-031287fe0b5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm fontsize-13-weight-600 ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>9f2ec5df-6ef5-4327-9752-2d808ed2da3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ddlCaregiverType</value>
      <webElementGuid>7ae2a26f-2f68-4477-8a7d-813a06002d4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>plotVisit.OnCgTypeChange()</value>
      <webElementGuid>1bbf97b3-1366-4bf1-8b56-03d0fe091639</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>plotVisit.SelectedCgType</value>
      <webElementGuid>cb685500-38ab-4cd2-9455-5e6ab79d93cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                </value>
      <webElementGuid>673b4266-3dcb-4b4a-b0ba-6afe90a03728</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ddlCaregiverType&quot;)</value>
      <webElementGuid>12ed5d5d-2d39-42d8-88eb-f05a3302973b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ddlCaregiverType']</value>
      <webElementGuid>312d04c7-f3ca-4d7b-ba8f-0ebbab12790c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>45aaf494-043a-44ff-a308-b05dc4c495c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff Type'])[1]/following::select[1]</value>
      <webElementGuid>3ad54ce2-5c2b-4a9f-a59c-56dfdb8d90b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payer'])[1]/following::select[2]</value>
      <webElementGuid>ff9a0707-fa06-488d-a349-c3cc38b6aafb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CareStaff'])[2]/preceding::select[1]</value>
      <webElementGuid>8ab89450-56d2-45f9-9a0a-a6e703b0aa06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Type'])[1]/preceding::select[2]</value>
      <webElementGuid>c3d77e89-f2f6-41d2-a2c4-bf593c857552</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div[2]/select</value>
      <webElementGuid>1bd6cd46-97bb-442d-ae2e-136ed68cd237</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ddlCaregiverType' and (text() = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ' or . = '
                                    --Select Discipline--
                                    Skilled Nursing
                                    Case Manager
                                    Home Health Aide
                                    Medical Social Worker
                                    Occupational Therapy
                                    Physical Therapy
                                    Speech Therapy
                                    Respiratory Therapy
                                    Others
                                ')]</value>
      <webElementGuid>7154f020-f300-4e1b-8999-f0a71f8e3b08</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
