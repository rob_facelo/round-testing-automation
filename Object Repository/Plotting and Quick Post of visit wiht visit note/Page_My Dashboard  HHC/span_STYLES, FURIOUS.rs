<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_STYLES, FURIOUS</name>
   <tag></tag>
   <elementGuidId>81216799-3431-4ef1-a2d6-44269b1411bb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.list-group-item-heading.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='widgetholder']/section/div[4]/div/div/div[2]/ul/li/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ce269ff4-0cac-447c-9a95-244c4d372897</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item-heading ng-binding</value>
      <webElementGuid>0c9ac1ed-9d31-4915-b5bd-ff5fb3bbaef3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>STYLES, FURIOUS </value>
      <webElementGuid>b29defbf-5760-408a-b698-e6c6f6d8257f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;widgetholder&quot;)/section[@class=&quot;ng-scope&quot;]/div[@class=&quot;clearfix&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-primary&quot;]/div[@class=&quot;panel-body recentPatientActivity&quot;]/ul[@class=&quot;list-group&quot;]/li[@class=&quot;list-group-item ng-scope&quot;]/a[1]/span[@class=&quot;list-group-item-heading ng-binding&quot;]</value>
      <webElementGuid>ed500509-f123-4f4b-8ad4-9f73b954e579</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='widgetholder']/section/div[4]/div/div/div[2]/ul/li/a/span</value>
      <webElementGuid>5f3bd4c3-f7ee-4f6d-8704-593e3d9d2247</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::span[1]</value>
      <webElementGuid>42e9f941-b634-4627-95b6-c5841ed5eca4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/following::span[3]</value>
      <webElementGuid>a5028d6b-805a-4664-9508-7160ebf95435</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient Alerts'])[1]/preceding::span[2]</value>
      <webElementGuid>a88cbb22-ce53-4bc9-be33-f6d856db8654</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[2]/preceding::span[2]</value>
      <webElementGuid>68063110-7cdc-4e23-88b5-d50685faa4c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='STYLES, FURIOUS']/parent::*</value>
      <webElementGuid>65c7792e-a52c-4d71-86f8-2bb441e4ccc5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/ul/li/a/span</value>
      <webElementGuid>9780869d-9159-4205-8177-3e45d4987e14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'STYLES, FURIOUS ' or . = 'STYLES, FURIOUS ')]</value>
      <webElementGuid>07ee3ecd-7042-433e-8a94-3b5971a1f0b9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
