<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Quick Post jul 15</name>
   <tag></tag>
   <elementGuidId>b2bbb7d3-b5dd-4699-bac6-32bfe6b1d9f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > a.list-group-item.list-group-item-action.cursorPointer.font13.ActionItemPopOverFirst</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover136551']/div[2]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst' and @ng-click = 'cl.QuickPostVisit()' and (text() = '
                  Quick Post' or . = '
                  Quick Post')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6e2e3015-ff78-4978-a07a-239bfe9028dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst</value>
      <webElementGuid>c0b95691-f93b-491d-a80c-970e978c8dbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>cl.QuickPostVisit()</value>
      <webElementGuid>c8a1faf7-d7c6-4752-bee7-b2c82a5aea74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Quick Post</value>
      <webElementGuid>7ef69983-0f5a-4ca1-9433-ac71feaf03af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover136551&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst&quot;]</value>
      <webElementGuid>5e159695-e5a0-4ab0-9f67-8126d7e9e6a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover136551']/div[2]/a</value>
      <webElementGuid>0f82474a-4fbe-4bc9-8d32-d4f65456cc9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::a[1]</value>
      <webElementGuid>f070f753-504b-4086-ab9c-1fd2e6741363</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::a[1]</value>
      <webElementGuid>caef27b6-d3e2-4931-959d-c781e89afcb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/a</value>
      <webElementGuid>9e631f3f-1415-4083-af3b-5d647230454f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  Quick Post' or . = '
                  Quick Post')]</value>
      <webElementGuid>32f67e36-57d8-4992-bccc-18b3b805bc15</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
