<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Quick Post Aug 2</name>
   <tag></tag>
   <elementGuidId>a72e0ab1-89a5-44b7-907f-f0ffdc01c080</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > a.list-group-item.list-group-item-action.cursorPointer.font13.ActionItemPopOverFirst</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover286677']/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a0621404-851a-42be-8ee0-038851b4b111</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst</value>
      <webElementGuid>b6e89b8f-a130-4c6a-95bd-cee33e429c4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>cl.QuickPostVisit()</value>
      <webElementGuid>99dcf70e-f134-47d4-b42c-4cf2e1875442</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Quick Post</value>
      <webElementGuid>4adce8b5-36d6-4fed-8d9d-c99e0263c738</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover286677&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst&quot;]</value>
      <webElementGuid>21e05f99-5db9-4dc4-b582-2f65c4dc18c2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover286677']/div[2]/a</value>
      <webElementGuid>430c4920-7cc4-4a0c-abaf-3b35893e9770</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::a[1]</value>
      <webElementGuid>8f0d1541-b095-4fee-9881-5d19c720b084</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::a[1]</value>
      <webElementGuid>2b03fcbb-7ab9-4dae-b5fa-998ddfba7ea5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/a</value>
      <webElementGuid>9e122430-63f0-428a-8f8a-1935bb13c800</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  Quick Post' or . = '
                  Quick Post')]</value>
      <webElementGuid>f568a3a4-f7df-4bb8-9ee7-60eb2e08371d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
