<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Quick Post jul 10</name>
   <tag></tag>
   <elementGuidId>f36034ab-0799-44fb-98da-3addb08df7ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > a.list-group-item.list-group-item-action.cursorPointer.font13.ActionItemPopOverFirst</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = 'cl.QuickPostVisit()' and (text() = '
                  Quick Post' or . = '
                  Quick Post')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover524737']/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4bc4fa58-3ed2-47c7-b6e0-f9fbaabe9f0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst</value>
      <webElementGuid>caf093e9-4026-44c6-8bfc-cb6b84178ed8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>cl.QuickPostVisit()</value>
      <webElementGuid>54d8b340-25b2-4907-886f-0aca8c94065b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Quick Post</value>
      <webElementGuid>2c227a14-4f7c-43b8-9ab1-4bf42b02f2af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover524737&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverFirst&quot;]</value>
      <webElementGuid>92a4c2e7-bbaf-4337-91d9-a59240e5e1b6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover524737']/div[2]/a</value>
      <webElementGuid>ad1fc459-f69e-4800-bac9-bf71b235cdc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::a[1]</value>
      <webElementGuid>73deacda-8707-46aa-ae72-64386a3d3265</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::a[1]</value>
      <webElementGuid>a46e7918-c97c-4c83-8764-82f8243a7a58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/a</value>
      <webElementGuid>bb8acb51-28ea-411d-933b-682e51f4743b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                  Quick Post' or . = '
                  Quick Post')]</value>
      <webElementGuid>672c0738-fa75-46fb-96c2-a96904231875</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
