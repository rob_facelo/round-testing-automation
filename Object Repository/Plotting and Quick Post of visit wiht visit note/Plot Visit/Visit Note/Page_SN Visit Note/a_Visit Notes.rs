<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Visit Notes</name>
   <tag></tag>
   <elementGuidId>64bb1d69-22f1-4420-909e-aa33230017e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover862029']/div[2]/a[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > a.list-group-item.list-group-item-action.cursorPointer.font13.ActionItemPopOverLast</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;popover862029&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast&quot;][count(. | //*[@class = 'list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast' and @ng-click = 'ShowVisitNewNoteModal()' and (text() = '
                 Visit Notes' or . = '
                 Visit Notes')]) = count(//*[@class = 'list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast' and @ng-click = 'ShowVisitNewNoteModal()' and (text() = '
                 Visit Notes' or . = '
                 Visit Notes')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1943a2ac-f4e6-4ed2-bc8f-9ab74a7fd945</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast</value>
      <webElementGuid>151bc992-92c9-4f45-b4ca-3ef5dfb67b3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>ShowVisitNewNoteModal()</value>
      <webElementGuid>5325c14f-578a-49d9-bc28-30f7414891f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                 Visit Notes</value>
      <webElementGuid>33f32eee-fdee-429b-9616-748840891332</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover862029&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast&quot;]</value>
      <webElementGuid>fb8c2145-e155-489b-94cb-a2903919d6cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover862029']/div[2]/a[3]</value>
      <webElementGuid>018df789-48a5-4e2b-973f-ede697853dd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[3]</value>
      <webElementGuid>fa3fc42f-eb8a-4075-9514-54f48ec6909d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                 Visit Notes' or . = '
                 Visit Notes')]</value>
      <webElementGuid>f80b5d0a-d170-486d-868e-5c022c28387b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
