<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Visit Notes</name>
   <tag></tag>
   <elementGuidId>4cebac8c-10ac-4bf7-816f-e7e7b4a84027</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover932605']/div[2]/a[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > a.list-group-item.list-group-item-action.cursorPointer.font13.ActionItemPopOverLast</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-click = 'ShowVisitNewNoteModal()' and (text() = '
                 Visit Notes' or . = '
                 Visit Notes')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bb9ec5fc-e69d-4b64-afdf-e71834634b9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast</value>
      <webElementGuid>9ec966d1-2a3d-4411-aaa9-04d22b78a3a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>ShowVisitNewNoteModal()</value>
      <webElementGuid>6175647e-85fc-447b-ba78-a51a3920655e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                 Visit Notes</value>
      <webElementGuid>73d7581d-222f-4a52-ac69-703b50c8389e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover932605&quot;)/div[@class=&quot;popover-content&quot;]/a[@class=&quot;list-group-item list-group-item-action cursorPointer font13 ActionItemPopOverLast&quot;]</value>
      <webElementGuid>ac78e02a-edce-4e96-ba19-6fc19c142a4a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover932605']/div[2]/a[3]</value>
      <webElementGuid>6fee5af8-9900-41d2-bb94-6580cb9481c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[3]</value>
      <webElementGuid>8e33249d-643c-4470-bb19-dee575881dc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                 Visit Notes' or . = '
                 Visit Notes')]</value>
      <webElementGuid>9a089b7d-baed-4451-98c2-24fcdf6829b5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
