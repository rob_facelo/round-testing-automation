<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save visit note</name>
   <tag></tag>
   <elementGuidId>c4225da7-54ae-4a45-a19e-9717ba8725c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='VisitNoteContainer']/div/div[2]/div/div/div/button/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > button.btn.btn-default.font12.ng-scope > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>13b75e17-119e-4f2b-9435-883a157a0e8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Save</value>
      <webElementGuid>2e21a488-6f2c-4658-97b5-4c08c7effdbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;VisitNoteContainer&quot;)/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body visit-modal-body&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;row modal-header&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/button[@class=&quot;btn btn-default font12 ng-scope&quot;]/span[1]</value>
      <webElementGuid>a0faf6a5-ea5f-44bb-ae01-ef563bb0c03c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='VisitNoteContainer']/div/div[2]/div/div/div/button/span</value>
      <webElementGuid>e09d0dde-a323-4d63-9dfd-d46c3e9fd247</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comments:'])[1]/following::span[3]</value>
      <webElementGuid>b700df22-c019-478c-93f4-490ccce12d62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='QA Comments'])[2]/preceding::span[1]</value>
      <webElementGuid>d8bffd81-0ba0-4970-981e-35d82a034de9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More Options'])[1]/preceding::span[2]</value>
      <webElementGuid>0d46217c-1cbf-4c6c-b474-6ca36ce3a1e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]/div/div/div/button/span</value>
      <webElementGuid>7e824831-84c4-479c-aaf3-cf6319c1584b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Save' or . = ' Save')]</value>
      <webElementGuid>d0e6f702-c786-4e8c-898e-6069b0dd0580</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
