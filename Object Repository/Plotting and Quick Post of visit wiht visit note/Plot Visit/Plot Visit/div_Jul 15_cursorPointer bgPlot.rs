<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Jul 15_cursorPointer bgPlot</name>
   <tag></tag>
   <elementGuidId>7579b123-71b5-4e10-936b-a878b41d7f29</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.cursorPointer.bgPlot</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[5]/div[6]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>687efdf5-e0ca-4d4a-8640-11f76be68cb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>plotVisit.DayClick('btnDate', 0, 0, '7-15-2022')</value>
      <webElementGuid>9777d4a1-450a-47e4-8473-f0c176285917</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursorPointer bgPlot</value>
      <webElementGuid>9b4ff2fc-03e5-4b0f-a579-3e953e100f65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlotVisitModal&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;boxShadow&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;calendarWidth font11 ng-scope&quot;]/div[@class=&quot;cursorPointer bgPlot&quot;]</value>
      <webElementGuid>b9391398-dab4-4951-ad8c-ce235feb873a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlotVisitModal']/div/div/div[2]/div/div[5]/div[6]/div[2]</value>
      <webElementGuid>d9e650e6-aeed-46e4-bf80-6d31ef5fb25c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[6]/div[2]</value>
      <webElementGuid>be533456-73c0-4152-9b74-c598d5f98e93</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
