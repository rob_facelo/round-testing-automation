<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__order date</name>
   <tag></tag>
   <elementGuidId>bf8f23d3-db70-4c44-a83d-d086646d2f9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='intakeOrderDate']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#intakeOrderDate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2eea96bb-c6f1-4648-9463-71a658d9e0e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>intakeOrderDate</value>
      <webElementGuid>ce448472-b54c-448d-8981-b4cf4f58d638</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>intakewizardorderdate</value>
      <webElementGuid>271de834-1d83-41e5-8bcb-b61c3219c9c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>668bb8b6-991f-400a-a851-1d56a1127523</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>model-view-value</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>0498b73a-989a-48e9-8e6b-73cb92d8ddee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>showtoday</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>71a5232f-d24d-47b9-ae49-6fc384daf3f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>1ca6efeb-fafc-4e40-914c-b7690949ef77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-pattern</name>
      <type>Main</type>
      <value>/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/</value>
      <webElementGuid>01d78629-b2e1-426b-a46b-e7b72d1c938f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>cdf33c83-50f8-436f-8b13-f65a40a29b9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control font14 ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-valid-maxlength ng-touched</value>
      <webElementGuid>3f44b270-851d-43bc-bf00-c9c00ef6572f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>piCtrl.ptoInput.CallDate</value>
      <webElementGuid>79c30ffd-bfcc-4e32-bee4-8f666406c6e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>date</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>09b705a7-07ae-4f9c-9013-5e1727e3eb65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this, this.value, event, false, '1')</value>
      <webElementGuid>ccd56c20-1a78-4962-ad09-2793b5b4f420</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>47f9eb37-8b87-43cb-8104-a2431756b9ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeOrderDate&quot;)</value>
      <webElementGuid>7cf7aba6-88aa-4c40-b00e-25315be2f7d9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='intakeOrderDate']</value>
      <webElementGuid>c9221867-cea8-49b8-b09c-f9cbcee061f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='intakewinfo']/div[3]/div/div/div[3]/div[2]/div/div/div[3]/div/div/input</value>
      <webElementGuid>4c7df455-da33-4a82-8ea4-4c48803f3636</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/div/div[3]/div/div/input</value>
      <webElementGuid>92f54894-1b37-4ea2-862d-2a1f665de186</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'intakeOrderDate' and @name = 'intakewizardorderdate' and @type = 'text' and @placeholder = 'MM/dd/yyyy']</value>
      <webElementGuid>b2b20768-4cd5-44d0-94d3-eae80beb3318</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
