<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_ctl00ContentPlaceHolderLogon_5de2a7</name>
   <tag></tag>
   <elementGuidId>9e01c763-57e6-450e-9548-a1ec02c93482</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtPassword']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolderLogonPage_LoginControl_txtPassword</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>81333197-0efd-46aa-a70a-6520068dec20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolderLogonPage$LoginControl$txtPassword</value>
      <webElementGuid>0d29df97-2e2e-4a1e-95e1-7439ac23a002</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>91334c71-7c4d-4086-94fe-1200db8fce26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolderLogonPage_LoginControl_txtPassword</value>
      <webElementGuid>f3128172-a28a-440f-9fbf-da98c6ab6b89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>d2e65e14-fbeb-4eb6-8b28-4cbb700e659e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>tooltip</value>
      <webElementGuid>d3238d7b-fe1d-4d63-b109-4450023efa7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-trigger</name>
      <type>Main</type>
      <value>manual</value>
      <webElementGuid>ce576325-ec6e-426b-94e2-4a81fd12af06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-title</name>
      <type>Main</type>
      <value>Caps Lock is on</value>
      <webElementGuid>be2afaab-4bc1-4a72-a3ec-56bd2ffc4c25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolderLogonPage_LoginControl_txtPassword&quot;)</value>
      <webElementGuid>6e098d3a-0ce9-4eec-9d6f-62a2987b9bfc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtPassword']</value>
      <webElementGuid>e1fd18a3-4431-4b15-979a-7c670fa3986f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]/fieldset[3]/input</value>
      <webElementGuid>320e590e-6d22-4d7b-85a5-0c2b6b21cbb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[3]/input</value>
      <webElementGuid>c30c62b4-7487-4a1c-bc65-b65f0cba68c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$ContentPlaceHolderLogonPage$LoginControl$txtPassword' and @type = 'password' and @id = 'ContentPlaceHolderLogonPage_LoginControl_txtPassword']</value>
      <webElementGuid>1563f16a-4743-4030-b6aa-fad775440a5d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
