<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_User ID_ctl00ContentPlaceHolderLogonP_952ea2</name>
   <tag></tag>
   <elementGuidId>149f3f70-817c-4065-a0c4-dd90b17d1181</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtUserId']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolderLogonPage_LoginControl_txtUserId</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e1dfb1e7-7cee-4db8-bc9c-08c7d66a86e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolderLogonPage$LoginControl$txtUserId</value>
      <webElementGuid>dfb8e820-1773-47ac-b853-f687414369ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>705b5fd8-8c48-4f8d-b6b1-b2e697414a4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolderLogonPage_LoginControl_txtUserId</value>
      <webElementGuid>530ef3c3-2464-4d04-84af-f5dd4caee983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>b81c151e-f83c-44ac-9485-66d1372da58c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolderLogonPage_LoginControl_txtUserId&quot;)</value>
      <webElementGuid>505e3850-71a9-4d83-bd48-f34c29f4383d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolderLogonPage_LoginControl_txtUserId']</value>
      <webElementGuid>6355c211-06b7-4bc6-b367-dc6b90819d11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmLogin']/div[3]/fieldset[2]/input</value>
      <webElementGuid>7768c8bf-5247-44d6-9d37-603d94c6ddce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]/input</value>
      <webElementGuid>cc9ff0a9-fbfa-4c49-a1d3-401d0f2df3c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$ContentPlaceHolderLogonPage$LoginControl$txtUserId' and @type = 'text' and @id = 'ContentPlaceHolderLogonPage_LoginControl_txtUserId']</value>
      <webElementGuid>eca48993-b72e-40b4-99d3-18e8faee8689</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
