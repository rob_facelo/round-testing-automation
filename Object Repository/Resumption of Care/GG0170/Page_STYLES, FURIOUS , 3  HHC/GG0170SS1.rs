<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0170SS1</name>
   <tag></tag>
   <elementGuidId>5effd209-19cb-4e85-af99-06eb6d049303</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#GG0170SS1 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170SS1']/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>954674b8-6f7f-414f-a4e4-56c8c8e8dba0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>8b2af17f-8790-44bc-b210-25c37d1c83a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170SS1</value>
      <webElementGuid>6d9d6ab7-6902-4cb4-957a-c9774a50b63e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>134cd50e-ebc4-4b87-bbfa-7b89d405c5d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170SS1</value>
      <webElementGuid>07620e8a-8290-4615-b8f5-1cd599aef4ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0170SS1', oasis.oasisDetails.GG0170SS1)</value>
      <webElementGuid>0e4b2488-0266-42ec-8291-f7f0ac14a009</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170Q1 == '0' || oasis.isLocked</value>
      <webElementGuid>59250497-b812-421e-81ac-6befd9dfa4ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    </value>
      <webElementGuid>b3dce997-6514-4d7a-a551-c23dec87d7f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170SS1&quot;)/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>2e0ba1e5-d654-447b-8086-cf215e3ce2fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170SS1']/div/select</value>
      <webElementGuid>90029952-c741-4dc3-be64-02b32bce292f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='S. Wheel 150 feet:'])[1]/following::select[1]</value>
      <webElementGuid>bb645d39-9fdf-49d4-afe9-1ec6e2575d03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RR1. Indicate the type of wheelchair or scooter used.'])[1]/following::select[3]</value>
      <webElementGuid>7ac095fe-9a15-4b8e-83d5-7bacc3cc4732</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SS1. Indicate the type of wheelchair or scooter used.'])[1]/preceding::select[1]</value>
      <webElementGuid>c57cbc21-5e4d-433f-8909-7fa6491559a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[31]/preceding::select[1]</value>
      <webElementGuid>f12c1ab3-6b59-4abd-9de8-6515ab894c75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[21]/div/div[2]/div/div/div/select</value>
      <webElementGuid>fde34bde-e0de-406b-8663-8f9355052f5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    ' or . = '
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    ')]</value>
      <webElementGuid>a561307d-ae8c-4117-8584-97b9d8a07920</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
