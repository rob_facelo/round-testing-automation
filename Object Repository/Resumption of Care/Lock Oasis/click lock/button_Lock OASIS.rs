<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Lock OASIS</name>
   <tag></tag>
   <elementGuidId>e3457e06-44de-42e1-90c4-5ea968881175</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'lockOasis(lockDate)' and (text() = 'Lock OASIS' or . = 'Lock OASIS')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[167]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-dialog.modal-sm > div.modal-content > div.modal-footer > button.btn.btn-success</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6f318d31-00b6-49b5-8f31-14bb2bc4cdd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>683496f9-5f55-4827-9b00-5986cf08b232</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>lockOasis(lockDate)</value>
      <webElementGuid>fde984e9-2207-4094-813e-6d4d384deba3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-success</value>
      <webElementGuid>fba240ab-0d9f-40ca-a475-2059ed68fa01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lock OASIS</value>
      <webElementGuid>7219bf15-4bfe-4d02-b73a-a90c727a67a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer&quot;]/button[@class=&quot;btn btn-success&quot;]</value>
      <webElementGuid>ae362230-65b4-4df4-a787-36e85d0f8f73</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[167]</value>
      <webElementGuid>cc08e2c1-4945-4603-a639-f732f4f843fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[1]/following::button[1]</value>
      <webElementGuid>8734b6c2-1535-47ff-ba37-ee05a8bcf03f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Oasis'])[1]/following::button[1]</value>
      <webElementGuid>816bcf24-5df7-4f58-afa5-a451e17bde2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/preceding::button[1]</value>
      <webElementGuid>3ec1606a-ec32-4762-8c46-e39586466932</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lock OASIS']/parent::*</value>
      <webElementGuid>32c7ddc1-489f-4d50-8f0d-aa6bbeafb7ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div[4]/button</value>
      <webElementGuid>332bae83-10c9-4d04-86c9-c030667814ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Lock OASIS' or . = 'Lock OASIS')]</value>
      <webElementGuid>1aee71a6-f924-42c1-9996-79b1802a3df9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
