<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_29</name>
   <tag></tag>
   <elementGuidId>7ae67145-b27f-4c14-a53d-d8abbfe46bcc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '29' or . = '29')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '29' or . = '29')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>ad014ca2-90c9-440a-989e-b0cfd73d7fa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>active day</value>
      <webElementGuid>94a2127f-ae0e-42de-bd3f-7a8b8b9b7dba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>29</value>
      <webElementGuid>0bb94967-0dfb-4023-9b72-295e4f07b463</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/tr[1]/td[@class=&quot;active day&quot;]</value>
      <webElementGuid>df49b986-2c00-4a44-972b-d45e1336e0c3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '29' or . = '29')]</value>
      <webElementGuid>34dbf2e8-b7e4-45ec-aa61-064fcb5f716b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
