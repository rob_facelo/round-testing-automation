<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>check and uncheck UK</name>
   <tag></tag>
   <elementGuidId>f7668755-d99b-49f6-aee5-4e5ca0239352</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-4.col-md-4.col-lg-4.bottom-space > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > input.ng-valid.ng-dirty.ng-valid-parse.ng-touched</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[59]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>70096744-a44e-4fd1-b2e6-c6716c91f8b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>caafe01a-fa0e-437f-91fd-fa04cb39f2fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1005_INP_DSCHG_UNKNOWN</value>
      <webElementGuid>92590cde-1d5e-4931-859f-9379cb819fcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1005_INP_DSCHG_UNKNOWN</value>
      <webElementGuid>62b80775-d680-41ae-8634-c596ccb00ff6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>'1'</value>
      <webElementGuid>11870fcd-afc0-421f-9277-8a8b340c73ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-false-value</name>
      <type>Main</type>
      <value>'0'</value>
      <webElementGuid>ce980e67-3557-4a9b-aebc-99c77b738a4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1005_INP_DSCHG_UNKNOWN', oasis.oasisDetails.M1005_INP_DSCHG_UNKNOWN)</value>
      <webElementGuid>7b7992d7-be03-49c3-9492-9d66f6f868d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1000_DC_NONE_14_DA == '1' || oasis.isLocked</value>
      <webElementGuid>0de61c69-de25-4fe1-874d-e862abf19af8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-valid ng-dirty ng-valid-parse ng-touched</value>
      <webElementGuid>eb73a7e4-209f-4193-bb57-7b57d894689c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1005&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-4 col-lg-4 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;ng-valid ng-dirty ng-valid-parse ng-touched&quot;]</value>
      <webElementGuid>290ba23c-a0cb-45b1-82c2-3614def46ce1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[59]</value>
      <webElementGuid>b2532f1d-76fb-4dc3-9b05-72c16f0fc273</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1005']/div/div[2]/div[2]/input</value>
      <webElementGuid>82c69ec9-209b-44ee-b003-3bf446647fd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div[2]/div[2]/input</value>
      <webElementGuid>40626d1e-cfb4-49db-aeef-a8a451925704</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>31e72e92-a7b3-4834-9de1-7d75c9071723</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
