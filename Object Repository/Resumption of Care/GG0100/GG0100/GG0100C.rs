<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0100C</name>
   <tag></tag>
   <elementGuidId>4da2de39-c745-4e87-a4c6-e0de6a47f395</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#GG0100C > div.row > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0100C']/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>aaf3a838-b588-4af0-b8e1-fd7859f2f60b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>84665eb8-dde4-400b-b5f2-a1273602d283</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0100C</value>
      <webElementGuid>f134720f-e85c-4384-bea9-075b73bf778d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>78f3f334-4ac9-40d8-a80e-f43584d263ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0100C</value>
      <webElementGuid>311ebc02-5626-4b9a-b084-5d65e627502e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0100C', oasis.oasisDetails.GG0100C)</value>
      <webElementGuid>ae5f8a05-4be1-4519-8131-a5ab72a4581c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>b0072914-2c75-4c0b-b23d-3719bd62acc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            </value>
      <webElementGuid>36beed6c-8064-4eea-b03d-b0fe82e91182</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0100C&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>0b3b848d-5723-4cd0-86db-035a555eb91f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0100C']/div/div/div/select</value>
      <webElementGuid>e92a6de6-2d9c-4b3f-8c58-673fd84ccf94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Indoor Mobility (Ambulation):'])[1]/following::select[1]</value>
      <webElementGuid>71139d15-4f56-4db8-a311-fe7533098b64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. Self Care:'])[1]/following::select[2]</value>
      <webElementGuid>7da3c6d3-a130-4d05-b705-49bff5d58fd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C. Stairs:'])[1]/preceding::select[1]</value>
      <webElementGuid>cf14960c-9223-4a77-bcd7-ccad62b88989</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='D. Functional Cognition:'])[1]/preceding::select[2]</value>
      <webElementGuid>368b71cd-2b95-4ad2-893a-0d8dacf643e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/select</value>
      <webElementGuid>929c80b0-d767-4922-9114-3900ff5731aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ' or . = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ')]</value>
      <webElementGuid>76067a9b-55ba-4e66-99fd-6fea62f28483</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
