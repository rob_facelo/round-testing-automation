<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Inpatient Discharge Date_InpatientDis_f61833</name>
   <tag></tag>
   <elementGuidId>fa6bfe8d-62cb-4f80-b061-7335c0ccbfe3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='InpatientDischargeDate']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#InpatientDischargeDate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fd105c91-a959-4323-8aee-2809fe1da2bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>InpatientDischargeDate</value>
      <webElementGuid>a8033429-4eea-47ee-a6f7-d09fbeb1235a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>73e6a289-20c2-4286-b4e1-c7a7699b46ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-untouched ng-valid-maxlength ng-invalid ng-invalid-validation</value>
      <webElementGuid>0baddcbe-561a-4c96-ae8a-77d25d1260b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>InpatientDischargeDate</value>
      <webElementGuid>d3fa58f8-e2d9-45d2-8e0f-8a5d217bceef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>ptoCtrl.ptoInput.InpatientDischargeDate</value>
      <webElementGuid>fb2b29b3-ba5c-4543-abbc-d020c74ccaa6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>date</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>4b40b0dc-230a-413e-bf86-42959efcc015</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>validation</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>a7ad5f02-d16d-46be-b2e9-4ef7b81fe091</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>validation-error-to</name>
      <type>Main</type>
      <value>InpatientDischargeDateValidation</value>
      <webElementGuid>ae1b987d-fbcd-46fd-828f-e5d056c8f04c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>1d42cf69-87f6-41ee-8bed-9471bd644e09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>showtoday</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e491ac3d-3717-4571-8998-62014e92cc68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>model-view-value</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e05be7a1-7832-4616-ad73-38ee26a1d2ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,false,'1')</value>
      <webElementGuid>d04cad16-1a80-4124-845a-59cfe320e111</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>97768e9a-41dd-4d8d-ba6f-5e876e9f597b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>ce391262-5cf5-40d2-b412-c58f896144ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;InpatientDischargeDate&quot;)</value>
      <webElementGuid>de79e784-4061-4abb-be77-44dd7c9fe562</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='InpatientDischargeDate']</value>
      <webElementGuid>d47b3bd3-7bca-4ca7-9b10-79fdcf9038f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div[7]/div/div/div[2]/fieldset/div/div[2]/div/div/div/div/div/div/input</value>
      <webElementGuid>56518d39-9c69-4bcb-8106-43be09e64598</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div/div[2]/div/div/div/div/div/div/input</value>
      <webElementGuid>8c7f61e9-ce59-442e-8e36-327be041b2f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'InpatientDischargeDate' and @type = 'text' and @id = 'InpatientDischargeDate']</value>
      <webElementGuid>ee368b70-20a3-4412-bc66-383919f9387e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
