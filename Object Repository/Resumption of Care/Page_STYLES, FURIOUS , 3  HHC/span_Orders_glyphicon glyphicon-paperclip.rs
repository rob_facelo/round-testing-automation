<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Orders_glyphicon glyphicon-paperclip</name>
   <tag></tag>
   <elementGuidId>43d34bc2-912b-4fbd-b506-a7d0e04f39b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='orders']/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-paperclip</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>fa95e8f6-8cdc-4ec0-ad96-971940375d37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-paperclip</value>
      <webElementGuid>00db7690-89bd-4384-9b48-f34d89fcbb7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;orders&quot;)/span[@class=&quot;text font12 font-weight-600 ng-binding&quot;]/span[@class=&quot;glyphicon glyphicon-paperclip&quot;]</value>
      <webElementGuid>7ff4875c-4968-4255-a968-d766908c4d58</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='orders']/span/span</value>
      <webElementGuid>e36f7fad-9ab1-4227-95e7-dfa316ae5dde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a/span/span</value>
      <webElementGuid>f5f04cb1-84c3-452b-a896-7e91ed51c500</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
