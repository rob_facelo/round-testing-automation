<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Go_glyphicon glyphicon-search</name>
   <tag></tag>
   <elementGuidId>a35fa3e3-88e2-49cc-99f2-de95163b6c81</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-default.btn-sm.toolbar-btn-default.font12 > span.glyphicon.glyphicon-search</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>96f7cc1a-3b97-4352-9098-9d4f2b07bc7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-search</value>
      <webElementGuid>239307bb-58d9-4e6a-8f81-1614cfc940ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12&quot;]/span[@class=&quot;glyphicon glyphicon-search&quot;]</value>
      <webElementGuid>b53f3fae-a925-4e1e-90f3-e266b0c5d18a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button/span</value>
      <webElementGuid>88d2e54d-f5cc-4221-8725-88be56bf0a0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div[2]/fieldset/div/button/span</value>
      <webElementGuid>9442fd59-e7fa-4424-a3a8-f2ae80b6fb90</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
