<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Chat Support_searchKey</name>
   <tag></tag>
   <elementGuidId>fa94a43f-ad22-46b0-9f6f-ddbe299d6ae2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='searchKey']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#searchKey</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>93f5abe4-e5d6-4d4e-8f7f-8d280c2e4d91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>cd28c584-e185-4e62-a1d0-0497e18373c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>searchKey</value>
      <webElementGuid>c3e20a91-c43c-431e-a69d-26308ac2313b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control search-field-holder searchbox ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>3bede800-ca0a-460e-ba9c-0b66a536ac91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search for patient...</value>
      <webElementGuid>189e05dc-8f40-488c-b8cd-b8882235f894</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>323bc8e8-5e2d-4fe0-8325-a284b789195f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>search.startSearch()</value>
      <webElementGuid>fa0eca50-c79f-41d9-a55a-7de77991b08c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>search.searchvalue</value>
      <webElementGuid>6581da04-07f8-4f80-829c-b506c0a82925</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-init</name>
      <type>Main</type>
      <value>search.searchvalue=''</value>
      <webElementGuid>4cad78d7-3418-408b-8cf3-f8f54864454e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 750}</value>
      <webElementGuid>c776f00e-20ac-4daf-95cb-9ae8e6fdc268</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>031bd8f7-babb-4bb2-9361-885e2939b925</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>50</value>
      <webElementGuid>21420064-4503-448b-bdbb-7e1e9de4b383</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchKey&quot;)</value>
      <webElementGuid>591fe7d3-ba98-447c-a5bd-5eee58ebb96a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='searchKey']</value>
      <webElementGuid>9b03073e-ba7a-42a2-9060-9126f90ae5d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/input[3]</value>
      <webElementGuid>2939ea51-12ed-47b4-bea9-a47d24f0c849</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input[3]</value>
      <webElementGuid>6a82132b-f8ce-4c50-b171-7c954dd4763e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'searchKey' and @id = 'searchKey' and @placeholder = 'Search for patient...' and @type = 'text']</value>
      <webElementGuid>4d6fbd08-92b5-463e-89ba-29a933229020</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
