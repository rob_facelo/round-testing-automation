<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Plan Of Care  485</name>
   <tag></tag>
   <elementGuidId>e260504c-cacf-4bf1-b9c7-1616e138ccf6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OasisPlan']/div/div/table/tbody[4]/tr[2]/td/table/tbody/tr/td/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdFlatPadding&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13 cursorPointer ng-binding&quot;][count(. | //*[@href = '../../POC/Poc485.aspx?pocId=62005' and (text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')]) = count(//*[@href = '../../POC/Poc485.aspx?pocId=62005' and (text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')])]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d9beb320-e8d4-40a0-9a74-19ea61691883</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fontcolorDark13 cursorPointer ng-binding</value>
      <webElementGuid>64582f36-fa71-4e4c-85b0-2e0d396987c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>../../POC/Poc485.aspx?pocId=62005</value>
      <webElementGuid>71b045ca-0e6c-4608-ac08-3d03a37dec85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Plan Of Care / 485</value>
      <webElementGuid>aff144c1-6ef4-4833-bd64-288be011f5b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdFlatPadding&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13 cursorPointer ng-binding&quot;]</value>
      <webElementGuid>5ddcb56e-dc92-41c7-a4f4-1cdac634173a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OasisPlan']/div/div/table/tbody[4]/tr[2]/td/table/tbody/tr/td/a</value>
      <webElementGuid>2247a895-4478-4e90-b5d3-4a166a22734c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Plan Of Care / 485')])[2]</value>
      <webElementGuid>e6c262ad-e6e1-4543-a99e-15c9d53878f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Locked: 08/26/2022'])[1]/following::a[1]</value>
      <webElementGuid>6e6153ed-3b48-426e-9c36-d84e07d6f289</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recertification (follow-up) assessment'])[1]/following::a[1]</value>
      <webElementGuid>3958e197-b1a5-48fa-ad84-696a8078f5cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Progress'])[1]/preceding::a[1]</value>
      <webElementGuid>6ba6e342-5856-4f64-960e-430e2940776b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Updated By: dslfacelo'])[2]/preceding::a[1]</value>
      <webElementGuid>e50688ed-9b85-4a54-9a88-7dfc26e6ebb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '../../POC/Poc485.aspx?pocId=62005')]</value>
      <webElementGuid>260ab4d0-bf88-4984-b977-698d81873040</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tbody[4]/tr[2]/td/table/tbody/tr/td/a</value>
      <webElementGuid>51211027-a4f2-4b65-9764-38d1fb4cf386</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '../../POC/Poc485.aspx?pocId=62005' and (text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')]</value>
      <webElementGuid>12743a4f-f980-4274-8839-a217913811c3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
