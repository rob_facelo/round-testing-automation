<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Recert POC</name>
   <tag></tag>
   <elementGuidId>3cab8aef-11b7-4736-9a00-ce2c4beff6aa</elementGuidId>
   <imagePath>Screenshots/Targets/Page_WHITE, WALTER , 6  HHC/a_Plan Of Care  485.png</imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OasisPlan']/div/div/table/tbody[4]/tr[2]/td/table/tbody/tr/td/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdFlatPadding&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13 cursorPointer ng-binding&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value>Screenshots/Targets/Page_WHITE, WALTER , 6  HHC/a_Plan Of Care  485.png</value>
      </entry>
   </selectorCollection>
   <selectorMethod>IMAGE</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bded2664-3ed2-4b59-9377-92e7b042a53b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fontcolorDark13 cursorPointer ng-binding</value>
      <webElementGuid>7407fc5a-aab1-4a2c-945a-cccb0384a80e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>../../POC/Poc485.aspx?pocId=62002</value>
      <webElementGuid>a1d54dba-56e4-49ee-9378-8b6fee25b731</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Plan Of Care / 485</value>
      <webElementGuid>08b9ecc5-793a-4760-aae9-a6e419a7177f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;tdFlatPadding&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13 cursorPointer ng-binding&quot;]</value>
      <webElementGuid>7c560e31-f703-4142-9d4b-abee9f299d80</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OasisPlan']/div/div/table/tbody[4]/tr[2]/td/table/tbody/tr/td/a</value>
      <webElementGuid>15fea132-d6df-4c36-92a7-c50be57f8e79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Plan Of Care / 485')])[2]</value>
      <webElementGuid>3499e5aa-2e35-4bd5-a345-39a5ded571d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Locked: 02/27/2023'])[1]/following::a[1]</value>
      <webElementGuid>563b019d-adbd-4428-8a03-323c1c85790c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recertification (follow-up) assessment'])[1]/following::a[1]</value>
      <webElementGuid>cc55eadf-9326-4944-9e0c-6cad681d260d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Progress'])[2]/preceding::a[1]</value>
      <webElementGuid>d62825f2-adbc-41bb-bc70-ce0c028ac56f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Updated By: dslfacelo'])[2]/preceding::a[1]</value>
      <webElementGuid>241dd468-f6a8-49c6-83ee-8ba616c52b55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '../../POC/Poc485.aspx?pocId=62002')]</value>
      <webElementGuid>51e3af2f-a11e-4436-aa1b-bb22aa5abb82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tbody[4]/tr[2]/td/table/tbody/tr/td/a</value>
      <webElementGuid>10473a17-fbb7-48f8-9f68-bc5098a9ff3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '../../POC/Poc485.aspx?pocId=62002' and (text() = 'Plan Of Care / 485' or . = 'Plan Of Care / 485')]</value>
      <webElementGuid>1fce0fb8-939c-47de-bea7-cfd92501ef29</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
