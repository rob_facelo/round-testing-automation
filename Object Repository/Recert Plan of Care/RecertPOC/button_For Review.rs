<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_For Review</name>
   <tag></tag>
   <elementGuidId>8a33e5a1-770a-4b27-ae3f-bd983cffd4ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='reviewPoc']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'pocmaincontrol.ReviewPOC()' and (text() = '
                                
                                For Review
                            ' or . = '
                                
                                For Review
                            ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#reviewPoc</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>567d22ce-368c-4a46-b215-181091ad6b74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2650d7f4-2320-4af6-b5fc-e0b6de382201</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>pocmaincontrol.ReviewPOC()</value>
      <webElementGuid>7db1e5e2-e2fc-4634-beda-c5a9aacca6fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>reviewPoc</value>
      <webElementGuid>fd9d9808-1fc9-4d7e-964c-b040903db16f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default toolbar-btn-default</value>
      <webElementGuid>774811ba-780b-4fe0-94e1-a0003e8bd49b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                For Review
                            </value>
      <webElementGuid>49be19e4-81f6-4b9d-9789-d2c5698802b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;reviewPoc&quot;)</value>
      <webElementGuid>0003bdc6-5030-417b-bcdd-a98950141f9f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='reviewPoc']</value>
      <webElementGuid>dd6dd734-02a4-4428-8de6-17059d274764</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//fieldset[@id='pocButtons']/div/button[6]</value>
      <webElementGuid>ff0348d0-5edc-40db-b8b5-b39aefec07c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='For Review']/parent::*</value>
      <webElementGuid>7435d942-44bb-4dca-97ce-c77ef16f8eda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[6]</value>
      <webElementGuid>c8ca32b4-3aff-4ce1-8e2d-1a4e5c6bd3d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'reviewPoc' and (text() = '
                                
                                For Review
                            ' or . = '
                                
                                For Review
                            ')]</value>
      <webElementGuid>cc63c2bc-bbb6-41c9-b69f-e97ef3ae3bde</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
