<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Restrictions_btnAgree</name>
   <tag></tag>
   <elementGuidId>2eb3fdfb-73ee-4da4-8ae6-35961e01daa5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#btnAgree</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='btnAgree']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ad6344be-756c-47cc-a82a-477fd4c1c00b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>ed2e139e-cdb6-4211-8797-ca11cc03575e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>btnAgree</value>
      <webElementGuid>a8b08262-1796-4487-8b92-d56c6b376b18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>I/We Agree</value>
      <webElementGuid>d371265f-a956-42d4-afc8-98c1f255289d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>clearUserLogsStorage();</value>
      <webElementGuid>64a518fd-24b3-4d0c-ae8b-f4f6d10c3b3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnAgree</value>
      <webElementGuid>08af3b17-3578-40d2-b2cb-131406524031</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>agree-btn form-control</value>
      <webElementGuid>642370ba-5fbb-4f87-8c35-b85002024dc6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnAgree&quot;)</value>
      <webElementGuid>02375675-f800-430e-818a-5ed8904fecfb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='btnAgree']</value>
      <webElementGuid>f7d4df84-e363-4b4a-a618-0be356269938</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]/div[2]/div/div[3]/input</value>
      <webElementGuid>51fa90f5-c281-46ea-8502-ef5341f390bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/input</value>
      <webElementGuid>3d8f51ed-ff1e-4691-a988-bea82de11446</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'btnAgree' and @id = 'btnAgree']</value>
      <webElementGuid>d62a5cd1-d04a-47eb-b782-12b236f7057b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
