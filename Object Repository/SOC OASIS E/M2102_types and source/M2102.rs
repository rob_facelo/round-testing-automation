<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M2102</name>
   <tag></tag>
   <elementGuidId>c4233188-ad7f-409b-ae80-5ee139840f73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space.ng-scope > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2102']/div/div[2]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>d19821f7-06bf-40a5-9748-d6d12ef869e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>3b204fe0-75d8-4a20-a347-065e36c33bbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2100_CARE_TYPE_SRC_SPRVSN</value>
      <webElementGuid>833b8866-c60d-4800-aa18-fdfdec00c099</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN</value>
      <webElementGuid>8c7ddad1-83c9-4611-a4a5-a320426c1634</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2100_CARE_TYPE_SRC_SPRVSN', oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN)</value>
      <webElementGuid>acec3446-4ffb-4e53-bfe9-f9e81ce21333</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>5b9cb173-969a-43ba-a0bd-9df38b37add5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                    </value>
      <webElementGuid>64d3fca4-c03b-4ecb-99a8-cf0eb9cf814c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2102&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>b83835c4-2cc7-4386-b385-1c9895804cb9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2102']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>711685ae-6aba-44d4-8d96-2191f8d5a4d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[23]/following::select[1]</value>
      <webElementGuid>2c203f37-a148-4770-98f7-9b0938f58dcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plan/Comments:'])[1]/following::select[1]</value>
      <webElementGuid>0fad1610-8bcf-4764-8d28-5baf4bcdae6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Supervision and safety'])[1]/preceding::select[1]</value>
      <webElementGuid>c14b6eee-63dd-48aa-b259-b77bceeb8ec5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[6]/div/div[2]/div[2]/div/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>7d4f5eed-ec03-480e-86e4-5922b026b9dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                    ' or . = '
                                        
                                        0
                                        1
                                        2
                                        3
                                        4
                                    ')]</value>
      <webElementGuid>cb242cad-3658-4699-9eeb-ba5315a06964</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
