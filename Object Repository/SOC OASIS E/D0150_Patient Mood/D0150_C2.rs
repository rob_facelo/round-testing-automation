<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>D0150_C2</name>
   <tag></tag>
   <elementGuidId>27bd05bc-fef9-4a2c-90bf-b1df64be50d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-6.col-md-6.col-lg-6.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='D0150']/div/div[2]/div[9]/div/div/div[2]/div/div[2]/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>381e2007-6965-4239-b490-8792be54f57f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>8b22ce37-7f90-4f17-9294-0e6f022d7a99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>D0150C2</value>
      <webElementGuid>085253fa-e568-4351-9d29-25d5ef6febc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>870a0efb-ff41-4914-8147-39e017ad5e26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.D0150C2</value>
      <webElementGuid>401499c9-248d-4c04-aec5-c968e3b7c553</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('D0150C2', oasis.oasisDetails.D0150C2); oasis.autoCalculateD0160(1);</value>
      <webElementGuid>5d4f637e-3cf8-404f-b1bf-2f679710b514</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked || oasis.oasisDetails.D0150C1 == '9' || oasis.oasisDetails.D0150C1 == '' || oasis.oasisDetails.D0150C1 == '-' || (oasis.oasisDetails.D0150A1 == '9' &amp;&amp; oasis.oasisDetails.D0150B1 == '9')
                                                            || ((oasis.oasisDetails.D0150A2 == '0' || oasis.oasisDetails.D0150A2 == '1') &amp;&amp; (oasis.oasisDetails.D0150B2 == '0' || oasis.oasisDetails.D0150B2 == '1'))</value>
      <webElementGuid>04370302-9235-490a-9b52-bc5bed31941e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        0
                                                        1
                                                        2
                                                        3
                                                    </value>
      <webElementGuid>49789f25-4b56-49ae-b270-e6450fce24b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;D0150&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-3 col-md-2 col-lg-2&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>b97e4a1a-f881-4792-97c8-a761b6ba764f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='D0150']/div/div[2]/div[9]/div/div/div[2]/div/div[2]/div/select</value>
      <webElementGuid>8f575c0e-3aa4-4672-bad8-2e829be1343d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Trouble falling or staying asleep, or sleeping too much'])[1]/following::select[2]</value>
      <webElementGuid>56f96fca-3227-463a-be31-2c989cb959ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Feeling tired or having little energy'])[1]/preceding::select[1]</value>
      <webElementGuid>2460a41a-4664-4e43-ba75-04ef8ad82bbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div/div/div[2]/div/div[2]/div/select</value>
      <webElementGuid>86471c5f-7ecc-4bff-b74d-b1e3ea8a87ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        3
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        3
                                                    ')]</value>
      <webElementGuid>b42afb0e-cf50-45df-92f8-464ebab860d7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
