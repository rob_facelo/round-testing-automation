<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_other Diagnoses</name>
   <tag></tag>
   <elementGuidId>880f7f8e-1119-4a8f-b3d5-feccf325a45e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M0240_OTH_DIAG1_ICD > div.dropdown > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > input.form-control.input-sm.ng-pristine.ng-untouched.ng-valid.ng-valid-minlength.ng-valid-maxlength</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[206]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;M0240_OTH_DIAG1_ICD&quot;)/div[@class=&quot;dropdown&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-minlength ng-valid-maxlength&quot;][count(. | //*[@type = 'text' and @class = 'form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-minlength ng-valid-maxlength']) = count(//*[@type = 'text' and @class = 'form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-minlength ng-valid-maxlength'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>75639b09-4871-4cb8-81f7-18b45107a768</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>1136b4bb-dd4f-4335-b7b5-311fa32a2ff9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M0240_OTH_DIAG1_ICD</value>
      <webElementGuid>849702ea-49da-4bc2-8e6f-b9385fcd15c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-minlength ng-valid-maxlength</value>
      <webElementGuid>3a196e83-127f-4ae6-90ef-413a650fa428</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>model</value>
      <webElementGuid>40e8a366-71e6-49cc-96c0-3f8e7e20c226</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>click-outside</name>
      <type>Main</type>
      <value>closeThis()</value>
      <webElementGuid>3ccfda3a-463a-4838-a1c7-f65e6d037c0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>searchValue(model,searchtype)</value>
      <webElementGuid>b9c182d3-2548-4938-adbb-f6638a16ace4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>searchValue(model,searchtype)</value>
      <webElementGuid>46835848-469c-48ff-82c2-b53c44cfdd8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-keydown</name>
      <type>Main</type>
      <value>selected=false</value>
      <webElementGuid>b2bc7d45-7c9e-435d-af41-9433558c4e87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-minlength</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>447b8a70-f5d7-4f5c-a9c8-e8f86679d150</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>customDisabled</value>
      <webElementGuid>d56af677-7f4d-4fee-bd93-03380e084722</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model-options</name>
      <type>Main</type>
      <value>{debounce: 500}</value>
      <webElementGuid>28c1acea-e717-4a1c-8af0-47830b3ff2d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>1a7030a8-2c5c-4b57-b734-72b5fab66ac7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M0240_OTH_DIAG1_ICD&quot;)/div[@class=&quot;dropdown&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-minlength ng-valid-maxlength&quot;]</value>
      <webElementGuid>1f132b5d-cc68-4e4c-aadd-f3357fe83545</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[206]</value>
      <webElementGuid>30617f51-2660-4420-a59b-023242278824</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//autocomplete-directive[@id='M0240_OTH_DIAG1_ICD']/div/div/div/input</value>
      <webElementGuid>9dc9a707-aee2-4f66-a03f-a457941df1f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[5]/td[2]/div[2]/div/autocomplete-directive/div/div/div/input</value>
      <webElementGuid>557638f2-1a21-4abc-a0eb-2de6e1669b96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>b504e835-1231-42ab-9d12-c49cfd5af3d9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
