<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0130G2</name>
   <tag></tag>
   <elementGuidId>bb3a8ec2-9cdc-4fd8-9fe6-0ae52f329b34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-12.col-lg-12.ng-scope > div.row > div.col-xs-4.col-sm-4.col-md-3.col-lg-3.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.row > div.col-xs-6.col-sm-6.col-md-6.col-lg-6 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0130G2']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[7]/div/div/div/div/div[2]/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>42f2a58c-3865-4419-b1c2-2307d73f14c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>39f97812-bea4-4f2d-adb8-3efc7d7c90e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0130G2</value>
      <webElementGuid>a60eaa31-2091-4da2-8e7d-1a8e3f441355</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>b54ff374-758a-4e1b-b329-86b61a4d01e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0130G2</value>
      <webElementGuid>3096fb66-c442-4446-82a0-054d3cdaf898</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0130G2', oasis.oasisDetails.GG0130G2)</value>
      <webElementGuid>1f36b829-540c-41ae-9b5e-81b40a024cc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>2c029de3-6389-4ace-b3c3-d73e136708aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        </value>
      <webElementGuid>870aa89b-e3af-4330-8431-a113d898f892</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0130&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-3 col-lg-3 row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-6 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>a8336908-9a6c-48ac-9f0c-443fbb71b049</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[7]/div/div/div/div/div[2]/div/select</value>
      <webElementGuid>69a689a2-a8bf-45d8-9e40-9571f715be63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='F. Upper body dressing:'])[1]/following::select[2]</value>
      <webElementGuid>0bd48a21-d9e8-4a91-bd35-4a15cb7086c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='G. Lower body dressing:'])[1]/preceding::select[1]</value>
      <webElementGuid>52593337-e335-4a56-aa08-9d2a55c429d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]/div/div[2]/div[7]/div/div/div/div/div[2]/div/select</value>
      <webElementGuid>f26cfecf-68cb-40d9-8fee-7a6de4415c09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ' or . = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ')]</value>
      <webElementGuid>25b3b37a-52c7-4192-9d0f-4476afc84b4d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
