<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0130C1</name>
   <tag></tag>
   <elementGuidId>74f3ddb6-03c7-41a7-8f30-9c031228ace1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#GG0130 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.ng-scope > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.row > div.col-xs-4.col-sm-4.col-md-3.col-lg-3.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > div.row > div.col-xs-6.col-sm-6.col-md-6.col-lg-6 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0130C1']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>e5a1faeb-f510-4466-a6aa-b47e2f651f36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>94e28241-224f-439b-97f5-6c7b6645b52a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0130C1</value>
      <webElementGuid>bbc138f9-9418-440d-9846-f4afffb90fb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>90ebf4f1-f9db-4b38-8982-d2c0027a5b70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0130C1</value>
      <webElementGuid>3f2bb88e-993d-4e43-a049-25ace1889599</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0130C1', oasis.oasisDetails.GG0130C1)</value>
      <webElementGuid>0897ddaa-891f-458b-997f-da8453edc339</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>ea169d5c-6880-42c7-a948-d33000caba2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        </value>
      <webElementGuid>25cc57b9-a301-492d-b041-8bb7e49fbcbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0130&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-3 col-lg-3 row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-6 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>f2b61386-c872-421a-a483-7d02fff7ab1d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0130']/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      <webElementGuid>c190ed7c-0145-4202-b1ad-ade125154d91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Oral Hygiene:'])[1]/following::select[1]</value>
      <webElementGuid>3924c72d-2c87-4e80-bf11-fae9dc98c13f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. Eating:'])[1]/following::select[3]</value>
      <webElementGuid>47a4440c-2041-45f1-bd8d-fe461ca4100b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C. Toileting Hygiene:'])[1]/preceding::select[2]</value>
      <webElementGuid>3b0129a0-c3df-4b60-be4d-144ea62e077c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='E. Shower/bathe self:'])[1]/preceding::select[4]</value>
      <webElementGuid>52eed70b-a18a-4673-b203-7caf3ecd5989</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/select</value>
      <webElementGuid>974ab057-4398-46d2-91f4-d91788ff5f78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ' or . = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ')]</value>
      <webElementGuid>7e7f6ede-87bb-4adf-a4c8-8f6b4e748093</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
