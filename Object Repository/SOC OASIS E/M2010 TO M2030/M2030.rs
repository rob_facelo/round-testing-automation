<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M2030</name>
   <tag></tag>
   <elementGuidId>f42e8fcb-3038-4dd4-b371-4207a5fa9699</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2030']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M2030 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b250f601-d3f0-40d5-bc5a-d71bbf32ed00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>17c07c72-74f4-4b55-82ed-d978a4d31037</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2030_CRNT_MGMT_INJCTN_MDCTN</value>
      <webElementGuid>cf518105-e70a-486f-9c39-d7825cbd59ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN</value>
      <webElementGuid>bd1cb95c-1355-4081-b1ac-32dbed05fce1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2030_CRNT_MGMT_INJCTN_MDCTN', oasis.oasisDetails.M2030_CRNT_MGMT_INJCTN_MDCTN)</value>
      <webElementGuid>a17fdd92-2ece-4838-a7f4-4fa867b46198</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>(oasis.oasisDetails.M2000_DRUG_RGMN_RVW == '9' &amp;&amp; (oasis.oasisDetails.M0100_ASSMT_REASON == '01' || oasis.oasisDetails.M0100_ASSMT_REASON =='03')) || oasis.isLocked</value>
      <webElementGuid>5b851c3a-3ed4-42ac-a4d1-e9fb69932416</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                </value>
      <webElementGuid>570806db-b630-49e1-9ae8-862cf0999740</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2030&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>017d61a4-7b3e-4f6d-9940-3c33914b5a68</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2030']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>7b344c70-cbd3-4dc3-ad4a-3f3b97d75291</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[56]/following::select[1]</value>
      <webElementGuid>cfed3eda-09da-4f5b-83cf-8c9968705235</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Excludes'])[3]/following::select[1]</value>
      <webElementGuid>84b58a53-958a-4ff9-ae11-9b8eb7b65a5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to independently take the correct medication(s) and proper dosage(s) at the correct times.'])[1]/preceding::select[1]</value>
      <webElementGuid>291c1205-41b4-47bb-8db4-2968fad1b3cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to take injectable medication(s) at the correct times if:'])[1]/preceding::select[1]</value>
      <webElementGuid>799c835a-09bb-450d-be34-d9d351be553f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[14]/div/div[2]/div[7]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>ddc548a3-8ab3-4a24-88d4-9b404496c4e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    NA
                                    
                                ')]</value>
      <webElementGuid>d16b2cb6-c479-477a-8dd7-97d4e2418a18</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
