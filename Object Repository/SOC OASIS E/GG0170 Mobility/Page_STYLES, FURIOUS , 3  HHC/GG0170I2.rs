<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0170I2</name>
   <tag></tag>
   <elementGuidId>0e8c5617-511d-4d5c-bb14-fc57e904f62d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0170I2']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[9]/div/div/div/div/div[2]/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>e9b6fdc6-2019-4c14-97e6-e5bdf7dab464</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>77f6b7bd-c86d-4f0f-b8af-9f48e31543d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170I2</value>
      <webElementGuid>72e2489a-564f-48a0-a0ba-768c7261b374</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>24e68146-1f50-4759-84c4-289aad4fd70c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170I2</value>
      <webElementGuid>162da017-a382-4534-93fa-3d8c4f5983a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0170I2', oasis.oasisDetails.GG0170I2)</value>
      <webElementGuid>626dfeff-1c30-4d55-b6a6-5b3fb7d7ccee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>c6600a1d-0a73-4223-a2a1-40ee01c8e902</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        </value>
      <webElementGuid>ce16d5ed-2cc1-452e-a10b-0ad72f525a64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-3 col-lg-3 row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-6 col-sm-6 col-md-6 col-lg-6&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>418a156a-6324-4a3e-ba43-4a7dc696cf97</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[9]/div/div/div/div/div[2]/div/select</value>
      <webElementGuid>4849e025-406d-454e-8957-135e68efbef8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='G. Car Transfer:'])[1]/following::select[2]</value>
      <webElementGuid>f716dc41-c188-4a10-9cc0-33396125b50e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='F. Toilet transfer:'])[1]/following::select[4]</value>
      <webElementGuid>37921c6a-3fdf-4bfa-abeb-47b5f47472dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='I. Walk 10 feet:'])[1]/preceding::select[1]</value>
      <webElementGuid>89c6c999-cce6-4eb7-857e-f880a8f09a23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div/div/div/div/div[2]/div/select</value>
      <webElementGuid>1a4c5814-e6da-4b4c-ab9c-c53e4b589495</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ' or . = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ')]</value>
      <webElementGuid>b37de377-0fe6-44d4-a56d-2a19c7d73d10</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
