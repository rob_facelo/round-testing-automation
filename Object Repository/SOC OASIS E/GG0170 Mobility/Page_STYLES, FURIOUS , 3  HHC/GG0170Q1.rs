<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0170Q1</name>
   <tag></tag>
   <elementGuidId>821fdee5-aa3e-4cf0-848d-57f8138d09b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-4.col-sm-4.col-md-2.col-lg-2 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0170Q1' and (text() = '
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    ' or . = '
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[17]/div/div[2]/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>be1d0aab-0490-4521-a4ec-3dd294ed4eef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>6512d48c-b198-4777-8e71-bf83afe45f7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170Q1</value>
      <webElementGuid>926140fd-5527-4607-8d61-0ed89dd83e0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>ce32b1c9-7b2a-4a9a-ad2f-1aa14d8dd83c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170Q1</value>
      <webElementGuid>10c0646f-9fc5-40dd-8ff8-994f0450596a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.clearMItemFieldsValues('GG0170Q1','select',['0,GG0170R1,GG0170R2,GG0170RR1,GG0170S1,GG0170S2,GG0170SS1'])</value>
      <webElementGuid>7b460b52-94c6-45c3-b5da-dbcdd292486a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>cedb41c9-f8e1-4bf9-aff8-254e48109893</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    </value>
      <webElementGuid>05c99c08-d5bc-4ca3-8e7a-c6d41e823cf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-8 col-sm-8 col-md-9 col-lg-9&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-2 col-lg-2&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>e2cd3bc3-efaa-495d-bd8c-5fad500be62c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[17]/div/div[2]/div/div/div/select</value>
      <webElementGuid>db88274c-525a-4bb4-8116-b65dad488df6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='P. Picking up object:'])[1]/following::select[1]</value>
      <webElementGuid>5d86e128-1d87-4ef8-b8fa-a19d6779cc61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='O. 12 steps:'])[1]/following::select[3]</value>
      <webElementGuid>36c0b381-747b-4ace-9e7a-8215e364ac42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Q. Does patient use wheelchair/scooter?'])[1]/preceding::select[1]</value>
      <webElementGuid>f9f1af56-49cf-494d-8f2c-7979d6b83323</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/select</value>
      <webElementGuid>07a531f0-a7e2-4fc8-af25-0fd23e100753</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    ' or . = '
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    ')]</value>
      <webElementGuid>f5138a80-4284-4727-b10b-d6367b025d94</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
