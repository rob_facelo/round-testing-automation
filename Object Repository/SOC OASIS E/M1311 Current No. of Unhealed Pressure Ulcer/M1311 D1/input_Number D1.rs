<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Number D1</name>
   <tag></tag>
   <elementGuidId>48d5e9e6-03f4-4f0a-af4b-ef1dee0cf36b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @ng-model = 'oasis.oasisDetails.M1308_NSTG_DRSG']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[163]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7b7b9df4-3636-459b-8f82-7f1af5c18559</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d3ad6623-bb43-4001-9476-64e8564cf10c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-valid ng-valid-maxlength ng-touched</value>
      <webElementGuid>dc556146-d26b-4c1f-a1df-d2e9e1ffbd33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1308_NSTG_DRSG</value>
      <webElementGuid>102756ad-e939-4101-9205-8134092e4f40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>96947565-1781-47a6-8eab-2aee9bc1d958</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1308_NSTG_DRSG</value>
      <webElementGuid>bf594d95-de03-4611-b5f9-e06b7048a7d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1308_NSTG_DRSG', oasis.oasisDetails.M1308_NSTG_DRSG)</value>
      <webElementGuid>39c9eac1-6f9a-4399-bcfd-8cca6333be3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1306_UNHLD_STG2_PRSR_ULCR == '0' || oasis.isLocked</value>
      <webElementGuid>5048afbb-3066-4db7-9300-99ca20634737</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1311&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space content-box content-box-m1311&quot;]/table[@class=&quot;table table-bordered tbl1311&quot;]/tbody[1]/tr[5]/td[2]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-valid ng-valid-maxlength ng-touched&quot;]</value>
      <webElementGuid>66f01538-c667-4d59-86e7-d0e20d856b87</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[163]</value>
      <webElementGuid>a7d4f070-0b14-4ea1-a46d-bf85884b82db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1311']/div/div[2]/table/tbody/tr[5]/td[2]/div/input</value>
      <webElementGuid>4c48b35a-1035-4d3a-92c6-e711f85dbfe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/table/tbody/tr[5]/td[2]/div/input</value>
      <webElementGuid>4a18e55e-2a88-4bbc-a8e0-54b3275b1ff4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>27fd92b0-52d4-4d89-bdd6-2cf4d8c9cc96</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
