<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1332</name>
   <tag></tag>
   <elementGuidId>fa42ccec-d869-4ab9-9690-ccc9d8e3e494</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1332']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1332_NUM_STAS_ULCR']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M1332 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b2f3aa0b-c097-49f8-844a-2fc53b44fb6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>22b32b34-6177-4ebb-9a3d-6c2448160d8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1332_NUM_STAS_ULCR</value>
      <webElementGuid>aafc6db4-e932-41fc-8008-e016e98960a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1332_NUM_STAS_ULCR</value>
      <webElementGuid>438c53b0-02df-4d23-b1f3-df0c87496b30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1332_NUM_STAS_ULCR', oasis.oasisDetails.M1332_NUM_STAS_ULCR)</value>
      <webElementGuid>5376046f-d6c3-43cd-9ef2-af84fa3fcd5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1330_STAS_ULCR_PRSNT == '00' || oasis.oasisDetails.M1330_STAS_ULCR_PRSNT == '03' || oasis.isLocked</value>
      <webElementGuid>bfb7c2f0-9730-4788-9ec1-aac6ac47ea09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                </value>
      <webElementGuid>8ad487a6-a989-48cf-9cb4-97e3faac3f58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1332&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>0f28c62c-af5b-4c39-a793-24fe02142343</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1332']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>f8baa280-0864-4808-94bf-f2753b66903c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[48]/following::select[1]</value>
      <webElementGuid>20d889b7-9d7d-4648-9109-1e291e852e14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M1332. Current Number of Stasis Ulcer(s) that are Observable'])[1]/following::select[1]</value>
      <webElementGuid>6947eb77-318f-4a1d-977e-009eac0bd355</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='One'])[2]/preceding::select[1]</value>
      <webElementGuid>ec6a3f0f-b79d-4355-ab89-95d0cce6faec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Two'])[2]/preceding::select[1]</value>
      <webElementGuid>fac54a2c-9f43-4738-a58d-2fa52a6bc28d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[13]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>e21ae9be-a8bc-4bec-91d4-14fcf831599b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                ' or . = '
                                    
                                    1
                                    2
                                    3
                                    4
                                    
                                ')]</value>
      <webElementGuid>bc926b51-b5fb-435d-abe1-cb93071a8c57</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
