<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M2020</name>
   <tag></tag>
   <elementGuidId>b21a0bd5-f79c-41c4-af80-5712faf0aebc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2020']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M2020_CRNT_MGMT_ORAL_MDCTN' and (text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                NA
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                NA
                                            ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M2020 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>908d575d-02c1-430d-885f-3fe931e906e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>68d9a3d5-6620-496b-9687-d8ecd1d8fc4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2020_CRNT_MGMT_ORAL_MDCTN</value>
      <webElementGuid>f592989b-b786-48e2-ac96-7a73dd3704a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2020_CRNT_MGMT_ORAL_MDCTN</value>
      <webElementGuid>2d397726-6ccc-462d-b2ec-c8d3707cee73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2020_CRNT_MGMT_ORAL_MDCTN', oasis.oasisDetails.M2020_CRNT_MGMT_ORAL_MDCTN)</value>
      <webElementGuid>f6551b15-2f24-4281-a28a-a2b2f86271c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>(oasis.oasisDetails.M2000_DRUG_RGMN_RVW == '9' &amp;&amp; (oasis.oasisDetails.M0100_ASSMT_REASON == '01' || oasis.oasisDetails.M0100_ASSMT_REASON =='03')) || oasis.isLocked</value>
      <webElementGuid>baeb316f-ea0c-414d-a1f5-c416bf889356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                NA
                                            </value>
      <webElementGuid>6d9e03e0-7f02-428a-aac9-ffa61bd94a3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2020&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>ee7bdb1e-3a3b-467e-b818-7cebd1554372</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2020']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>2281a3ea-1891-4d59-a8c3-157ea2826196</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[37]/following::select[1]</value>
      <webElementGuid>281723fe-6b03-47f7-80a8-72b1adf98a1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Excludes'])[2]/following::select[1]</value>
      <webElementGuid>7c6b579a-72ad-4508-bccf-97e789196dbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[14]/div/div[2]/div/div/div[2]/div[5]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>0d928a74-8d8e-4eee-b6c5-12d7e355cbf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                NA
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                NA
                                            ')]</value>
      <webElementGuid>080f7293-1dce-4a8f-a5cc-90041cc20092</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
