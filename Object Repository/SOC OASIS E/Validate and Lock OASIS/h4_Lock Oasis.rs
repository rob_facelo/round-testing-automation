<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Lock Oasis</name>
   <tag></tag>
   <elementGuidId>a49d2a94-9212-4c88-b307-a56ed5ce28c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/following::h4[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Lock Oasis' or . = ' Lock Oasis')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal.fade.ng-scope.in > div.modal-dialog.modal-sm > div.modal-content > div.modal-header.hhc-blue > h4.modal-title.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>1fcab269-4bb6-4259-8419-346b6b79ba7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-title ng-binding</value>
      <webElementGuid>aed5f3f8-75c4-4edd-b65e-18d2c2a13852</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Lock Oasis</value>
      <webElementGuid>46a1d25b-0114-470a-9c7c-89c228d2668a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header hhc-blue&quot;]/h4[@class=&quot;modal-title ng-binding&quot;]</value>
      <webElementGuid>a2268230-e0a0-48c0-8110-34d6fb35a1ac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/following::h4[1]</value>
      <webElementGuid>b61beece-8d6c-4bd5-846d-bd1ec4cbb432</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock OASIS'])[1]/following::h4[1]</value>
      <webElementGuid>b1288199-8552-48f6-b76d-f2606dfa357d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[2]/preceding::h4[1]</value>
      <webElementGuid>4077d6fa-07d5-4d99-b061-f647a98792af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock OASIS'])[2]/preceding::h4[1]</value>
      <webElementGuid>5e74e650-39df-4c7d-b641-f51863a7f0b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/h4</value>
      <webElementGuid>5e3216ee-9e98-4ee9-b562-c607263c7a79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = ' Lock Oasis' or . = ' Lock Oasis')]</value>
      <webElementGuid>3a2c29db-1445-40e0-a4b3-d5eb585c49ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
