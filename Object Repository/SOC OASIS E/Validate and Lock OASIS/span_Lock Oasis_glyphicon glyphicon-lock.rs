<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Lock Oasis_glyphicon glyphicon-lock</name>
   <tag></tag>
   <elementGuidId>f3aae630-c2b9-4214-a075-397596e79e57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-lock</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[3]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1b4cd423-d7dd-44e7-b2b0-dac9ac7a5628</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-lock</value>
      <webElementGuid>23714e85-4496-4609-8167-313c491152b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 header_nav_container_oasis&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]/span[@class=&quot;glyphicon glyphicon-lock&quot;]</value>
      <webElementGuid>41e54959-d4b2-48c9-a1e9-136c61a13ee9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[3]/span</value>
      <webElementGuid>b5b23721-f24a-4899-9274-eb05dc88238b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]/span</value>
      <webElementGuid>e4e06f69-43de-490b-9492-fa027d0ebce8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
