<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>J0510</name>
   <tag></tag>
   <elementGuidId>fcd40ae8-2637-4b7b-869a-6a3b2d17f55b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='J0510']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.J0510']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select.form-control.enter-code.ng-valid.ng-dirty.ng-valid-parse.ng-touched</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>efb3de39-afee-4c85-852c-757837c376c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-valid ng-dirty ng-valid-parse ng-touched</value>
      <webElementGuid>1d5952bb-2935-42ef-a4de-19b7b18fde81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>J0510</value>
      <webElementGuid>7a704558-fc51-40ea-b691-857bdad600bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>0f85afff-f642-4841-b27b-5cfaf4d0d5ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J0510</value>
      <webElementGuid>24b47e8d-c57f-4ae0-b482-6c2a459a36b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('J0510', oasis.oasisDetails.J0510)</value>
      <webElementGuid>0fe36b63-5120-429c-986b-d476f28d9449</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>904b51cc-f642-4b5d-b988-958904cac844</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    8
                                </value>
      <webElementGuid>d5b4adf7-4c41-4673-933c-0e26a3905a88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;J0510&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-valid ng-dirty ng-valid-parse ng-touched&quot;]</value>
      <webElementGuid>fb53960d-0e15-41f3-b043-eb38bb248810</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='J0510']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>ead05c12-58a6-4e4c-8c8b-c63e8f1c2bec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[39]/following::select[1]</value>
      <webElementGuid>11d04964-93ed-4463-be20-5e27dd4cb55a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='J0510. Pain Effect on Sleep'])[1]/following::select[1]</value>
      <webElementGuid>de5f5e48-f28a-4cbb-9613-6b3f7fefca75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[11]/div/div[2]/div[2]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>cc75f31c-84f4-4875-b644-f10c0ceeaf26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    8
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    8
                                ')]</value>
      <webElementGuid>5de16060-80ef-4b8f-ad78-5f20e82ccb7b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
