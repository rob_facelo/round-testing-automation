<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_M1023 Date</name>
   <tag></tag>
   <elementGuidId>7eac6ccf-4eb1-4c5b-8229-70d8fe0e4687</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td[name=&quot;M1023&quot;] > div.col-xs-8.col-sm-8.col-md-8.col-lg-8.space.row > div.input-group.date > input.form-control.input-sm.ng-pristine.ng-valid.ng-valid-maxlength.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M0240_OTH_DIAG1_DATE']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[88]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1542eb9e-7a61-48e6-bbf0-38cee66fe2e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>13c21fe4-d608-4c0d-89fd-fa91e246d446</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-valid ng-valid-maxlength ng-touched</value>
      <webElementGuid>311afe18-3a0e-4dd4-ad0e-402e5fd1911f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this, this.value, event, false, '1')</value>
      <webElementGuid>061cd1af-c5f2-47cc-be55-93790de11e9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>9b2cc701-bb65-4dab-a81c-e6b23c6cc5a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M0240_OTH_DIAG1_DATE</value>
      <webElementGuid>f03d0a1e-50e3-479e-89f2-2e345251a9bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M0240_OTH_DIAG1_DATE', oasis.oasisDetails.M0240_OTH_DIAG1_DATE)</value>
      <webElementGuid>4823cf7b-d575-4b17-b660-07c9654dd510</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>date</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>68a596a5-ead0-43d9-8304-7b28e41986db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked || oasis.oasisDetails.M0240_OTH_DIAG1_ICD == '='</value>
      <webElementGuid>63891b29-0407-417e-9270-9adf2aecfce2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>9ce4a27f-dd1f-438e-9e35-51831e65e248</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1023&quot;)/td[1]/div[@class=&quot;col-xs-8 col-sm-8 col-md-8 col-lg-8 space row&quot;]/div[@class=&quot;input-group date&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-valid ng-valid-maxlength ng-touched&quot;]</value>
      <webElementGuid>30b319fc-0ed9-42e1-83d6-ddf1d7c69771</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[88]</value>
      <webElementGuid>b1d4f069-6fc0-4cb3-a65f-0652dc2c898a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='M1023']/td/div[3]/div/input</value>
      <webElementGuid>30b14c75-2cf1-406d-b169-89d2c069fcfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[6]/td/div[3]/div/input</value>
      <webElementGuid>ab426304-5bc6-4a5f-9526-7ac6b89e122c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>f793732e-f71b-4527-aab2-71b07f53a453</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
