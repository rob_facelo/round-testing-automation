<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_A2. IV</name>
   <tag></tag>
   <elementGuidId>084290db-22be-4f5f-8609-ddacfdf6ad2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='O0110']/div/div[2]/div/div[4]/div/div/strong</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-6.col-sm-8.col-md-4.col-lg-3 > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>38803733-3036-4c71-a6e6-b1921a021420</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>A2. IV</value>
      <webElementGuid>7693c8a5-b986-49e1-8657-4a5aa5d033ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;O0110&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 space&quot;]/div[@class=&quot;col-xs-6 col-sm-8 col-md-4 col-lg-3&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/strong[1]</value>
      <webElementGuid>84dccac7-0afe-4220-8cde-9b2d3013c837</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='O0110']/div/div[2]/div/div[4]/div/div/strong</value>
      <webElementGuid>3e9501c7-edc2-46ab-ace1-0c3592b0a64f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A1. Chemotherapy'])[1]/following::strong[1]</value>
      <webElementGuid>325ca616-dad9-4fe7-a5bf-1abeb76b4913</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='🡓 Check all that apply 🡓'])[3]/following::strong[2]</value>
      <webElementGuid>7bd3f171-d446-4669-83fe-b1479c3b0dae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A3. Oral'])[1]/preceding::strong[1]</value>
      <webElementGuid>f74c3298-c5e9-400a-904c-4dd89bc68fbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A10. Other'])[1]/preceding::strong[2]</value>
      <webElementGuid>f10ce955-86ea-4330-a60b-e53ed2c686a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='A2. IV']/parent::*</value>
      <webElementGuid>f5772af2-e48a-4baf-a3bc-c15cc93238e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[4]/div/div/strong</value>
      <webElementGuid>4afbea1d-db3b-49c7-88f3-6342c2895299</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'A2. IV' or . = 'A2. IV')]</value>
      <webElementGuid>874f03b8-e65c-43e1-b4ed-d5aa3b399948</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
