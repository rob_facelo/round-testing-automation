<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_NA  No specific SOC date ordered by physician_panel-body panelContent</name>
   <tag></tag>
   <elementGuidId>0c4cb50a-7c83-447b-bf9d-fd2d8b4ca28e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M0104']/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M0104 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;M0104&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>899a98d0-095f-4295-8a22-f5ea58f29ec6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>panel-body panelContent</value>
      <webElementGuid>0f7e54d6-471c-4566-9b00-0211456324e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M0104&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]</value>
      <webElementGuid>815e6dfd-e13b-46de-8f9a-7aa6ef6ce1e4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M0104']/div/div[2]</value>
      <webElementGuid>af6001a7-d160-488b-9d19-a2cfb4185374</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0110'])[1]/following::div[7]</value>
      <webElementGuid>538c95d6-1984-4715-a00b-aaad42541e29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[4]/preceding::div[7]</value>
      <webElementGuid>3b6340ed-80d1-4822-8c11-791052a85803</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Early'])[1]/preceding::div[10]</value>
      <webElementGuid>49b9e165-2721-4249-9fb4-16ece948baaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[15]/div/div/div[2]</value>
      <webElementGuid>15e600a7-3414-4256-a97f-1375653f39c8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
