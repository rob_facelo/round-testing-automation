<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_M0Set Lookup                           _81d67f_1_2</name>
   <tag></tag>
   <elementGuidId>615a543f-fe71-4cc1-8e17-f46e3ac4ad4e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row.footer_nav_container_oasis</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>324a9afe-a8cd-426b-8397-609553f55677</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row footer_nav_container_oasis</value>
      <webElementGuid>77f410b9-b5bc-4c6b-9596-d8d9caa1edc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    </value>
      <webElementGuid>bdce839a-a7e7-4a0a-98b3-cfbf680f7922</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]</value>
      <webElementGuid>fb571b36-a270-49fd-a325-44cf1fc4e162</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]</value>
      <webElementGuid>53d1d65f-ef51-41e9-9c80-f20fca4df128</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Safety Measures (goes to POC Box #15)'])[1]/following::div[16]</value>
      <webElementGuid>c4edc3ee-8f1e-4ac3-ab60-49633e10a415</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rehabilitation Potential is Excellent'])[1]/following::div[20]</value>
      <webElementGuid>e64e7fe0-8b6e-4f1b-80b5-4c5e6ad293d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div[7]</value>
      <webElementGuid>484922c3-16c1-41db-839f-e31392a495c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    ' or . = '
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    ')]</value>
      <webElementGuid>d43ed590-bba4-4470-ab83-0a20a5b6463b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
