<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Go</name>
   <tag></tag>
   <elementGuidId>ddc674ad-679e-4a6d-8735-0e255228c79e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[81]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row > button.btn.btn-default.btn-sm.toolbar-btn-default.font12</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'oasis.goTo(oasis.goToMosetVal)' and (text() = '
                            
                            Go
                        ' or . = '
                            
                            Go
                        ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>34d2c910-6baf-435f-b952-f4a9d8996895</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4b1d6275-45df-492a-ac56-a0bc411f5580</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btn-sm toolbar-btn-default font12</value>
      <webElementGuid>bcecf0d9-1812-4907-96cd-883710ec24af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.goTo(oasis.goToMosetVal)</value>
      <webElementGuid>d182fbce-81f8-44ca-b211-1aa6ecff7acf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>!oasis.oasisDetails.FULL_OASIS || oasis.goToMosetVal == ''</value>
      <webElementGuid>6dab8021-fb96-47a0-901e-68a0f1104162</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Go
                        </value>
      <webElementGuid>3ab6bb1c-9d20-4a8e-b3fb-b46ed58dc5d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/fieldset[1]/div[@class=&quot;row&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12&quot;]</value>
      <webElementGuid>b1d666ac-35da-4602-9ad6-853bf9412d21</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[81]</value>
      <webElementGuid>c2fdc807-b436-4431-88da-5574e8fe123d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button</value>
      <webElementGuid>5318d497-f31b-457e-9758-45b3aaab4697</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0Set Lookup:'])[1]/following::button[1]</value>
      <webElementGuid>31332d87-d111-4c33-9f75-7946103272f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Safety Measures (goes to POC Box #15)'])[1]/following::button[1]</value>
      <webElementGuid>9663bf8b-d387-4662-9b59-e3e4f968d778</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Go']/parent::*</value>
      <webElementGuid>b2d8a741-c882-4e55-8f26-e7da13e67e13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div[2]/fieldset/div/button</value>
      <webElementGuid>40f36180-eb8d-4b72-907b-a7e2b0f1f224</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                            
                            Go
                        ' or . = '
                            
                            Go
                        ')]</value>
      <webElementGuid>6bcaf0a8-f37d-46c4-904a-38dd5a6cc015</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
