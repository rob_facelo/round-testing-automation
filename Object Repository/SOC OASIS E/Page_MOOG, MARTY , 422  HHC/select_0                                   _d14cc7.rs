<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _d14cc7</name>
   <tag></tag>
   <elementGuidId>3099e13c-407c-45e8-a9c0-ff9d6845dc16</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M2010 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2010']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>6854d9d6-1ada-480d-9eb3-0b27c1a0ceec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>88c69d9c-973a-4106-9466-548fa8f03720</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2010_HIGH_RISK_DRUG_EDCTN</value>
      <webElementGuid>bac11111-d407-49a0-a9d1-a45605cb1283</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2010_HIGH_RISK_DRUG_EDCTN</value>
      <webElementGuid>7aceef76-6844-4d03-a32a-68e5ae0adbf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2010_HIGH_RISK_DRUG_EDCTN', oasis.oasisDetails.M2010_HIGH_RISK_DRUG_EDCTN)</value>
      <webElementGuid>47e9bf0a-0703-4428-8108-329a66947ffd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2000_DRUG_RGMN_RVW == '9' || oasis.isLocked</value>
      <webElementGuid>a8ab5638-55e4-47e8-a430-9c5c54fa90ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    NA
                                </value>
      <webElementGuid>11ca14b4-fae7-4e34-b5de-8350a9e64def</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2010&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>457a5f55-e6ef-49b0-9475-c7e2427ba5e2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2010']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>f66f4eb7-d792-4c14-af21-9b4df5ad70e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[54]/following::select[1]</value>
      <webElementGuid>ac87542c-bcdb-4f8b-afc7-e5cbf6e9fe69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[20]/following::select[1]</value>
      <webElementGuid>3b792bfb-c1f6-457a-94e9-5eb93fbe401c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No'])[33]/preceding::select[1]</value>
      <webElementGuid>944d7826-d9bf-4f64-80d5-29498f9b8c95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[21]/preceding::select[1]</value>
      <webElementGuid>532b27a4-0b83-4af8-96d5-47ac535ef931</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[14]/div/div[2]/div[5]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>7e289bb6-57a7-4619-b3db-6798f76762bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    NA
                                ' or . = '
                                    
                                    0
                                    1
                                    NA
                                ')]</value>
      <webElementGuid>84a49aba-3a41-4b55-ba38-a1f145ac83af</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
