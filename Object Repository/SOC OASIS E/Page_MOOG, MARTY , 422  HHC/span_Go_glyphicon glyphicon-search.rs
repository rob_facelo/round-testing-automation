<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Go_glyphicon glyphicon-search</name>
   <tag></tag>
   <elementGuidId>9b32ad6a-37c0-4ed2-9acc-06b088abcaf3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/fieldset[1]/div[@class=&quot;row&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12&quot;]/span[@class=&quot;glyphicon glyphicon-search&quot;][count(. | //*[@class = 'glyphicon glyphicon-search']) = count(//*[@class = 'glyphicon glyphicon-search'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-default.btn-sm.toolbar-btn-default.font12 > span.glyphicon.glyphicon-search</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>06aadb52-5d55-4ac8-97d1-5df9115ef128</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-search</value>
      <webElementGuid>e3b383cc-af43-4f0c-9384-f6fe5c4d92f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/fieldset[1]/div[@class=&quot;row&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12&quot;]/span[@class=&quot;glyphicon glyphicon-search&quot;]</value>
      <webElementGuid>71b6c999-a116-4f56-b273-60c08b161d63</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button/span</value>
      <webElementGuid>ebdfcec2-6955-4852-a4e6-3368900b75b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div[2]/fieldset/div/button/span</value>
      <webElementGuid>5e764d5f-8b19-4142-97c7-29572f046f57</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
