<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save_glyphicon glyphicon-floppy-disk</name>
   <tag></tag>
   <elementGuidId>d22ab277-5f24-4b79-a080-0f5ea536b2b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > button.btn.btn-default.btn-sm.toolbar-btn-default.font12.ng-scope > span.glyphicon.glyphicon-floppy-disk</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/fieldset[1]/div[@class=&quot;row&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]/span[@class=&quot;glyphicon glyphicon-floppy-disk&quot;][count(. | //*[@class = 'glyphicon glyphicon-floppy-disk']) = count(//*[@class = 'glyphicon glyphicon-floppy-disk'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8969ccc5-af04-45b3-aba0-c616f13efcf7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-floppy-disk</value>
      <webElementGuid>5ef183e6-2e63-4013-a97a-37f92a828868</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-5 col-md-5 col-lg-5&quot;]/fieldset[1]/div[@class=&quot;row&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]/span[@class=&quot;glyphicon glyphicon-floppy-disk&quot;]</value>
      <webElementGuid>8584205a-f869-4a45-93bf-81348af756d4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      <webElementGuid>0a212ae2-7f62-4ff3-b868-603d0999be46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      <webElementGuid>cfc8cbc2-1bbe-42ae-afdc-0c97f10d4ea6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
