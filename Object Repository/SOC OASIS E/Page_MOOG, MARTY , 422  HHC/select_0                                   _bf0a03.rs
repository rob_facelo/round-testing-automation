<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _bf0a03</name>
   <tag></tag>
   <elementGuidId>460b8962-7d22-4cc5-bdea-de6852d9df8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='A1110']/div/div[2]/div/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
                                        
                                        0
                                        1
                                        9
                                    ' or . = '
                                        
                                        0
                                        1
                                        9
                                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f089457b-34df-489a-b67c-42691c5de482</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>0f0629a6-c804-4de7-88b8-1d347c870519</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>A1110B</value>
      <webElementGuid>965d345e-6eba-4bf8-a7e3-b105864a4c24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisE</value>
      <webElementGuid>fdb646cd-b242-4728-af01-01f49357228a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.A1110B</value>
      <webElementGuid>78c7fe40-6314-4fc5-bdd6-186dbeb8db61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('A1110B', oasis.oasisDetails.A1110B)</value>
      <webElementGuid>c6893eef-f0e8-41d0-b266-b21613bac1ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>16c9f2bd-80a8-4cf1-bbee-7a9ea64f23db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        0
                                        1
                                        9
                                    </value>
      <webElementGuid>3327584d-4beb-470e-87b2-4961f8de37d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;A1110&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>32c379c0-549d-487d-acb6-078b45b5a602</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='A1110']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>eed2fa18-2a07-48ba-ad1e-b029e4353d22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[1]/following::select[1]</value>
      <webElementGuid>53e01c53-c207-420e-a195-bab73846536a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A1110. Language'])[1]/following::select[1]</value>
      <webElementGuid>d14e9e14-1b45-40cb-8a38-e9de777a5521</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What is your preferred language?'])[1]/preceding::select[1]</value>
      <webElementGuid>f79da386-8c05-47f5-afc4-55664bcdbae9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/select</value>
      <webElementGuid>0ec7f0a9-a973-4c49-ae2d-d9c8c5470290</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        0
                                        1
                                        9
                                    ' or . = '
                                        
                                        0
                                        1
                                        9
                                    ')]</value>
      <webElementGuid>77607aca-db13-4be5-8904-425fd571603f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
