<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _c1430d_1</name>
   <tag></tag>
   <elementGuidId>90575f47-1d54-466c-87fd-2ff517a8436e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1400 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1400']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>804889a1-65a6-47dc-804e-1ec98f020c9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>b6e8a9f4-1d8b-4b5b-b3da-efd9da73debb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1400_WHEN_DYSPNEIC</value>
      <webElementGuid>4f4a36c2-380d-48a9-862b-be206293ddac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1400_WHEN_DYSPNEIC</value>
      <webElementGuid>49d17446-5fbe-4730-a92e-cb10a8169171</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1400_WHEN_DYSPNEIC', oasis.oasisDetails.M1400_WHEN_DYSPNEIC)</value>
      <webElementGuid>347f3c9c-f226-40d4-98d0-5cb805366ded</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked || (oasis.oasisDetails.J1800 == '0' &amp;&amp; oasis.oasisDetails.M0100_ASSMT_REASON !='09')</value>
      <webElementGuid>3617137d-0b35-49a8-a3be-32ce1ebca655</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                </value>
      <webElementGuid>a26baafe-4502-4fd2-9f45-e14f0d510ed3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1400&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>8a02c95a-c8c6-4dca-8598-324e07dbeaa8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1400']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>f6919cb4-8319-420e-8db8-40d19a822df3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[42]/following::select[1]</value>
      <webElementGuid>55b9c7b4-19b3-485a-aac7-d902373b988b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='here'])[9]/following::select[1]</value>
      <webElementGuid>c0ff638b-de5a-4e8d-b93f-40c93d8c7b0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient is not short of breath'])[1]/preceding::select[1]</value>
      <webElementGuid>c036243a-8531-45ec-bf74-327b929c8afe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='When walking more than 20 feet, climbing stairs'])[1]/preceding::select[1]</value>
      <webElementGuid>fa010426-7ad6-44ce-afc2-7d1f35dd32d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[11]/div/div[2]/div[5]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>a1ecb1c0-a0f4-4230-a606-5777e6b2104f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                ' or . = '
                                    
                                    0
                                    1
                                    2
                                    3
                                    4
                                    
                                ')]</value>
      <webElementGuid>f1c13289-934b-43dc-8015-ed399072977c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
