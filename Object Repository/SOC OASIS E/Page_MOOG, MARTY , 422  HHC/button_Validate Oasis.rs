<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Validate Oasis</name>
   <tag></tag>
   <elementGuidId>bb79aeec-ec9a-408a-9e42-407e0c468a7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[54]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8d1f57dc-535f-41ed-8102-b6005cf700ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d759c392-dc37-4864-a62e-bbb8da6440e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btn-sm toolbar-btn-default font12 ng-scope</value>
      <webElementGuid>f8270666-c5b6-41e7-8744-6253afaf19e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.validateOasis()</value>
      <webElementGuid>1f2243e0-fb9d-4e7e-9a01-1cc8f255bcb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>(!oasis.isLocked &amp;&amp; (oasis.oasisDetails.M0100_ASSMT_REASON != '1A' || !oasis.oasisDetails.FULL_OASIS)) &amp;&amp; !oasis.isViewOtherOasisVersion &amp;&amp;  oasis.oasisDetails.M0100_ASSMT_REASON != 'CD'</value>
      <webElementGuid>6ac9801c-344a-4e57-881e-f9118671b330</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isOnSaveState || oasis.isValidating</value>
      <webElementGuid>85bb832f-96d3-4c3e-9451-4d9130c77156</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                Validate Oasis
            </value>
      <webElementGuid>5570443a-4572-49ea-9afe-23eef0ec2768</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 header_nav_container_oasis&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]</value>
      <webElementGuid>690b6e81-343d-463f-9fde-5bf38a724402</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[54]</value>
      <webElementGuid>d588ba93-973b-416c-90b0-445c691082d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[2]</value>
      <webElementGuid>253c3059-ab37-4a20-8deb-d6e0d7b484ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Products/Agencies'])[1]/following::button[2]</value>
      <webElementGuid>ee395e69-33ee-41c6-bd23-46b31b8f2913</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PPS Calculator'])[1]/preceding::button[2]</value>
      <webElementGuid>757b8512-95a2-451e-9c50-b3d689470765</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Validate Oasis']/parent::*</value>
      <webElementGuid>d0c278d5-67b4-4c3e-8721-bda0a1554208</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset/div/button[2]</value>
      <webElementGuid>b5210df7-7f0b-424f-bf88-852f68e29173</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                
                Validate Oasis
            ' or . = '
                
                Validate Oasis
            ')]</value>
      <webElementGuid>74c39b24-0cb2-4383-8d4f-455d43e5924b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
