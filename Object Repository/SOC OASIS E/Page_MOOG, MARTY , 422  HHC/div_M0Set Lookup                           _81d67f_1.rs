<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_M0Set Lookup                           _81d67f_1</name>
   <tag></tag>
   <elementGuidId>72ec8054-dccf-451c-aa44-266bfaae1a98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row.footer_nav_container_oasis > div.col-xs-12.col-sm-12.col-md-12.col-lg-12</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>bf123f8d-2070-4571-9712-f68f5259367c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xs-12 col-sm-12 col-md-12 col-lg-12</value>
      <webElementGuid>c6712478-74d3-4c98-9474-8638278eae48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        </value>
      <webElementGuid>73e11c09-32dc-43ff-9ee9-191c9774fe80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]</value>
      <webElementGuid>a411a75b-175a-4fff-9d11-36d5ca71df7b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div</value>
      <webElementGuid>ff4059f5-a7c8-4155-9f4c-a1955608a2ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Safety Measures (goes to POC Box #15)'])[1]/following::div[17]</value>
      <webElementGuid>08023125-e4bb-48ab-96f5-f830959a5695</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rehabilitation Potential is Excellent'])[1]/following::div[21]</value>
      <webElementGuid>f01b89e8-89d8-425a-929b-32d7fde51ac3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div[7]/div</value>
      <webElementGuid>127f25f7-54e3-4796-be6e-224bd8481ba2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        ' or . = '
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                            Care Plan
                        
                    
                
            

            
                
                    LEGEND: 
                    Oasis Items
                    Oasis Supplementals
                    POC Supplementals
                    Go To Item
                
            

            
                
                    
                
            

        ')]</value>
      <webElementGuid>bc081bb1-8898-4a58-9063-591077ae75be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
