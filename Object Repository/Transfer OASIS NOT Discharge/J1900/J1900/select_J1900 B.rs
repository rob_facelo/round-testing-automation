<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_J1900 B</name>
   <tag></tag>
   <elementGuidId>fd6f9d57-ff42-437c-840f-8897bcf0331d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='J1900']/div/div[2]/div[2]/div[3]/div/div/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.J1900B' and (text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>c248037a-577d-45f5-b3ee-40520636f693</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>f32ee9f6-0681-4514-b2a8-b43308de8ad6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>J1900B</value>
      <webElementGuid>df629382-a143-4c54-922f-f5cb11d4aa9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>d75e4b50-cc11-4eb0-b4c2-a4668565d7c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1900B</value>
      <webElementGuid>1bc82262-7111-4fca-9705-67fff7f78ee3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('J1900B', oasis.oasisDetails.J1900B, 'OasisD')</value>
      <webElementGuid>8a9cd2a5-95af-4599-b3c3-56e156cbf8a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1800 == '0' || oasis.isLocked</value>
      <webElementGuid>52659ccd-cbbf-411b-9f5a-768db8f33e31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    </value>
      <webElementGuid>6295cbb1-6ce4-48b8-909d-937a610738cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;J1900&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-7 col-md-10 col-lg-10&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-1 col-lg-1 row&quot;]/div[@class=&quot;form-inline&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>88745c5c-84ab-409b-b145-16feb04a5310</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='J1900']/div/div[2]/div[2]/div[3]/div/div/select</value>
      <webElementGuid>f6018606-f750-4b79-883f-592e9cd84993</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. No injury:'])[1]/following::select[1]</value>
      <webElementGuid>a1de676d-d1fd-4d50-ad09-5d246280498f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Codes in Boxes'])[1]/following::select[2]</value>
      <webElementGuid>4d4d8663-347f-47fe-b888-448b30d970d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Injury (except major):'])[1]/preceding::select[1]</value>
      <webElementGuid>fe515806-5c77-4df6-ad88-2652f5be4c93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C. Major injury:'])[1]/preceding::select[2]</value>
      <webElementGuid>44270e0f-10a8-4ac3-ab86-24f349ef7a32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/select</value>
      <webElementGuid>ddcfd0bf-c703-48dc-b2a3-4bc17af8e57e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ')]</value>
      <webElementGuid>1e276905-d9a8-4180-8fe4-77d42f0cb067</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
