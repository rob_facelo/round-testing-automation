<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_J1900 A</name>
   <tag></tag>
   <elementGuidId>bb5b068e-3949-4baf-856a-93f490f94c62</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='J1900']/div/div[2]/div[2]/div[2]/div/div/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.J1900A' and (text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>d73b0acd-99c8-4d57-b8d8-3fef22e7f135</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>b962b60e-a9bd-4944-946c-9652fa668d8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>J1900A</value>
      <webElementGuid>efeec58e-6a37-469c-ad11-e3d5b8350acf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>68f8e803-507e-4bac-8230-43508a3b79de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1900A</value>
      <webElementGuid>2f4c5b52-6a67-4a0e-b58d-7faa3684e0b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('J1900A', oasis.oasisDetails.J1900A, 'OasisD')</value>
      <webElementGuid>4e0e995c-bdde-4bc4-93c2-cf0c8f2d3613</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1800 == '0' || oasis.isLocked</value>
      <webElementGuid>3d835adc-6ab5-476c-9c8e-0198ee5bb01c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    </value>
      <webElementGuid>55a54c20-8063-43b9-92ab-e1855c8691af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;J1900&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-7 col-md-10 col-lg-10&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-1 col-lg-1 row&quot;]/div[@class=&quot;form-inline&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>07e92aec-7562-4ab5-bca3-db75316806ca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='J1900']/div/div[2]/div[2]/div[2]/div/div/select</value>
      <webElementGuid>70173116-3f40-4e2d-8704-363212191708</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Codes in Boxes'])[1]/following::select[1]</value>
      <webElementGuid>50063958-5649-44e7-b5fc-caae6068e6b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING:'])[1]/following::select[1]</value>
      <webElementGuid>0311759e-0930-439f-a5aa-05a7cacc9bc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. No injury:'])[1]/preceding::select[1]</value>
      <webElementGuid>d90812a8-aa64-4f2b-a5bf-dfc48feb6cb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Injury (except major):'])[1]/preceding::select[2]</value>
      <webElementGuid>230240b5-7426-443d-938f-c2a174e06dfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/select</value>
      <webElementGuid>476e8345-36fd-4fad-b8df-ab6a27ba1a2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ')]</value>
      <webElementGuid>9b8551f3-f73b-4fba-af31-7e61bebbc375</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
