<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_J1900 C</name>
   <tag></tag>
   <elementGuidId>30aadad0-4585-4962-b933-63b848c0c310</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='J1900']/div/div[2]/div[2]/div[4]/div/div/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.J1900C' and (text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>42ffb3f5-0b1c-4f2d-b55e-a8cff352bb95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>4749eb3c-7607-4d40-b6ad-1fc3bd76e6e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>J1900C</value>
      <webElementGuid>d00f3686-81e0-4124-a98f-a081bf5a372c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>8a184a35-d817-4e35-b7cd-fbd28ad13b11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1900C</value>
      <webElementGuid>e7c74fee-a225-40dc-9185-d68f8d364c5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('J1900C', oasis.oasisDetails.J1900C, 'OasisD')</value>
      <webElementGuid>a245afee-8030-44d5-a587-1273e49a31fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1800 == '0' || oasis.isLocked</value>
      <webElementGuid>1e5cfe87-0993-44e0-96ce-ecff04731b5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    </value>
      <webElementGuid>908c061f-20dc-4ab2-bb80-dd17726a21b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;J1900&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-7 col-md-10 col-lg-10&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-1 col-lg-1 row&quot;]/div[@class=&quot;form-inline&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>e16577be-93ec-4059-935e-f117ddd03591</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='J1900']/div/div[2]/div[2]/div[4]/div/div/select</value>
      <webElementGuid>295c7c4f-f47e-4f0b-a62d-4a5d01cf3fb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Injury (except major):'])[1]/following::select[1]</value>
      <webElementGuid>4b04000c-a3a2-4e92-ada8-c124d29d7513</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. No injury:'])[1]/following::select[2]</value>
      <webElementGuid>add2a2c3-9e62-4a60-87eb-a80fa09095fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C. Major injury:'])[1]/preceding::select[1]</value>
      <webElementGuid>fc4de676-46fa-4300-b0dd-afd1111b5e18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M2410) To which Inpatient Facility has the patient been admitted?'])[1]/preceding::select[1]</value>
      <webElementGuid>c90b8df7-0e89-4a9a-8a32-f23742e3a518</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/select</value>
      <webElementGuid>66c001ba-d7a4-4932-ba2e-33674866ef57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ')]</value>
      <webElementGuid>5a4627ab-1807-4b7b-a346-85538bfc3cff</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
