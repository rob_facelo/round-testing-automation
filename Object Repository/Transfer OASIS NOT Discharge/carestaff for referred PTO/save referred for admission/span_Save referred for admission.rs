<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save referred for admission</name>
   <tag></tag>
   <elementGuidId>896c64d3-10d5-4145-9eb5-31059533f1a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-floppy-disk</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div/div/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0a967522-4c8e-418f-9fd6-1e1f1b5966ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon  glyphicon-floppy-disk</value>
      <webElementGuid>0166ae3a-e287-48b9-b5d0-eb8fab732ae3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PTOForm&quot;)/div[@class=&quot;col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim hhc-blue-border margin-bottom-10px&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row text-sm-modified&quot;]/div[@class=&quot;col-md-12 col-lg-12&quot;]/ng-form[@class=&quot;form-horizontal ng-pristine ng-valid-mask ng-invalid ng-invalid-validation ng-valid-maxlength ng-valid-pattern ng-valid-minlength&quot;]/div[@class=&quot;form-actions&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-9 col-lg-9&quot;]/div[@class=&quot;btn-group&quot;]/button[@class=&quot;btn btn-default&quot;]/span[@class=&quot;glyphicon  glyphicon-floppy-disk&quot;]</value>
      <webElementGuid>a772eef2-e718-40e7-95c3-556b15c828af</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PTOForm']/div/div/div[2]/div[4]/div/ng-form/div/div/div/div/button/span</value>
      <webElementGuid>2462fd35-f2c4-41c6-b64f-197eabba24ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ng-form/div/div/div/div/button/span</value>
      <webElementGuid>d63695b7-7efd-4037-856e-161ab438437e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
