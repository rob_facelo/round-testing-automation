<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _879a22</name>
   <tag></tag>
   <elementGuidId>2d9fa323-2ca8-4527-ad9a-5313b2a9e79b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='J1900']/div/div[2]/div[2]/div[2]/div/div/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-4.col-sm-4.col-md-1.col-lg-1.row > div.form-inline > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>c10bb648-dc24-46ee-8378-c3cd5f162fea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>a2fd4d19-9523-4b6e-95dc-2dcd4331fcb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>J1900A</value>
      <webElementGuid>3dd0a8e4-9499-45fe-8c15-0e2651792e3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>37515e31-8f74-4c4f-be80-e841d58912a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1900A</value>
      <webElementGuid>79ad0384-273e-41ce-b053-5ca93c66bf81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('J1900A', oasis.oasisDetails.J1900A, 'OasisD')</value>
      <webElementGuid>502860d1-42ee-44ee-b33f-7076b196cc1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1800 == '0' || oasis.isLocked</value>
      <webElementGuid>53fad7a3-15cb-4238-aec8-52aafe5b5b37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    </value>
      <webElementGuid>dd5534f2-bf04-469c-8800-339fa1c388dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;J1900&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-7 col-md-10 col-lg-10&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-1 col-lg-1 row&quot;]/div[@class=&quot;form-inline&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>1fc22424-8500-417a-8a5b-2b1bff901f4e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='J1900']/div/div[2]/div[2]/div[2]/div/div/select</value>
      <webElementGuid>e97f00e6-ef0e-465f-be32-a1447a59f898</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Codes in Boxes'])[1]/following::select[1]</value>
      <webElementGuid>dafcbb74-fa3e-4e6f-bb5c-9eb2d9bb78a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING:'])[1]/following::select[1]</value>
      <webElementGuid>0fe0f0d2-b76e-4621-8985-41d1561065b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. No injury:'])[1]/preceding::select[1]</value>
      <webElementGuid>154f74b8-17f4-446d-a3ea-bfb55cc8d452</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Injury (except major):'])[1]/preceding::select[2]</value>
      <webElementGuid>8afe823d-d83c-49e0-96f2-d0093ccc9552</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/select</value>
      <webElementGuid>493480e2-f863-43b0-a010-275183e87e6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ')]</value>
      <webElementGuid>5f23418e-770e-47b6-8dcd-1b7b6a4852b5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
