<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _488590</name>
   <tag></tag>
   <elementGuidId>8b51afab-7033-416c-b509-ceed55799f0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2410']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M2410 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>154c6a41-4057-4ac2-9ff5-9678c28413e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>1ec2cacd-6571-4c29-a2cc-ad12e12e0efa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2410_INPAT_FACILITY</value>
      <webElementGuid>8f597d28-a129-4816-b993-e1b8af780086</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2410_INPAT_FACILITY</value>
      <webElementGuid>39e393e4-5f1b-4aa4-ab48-66c72f4b96ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.clearMItemFieldsValues('M2410_INPAT_FACILITY','select',['01,M2420','02,M2420,M2430','03,M2420,M2430','04,M2420,M2430'])</value>
      <webElementGuid>d73b26b8-b216-4c44-8f41-48a0b88fbf77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>12bc9e1b-d589-447f-8c9d-b1acfc1df5e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                1
                                                2
                                                3
                                                4
                                                
                                            </value>
      <webElementGuid>09260727-b7be-45ed-bf1e-af1a03653791</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2410&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>d8ce40e5-2bbb-4209-b3a1-14935dfca285</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2410']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>7ee3d27f-d2ed-4787-934d-ea9991dbe3de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[11]/following::select[1]</value>
      <webElementGuid>b117a895-bb48-4bc8-8bed-97d388bfd649</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M2410) To which Inpatient Facility has the patient been admitted?'])[1]/following::select[1]</value>
      <webElementGuid>80eac575-c108-4ccb-b742-a96267807bc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0906'])[1]/preceding::select[1]</value>
      <webElementGuid>0eda2231-51cf-40a8-a716-65690b399eeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[6]/div/div[2]/div/div/div[2]/div[4]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>ed289b27-6e8d-4a64-bea2-04a99cfb4b7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                1
                                                2
                                                3
                                                4
                                                
                                            ' or . = '
                                                
                                                1
                                                2
                                                3
                                                4
                                                
                                            ')]</value>
      <webElementGuid>966bc213-6a9f-43e1-a7f1-274980c44ac3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
