<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_GG0170D</name>
   <tag></tag>
   <elementGuidId>4cbef7a1-06f4-4d15-b2c7-bf331daa4c34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[5]/div/div/div/div/div/div/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select.form-control.enter-code.ng-untouched.ng-valid.ng-dirty.ng-valid-parse</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>33f9486f-3af5-44ba-a03d-9a74dca08e5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-untouched ng-valid ng-dirty ng-valid-parse</value>
      <webElementGuid>e6f41bb5-a211-425b-88ad-21fffc18c91e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170D4</value>
      <webElementGuid>b850123f-fb09-47a9-b9a7-15284740789f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>1ae6251d-4b04-47fb-9622-47834ec7abb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170D4</value>
      <webElementGuid>1940e821-0751-4555-a2bd-c529a5c104d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0170D4', oasis.oasisDetails.GG0170D4)</value>
      <webElementGuid>dcc28cea-f1ae-4b34-a14a-a29eaeb6e132</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>1ce6ef5d-0eeb-4682-9c3e-2a30f8978ee9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        </value>
      <webElementGuid>730f46d8-0c46-49b1-a971-f3bb10bf5c70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-11 col-sm-11 col-md-11 col-lg-11&quot;]/div[@class=&quot;col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-untouched ng-valid ng-dirty ng-valid-parse&quot;]</value>
      <webElementGuid>0b97acd0-a682-4e2e-b650-20f1ac7d6913</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[5]/div/div/div/div/div/div/select</value>
      <webElementGuid>2f601306-c159-4f5e-a640-bb62f9fa906b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C. Lying to sitting on side of bed:'])[1]/following::select[1]</value>
      <webElementGuid>92f1feaa-00d2-4e9b-91d4-a54d715d9806</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Sit to lying:'])[1]/following::select[2]</value>
      <webElementGuid>272ed489-f947-458c-b4ee-a87de821570c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='D. Sit to stand:'])[1]/preceding::select[1]</value>
      <webElementGuid>00d0c495-81b2-4f17-9c75-24cb70861bfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div/div/div/select</value>
      <webElementGuid>2831863d-5c11-44ab-be62-ef2e5b2eea9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ' or . = '
                                                                            
                                                                            01
                                                                            02
                                                                            03
                                                                            04
                                                                            05
                                                                            06
                                                                            07
                                                                            09
                                                                            10
                                                                            88
                                                                            -
                                                                        ')]</value>
      <webElementGuid>617afc56-fbc7-4a39-97d2-bec262a55dcf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
