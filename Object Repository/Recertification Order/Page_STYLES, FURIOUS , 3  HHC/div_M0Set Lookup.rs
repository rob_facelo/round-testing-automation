<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_M0Set Lookup</name>
   <tag></tag>
   <elementGuidId>a5606916-6530-47b6-bf2b-030412838d79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-3.col-md-2.col-lg-2 > div.form-inline.input-group</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>40cbbd4f-82d4-4a8e-9aec-6408562975cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-inline input-group</value>
      <webElementGuid>87256d2a-cb9d-4163-8667-28ee0e715e20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    M0Set Lookup:
                    
                </value>
      <webElementGuid>8dad7679-6fc9-4b13-bd04-e261a34a8525</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-3 col-md-2 col-lg-2&quot;]/div[@class=&quot;form-inline input-group&quot;]</value>
      <webElementGuid>94deb6e8-0a85-43b4-a9d5-412e2aac238b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div/div</value>
      <webElementGuid>ebe0257e-56ea-43ce-a922-5b598835d4bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Safety Measures (Form 485 - Locator #15)'])[1]/following::div[19]</value>
      <webElementGuid>cf09f32a-94a5-4ee0-9165-bc9b9973d10c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Any changes since last assessment:'])[1]/following::div[22]</value>
      <webElementGuid>65d65e24-8da3-4f81-b095-ebea0d862fff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[7]/div/div/div</value>
      <webElementGuid>6f8014b0-6237-420f-8a4a-58a7f485d093</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    M0Set Lookup:
                    
                ' or . = '
                    M0Set Lookup:
                    
                ')]</value>
      <webElementGuid>1c1c69d1-7056-4900-9372-2fc59563b13c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
