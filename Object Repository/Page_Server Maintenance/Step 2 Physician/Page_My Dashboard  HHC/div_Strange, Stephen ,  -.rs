<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Strange, Stephen ,  -</name>
   <tag></tag>
   <elementGuidId>f36ae00a-6980-4f34-b28b-5362c18d962c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;intakeWizardPhys&quot;)/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;][count(. | //div[(text() = '
                        Strange, Stephen ,  - 
                    ' or . = '
                        Strange, Stephen ,  - 
                    ')]) = count(//div[(text() = '
                        Strange, Stephen ,  - 
                    ' or . = '
                        Strange, Stephen ,  - 
                    ')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//autocomplete-directive[@id='intakeWizardPhys']/div/ul/li/a/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.col-lg-12.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>346267c0-e178-4095-89db-88ab5279021c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 col-lg-12 ng-binding</value>
      <webElementGuid>76e8983d-78f0-48b0-bd76-e61d8515a5d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Strange, Stephen ,  - 
                    </value>
      <webElementGuid>4d78704a-5c2a-46af-a7d9-c35b0eb8865d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;intakeWizardPhys&quot;)/div[@class=&quot;dropdown&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-lg-12 ng-binding&quot;]</value>
      <webElementGuid>4609646b-96fa-4a2a-a5ea-5248e216fb6d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//autocomplete-directive[@id='intakeWizardPhys']/div/ul/li/a/div/div</value>
      <webElementGuid>e6502bdc-0864-4439-bea2-b729d9b9b235</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[14]/following::div[4]</value>
      <webElementGuid>88747fe6-2872-4bd4-8a47-ba9abbaab494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Start of Care - Referred for Admission Order'])[1]/following::div[8]</value>
      <webElementGuid>439c77eb-b5d5-4eec-a27d-cb6780d944fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No record found'])[43]/preceding::div[3]</value>
      <webElementGuid>a6097e38-6424-49bd-ae45-9100d82d2e0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a/div/div</value>
      <webElementGuid>97887f75-f89f-4a1c-9994-6b21aa227874</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Strange, Stephen ,  - 
                    ' or . = '
                        Strange, Stephen ,  - 
                    ')]</value>
      <webElementGuid>66408e4b-0d46-47e9-ac0e-0e773ddfa9b0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
