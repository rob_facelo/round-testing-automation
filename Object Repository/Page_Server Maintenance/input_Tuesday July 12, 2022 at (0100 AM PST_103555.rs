<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Tuesday July 12, 2022 at (0100 AM PST_103555</name>
   <tag></tag>
   <elementGuidId>79c17b40-389c-49aa-b343-3797ef9ef1a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#btnContinue</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='btnContinue']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>cc0f6b16-0a1c-4b88-b573-531b12c8f463</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>17fe904f-3345-44af-bea2-6a659376a1a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>btnContinue</value>
      <webElementGuid>6a4d2d7b-8ba4-4110-9177-c3f81fd1d40e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>218b0080-e3b4-4fb4-99bf-5e3fb684cf92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnContinue</value>
      <webElementGuid>99af60e9-a206-4cc0-bb73-e5a0d7707bfc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>LogoffButton</value>
      <webElementGuid>5f97cee1-3c45-49b5-8b64-fbc2e5e12728</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnContinue&quot;)</value>
      <webElementGuid>be0ef97a-320a-4b0c-85ab-4bb168d6d264</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='btnContinue']</value>
      <webElementGuid>edb20fdd-2bd9-4436-988e-a8a2654da07d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='formBox']/div[2]/input</value>
      <webElementGuid>32293990-450e-4590-a9f7-14b82e423c2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/input</value>
      <webElementGuid>764754d2-c0ed-40d0-b342-faa19f0fa1fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'btnContinue' and @id = 'btnContinue']</value>
      <webElementGuid>cd0b85ac-41da-41e4-8226-d8b1f6e1ebfe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
