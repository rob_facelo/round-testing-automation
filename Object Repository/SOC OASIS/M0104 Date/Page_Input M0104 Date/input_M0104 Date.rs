<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_M0104 Date</name>
   <tag></tag>
   <elementGuidId>6707d4c3-0002-4ecb-bffe-83ba79cb83fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[74]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M0104 > div.row > div.col-xs-12.col-sm-8.col-md-8.col-lg-8.bottom-space > div.col-xs-12.col-sm-6.col-md-6.col-lg-6.row > div.col-xs-12.col-sm-11.col-md-11.col-lg-11.row > div.form-inline.input-group.date.row > input.form-control.input-sm-modified.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlength</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>001e4d2a-5b0b-4622-8bfa-f7b0993e7d99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>34fcc8f0-ad4b-4c1a-8693-a7c5807f2bc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm-modified ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>4b38d856-d85f-471e-a8a9-29beb921d6b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeyup</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,false,'1')</value>
      <webElementGuid>dbdd53c7-625f-471d-8b32-fb2966fa026c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>DateFormat(this,this.value,event,true,'1')</value>
      <webElementGuid>5cbabe02-ebd6-431f-bee1-d1d539648fc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M0104_PHYSN_RFRL_DT</value>
      <webElementGuid>68de0022-98da-46b7-bb27-e431fc745373</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>date</name>
      <type>Main</type>
      <value>MM/dd/yyyy</value>
      <webElementGuid>0e27556b-3657-4e9b-a463-11f8891669c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M0104_PHYSN_RFRL_DT', oasis.oasisDetails.M0104_PHYSN_RFRL_DT)</value>
      <webElementGuid>4f0903a3-9dd1-4f22-86a3-c9468ffc8b76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>4a1b1304-e4fd-4d58-bb12-5ac2e0b23b01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>75ff1b54-b761-4265-a093-2e94e379a7ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M0104&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-8 col-md-8 col-lg-8 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6 row&quot;]/div[@class=&quot;col-xs-12 col-sm-11 col-md-11 col-lg-11 row&quot;]/div[@class=&quot;form-inline input-group date row&quot;]/input[@class=&quot;form-control input-sm-modified ng-pristine ng-untouched ng-valid ng-valid-maxlength&quot;]</value>
      <webElementGuid>41695f69-a811-4735-a6bd-b27d848dd5ed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[74]</value>
      <webElementGuid>6accf7b6-59cf-4106-b366-98c2458c16de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M0104']/div/div[2]/div[2]/div/div/input</value>
      <webElementGuid>619c7ef5-fdbb-4ef0-87dc-49ea1e954b5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]/div[2]/div/div/input</value>
      <webElementGuid>cfd0fde0-704b-4284-92f0-299a6c7241c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>9a7e9f9b-5746-4fd0-adb1-7bfcb73c3540</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
