<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Restrictions_btnAgree</name>
   <tag></tag>
   <elementGuidId>7b03e21d-807a-41c5-8729-04ae9b755b1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='btnAgree']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#btnAgree</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>dbfea316-19ef-42e4-bad0-aec5e8e81e2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>8351b5cf-3a23-4e64-9aec-f98b983d4dbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>btnAgree</value>
      <webElementGuid>ee47bc3b-eee3-459b-b816-a3e55579e1d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>I/We Agree</value>
      <webElementGuid>b15fbc48-40f6-472b-848f-2429fea66753</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>clearUserLogsStorage();</value>
      <webElementGuid>6682ecd1-e93a-4ea8-87a9-0c3aac1a4b6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnAgree</value>
      <webElementGuid>58c47b8c-beb4-4874-a4c7-2188379c9f5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>agree-btn form-control</value>
      <webElementGuid>65999ef2-16a8-4ac2-ae4b-4c8158983de6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnAgree&quot;)</value>
      <webElementGuid>bdaef2e3-c037-4081-897e-0685b51611cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='btnAgree']</value>
      <webElementGuid>29430033-fd25-4c8e-bcfc-cda47b977317</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]/div[2]/div/div[3]/input</value>
      <webElementGuid>1255bb13-9845-49d6-b86e-67e2d0c3bc3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/input</value>
      <webElementGuid>32e12c0e-d0ca-4e9b-9195-58b2f5155799</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'btnAgree' and @id = 'btnAgree']</value>
      <webElementGuid>4982b591-852f-4f38-834d-b2153fe9dc2c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
