<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0170Q1</name>
   <tag></tag>
   <elementGuidId>ca255a3f-28ce-4695-bd7b-dc54c433aa77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-4.col-sm-4.col-md-2.col-lg-2 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[17]/div/div[2]/div/div/div/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0170Q1' and (text() = '
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    ' or . = '
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a9640695-1891-4acc-8878-a164e435f48d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>6437af85-409c-45e9-a2b7-bcbe005f8e0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170Q1</value>
      <webElementGuid>db621326-f7fc-445b-a5eb-1990c80a75ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>8c15de06-7b28-40e9-b197-51e1f9b746d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170Q1</value>
      <webElementGuid>bcee26a1-8a2b-4e37-94da-f51bfe8f74bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.clearMItemFieldsValues('GG0170Q1','select',['0,GG0170R1,GG0170R2,GG0170RR1,GG0170S1,GG0170S2,GG0170SS1'])</value>
      <webElementGuid>d089a905-d609-42ab-93b6-47eb3789d2f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>0a469576-dddc-4e3b-ac51-79860b019ce0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    </value>
      <webElementGuid>636c6d1b-2654-464f-8a17-5ae2cf06ee0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-8 col-sm-8 col-md-9 col-lg-9&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-2 col-lg-2&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>47208196-5561-4cb7-be02-14bb339c841d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170']/div/div[2]/div/div[2]/div[17]/div/div[2]/div/div/div/select</value>
      <webElementGuid>d2ffa58c-9d01-46dc-8d37-83d00e6b0d26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='P. Picking up object:'])[1]/following::select[1]</value>
      <webElementGuid>4a0f7820-0832-49ad-83a9-6da538080a9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='O. 12 steps:'])[1]/following::select[3]</value>
      <webElementGuid>2f5d7a0b-9fc8-45b9-b413-1e2f7772831f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Q. Does patient use wheelchair/scooter?'])[1]/preceding::select[1]</value>
      <webElementGuid>e2a4d475-a8e6-4d49-95b9-d7c037b14ce4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/select</value>
      <webElementGuid>84d6aac3-544a-4b6a-8432-9066debd307e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    ' or . = '
                                                                        
                                                                        0
                                                                        1
                                                                        -
                                                                    ')]</value>
      <webElementGuid>b9a9b070-1708-4bc1-944b-75b4b46cdaba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
