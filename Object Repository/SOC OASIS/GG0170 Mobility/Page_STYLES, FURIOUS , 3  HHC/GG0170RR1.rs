<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0170RR1</name>
   <tag></tag>
   <elementGuidId>ef3acf15-8d52-41d9-9c37-b976f971792a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#GG0170RR1 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170RR1']/div/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0170RR1' and (text() = '
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    ' or . = '
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>0686b1a8-18fd-482d-9a51-1e92d0c671cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>9a485147-1671-48d3-9a7c-874ead2afc2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170RR1</value>
      <webElementGuid>30addc14-9d78-425e-bee6-0c555845396e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>6b5ab112-7c15-4369-84f4-8c7ff02d71d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170RR1</value>
      <webElementGuid>114dec05-05d4-4d3c-b7b7-957f5a4c1ada</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0170RR1', oasis.oasisDetails.GG0170RR1)</value>
      <webElementGuid>d1cb2180-acb6-4487-8278-b041b8dd5394</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170Q1 == '0' || oasis.isLocked</value>
      <webElementGuid>f48ecea9-1e98-4a2f-9853-203158c71e9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    </value>
      <webElementGuid>32e67d82-65b8-46c4-827c-65dae85e9a4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170RR1&quot;)/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>b36e0493-4c84-4c9c-9084-03ebde79333a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170RR1']/div/select</value>
      <webElementGuid>6c9ced0d-fbb3-4677-88d6-b40d10c22ab8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='R. Wheel 50 feet with two turns:'])[1]/following::select[1]</value>
      <webElementGuid>3b54b20d-ef3d-4ea3-a5bc-10cef86837ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RR1. Indicate the type of wheelchair or scooter used.'])[1]/preceding::select[1]</value>
      <webElementGuid>3e02284f-6550-40dd-ad3f-bc365312fd61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='S. Wheel 150 feet:'])[1]/preceding::select[3]</value>
      <webElementGuid>0b475413-cb15-4a62-be14-dffa81639f31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[19]/div/div[2]/div/div/div/select</value>
      <webElementGuid>27aad58c-57a8-4419-a506-4efa2ab4c921</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    ' or . = '
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    ')]</value>
      <webElementGuid>4aeed20e-1465-40df-9791-2164b2a72503</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
