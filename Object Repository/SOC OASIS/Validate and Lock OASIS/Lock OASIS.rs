<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lock OASIS</name>
   <tag></tag>
   <elementGuidId>90b42eb7-f692-4e2a-aa70-d36a5ea36252</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'lockOasis(lockDate)' and (text() = 'Lock OASIS' or . = 'Lock OASIS')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[166]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-dialog.modal-sm > div.modal-content > div.modal-footer > button.btn.btn-success</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>fa7deaf1-d308-4aa5-9b53-884e43d3b564</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>911fbebf-cce4-4c22-a833-9f6594528b8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>lockOasis(lockDate)</value>
      <webElementGuid>bb8153ed-a72e-4616-afb4-c9cff3e25672</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-success</value>
      <webElementGuid>1e8595c1-c3d3-44e4-9f60-4d03bcc3aba0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lock OASIS</value>
      <webElementGuid>f420a862-ce31-481e-b15c-1bd961cbbcdf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer&quot;]/button[@class=&quot;btn btn-success&quot;]</value>
      <webElementGuid>1ea39308-8d9d-4f41-a5be-50366093e4a1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[166]</value>
      <webElementGuid>dc38f6bc-e169-456b-958f-ad88ec35ea0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[1]/following::button[1]</value>
      <webElementGuid>6806e75e-2353-4907-b5be-6482d63bb5cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Oasis'])[1]/following::button[1]</value>
      <webElementGuid>5679acac-3a63-40e2-9734-bd1cc338774f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[10]/preceding::button[1]</value>
      <webElementGuid>744a4993-4827-43a1-bbdf-d6a203665558</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lock OASIS']/parent::*</value>
      <webElementGuid>93e83c4a-337c-4356-9303-ae8a2b08b58f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div[4]/button</value>
      <webElementGuid>934de19f-0dae-4e5b-894a-1171035bd5b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Lock OASIS' or . = 'Lock OASIS')]</value>
      <webElementGuid>00b7187d-9d97-4d18-a134-ff37d1e37521</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
