<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Lock Oasis</name>
   <tag></tag>
   <elementGuidId>858a3f0b-fd04-4d84-9045-83fc6e7cdd9f</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @ng-click = 'oasis.lockOasis()' and (text() = '
                
                Lock Oasis
            ' or . = '
                
                Lock Oasis
            ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[55]</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>efe093e7-6abe-42ae-a18d-44d6e1f5f296</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6a1ac509-7262-4146-bfca-b9d3fedc042e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btn-sm toolbar-btn-default font12 ng-scope</value>
      <webElementGuid>1ad219a1-4408-4e61-b0fc-ad75ad560fbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.lockOasis()</value>
      <webElementGuid>cb028b88-ea2d-4e6a-859c-3d0f0f088b65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>(!oasis.isLocked &amp;&amp; ((oasis.isInUse &amp;&amp; oasis.oasisDetails.inuse_by_user_id == oasis.userCredentials.UserId) || !oasis.isInUse)) &amp;&amp; !oasis.isViewOtherOasisVersion</value>
      <webElementGuid>a70631c1-0b8d-41a1-a685-1f6875fb6ed0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>!oasis.permissions.LockUnlock || oasis.isOnSaveState || oasis.isValidating</value>
      <webElementGuid>423b075b-9c0d-4799-a668-f58abab234e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                Lock Oasis
            </value>
      <webElementGuid>61f89934-ef8b-42ce-9885-b31eb28b7061</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 header_nav_container_oasis&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]</value>
      <webElementGuid>1a676a4a-3bb7-47e4-a039-42514a1a7c20</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[55]</value>
      <webElementGuid>2adf452f-9e38-4b21-9e47-61c5bdbc3f0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/fieldset/div/button[3]</value>
      <webElementGuid>98a8384f-d33b-4162-bd41-bf616575a6b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PPS Calculator'])[1]/preceding::button[1]</value>
      <webElementGuid>ca2959f3-7e05-4a54-be4a-ae15b061872e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDGM Calculator'])[1]/preceding::button[2]</value>
      <webElementGuid>9531d2d9-62f1-4042-a3f0-80ea2009e3cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lock Oasis']/parent::*</value>
      <webElementGuid>00d25d93-8ca1-44a6-95b6-779a4a85cca7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]</value>
      <webElementGuid>93916d9a-d6ad-40cd-a18a-80c238df35bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                
                Lock Oasis
            ' or . = '
                
                Lock Oasis
            ')]</value>
      <webElementGuid>6a514110-7828-45df-8838-c332fa5138b0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
