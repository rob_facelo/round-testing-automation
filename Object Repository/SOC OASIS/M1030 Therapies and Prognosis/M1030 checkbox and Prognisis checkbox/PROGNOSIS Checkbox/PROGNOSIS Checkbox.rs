<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PROGNOSIS Checkbox</name>
   <tag></tag>
   <elementGuidId>06a8628c-53a2-4d9b-a2df-3b8646d77ba9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[135]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.space.ng-scope > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > input.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.POCPrognosisLoc20']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>285c0830-d3c0-4328-88c4-590a9e13d432</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>1dc3936f-9b13-4a8d-a672-a341bc20f065</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.POCPrognosisLoc20</value>
      <webElementGuid>e550fabe-173b-4d1e-ad1b-242fe7bd6970</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>531a19b7-1d7f-4be8-9ebf-ebe33bfc78da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('POCPrognosisLoc20', oasis.oasisDetails.POCPrognosisLoc20)</value>
      <webElementGuid>0d71fd1c-12fd-40b2-ac72-a365d4533e3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked &amp;&amp; (oasis.poc_readonly || !oasis.oasisDetails.UNLOCKADDENDUM)</value>
      <webElementGuid>7b074d02-ddf6-415e-82e3-fe9b81737043</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>566837d0-7593-4027-be4f-de91eea971ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;testtest&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[@class=&quot;space ng-scope&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>60e4d1b0-f9a3-4584-9bbc-4445d0ffe9f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[135]</value>
      <webElementGuid>2f50bf2c-7122-4f6c-8ed7-10583e56336e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='testtest']/div/div[2]/div[11]/div[2]/div/input</value>
      <webElementGuid>52f73c67-dbec-44f6-887e-5202994937bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/div[2]/div/input</value>
      <webElementGuid>df86147e-ba27-497c-82cd-eb03913fe83a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>ec0bdbbe-3fda-4e00-ab7b-8c38c9fe0d59</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
