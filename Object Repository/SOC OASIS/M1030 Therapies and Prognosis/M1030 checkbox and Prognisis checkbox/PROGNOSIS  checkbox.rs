<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PROGNOSIS  checkbox</name>
   <tag></tag>
   <elementGuidId>d45bd77d-e148-4e86-a8f7-c9465c836b7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[140]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M1033 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > input.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1032_HOSP_0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2f4505e2-4b4a-4e6d-9be7-025ee9e4cdfb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>3a90a528-82e9-41fd-9a1c-f8ba2def28a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1032_HOSP_0</value>
      <webElementGuid>27eca668-2b78-420c-93f7-dd1d69c97b14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1032_HOSP_0</value>
      <webElementGuid>55a1b67e-6764-46da-ab06-70bbebdf134b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>'1'</value>
      <webElementGuid>2a3eab12-ffa2-435d-9b39-862b56b33878</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-false-value</name>
      <type>Main</type>
      <value>'0'</value>
      <webElementGuid>f2c92f73-819c-4917-826a-5d301a777f2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>oasis.clearMItemField([$event,'M1032_HOSP_9'],false)</value>
      <webElementGuid>bfdc8549-a988-4012-b3c7-10900477a021</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>c6e3489f-9e2d-441d-9be1-5537ec7fdce0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>735cb12e-b398-4f66-a7e7-2090dcd9e896</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1033&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/input[@class=&quot;ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>8042d538-ca8c-4302-a662-c1f2d53eca25</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[140]</value>
      <webElementGuid>84622ec3-0c28-47a6-914b-77d202943e92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1033']/div/div[2]/div/input</value>
      <webElementGuid>34fe12b2-4e06-43e2-99b1-84193ff0ddf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/div/div/div[2]/div/input</value>
      <webElementGuid>44b390c8-3c97-4a92-a66e-0730dd303af6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>33b6f3db-d2e7-4adf-bb43-905138e6456c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
