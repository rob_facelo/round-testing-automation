<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1860</name>
   <tag></tag>
   <elementGuidId>790fbce1-06ec-4278-aa40-eedcc3f6d219</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1860 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1860']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1860_CRNT_AMBLTN' and (text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>41b09d15-1de1-4ebf-8f61-e44d73fa3990</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>3575f4c8-feb3-4c54-8b05-7ebafaa7d2a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1860_CRNT_AMBLTN</value>
      <webElementGuid>c246c907-9a6a-47b2-8ae4-4b28c766d511</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1860_CRNT_AMBLTN</value>
      <webElementGuid>bb31440f-62e2-4b68-b314-e1722bc2a602</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1860_CRNT_AMBLTN', oasis.oasisDetails.M1860_CRNT_AMBLTN)</value>
      <webElementGuid>aba91bdc-6e4d-4509-b097-3e50bb7bbdb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>f5e9b772-d90a-4938-b79d-fbc9ca96431a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            </value>
      <webElementGuid>3bf40474-90c0-4fb9-ac27-3be09be8c913</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1860&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>a46ba45a-b954-4907-bcba-fc8442f55b1e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1860']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>862f7a7f-3840-49ea-973b-4c5518d36979</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[31]/following::select[1]</value>
      <webElementGuid>e034a231-45dd-4f78-ad80-622244fa5fb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SS1. Indicate the type of wheelchair or scooter used.'])[1]/following::select[1]</value>
      <webElementGuid>c0c35c61-0d10-4c9c-bd4c-1878abc232af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to walk only with the supervision or assistance of another person at all times.'])[1]/preceding::select[1]</value>
      <webElementGuid>d39b8985-bb57-4a87-b655-380679037acc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chairfast, unable to ambulate but is able to wheel self independently.'])[1]/preceding::select[1]</value>
      <webElementGuid>d480382f-09a9-44f2-8933-bbb0dd666892</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[12]/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>47e33a74-def8-4961-bcdb-253f6a4077ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                6
                                            ')]</value>
      <webElementGuid>552d3f76-ca01-4435-a8b3-fd31e73593e2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
