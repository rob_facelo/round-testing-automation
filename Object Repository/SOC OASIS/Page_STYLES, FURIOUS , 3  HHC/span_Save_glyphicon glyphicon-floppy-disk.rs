<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Save_glyphicon glyphicon-floppy-disk</name>
   <tag></tag>
   <elementGuidId>c6f3bcc6-9f23-4e37-aa8e-f2877c60f8ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-6.col-md-6.col-lg-6 > button.btn.btn-default.btn-sm.toolbar-btn-default.font12.ng-scope > span.glyphicon.glyphicon-floppy-disk</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[@class = 'glyphicon glyphicon-floppy-disk']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>00828f54-05a5-425c-9fbf-0e0fcdbea325</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-floppy-disk</value>
      <webElementGuid>38d6132b-03a8-41d2-83e3-58e97f0d0ace</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12 ng-scope&quot;]/span[@class=&quot;glyphicon glyphicon-floppy-disk&quot;]</value>
      <webElementGuid>db34615e-61c9-426c-92a1-0151da5a1031</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      <webElementGuid>447a9749-8bea-493f-8402-8f9359744b45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div[2]/fieldset/div/button[2]/span</value>
      <webElementGuid>696a4746-cfc5-4aa7-9bc9-3a77cdcac61e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
