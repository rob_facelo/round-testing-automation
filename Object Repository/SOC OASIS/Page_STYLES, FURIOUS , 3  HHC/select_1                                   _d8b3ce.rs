<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _d8b3ce</name>
   <tag></tag>
   <elementGuidId>baabd047-df37-4407-a38f-fa89b5d21e26</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0170RR1']/div/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#GG0170RR1 > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a8c2f320-95b0-4bac-b36b-cc27225904e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>1380223f-8c89-4a0e-ae30-e069d06ceb73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0170RR1</value>
      <webElementGuid>fdd5afec-a7e6-4671-9232-70bb05bbb71c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>1779c306-2667-47a7-b47d-8df7b446c56d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170RR1</value>
      <webElementGuid>7c0dca23-7174-4985-aaca-a61969dff76a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0170RR1', oasis.oasisDetails.GG0170RR1)</value>
      <webElementGuid>868cfb29-fcef-4f9b-b912-de99c43a67da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0170Q1 == '0' || oasis.isLocked</value>
      <webElementGuid>ef10c2ee-ac3f-4d53-9ee0-0b90d3e781f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    </value>
      <webElementGuid>45ac87c9-1fce-4cfe-9090-c66e7f293fed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0170RR1&quot;)/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>c0fbfaa1-9503-4e7a-a7d5-9c403b4c3a42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0170RR1']/div/select</value>
      <webElementGuid>5becd92d-97ac-46af-926e-dc14e5f4637e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='R. Wheel 50 feet with two turns:'])[1]/following::select[1]</value>
      <webElementGuid>87dd7b41-db50-416b-b0d6-f4aa82514f77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RR1. Indicate the type of wheelchair or scooter used.'])[1]/preceding::select[1]</value>
      <webElementGuid>094eb3cf-9d00-4678-9b33-ba064995d7d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='S. Wheel 150 feet:'])[1]/preceding::select[3]</value>
      <webElementGuid>19afc0dc-81dd-46ed-acf2-b02ddcf86414</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[19]/div/div[2]/div/div/div/select</value>
      <webElementGuid>f05ff9e5-d496-417a-954d-e505f11164b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    ' or . = '
                                                                        
                                                                        1
                                                                        2
                                                                        -
                                                                    ')]</value>
      <webElementGuid>9a00f824-b3c3-41b4-8e5e-fce6322bf5f3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
