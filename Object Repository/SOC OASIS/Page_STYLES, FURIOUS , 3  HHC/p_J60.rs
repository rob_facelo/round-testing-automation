<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_J60</name>
   <tag></tag>
   <elementGuidId>79e07c08-8739-4464-9221-815f2b56f06b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//autocomplete-directive[@id='M0240_OTH_DIAG1_ICD']/div/div[2]/ul/li/a/div/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M0240_OTH_DIAG1_ICD > div.dropdown > div.col-xs-12.col-sm-12.col-md-12.col-lg-12 > ul.dropdown-menu.autocomplete-directive > li.ng-scope > a > div.row > p.text-primary.ng-binding</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;M0240_OTH_DIAG1_ICD&quot;)/div[@class=&quot;dropdown&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/p[@class=&quot;text-primary ng-binding&quot;][count(. | //*[(text() = 'J60       ' or . = 'J60       ')]) = count(//*[(text() = 'J60       ' or . = 'J60       ')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>5201a0ce-5448-4f0b-b3a3-7a3147031fc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-primary ng-binding</value>
      <webElementGuid>eea0db9f-d057-43d0-aacc-7801d3d3e016</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>J60       </value>
      <webElementGuid>4019ae03-9d96-4774-9abd-5657fa9dd714</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M0240_OTH_DIAG1_ICD&quot;)/div[@class=&quot;dropdown&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/ul[@class=&quot;dropdown-menu autocomplete-directive&quot;]/li[@class=&quot;ng-scope&quot;]/a[1]/div[@class=&quot;row&quot;]/p[@class=&quot;text-primary ng-binding&quot;]</value>
      <webElementGuid>ab5ee151-e3f4-4b67-b518-10c79473defb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//autocomplete-directive[@id='M0240_OTH_DIAG1_ICD']/div/div[2]/ul/li/a/div/p</value>
      <webElementGuid>2677abf0-3b6c-442c-9705-ca4d573d61e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='b.'])[2]/following::p[1]</value>
      <webElementGuid>31f4fc3e-3d0d-423d-8192-66e206230879</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All ICD-10–CM codes allowed'])[1]/following::p[1]</value>
      <webElementGuid>b904b2d3-6b11-4704-8817-f45c0ed6ab1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='c.'])[1]/preceding::p[20]</value>
      <webElementGuid>a87ddb23-83fb-4215-bbb8-3a00c40f0e45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[6]/td[2]/div[2]/div/autocomplete-directive/div/div[2]/ul/li/a/div/p</value>
      <webElementGuid>1f258e0d-1760-41b6-9b94-a339e2079dd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'J60       ' or . = 'J60       ')]</value>
      <webElementGuid>69d6debb-61dc-428a-8548-befe9295f6ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
