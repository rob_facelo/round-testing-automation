<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _eba846_1</name>
   <tag></tag>
   <elementGuidId>b4886240-fe4d-4f92-97c7-df7f143efcfb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1700']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M1700 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>500e0b99-bde9-4b65-b87e-681cbf2afbec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>8207ca0e-25d6-4d1e-a53e-3a6d1a0835ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1700_COG_FUNCTION</value>
      <webElementGuid>796d8712-1c56-437b-aeae-b91e9aa9c78c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1700_COG_FUNCTION</value>
      <webElementGuid>5fbf6cd3-e749-4b61-8f8e-d33eb7e5beb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1700_COG_FUNCTION', oasis.oasisDetails.M1700_COG_FUNCTION)</value>
      <webElementGuid>e46a969d-6d13-4db6-b8c1-7370bf38cfe8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>2ab5507d-2bf7-4df4-94a4-d5ef0bfed392</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            </value>
      <webElementGuid>31e56f27-8d41-4f61-8d53-bb3743a1600e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1700&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>c1ccbdd6-b3f2-4d9b-b03c-606a837c7772</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1700']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>17a1d5c7-1aae-4bcf-a222-9f18846f31bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[19]/following::select[1]</value>
      <webElementGuid>835dba99-b476-4c02-b6d1-eca857578004</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('How does patient', &quot;'&quot;, 's condition affect their functional ability and safety?')])[1]/following::select[1]</value>
      <webElementGuid>409ff0b9-91b8-4215-9952-67460042afbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.'])[1]/preceding::select[1]</value>
      <webElementGuid>989fc680-307a-4007-83e2-897ddb1952ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cognitive Functioning Narrative'])[1]/preceding::select[1]</value>
      <webElementGuid>878d5bd6-497d-4792-b009-cd7dbb94b9b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[9]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>a1cc7ed2-0724-4898-a947-e1902c6e8e20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ')]</value>
      <webElementGuid>579f85f9-fb98-4249-ba41-55c60fb8cf67</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
