<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_07012022 - 08292022</name>
   <tag></tag>
   <elementGuidId>09d121a1-b198-43ef-8741-f792339e9ead</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='OasisPlan']/div/div/table/tbody/tr/td/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.fontcolorDark13Bold.cursorPointer.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>09e97524-272e-4414-b304-4a087723ba75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fontcolorDark13Bold cursorPointer ng-binding</value>
      <webElementGuid>8f4843e1-8224-49d8-86a8-11f5b01b1f1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>../../Oasis/Oasis.aspx?oasisId=72521</value>
      <webElementGuid>0970619d-90f8-4b14-a135-fdfaaf544506</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>07/01/2022 - 08/29/2022</value>
      <webElementGuid>4d42f5e3-848a-4981-a0a6-48b0aaf53d73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;OasisPlan&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[@class=&quot;ng-scope&quot;]/tr[@class=&quot;bgLightskyblue&quot;]/td[@class=&quot;tdFlatPadding13 verticalAlignMiddle&quot;]/a[@class=&quot;fontcolorDark13Bold cursorPointer ng-binding&quot;]</value>
      <webElementGuid>3fd5187e-0b06-4633-befc-de71e304d080</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='OasisPlan']/div/div/table/tbody/tr/td/a</value>
      <webElementGuid>b8f3b759-dee6-42c8-b836-116e2bc94df2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'07/01/2022 - 08/29/2022')]</value>
      <webElementGuid>4f75114d-1e18-4753-9c07-d252c6d7964c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[2]/following::a[1]</value>
      <webElementGuid>90cff900-b8a3-474a-86c2-ed2f13c8f825</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cor #'])[1]/following::a[1]</value>
      <webElementGuid>b03bd339-4ae5-49c2-b23c-456762f8f66b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Start of care - further visits planned'])[1]/preceding::a[1]</value>
      <webElementGuid>9cae0fd0-177c-49ae-af60-9444d32bdfec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Progress'])[1]/preceding::a[1]</value>
      <webElementGuid>6f095853-c564-40f8-9a2c-73f139ec9dd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='07/01/2022 - 08/29/2022']/parent::*</value>
      <webElementGuid>7c385787-be40-4e66-abf0-0cb00d5a658b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '../../Oasis/Oasis.aspx?oasisId=72521')]</value>
      <webElementGuid>70ad4e6d-edb5-47aa-b009-a7cbb722cd24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/a</value>
      <webElementGuid>6626c6b2-ee6d-4b2a-922b-143125102281</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '../../Oasis/Oasis.aspx?oasisId=72521' and (text() = '07/01/2022 - 08/29/2022' or . = '07/01/2022 - 08/29/2022')]</value>
      <webElementGuid>78f9d10e-918d-43ff-a0c3-3e82acc7ad63</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
