<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _c0b4ec</name>
   <tag></tag>
   <elementGuidId>2581d710-419d-4104-b045-838e04859abd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1620']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M1620 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f464c1ef-91ac-4d3a-b289-ed334f9e12ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>685c6003-7860-4419-9f05-8fb49447e00d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1620_BWL_INCONT</value>
      <webElementGuid>ffc3f0f8-cac3-47ef-9f39-162c53d58451</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1620_BWL_INCONT</value>
      <webElementGuid>4993cf0d-ed4d-41ea-8d04-95fd4fab7940</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1620_BWL_INCONT', oasis.oasisDetails.M1620_BWL_INCONT)</value>
      <webElementGuid>eaf7ccc2-a0bb-487d-af4f-5e39d5f7c8c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>5828ad07-1fa3-4e97-b647-849f5c98a54e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                NA
                                                UK
                                                
                                            </value>
      <webElementGuid>aa5c7902-cc98-4f82-8450-70417ee750f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1620&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>a4f76dc4-45e5-4eb8-9c4f-61b4aaad9e83</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1620']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>a97745e6-1b55-4b3e-a43f-31c2a5da03fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[17]/following::select[1]</value>
      <webElementGuid>3c93c262-88f8-42fe-bdba-951e4e6674b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M1620) Bowel Incontinence Frequency:'])[1]/following::select[1]</value>
      <webElementGuid>3d815013-a184-4997-94b1-f30fa57190df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very rarely or never has bowel incontinence'])[1]/preceding::select[1]</value>
      <webElementGuid>f5e29bf9-0263-4cf3-b534-b1447bfda065</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Less than once weekly'])[1]/preceding::select[1]</value>
      <webElementGuid>9a66e4f4-305a-4348-8c5c-912573862f8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[8]/div/div[2]/div/div/div[2]/div[3]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>aef47033-0e5b-41e9-88f0-aa8b7df52728</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                NA
                                                UK
                                                
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                                NA
                                                UK
                                                
                                            ')]</value>
      <webElementGuid>97fb86ef-4179-4886-993e-980fb2a89590</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
