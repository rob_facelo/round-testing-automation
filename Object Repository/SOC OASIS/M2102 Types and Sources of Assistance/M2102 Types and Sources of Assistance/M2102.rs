<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M2102</name>
   <tag></tag>
   <elementGuidId>91140e2e-4c64-41ed-8588-f548caac2461</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M2102 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M2102']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN' and (text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>4ca25a8d-d94b-4290-9e1e-1fd57f6cea02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>fe3a482f-8d20-4291-83d8-9ed6f40176f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M2100_CARE_TYPE_SRC_SPRVSN</value>
      <webElementGuid>b5b2cda3-1cf9-4f75-8b12-7ed97443f0c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN</value>
      <webElementGuid>5bd268b9-8b7c-4c25-99d4-106c5720601a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M2100_CARE_TYPE_SRC_SPRVSN', oasis.oasisDetails.M2100_CARE_TYPE_SRC_SPRVSN)</value>
      <webElementGuid>8156e4c0-18a5-4366-a378-ca2f1c6b435d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>7bb524c3-ee63-4d45-9439-30514e2fd0cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            </value>
      <webElementGuid>6a6e4100-bce9-4f10-9c8b-5f938a2174fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M2102&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>0a173e74-2fee-45a4-8bb9-d86282e75c85</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M2102']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>f3202839-06a7-4be4-885b-7f4d1fef9e5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[39]/following::select[1]</value>
      <webElementGuid>5c0da0c7-fe04-47ba-aa73-8d5406b1e0d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CARE MANAGEMENT'])[2]/following::select[1]</value>
      <webElementGuid>8bef6977-81c3-4959-8c56-103b0ef5d1f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='f. Supervision and safety (for example, due to cognitive impairment)'])[1]/preceding::select[1]</value>
      <webElementGuid>7668e367-3fbd-46f3-b5fd-5b34c6d2bb3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No assistance needed – patient is independent or does not have needs in this area'])[1]/preceding::select[1]</value>
      <webElementGuid>2b0fe7f4-4e64-42c0-820d-a0ff08cf96e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[15]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>7d470237-bee8-4b13-9428-eaa75db69ae7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ')]</value>
      <webElementGuid>a389e22a-a190-4acc-a069-a4460a708abd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
