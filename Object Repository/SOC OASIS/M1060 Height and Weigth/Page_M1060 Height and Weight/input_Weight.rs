<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Weight</name>
   <tag></tag>
   <elementGuidId>1b702cda-a0ea-46f5-8892-f1b0995ac025</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[159]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.form-inline.input-group > input.form-control.input-sm.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlength</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1060_WGT']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>83eabed8-83da-408e-a272-57dcca2e8dfa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>17257023-2485-46bd-8b9d-3bc8426fdd73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>031c9e1a-55d4-4d7a-ab1a-e21956bfb1ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>ee7686a9-8eb5-4800-9e9e-571588963c19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1060_WGT</value>
      <webElementGuid>5e86d53f-2bd8-4e3e-9e2b-f037a6c34775</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeypress</name>
      <type>Main</type>
      <value>return blockNonNumbers(this, event, false, false, '-');</value>
      <webElementGuid>b4ed3aa3-59d5-42df-8d0d-fe65c554e3b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1060_WGT', oasis.oasisDetails.M1060_WGT)</value>
      <webElementGuid>a8d58ee2-ab30-43b8-83fa-d2da7276617c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>d8c40800-4d28-485b-862f-e985a5494c4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1060&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2&quot;]/div[@class=&quot;form-inline input-group&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-maxlength&quot;]</value>
      <webElementGuid>257f2e68-ed28-4f07-8da9-b8899fec4913</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[159]</value>
      <webElementGuid>edec28c3-a0b3-47b0-90a9-ea27e644eba0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1060']/div/div[3]/div/div/div/input</value>
      <webElementGuid>ee3ce48e-0c87-46d7-b34c-d83cc1496807</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[13]/div/div/div[3]/div/div/div/input</value>
      <webElementGuid>b53109d3-7c03-47c0-8921-6b324f978fe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>180eacf9-4ed4-453e-9412-eb4de4949618</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
