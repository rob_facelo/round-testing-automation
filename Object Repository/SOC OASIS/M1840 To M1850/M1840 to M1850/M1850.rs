<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1850</name>
   <tag></tag>
   <elementGuidId>bbbd0008-175e-45f0-b25f-afac6dd1c422</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1850 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1850']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1850_CUR_TRNSFRNG' and (text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>1faaca70-5db6-4b2c-8345-166ac806586e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>55b13f33-1f23-4484-be57-481df0192ee5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1850_CUR_TRNSFRNG</value>
      <webElementGuid>74a9a086-c86c-492a-b740-0efe50a6fc07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1850_CUR_TRNSFRNG</value>
      <webElementGuid>ae39e205-a9e9-422e-a436-355ad653a399</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1850_CUR_TRNSFRNG', oasis.oasisDetails.M1850_CUR_TRNSFRNG)</value>
      <webElementGuid>0e55d17a-0c32-4683-80fe-e09d9a9a4135</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>b3e17679-b685-4444-bf57-9d095c0e1e70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            </value>
      <webElementGuid>0a692c86-17a4-4605-b98b-91f456dca76e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1850&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>8f87a42c-5636-4c03-9911-64921f29750c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1850']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>7e3dc2ab-2c75-46dd-80e7-4ad9cb0cc4ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[30]/following::select[1]</value>
      <webElementGuid>0e582f73-76d1-4b59-8dbc-54698624d99f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient depends entirely upon another person to maintain toileting hygiene.'])[1]/following::select[1]</value>
      <webElementGuid>9faba906-ea5e-4e17-b6f7-5897f76c6f7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to independently transfer.'])[1]/preceding::select[1]</value>
      <webElementGuid>e80ec126-1e2e-4dac-ad6a-4e9fb260ef69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to transfer with minimal human assistance or with use of an assistive device.'])[1]/preceding::select[1]</value>
      <webElementGuid>38943f4f-b76b-41c1-b80e-817052929a32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[11]/div/div[2]/div/div/div[2]/div[3]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>79ea16d6-d537-4a4c-a6d7-368c6c523600</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                                5
                                            ')]</value>
      <webElementGuid>62e54d13-1ca2-496d-8dea-c6f8a03e61f8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
