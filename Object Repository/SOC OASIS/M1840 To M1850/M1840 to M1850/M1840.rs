<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1840</name>
   <tag></tag>
   <elementGuidId>e5b30a74-d9d0-424d-aa0f-61f90d406100</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1840 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1840']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1840_CUR_TOILTG' and (text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f79bb4ec-8045-49ce-af10-eddc75d72e7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>8e5e8380-2845-483d-a98e-53f9409fa602</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1840_CUR_TOILTG</value>
      <webElementGuid>4f6611c0-427b-4949-a194-bb41cee4b3f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1840_CUR_TOILTG</value>
      <webElementGuid>a9ae8b65-ee68-456e-9732-865e68bbfde1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1840_CUR_TOILTG', oasis.oasisDetails.M1840_CUR_TOILTG)</value>
      <webElementGuid>1082e3d4-69f2-4c6b-9a14-7ca342bd80a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>5af609a3-a6f2-4bc5-aedb-31c1d0257a32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            </value>
      <webElementGuid>e932d0ef-95da-4ae3-8a81-47e204106e7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1840&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>3108d979-e6e8-4805-989e-669b98f712bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1840']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>aca7c318-1624-4083-9cd2-53a074b742f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[28]/following::select[1]</value>
      <webElementGuid>350e7fe0-699e-4f52-a6d9-42da71520892</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='and'])[1]/following::select[1]</value>
      <webElementGuid>a400ec44-a18e-4cfc-bdfd-a618791e1406</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to get to and from the toilet and transfer independently with or without a device.'])[1]/preceding::select[1]</value>
      <webElementGuid>c8c9e12f-1031-4b56-bd9f-34fdadca5436</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[11]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>58680cd9-8399-4ab4-8f18-06f1972aa745</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                4
                                            ')]</value>
      <webElementGuid>66c879da-3f0c-4319-8d2d-7675f91d69b4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
