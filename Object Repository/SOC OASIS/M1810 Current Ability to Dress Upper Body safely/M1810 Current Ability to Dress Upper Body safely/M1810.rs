<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1810</name>
   <tag></tag>
   <elementGuidId>af1a1247-9837-4d2f-8d89-aa800214c77e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1810']/div/div[2]/div/div[2]/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#M1810 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M1810_CUR_DRESS_UPPER' and (text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>d1e1744c-eb01-45dd-a742-1868a9bc6e48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>672975e2-c3e4-461f-a247-a2938de714c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1810_CUR_DRESS_UPPER</value>
      <webElementGuid>06c2d165-2f0a-4f52-aa66-2a88425af5ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1810_CUR_DRESS_UPPER</value>
      <webElementGuid>ff36f3bc-5597-4a8b-ac94-bd24e5202ec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1810_CUR_DRESS_UPPER', oasis.oasisDetails.M1810_CUR_DRESS_UPPER)</value>
      <webElementGuid>42476809-9eb5-4c8f-a635-0acaf9d51abc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>35eb8df3-1b8e-436a-b26a-b181f2cb572d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                            </value>
      <webElementGuid>001e1c4f-c5aa-4b0c-8641-5357a1dd6d23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1810&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>cb9031fc-93cf-430a-8e0a-32cc5a53808c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1810']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>f7a531d0-a82f-44ed-9030-bdb082c4753c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[25]/following::select[1]</value>
      <webElementGuid>d62cb002-0a7b-43cb-a001-149d94b0d65b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Upper'])[2]/following::select[1]</value>
      <webElementGuid>e26038f3-5f93-4a54-8da0-47e11153b740</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Able to dress upper body without assistance if clothing is laid out or handed to the patient.'])[1]/preceding::select[1]</value>
      <webElementGuid>666e6da6-136c-4769-8f13-0e1b7fd26d90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Someone must help the patient put on upper body clothing.'])[1]/preceding::select[1]</value>
      <webElementGuid>785d6495-4232-44c4-baa3-1e8911258376</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[10]/div/div[2]/div/div/div[2]/div[3]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>74581279-58b3-4a13-9835-f74813ffa625</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                            ')]</value>
      <webElementGuid>49cef745-3d7c-4411-a157-4dd8913ae9d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
