<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Number A1</name>
   <tag></tag>
   <elementGuidId>216549fd-e06a-4cb8-9b7b-952ce989ae48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[160]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3 > input.form-control.input-sm.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlength</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @ng-model = 'oasis.oasisDetails.M1308_NBR_PRSULC_STG2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a9eda58d-b837-4b6a-84a5-33adea67c579</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>4730f478-1a61-42ce-a86a-c11c79327094</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>9c8fbad4-1202-46e7-b7c2-588b58f25ff3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1308_NBR_PRSULC_STG2</value>
      <webElementGuid>2b036b40-f495-4121-b736-c72eb25a7382</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>94518de1-78fb-4cef-988f-055862cdfc0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1308_NBR_PRSULC_STG2</value>
      <webElementGuid>a78faf76-40c1-47b4-8b60-6623f0720688</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1308_NBR_PRSULC_STG2', oasis.oasisDetails.M1308_NBR_PRSULC_STG2)</value>
      <webElementGuid>578a9893-f75f-4094-a1f4-1d6735cf32a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1306_UNHLD_STG2_PRSR_ULCR == '0' || oasis.isLocked</value>
      <webElementGuid>6d757af6-3c06-45d0-b16b-0a743996a006</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1311&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space content-box content-box-m1311&quot;]/table[@class=&quot;table table-bordered tbl1311&quot;]/tbody[1]/tr[2]/td[2]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-maxlength&quot;]</value>
      <webElementGuid>2e09f956-718a-445f-aa05-d4c2078dce93</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[160]</value>
      <webElementGuid>5c30a90c-3c66-4525-a5e8-45bfdec84cbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1311']/div/div[2]/table/tbody/tr[2]/td[2]/div/input</value>
      <webElementGuid>d5e13d5c-6b7f-43b8-9ca2-f7069cd78fae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/table/tbody/tr[2]/td[2]/div/input</value>
      <webElementGuid>460e3615-00b9-4013-8f83-d8dd1925a134</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>ed1d8ad3-22be-44fb-af8d-703d591e6cca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
