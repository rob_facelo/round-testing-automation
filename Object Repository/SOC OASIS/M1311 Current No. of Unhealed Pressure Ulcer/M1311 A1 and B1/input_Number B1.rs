<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Number B1</name>
   <tag></tag>
   <elementGuidId>47c56779-315f-48bf-be1e-159c0ba98249</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[161]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @ng-model = 'oasis.oasisDetails.M1308_NBR_PRSULC_STG3']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e6f2e0ff-033f-407a-a08f-2c3359a15f26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>ebffdd36-8766-4316-85b8-3e1ba926c9e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-maxlength</value>
      <webElementGuid>1b2bcca7-4f06-4ed3-88ff-a5fa56c42ba5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1308_NBR_PRSULC_STG3</value>
      <webElementGuid>8d886f5b-2006-41dd-9b22-26b066f6f776</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>b69a8c00-63b4-4541-bb97-bb71cfcd1cf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1308_NBR_PRSULC_STG3</value>
      <webElementGuid>bff890b1-7153-4f40-9d00-0d14e22f1682</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1308_NBR_PRSULC_STG3', oasis.oasisDetails.M1308_NBR_PRSULC_STG3)</value>
      <webElementGuid>2267bc68-5861-4724-863f-1976fbe0abb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1306_UNHLD_STG2_PRSR_ULCR == '0' || oasis.isLocked</value>
      <webElementGuid>13d61584-6427-4cc8-8a42-7ca806c81505</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1311&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space content-box content-box-m1311&quot;]/table[@class=&quot;table table-bordered tbl1311&quot;]/tbody[1]/tr[3]/td[2]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid ng-valid-maxlength&quot;]</value>
      <webElementGuid>575fa0cd-3105-489d-9f61-34b498f7616f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[161]</value>
      <webElementGuid>b996b195-fb37-4c2d-b22c-fa4ca5d2cbfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1311']/div/div[2]/table/tbody/tr[3]/td[2]/div/input</value>
      <webElementGuid>38120e94-518f-4c00-8ab2-48f78ae1e04b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/table/tbody/tr[3]/td[2]/div/input</value>
      <webElementGuid>837d9180-68b5-4425-a5c9-e5f179aeb8a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>bd82144d-e6ad-492d-aa9d-142ba3feaff7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
