<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0100A</name>
   <tag></tag>
   <elementGuidId>f6dc0051-6ff3-4f07-9208-abf32c251802</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select.form-control.enter-code.ng-pristine.ng-valid.ng-touched</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0100A']/div/div/div/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0100A' and (text() = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ' or . = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a10dda41-d196-4bc6-a225-91097ca89030</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-valid ng-touched</value>
      <webElementGuid>37546f54-cbc4-42e0-909e-9e1de3a7b451</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0100A</value>
      <webElementGuid>72a50745-93f5-43af-86c8-673fb357008b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>8e68c2e2-46ec-4efb-9e40-49d8f5684c65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0100A</value>
      <webElementGuid>99794fc5-dd05-430b-ab9c-03ee59b03842</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0100A', oasis.oasisDetails.GG0100A)</value>
      <webElementGuid>488cd77f-2d66-4a82-9c3b-ef262e64f09e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>de488fc2-1c8e-4c29-9cac-9edb5fc1b11e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            </value>
      <webElementGuid>61d851c6-34b5-4227-a777-4b3f700dd37e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0100A&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>d8289a67-83d6-49e9-a1a0-5179bc674be1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0100A']/div/div/div/select</value>
      <webElementGuid>399bffcc-193e-4551-aee6-d1732cfa23e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Codes in Boxes'])[2]/following::select[1]</value>
      <webElementGuid>d3506173-ac39-45ed-8f4c-7da6f2784419</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. Self Care:'])[1]/preceding::select[1]</value>
      <webElementGuid>fd309ed9-0d6b-4474-8ed6-bdbbb96142a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Indoor Mobility (Ambulation):'])[1]/preceding::select[2]</value>
      <webElementGuid>8f34e9e4-f69c-4ba3-a33a-d51691d6f9f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/select</value>
      <webElementGuid>c93e73e1-8375-4fc2-b273-77764f7fcdda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ' or . = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ')]</value>
      <webElementGuid>b83b63b8-36f4-4a24-bf41-9147adb977ae</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
