<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>GG0100D</name>
   <tag></tag>
   <elementGuidId>743ce862-ee6b-4cc9-9f03-b281bc07241b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.GG0100D']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='GG0100D']/div/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>e2ee396d-3926-4b6d-80d1-7e6f7b091660</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>23c83245-ae51-4984-9dcf-6d51e457a10d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>GG0100D</value>
      <webElementGuid>0d9ce4c6-6efe-456f-a66f-53ceb74cb5c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>21964664-db37-43a6-9b85-844ce1ef64d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.GG0100D</value>
      <webElementGuid>8d656f45-08be-48a2-917e-2ff5c07572af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('GG0100D', oasis.oasisDetails.GG0100D)</value>
      <webElementGuid>ea51d1dd-89bc-4c5e-b897-b6cb5ce8ba7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>09e5afe4-13ba-42b0-b07c-01dec4d3bfe7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            </value>
      <webElementGuid>d784e3b1-c647-40e4-b63a-dbe1ed41dde4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;GG0100D&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>02173e36-8694-4e55-b1bf-1ff370b4ad90</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='GG0100D']/div/div/div/select</value>
      <webElementGuid>b46cfb23-e154-4c7e-babe-0c02ded1de7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C. Stairs:'])[1]/following::select[1]</value>
      <webElementGuid>697cb08e-fc19-410f-89a4-92927a320e7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Indoor Mobility (Ambulation):'])[1]/following::select[2]</value>
      <webElementGuid>ec809986-b87f-473a-8708-854735400eb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='D. Functional Cognition:'])[1]/preceding::select[1]</value>
      <webElementGuid>9f6f43c4-288e-4eac-a142-51d73b19daca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/select</value>
      <webElementGuid>e06f3700-f874-4903-9674-ef42f75d5277</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ' or . = '
                                                                
                                                                1
                                                                2
                                                                3
                                                                8
                                                                9
                                                                -
                                                            ')]</value>
      <webElementGuid>d6c050df-6ad9-4260-af47-059eb5e026df</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
