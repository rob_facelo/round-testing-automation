<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_3 - STYLES, FURIOUS</name>
   <tag></tag>
   <elementGuidId>db11c72c-f92b-4847-b6f7-132f79989ed9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchresultslist']/ul/li/a/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.text-primary.ng-binding</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>fe0ea4e1-dfff-4775-8c61-fcbdea730d2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-primary ng-binding</value>
      <webElementGuid>7c746690-15b5-4290-a133-1698af86a7b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>3 - STYLES, FURIOUS</value>
      <webElementGuid>c9722292-7d73-42c2-b4a4-12a072208a46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchresultslist&quot;)/ul[@class=&quot;dropdown-menu&quot;]/li[@class=&quot;ng-scope&quot;]/a[@class=&quot;ng-scope&quot;]/p[@class=&quot;text-primary ng-binding&quot;]</value>
      <webElementGuid>2d9edb47-fef4-4d32-b907-381541a694b8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchresultslist']/ul/li/a/p</value>
      <webElementGuid>8c4bf969-fe80-4e12-a8fa-37cc9dd1be7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chat Support'])[1]/following::p[1]</value>
      <webElementGuid>cefd8eec-9003-4635-a460-7ffd3d621f61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::p[4]</value>
      <webElementGuid>cd413ccc-b7f3-469a-aa77-b4e0de2ff305</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Admin'])[1]/preceding::p[4]</value>
      <webElementGuid>f498fd5c-661d-46ad-8e4f-e310ab075592</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='3 - STYLES, FURIOUS']/parent::*</value>
      <webElementGuid>e6d21230-2387-43d6-a83a-311ca417d804</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/p</value>
      <webElementGuid>ed31231c-e961-45a0-8fcd-41fd70d3322c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '3 - STYLES, FURIOUS' or . = '3 - STYLES, FURIOUS')]</value>
      <webElementGuid>37c524f7-d66e-4a71-a277-851b80226020</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
