<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>M1730 Depression Screening (a)</name>
   <tag></tag>
   <elementGuidId>43928dff-b8b3-48cc-918d-847eb6d98689</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])[741]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a08bdcda-14bc-4347-ae2e-576dccae0015</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>a466f1cf-0f2c-44ae-9373-c2b5a2727787</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1730_PHQ2_LACK_INTRST</value>
      <webElementGuid>e0b6fb95-a934-476e-af9d-e1b61b9c994e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1730_PHQ2_LACK_INTRST</value>
      <webElementGuid>c8146ed9-e723-4d66-b6dc-608128f7f510</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-true-value</name>
      <type>Main</type>
      <value>'01'</value>
      <webElementGuid>e99ebfb3-6ec6-4da3-b0d3-6360f47bd983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1730_PHQ2_LACK_INTRST', oasis.oasisDetails.M1730_PHQ2_LACK_INTRST)</value>
      <webElementGuid>51187b58-ab55-4c8e-bd7b-b3eaed21f874</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1730_STDZ_DPRSN_SCRNG != '01' || oasis.isLocked</value>
      <webElementGuid>cc06696c-48e8-4aeb-91c8-511a5e1d013d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>cf95cc74-5235-4eae-b138-f9650a5d7a27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1730&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-10 col-md-10 col-lg-10 row&quot;]/table[@class=&quot;mItemListTable&quot;]/tbody[1]/tr[2]/td[2]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive&quot;]/table[@class=&quot;table-bordered tblM1730&quot;]/tbody[1]/tr[3]/td[3]/div[@class=&quot;form-inline&quot;]/input[@class=&quot;ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>7ec0378b-32b8-4d2c-aa45-cd9955859bff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[741]</value>
      <webElementGuid>c4a74b57-8142-4608-bb7e-e384bd8b0bf3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1730']/div/div[2]/div[2]/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td[3]/div/input</value>
      <webElementGuid>745d0155-0360-492b-afca-09e31b914700</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/div/table/tbody/tr[3]/td[3]/div/input</value>
      <webElementGuid>d24d2d2f-2864-4a27-a1a4-a9096da67d50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox']</value>
      <webElementGuid>807529a9-02fd-44f8-825c-3a463e78d593</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
