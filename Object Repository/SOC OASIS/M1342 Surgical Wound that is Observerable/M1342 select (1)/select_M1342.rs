<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_M1342</name>
   <tag></tag>
   <elementGuidId>4b288405-2bfb-4250-ba58-360b64fc5d69</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M1342 > div.row > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.bottom-space > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.form-inline.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M1342']/div/div[2]/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>3b986377-5c0c-4b6a-96ec-4f81ae75d554</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>954b56d4-3a7c-47f7-9ce5-403432634ec5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M1342_STUS_PRBLM_SRGCL_WND</value>
      <webElementGuid>5e573dcd-ca89-4d7a-b526-2820cb1c1ad7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1342_STUS_PRBLM_SRGCL_WND</value>
      <webElementGuid>a58d289b-55ae-4f1f-bb86-a40d8e70c3e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M1342_STUS_PRBLM_SRGCL_WND', oasis.oasisDetails.M1342_STUS_PRBLM_SRGCL_WND)</value>
      <webElementGuid>424d4268-2486-4800-a01b-7525392f339c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M1340_SRGCL_WND_PRSNT == '00' || oasis.oasisDetails.M1340_SRGCL_WND_PRSNT == '02' || oasis.isLocked</value>
      <webElementGuid>a59b19e6-b314-441d-812a-96aa10d33ea4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                0
                                                1
                                                2
                                                3
                                                
                                            </value>
      <webElementGuid>7fda2a2d-9fe6-4d0b-8a09-7a105787cd24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M1342&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>2ab19ebb-b148-4d40-81ca-b50310c63ff7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M1342']/div/div[2]/div/div[2]/select</value>
      <webElementGuid>4ba88396-bf1c-4e76-863a-a3f0256e3a39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[13]/following::select[1]</value>
      <webElementGuid>1a31ccea-b816-4865-9645-b9b580785af4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(M1342) Status of Most Problematic Surgical Wound that is Observable'])[1]/following::select[1]</value>
      <webElementGuid>6b2bbe8f-7c61-4867-ae52-88fda02355d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newly epithelialized'])[1]/preceding::select[1]</value>
      <webElementGuid>c0105d96-08f1-41c7-b04c-8e4b82425a2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fully granulating'])[2]/preceding::select[1]</value>
      <webElementGuid>2e8d762e-c76d-4a3e-95e1-64b503db3a08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div/div/div[2]/div/div[2]/select</value>
      <webElementGuid>17277b5c-6a8f-4802-a56d-252443822b15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                
                                            ' or . = '
                                                
                                                0
                                                1
                                                2
                                                3
                                                
                                            ')]</value>
      <webElementGuid>cb63e2a8-701c-497d-bb9f-a4258a5bbcc1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
