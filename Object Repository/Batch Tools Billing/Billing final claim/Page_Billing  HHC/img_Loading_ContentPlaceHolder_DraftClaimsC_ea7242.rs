<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Loading_ContentPlaceHolder_DraftClaimsC_ea7242</name>
   <tag></tag>
   <elementGuidId>42a340b3-8ede-49af-a772-2c4033b0becf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[@id='ContentPlaceHolder_DraftClaimsControl_ddlSearchType_B-1Img']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_DraftClaimsControl_ddlSearchType_B-1Img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>37d64ca2-57a4-4cc3-bb9e-79fd5d78ed64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_DraftClaimsControl_ddlSearchType_B-1Img</value>
      <webElementGuid>2a16d079-60c2-4663-ab53-ab3691657bf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxEditors_edtDropDown</value>
      <webElementGuid>0d694020-58e5-40dc-9bd7-af11f6e25c16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/DXR.axd?r=1_37-VczGm</value>
      <webElementGuid>0e5e8242-2d5a-4873-be49-5069276c4a06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>v</value>
      <webElementGuid>87a8397e-bd3b-47d7-b454-b1361d2a69fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_DraftClaimsControl_ddlSearchType_B-1Img&quot;)</value>
      <webElementGuid>0f465275-1c22-4b80-bb12-ea78beff445d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//img[@id='ContentPlaceHolder_DraftClaimsControl_ddlSearchType_B-1Img']</value>
      <webElementGuid>b27e1a50-e905-4721-97a9-3203be739af3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_DraftClaimsControl_ddlSearchType_B-1']/img</value>
      <webElementGuid>0e8aaa06-8f70-440a-8465-4d902c384978</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[@alt='v'])[7]</value>
      <webElementGuid>4d8cf455-e553-4c4b-88f5-302524ef501c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/img</value>
      <webElementGuid>d17d5502-5ea3-40f7-8743-47dcfe5f7e68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@id = 'ContentPlaceHolder_DraftClaimsControl_ddlSearchType_B-1Img' and @src = '/DXR.axd?r=1_37-VczGm' and @alt = 'v']</value>
      <webElementGuid>5d306a69-5a1c-4141-bffa-f7f43c9dfe46</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
