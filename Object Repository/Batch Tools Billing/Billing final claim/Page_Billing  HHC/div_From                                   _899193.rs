<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_From                                   _899193</name>
   <tag></tag>
   <elementGuidId>593bebad-c6a8-4e58-bfb1-acda0cb6c18c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='angularElement']/div/div/div/div[2]/div/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-4.col-lg-3 > div.row.margin-bottom-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7b13572e-a7d2-4aa4-9abc-04a7ab5a924c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>a2a21b80-f4f6-4ef8-91cc-d8e744868bf7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                From:
                            
                            
                                
&lt;!--
(function(){var a = ({'numNegInf':'-∞','percentPattern':1,'numPosInf':'∞'});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											August 2022
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											3131123456
										
											3278910111213
										
											3314151617181920
										
											3421222324252627
										
											3528293031123
										
											3645678910
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP');
dxo.InitGlobalVariable('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD$C$FNP';
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='fade';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C',[[['dxeCalendarButtonHover'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[['dxeCalendarFastNavMonthHover'],[''],['FNP_M0','FNP_M1','FNP_M2','FNP_M3','FNP_M4','FNP_M5','FNP_M6','FNP_M7','FNP_M8','FNP_M9','FNP_M10','FNP_M11']],[['dxeCalendarFastNavYearHover'],[''],['FNP_Y0','FNP_Y1','FNP_Y2','FNP_Y3','FNP_Y4','FNP_Y5','FNP_Y6','FNP_Y7','FNP_Y8','FNP_Y9']]]);
ASPx.AddPressedItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C',[[['dxeCalendarButtonPressed'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C',[[['dxeDisabled'],[''],['']],[['dxeDisabled dxeButtonDisabled'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[[''],[''],['PYC','PMC','NMC','NYC'],,[[{'spriteCssClass':'dxEditors_edtCalendarPrevYearDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarPrevMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextYearDisabled'}]],['Img']]]);

var dxo = new ASPxClientCalendar('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C');
dxo.InitGlobalVariable('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD$C';
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2022,7,16);
dxo.selection.AddArray([new Date(2022,7,16,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD');
dxo.InitGlobalVariable('ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD';
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown('ContentPlaceHolder_DraftClaimsControl_dteFromDate', e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='slide';
dxo.closeAction='CloseButton';
dxo.popupHorizontalAlign='LeftSides';
dxo.popupVerticalAlign='Below';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate',[[['dxeButtonEditButtonHover'],[''],['B-1']]]);
ASPx.RemoveHoverItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate',[[['B-100']]]);
ASPx.AddPressedItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate',[[['dxeButtonEditButtonPressed'],[''],['B-1']]]);
ASPx.RemovePressedItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate',[[['B-100']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate',[[['dxeDisabled'],[''],['','I']],[['dxeDisabled dxeButtonDisabled'],[''],['B-1'],,[[{'spriteCssClass':'dxEditors_edtDropDownDisabled'}]],['Img']]]);
ASPx.RemoveDisabledItems('ContentPlaceHolder_DraftClaimsControl_dteFromDate',[[['B-100'],]]);
document.getElementById(&quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit('ContentPlaceHolder_DraftClaimsControl_dteFromDate');
dxo.InitGlobalVariable('dteFromDate');
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate',arg,ASPx.Callback,'ContentPlaceHolder_DraftClaimsControl_dteFromDate',ASPx.CallbackError,true); };
dxo.uniqueID = 'ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate';
dxo.stateObject = ({'rawValue':'1660608000000'});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.outOfRangeWarningClassName='dxeOutOfRWarn dxeOutOfRWarnRight';
dxo.outOfRangeWarningMessages=['The date must be in the range {0}...{1}', 'The date must be greater than or equal to {0}', 'The date must be less than or equal to {0}'];
dxo.date = new Date(2022,7,16);
dxo.dateFormatter = ASPx.DateFormatter.Create('M/d/yyyy');
dxo.AfterCreate();

//-->

                                                       
                        </value>
      <webElementGuid>4bce50b1-a97e-407a-bebe-fdbd1daa0351</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;angularElement&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-4 col-lg-3&quot;]/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>753d7fd7-678e-4feb-a692-2accf92d11dd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='angularElement']/div/div/div/div[2]/div/div[2]/div</value>
      <webElementGuid>8c192ef9-8244-4d97-bb1d-94dbaddaca0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Second 30 Days'])[2]/following::div[2]</value>
      <webElementGuid>62e7132a-47cd-4cc0-9930-e2b13a1aeb13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First 30 Days'])[2]/following::div[2]</value>
      <webElementGuid>2a5bbd78-1109-4ddb-b26b-631adfaa712f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div[3]/div/div/div/div/div/div[2]/div/div[2]/div</value>
      <webElementGuid>88775049-37a3-40a7-b932-7778624fcdf4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                            
                                From:
                            
                            
                                
&lt;!--
(function(){var a = ({&quot; , &quot;'&quot; , &quot;numNegInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;-∞&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;percentPattern&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;numPosInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;∞&quot; , &quot;'&quot; , &quot;});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											August 2022
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											3131123456
										
											3278910111213
										
											3314151617181920
										
											3421222324252627
										
											3528293031123
										
											3645678910
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2022,7,16);
dxo.selection.AddArray([new Date(2022,7,16,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;I&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtDropDownDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;],]]);
document.getElementById(&quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1660608000000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2022,7,16);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;M/d/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                                       
                        &quot;) or . = concat(&quot;
                            
                                From:
                            
                            
                                
&lt;!--
(function(){var a = ({&quot; , &quot;'&quot; , &quot;numNegInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;-∞&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;percentPattern&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;numPosInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;∞&quot; , &quot;'&quot; , &quot;});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											August 2022
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											3131123456
										
											3278910111213
										
											3314151617181920
										
											3421222324252627
										
											3528293031123
										
											3645678910
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,10,16);
dxo.visibleDate = new Date(2022,7,16);
dxo.selection.AddArray([new Date(2022,7,16,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;I&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtDropDownDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;],]]);
document.getElementById(&quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_DraftClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$DraftClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1660608000000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2022,7,16);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;M/d/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                                       
                        &quot;))]</value>
      <webElementGuid>3a0b8d75-8a9f-4c70-940c-6bb3d14a421b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
