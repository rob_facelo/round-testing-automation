<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_32270  styles final</name>
   <tag></tag>
   <elementGuidId>c7a935eb-3e11-45fc-935b-b4cf85b4d682</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_SubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_SubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>75603734-586f-4c09-83b0-9a49f35a2076</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>32270 / styles final</value>
      <webElementGuid>1ab46e3c-d353-4fa9-b26f-51a333790715</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_SubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0&quot;)/span[1]</value>
      <webElementGuid>b0082e6c-9d70-4aa8-89a3-d1a52edb3826</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_SubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0']/span</value>
      <webElementGuid>ca4a9184-7304-4d01-abea-4bc78c8ca34d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[2]/following::span[4]</value>
      <webElementGuid>89940705-33d3-4ad8-9542-105e0b48c4e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unbatch'])[1]/following::span[5]</value>
      <webElementGuid>f74e3f1b-8278-48ed-9107-3279b087de7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download 837 File'])[1]/preceding::span[1]</value>
      <webElementGuid>f6e13b93-a766-4bc6-9072-5afb383ecf92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$3,019.24'])[1]/preceding::span[2]</value>
      <webElementGuid>ac0343bd-e1a6-4727-8fe6-77ad3c6fe025</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='32270 / styles final']/parent::*</value>
      <webElementGuid>0f095b2e-739e-460a-8cfd-5ce329bf29be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/a/span</value>
      <webElementGuid>49e441fe-31b0-4271-99a1-7fffd1c30059</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '32270 / styles final' or . = '32270 / styles final')]</value>
      <webElementGuid>af51afe7-6eb4-42e2-bf9f-6a644072d6e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
