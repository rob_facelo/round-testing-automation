<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_All Medicare Final Claims_1</name>
   <tag></tag>
   <elementGuidId>e62243be-2cdf-4bbb-886e-7b1eb6b0c27f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_BatchClaimsControl_ddlSearchType_DDD_L_LBI3T0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchClaimsControl_ddlSearchType_DDD_L_LBI3T0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>beb24dfb-77c2-4d85-be0d-309b6b052dd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeListBoxItem dxeListBoxItemHover dxeListBoxItemSelected</value>
      <webElementGuid>b71b27a3-250f-4d71-b0e6-4621f154a5a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_BatchClaimsControl_ddlSearchType_DDD_L_LBI3T0</value>
      <webElementGuid>4a332925-bac3-43c1-aa47-1985cb6ecb12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All Medicare Final Claims</value>
      <webElementGuid>ea664801-c513-45bd-81ad-a94e5d25f14b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchClaimsControl_ddlSearchType_DDD_L_LBI3T0&quot;)</value>
      <webElementGuid>dc40eca7-7a53-4f85-8224-f79efe0f003f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_BatchClaimsControl_ddlSearchType_DDD_L_LBI3T0']</value>
      <webElementGuid>88d3eee2-3bd8-4a1d-9095-e2a850fbacdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_BatchClaimsControl_ddlSearchType_DDD_L_LBT']/tbody/tr[4]/td</value>
      <webElementGuid>ba17fd43-656a-4da4-b9e9-c7feb0e671ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Medicare RAPs'])[1]/following::td[1]</value>
      <webElementGuid>7613e82e-4ff4-49f2-af38-0d85cd02ddc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Private (NOA)'])[1]/following::td[2]</value>
      <webElementGuid>8f75e6ec-a549-4daa-b776-3e0696c0dbd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Private RAP Claims'])[1]/preceding::td[1]</value>
      <webElementGuid>7e6d53ed-a771-433c-8af5-9c9927a6074d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Private Final Claims'])[1]/preceding::td[2]</value>
      <webElementGuid>c54b6a29-8b12-42fb-b749-c131e33cde32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Medicare Final Claims']/parent::*</value>
      <webElementGuid>4fab1340-11e0-4143-a2e7-782b2df992ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table[2]/tbody/tr[4]/td</value>
      <webElementGuid>cd83a3ce-62d5-4b67-9633-e8059f1deeff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_BatchClaimsControl_ddlSearchType_DDD_L_LBI3T0' and (text() = 'All Medicare Final Claims' or . = 'All Medicare Final Claims')]</value>
      <webElementGuid>e1eb357a-fffc-4972-a577-ad69ce1a11ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
