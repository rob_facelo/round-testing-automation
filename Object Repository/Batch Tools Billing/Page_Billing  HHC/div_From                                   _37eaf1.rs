<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_From                                   _37eaf1</name>
   <tag></tag>
   <elementGuidId>97ea9a36-e3fb-4644-8e8b-8dc771d194ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='angularElement']/div/div/div/div[2]/div/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-12.col-md-4.col-lg-3 > div.row.margin-bottom-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8bfeac86-7d76-420b-b06e-bef196bdde95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row margin-bottom-sm</value>
      <webElementGuid>a98fdcb1-8d05-46db-8415-505080bc2238</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    From:
                                
                                
                                    
&lt;!--
(function(){var a = ({'numNegInf':'-∞','percentPattern':1,'numPosInf':'∞'});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2021
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											4431123456
										
											4578910111213
										
											4614151617181920
										
											4721222324252627
										
											482829301234
										
											49567891011
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C$FNP';
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='fade';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C',[[['dxeCalendarButtonHover'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[['dxeCalendarFastNavMonthHover'],[''],['FNP_M0','FNP_M1','FNP_M2','FNP_M3','FNP_M4','FNP_M5','FNP_M6','FNP_M7','FNP_M8','FNP_M9','FNP_M10','FNP_M11']],[['dxeCalendarFastNavYearHover'],[''],['FNP_Y0','FNP_Y1','FNP_Y2','FNP_Y3','FNP_Y4','FNP_Y5','FNP_Y6','FNP_Y7','FNP_Y8','FNP_Y9']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C',[[['dxeCalendarButtonPressed'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']]]);
ASPx.AddDisabledItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C',[[['dxeDisabled'],[''],['']],[['dxeDisabled dxeButtonDisabled'],[''],['BT','BC','BO','BCN','FNP_BO','FNP_BC']],[[''],[''],['PYC','PMC','NMC','NYC'],,[[{'spriteCssClass':'dxEditors_edtCalendarPrevYearDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarPrevMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextMonthDisabled'}],[{'spriteCssClass':'dxEditors_edtCalendarNextYearDisabled'}]],['Img']]]);

var dxo = new ASPxClientCalendar('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C';
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.serverCurrentDate=new Date(2022,10,15);
dxo.visibleDate = new Date(2021,10,15);
dxo.selection.AddArray([new Date(2021,10,15,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD');
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD';
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown('ContentPlaceHolder_BatchClaimsControl_dteFromDate', e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='slide';
dxo.closeAction='CloseButton';
dxo.popupHorizontalAlign='LeftSides';
dxo.popupVerticalAlign='Below';
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate',[[['dxeButtonEditButtonHover'],[''],['B-1']]]);
ASPx.RemoveHoverItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate',[[['B-100']]]);
ASPx.AddPressedItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate',[[['dxeButtonEditButtonPressed'],[''],['B-1']]]);
ASPx.RemovePressedItems('ContentPlaceHolder_BatchClaimsControl_dteFromDate',[[['B-100']]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit('ContentPlaceHolder_BatchClaimsControl_dteFromDate');
dxo.InitGlobalVariable('ContentPlaceHolder_BatchClaimsControl_dteFromDate');
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate',arg,ASPx.Callback,'ContentPlaceHolder_BatchClaimsControl_dteFromDate',ASPx.CallbackError,true); };
dxo.uniqueID = 'ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate';
dxo.stateObject = ({'rawValue':'1636934400000'});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused','');
dxo.outOfRangeWarningClassName='dxeOutOfRWarn dxeOutOfRWarnRight';
dxo.outOfRangeWarningMessages=['The date must be in the range {0}...{1}', 'The date must be greater than or equal to {0}', 'The date must be less than or equal to {0}'];
dxo.date = new Date(2021,10,15);
dxo.dateFormatter = ASPx.DateFormatter.Create('M/d/yyyy');
dxo.AfterCreate();

//-->

                                
                            </value>
      <webElementGuid>fd5dc162-709e-4c12-b41f-2af7321e5e94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;angularElement&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;panel panel-warning panelPrim margin-bottom-none hhc-blue-border-lighter&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;row margin-bottom-sm&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-4 col-lg-3&quot;]/div[@class=&quot;row margin-bottom-sm&quot;]</value>
      <webElementGuid>f8454bf1-0cfa-4989-b009-ff5689274a8a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='angularElement']/div/div/div/div[2]/div/div[2]/div</value>
      <webElementGuid>90738d1a-2ef3-40be-984f-4325fb5fff34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient Last Name:'])[1]/following::div[3]</value>
      <webElementGuid>5ab3d1d5-e308-4241-a5fc-52db4d961f13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Ge HIT 3 Claims'])[1]/following::div[5]</value>
      <webElementGuid>460b9d80-67d2-4fdf-adc6-9ed605926585</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div[3]/div/div/div/div/div/div[2]/div/div[2]/div</value>
      <webElementGuid>6f0dd05d-c0e7-4583-a67d-9e6a0159a95e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                                
                                    From:
                                
                                
                                    
&lt;!--
(function(){var a = ({&quot; , &quot;'&quot; , &quot;numNegInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;-∞&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;percentPattern&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;numPosInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;∞&quot; , &quot;'&quot; , &quot;});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2021
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											4431123456
										
											4578910111213
										
											4614151617181920
										
											4721222324252627
										
											482829301234
										
											49567891011
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,10,15);
dxo.visibleDate = new Date(2021,10,15);
dxo.selection.AddArray([new Date(2021,10,15,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1636934400000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2021,10,15);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;M/d/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                
                            &quot;) or . = concat(&quot;
                                
                                    From:
                                
                                
                                    
&lt;!--
(function(){var a = ({&quot; , &quot;'&quot; , &quot;numNegInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;-∞&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;percentPattern&quot; , &quot;'&quot; , &quot;:1,&quot; , &quot;'&quot; , &quot;numPosInf&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;∞&quot; , &quot;'&quot; , &quot;});for(var b in a) ASPx.CultureInfo[b] = a[b];})();
//-->

		
			Loading…
		
	
		
			
		
	
		
			
				
					
						
							
						
					
						
							
								
									
										
											November 2021
										
									
								
									
										
											SunMonTueWedThuFriSat
										
											4431123456
										
											4578910111213
										
											4614151617181920
										
											4721222324252627
										
											482829301234
										
											49567891011
										
									
								
							
						
							
								
									TodayClear
								
							
						
					
						
							
								
									
										
											
												
													JanFebMarApr
												
													MayJunJulAug
												
													SepOctNovDec
												
											
										
											
												
													
												
													
												
											
										
									
										
											
												OKCancel
											
										
									
								
							
						
					
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C_FNP&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C$FNP&quot; , &quot;'&quot; , &quot;;
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;fade&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavMonthHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_M0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_M11&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeCalendarFastNavYearHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;FNP_Y0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_Y9&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeCalendarButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddDisabledItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;dxeDisabled dxeButtonDisabled&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;BT&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;BCN&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BO&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;FNP_BC&quot; , &quot;'&quot; , &quot;]],[[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;PYC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;PMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NMC&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;NYC&quot; , &quot;'&quot; , &quot;],,[[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevYearDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarPrevMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextMonthDisabled&quot; , &quot;'&quot; , &quot;}],[{&quot; , &quot;'&quot; , &quot;spriteCssClass&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;dxEditors_edtCalendarNextYearDisabled&quot; , &quot;'&quot; , &quot;}]],[&quot; , &quot;'&quot; , &quot;Img&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientCalendar(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD_C&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD$C&quot; , &quot;'&quot; , &quot;;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.serverCurrentDate=new Date(2022,10,15);
dxo.visibleDate = new Date(2021,10,15);
dxo.selection.AddArray([new Date(2021,10,15,0,0,0,0)]);
dxo.isDateEditCalendar = true;
dxo.AfterCreate();

//-->

				
			
		
	
&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxpc-closeBtnHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;HCB-1&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientPopupControl(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_DDD&quot; , &quot;'&quot; , &quot;);
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate$DDD&quot; , &quot;'&quot; , &quot;;
dxo.Shown.AddHandler(function (s, e) { ASPx.DDBPCShown(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;, e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType=&quot; , &quot;'&quot; , &quot;slide&quot; , &quot;'&quot; , &quot;;
dxo.closeAction=&quot; , &quot;'&quot; , &quot;CloseButton&quot; , &quot;'&quot; , &quot;;
dxo.popupHorizontalAlign=&quot; , &quot;'&quot; , &quot;LeftSides&quot; , &quot;'&quot; , &quot;;
dxo.popupVerticalAlign=&quot; , &quot;'&quot; , &quot;Below&quot; , &quot;'&quot; , &quot;;
dxo.isPopupPositionCorrectionOn=false;
dxo.AfterCreate();

//-->

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonHover&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemoveHoverItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddPressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxeButtonEditButtonPressed&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;B-1&quot; , &quot;'&quot; , &quot;]]]);
ASPx.RemovePressedItems(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;B-100&quot; , &quot;'&quot; , &quot;]]]);
document.getElementById(&quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientDateEdit(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;);
dxo.callBack = function(arg) { WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;,arg,ASPx.Callback,&quot; , &quot;'&quot; , &quot;ContentPlaceHolder_BatchClaimsControl_dteFromDate&quot; , &quot;'&quot; , &quot;,ASPx.CallbackError,true); };
dxo.uniqueID = &quot; , &quot;'&quot; , &quot;ctl00$ContentPlaceHolder$BatchClaimsControl$dteFromDate&quot; , &quot;'&quot; , &quot;;
dxo.stateObject = ({&quot; , &quot;'&quot; , &quot;rawValue&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;1636934400000&quot; , &quot;'&quot; , &quot;});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle(&quot; , &quot;'&quot; , &quot;F&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;dxeFocused&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
dxo.outOfRangeWarningClassName=&quot; , &quot;'&quot; , &quot;dxeOutOfRWarn dxeOutOfRWarnRight&quot; , &quot;'&quot; , &quot;;
dxo.outOfRangeWarningMessages=[&quot; , &quot;'&quot; , &quot;The date must be in the range {0}...{1}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be greater than or equal to {0}&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;The date must be less than or equal to {0}&quot; , &quot;'&quot; , &quot;];
dxo.date = new Date(2021,10,15);
dxo.dateFormatter = ASPx.DateFormatter.Create(&quot; , &quot;'&quot; , &quot;M/d/yyyy&quot; , &quot;'&quot; , &quot;);
dxo.AfterCreate();

//-->

                                
                            &quot;))]</value>
      <webElementGuid>764e4cd4-5dc2-4d45-9e16-5f1fcac80f17</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
