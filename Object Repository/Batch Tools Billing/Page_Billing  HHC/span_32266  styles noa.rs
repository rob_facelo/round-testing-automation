<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_32266  styles noa</name>
   <tag></tag>
   <elementGuidId>25cb1f9e-af72-4266-8b35-ea98f3d10b00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ContentPlaceHolder_SubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_SubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>73ccec7a-cd7b-4b6f-86a5-14059df7a8ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>32266 / styles noa</value>
      <webElementGuid>770cd2fc-0b0b-4c02-b78b-3ba58b21be89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_SubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0&quot;)/span[1]</value>
      <webElementGuid>64858a5b-4f90-4c32-8912-31ffbaf19de9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder_SubmittedClaimsControl_grvSubmittedClaims_cell0_1_btnBatchDesc_0']/span</value>
      <webElementGuid>1860dd67-631a-4a81-ab23-6a3da85263d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[2]/following::span[4]</value>
      <webElementGuid>298a8046-1c6d-475e-978c-8170bcfd6d4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unbatch'])[1]/following::span[5]</value>
      <webElementGuid>d056f0b6-5f9f-44e9-950e-8c9f508290c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download 837 File'])[1]/preceding::span[1]</value>
      <webElementGuid>b0121af4-3d66-4413-9128-1b90d032fa26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$4,756.40'])[1]/preceding::span[2]</value>
      <webElementGuid>dbb53c8a-bc84-4fc5-9dc1-cd2221071cd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='32266 / styles noa']/parent::*</value>
      <webElementGuid>620a5c40-749c-4300-9e33-7f9a336d29d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/a/span</value>
      <webElementGuid>1eb312b9-580a-40ad-8b2e-29aa6f514cef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '32266 / styles noa' or . = '32266 / styles noa')]</value>
      <webElementGuid>502ac385-3bfd-44e1-9e61-e95a6cc5d152</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
