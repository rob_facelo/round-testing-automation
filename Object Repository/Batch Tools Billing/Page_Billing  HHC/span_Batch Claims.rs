<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Batch Claims</name>
   <tag></tag>
   <elementGuidId>e8d07dc9-4b4c-4cdc-a092-7e00d802b4d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_BatchClaimsControl_btnBatchPopup_CD']/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchClaimsControl_btnBatchPopup_CD > span.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d2b19523-6b74-4ad6-9c6b-71b308901dc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam</value>
      <webElementGuid>4994ac97-e4b0-4b3b-8c52-32253bcfded7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Batch Claims</value>
      <webElementGuid>8f304e65-af6f-4a84-b548-352e820d59cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchClaimsControl_btnBatchPopup_CD&quot;)/span[@class=&quot;dx-vam&quot;]</value>
      <webElementGuid>aeb8b1bd-9b4d-4a67-ad49-7df505d5f49c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_BatchClaimsControl_btnBatchPopup_CD']/span[2]</value>
      <webElementGuid>1649b730-f414-4dcb-8e71-06dedc0f87b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[2]/following::span[2]</value>
      <webElementGuid>9a581800-2afd-4ffa-b8bb-8fb83ec0b4cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[3]/following::span[4]</value>
      <webElementGuid>695bdda1-e4a1-41c9-9b79-929b78d4c32c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Undraft Claims'])[1]/preceding::span[1]</value>
      <webElementGuid>5ffcec0c-b6a8-401f-a4ec-b9acb757cfbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Patient Name'])[1]/preceding::span[4]</value>
      <webElementGuid>2cf9cd25-19f6-4f97-ba75-6ceac5d3b288</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Batch Claims']/parent::*</value>
      <webElementGuid>ba21411e-bb3b-4c65-9b1c-b3425b6b5e94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/span[2]</value>
      <webElementGuid>ed059fc5-5366-44c6-8a5a-f66a516d05e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Batch Claims' or . = 'Batch Claims')]</value>
      <webElementGuid>2f52f9b5-c040-4b55-a4dc-132f28453de3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
