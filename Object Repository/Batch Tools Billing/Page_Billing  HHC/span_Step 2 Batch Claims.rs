<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Step 2 Batch Claims</name>
   <tag></tag>
   <elementGuidId>c1e5d013-5e46-4d54-a022-6aa1205694ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i1_T']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#applicationLeftNav_LeftPanel_nbLeftNavigation_I0i1_T > span.dx-vam.dx-wrap</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8b8eecfa-ee3f-4e6d-85bd-7b3f80063d5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam dx-wrap</value>
      <webElementGuid>4a8db0df-a7dc-4689-9eb1-f7982c132239</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Step 2: Batch Claims</value>
      <webElementGuid>150003ab-ea01-4195-ba41-9355894e8de1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;applicationLeftNav_LeftPanel_nbLeftNavigation_I0i1_T&quot;)/span[@class=&quot;dx-vam dx-wrap&quot;]</value>
      <webElementGuid>db23b69d-54ec-40eb-8a2d-b06f1ecacb8a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='applicationLeftNav_LeftPanel_nbLeftNavigation_I0i1_T']/span</value>
      <webElementGuid>941ccb43-1dae-40c0-acbd-0d1eb215731c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 1: Draft Claims'])[1]/following::span[1]</value>
      <webElementGuid>b9edb924-3484-42f2-a487-3a3e0334651c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDGM Billing Process'])[2]/following::span[2]</value>
      <webElementGuid>88a3f7b3-579e-49e8-81d0-7b392ee7b22f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 3: Submit Claims'])[1]/preceding::span[1]</value>
      <webElementGuid>a69e5c3f-4cb9-44a9-a09f-e98e9861c9fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 4: Submitted Claims'])[1]/preceding::span[2]</value>
      <webElementGuid>d4f1579e-d67f-4007-83ad-b2791c455637</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Step 2: Batch Claims']/parent::*</value>
      <webElementGuid>20e61890-680e-45c7-951c-4dd5b2bfe2f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/ul/li[2]/a/span</value>
      <webElementGuid>29dea54d-8f71-4e24-986c-1f4e8040b18e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Step 2: Batch Claims' or . = 'Step 2: Batch Claims')]</value>
      <webElementGuid>b5a52013-240d-4feb-a760-75d2e22e7cd2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
