<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Loading_ctl00ContentPlaceHolderVisitT_9b07f8</name>
   <tag></tag>
   <elementGuidId>55af565d-75e6-40d1-96be-0e431ce78a8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate_I</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate_I']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d483c479-31b3-43e3-a5cd-350bbec98842</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeEditArea dxeEditAreaSys dxh0</value>
      <webElementGuid>21170602-99f2-4b8d-aa2b-613724a6efb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate_I</value>
      <webElementGuid>06933796-647d-4dcb-80aa-d512158f300d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ContentPlaceHolder$VisitTypeDetailsControl$pnlInsuranceSpec$grvInsuranceSpec$efnew$TC$grvHCPCS$efnew$TC$dteEffectiveEndDate</value>
      <webElementGuid>890efcf3-bf16-4928-a2ab-cd1c12600e9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onfocus</name>
      <type>Main</type>
      <value>ASPx.EGotFocus('ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate')</value>
      <webElementGuid>7b50b90b-5767-4878-b6b7-2c9fb56abe93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>ASPx.ELostFocus('ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate')</value>
      <webElementGuid>8e82ed0d-f6cb-48d0-a152-fcca7c4e3d7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ASPx.ETextChanged('ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate')</value>
      <webElementGuid>17d0e93e-9e0d-4677-a649-fa249b03c822</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>1b8d7e4c-40f0-49c7-8b48-0cc44a77f0c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>28cf3c24-f406-4261-9997-efc640401788</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate_I&quot;)</value>
      <webElementGuid>d1f8a46d-821a-4d05-b66b-092f1f1a8405</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate_I']</value>
      <webElementGuid>cf1cd2f3-761b-4c6f-aeac-4fbe7a67cbaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate']/tbody/tr/td/input</value>
      <webElementGuid>abe4a7d2-79e5-4f9a-a33a-4d08388b6aaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/table[2]/tbody/tr/td/input</value>
      <webElementGuid>3c34c5b2-4765-4774-b494-f7e7c3eb5a30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder_VisitTypeDetailsControl_pnlInsuranceSpec_grvInsuranceSpec_efnew_grvHCPCS_efnew_dteEffectiveEndDate_I' and @name = 'ctl00$ContentPlaceHolder$VisitTypeDetailsControl$pnlInsuranceSpec$grvInsuranceSpec$efnew$TC$grvHCPCS$efnew$TC$dteEffectiveEndDate' and @type = 'text']</value>
      <webElementGuid>8f6bb128-4767-42c3-87fa-79be0ebaa367</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
