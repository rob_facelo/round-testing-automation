<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_OASIS Export</name>
   <tag></tag>
   <elementGuidId>1a0b0cda-64c3-43dd-b6d2-aeb78de87fc4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='headerMenu_DXI3i6_T']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#headerMenu_DXI3i6_T > span.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4fd1b324-68d7-4c8a-9903-ab56f8f8e877</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-vam</value>
      <webElementGuid>71d89292-867d-4e85-870c-653617534fcf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OASIS Export</value>
      <webElementGuid>d1e8ee0d-899b-4d7e-99e8-acb576e1f526</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;headerMenu_DXI3i6_T&quot;)/span[@class=&quot;dx-vam&quot;]</value>
      <webElementGuid>72dd6554-6e2a-4a2f-869a-f228824b7bb9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='headerMenu_DXI3i6_T']/span</value>
      <webElementGuid>0cb62557-10f6-4539-8dd4-ec55f8245e79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send/Receive'])[1]/following::span[1]</value>
      <webElementGuid>7de7cc10-9667-4b63-9f71-f14640e8fa42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payroll Processing'])[1]/following::span[2]</value>
      <webElementGuid>f4607e43-32a0-418c-a0b7-ac3c9a95f6a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HHCAHPS Export'])[1]/preceding::span[1]</value>
      <webElementGuid>41721e97-6b7d-4b0e-9cf6-edcee862dcbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax History'])[1]/preceding::span[2]</value>
      <webElementGuid>df4f4a9c-4cf2-4615-b04c-fff052bbb36f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='OASIS Export']/parent::*</value>
      <webElementGuid>3fcc4663-1713-43df-a40b-e6ddc173e392</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/ul/li[13]/a/span</value>
      <webElementGuid>548f0bd7-a4c5-40d9-9ff6-2f363bd5fde6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'OASIS Export' or . = 'OASIS Export')]</value>
      <webElementGuid>5aaa6e37-ae32-409d-83e2-14b714beeb29</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
