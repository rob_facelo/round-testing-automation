<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Sep</name>
   <tag></tag>
   <elementGuidId>98ed506d-d54b-4f32-8caf-d8c2773d6639</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_BatchExportControl_dteFromDate_DDD_C_FNP_M8']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchExportControl_dteFromDate_DDD_C_FNP_M8</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>c0b76744-8e5c-4722-b36d-7ebb6eb6c78d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeCalendarFastNavMonth dxeCalendarFastNavMonthSelected dxeCalendarFastNavMonthHover</value>
      <webElementGuid>71016512-0651-47fa-851c-1f4d4e2b1bb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_BatchExportControl_dteFromDate_DDD_C_FNP_M8</value>
      <webElementGuid>4f0cddff-2398-4642-b817-18c1ed23a65a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sep</value>
      <webElementGuid>42659d8d-953f-48e1-97ba-6c9e95a67c6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchExportControl_dteFromDate_DDD_C_FNP_M8&quot;)</value>
      <webElementGuid>3be83649-24df-46c7-b356-e044c2aa0bdb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_BatchExportControl_dteFromDate_DDD_C_FNP_M8']</value>
      <webElementGuid>0ecf7127-9fca-4dd5-8a27-15287c34760f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_BatchExportControl_dteFromDate_DDD_C_FNP_m']/tbody/tr[3]/td</value>
      <webElementGuid>3ec9b909-417f-46ad-b2c8-7af003951a18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aug'])[1]/following::td[1]</value>
      <webElementGuid>e6dfc4cf-8383-414a-b583-f00f629ba5fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jul'])[1]/following::td[2]</value>
      <webElementGuid>70af6ad6-fd6b-497e-8ecf-405879c2e379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oct'])[1]/preceding::td[1]</value>
      <webElementGuid>921f3e1f-a1ef-4df1-acf4-5c211a581836</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nov'])[1]/preceding::td[2]</value>
      <webElementGuid>7afb17d5-8f7e-4b1a-a111-e33fcc754449</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sep']/parent::*</value>
      <webElementGuid>47d04bdd-07af-45eb-b78b-4444f8f3c9cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div/div/div/div/table/tbody/tr[3]/td</value>
      <webElementGuid>c2f5d16a-5fd3-46cd-9084-5924c6977a19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_BatchExportControl_dteFromDate_DDD_C_FNP_M8' and (text() = 'Sep' or . = 'Sep')]</value>
      <webElementGuid>ff6c682d-5160-4fcb-a80c-1f13ab377bc4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
