<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Submit</name>
   <tag></tag>
   <elementGuidId>2a6afc64-d139-47bb-96a9-69d7f9c2dae1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_SubmitOasisControl_btnSubmitExportOasis_CD</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'ContentPlaceHolder_SubmitOasisControl_btnSubmitExportOasis_CD' and (text() = '
			
				
			Submit
		' or . = '
			
				
			Submit
		')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentPlaceHolder_SubmitOasisControl_btnSubmitExportOasis_CD']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>dc54c154-d780-4156-be9b-ac75254d6f41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxb</value>
      <webElementGuid>dc55e532-3331-47fa-8125-dbd3c5c5c3ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_SubmitOasisControl_btnSubmitExportOasis_CD</value>
      <webElementGuid>00775643-d021-47e4-a34c-80ce95167104</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			
				
			Submit
		</value>
      <webElementGuid>6fedb513-36d9-4799-8cfb-fa276bb23227</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_SubmitOasisControl_btnSubmitExportOasis_CD&quot;)</value>
      <webElementGuid>3b95fea8-e253-4b27-8b7e-e9c91db4c1a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_SubmitOasisControl_btnSubmitExportOasis_CD']</value>
      <webElementGuid>6b15ba55-f7f1-4758-94ed-4325f62cc5c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder_SubmitOasisControl_btnSubmitExportOasis']/div</value>
      <webElementGuid>9f21cfdb-e38c-466c-a8cf-bc51b45fe4c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[6]/following::div[3]</value>
      <webElementGuid>d07bf088-3794-48fc-aa98-4b6ef4aba946</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please enter a valid date.'])[1]/following::div[3]</value>
      <webElementGuid>25003e3b-fbee-49c3-b400-3e4275594268</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/div/div[3]/div/div</value>
      <webElementGuid>a4fb30b5-bcff-4f9e-ab53-408351123c7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ContentPlaceHolder_SubmitOasisControl_btnSubmitExportOasis_CD' and (text() = '
			
				
			Submit
		' or . = '
			
				
			Submit
		')]</value>
      <webElementGuid>6083af5a-7f11-408d-92cd-494256ba326c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
