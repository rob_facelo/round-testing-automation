<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OASIS Export calendar icon</name>
   <tag></tag>
   <elementGuidId>634fa441-15aa-4f00-9b5b-ad40521cae7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder_BatchExportControl_dteFromDate_B-1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ContentPlaceHolder_BatchExportControl_dteFromDate_B-1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>7bfd9c6d-f4fa-46ac-b32a-91be6595568d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder_BatchExportControl_dteFromDate_B-1</value>
      <webElementGuid>13446398-691b-418e-9082-7aa36892158a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeButton dxeButtonEditButton dxeButtonEditButtonHover</value>
      <webElementGuid>07d27223-e7c3-48f8-b548-d12c6f08031d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onmousedown</name>
      <type>Main</type>
      <value>return ASPx.DDDropDown('ContentPlaceHolder_BatchExportControl_dteFromDate', event)</value>
      <webElementGuid>5d79aab9-62d5-47e3-ba1f-5bb13f1c1aef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder_BatchExportControl_dteFromDate_B-1&quot;)</value>
      <webElementGuid>5c8d05d7-91d7-4207-b14b-c9d7829ba579</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='ContentPlaceHolder_BatchExportControl_dteFromDate_B-1']</value>
      <webElementGuid>86dddd23-4bac-445a-87ed-8a099fbb6ffd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ContentPlaceHolder_BatchExportControl_dteFromDate']/tbody/tr/td[2]</value>
      <webElementGuid>a29c7be3-630e-4826-a377-0319b1ba25ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading…'])[3]/following::td[2]</value>
      <webElementGuid>db9cb91c-856c-44fe-befb-12cd2509d3c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0090 Complete Date:'])[1]/following::td[4]</value>
      <webElementGuid>83411867-0286-45fa-a646-4f288b0c19b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='March 2022'])[1]/preceding::td[16]</value>
      <webElementGuid>5a65b049-6251-413f-98f7-bcd77825712e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sun'])[1]/preceding::td[22]</value>
      <webElementGuid>002c7cdb-92ab-4cf7-a659-30dbbc86d73c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/table[2]/tbody/tr/td[2]</value>
      <webElementGuid>a55d1377-2f05-4d09-ac64-24b3117d4b71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'ContentPlaceHolder_BatchExportControl_dteFromDate_B-1']</value>
      <webElementGuid>67df3e56-e933-4328-87c5-e767c0da0972</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
