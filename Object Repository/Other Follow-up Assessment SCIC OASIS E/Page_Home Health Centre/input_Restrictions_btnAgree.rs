<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Restrictions_btnAgree</name>
   <tag></tag>
   <elementGuidId>d9c5e25c-51cd-444c-84ba-cabb87743d94</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#btnAgree</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='btnAgree']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>53a33d1d-3252-4dba-a723-3c4631146d74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>de1449c0-6f4b-471a-8a78-fad483db69fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>btnAgree</value>
      <webElementGuid>7b28bad8-a4f8-4252-8757-122804370c74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>I/We Agree</value>
      <webElementGuid>8fceea39-d2ed-41ec-b63e-80d70cfb132b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>clearUserLogsStorage();</value>
      <webElementGuid>458c9bb6-7c45-4523-b885-8b6c81b175c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnAgree</value>
      <webElementGuid>94c65cba-ecad-48f2-b264-34fbf6c21a5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>agree-btn form-control</value>
      <webElementGuid>f7486d80-6810-4698-b2d0-f93d0a3647d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnAgree&quot;)</value>
      <webElementGuid>6b329f9b-290e-46a9-ba62-4ef4cb523964</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='btnAgree']</value>
      <webElementGuid>4ed797cb-c8ad-4180-bf2d-4892d538625a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]/div[2]/div/div[3]/input</value>
      <webElementGuid>4826499a-05dd-4465-8661-2ae31f3db087</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/input</value>
      <webElementGuid>d17e659f-d00e-4415-98fa-47b3d7ae90a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'btnAgree' and @id = 'btnAgree']</value>
      <webElementGuid>e7d0afe2-91c6-4265-925e-6c6b4b550cc3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
