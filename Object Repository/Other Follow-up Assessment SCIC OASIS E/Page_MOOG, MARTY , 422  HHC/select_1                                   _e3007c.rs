<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1                                   _e3007c</name>
   <tag></tag>
   <elementGuidId>4102565d-95fb-4ff8-9b4f-75144db0e03e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#M0110 > div.form-group.panel.panel-primary.hhc-blue-border > div.panel-body.panelContent > div > div.col-xs-12.col-sm-4.col-md-2.col-lg-2.row > div.col-xs-offset-3.col-sm-offset-3.col-md-offset-3.col-lg-offset-3.row > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M0110']/div/div[2]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a0932206-d19e-486d-8732-4e011277bf6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>f0425ba6-76e9-4dcf-89f9-f9414d60472b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M0110_EPISODE_TIMING</value>
      <webElementGuid>1d9a48bf-f0c0-4dbe-9b50-b178812dbfde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M0110_EPISODE_TIMING</value>
      <webElementGuid>047fa775-d285-44e6-b6a9-f32ec39998bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M0110_EPISODE_TIMING', oasis.oasisDetails.M0110_EPISODE_TIMING)</value>
      <webElementGuid>2e4ef4b5-a846-479c-b7f3-3e26e6e3258e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>94feec9d-5fa3-409b-8105-772c34a1cb83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    </value>
      <webElementGuid>a28bd2d8-93dd-4133-870e-225a02d33dd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M0110&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>241e232d-2709-4c7d-b7b4-5d24284cf4d2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M0110']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>d6b15eb1-1433-4052-b414-fffe3e10af5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[3]/following::select[1]</value>
      <webElementGuid>65cc0288-db31-4215-ac23-c18ccac9e81a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other follow-up'])[1]/following::select[1]</value>
      <webElementGuid>d9201bd8-bdfb-47a4-a190-abbc6c2be4a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Early'])[1]/preceding::select[1]</value>
      <webElementGuid>bd072001-5f5f-44c9-ac9b-a63b4047a52f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Later'])[1]/preceding::select[1]</value>
      <webElementGuid>855455b3-8ccf-4a98-b11e-0bcd8cfea93e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/div/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>cd0a75e2-fd1f-4ee8-8ebe-960c21143e06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    ' or . = '
                                        
                                        1
                                        2
                                        UK
                                        NA
                                    ')]</value>
      <webElementGuid>2b20e314-e655-482a-9d58-b928f80ad412</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
