<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_18</name>
   <tag></tag>
   <elementGuidId>a1bbabbb-a812-4938-9d22-89a16946a46b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '18' or . = '18')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>b8fdae48-2a41-42ef-953f-b242458d1791</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>active day</value>
      <webElementGuid>9c8c15c7-527c-4e58-9c9d-c9ca33904ef7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>18</value>
      <webElementGuid>05ccaab9-b0e0-4454-aa95-aa066a73d721</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/tr[1]/td[@class=&quot;active day&quot;]</value>
      <webElementGuid>910a7a81-c7b0-4869-9f2a-de6b70067b47</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '18' or . = '18')]</value>
      <webElementGuid>7b496935-b4d6-4d1a-bc54-0ae12b457b61</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
