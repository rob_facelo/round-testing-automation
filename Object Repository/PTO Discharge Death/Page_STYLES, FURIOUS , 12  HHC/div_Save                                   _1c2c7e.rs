<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Save                                   _1c2c7e</name>
   <tag></tag>
   <elementGuidId>7da654c4-9d9c-4950-886b-9a63669cfa34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row > div.row.ng-scope</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>93cc9355-ea65-481e-a7d1-819a51778240</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row ng-scope</value>
      <webElementGuid>04856c67-ae12-41aa-8713-1c354c8589cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-controller</name>
      <type>Main</type>
      <value>OasisController as oasis</value>
      <webElementGuid>4a637769-2e80-42d8-b761-687c55a4f1c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
            
            
            
                
                Upload File
            
            
            
            
                
                Fax Oasis
            
        
    

    
        Death at home (08/30/2022 - 10/28/2022) (Oasis D1)
    
    
        12 - STYLES, FURIOUS 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            
                
                    
                    
                        
                            PATIENT TRACKING SHEET
                        
                            CLINICAL RECORD ITEMS
                        
                            MEDICATIONS
                        
                            OTHERS
                        
                    
                
            
            
                
                    

                        
                        
                            
    
        
            PATIENT TRACKING SHEET
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

    
        
            
            
                
                
                    LEGAL REPRESENTATIVE
                    
                        
                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Contact No.:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                            
                            
                                
                                     DPOA           
                                     Conservator
                                
                            
                        

                    
                
            

            
                
                
                    PATIENT CONTACT PERSON
                    

                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Home Phone:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Mobile Phone:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        

                    
                
            

        
    

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        
            
                
                    (M0140) RACE/ETHNICITY (Mark all that apply.)
                
                
                    
                        
                            
                                
                                1 - American Indian or Alaska Native
                            
                        
                        
                            
                                
                                3 - Black or African-American
                            
                        
                        
                            
                                
                                5 - Native Hawaiian or Pacific Islander
                            
                        
                        
                    
                    
                        
                            
                                
                                2 - Asian
                            
                        
                        
                            
                                
                                4 - Hispanic or Latino
                            
                        
                        
                            
                                
                                6 - White
                            
                        
                    
                
            
        

        
        
            If an assessment was erroneously submitted in a masked format, that is, it was later discovered that the patient was a Medicare or Medicaid patient
            but was not originally indicated as such at M0150, then an inactivation must be submitted. Normally, the HHA will also submit a new, corrected assessment
            in this situation. For example, if the value at M0150 for a submitted and accepted assessment is not equal to 1, 2, 3, or 4, and it should have been, then
            an inactivation request should be submitted.
            
                
                    (M0150)Current Payment Sources for Home Care (Mark all that apply.)
                
                
                    
                        
                            
                                
                                0 - None; no charge for current services
                            
                        
                        
                            
                                
                                4 - Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8 - Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1 - Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5 - Workers' compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2 - Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6 - Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10 - Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7 - Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11 - Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK - Unknown
                            
                        
                    
                
            
        

    


    .panelTitle {
        text-align: center;
        font-weight: bold;
        font-size: 15px;
        padding: 10px;
    }

    div.ui-datepicker {
        font-size: 12px;
    }




    
        
            CLINICAL RECORD ITEMS
        
    
    

    
        
            
                
                


                    
                    
                        COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

                        
                        
                            
                                
                                    
                                        (M0080) Discipline of Person Completing Assessment
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                RN
                                            
                                            
                                                2
                                                PT
                                            
                                            
                                                3
                                                SLP/ST
                                            
                                            
                                                4
                                                OT
                                            
                                        
                                    
                                
                                
                                    
                                        Care Staff Name:
                                        
                                            
                                            
                                            
                                            
                                                
                                                    Acosta, Casey 
                                                
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    (M0090) Date Assessment Completed:
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                         Time In:    Time Out:    
                    

                    
                    
                        
                        

                        
                        
                            
                                
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                
                                                
                                                
                                                
                                                
                                                8
                                                9
                                            
                                        
                                    
                                    
                                    
                                    
                                    
                                        
                                            Discharge from Agency - Not to an Inpatient Facility
                                        
                                        
                                            
                                                
                                                    8
                                                    Death at home [ Go to M2005 ]
                                                
                                                
                                                    9
                                                    Discharge from agency [ Go to M1041 ]
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                
            
        
    






    
        
            MEDICATIONS
        
    
    

    
        

            
                
                

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M2005) Medication Intervention: Did the agency contact and complete physician (or physician-designee) prescribed/recommended actions by midnight of the next calendar 
                                        day each time potential clinically significant medication issues were identified since the SOC/ROC?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                9
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No
                                            
                                            
                                                1
                                                Yes
                                            
                                            
                                                9
                                                NA - There were no potential clinically significant medication issues identified since SOC/ROC or patient is not taking any medications
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                    
                    

                
            

        
    




    
        
            OTHERS
        
    
    

    
        

            
                
                


                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (J1800) Any Falls Since SOC/ROC, whichever is more recent.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                                
                                                0
                                                1
                                                -
                                            
                                        
                                    
                                    
                                        Has the patient had any falls since SOC/ROC, whichever is more recent?
                                    
                                    
                                        
                                            
                                                0
                                                No → Skip J1900
                                            
                                            
                                                1
                                                
                                                    Yes → Continue to J1900. Number of Falls Since SOC/ROC, whichever is more recent
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (J1900) Number of Falls Since SOC/ROC, whichever is more recent
                                    
                                    
                                
                                
                                    
                                        
                                            CODING:
                                        
                                        0. None
                                        1. One
                                        2. Two or more
                                    
                                    
                                        
                                            
                                                
                                                    Enter Codes in Boxes
                                                
                                            
                                            
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                A. No injury: No evidence of any injury is noted on physical assessment by the nurse or primary care clinician; no complaints of pain or injury by the patient; no change in the patient's behavior is noted after the fall
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                B. Injury (except major): Skin tears, abrasions, lacerations, superficial bruises, hematomas and sprains; or any fall-related injury that causes the patient to complain of pain
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                C. Major injury: Bone fractures, joint dislocations, closed head injuries with altered consciousness, subdural hematoma
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    

                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.
                                    
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        DISCIPLINES INVOLVED: SN PT OT ST MSW Aide Other     All involved team members notifiedWas a referral made to MSW for assistance with community resources/assistance with a living will/counseling needs  (depression/suicidal ideation) and/or unsafe environment? Date  Yes No Refused N/AComment:    Complete this Section for either Transfer to Inpatient Facility or Death at Home.REASON FOR ADMISSION TO HOME HEALTH AND SUMMARY OF CARE TO DATE (describe condition):   textAreaAdjust(false,11293);DETAILS RELATED TO EMERGENT CARE AND/OR HOSPITALIZATION/NURSING HOME (when known):   textAreaAdjust(false,11296);Copy of summary   sent faxedDate:   To:  Physician    Facility Name    Copy of current P.O.C. attached  Yes NoCurrent medication list attached  Yes NoAdvance directive exists  Yes NoCopy attached  Yes NoDNR  Yes NoCopy attached  Yes No
                    

                
            

        
    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                    
                

                
                    
                
            
        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

</value>
      <webElementGuid>267476ff-b64c-4687-a356-137cead04779</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]</value>
      <webElementGuid>5171ca92-335b-430d-a2b7-0f514f90292e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]</value>
      <webElementGuid>ef35e99a-753a-4037-89dd-85ee96eeaa40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Products/Agencies'])[1]/following::div[9]</value>
      <webElementGuid>783803e6-43d3-48fa-a985-6182c4104bbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DDE / CWF Connectivity'])[1]/following::div[15]</value>
      <webElementGuid>7d91e43e-30ba-49b9-bdc6-dd1284f1db35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div/div[2]</value>
      <webElementGuid>3eb099f3-9197-4dbf-86c1-c97083d030f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
            
            
            
                
                Upload File
            
            
            
            
                
                Fax Oasis
            
        
    

    
        Death at home (08/30/2022 - 10/28/2022) (Oasis D1)
    
    
        12 - STYLES, FURIOUS 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            
                
                    
                    
                        
                            PATIENT TRACKING SHEET
                        
                            CLINICAL RECORD ITEMS
                        
                            MEDICATIONS
                        
                            OTHERS
                        
                    
                
            
            
                
                    

                        
                        
                            
    
        
            PATIENT TRACKING SHEET
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

    
        
            
            
                
                
                    LEGAL REPRESENTATIVE
                    
                        
                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Contact No.:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                            
                            
                                
                                     DPOA           
                                     Conservator
                                
                            
                        

                    
                
            

            
                
                
                    PATIENT CONTACT PERSON
                    

                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Home Phone:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Mobile Phone:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        

                    
                
            

        
    

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        
            
                
                    (M0140) RACE/ETHNICITY (Mark all that apply.)
                
                
                    
                        
                            
                                
                                1 - American Indian or Alaska Native
                            
                        
                        
                            
                                
                                3 - Black or African-American
                            
                        
                        
                            
                                
                                5 - Native Hawaiian or Pacific Islander
                            
                        
                        
                    
                    
                        
                            
                                
                                2 - Asian
                            
                        
                        
                            
                                
                                4 - Hispanic or Latino
                            
                        
                        
                            
                                
                                6 - White
                            
                        
                    
                
            
        

        
        
            If an assessment was erroneously submitted in a masked format, that is, it was later discovered that the patient was a Medicare or Medicaid patient
            but was not originally indicated as such at M0150, then an inactivation must be submitted. Normally, the HHA will also submit a new, corrected assessment
            in this situation. For example, if the value at M0150 for a submitted and accepted assessment is not equal to 1, 2, 3, or 4, and it should have been, then
            an inactivation request should be submitted.
            
                
                    (M0150)Current Payment Sources for Home Care (Mark all that apply.)
                
                
                    
                        
                            
                                
                                0 - None; no charge for current services
                            
                        
                        
                            
                                
                                4 - Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8 - Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1 - Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5 - Workers&quot; , &quot;'&quot; , &quot; compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2 - Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6 - Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10 - Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7 - Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11 - Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK - Unknown
                            
                        
                    
                
            
        

    


    .panelTitle {
        text-align: center;
        font-weight: bold;
        font-size: 15px;
        padding: 10px;
    }

    div.ui-datepicker {
        font-size: 12px;
    }




    
        
            CLINICAL RECORD ITEMS
        
    
    

    
        
            
                
                


                    
                    
                        COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

                        
                        
                            
                                
                                    
                                        (M0080) Discipline of Person Completing Assessment
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                RN
                                            
                                            
                                                2
                                                PT
                                            
                                            
                                                3
                                                SLP/ST
                                            
                                            
                                                4
                                                OT
                                            
                                        
                                    
                                
                                
                                    
                                        Care Staff Name:
                                        
                                            
                                            
                                            
                                            
                                                
                                                    Acosta, Casey 
                                                
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    (M0090) Date Assessment Completed:
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                         Time In:    Time Out:    
                    

                    
                    
                        
                        

                        
                        
                            
                                
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                
                                                
                                                
                                                
                                                
                                                8
                                                9
                                            
                                        
                                    
                                    
                                    
                                    
                                    
                                        
                                            Discharge from Agency - Not to an Inpatient Facility
                                        
                                        
                                            
                                                
                                                    8
                                                    Death at home [ Go to M2005 ]
                                                
                                                
                                                    9
                                                    Discharge from agency [ Go to M1041 ]
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                
            
        
    






    
        
            MEDICATIONS
        
    
    

    
        

            
                
                

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M2005) Medication Intervention: Did the agency contact and complete physician (or physician-designee) prescribed/recommended actions by midnight of the next calendar 
                                        day each time potential clinically significant medication issues were identified since the SOC/ROC?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                9
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No
                                            
                                            
                                                1
                                                Yes
                                            
                                            
                                                9
                                                NA - There were no potential clinically significant medication issues identified since SOC/ROC or patient is not taking any medications
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                    
                    

                
            

        
    




    
        
            OTHERS
        
    
    

    
        

            
                
                


                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (J1800) Any Falls Since SOC/ROC, whichever is more recent.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                                
                                                0
                                                1
                                                -
                                            
                                        
                                    
                                    
                                        Has the patient had any falls since SOC/ROC, whichever is more recent?
                                    
                                    
                                        
                                            
                                                0
                                                No → Skip J1900
                                            
                                            
                                                1
                                                
                                                    Yes → Continue to J1900. Number of Falls Since SOC/ROC, whichever is more recent
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (J1900) Number of Falls Since SOC/ROC, whichever is more recent
                                    
                                    
                                
                                
                                    
                                        
                                            CODING:
                                        
                                        0. None
                                        1. One
                                        2. Two or more
                                    
                                    
                                        
                                            
                                                
                                                    Enter Codes in Boxes
                                                
                                            
                                            
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                A. No injury: No evidence of any injury is noted on physical assessment by the nurse or primary care clinician; no complaints of pain or injury by the patient; no change in the patient&quot; , &quot;'&quot; , &quot;s behavior is noted after the fall
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                B. Injury (except major): Skin tears, abrasions, lacerations, superficial bruises, hematomas and sprains; or any fall-related injury that causes the patient to complain of pain
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                C. Major injury: Bone fractures, joint dislocations, closed head injuries with altered consciousness, subdural hematoma
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    

                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.
                                    
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        DISCIPLINES INVOLVED: SN PT OT ST MSW Aide Other     All involved team members notifiedWas a referral made to MSW for assistance with community resources/assistance with a living will/counseling needs  (depression/suicidal ideation) and/or unsafe environment? Date  Yes No Refused N/AComment:    Complete this Section for either Transfer to Inpatient Facility or Death at Home.REASON FOR ADMISSION TO HOME HEALTH AND SUMMARY OF CARE TO DATE (describe condition):   textAreaAdjust(false,11293);DETAILS RELATED TO EMERGENT CARE AND/OR HOSPITALIZATION/NURSING HOME (when known):   textAreaAdjust(false,11296);Copy of summary   sent faxedDate:   To:  Physician    Facility Name    Copy of current P.O.C. attached  Yes NoCurrent medication list attached  Yes NoAdvance directive exists  Yes NoCopy attached  Yes NoDNR  Yes NoCopy attached  Yes No
                    

                
            

        
    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                    
                

                
                    
                
            
        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

&quot;) or . = concat(&quot;
    
    
        
            
                
                Save
            
            
                
                Validate Oasis
            
            
                
                Lock Oasis
            
            
            
            
            
            
            
            
                
                Printer Friendly (Full)
            
            
                
                Printer Friendly (Response Summary)
            
            
                
                Print Oasis Template
            
            
            
            
            
                
                Upload File
            
            
            
            
                
                Fax Oasis
            
        
    

    
        Death at home (08/30/2022 - 10/28/2022) (Oasis D1)
    
    
        12 - STYLES, FURIOUS 
    
    
        
        
            
            
                Lock Document For Exclusive Use
            
            
        
    
    
        
            
                Full Oasis
            
            
                Summary Only
            
        
    

    
        
            
                
                    
                    
                        
                            PATIENT TRACKING SHEET
                        
                            CLINICAL RECORD ITEMS
                        
                            MEDICATIONS
                        
                            OTHERS
                        
                    
                
            
            
                
                    

                        
                        
                            
    
        
            PATIENT TRACKING SHEET
        
    
    

    

        
        
            
                AGENCY/BRANCH 
                
                    
                        
                            
                                
                                    
                                        (M0010) CMS Certification Number:
                                    
                                
                                
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        (M0014) Branch State:
                                    
                                
                                
                                    
                                
                            
                        
                    
                    
                        
                            
                                
                                    
                                        (M0016) Branch ID Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                
            
        

        
            
                
                    
                    
                        PRIMARY PHYSICIAN
                        
                            
                                
                                    
                                        (M0018) National Provider Identifier (NPI):
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                    
                                
                                
                                    
                                         UK - Unknown or Not Available
                                    
                                
                            
                            
                                
                                    
                                        Full Name:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Address:
                                    
                                
                                
                                    
                                
                            
                            
                                
                                    
                                        Phone Number:
                                    
                                
                                
                                    
                                
                            
                        
                    
                

                
                    
                    
                        CERTIFICATION DATES
                        
                            
                                
                                    
                                        Certification Period:
                                    
                                
                                
                                    
                                

                            
                            
                            
                                
                                    
                                        (M0030) Start of Care Date:
                                    
                                
                                
                                    
                                        
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            (M0032) Resumption of Care Date:
                                        
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                                
                                    
                                    
                                    
                                         NA - Not Applicable
                                    
                                
                            
                        
                    
                
            
        

        
            
            
                PATIENT INFORMATION
                
                    
                        
                            
                                (M0020) Patient ID Number:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) First Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Middle Initial:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0040) Last Name:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0066) Patient Birth Date:
                            
                        
                        
                            
                        
                    

                    
                        
                            
                                (M0069) Gender:
                            
                        
                        
                            
                                
                            
                        
                        
                            
                                1 - Male
                            
                            
                                2 - Female
                            
                        
                    
                
            
        

        
            
            
                ADDRESS
                
                    
                        
                            
                                Phone Number:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Street:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                City:
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                Country
                            
                        
                        
                            
                        
                    
                    
                        
                            
                                (M0050) State:
                            
                        
                        
                            
                        

                    
                    
                        
                            
                                (M0060) ZIP Code:
                            
                        
                        
                            
                        
                    
                
            
        

    
        
            
            
                
                
                    LEGAL REPRESENTATIVE
                    
                        
                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Contact No.:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                            
                            
                                
                                     DPOA           
                                     Conservator
                                
                            
                        

                    
                
            

            
                
                
                    PATIENT CONTACT PERSON
                    

                        
                            
                                
                                    Name:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Home Phone:
                                
                            
                            
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                                    Mobile Phone:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Email Address:
                                
                            
                            
                                
                                    
                                
                            
                        

                        
                            
                                
                                    Relationship:
                                
                            
                            
                                
                                    
                                
                            
                        

                    
                
            

        
    

        
            
            
                INSURANCES
                
                    
                        
                            
                                (M0063) Medicare Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicare
                        
                    
                    
                        
                            
                                (M0064) Social Security Number:
                            
                        
                        
                            
                        
                        
                             UK - Unknown or Not Available
                        
                    
                    
                        
                            
                                (M0065) Medicaid Number:
                            
                        
                        
                            
                        
                        
                             NA - No Medicaid
                        
                    
                
            
        

        
        
            
                
                    (M0140) RACE/ETHNICITY (Mark all that apply.)
                
                
                    
                        
                            
                                
                                1 - American Indian or Alaska Native
                            
                        
                        
                            
                                
                                3 - Black or African-American
                            
                        
                        
                            
                                
                                5 - Native Hawaiian or Pacific Islander
                            
                        
                        
                    
                    
                        
                            
                                
                                2 - Asian
                            
                        
                        
                            
                                
                                4 - Hispanic or Latino
                            
                        
                        
                            
                                
                                6 - White
                            
                        
                    
                
            
        

        
        
            If an assessment was erroneously submitted in a masked format, that is, it was later discovered that the patient was a Medicare or Medicaid patient
            but was not originally indicated as such at M0150, then an inactivation must be submitted. Normally, the HHA will also submit a new, corrected assessment
            in this situation. For example, if the value at M0150 for a submitted and accepted assessment is not equal to 1, 2, 3, or 4, and it should have been, then
            an inactivation request should be submitted.
            
                
                    (M0150)Current Payment Sources for Home Care (Mark all that apply.)
                
                
                    
                        
                            
                                
                                0 - None; no charge for current services
                            
                        
                        
                            
                                
                                4 - Medicaid (HMO/managed care)
                            
                        
                        
                            
                                
                                8 - Private insurance
                            
                        
                    
                    
                        
                            
                                
                                1 - Medicare (traditional fee-for-service)
                            
                        
                        
                            
                                
                                5 - Workers&quot; , &quot;'&quot; , &quot; compensation
                            
                        
                        
                            
                                
                                9 - Private HMO/managed care
                            
                        
                    
                    
                        
                            
                                
                                2 - Medicare (HMO/managed care/Advantage plan)
                            
                        
                        
                            
                                
                                6 - Title programs (for example, Title III, V, or XX)
                            
                        
                        
                            
                                
                                10 - Self-pay
                            
                        
                    
                    
                        
                            
                                
                                3 - Medicaid (traditional fee-for-service)
                            
                        
                        
                            
                                
                                7 - Other government (for example, TriCare, VA, etc.)
                            
                        
                        

                            
                                
                                11 - Other (specify)
                                
                            
                        
                    
                    
                        
                            
                        
                        
                            
                        
                        
                            
                                
                                UK - Unknown
                            
                        
                    
                
            
        

    


    .panelTitle {
        text-align: center;
        font-weight: bold;
        font-size: 15px;
        padding: 10px;
    }

    div.ui-datepicker {
        font-size: 12px;
    }




    
        
            CLINICAL RECORD ITEMS
        
    
    

    
        
            
                
                


                    
                    
                        COVID-19 SCREENING Followed COVID-19 Infection precaution guidelines for Health Care Professionals. CDC GuidelinesIs Patient/PCG reporting symptoms of  SOB Cough High Fever Sore Throat Chills Repeated shaking with chills Muscle pain Headache New loss of taste or smellat time of Visit or last 48 hours?  No  Yes ** Instructed Patient/PCG on COVID-19 Prevention Measures provided by CDC. ** 

                        
                        
                            
                                
                                    
                                        (M0080) Discipline of Person Completing Assessment
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                1
                                                2
                                                3
                                                4
                                            
                                        
                                    
                                    
                                        
                                            
                                                1
                                                RN
                                            
                                            
                                                2
                                                PT
                                            
                                            
                                                3
                                                SLP/ST
                                            
                                            
                                                4
                                                OT
                                            
                                        
                                    
                                
                                
                                    
                                        Care Staff Name:
                                        
                                            
                                            
                                            
                                            
                                                
                                                    Acosta, Casey 
                                                
                                            
                                        
                                    
                                
                            
                        

                         
                    

                    
                    
                         

                        
                            
                                
                                    (M0090) Date Assessment Completed:
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                         Time In:    Time Out:    
                    

                    
                    
                        
                        

                        
                        
                            
                                
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                
                                                
                                                
                                                
                                                
                                                8
                                                9
                                            
                                        
                                    
                                    
                                    
                                    
                                    
                                        
                                            Discharge from Agency - Not to an Inpatient Facility
                                        
                                        
                                            
                                                
                                                    8
                                                    Death at home [ Go to M2005 ]
                                                
                                                
                                                    9
                                                    Discharge from agency [ Go to M1041 ]
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                
            
        
    






    
        
            MEDICATIONS
        
    
    

    
        

            
                
                

                    
                    

                    
                    

                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M2005) Medication Intervention: Did the agency contact and complete physician (or physician-designee) prescribed/recommended actions by midnight of the next calendar 
                                        day each time potential clinically significant medication issues were identified since the SOC/ROC?
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                            
                                                
                                                0
                                                1
                                                9
                                            
                                        
                                    
                                    
                                        
                                            
                                                0
                                                No
                                            
                                            
                                                1
                                                Yes
                                            
                                            
                                                9
                                                NA - There were no potential clinically significant medication issues identified since SOC/ROC or patient is not taking any medications
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    
                    

                    
                    

                
            

        
    




    
        
            OTHERS
        
    
    

    
        

            
                
                


                    
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (J1800) Any Falls Since SOC/ROC, whichever is more recent.
                                    
                                    
                                
                                
                                    
                                        Enter Code
                                        
                                            
                                                
                                                0
                                                1
                                                -
                                            
                                        
                                    
                                    
                                        Has the patient had any falls since SOC/ROC, whichever is more recent?
                                    
                                    
                                        
                                            
                                                0
                                                No → Skip J1900
                                            
                                            
                                                1
                                                
                                                    Yes → Continue to J1900. Number of Falls Since SOC/ROC, whichever is more recent
                                                
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    
                        

                        
                            
                                
                                    
                                        (J1900) Number of Falls Since SOC/ROC, whichever is more recent
                                    
                                    
                                
                                
                                    
                                        
                                            CODING:
                                        
                                        0. None
                                        1. One
                                        2. Two or more
                                    
                                    
                                        
                                            
                                                
                                                    Enter Codes in Boxes
                                                
                                            
                                            
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                A. No injury: No evidence of any injury is noted on physical assessment by the nurse or primary care clinician; no complaints of pain or injury by the patient; no change in the patient&quot; , &quot;'&quot; , &quot;s behavior is noted after the fall
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                B. Injury (except major): Skin tears, abrasions, lacerations, superficial bruises, hematomas and sprains; or any fall-related injury that causes the patient to complain of pain
                                            
                                        
                                        
                                            
                                                
                                                    
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    
                                                
                                            
                                            
                                                C. Major injury: Bone fractures, joint dislocations, closed head injuries with altered consciousness, subdural hematoma
                                            
                                        
                                    
                                
                            
                        

                        
                    

                    
                    

                    
                    

                    

                    

                    
                    
                        

                        
                            
                                
                                    
                                        (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.
                                    
                                    
                                
                                
                                    
                                    
                                        
                                            
                                                
                                                
                                            
                                        
                                    
                                
                            
                        

                        DISCIPLINES INVOLVED: SN PT OT ST MSW Aide Other     All involved team members notifiedWas a referral made to MSW for assistance with community resources/assistance with a living will/counseling needs  (depression/suicidal ideation) and/or unsafe environment? Date  Yes No Refused N/AComment:    Complete this Section for either Transfer to Inpatient Facility or Death at Home.REASON FOR ADMISSION TO HOME HEALTH AND SUMMARY OF CARE TO DATE (describe condition):   textAreaAdjust(false,11293);DETAILS RELATED TO EMERGENT CARE AND/OR HOSPITALIZATION/NURSING HOME (when known):   textAreaAdjust(false,11296);Copy of summary   sent faxedDate:   To:  Physician    Facility Name    Copy of current P.O.C. attached  Yes NoCurrent medication list attached  Yes NoAdvance directive exists  Yes NoCopy attached  Yes NoDNR  Yes NoCopy attached  Yes No
                    

                
            

        
    



                        

                        
                        
                    
                
            

            

        
    

    
        
            
                
                    M0Set Lookup:
                    
                
            

            
                
                    
                        
                            
                            Go
                        
                        
                            
                            Save
                        
                        
                            
                            Validate
                        
                        
                            
                            Save &amp; Back
                        
                        
                            
                            Save &amp; Next
                        
                        
                        
                        
                            Notes
                        
                        Export Supplementals
                        
                        
                    
                

                
                    
                
            
        

        
            
                Notes
                
                    X
            
            
                
            
            
                
                    Save
                    
                        Cancel
                    
                
            
            
                
            
        

        
            
                Validation Results
                
                    
                
            
            Printer Friendly Format


    

    

    

        
    

    
        


    .carePlanProblemContainer {
        padding: 0px 8px;
        width: 100%;
        display: table;
        margin: 12px 0px;
        border: 1px solid #d0d0d0;
        border-left: 4px solid #ce3333a8;
    }

    .carePlanCareGoalContainer {
        width: 100%;
        display: table;
        border-left: 4px solid #3937b5a8;
        margin-top: 10px;
        margin-bottom: 20px;
    }

    .carePlanInterventionContainer {
        width: 100%;
        border-left: 4px solid #37b54ca8;
    }

    .carePlanExpandArrow {
        font-size: 18px;
        line-height: 32px;
        color: #1c83a9;
    }

    .carePlanPaddingLeft15 {
        padding-left: 15px;
    }

    #assignCgDate .input-group-addon {
        font-size: 11px;
        background-color: #e9e9e9;
    }

    .goalOutComeLegendHeader {
        padding: 8px 8px;
        margin: 0;
        font-size: 13px;
        font-weight: 600;
        color: inherit;
        background-color: #e8e8e8;
        border-bottom: 1px solid #cacaca;
        border-top-left-radius: calc(.3rem - 1px);
        border-top-right-radius: calc(.3rem - 1px);
    }

    .goalOutComeLegendItem {
        margin: 10px;
        font-size: 13px;
    }



    
        Care Plan
        ×
    
    
        
            
                
                    Add New Problem
                    
                    Get Updated CarePlan
                
            
        
        
            
                

            
        
        
            
                
            
        

        
            
                
                    
                        ×
                          Add New Problem
                    
                    
                        
                            
                                Body System
                            
                            

                                
                                    
                                
                            
                        

                        
                            

                                
                                    
                                        Problem
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Care Goals
                                    
                                    
                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention
                    
                    
                        
                            
                                Problem
                            
                            
                                
                            
                        
                        
                            
                                Care Goal
                            
                            
                                
                            
                        

                        
                            

                                
                                    
                                        Intervention
                                    
                                    

                                        
                                    
                                

                            
                            
                                
                                    Search
                                    
                                
                            
                        
                        
                            
                        
                    
                    
                        Add
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          
                    
                    
                        
                            
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    Acosta, Casey SN
                                
                            
                        
                        
                            
                                
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                        Cancel
                    
                
            
        

        
            
                
                    
                          Set Required Target Date
                    
                    
                        
                            
                                Care Goal(s):
                                
                                    
                                
                            
                        
                        
                            
                                Target Date
                            
                            
                                
                                    
                                    
                                
                            
                        
                    
                    
                        Ok
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Problem Template
                    
                    

                        
                            
                                Body System
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Problem
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Care Goal Template
                    
                    

                        
                            
                                Problem
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Care Goal
                            
                            
                                
                            
                        
                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                        ×
                          Add New Intervention Template
                    
                    

                        
                            
                                Care Goal
                            
                            
                                
                                    
                                        
                                            
                                                
                                            

                                        
                                        
                                            
                                        
                                    
                                
                                
                                    
                                
                            
                        

                        
                            
                                Intervention
                            
                            
                                
                            
                        

                    
                    
                        Save
                        Cancel
                    
                
            
        

        
            
                
                    
                          Intervention Comments
                    
                    
                        
                            
                                Problem :  
                            
                            
                                Care Goal :  
                            
                            
                                Intervention :  
                            
                        

                        
                            
                                
                                    
                                        Comments
                                    
                                
                                
                                    
                                
                            
                        
                        
                        
                            
                                
                            
                            
                                
                                    
                                        Note: 
                                        Closing the comment section will automatically update comments of existing interventions.
                                            Newly inserted interventions with comments should be updated manually.
                                    
                                    
                                        Add Comment
                                        
                                    
                                
                            
                        
                    
                
            
        

        
            GOAL OUTCOME LEGEND:
            1 - Goal met. Hopeful. Significant progress. (75-100% desired level was attained)
            2 - Goal met with ongoing action. Often hopeful. Moderate progress. (50-75% desired level was mostly attained)
            3 - Goal partially met. Sometimes hopeful. Slight progress. (25-50% desired level was occasionally attained)
            4 - Goal not met. Not much hope. No progress. (0-25% desired level was rarely attained)
        

    


    

&quot;))]</value>
      <webElementGuid>ba2a0a37-6efe-4fc6-bf4f-c7950019c482</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
