<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Go_glyphicon glyphicon-search</name>
   <tag></tag>
   <elementGuidId>ce04fd9d-46b3-4c08-9181-fff45c8900d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-search</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2894a439-1544-4fc9-a659-4f50a3226b2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-search</value>
      <webElementGuid>d9cd49b1-9582-4239-9ca6-892e87b41378</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/fieldset[1]/div[@class=&quot;col-xs-12 col-sm-6 col-md-6 col-lg-6&quot;]/button[@class=&quot;btn btn-default btn-sm toolbar-btn-default font12&quot;]/span[@class=&quot;glyphicon glyphicon-search&quot;]</value>
      <webElementGuid>bb1af6b1-2bf2-4636-aada-f5916a0673bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div[2]/fieldset/div/button/span</value>
      <webElementGuid>2f8379f2-fa26-4ad5-8b6c-8793f6e03ac3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div[2]/fieldset/div/button/span</value>
      <webElementGuid>4508a15e-fab8-4121-ab16-23b9d626c0b7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
