<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_M0Set Lookup_form-control input-sm ng_a573e6</name>
   <tag></tag>
   <elementGuidId>fdf766d9-0a6f-45b7-b273-78e5162ea3b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-3.col-md-2.col-lg-2 > div.form-inline.input-group > input.form-control.input-sm.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @ng-model = 'oasis.goToMosetVal']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[81]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e868fd7f-bad5-4401-9e0e-647d66228f1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>31e70a4c-b073-4a48-b775-9e8aa7805746</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control input-sm ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>d5bf93b4-e0f2-4e64-8704-a9d5a79db574</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.goToMosetVal</value>
      <webElementGuid>3ce35811-134b-49c8-b8aa-6fd87dfc2ec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>!oasis.oasisDetails.FULL_OASIS</value>
      <webElementGuid>0e2ab627-7576-471b-a831-f963288fac56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl01&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;row ng-scope&quot;]/div[@class=&quot;row footer_nav_container_oasis&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-12 col-sm-3 col-md-2 col-lg-2&quot;]/div[@class=&quot;form-inline input-group&quot;]/input[@class=&quot;form-control input-sm ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>f5069b9e-1f55-494b-ada4-342f84d5d16f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[81]</value>
      <webElementGuid>07a56356-1f5e-437a-958f-db48bf5dc66b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='ctl01']/div[4]/div[3]/div/div[2]/div[7]/div/div/div/input</value>
      <webElementGuid>cecf1406-8899-4e9e-b0dd-4888a3630f8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/input</value>
      <webElementGuid>19595ffd-0f40-4c7b-9630-f25578b298cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>616ecab8-2677-4f8f-909f-12a8589de750</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
