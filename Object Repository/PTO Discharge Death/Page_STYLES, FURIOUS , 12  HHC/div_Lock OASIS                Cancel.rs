<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Lock OASIS                Cancel</name>
   <tag></tag>
   <elementGuidId>a2473bbd-5bef-4dad-bcbb-8da3f296fd95</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[1]/following::div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal.fade.ng-scope.in > div.modal-dialog.modal-sm > div.modal-content > div.modal-footer</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7f5a74ff-c729-4c47-8f73-f2ec2d0e147f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-footer</value>
      <webElementGuid>f21bb850-cccf-4b9a-bb6f-92807f45e803</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Lock OASIS
                Cancel
            </value>
      <webElementGuid>b69da0b8-2e69-4dbe-8391-6d11c6f5f43e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dxChrome dxWindowsPlatform dxWebKitFamily dxBrowserVersion-105 ng-scope&quot;]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;modal fade ng-scope in&quot;]/div[@class=&quot;modal-dialog modal-sm&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-footer&quot;]</value>
      <webElementGuid>86db7701-5a79-4672-bbcd-c8bbdb3c9aeb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Date:'])[1]/following::div[5]</value>
      <webElementGuid>ada5b04d-b2ca-4986-bb5b-938733bfe7e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock Oasis'])[1]/following::div[7]</value>
      <webElementGuid>a4dc7dc3-f486-4b0a-ba0f-647891a6464b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div[4]</value>
      <webElementGuid>e52cf06c-6a5b-4ec6-b812-a0c4522f2637</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Lock OASIS
                Cancel
            ' or . = '
                Lock OASIS
                Cancel
            ')]</value>
      <webElementGuid>eabc8133-094e-4211-b678-6bb1b08c5ee6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
