<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_0                                   _879a22</name>
   <tag></tag>
   <elementGuidId>a9a061a3-e913-4918-bacd-72da5ee921ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='J1900']/div/div[2]/div[2]/div[2]/div/div/select</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-4.col-sm-4.col-md-1.col-lg-1.row > div.form-inline > select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>88838c3e-9526-4275-8122-c309eabf8562</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>d295588c-ef26-4f3d-884a-e5c9b18ce51c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>J1900A</value>
      <webElementGuid>96bdf268-051f-4165-a0a6-bd1123d14db9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>oasisversion</name>
      <type>Main</type>
      <value>OasisD</value>
      <webElementGuid>896ff51a-cb88-4e68-8350-e4486d249d4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1900A</value>
      <webElementGuid>d78b92a0-703b-4868-9bf4-0674d152c229</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('J1900A', oasis.oasisDetails.J1900A, 'OasisD')</value>
      <webElementGuid>68d5e1dd-7060-4c25-bfc8-de8c7f1f1d87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.oasisDetails.J1800 == '0' || oasis.isLocked</value>
      <webElementGuid>77ce82f5-c5cb-4876-9a07-e2481f826379</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    </value>
      <webElementGuid>75e30710-3958-4ca5-8b2b-e67565b2f041</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;J1900&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 bottom-space&quot;]/div[@class=&quot;col-xs-12 col-sm-7 col-md-10 col-lg-10&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;col-xs-4 col-sm-4 col-md-1 col-lg-1 row&quot;]/div[@class=&quot;form-inline&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>050a8f78-1711-4571-9037-f5ee37b8e5a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='J1900']/div/div[2]/div[2]/div[2]/div/div/select</value>
      <webElementGuid>8eea0749-dcc7-47e9-be08-fa3e82210884</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Codes in Boxes'])[1]/following::select[1]</value>
      <webElementGuid>88557513-d67d-4c50-8130-03cd85996ac9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING:'])[1]/following::select[1]</value>
      <webElementGuid>3a412849-a764-4e48-bb52-e681ccf2f850</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A. No injury:'])[1]/preceding::select[1]</value>
      <webElementGuid>59a5b81a-d232-4883-a7f1-e22029263455</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B. Injury (except major):'])[1]/preceding::select[2]</value>
      <webElementGuid>376c13d9-a377-4be2-b855-fbe2bfcaa668</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/select</value>
      <webElementGuid>2b564450-9112-490d-90e5-7701278208a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ' or . = '
                                                        
                                                        0
                                                        1
                                                        2
                                                        -
                                                    ')]</value>
      <webElementGuid>36843851-3531-4543-b4b6-ba52464386b4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
