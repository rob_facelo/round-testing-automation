<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_CODE</name>
   <tag></tag>
   <elementGuidId>42e6cc7a-d50c-418c-98d4-7e4c69fc4c8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select.form-control.enter-code.ng-pristine.ng-untouched.ng-valid</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ng-model = 'oasis.oasisDetails.M0080_ASSESSOR_DISCIPLINE' and (text() = '
                                        
                                        1
                                        2
                                        3
                                        4
                                    ' or . = '
                                        
                                        1
                                        2
                                        3
                                        4
                                    ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='M0080']/div/div[2]/div/div/div[2]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>5f19b78a-15da-45da-9e2d-97791d6a5add</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control enter-code ng-pristine ng-untouched ng-valid</value>
      <webElementGuid>f4767a09-8db2-4037-a95a-3214bc860e22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mitemfield</name>
      <type>Main</type>
      <value>M0080_ASSESSOR_DISCIPLINE</value>
      <webElementGuid>20175eea-18f5-4546-a713-f9e8049a660e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>oasis.oasisDetails.M0080_ASSESSOR_DISCIPLINE</value>
      <webElementGuid>1bcaaa67-fc78-41b4-b104-1db178f8aab9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>oasis.oasisDetailChange('M0080_ASSESSOR_DISCIPLINE', oasis.oasisDetails.M0080_ASSESSOR_DISCIPLINE)</value>
      <webElementGuid>33975e43-36e5-4e9e-8e14-ffa1a138d80d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>oasis.isLocked</value>
      <webElementGuid>3fb6bdc9-408b-459b-94ce-a96764c3e54e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        1
                                        2
                                        3
                                        4
                                    </value>
      <webElementGuid>1aeae11f-4957-4e42-9ecf-c720dca5a626</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M0080&quot;)/div[@class=&quot;form-group panel panel-primary hhc-blue-border&quot;]/div[@class=&quot;panel-body panelContent&quot;]/div[1]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 row&quot;]/div[@class=&quot;col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 form-inline row&quot;]/select[@class=&quot;form-control enter-code ng-pristine ng-untouched ng-valid&quot;]</value>
      <webElementGuid>e69a587f-4628-46f6-a881-e90a7c5bb908</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='M0080']/div/div[2]/div/div/div[2]/select</value>
      <webElementGuid>9e34e325-d323-4fb3-8079-384af191592d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter Code'])[1]/following::select[1]</value>
      <webElementGuid>0f0ce5d8-e7c4-41cc-8765-9b6190bc0347</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='M0080 Discipline of Person Completing Assessment.'])[1]/following::select[1]</value>
      <webElementGuid>7f517aa3-1464-4175-98e8-0102851e4aac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RN'])[1]/preceding::select[1]</value>
      <webElementGuid>9a074a28-bb52-43f7-beb4-bc16de73c988</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PT'])[1]/preceding::select[1]</value>
      <webElementGuid>d0a54b7c-0bb0-42b2-b498-ce9a975220b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/select</value>
      <webElementGuid>ab837463-d081-4a81-9392-a6adca38b61f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[(text() = '
                                        
                                        1
                                        2
                                        3
                                        4
                                    ' or . = '
                                        
                                        1
                                        2
                                        3
                                        4
                                    ')]</value>
      <webElementGuid>149b6e36-1d8a-426d-b4db-bd002de4cf06</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
